

class Configuration():
    """
    A class which describes a configuration we would like to compile and put in test in a Yaml file.
    
    The expected usage of this class is to use its Print() method which prints the content as expected in the Yaml
    file.
    """
    
    
    def __init__(self, os, compiler, mode, library_type, is_one_library):
        """
        \param os The chosen OS, among "macos", "fedora" and "ubuntu"
        \param compiler "clang" or "gcc" so far
        \param mode "debug" or "release"
        \param library_type "static" or "shared"
        \param[in] is_one_library If True, MoReFEM will generate (mostly) one library (with the exception of
         post-processing and test which are separate). If False, each MoReFEM module will be in its own library 
        (except Utilities and ThirdParty which are way too intertwined).
        
        
        """
        __slots__ = [ "__os", "__compiler", "__mode", "__library_type", "__is_one_library" ]
        
        self.__os = os
        self.__compiler = compiler
        self.__mode = mode
        self.__library_type = library_type
        self.__is_one_library = is_one_library
        
        
    def __key(self):
        """Returns the key that will be used to designate the job in the Yaml file."""
        
        if self.__is_one_library:
            library_description = "one_{}_library".format(self.__library_type)
        else:
            library_description = "several_{}_libraries".format(self.__library_type)
        
        return "{os}_{compiler}_{mode}_{library_description}".format(os = self.__os, \
                                                                   compiler = self.__compiler, \
                                                                   mode = self.__mode, \
                                                                   library_description = library_description)
        
        
    def __PrintTagsAndVariables(self, do_define_tags):
        """Helper method to Print().
        
        \param define_tags If True, a field with tags will also be written to coerce the jobs to go on dedicated
        VMs.
        """
        
        if do_define_tags:
            print("  tags:")
            print("      - {}".format(self.__os))
            print("      - {}".format(self.__compiler))
            print("      - {}".format(self.__mode))
        
        print("  variables:")
        print('      OS: "{}"'.format(self.__os))
        print('      COMPILER: "{}"'.format(self.__compiler))
        print('      MODE: "{}"'.format(self.__mode))
        print('      LIB_NATURE: "{}"'.format(self.__library_type))
        print('      IS_ONLY_ONE_LIB: "{}"'.format(self.__is_one_library))
        
    
    def Print(self, do_define_tags):
        """Print the two jobs related to the chosen configuration; the output may be pasted as such in the Yaml file.
        
        \param define_tags If True, a field with tags will also be written to coerce the jobs to go on dedicated
        VMs.
        """
        
        
        key = self.__key()
        
        if self.__os in ("ubuntu", "fedora"):
            unix = "linux"
        elif self.__os == "macos":
            unix = "macos"
        else:
            raise Exception("Chosen OS ({}) is not a known one".format(self.__os))
        
        print("build_{}:".format(key))
        print("  extends: .build_template_{}".format(unix))
        self.__PrintTagsAndVariables(do_define_tags)
        print("")
        print("check_{}:".format(key))
        print("  extends: .check_compilation_warning_template")
        self.__PrintTagsAndVariables(do_define_tags)


if __name__ == "__main__":
    
    do_define_tags = True
    
    configuration_list =  \
        (
            Configuration("ubuntu", "gcc", "debug", "shared", False),
            Configuration("ubuntu", "gcc", "release", "static", True),
            Configuration("fedora", "gcc", "debug", "shared", True),
            Configuration("fedora", "gcc", "release", "static", False),
            Configuration("fedora", "clang", "debug", "shared", False),
            Configuration("fedora", "clang", "release", "static", True),
#            Configuration("macos", "clang", "debug", "static", True),
#            Configuration("macos", "clang", "release", "shared", False),
        )
    
    for config in configuration_list:
        config.Print(do_define_tags)
        print("\n")
