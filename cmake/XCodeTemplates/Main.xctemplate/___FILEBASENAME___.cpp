//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//


#include "Model/Main/Main.hpp"
#include "Model.hpp"

int main(int argc, char** argv)
{
    return MoReFEM::ModelNS::Main<___VARIABLE_problemName:identifier___NS::Model>(argc, argv);
}
