//! \file 

#include "InputData.hpp"


namespace MoReFEM::___VARIABLE_modelName:name___NS
{


    void ModelSettings::Init()
    {
        // ****** Numbering subset ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::example)>>(
                "Describe the purpose of your numbering subset");
            Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::example)>::DoMoveMesh>(false);
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::example)>>(
                { "Describe the purpose of your unknown" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::example)>::Name>("example");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::example)>::Nature>("vectorial");
        }


         // ****** Mesh ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(MeshIndex::example)>>(
                { "Describe the purpose of your mesh" });
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::item)>>(
                { "Describe the purpose of your domain" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::item)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::example) });
        }

        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::example)>>(
                "Describe the purpose of your finite element space");

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::example)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::example));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::example)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::item));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::example)>::UnknownList>(
                { "example_unknown" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::example)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::example) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::example)>>(
                { "Describe the purpose of your boundary condition" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::example)>::UnknownName>(
                "bc");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::example)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::item));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::example)>::IsMutable>(
                false);
        }


        // ****** PETSc solver ******
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({  "Describe the purpose of your solver" });
    }


} // namespace MoReFEM::___VARIABLE_modelName:name___NS
