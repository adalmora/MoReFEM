//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include <memory>
# include <type_traits>  // IWYU pragma: export
# include <vector>

# include "Geometry/Domain/Domain.hpp"

# include "FormulationSolver/VariationalFormulation.hpp"

# include "___VARIABLE_pathToFiles:name___/InputData.hpp"


namespace MoReFEM::___VARIABLE_modelName:name___NS
{
    

    //! \copydoc doxygen_hide_simple_varf
    class VariationalFormulation final
    : public MoReFEM::VariationalFormulation
    <
        VariationalFormulation,
        EnumUnderlyingType(SolverIndex::solver)
    >
    {
    private:
        
        //! \copydoc doxygen_hide_alias_self
        // \TODO This might seem a bit dumb but is actually very convenient for template classes.
        using self = VariationalFormulation;
        
        //! Alias to the parent class.
        using parent = MoReFEM::VariationalFormulation
        <
            self,
            EnumUnderlyingType(SolverIndex::solver)
        >;
        
        static_assert(std::is_convertible<self*, parent*>());
        
        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;
        
    public:
        
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;
        
    public:

        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_varf_constructor
        explicit VariationalFormulation(const morefem_data_type& morefem_data,
                                        const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        TimeManager& time_manager);

        //! Destructor.
        ~VariationalFormulation() override;

        //! \copydoc doxygen_hide_copy_constructor
        VariationalFormulation(const VariationalFormulation& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariationalFormulation(VariationalFormulation&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariationalFormulation& operator=(const VariationalFormulation& rhs) = delete;
        
        //! \copydoc doxygen_hide_move_affectation
        VariationalFormulation& operator=(VariationalFormulation&& rhs) = delete;

        ///@}
        
        
    private:

        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
            * \brief Allocate the global matrices and vectors.
            */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;



        ///@}
        
    private:
        
        
        /// \name Global variational operators.
        ///@{
        
        // \TODO Define there as data attributes the global operators involved in the formulation.
        // They should be defined as std::unique_ptr; accessors that returns a reference to them should also be
        // foreseen.
    
        ///@}
        
    private:
        
        /// \name Global vectors and matrices specific to the problem.
        ///@{
        
        // \TODO Define here the GlobalMatrix and GlobalVector objects into which the global operators are 
        // assembled.
        
        ///@}
        
    private:
        
           

    };

    
} // namespace MoReFEM::___VARIABLE_modelName:name___NS


# include "___VARIABLE_pathToFiles:name___/___FILEBASENAME___.hxx" // IWYU pragma: export


#endif 
