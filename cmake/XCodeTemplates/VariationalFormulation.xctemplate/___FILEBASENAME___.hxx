//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX


namespace MoReFEM::___VARIABLE_modelName:name___NS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


} // namespace MoReFEM::___VARIABLE_modelName:name___NS


#endif /* defined(_____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HXX) */
