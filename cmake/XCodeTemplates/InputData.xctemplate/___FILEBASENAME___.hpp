//! \file 
//
//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#ifndef _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP
# define _____PROJECTNAMEASIDENTIFIER________FILEBASENAMEASIDENTIFIER_____HPP

# include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

# include "Core/InputData/Instances/Geometry/Mesh.hpp"
# include "Core/InputData/Instances/Geometry/Domain.hpp"
# include "Core/InputData/Instances/FElt/FEltSpace.hpp"
# include "Core/InputData/Instances/FElt/Unknown.hpp"
# include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
# include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
# include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"


namespace MoReFEM::___VARIABLE_modelName:name___NS
{

    
    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t
    {
        example = 5 // will generate a `Mesh_5` field in the Lua file
    };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t
    {
        item = 7 // will generate a `Domain_7` field in the Lua file
        // the name used doesn't matter much - could be the same or not 
        // due ti the use of an `enum class`
    };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t
    {
        example = 6
    };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t
    {
        example = 12
    };
    
    
    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t
    {
        example = 1
    };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex
    {
        solver = 1
    };
    
    
    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t
    {
         example = 1
    };
    

    //! \copydoc doxygen_hide_input_data_tuple
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::example),
        
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::item),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::example),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::example),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Result
        // TODO: Add the model-specific input parameters here!
    >;
    
     //! \copydoc doxygen_hide_input_data_type
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::example),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::example),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::example),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::item),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::example),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::example),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver)
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::model>;
        
    
} // namespace MoReFEM::___VARIABLE_modelName:name___NS


#endif 
