[TOC]

# Main library 

As already told in the [main README](../../README.md), the MoReFEM library may be compiled:

- Either as a single library
- Or as 10 different libraries, which are organized in a hierarchy.

depending on the value of `BUILD_MOREFEM_UNIQUE_LIBRARY` in the CMake build (in the precache file or in command line).

The idea is that it is within given library we may only use symbols defined in lower level modules: a function defined in *Core* may use something defined in *Utilities* but not something defined in *Geometry*.

The different layers, from the lower levels to the higher ones, are:

## Utilities / ThirdParty

## Utilities

a lot of handy tools that might as well be used in completely different projects (e.g. displaying content of a container, creating safely a file on a filesystem, etc...)
## ThirdParty

stuff related to third party libraries: wrappers to provide a nice C++ interface within the code, facility to avoid getting compiler warnings from header files, utility functions to extend their use, etc...

Both those modules are compiled together in a single libraries are both are strongly coupled.

## Core

This module encompass some generic stuff that may be used at every level of MoReFEM; contrary to Utilities ones however they are already specific to the library.

## Geometry

This module encompass purely geometric objects of the code, such as the mesh, geometric elements, domains and so forth.

## FiniteElement

Large module to encompass stuff closely related to finite elements: nodes and dofs, boundary conditions, finite element spaces, unknowns and so forth...

## Parameters

This module encompass the API of so-called Parameters, which are objects for which values at quadrature points might be queried.

## Operators

This module encompass operators the API for several type of operators: variational operators, interpolators, etc...

## ParameterInstances

This module provides implementation of several parameters; it is separated from Parameters module as knowledge of stuff defined in Operators is required.

## OperatorInstances

This module provides implementation of several operators. As for ParameterInstances, it is separated from Operators module as it requires stuff from Parameters and ParameterInstances.

## FormulationSolver

This module provides the base class used to define a variational formulation.

## Model

This module is around the Model class, which drives the whole MoReFEM program.


# Ancillary libraries

Even when `BUILD_MOREFEM_UNIQUE_LIBRARY` is set to True, few libraries and executables are built besides the principal library upon calling your build command (`make` or `ninja` or whatever you chose to use):

## PostProcessing

A module which uses up the basic output data to provide a usable output. For instance default output might be used to provide input for Ensight visualisation software.

## ModelInstances

This module build several executables to showcase how to use the library.

Each of the Model there comes with a demo Lua file which enables to run directly a model after compilation.

More models run independently are present on Gitlab [here](https://gitlab.inria.fr/MoReFEM/Models); when writing your own model it is advised to do so in an independent git repository as for those models rather than putting them in the ModelInstances which shares the git repository of the main library.

## Test

Unit and integration tests.