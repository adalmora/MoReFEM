[TOC]

# Introduction

The goal of this tutorial is to build step by step a rather simple model using MoReFEM: elastic deformation of a bar upon which a force is applied; time step is constant and volumic mass is the same throughout the bar.  

The library was first built with a use in macOS and XCode in mind, but it has been extended since and the procedure should guide you to make the project work in both macOS and Linux environments.


# Description of the mechanical problem

In this tutorial we are solving a simple <strong>2D problem</strong>: the deformation of a bar within the theory of <strong> linear elasticity </strong> using a finite element method. Here is a quick description of the equations depicting the problem and the resolution method that we will implement step by step; our model will in fact be generic enough to work with 3D cases.

The strong formulation of the equilibrium equations of a dynamic mechanical problem on a fixed domain $ \Omega_t $  reads:
```math

\begin{cases}
\displaystyle  \rho(\underline{f}(\underline{x}) - \underline{\ddot{y}}(\underline{x})) + \underline{\nabla}_{\underline{x}} \cdot \underline{\underline{\sigma}}(\underline{x}) = 0 \quad \text{in $\Omega_t$} \\
\displaystyle \underline{\underline{\sigma}}(\underline{x}) \cdot \underline{n} = \underline{g}(\underline{x}) \quad \text{on } \Gamma^N \\
\displaystyle \underline{y}(\underline{x}) = \underline{0} \quad \text{on } \, \Gamma^D \\
\displaystyle \underline{\underline{\sigma}}^T(\underline{x}) = \underline{\underline{\sigma}}(\underline{x})
\end{cases}

```

The domain $\Omega_t$ over which the solid is defined has a fixed boundary $\Gamma^D$ where Dirichlet conditions are applied (here we impose a zero displacement condition) and another boundary $\Gamma^N$ where Neumann conditions are applied (for instance external surfacic loadings in 3D). $\underline{\underline{\sigma}}$ is the Cauchy stress tensor, $\rho$ the volumic mass in the current configuration, $\underline{f}$ represents the applied external forces, $\underline{\ddot{y}}$ is the acceleration, $\underline{n}$ is the outward-pointing normal of the boundary $\Gamma^N $ and $\underline{g}$ the external surfacic load.

The weak formulation of the fundamental law of dynamics in the deformed configuration reads:
```math
\forall \underline{y}^* \in \mathcal{V}, \quad \int_{\Omega_t} \rho(\underline{f}-\underline{\ddot{y})} \cdot \underline{y}^* \textrm{d}\Omega_t + \int_{\Omega_t} \underline{\underline{\sigma}}:\underline{\nabla}_{\underline{x}}\underline{y}^* \textrm{d}\Omega_t  = \int_{\Gamma^N} \underline{g}\cdot \underline{y}^* \text{d}S 
```

In order to solve this equation without having to deal with the changes of positions due to the fact that each variable is expressed in the reference configuration, we can rewrite this equation within a Total Lagrangian formalism.  Here we will only consider a surfacic loading $ \underline{g}(\underline{x})$  without a volumic contribution (namely $ \underline{f}(\underline{x}) = \underline{0} $). Without going into the details of the chain rules involved, the variational formulation of the fundamental law of dynamics in total Lagrangian formalism within elasticity theory reads:  

```math
\forall \underline{y}^* \in \mathcal{V}, \quad \displaystyle \int_{\Omega _{0}}^{} \rho_0 \underline{\ddot{y}} \cdot \underline{y}^{*}\textrm{d}\Omega_0 + \int_{\Omega_{0}}^{} \left( \underline{\underline{\underline{\underline{\text{A}}}}} : \underline{\underline{\varepsilon}}(\underline{y}) \right) : \underline{\underline{\varepsilon}}(\underline{y}^*) \textrm{d}\Omega_0
= \int_{\Gamma_0^N} \underline{g}_0 \cdot \underline{y}^* \text{d}S_0 
```
Where $ \underline{\underline{\varepsilon}}(\underline{y}) = \underline{\underline{\nabla}} \, \underline{y} + (\underline{\underline{\nabla}} \, \underline{y})^T  $ is the linearized strain tensor and the consitutive behaviour law $ \underline{\underline{\sigma}} = \underline{\underline{\underline{\underline{\text{A}}}}} : \underline{\underline{\varepsilon}}(\underline{y}) $.

Here we are only solving for one unknown: the displacement  $ \underline{y}$. In order to do so, we will place ourselves within the <strong>plane strain </strong> hypothesis, namely $ \varepsilon_{33} = 0$ which implies: 

```math
\underline{\underline{\sigma}} =  \begin{pmatrix} 
\sigma_{xx} & \sigma_{xy} & 0 & \\
\sigma_{xy} & \sigma_{yy} & 0 & \\
0 & 0 & \sigma_{zz} & 
\end{pmatrix} \quad \text{with} \quad \displaystyle \sigma_{zz} =  \frac{\lambda}{2(\mu+\lambda)}(\sigma_{xx}+\sigma_{yy})
```
where $\lambda$ and $\mu$ are the Lamé coefficients of the elastic solid.

Thus the constitutive behaviour law in 2D reads, using engineering notation:

```math
\begin{pmatrix}
\sigma_{xx}(\underline{y}) & \\
\sigma_{yy}(\underline{y}) & \\
\sigma_{xy}(\underline{y}) & 
\end{pmatrix} =  \begin{pmatrix}
\lambda + 2\mu & \lambda & 0 & \\
\lambda  & \lambda + 2\mu  & 0 & \\
0 & 0 & \mu & 
\end{pmatrix}\begin{pmatrix}  
\varepsilon_{xx}({\underline{y}}) &\\
\varepsilon_{yy}({\underline{y}}) &\\
2\varepsilon_{xy}({\underline{y}}) &

\end{pmatrix} = \underline{\underline{\hat{\text{A}}}} \cdot \underline{\hat{\varepsilon}} (\underline{y})
```
Finally, we are left with the following set of equations and boundary conditions:

```math
\begin{cases}
\displaystyle \forall \underline{y}^* \in \mathcal{V}, \quad \displaystyle \int_{\Omega _{0}}^{} \rho_0 \, \underline{y}^{*} \cdot  \underline{\ddot{y}} \, \textrm{d}\Omega_0 + \int_{\Omega_{0}}^{}   \underline{\hat{\varepsilon}}(\underline{y}^*)^T \cdot \underline{\underline{\hat{\text{A}}}} \cdot \underline{\hat{\varepsilon}}(\underline{y}) \, \textrm{d}\Omega_0
= \int_{\Gamma_0^N}  \underline{y}^* \cdot \underline{g}_0 \, \text{d}S_0 \\
\displaystyle \underline{y}(\underline{x}) = \underline{0} \quad \text{on } \, \Gamma^D 
\end{cases}
```
This bilinear system (with respect to the virtual displacement field $ \underline{y}^* $ and $\underline{y}$) will be solved using the finite element method.

# Resolution of the linear system
## Spatial discretization
In order to solve our previously defined bilinear system (with respect to $\underline{y}$ and $\underline{y}^*$), we will be using the standard Galerkin method. It consists of approximating the function of interest (the displacement field in our case) by a finite sum of known shape functions (polynomials usually) $\phi_k(\underline{\xi})$   weighted by unknown coefficients  $y_{jk} $  where $k \in [1, \,  N + 1] $, $ N $ being the order of the shape functions used. In 2D, the discretization of the displacement field gives: 

```math
y_j = \sum_{k=1}^{N+1} y_{jk} \phi_k (\underline{\xi}) \quad j \in [x,y]  
```

```math
\underline{y}(\underline{x})_{2D} = \begin{pmatrix} 
y_{x} & \\
y_{y} & 
\end{pmatrix} = \begin{pmatrix}
\phi_1 & \dots &  \phi_{N+1} & 0 & \dots & 0 &  \\
0 & \dots & 0 & \phi_1 & \dots & \phi_{N+1} & 
\end{pmatrix} \begin{pmatrix} y_{x1} & \\
\vdots & \\
y_{xN+1} & \\
y_{y1} & \\
\vdots & \\
y_{yN+1} & 
\end{pmatrix} = \underline{\underline{\mathbb{N}}} \cdot \underline{\mathbb{U}}_h 
```
To discretize the linearized strain tensor $\underline{\underline{\varepsilon}} $ we can note that: 
```math
\hat{\underline{\varepsilon}}(\underline{y}) = \begin{pmatrix} \varepsilon_{xx} \\
\varepsilon_{yy} & \\
2\varepsilon_{xy} & 
\end{pmatrix}  = \begin{pmatrix} 
\partial_x y_x & \\
\partial_y y_y & \\
\partial_y y_x + \partial_x y_y &
\end{pmatrix} =
\begin{pmatrix} 
1 & 0 & 0 & 0 & \\
0 & 0 & 0 & 1 & \\
0 & 1 & 1 & 0 & 
\end{pmatrix} \begin{pmatrix} 
\partial_x y_x & \\
\partial_y y_x & \\
\partial_x y_y & \\
\partial_y y_y & \\
\end{pmatrix}
```
We can then apply the spatial discretization in a similar fashion using the derivatives of the shape functions:
```math
\hat{\underline{\varepsilon}}(\underline{y}) = \begin{pmatrix} \varepsilon_{xx} \\
\varepsilon_{yy} & \\
2\varepsilon_{xy} & 
\end{pmatrix}  = 
\begin{pmatrix} 
1 & 0 & 0 & 0 & \\
0 & 0 & 0 & 1 & \\
0 & 1 & 1 & 0 & 
\end{pmatrix} \begin{pmatrix}
\partial_x \phi_1 & \dots &  \partial_x \phi_{N+1} & 0 & \dots & 0 &  \\
\partial_y \phi_1 & \dots &  \partial_y \phi_{N+1} & 0 & \dots & 0 &  \\
0 & \dots & 0 & \partial_x \phi_1 & \dots & \partial_x \phi_{N+1} & \\
0 & \dots & 0 & \partial_y \phi_1 & \dots & \partial_y \phi_{N+1} & 
\end{pmatrix} \begin{pmatrix} y_{x1} & \\
\vdots & \\
y_{xN+1} & \\
y_{y1} & \\
\vdots & \\
y_{yN+1} & 
\end{pmatrix} = \underline{\underline{\mathbb{B}}} \cdot \underline{\mathbb{U}}_h
```
Plugging these discretized forms into our equilibrium equation gives:
```math
\forall \, \underline{\mathbb{U}}^*_h \in \mathcal{V}_h, \quad \displaystyle \int_{\Omega_0} \rho_0  \underline{\mathbb{U}}^{*T}_h \underline{\underline{\mathbb{N}}} ^T \cdot \underline{\underline{\mathbb{N}}} \cdot \underline{\dot{\mathbb{V}}}_h \, \text{d}\Omega_0 + \int_{\Omega_{0}}^{}  \underline{\mathbb{U}}^{*T}_h \cdot \underline{\underline{\mathbb{B}}}^T \cdot  \underline{\underline{\hat{\text{A}}}} \cdot \underline{\underline{\mathbb{B}}} \cdot \underline{\mathbb{U}}_h \, \textrm{d}\Omega_0
= \int_{\Gamma_0^N} \underline{\mathbb{U}}^{*T} \cdot \underline{\underline{\mathbb{N}}}^T \cdot \underline{g}_0 \, \text{d}S_0
```
where $\underline{\dot{\mathbb{V}}}_h $ is time derivative of the unknown coefficients $\dot{y}_{jk}$ relative to the velocity field (which is itself the time derivative of the unknown weighting coefficients of the displacement field).

This equation can be factorized and simplified as follows:

```math
\forall \, \underline{\mathbb{U}}^*_h \in \mathcal{V}_h, \quad \displaystyle  \underline{\mathbb{U}}^{*T}_h \left[ \int_{\Omega_0} \rho_0  \underline{\underline{\mathbb{N}}} ^T \cdot \underline{\underline{\mathbb{N}}} \, \text{d}\Omega_0 \right] \underline{\dot{\mathbb{V}}}_h + \underline{\mathbb{U}}^{*T}_h \left[ \int_{\Omega_{0}}^{}  \underline{\underline{\mathbb{B}}}^T \cdot  \underline{\underline{\hat{\text{A}}}} \cdot \underline{\underline{\mathbb{B}}} \, \textrm{d}\Omega_0 \right] \underline{\mathbb{U}}_h
= \underline{\mathbb{U}}^{*T}_h \left[ \int_{\Gamma_0^N} \underline{\underline{\mathbb{N}}}^T \cdot \underline{g}_0 \, \text{d}S_0 \right]

```
```math
\underline{\mathbb{U}}^{*T}_h  \cdot \underline{\underline{\mathbb{M}}} \cdot \underline{\dot{\mathbb{V}}}_h +  \underline{\mathbb{U}}^{*T}_h \cdot \underline{\underline{\mathbb{K}}} \cdot \underline{\mathbb{U}}_h = \underline{\mathbb{U}}^{*T}_h \cdot \underline{\mathbb{F}}
```
Finally, we have:
```math
\underline{\underline{\mathbb{M}}} \cdot \underline{\dot{\mathbb{V}}}_h + \underline{\underline{\mathbb{K}}} \cdot \underline{\mathbb{U}}_h  = \underline{\mathbb{F}} 
```
where $ \underline{\underline{\mathbb{M}}} $ corresponds to the <strong>mass matrix</strong>, $ \underline{\underline{\mathbb{K}}} $  to the <strong>stiffness matrix</strong> and $ \underline{\mathbb{F}} $ is the discretized <strong> right-hand side</strong> (corresponding to a surfacic load). Each of these 3 quantities correspond to different operators that will be assembled in the <strong>variational formulation</strong> later in this tutorial. 

## Time discretization
Here the only time dependency for our elastic problem is the term associated to the inertia, involving the acceleration field $ \ddot{\underline{y}} $:

```math
\int_{\Omega _{0}}^{} \rho_0 \, \underline{y}^{*} \cdot  \underline{\ddot{y}} \, \textrm{d}\Omega_0
```
This means that in order to solve our system, we just need to update the acceleration values (no need to solve a linear system) with a selected time scheme , once we have the static solution. In this demo, we will implement the Newmark time scheme, which reads:
```math
\frac{\dot{\underline{y}}^{n+1} + \dot{\underline{y}}^n}{2} = \frac{\underline{y}^{n+1} - \underline{y}^n}{\Delta t}
```

# Creating the new project

## Prerequisites

The prerequisites are:


- [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) is properly installed.

Make sure the command to install it (`make install` or `ninja install`) has been properly run; you should have generated somewhere (given by the `--install_directory` flag you used to compile MoReFEM) a directory named `MoReFEM` which contains several subdirectories (`bin`, `cmake`, `include`, `lib`, `share` at the time of this writing).

In the following we will assume the environment variable `MOREFEM_INSTALL_PREFIX` points to this directory (you may skip the environment variable and provide the path whenever required of course).

- _(optional)_ `ninja` is installed. If not, remove the `-G Ninja` in later commands and use `make` whenever `ninja` is specified.

We will also assume an environment variable `MOREFEM_MODEL_TUTORIAL` has been defined to point to the directory in which our new project will be created (this is of course entirely optional but is helpful for the sake of this tutorial to avoid periphrasis).


## Creating a (very bare) project

- Create the directory `${MOREFEM_MODEL_TUTORIAL}` for your project wherever you want on your filesystem and place yourself there.

- Copy a basic `CMakeLists.txt`there:

````
 cp ${MOREFEM_INSTALL_PREFIX}/cmake/ExternalModelCMakeLists.txt CMakeLists.txt
````

Edit this file to fulfill the field `project()`, e.g. with `project(TutorialModel)` (the important is to choose a name without spaces or unsavory special characters). 

This file won't be modified more.

- Create the skeleton files for the build:

````bash
mkdir Sources
touch Sources/CMakeLists.txt
````

- Build the very bare project:

````bash
mkdir build && cd build
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Ninja"
````

You should get a line indicating build files have been written; `ninja` command should work and inform you there is no work to do.

It's now time to issue our very first commit:

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
echo build > .gitignore
git init
git add * .gitignore
git commit -m "Initial commit."
````


## Creating a library

Let's edit and see the structure to use to define our compilation chain.

First we need to compile all the local files (save the ones including a `main` function) we will create during this tutorial into a library.

The reason to use a library is that we might want to reuse the same files for other executables than just running the model - for instance we very often prepare as well a post-processing executable which translates the output data into something that might be fed to Ensight.

So we may start by defining a list of files, typically something like: 

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

````cmake
add_library(ModelTutorial_lib ${LIBRARY_TYPE} "")

target_sources(ModelTutorial_lib
	PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
    	${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
    	${CMAKE_CURRENT_LIST_DIR}/Model.hpp
    	${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
    	${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
    	${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
)

target_link_libraries(ModelTutorial_lib                          
                     $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MAIN_LIBS}>)
````

These commands:

- Create a library with all the listed files (one of which must be a compiled file - i.e. one with `cpp` extension  following MoReFEM coding standards; if not you should use instead the `INTERFACE` syntax - see for instance this [CMakeLists.txt](https://gitlab.inria.fr/MoReFEM/Models/ReactionDiffusion/-/blob/master/Sources/CMakeLists.txt) to see how it should be used). This library may be static or dynamic, choosing the same convention as the one used to build MoReFEM library.
- Link this library to the main MoReFEM library. Please keep the `LINK_LIBRARY:WHOLE_ARCHIVE` command; it is required to make the linking work in all build configurations (static or dynamic library, in macOS or in Linux).


One this is done, the executable may be created with:

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

````cmake
add_executable(ModelTutorial ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(ModelTutorial
                      ModelTutorial_lib)
````

At this point all the machinery is ready to build our Model...

## Main program

Let's first create in _Sources_ directory the `main.cpp` that will be used to create the executable.

````c++
#include "Model/Main/Main.hpp"
#include "Model.hpp"

int main(int argc, char** argv)
{
    return MoReFEM::ModelNS::Main<MoReFEM::ModelTutorialNS::Model>(argc, argv);
}
````

Few explanations:

- First include is found in `${MOREFEM_INSTALL_PREFIX}/include` directory.
- Second include is for the Model we will write and is expected to be located in _Sources_ directory directly.
Feel free to place it elsewhere, knowing that the convention is that you headers must be located relatively to either your `Sources` directory or `${MOREFEM_INSTALL_PREFIX}/include`.[^header_ambiguity]

[^header_ambiguity]:Make sure when creating a file locally there is no ambiguity with a file from the main library (e.g. don't create a file named `Model.hpp` locally in a directory named `Model` as _${MOREFEM_INSTALL_PREFIX}/include/Model/Model.hpp_ does already exist). 


The second one expected doesn't exist yet; let's create it.


# Skeleton versions of the required files

We may copy from the installed MoReFEM skeleton versions of the files defined previously in the CMake; we will have a closer look at each of them shortly.

The files that are copied here are from a facility used in XCode when a new file is to be generated, but it can be easily repurported here when writing a new model (you may see it as a cookie-cutter).

<p><strong><font color="red">In <i>Sources</i> directory:</font></strong></p>


````
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/ModelSettings.xctemplate/___FILEBASENAME___.cpp ModelSettings.cpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/InputData.xctemplate/___FILEBASENAME___.hpp InputData.hpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/VariationalFormulation.xctemplate/___FILEBASENAME___.cpp VariationalFormulation.cpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/VariationalFormulation.xctemplate/___FILEBASENAME___.hpp VariationalFormulation.hpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/VariationalFormulation.xctemplate/___FILEBASENAME___.hxx VariationalFormulation.hxx
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/Model.xctemplate/___FILEBASENAME___.cpp Model.cpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/Model.xctemplate/___FILEBASENAME___.hpp Model.hpp
cp ${MOREFEM_INSTALL_PREFIX}/cmake/XCodeTemplates/Model.xctemplate/___FILEBASENAME___.hxx Model.hxx
````

XCode makes several text substitutions when creating a new file; we can mimic it rather easily with Unix `sed` command (or `gsed` for macOS [^gsed]):

[^gsed]: Or `gsed`if you're using macOS: the `-i` option doesn't work with the native `sed` command. `gsed` may be installed through Homebrew with `brew install gnu-sed`.


````
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+Model+g' -e 's+___FILEBASENAME___+Model+g'    ModelSettings.cpp
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+Model+g' -e 's+___FILEBASENAME___+Model+g'    Model.cpp
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+Model+g' -e 's+___FILEBASENAME___+Model+g' -e 's+___PROJECTNAMEASIDENTIFIER___+MODEL_TUTORIAL+g' -e 's+___FILEBASENAMEASIDENTIFIER___+MODEL+g' Model.hpp
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+Model+g' -e 's+___FILEBASENAME___+Model+g' -e 's+___PROJECTNAMEASIDENTIFIER___+MODEL_TUTORIAL+g' -e 's+___FILEBASENAMEASIDENTIFIER___+MODEL+g' Model.hxx
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+VariationalFormulation+g' -e 's+___FILEBASENAME___+VariationalFormulation+g' VariationalFormulation.cpp
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+VariationalFormulation+g' -e 's+___FILEBASENAME___+VariationalFormulation+g' -e 's+___PROJECTNAMEASIDENTIFIER___+MODEL_TUTORIAL+g' -e 's+___FILEBASENAMEASIDENTIFIER___+VARIATIONAL_FORMULATION+g' VariationalFormulation.hpp
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+VariationalFormulation+g' -e 's+___FILEBASENAME___+VariationalFormulation+g' -e 's+___PROJECTNAMEASIDENTIFIER___+MODEL_TUTORIAL+g' -e 's+___FILEBASENAMEASIDENTIFIER___+VARIATIONAL_FORMULATION+g' VariationalFormulation.hxx
sed -i -e 's+___VARIABLE_pathToFiles:name___/++g' -e 's+___VARIABLE_modelName:name___+ModelTutorial+g' -e 's+/___FILEBASENAME___+InputData+g' -e 's+___FILEBASENAME___+InputData+g' -e 's+___PROJECTNAMEASIDENTIFIER___+MODEL_TUTORIAL+g' -e 's+___FILEBASENAMEASIDENTIFIER___+INPUT_DATA+g' InputData.hpp
````

<p><strong><font color="red">In <i>build</i> directory:</font></strong></p>

````bash
ninja 
````

should compile (possibly with warning about unused variable - we have not defined anything yet!)

Let's commit before 

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add skeleton versions of the files needed to define the model."
````

## _(optional)_ Generate XCode project 


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
echo XCode >> .gitignore
git commit -a -m "Add XCode directory to .gitignore."
mkdir XCode && cd XCode
python ${MOREFEM_INSTALL_PREFIX}/cmake/configure_cmake_external_model.py --morefem_install_dir=${MOREFEM_INSTALL_PREFIX} --cmake_args="-G Xcode"
````

You now get a `TutorialModel.xcodeproj` in this directory, that may be used to edit, compile and run the code.
It is possible to refine the presentation of the file hierarchy inside - this has been done for the generated MoReFEM XCode project - but it would complicate needlessly the CMake files so we won't do it here in the scope of this tutorial.

To be able to really run the model within XCode[^running_within_XCode], you will need to specify the Lua file which would otherwise be given in the command line:

[^running_within_XCode]: Which is also optional - personally I edit the code within XCode but run the models and tests from a terminal most of the time.

<p><strong><font color="red">Within the XCode project:</font></strong></p>

- Either press **Command**, **shift** and **<** simultaneously or through the menu: _Product_ > _Scheme_
- Choose the scheme **ModelTutorial**.
- Click on the new target, and remove in the _Build Phases_ tab all that is in the _Compilation Sources_ block.
- In _Run_ > _Arguments_, add a line:
````bash
-i /path/to/your/project/directory/demo.lua    
````

in _Arguments Passed on Launch_ (of course provide the appropriate path, not the gibberish I provided). This is really what would be provided to the command line in a terminal; it indicates the path of the Lua file to use (that should not exist yet in our case). This Lua file will be used to define all the parameters required by our model.


# Writing the model

Now we can really start dealing with the model we intend to write. First step is to define the data the end user might provide in a Lua file; to do so we have to edit the _InputData.hpp_ file and replace the default generated context by the one required by our model:

## `InputData` and `ModelSettings`

One of the very first step you need to tackle is to identify which data will be modifiable by the end user of the model, through a file (often called _lua file_ as it is written in Lua format to enable for instance interpretation of mathematic functions given in this file).

The object which holds the value possibly provided by the end user to use for a given data is called a _leaf_; many _leaves_ may be grouped together in an entity called a _section_. Typically (we will see that more in depth shortly) to define a finite element space section you need to define several leaves such as the unknowns considered and the shape functions to apply for each of them.

However, more often than not you do not want the end user to meddle with some of the leaves - here letting the user modify the shape functions make sense for most of models but on the other hand you do not want them to modify the unknown to use in a finite element space. To handle properly those cases, there are two distinct objects in MoReFEM:

- An `InputData` object, which is dedicated to handle the leaves that are modifiable by the end user.
- A `ModelSettings` object, which handles the leaves the end user shouldn't be privy to.

Concretely, in a `Model` you define an class which inherits from the base template classes `MoReFEM::InputData` and `MoReFEM::ModelSettings`.

In the files already copied, those steps are already performed; we just have to define all the sections and leaves that must go either in `InputData` or in `ModelSettings`. Doing so entails specifying specific types to be provided to the already defined `input_data_tuple` and `model_settings_tuple` tuples.

There are many such leaves and sections pre-defined within MoReFEM library - they are defined in in `Core/InputData/Instances` directory and it is possible to define your own if need be (see the dedicated paragraph [on this wiki](../Utilities/InputData.md)).


We will now detail the sections and leaves we need to define our current model; the MoReFEM concepts considered will be explained **very** shortly here but you may found more in [the lexicon](https://gitlab.inria.fr/MoReFEM/CoreLibrary/introductiontalks/-/tree/master/Lexicon)[^ticket_1825].

[^ticket_1825]: And soon enough (once [ticket #1825](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1825) is handled) in dedicated wiki entries).


### TimeManager

This is to be defined whenever you need to handle time steps within your code[^time_manager]. We need it for our model, so let's keep it.

It is typically values the end user wants to modify so its place is in `InputData` tuple.

[^time_manager]: This will generate a `transient` block in the lua file, with fields such as `init_time`, `timeStep` or `timeMax`.

### NumberingSubset

Very briefly, numbering subsets are a way to tag dofs together in a cohesive numbering (more in the [lexicon](https://gitlab.inria.fr/MoReFEM/CoreLibrary/introductiontalks/-/tree/master/Lexicon) or soon in [this wiki entry](../Core/NumberingSubset.md))[^ticket_1825].

In our model, we need only one `NumberingSubset` to number the `Dof` related to displacement.

There are only two fields related to an _indexed section_ `NumberingSubset`:

- A description of what is entailed by the section.
- A field to specify whether a vector defined upon this numbering subset will be used to move the mesh between two time steps. Most of the time the value of this fields is left to `false`; that is the case for current model.

None of these fields is meant to be parametrized by the end user; `NumberingSubset` is therefore defined solely in `ModelSettings`.


<p><strong><font color="green">In InputData.hpp</font></strong></p>

We will rename more appropriately the `enum class` used:

````c++
enum class NumberingSubsetIndex : std::size_t
{
     displacement = 1
};
````

and modify accordingly in `model_settings_tuple`:

````c++
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::displacement)
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
// ****** Numbering subset ******
{
    SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
        "Displacement");
    Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>::DoMoveMesh>(false);
}
````

### Unknown

There are three fields in an `Unknown` indexed section:

- A description of what is entailed by the section.
- A name (should probably be dropped in #1828).
- A nature (whether it's a scalar or a vectorial unknown).

None of these leaves is the business of the end user; as for `NumberingSubset` `ModelSettings` should take of it entirely. Please notice it is logical to use the name `displacement` as well but you may choose anything; the same is true for the integer value (I keep the 12 of the example to illustrate it works - we'll see in next section when such a value is used).

<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
    enum class UnknownIndex : std::size_t
    {
        displacement = 12
    };
````

and modify accordingly in `model_settings_tuple`:

````
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::displacement)
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
    // ****** Unknown ******
    {
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>(
            { "Displacement unknown" });

        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>("displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>("vectorial");
    }
````


### Mesh

`Mesh` indexed sections are used to define the characteristics of a mesh. Its leaves are:

- A description of what is entailed by the section.
- The path of the mesh file on the filesystem.
- The format of the mesh file (Ensight6, Medit, ...)
- The highest dimension considered in the mesh
- A multiplicative factor to apply to the lengths read in the original mesh file.

Most of the time, all save the very first of these leaves are left open for the end user to modify and are therefore handled by `InputData`.

As for previous indexed sections, we'll start by defining an enum class:

<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t
    {
        solid = 5
    };
````

In `input_data_tuple`:

````c++
MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::solid)
````

and in `model_settings_tuple`:

````c++
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::solid)
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
    // ****** Mesh ******
    {
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>>(
            { "Mesh representing the solid" });
    }
````

<p><strong><font color="green">In the <i>future</i> Lua file </font></strong></p>

The Lua file doesn't exist yet; we will generate it [a bit later](#DemoLuaFile). 

When it exists, the `InputData` fields will appear with something like:

````lua
    -- Mesh representing the solid
    Mesh5 = {

            -- Path of the mesh file to use. 
            -- IMPORTANT: if run from preprocessed data, this path won't be used (the already cut mesh will be used 
            -- instead). 
            -- Expected format: "VALUE"
            mesh = No default value was provided!,

            -- Format of the input mesh.
            -- Expected format: "VALUE"
            -- Constraint: value_in(v, {'Ensight', 'Medit'})
            format = "Medit",

            -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the 
            -- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an 
            -- exception is thrown. 
            -- Expected format: VALUE
            -- Constraint: v <= 3 and v > 0
            dimension = No default value was provided!,

            -- Space unit of the mesh.
            -- Expected format: VALUE
            space_unit = 1.

    } -- Mesh5
````

Two remarks:

- Notice the name of the section: <i>Mesh<b>5</b></i>. The integer is the underlying value of the enum class `MeshIndex::solid` defined earlier.
- The description given in `ModelSettings::Init()` appears as a comment above the section.



### Domain

`Domain` indexed sections are used to define the characteristics of a geometric domain - i.e. a subset of all the geometric space upon which some specific operations are performed. The leaves mostly provide levers to define properly your domain:

- A description of what is entailed by the section.
- The list of `Mesh` objects (through their index provided here by `MeshIndex` enum class values) that are considered for the domain. This list may be left empty if no mesh restriction.
- The list of dimensions to consider - for instance if `{ 3 }` is given only dimension 3 geometric elements will be considered. If left empty no restriction on dimension.
- The list of `MeshLabel`[^mesh_label]. As for previous leaves no restriction if left empty.
- The list of the kind of geometric elements to be considered - most of the time it is left empty.

[^mesh_label]: MoReFEM name for the _references_ - we chose a different moniker to avoid confusion with what C++ calls a reference).

The two first leaves aren't meant to be handled by a mere user and are therefore handled by the model author; most of the time the other fields depends upon the input mesh and are thus modifiable by the end user.

For the elastic model we will consider 4 differents `Domain`:[^domains]

[^domains]: The use for each of them will become apparent as we write the model.

  - One which encompasses the whole mesh with nothing left aside. This corresponds to $\Omega_0$.
  - One used to define the area upon which the Neumann boundary condition is applied. This corresponds to $\Gamma^N$.
  - One used to define the area upon which Dirichlet boundary condition is applied. This corresponds to $\Gamma^D$.
  - One which will encompass all the `GeometricElt` of the mesh that share the highest possible dimension. This one is required to define the proper finite element space (see below). This corresponds to $ \displaystyle \Omega_0 \setminus \{ \Gamma^D \cup \Gamma^N \}$.



<p><strong><font color="green">In InputData.hpp</font></strong></p>
 
````c++
enum class DomainIndex : std::size_t { highest_dimension = 1, neumann = 2, dirichlet, full_mesh = 10 };
````

In `input_data_tuple`:

````c++
MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),
````

and in `model_settings_tuple`:

````c++
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),
````



<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>


````c++
// ****** Domain ******
{
    SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
        { "Highest dimension geometric elements" });
    SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>>(
        { "Domain upon which Neumann boundary condition is applied" });
    SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
        { "Domain upon which Dirichlet boundary condition is applied" });
    SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ "Full mesh" });

    Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
        { EnumUnderlyingType(MeshIndex::solid) });
    Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>::MeshIndexList>(
        { EnumUnderlyingType(MeshIndex::solid) });
    Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
        { EnumUnderlyingType(MeshIndex::solid) });
    Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
        { EnumUnderlyingType(MeshIndex::solid) });
}
````

As you can see, all 4 domains are defined over the mesh and the mesh index is used to indicate this in the `MeshIndexList` objects.



### FEltSpace

`FEltSpace` indexed sections entails many leaves:

- A description of what is entailed by the section.
- The `Domain` upon which it is defined. This domain *must* be defined only exactly one dimension - you can't put in a same finite element space finite elements of different dimensions.
- The list of `Unknown`s covered by the finite element space; a given `Unknown` may be present at most once.
- The shape function to use for each unknown (this list must be the same length as the previous one).
- The numbering subset for each of the unknown (this list must be the same length as the previous one).

Among all these leaves, only the one related to the shape function to use is to be left open for the model user in most cases.

For our simple model, we will need two different finite element spaces:

- One covering the `Dofs`of the whole mesh
- One for the definition of the Neumann operator.

<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, neumann = 2 };
````

In `input_data_tuple`:

````c++
    MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
    MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann)
````

and in `model_settings_tuple`:

````c++
    MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
    MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann)
````



<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>
 
````c++
// ****** Finite element space ******
{
    SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
        "Finite element space for highest geometric dimension");
    SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>>(
        { "Finite element space for Neumann boundary condition" });

    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(
        EnumUnderlyingType(MeshIndex::solid));
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
        EnumUnderlyingType(DomainIndex::highest_dimension));
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
        { "displacement" });
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>(
        { EnumUnderlyingType(NumberingSubsetIndex::displacement) });

    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::GodOfDofIndex>(
        EnumUnderlyingType(MeshIndex::solid));
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::DomainIndex>(
        EnumUnderlyingType(DomainIndex::neumann));
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::UnknownList>(
        { "displacement" });
    Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::NumberingSubsetList>(
        { EnumUnderlyingType(NumberingSubsetIndex::displacement) });
}
````




### DirichletBoundaryCondition

A `DirichletBoundaryCondition` encompasses the following leaves:

- A description of what is entailed by the section.
- Upon which spatial omponent(s) the condition is applied.
- The unknown considered.
- The value to set for each of the entailed component.
- The domain in which the condition is applied.
- Whether the boundary condition may change at each time step.

The only leaves we need to let accessible to the end user are the components upon which the condition is applied and the value to apply, and to be honest for this model it is debatable.[^bc_component]

[^bc_component]: The only reason we let it this way is that we want to tackle both 2D and 3D models and currently we have no way to say "apply 0 to all components", but that could be implemented rather easily in the library.


<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
    enum class BoundaryConditionIndex : std::size_t { sole };
````

In `input_data_tuple`:

````c++
   MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::sole)
````

and in `model_settings_tuple`:

````c++
    MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::sole)
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
// ****** Dirichlet boundary condition ******
{
    SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>>(
        "Boundary condition");

    Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::UnknownName>(
        "displacement");
    Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::DomainIndex>(
        EnumUnderlyingType(DomainIndex::highest_dimension));
    Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>::IsMutable>(
        false);
}
````

### Solid

Our model requires several `Parameter` related to the physical properties of the solid.

For a given `Parameter`, there are two leaves to be addressed: its nature (whether it's constant, piecewise constant by domain, given by a Lua function, ...) and its value (which type depends on the nature).

For most of the parameters, we want to let this choice open for the end user.

The only exception concerns the volumic mass: we write the model with the hypothesis that it is constant, thus enabling  a massive simplification of the dynamic steps.

For most of the parameters, we will just put the name of the section in input data:

<p><strong><font color="green">In InputData.hpp</font></strong></p>

In `input_data_tuple`:

````c++
InputDataNS::Solid::YoungModulus,
InputDataNS::Solid::PoissonRatio,
InputDataNS::Solid::PlaneStressStrain,
````

For `VolumicMass`, we want to split it so we will specify the leaves explicitly:

In `input_data_tuple`:

````c++
InputDataNS::Solid::VolumicMass::Value
````

and in `model_settings_tuple`: 

````c++
InputDataNS::Solid::VolumicMass::Nature
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
    // ****** Volumic mass ******
    Add<InputDataNS::Solid::VolumicMass::Nature>("constant");
````


### Solver

`Petsc` is an indexed section used to describe parameters related to the solver.

All the related leaves (save the one for the description) are used to set up numerical values to use for the solve, such as absolute and relative tolerance, maximum number of iterations and so forth...

In most cases, it is best to let the model user play with these; only the description is in `ModelSettings`.

For our current model, we need only one solver, which is used only in the static phase.
 
<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
    enum class SolverIndex : std::size_t { solver };
````

In `input_data_tuple`:

````c++
   MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver)
````

and in `model_settings_tuple`:

````c++
    MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver)
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
// ****** PETSc solver ******
SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({  "Solver used in the static phase of the model" });
````


### VectorialTransientSource

This indexed section is used to represent the force that is applied to the bar.

The parameters are:

- A description of what is entailed by the section.
- Whether the force is a constant value, a Lua function, is piewise constant by domain, etc...
- The values for each of the component.

<p><strong><font color="green">In InputData.hpp</font></strong></p>

````c++
    //! \copydoc doxygen_hide_source_enum
    enum class SourceIndex : std::size_t { surfacic };
````

In `input_data_tuple`:

````c++
   MOST_USUAL_INPUT_DATA_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::surfacic)
````

and in `model_settings_tuple`:

````c++
    MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::surfacic)    
````

<p><strong><font color="green">In ModelSettings.cpp, within <i>ModelSettings::Init()</i> </font></strong></p>

````c++
// ****** Force ******
SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>>(
            { "Surfacic transient source" });
````


### Result

To generate the block which tells where to write outputs; keep the default values.

The code should compile:

<p><strong><font color="red">In <i>build</i> directory:</font></strong></p>

````bash
ninja 
````

so a commit is in order:

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources/InputData.hpp Sources/ModelSettings.cpp
git commit -m "Fill InputData.hpp and ModelSettings.cpp with the data relevant for the model at hand."
````

<div id="DemoLuaFile"></div>

## The demo.lua file

### First run of the executable

Let's run the code:[^xcode_run]

[^xcode_run]: You may of course run it within the XCode project if you're a macOS user - to do so just press **Cmd** + **R** which will build the code if needed and run it.

<p><strong><font color="red">In <i>build</i> directory:</font></strong></p>

````bash
Sources/ModelTutorial -i ${MOREFEM_MODEL_TUTORIAL}/Sources/demo.lua 
````



You should get something like message:

````
/Users/sgilles/Codes/MoReFEM/Models/Tutorial/Sources/demo.lua wasn't existing and has just been created on root processor; please edit it and then copy it onto all machines intended to run the code in parallel.
````

This is normal: the path for the Lua file was valid but the file didn't exist yet; the choice was made in this case to create a Lua file with the blocks mentioned in the `InputData.hpp` file and default value written when possible.

This is not the case all the time, so you really need to edit this file and specify your input data.

To be able to see the file within your IDE generated project, you will need to add it either to the library or in the executable, e.g.:

<p><strong><font color="green"><i>(optional) </i>In Sources/CMakeLists.txt:</font></strong></p>

````cmake
add_executable(ModelTutorial
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp
               ${CMAKE_CURRENT_LIST_DIR}/demo.lua
               )
````

Rerunning the generation of the XCode project will add it properly.


### Filling the Lua file

__WARNING__: This file must respect Lua syntax; you must put a **,** at the end of each line you fill (save for the last line in a block for which it is not mandatory - but there is no harm putting it nonetheless).

For each field, check the **Expected format** in the comment, which provides the way the entry should be provided.

If not specified otherwise, I indicate here only the fields that are to be changed; if a field appears in the Lua file that is not mentioned in the following, do not modify or remove it.

The file has been created and provide default values when some makes sense, or a sentence telling to complete fhe field if no default value would be adequate.


#### Transient block

This block sets the time interval over which the model is run. Choose `timeMax = 0.5`  for instance.


#### Mesh1

Let's choose a toy 2D mesh present in the library:

````lua
mesh = "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh",
dimension = 2
````

where `MOREFEM_ROOT` is an environment variable set to the location of the directory in which MoReFEM library is cloned. If you do not want to use an environment variable you may hardcode the path here.

#### Domains
The domains we define are just a geometrical description of our mesh, they describe how the mesh is partionned. This is required so that we can apply the different operators on the relevant domains. 


##### Domain1

For the `Domain` which index is `DomainIndex::highest_dimension` (that covers $ \displaystyle \Omega_0 \setminus \{ \Gamma^D \cup \Gamma^N \}$):

````lua
dimension_list = { 2 }, -- only elements with dimension = 2
mesh_label_list = { }, -- no constraint upon MeshLabel
geometric_element_type_list = { } -- no constraint upon geometric element type
````

##### Domain2

For the `Domain` which index is `DomainIndex::neumann` (that covers $\Gamma^N$):


````lua
dimension_list = { 1 }, -- only elements with dimension = 1
mesh_label_list = { 2 }, -- consider only edges with this reference (called MeshLabel to avoid confusion with C++ namesake)
geometric_element_type_list = { } -- no constraint upon geometric element type
````

##### Domain3

For the `Domain` which index is `DomainIndex::dirichlet` (that covers $\Gamma^D$):

````lua
dimension_list = { }, -- no constraint upon dimension
mesh_label_list = { 1 }, -- consider only geometric elements with this mesh label
geometric_element_type_list = { } -- no constraint upon geometric element type
````

##### Domain10

Fourth domain is the whole mesh with nothing left aside. This corresponds to $\Omega_0$.

````lua
dimension_list = { }, -- no constraint upon dimension
mesh_label_list = { }, -- no constraint upon MeshLabel
geometric_element_type_list = { } -- no constraint upon geometric element type
````

#### FiniteElementSpaces

##### FiniteElementSpace1

The finite element space upon which the elastic operator is defined. 

Please notice the comment just above that says:

````lua
-- unknown_list: { 'displacement' }
````

It tells which unknowns are considered and in which order. You must provide here the list of shape functions to use in the same order (of course here with only one unknown there are no risk of mapping incorrectly shape functions to their unknowns...)


````lua
shape_function_list = { "P1b" }, -- the moniker for the type of shape function you want. Putting a stupid value will result in an error message that will provides you all valid values. 
````

##### FiniteElementSpace2

The finite element space upon which the Neumann condition is defined. The Neumann condition itself is a specific variational term added to the right-hand side of our system, which is defined below as a `TransientSource_2`.

````lua
shape_function_list = { "P1" }, -- not 'P1b': we're talking about segments here.
````

#### EssentialBoundaryCondition0 

This corresponds to $ \underline{y}(\underline{x}) = \underline{0} \quad \text{on} \quad \Gamma^D$. 

To apply this boundary condition we modify the stiffness matrix of our system (either by pseudo-elimination or penalization) on the relevant degrees of freedom (those encompassed by Domain3 here).

````lua
component = "Comp12", -- means a value provided for X and another for Y
value = { 0., 0. }, -- the values for X and Y constrained by the Dirichlet boundary condition
````

#### Solid 

##### Volumic mass $ \rho_0 $ 

````lua
nature = "constant",
value = 1.3
````

##### Young modulus $E$

````lua
nature = "lua_function", -- for the demo: we'll just provide a constant function...
value = [[
        function (x, y, z)
        return 8307692.02366862
        end
        ]]
````

##### Poisson ratio $\nu$

````lua
nature = "constant",
value = 0.0384615029585771
````

#### Transient source. 
This corresponds to  $ \displaystyle \underline{g}_0(\underline{x}) \quad \text{in} \quad \int_{\Gamma_0^N} \underline{g}_0 \cdot \underline{y}^* \, \text{d}S_0 $ 


````lua
nature = { "constant", "constant", "constant" },
value = { 0., 5.e-3, 0. }
````

#### Result

````lua
output_directory = "${MOREFEM_RESULT_DIR}/ModelTutorial",
````

## Running again

With this file, the code should run smoothly and exit with exit code 0; an exception may be thrown if:

- The Lua file is not properly formed (typically a missing ',' or all values were not properly filled)
- An expected field (from the `InputData` tuple) was not found.
- An unexpected field was found (typically when something has been removed from the tuple but the Lua file has not yet been updated)
- A constraint for a given input datum is not fulfilled.
- An environment variable used in one of the leaves is not defined (see below)

<p><strong><font color="red">In <i>build</i> directory:</font></strong></p>

````bash
export MOREFEM_RESULT_DIR=/tmp/ModelTutorial
Sources/ModelTutorial -i ${MOREFEM_MODEL_TUTORIAL}/Sources/demo.lua --overwrite_directory
````

(the `--overwrite_directory`tells the program to remove the content of your output directory if it already exists, typically from a previous run.)

You may of course choose whatever path you want as `MOREFEM_RESULT_DIR` environment variable[^env_variable_XCode] (and `MOREFEM_ROOT` needs to be defined as well if you used it for the mesh path).

[^env_variable_XCode]: If you're running from XCode, make sure to fill the field "EnvironmentVariables" in the "Run" frame of the model scheme.

If the program run smoothly:

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources/CMakeLists.txt Sources/demo.lua
git commit -m "Add demo.lua file."
````


## Add variational_formulation object in the Model

`VariationalFormulation` is currently not connected in any way to the `Model`; in fact you may completely define a `Model` without any such class, even if it is handy in most models to use one.

### Modifications in the code

So we'll start effectively writing the model by setting up the variational formulation:

<p><strong><font color="green">In Model.hpp:</font></strong></p>

We begin by the _hpp_ file, which provides the **class declaration**. We will just add by composition a `VariationalFormulation` object and define the accessors:

At the top of Model.hpp with others include:

````c++
# include "VariationalFormulation.hpp"
````


At the bottom of the class declaration:

````c++
private:

//! Non constant access to the underlying VariationalFormulation object.
VariationalFormulation& GetNonCstVariationalFormulation() noexcept;

//! Access to the underlying VariationalFormulation object.
const VariationalFormulation& GetVariationalFormulation() const noexcept;


private:

//! Underlying variational formulation.
VariationalFormulation::unique_ptr variational_formulation_ = nullptr;

````


<p><strong><font color="green">In Model.hxx:</font></strong></p>

The _hxx_ file is designed to put the **definitions** of template and inline functions and methods. It is included at the end of its `hpp` counterpart; a user of the library or of your model should **never** have to include themselves an `hxx` file in their program.


````c++
inline const VariationalFormulation& Model::GetVariationalFormulation() const noexcept
{
    assert(!(!variational_formulation_));
    return *variational_formulation_;
}


inline VariationalFormulation& Model::GetNonCstVariationalFormulation() noexcept
{
    return const_cast<VariationalFormulation&>(GetVariationalFormulation());
}
````

<p><strong><font color="green">In Model.cpp, fill SupplInitialize():</font></strong></p>

The `cpp` file provides the definitions for the compiled methods and functions.


````c++
void Model::SupplInitialize()
{
   // The `GodOfDof` has already been properly created automagically by the parent class:
   // `SupplInit()` is called at the end of `Init()`, which on its own has already set up
   // lots of internal stuff.
   // MeshIndex::solid is an enum value as we defined in the InputData.hpp.
   // The `AsMeshId()` here is to transform this enum class value into a `MeshNS::unique_id`
   // object expected by `GetGodOfDof()` method (this might seem needlessly complicated at
   // first sight but it adds lot of safety: you can't use an inadequate index by mistake
   // this way).
   decltype(auto) god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::solid));
   
   decltype(auto) morefem_data = parent::GetMoReFEMData();
   decltype(auto) time_manager = parent::GetNonCstTimeManager();

   {
       // We need the boundary condition inside the `VariationalFormulation`.
       decltype(auto) bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);
       auto bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::sole), __FILE__, __LINE__) };
       
       variational_formulation_ =
           std::make_unique<VariationalFormulation>(morefem_data,
                                                    god_of_dof,
                                                    std::move(bc_list),
                                                    time_manager);
   }

   // Make sure the newly minted `VariationalFormulation` object is properly set up
   // (if you forget this step you would get an error when running the program in debug mode).
   decltype(auto) variational_formulation = GetNonCstVariationalFormulation();
   variational_formulation.Init(morefem_data);
}
````

### C++ explanations

#### File structure

To put in a nutshell:
- hpp file deals declarations
- hxx file deals with definitions of inline and template methods and functions
- cpp file deals with other definitions.

#### StrongType

The `AsMeshId` or `AsBoundaryConditionId` in the code below may seem tricky but they are in fact just syntactic sugar:

- MoReFEM uses `StrongType` (see [this post if you want to learn more about it](https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/)) to limit the risk of mistakes by the developer. Very briefly, the idea is that if you use directly integer - say `int` - to store all types of indexes, you may not see you're doing something wrong if you provide an index representing a `Mesh` to a function expecting a `Dof`[^object_and_strong_type]. StrongType is a neat way to prevent that: you use an enriched `int` which is not exactly the same so the compiler may tell you you're doing something wrong. The cherry on the top is that in release mode at least there is no additional runtime cost incurred.
- `AsMeshId` is just a facility to convert an `enum class` (that you are strongly encouraged to use when defining your models) into the adequate `StrongType`.
- `Mesh` and `GodOfDof` are so closely related that they share the same type of id; that's why `AsMeshId` is used when `GodOfDof` is fetched.


[^object_and_strong_type]: In MoReFEM the favored approach is to use objects whenever possible: if a function expects a `Dof` argument providing a `Mesh` argument will not compile at all. StrongTypes is the next best thing when it's not possible to circumvemt entirely the use of an integer index.
 
 #### decltype(auto)

`auto`, introduced in C++ 11, deduces statically (i.e. at compile time) the type of a local variable, but it loses up qualifiers such as `const` or `&`. `decltype(auto)` (introduced in C++ 14) keeps them.

Concretely here:

- In parent class `MoReFEM::Model`, `GetMoReFEMData()` is defined with signature

````c++
const morefem_data_type& GetMoReFEMData() const noexcept;
````

- If we call

````c++
auto morefem_data = parent::GetMoReFEMData();
````

as `const` and `&` are dropped it will be like:

````c++
morefem_data_type morefem_data = parent::GetMoReFEMData();
````

which won't compile as is incurs a copy and `morefem_data_type` objects are not copyable. 

So to make it work as intended (just get a reference to the object) you must choose one of the three syntaxes:

````c++
decltype(auto) morefem_data = parent::GetMoReFEMData(); // C++ 14 onward
const auto& morefem_data = parent::GetMoReFEMData(); // C++ 11 onward
const morefem_data_type& morefem_data = parent::GetMoReFEMData(); // any C++ standard, but potentially much more verbose
````


### Commit


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add variational formulation in the model." 
````

## Variational formulation: defining the Parameters

The MoReFEM library automatically loads the Lua file in the initialization process, but it doesn't mean all the content is instantly interpreted as directly usable MoReFEM objects. Some are (for instance the `Mesh`is fully built without further ado) but others like `Parameter` objects need to be built explicitly.

To begin with, by definition a `Parameter` is an object which purpose is to be evaluated at geometric coordinates; there are two flavours of such coordinates:
- Coordinates on the mesh (`Coords`)
- Coordinates in the reference element (`LocalCoords`)

<p><strong><font color="green">In VariationalFormulation.hpp, in the class declaration:</font></strong></p>

At the top of the class, with `using self`, `using parent`, etc...:

````c++
//! Alias to the type of the source Parameter.
using source_parameter_type = Parameter<ParameterNS::Type::vector, LocalCoords>;
````

At the bottom of the class:

````c++
private:

//! Volumic mass.
const ScalarParameter<>& GetVolumicMass() const noexcept;

//! Young modulus.
const ScalarParameter<>& GetYoungModulus() const noexcept;

//! Poisson ratio.
const ScalarParameter<>& GetPoissonRatio() const noexcept;

//! Source parameter.
const source_parameter_type& GetSourceParameter() const noexcept;


private:

//! Volumic mass.
ScalarParameter<>::unique_ptr volumic_mass_ = nullptr;

//! Young modulus.
ScalarParameter<>::unique_ptr young_modulus_ = nullptr;

//! Poisson ratio.
ScalarParameter<>::unique_ptr poisson_ratio_ = nullptr;

//! Source parameter.
typename source_parameter_type::unique_ptr source_parameter_ = nullptr;
````

<p><strong><font color="green">In VariationalFormulation.hxx:</font></strong></p>

````c++
inline const ScalarParameter<>& VariationalFormulation
::GetVolumicMass() const noexcept
{
assert(!(!volumic_mass_));
return *volumic_mass_;
}


inline const ScalarParameter<>& VariationalFormulation
::GetYoungModulus() const noexcept
{
assert(!(!young_modulus_));
return *young_modulus_;
}


inline const ScalarParameter<>& VariationalFormulation
::GetPoissonRatio() const noexcept
{
assert(!(!poisson_ratio_));
return *poisson_ratio_;
}


inline const VariationalFormulation::source_parameter_type& VariationalFormulation
::GetSourceParameter() const noexcept
{
assert(!(!source_parameter_));
return *source_parameter_;
}
````


<p><strong><font color="green">In VariationalFormulation.cpp, fill SupplInit():</font></strong></p>


````c++
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
{
    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

    decltype(auto) full_mesh_domain =
        domain_manager.GetDomain(AsDomainId(DomainIndex::full_mesh), __FILE__, __LINE__);

    volumic_mass_ =
        InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass",
                                                                          full_mesh_domain,
                                                                          morefem_data);

    if (!GetVolumicMass().IsConstant())
        throw Exception("Current elastic model is restricted to a constant volumic mass!", __FILE__, __LINE__);

    young_modulus_ =
        InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>("Young modulus",
                                                                            full_mesh_domain,
                                                                           morefem_data);

    poisson_ratio_ =
        InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>("Poisson ratio",
                                                                           full_mesh_domain,
                                                                           morefem_data);

    decltype(auto) neumann_domain =
        domain_manager.GetDomain(AsDomainId(DomainIndex::neumann), __FILE__, __LINE__);

    source_parameter_ =
        Init3DCompoundParameterFromInputData
        <
            InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::surfacic)>
        >("Surfacic source",
          neumann_domain,
          morefem_data);
}
````


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

(after checking it compiles and runs of course...):

````bash
git add Sources
git commit -m "Parameters properly added to the variational formulation."
````

## Add method to run the static case

Let's provide a method in variational formulation to run the static case in the initialization; at the moment we will let it empty. This method will be used to solve the initial static problem $\displaystyle \underline{\underline{\mathbb{K}}} \cdot \underline{\mathbb{U}}_h  = \underline{\mathbb{F}}$ .

<p><strong><font color="green">In VariationalFormulation.hpp, in the class:</font></strong></p>

````c++
public:

//! Run the static case.
void RunStaticCase();
````
<p><strong><font color="green">In VariationalFormulation.cpp, in the class:</font></strong></p>

````c++
void VariationalFormulation::RunStaticCase()
{ }
````

(we will add the implementation shortly)


<p><strong><font color="green">In Model.cpp:</font></strong></p>

````c++
void Model::SupplInitialize()
{
    ... // (already existing content)
    
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));
    variational_formulation.RunStaticCase();
    variational_formulation.WriteSolution(time_manager, numbering_subset);
}
````





## VariationalFormulation: allocating the system matrices and vectors

If you try to run the code (in debug mode of course as you're developing it!), you will have an issue (materialized by an `assert` in debug mode): the `WriteSolution()` method expects that the system solution is properly allocated, and this has to be done explicitly (because in complex models with several numbering subsets there is often no need to build all the possible configuration of matrices and vectors).

<p><strong><font color="green">In VariationalFormulation.cpp, fill AllocateMatricesAndVectors():</font></strong></p>

````c++
void VariationalFormulation::AllocateMatricesAndVectors()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));
        
    parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
    parent::AllocateSystemVector(numbering_subset);
}
````

The code should now run properly.


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add the method in charge of running the static case (without its implementation for the time being) and allocate properly the linear algebra required for writing the output files."
````

## VariationalFormulation: define both operators ($\underline{\underline{\mathbb{K}}}$ and $\underline{\mathbb{F}}$ )  required for the static case

### Source operator

RHS for the system is just the surfacic source; we therefore need to define the related operator. This corresponds to the surfacic loading vector $\displaystyle \underline{\mathbb{F}} = \int_{\Gamma_0^N} \underline{\underline{\mathbb{N}}}^T \cdot \underline{g}_0 \, \text{d}S_0$:

<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
# include "OperatorInstances/VariationalOperator/LinearForm/TransientSource.hpp"
````

In the aliases at the top of the class:

````c++
private:

//! Alias to the type of the source operator.
using source_type_operator = GlobalVariationalOperatorNS::TransientSource<ParameterNS::Type::vector>;
````

And within the class:


````c++
private:

//! Surfacic source operator.
const source_type_operator& GetSurfacicSourceOperator() const noexcept;

private:

//! Surfacic source operator.
source_type_operator::const_unique_ptr surfacic_source_operator_ = nullptr;
````

<p><strong><font color="green">In VariationalFormulation.hxx</font></strong></p>

````c++
inline const VariationalFormulation::source_type_operator& VariationalFormulation
::GetSurfacicSourceOperator() const noexcept
{
    assert(!(!surfacic_source_operator_));
    return *surfacic_source_operator_;
}
````

I also usually define a method `DefineOperators`:

<p><strong><font color="green">In VariationalFormulation.hpp (argument will be used a bit later):</font></strong></p> 


````c++
private:

   /*!
    * \brief Define the properties of all the global variational operators involved.
    *
    * \copydoc doxygen_hide_morefem_data_arg
    */
    void DefineOperators(const morefem_data_type& morefem_data);
````

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

- Add a new line at the end of SupplInit:

````c++
void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
{
    ... 
    DefineOperators(morefem_data);
}
````

- And provide the implementation for the new method:

````c++
void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

    decltype(auto) displacement_ptr =
        unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));

    {
        decltype(auto) felt_space_neumann =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::neumann));

        decltype(auto) source_parameter = GetSourceParameter();

        surfacic_source_operator_ =
            std::make_unique<source_type_operator>(felt_space_neumann,
                                                   displacement_ptr,
                                                   source_parameter);
    }
}
````

This might seem a bit wordy, but there is not much here:

- We want to construct a `TransientSource` object (in the `hpp` source_type_operator is an alias to the exact type with the proper template specialization).
- Constructor of this object requires three arguments:
    * The finite element space upon which the operator should be defined.
    * The `Parameter` which holds the value to apply there.
    * The `Unknown` consider.
- The code is to fetch all there elements and build the `surfacic_source_operator_` object.

A quick note concerning `UnknownManager`: we use in MoReFEM a few singletons to hold data that may be used everywhere across the model. `UnknownManager` is one of them and stores the knowledge about every `Unknown` that is defined ina given `Model`.


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add the source operator."
````



### Stiffness operator

We also need to define the stiffness matrix. This corresponds to $\displaystyle \underline{\underline{\mathbb{K}}} = \int_{\Omega_{0}}^{}  \underline{\underline{\mathbb{B}}}^T \cdot  \underline{\underline{\hat{\text{A}}}} \cdot \underline{\underline{\mathbb{B}}} \, \textrm{d}\Omega_0$
<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
# include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
````

````c++
private:

    //! Get the stiffness operator.
    const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor& GetStiffnessOperator() const noexcept;

private:

    //! Stiffness operator.
    GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor::const_unique_ptr
        stiffness_operator_ = nullptr;
````


<p><strong><font color="green">In VariationalFormulation.hxx</font></strong></p>

````c++
inline const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor&
VariationalFormulation::GetStiffnessOperator() const noexcept
{
    assert(!(!stiffness_operator_));
    return *stiffness_operator_;
}
````

<p><strong><font color="green">In VariationalFormulation.cpp, complete InitializeOperators():</font></strong></p>

````c++
void VariationalFormulation::DefineOperators(const input_data_type& input_data)
{
    ... 
    {
        decltype(auto) felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        decltype(auto) mesh_dimension = god_of_dof.GetMesh().GetDimension();

        const auto configuration =
            ParameterNS::ReadGradientBasedElasticityTensorConfigurationFromFile(mesh_dimension,
                                                                                morefem_data.GetInputData());

        using op_type = GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor;
        
        stiffness_operator_
            = std::make_unique<op_type>(felt_space_highest_dimension,
                                        displacement_ptr,
                                        displacement_ptr,
                                        GetYoungModulus(),
                                        GetPoissonRatio(),
                                        configuration);
    }
````

This is very similar to what we did for the source operator, except that the constructor expects a bit more arguments.


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add the stiffness operator."
````

## Variational formulation: define a global matrix for stiffness

Additionally to the system matrices, it is convenient to add work matrices that might be reused and thus spare some recomputation and/or memory allocation. For instance it is useful to compute once and for all the stiffness matrix (given we consider in our model a constant volumic mass and time step); so let's add this matrix:

<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
private:

    //! Accessor to the stiffness matrix.
    const GlobalMatrix& GetStiffnessMatrix() const noexcept;

    //! Non constant accessor to the stiffness matrix.
    GlobalMatrix& GetNonCstStiffnessMatrix() noexcept;
````      

````c++
private:

    //! Stiffness matrix.
    GlobalMatrix::unique_ptr stiffness_matrix_ = nullptr;
````


<p><strong><font color="green">In VariationalFormulation.hxx</font></strong></p>

````c++
inline const GlobalMatrix& VariationalFormulation
::GetStiffnessMatrix() const noexcept
{
    assert(!(!stiffness_matrix_));
    return *stiffness_matrix_;
}


inline GlobalMatrix& VariationalFormulation
::GetNonCstStiffnessMatrix() noexcept
{
    return const_cast<GlobalMatrix&>(GetStiffnessMatrix());
}
````

I imagine easily this second accessor may rise your eyebrows: `const_cast` is not to be used lightly, especially in the sense of _removing_ a `const` qualifier. 

The present case is a pattern from a well known C++ guru (Herb Sutter) often used within MoReFEM when we need to define two accessors for the same data: one that is `const` and one that is not [^private_non_cst_accessor]. Here the implementation of the `const` version is straightforward (two lines including one for safety check) but it could not be the case - and providing the same implementation twice does not respect the **D**on't **R**epeat **Y**ourself (DRY) principle. So this trick enables you to define the non constant version in terms of the constant one, heeding completely the DRY principle. This is the only pattern for which `const_cast` is used throughout MoReFEM code base.

[^private_non_cst_accessor]: Please notice that 99 % of the time (if not more...) the non constant version should be `protected` or `private`.

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````c++
void VariationalFormulation
::AllocateMatricesAndVectors()
{
...
    {
        decltype(auto) system_matrix = parent::GetSystemMatrix(numbering_subset, numbering_subset);

        stiffness_matrix_ = std::make_unique<GlobalMatrix>(system_matrix);
    }
}
````

This uses up the copy constructor of the `GlobalMatrix`: `system_matrix`has been properly initialized beforehand and we reuse the result rather than calling again all the Petsc stuff to initialize properly this (Petsc) matrix. 



## Assembling the stiffness

Now we're able to assemble the stiffness operator into the stiffness matrix:

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>


````c++
void VariationalFormulation
::SupplInit(const morefem_data_type& morefem_data)
{
...

    {        
        GlobalMatrixWithCoefficient matrix(GetNonCstStiffnessMatrix(), 1.);
        GetStiffnessOperator().Assemble(std::make_tuple(std::ref(matrix)));
    }
}
````

The syntax is a tad heavy here, but it's the price to pay for flexibility and efficiency: we may assemble into several matrices in a single command (or even matrices and vectors for non linear operators) and we need to be able to apply a different coefficient for each of them.


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Assemble stiffness operator into an ad hoc global matrix."
````

## Static case (at last!)

We are now finally able to run the whole static case, solving:
```math
\begin{cases}
\displaystyle \underline{\underline{\mathbb{K}}} \cdot \underline{\mathbb{U}}_h  = \underline{\mathbb{F}} \\
\underline{y}(\underline{x}) = \underline{0} \quad \text{on} \quad \Gamma^D
\end{cases}
```

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````c++
void VariationalFormulation::RunStaticCase()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

    // Assembling transient source into system RHS.
    {
        constexpr double irrelevant_time = 0.; // as this parameter has no time dependency. 
        GlobalVectorWithCoefficient vector(GetNonCstSystemRhs(numbering_subset), 1.);
        GetSurfacicSourceOperator().Assemble(std::make_tuple(std::ref(vector)), irrelevant_time);
    }

    parent::GetNonCstSystemMatrix(numbering_subset, numbering_subset).Copy(GetStiffnessMatrix(), __FILE__, __LINE__);

    ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset,
    numbering_subset);
    SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset, __FILE__, __LINE__);
}
````


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Static case implemented and working." 
````

## Defining the mass operator

Here we are defining the mass matrix required for the dynamic part of the run $ \underline{\underline{\mathbb{M}}} = \int_{\Omega_0} \rho_0  \underline{\underline{\mathbb{N}}} ^T \cdot \underline{\underline{\mathbb{N}}} \, \text{d}\Omega_0  $

<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
# include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"
````

````c++
private:

    //! Get the mass per square time step operator.
    const GlobalVariationalOperatorNS::Mass& GetMassOperator() const noexcept;

private:

    //! Mass operator.
    GlobalVariationalOperatorNS::Mass::const_unique_ptr mass_operator_ = nullptr;
````    

<p><strong><font color="green">In VariationalFormulation.hxx:</font></strong></p>

````c++
inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation
::GetMassOperator() const noexcept
{
    assert(!(!mass_operator_));
    return *mass_operator_;
}
````

<p><strong><font color="green">In VariationalFormulation.cpp, complete InitializeOperators():</font></strong></p>

````c++
void VariationalFormulation::DefineOperators(const morefem_data_type& morefem_data)
{
    ...
    {
        decltype(auto) felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        mass_operator_ = 
            std::make_unique<GlobalVariationalOperatorNS::Mass>(felt_space_highest_dimension,
                                                                displacement_ptr,
                                                                displacement_ptr);
    }
}
````


## Additional global linear algebra

In our simple model, dynamic iterations do not need a call to a solver: basic matrix / vector operators are enough. Indeed, we just need to update the values of the velocity and displacement fields as the problem we are solving here has only one dynamic dependency (related to the inertia). This is done with the following time scheme (Newmark): $ \frac{\dot{\underline{y}}^{n+1} + \dot{\underline{y}}^n}{2} = \frac{\underline{y}^{n+1} - \underline{y}^n}{\Delta t} $.
We will define helpful matrices and vectors to limit at maximum recomputation (this will get a tad verbose with all accessors):

<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
private:

    /// \name Accessors to vectors and matrices specific to the elastic problem.
    ///@{

    //! Accessor to the \a GlobalVector which contains current displacement.
    const GlobalVector& GetVectorCurrentDisplacement() const noexcept;

    //! Non constant accessor to the \a GlobalVector which contains current displacement.
    GlobalVector& GetNonCstVectorCurrentDisplacement() noexcept;

    //! Accessor to the \a GlobalVector which contains current velocity.
    const GlobalVector& GetVectorCurrentVelocity() const noexcept;

    //! Non constant accessor to the \a GlobalVector which contains current velocity.
    GlobalVector& GetNonCstVectorCurrentVelocity() noexcept;

    //! Accessor to the \a GlobalMatrix used along displacement in the model.
    const GlobalMatrix& GetMatrixCurrentDisplacement() const noexcept;

    //! Non constant accessor to the \a GlobalMatrix used along displacement in the model.
    GlobalMatrix& GetNonCstMatrixCurrentDisplacement() noexcept;

    //! Accessor to the \a GlobalMatrix used along velocity in the model.
    const GlobalMatrix& GetMatrixCurrentVelocity() const noexcept;

    //! Non constant accessor to the \a GlobalMatrix used along velocity in the model.
    GlobalMatrix& GetNonCstMatrixCurrentVelocity() noexcept;

    //! Accessor to the mass matrix.
    const GlobalMatrix& GetMassMatrix() const noexcept;

    //! Non constant accessor to the mass matrix.
    GlobalMatrix& GetNonCstMassMatrix() noexcept;

    ///@}
````

````c++
private:

    //! Vector current displacement.
    GlobalVector::unique_ptr vector_current_displacement_ = nullptr;

    //! Vector current velocity.
    GlobalVector::unique_ptr vector_current_velocity_ = nullptr;

    //! Matrix current displacement.
    GlobalMatrix::unique_ptr matrix_current_displacement_ = nullptr;

    //! Matrix current velocity.
    GlobalMatrix::unique_ptr matrix_current_velocity_ = nullptr;

    //! Mass matrix.
    GlobalMatrix::unique_ptr mass_matrix_ = nullptr;
`````

<p><strong><font color="green">In VariationalFormulation.hxx</font></strong></p>


````c++
inline const GlobalVector& VariationalFormulation
::GetVectorCurrentDisplacement() const noexcept
{
    assert(!(!vector_current_displacement_));
    return *vector_current_displacement_;
}


inline GlobalVector& VariationalFormulation
::GetNonCstVectorCurrentDisplacement() noexcept
{
    return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
}


inline const GlobalVector& VariationalFormulation
::GetVectorCurrentVelocity() const noexcept
{
    assert(!(!vector_current_velocity_));
    return *vector_current_velocity_;
}


inline GlobalVector& VariationalFormulation
::GetNonCstVectorCurrentVelocity() noexcept
{
    return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
}


inline const GlobalMatrix& VariationalFormulation
::GetMatrixCurrentDisplacement() const noexcept
{
    assert(!(!matrix_current_displacement_));
    return *matrix_current_displacement_;
}


inline GlobalMatrix& VariationalFormulation
::GetNonCstMatrixCurrentDisplacement() noexcept
{
    return const_cast<GlobalMatrix&>(GetMatrixCurrentDisplacement());
}


inline const GlobalMatrix& VariationalFormulation
::GetMatrixCurrentVelocity() const noexcept
{
    assert(!(!matrix_current_velocity_));
    return *matrix_current_velocity_;
}


inline GlobalMatrix& VariationalFormulation
::GetNonCstMatrixCurrentVelocity() noexcept
{
    return const_cast<GlobalMatrix&>(GetMatrixCurrentVelocity());
}


inline const GlobalMatrix& VariationalFormulation
::GetMassMatrix() const noexcept
{
    assert(!(!mass_matrix_));
    return *mass_matrix_;
}


inline GlobalMatrix& VariationalFormulation
::GetNonCstMassMatrix() noexcept
{
    return const_cast<GlobalMatrix&>(GetMassMatrix());
}
````

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````c++
void VariationalFormulation
::AllocateMatricesAndVectors()
{
    ...

    {
        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

        decltype(auto) system_matrix = parent::GetSystemMatrix(numbering_subset, numbering_subset);
        mass_matrix_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_current_displacement_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_current_velocity_ = std::make_unique<GlobalMatrix>(system_matrix);

        decltype(auto) system_rhs = parent::GetSystemRhs(numbering_subset);
        vector_current_velocity_ = std::make_unique<GlobalVector>(system_rhs);
        vector_current_displacement_ = std::make_unique<GlobalVector>(system_rhs);
    }

}
`````


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Add mass operator and linear algebra required to run the dynamic steps."
````

## PrepareDynamicRun()

<p><strong><font color="green">In Model.cpp:</font></strong></p>

````c++
void Model::SupplInitialize()
{
    ...
    variational_formulation.PrepareDynamicRuns();
}
````

<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````c++
public:

    /*!
     * \brief Prepare dynamic runs.
     *
     * For instance for dynamic iterations the system matrix is always the same; compute it once and for all
     * here.
     *
     * StaticOrDynamic rhs is what changes between two time iterations, but to compute it the same matrices are used at
     * each time iteration; they are also computed there.
     */
    void PrepareDynamicRuns();

private:

    //! Compute all the matrices required for dynamic calculation.
    void ComputeDynamicMatrices();

    //! Update the displacement for the next time iteration.
    void UpdateDisplacement();   
````

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````c++
void VariationalFormulation::PrepareDynamicRuns()
{
    // Assemble once and for all the system matrix in dynamic case; intermediate matrices used
    // to compute rhs at each time iteration are also computed there.
    ComputeDynamicMatrices();
    UpdateDisplacement();
}

void VariationalFormulation::ComputeDynamicMatrices()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));
    decltype(auto) system_matrix = this->GetNonCstSystemMatrix(numbering_subset, numbering_subset);
    decltype(auto) stiffness = GetStiffnessMatrix();
    
    {
        GlobalMatrixWithCoefficient mass(GetNonCstMassMatrix(), 1.);
        GetMassOperator().Assemble(std::make_tuple(std::ref(mass)));
    }
    
    decltype(auto) mass = GetMassMatrix();
    
    {
        // Compute the system matrix, which won't change afterwards!
        system_matrix.Copy(stiffness, __FILE__, __LINE__);
        system_matrix.Scale(0.5, __FILE__, __LINE__);
        
        const auto coefficient =
        1. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
        
#ifndef NDEBUG
        AssertSameNumberingSubset(mass, system_matrix);
#endif // NDEBUG
        
        Wrappers::Petsc::AXPY<NonZeroPattern::same>( coefficient,
                                                    mass,
                                                    system_matrix,
                                                    __FILE__, __LINE__);
    }
    
    {
        // Displacement matrix.
        decltype(auto) current_displacement_matrix = GetNonCstMatrixCurrentDisplacement();
        current_displacement_matrix.Copy(mass, __FILE__, __LINE__);
        
        const auto coefficient =
        2. * GetVolumicMass().GetConstantValue() / NumericNS::Square(parent::GetTimeManager().GetTimeStep());
        
        current_displacement_matrix.Scale(coefficient, __FILE__, __LINE__);
        
#ifndef NDEBUG
        AssertSameNumberingSubset(stiffness, current_displacement_matrix);
#endif // NDEBUG
        
        Wrappers::Petsc::AXPY<NonZeroPattern::same>( -.5,
                                                    stiffness,
                                                    current_displacement_matrix,
                                                    __FILE__, __LINE__);
        
    }
    
    {
        // Velocity matrix.
        decltype(auto) current_velocity_matrix = GetNonCstMatrixCurrentVelocity();
        current_velocity_matrix.Copy(mass, __FILE__, __LINE__);
        current_velocity_matrix.Scale(2. * GetVolumicMass().GetConstantValue() / parent::GetTimeManager().GetTimeStep(),
                                      __FILE__, __LINE__);
    }
}

void VariationalFormulation::UpdateDisplacement()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));
    
    GetNonCstVectorCurrentDisplacement().Copy(parent::GetSystemSolution(numbering_subset),
                                              __FILE__, __LINE__);
}
````


<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "Quantities required for dynamic phase have all been computed."
````

## Model::Forward

This is where we are solving our dynamic linear system:

```math
\begin{cases}
\underline{\underline{\mathbb{M}}} \cdot \underline{\dot{\mathbb{V}}}_h + \underline{\underline{\mathbb{K}}} \cdot \underline{\mathbb{U}}_h  = \underline{\mathbb{F}} \\
\underline{y}(\underline{x}) = \underline{0} \quad \text{on} \quad \Gamma^D
\end{cases}
```
<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````cpp
public:

    //! At each time iteration, compute the system Rhs.
    void ComputeDynamicSystemRhs();
````

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````cpp
void VariationalFormulation::ComputeDynamicSystemRhs()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

    decltype(auto) rhs = this->GetNonCstSystemRhs(numbering_subset);

    // Compute the system RHS. The rhs is effectively zeroed through the first MatMult call.
    decltype(auto) current_displacement_matrix = GetMatrixCurrentDisplacement();
    decltype(auto) current_velocity_matrix = GetMatrixCurrentVelocity();

    decltype(auto) current_displacement_vector = GetVectorCurrentDisplacement();
    decltype(auto) current_velocity_vector = GetVectorCurrentVelocity();

    Wrappers::Petsc::MatMult(current_displacement_matrix, current_displacement_vector, rhs, __FILE__, __LINE__);
    Wrappers::Petsc::MatMultAdd(current_velocity_matrix, current_velocity_vector, rhs, rhs, __FILE__, __LINE__);
}
````

<p><strong><font color="green">In Model.cpp, complete Forward():</font></strong></p>

````cpp
void Model::Forward()
{
    decltype(auto) formulation = this->GetNonCstVariationalFormulation();

    // Only Rhs is modified at each time iteration; compute it and solve the system.
    formulation.ComputeDynamicSystemRhs();
     
    decltype(auto) god_of_dof = formulation.GetGodOfDof();
    decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

    if (GetTimeManager().NtimeModified() == 1)
    {
        formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(numbering_subset, numbering_subset);
        formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset, __FILE__, __LINE__);
    }
    else
    {
        formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset, numbering_subset);
        formulation.SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset, __FILE__, __LINE__);
    }
}
````

`GetTimeManager().NtimeModified()` is an index that can only be increased (and is automatically handled by the library). For our model the matrix needs to be set up properly only on first call, else the condition here.

## FinalizeStep



<p><strong><font color="green">In VariationalFormulation.hpp:</font></strong></p>

````cpp
public:
    //! Update displacement and velocity for next time step.
    void UpdateDisplacementAndVelocity();

private:
    /*!
    * \brief Update the velocity for the next time iteration.
    *
    * BEWARE: this method must be called BEFORE UpdateDisplacement(), as it relies upon the displacement
    * that has been used in the last Ksp solve.
    */
    void UpdateVelocity();    
````

<p><strong><font color="green">In VariationalFormulation.cpp:</font></strong></p>

````cpp
void VariationalFormulation::UpdateDisplacementAndVelocity()
{
    // Ordering of both calls is capital here!
    UpdateVelocity();
    UpdateDisplacement();
}


void VariationalFormulation::UpdateVelocity()
{
    decltype(auto) god_of_dof = parent::GetGodOfDof();
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));
    
    decltype(auto) current_displacement_vector = GetVectorCurrentDisplacement();
    decltype(auto) system_solution = parent::GetSystemSolution(numbering_subset);
    decltype(auto) current_velocity_vector = GetNonCstVectorCurrentVelocity();

    assert(parent::GetTimeManager().GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

    {
        // Update first the velocity.
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> solution(system_solution, __FILE__, __LINE__);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> displacement_prev(current_displacement_vector, __FILE__, __LINE__);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> velocity(current_velocity_vector, __FILE__, __LINE__);

        const auto size = velocity.GetSize(__FILE__, __LINE__);
        assert(size == solution.GetSize(__FILE__, __LINE__));
        assert(size == displacement_prev.GetSize(__FILE__, __LINE__));

        const double factor = 2. / parent::GetTimeManager().GetTimeStep();

        for (auto i = 0ul; i < size; ++i)
        {
            velocity[i] *= -1.;
            velocity[i] += factor * (solution.GetValue(i) - displacement_prev.GetValue(i));
        }
    }
}
````

`AccessVectorContent` here is a facility to access the processor-wise content of a `GlobalVector` (which under the hood is really a PETSc vector) and possibly modify them if the access mode is `read and write`. It is as indicated by the namespace hints a wrapper over PETSc functions; it relies upon the [RAII idiom
](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/blob/master/5-UsefulConceptsAndSTL/2-RAII.ipynb) and the values inside the PETSc vectors are truly modified only when the `rappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write>`is destroyed (so here once the local variable goes out of scope when leaving the block).

<p><strong><font color="green">In Model.cpp, complete FinalizeStep():</font></strong></p>

````cpp
void Model::SupplFinalizeStep()
{
    // Update quantities for next iteration.
    decltype(auto) formulation = this->GetNonCstVariationalFormulation();
    decltype(auto) god_of_dof = formulation.GetGodOfDof();
    decltype(auto) numbering_subset =
        god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement));

    formulation.WriteSolution(this->GetTimeManager(), numbering_subset);

    formulation.UpdateDisplacementAndVelocity();
}
````

<p><strong><font color="red">In a terminal at the root of MoReFEM project:</font></strong></p>

````bash
git add Sources
git commit -m "Our model tutorial is fully implemented\!"
````

### Running in parallel

MoReFEM is designed so that a model written in sequential should work directly in parallel.

You may choose a parallel run with:

````bash
${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Openmpi/bin/mpirun -np 2 Sources/ModelTutorial -i ../Sources/demo.lua --overwrite_directory
````

where environment variable `MOREFEM_THIRD_PARTY_LIBRARIES_DIR` should be set to the value it held in `${MOREFEM_INSTALL_PREFIX}/cmake/PreCache.cmake` file and `np` indicates how many processors should be used (don't choose too high a value: we may run into errors when the number of processors is overdimensioned compared to the size of the model considered).

The data are written in a different subdirectory as the sequential run; when aggregated (for instance with `EnsightOutput` executable - see below) the values should be comparable to the sequential ones.


### Ensight output

For all our models, we usually provide a post processing executable which generates output files in Ensight format.

Let's create a new file `main_ensight_output.cpp` in `Sources` directory:

<p><strong><font color="green">In main_ensight_output:</font></strong></p>

````cpp
#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"

#include "Model.hpp"

using namespace MoReFEM;
using namespace MoReFEM::ModelTutorialNS;

int main(int argc, char** argv)
{
    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        NumberingSubsetIndex::displacement) };

    std::vector<std::string> unknown_list{ "displacement" };

    return ModelNS::MainEnsightOutput<ModelTutorialNS::Model>(
        argc, argv, AsMeshId(MeshIndex::solid), numbering_subset_id_list, unknown_list);
}
````

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

````cmake
add_executable(ModelTutorial-EnsightOutput
              ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)
              
target_link_libraries(ModelTutorial-EnsightOutput
                      ModelTutorial_lib
                      MoReFEM_post_processing)  
````

Of course check it works as expected:

<p><strong><font color="red">In a terminal in build directory:</font></strong></p>

````bash
ninja
Sources/ModelTutorial-EnsightOutput -i ${MOREFEM_RESULT_DIR}/ModelTutorial/Rank_0/input_data.lua
````

(the Lua file used has been copied there in the output directory.)

This will generate files in `$MOREFEM_RESULT_DIR/ModelTutorial/Rank_0/Mesh_1/Ensight6` directory; check the generated scl files are very close to the ones in `Sources/ModelInstances/Elasticity/ExpectedResults/Ascii/2D/Mesh_1/Ensight6` in MoReFEM.

You may use the files hence produced to visualize the result with Paraview or Ensight.

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "EnsightOutput executable added."
````



### UpdateLuaFile

Another facility that is rather handy is a program that enables upgrading of the Lua file (for instance to reflect the changes in automated comments). This is a beta program at best and don't cover all the cases that might have modified the Lua files, but it may still be of use.

Let's create a `main_update_lua_file.cpp` in `Sources` directory:


<p><strong><font color="green">In main_update_lua_file.cpp:</font></strong></p>

````c++
#include "Model/Main/MainUpdateLuaFile.hpp"

#include "Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    return ModelNS::MainUpdateLuaFile<ModelTutorialNS::Model>(argc, argv);
}
````

<p><strong><font color="green">In Sources/CMakeLists.txt:</font></strong></p>

````cmake
add_executable(ModelTutorial-UpdateLuaFile
              ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
              
target_link_libraries(ModelTutorial-UpdateLuaFile
                      ModelTutorial_lib
                      MoReFEM_post_processing)
````



<p><strong><font color="red">In a terminal in build directory:</font></strong></p>

````bash
ninja
Sources/ModelTutorial-UpdateLuaFile -i ../Sources/demo.lua 
````

If you're already familiar with the expected Lua syntax, you may use the `--skip-comments` flag which will remove most of the comments of the file, leaving only those you really need (for instance the list of unknowns in a finite element space remain because you need to know their ordering to assign properly the shape functions).


````bash
Sources/ModelTutorial-UpdateLuaFile -i ../Sources/demo.lua  --skip-comments
````

<p><strong><font color="red">In a terminal at the root of your project:</font></strong></p>

````bash
git add Sources
git commit -m "UpdateLuaFile executable added; demo.lua file features now much less comments."
````


### Integration tests

If your model is to be kept, it is advised to also set up integration tests to check the results keep stable over time, especially when libraries (both MoReFEM and third party ones) have been updated.

This tutorial has already been long and dense enough so we won't delve into this here and now, but you may have a look to existing models to see how it is usually done (for instance [here](https://gitlab.inria.fr/MoReFEM/Models/AcousticWave) for acoustic wave model).

The geist of the numeric comparison is a bit of cheating: we actually compare for the time beings the results from the `EnsightOutput` post-processing executable, as this format lose some numeric precision and we do not trigger a "discrepancy" when there is a neglictible numeric difference (especially considering the `ExpectedResults` were generated for a given OS on a given computer and we want to reach the same result regardless of the computer and OS used).