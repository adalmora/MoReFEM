[TOC]

# As a model developer

`MoReFEMData` object is the backbone of MoReFEM library; it is used to handle fairly important data.

Its responsability are:

- To manage the `InputData`, which provides data the end user may want to modify when using an existing model. More about it [here](../Utilities/InputData.md). Accessible through `GetInputData()`.
- To manage the `ModelSettings`, which provides data fixed in stone by the author of a given model. More about it [here](../Utilities/InputData.md). Accessible through `GetModelSettings()`.
- To initialize the mpi set up. Underlying `MoReFEM::Wrappers::Mpi` object is accessible through `GetMpi()`.
- To keep the information pertaining to parallel set up (do we want to write data to enable rerun of the program from pre-partitioned data? Are we using prepartitioned data to run it? etc...). Accessible through `GetParallelismPtr()` (that might return `nullptr` if no such field defined in input data).
- To provide through `GetResultDirectory()` a directory into which results may be written **safely**.


Third template argument specifies the type of program that is being run - there are some subtle differences of 
behaviour  when you're running a mere test or a full-fledged model. As a model develop we can assume you intend to write a specific model; `program_type::model` is the option you should use.
