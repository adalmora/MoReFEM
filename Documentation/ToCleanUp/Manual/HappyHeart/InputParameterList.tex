\chapter{Input parameter list}\label{ChapInputParameterList}


\section{Introduction}


The point of \code{InputParameterList} is to handle properly the input file that contains all the relevant input parameters for the problem\footnote{Several different input files such as in HeartLab could easily be used as well.}. This functionality might seem trivial but is really crucial to the user-friendliness of the code: doing it properly ensures that the user is exactly aware of all the input parameters in its problem.
\medskip

\code{InputParameterList} is a small library designed to meet this user-friendliness requirement, most noticeably by avoiding most of the common pitfalls met when dealing with such a feature. The library relies upon Ops library, which itself is a wrapper over Lua to access the content of the input file and process it. Ops is mostly used at the very beginning of the program, when the input parameters are loaded; afterwards it remains used only when the programs needs to use a function defined in the input parameter. Other parameters are stored in their own right in \code{InputParameterList::Base} class.

\section{Structure of the library}

The implementation of the library has been splitted into two parts:
\begin{itemize}
	\item The core of the \code{InputParameterList} library has been implemented as a base template class named \code{HappyHeart::Utilities::InputParameterList::Base} declared in \code{Utilities/InputParameterList} folder. All the underlying mechanisms are implemented there. I won't dwell much on this part of the library: it relies heavily on metaprogramming features, and is therefore rather terse
	\item The class actually used within HappyHeart is defined in \code{Core} folder and is named \code{HappyHeart::InputParameterList}. It is a mere chid class of \code{Utilities::InputParameterList::Base}, which only add more user-friendly accessor to the underlying data. Its role is to manage the input parameters related to the actual problem; as we shall see it is straightforward to add a new input parameter.
\end{itemize}

A really important feature of the design is template parameter of \code{Utilities::InputParameterList::Base}, which is expected a tuple that defines all the input parameters related to the problems. But first of all, let's see exactly how it's defined in HappyHeart.


\section{Input parameter class}\label{SecInputParameterClass}

Each input parameter is represented by a class, which derives from \code{Utilities/InputParameterList/Private/InputParameter}. This parent class is a template class which defines the return type of the input parameter (i.e. for instance whether it is a double, a std::string, a function or a std::vector; more on this in \refsection{SecInputParameterTypes}) and is in charge is storing the value of the parameter read in the input file. As the enclosing \code{Private} namespace highlights, a developer shouldn't care much about this mechanism: its sole concern is to define correctly the return type.
\medskip

An actual input parameter class adds on top of it several static member functions that describe precisely the object, as can be seen below:

\begin{lstlisting}[label=CodeInputParameterSolver, caption=Example of the definition of an input parameter class.]
// Notice the template parameter here, where the return type is defined.
struct Solver : public Utilities::InputParameterList::Private::InputParameter<std::vector<std::string>>
{
  //! Name of the section in Lua input file.
  static const std::string Section() { return "Petsc"; }

  //! Name of the input parameter in Lua input file.
  static const std::string NameInFile() { return "solver"; }

  //! Description of the input parameter. 
  static const std::string Description() { return "List of solver: { chebychev cg gmres preonly bicg python };\n\t-- To use Mumps choose preonly."; }

  /*!
   * \brief Constraint to fulfill.
   *
   * Might be left empty; if not the format to respect is the Ops one. Hereafter some text 
   * from Ops example file:
   *
   * An age should be greater than 0 and less than, say, 150. It is possible
   * to check it with a logical expression (written in Lua). The expression
   * should be written with 'v' being the variable to be checked.
   * \a constraint = "v >= 0 and v < 150"
   *
   * It is possible to check whether a variable is in a set of acceptable
   * value. This is performed with 'ops_in' (a Lua function defined by Ops).
   * \a constraint = "ops_in(v, {'Messiah', 'Water Music'})"
   *
   * If a vector is retrieved, the constraint must be satisfied on every
   * element of the vector.
   */
  static const std::string Constraint() { return "ops_in(v, {'chebychev', 'cg', 'gmres', 'preonly', 'bicg', 'python'})"; }


  /*!
   * \brief Default value.
   *
   * This is intended to be used only when the class is used to create a default file; never 
   * when no value has been given in the input parameter file (doing so is too much error
   * prone...)
   *
   * This is given as a string; if no default value return an empty string. 
   * The value must be Ops-formatted.
   */
  static const std::string DefaultValue() { return "{\"preonly\"}"; }
};
\end{lstlisting}

So if you need to create a new input parameter, you just have to copy the example below and fill correctly both the template parameter that specifies the return type and the few static member functions shown above.


\section{Registering a new input parameter}

Once a new input parameter class has been created, you must register it to the instantiation of \code{HappyHeart::InputParameter} by adding it in the tuple. When it's done, the code is aware that this input parameter exists and will for instance require any input parameter list to specify a value for this parameter. 
\medskip

The only remaining operation is to provide access to the value within the rest of the C++ code.

\section{Access to the value of an input parameter}\label{SecInputParameterAccess}

As said previously, all the underlying mechanisms are defined in the core part of the library. So we could almost access it directly without doing anything more, by calling the following method of \code{Utilities/InputParameterList::Base} template class to access for instance the input parameter \code{Solver} defined in \refcode{CodeInputParameterSolver}:

\begin{lstlisting}
this->template ReadHelper<Solver>();
\end{lstlisting}

However, this is rather unwieldy: the user must know and understand on some level the weird C++ syntax involving the \code{template} keyword\footnote{More on this in \refsection{SecTemplateKeyword}} and check the input parameter class \code{Solver} if he wants to know what type it is. So this method has been tagged as \code{protected}: the developer shouldn't call it directly, but instead define an appropriate method in \code{Core::InputParameterList} class that makes the call:

\begin{lstlisting}
const std::vector<std::string>& Solver() const;
\end{lstlisting}

which does the underlying work of calling the code above\footnote{And to be honest a bit more: it also check the return type matches what is defined in \code{Solver} class through a \code{static\_assert}.}.
\medskip 

It couldn't be plainer: the developer has only to look into the header file of \code{Core::InputParameterList} and he will see directly all the available methods, with the return type apparent directly.


\section{The different types available for input parameters}\label{SecInputParameterTypes}

\begin{itemize}
	\item std::string
	\item Real (\code{double} and \code{float}).
	\item Any kind of integral type\footnote{Against all expectations this point required some work: Ops does not natively support anything apart \code{int} and \code{bool}, whereas many integral types used in HappyHeart are intended to be unsigned.}
	\item Vectors of any of the above types.
	\item Functions.
\end{itemize}

The last point is very crucial, and ruled out entirely GetPot as a possible choice for the underlying library. 
\medskip

The principle when a function is given in the input file is that the calculations concerning this function are performed by Lua. When you have an input parameter which is a function, the type to give as template argument is:

\begin{lstlisting}
Utilities::InputParameterList::OpsFunction<ReturnType(Args...)>
\end{lstlisting}

where \code{Args...} represents the types of the argument and return type the type of the function. So for instance if you want to consider a function that 
returns a double and takes 4 double as arguments, the syntax is:

\begin{lstlisting}
Utilities::InputParameterList::OpsFunction<double(double, double, double, double)>
\end{lstlisting}

This syntax doesn't come out of the blue: it is exactly the same as the one used in definition of \code{std::function}. \code{OpsFunction} is a functor which keeps the Ops object underneath and calls the appropriate Ops method whenever an operation is required.


\section{Tying up the tuple and the input file}

Once your problem is well-define with all relevant input parameters identified and stuffed in the tuple, the only remaining step is to create the input file that will fill the values of each input parameter. A rather crude script is available at the moment in \code{Core} folder: \code{main\_create\_default\_input\_file} will take the tuple and generate from it a default input parameter file.

\medskip

This default file is not yet ready to use: when an input parameter defines a non empty default value through its \code{DefaultValue()} static member function, this value is put in the generated file. If none was provided, a filler text is written; not supplying a reasonable value would trigger an error while trying to run it.

\begin{lstlisting}[caption=Example of generated code for the Solver class defined previously]
-- List of solver: { chebychev cg gmres preonly bicg python };
-- To use Mumps choose preonly.
-- Expected format: {"VALUE1", "VALUE2", ...}
-- Constraint: ops_in(v, {'chebychev', 'cg', 'gmres', 'preonly', 'bicg', 'python'})
solver = {"preonly"}
\end{lstlisting}


\section{User-friendliness and safety}\label{SecInputParameterSafeties}

I have hinted previously the benefits of \code{InputParameterList}, but it is better to justify the safety it gives:

\begin{itemize}
	\item There are no hidden input parameter the user might be unaware of: it is mandatory for each of them to be defined both in the tuple and in the input file. If not defined in the tuple, any use would trigger a compilation error. If not defined in the input file, an exception thrown during \code{InputParameterList} construction. This requirement induces a very important safety: all input parameters used within the code are by design initialized correctly.
	\item No duplicates are allowed in the input file. If a same variable is defined twice, an exception is thrown, requesting one of them to be removed. The reason for this choice is to avoid any confusion: if the user sees for instance \code{Young = 1.e5}, the program should really roll with it and not use another value that might be  defined many lines later.
	\item All the entries in the input file should match an element of the tuple. This constraint has been put to avoid cluttered input files, with many obsolete parameters that are absolutely not used in the code. It also avoids the problems that may arise due to typo in an entry name.
	\item Ops provides a very neat mechanism to check whether a value is within a given range or a given list (comment of \code{InputParameterList} class gives away the syntax).
	\item Constraints involving several parameters can be implemented easily as well. There is for instance a method named \code{EnsureSameLength} that checks several input parameters are actually vectors with the same number of elements.
	\item Very early detection of possible issues. All problems are dealt with either at compile-time or in \code{InputParameterList} construction.	
	\item Customization of the \code{InputParameterList}: the object can be tailored neatly to any problems with the tuple template parameter. For instance in HappyHeart hyperelastic problem and Navier-Stokes one does not require at all the same input parameters; therefore for each of them the tuple given while defining the same problem won't be the same. This avoids a very length input parameter file which includes many input parameters, some of which might be completely unrelated to the problem at bay. Concretely, we might have:
	
	\begin{lstlisting}
	// In file defining the hyperelastic problem.
	#include "Core/InputParameterList"
	typedef std::tuple
	<
		...
		InputParameter::Solid::Poisson,
		...
	> HyperElasticParameters;
	
	InputParameterList<HyperElasticParameters> input_hyperelastic("hyperelastic_input_file");
	
	// In file defining the Navier-Stokes problem.
	typedef std::tuple
	<
		...
		InputParameter::Fluid::Viscosity,
		...
	> NavierStokesParameters;
	
	InputParameterList<NavierStokesParameters> input_ns("NS_input_file");

	
	
	
	\end{lstlisting}
	
	and a call such as:
	
	\begin{lstlisting}
	input_hyperelastic.FluidViscosity();
	\end{lstlisting}
	
	would yield a compilation error as \code{FluidViscosity} doesn't belong to the related tuple.
	
	\item Lastly, the class keeps track of the input parameter that were actually used within the program. When the \code{InputParameterList} object is destroyed,
	all the input parameters that were left unused are printed on screen or in a file. That does not necessarily highlight a bug or an issue, but it can be an handy tool for a developer to understand what occurred; it might for instance help to point out which parameter are actually useless for a given problem and could be dropped without any ado.
	
\end{itemize}


\section{Known limitations and issues}

Despite all its features, there are several limitations in \code{InputParameterList} library:

\subsection{All function arguments must be the same type}

I wasn't completely honest in \refsection{SecInputParameterTypes} when I presented how to deal with a function input parameter: the example that was provided worked only because all arguments were \code{double}. Should one of this argument be another type, such as an \code{unsigned int}, the code wouldn't compile.
\medskip

This limitation stems from Ops library: all arguments are stored in a same \code{std::vector}, which can work only if they are the same type. I know how to circumvent this smoothly, but I have not had yet the time to do it.

\subsection{No subsection}

Ops is very flexible about the nesting of the variables: you can define variables directly, group some of them in a same section as many time as you want.

For instance, you might have:
\begin{lstlisting}
section = {
    subsection = {
        subsubsection = {
                            myvariable = 10
                        }
                 }
           }
\end{lstlisting}

and access it through:

\begin{lstlisting}
Ops::Ops ops("path_to_the_input_file.lua");

ops.Get<int>("section.subsection.subsubsection.myvariable");
\end{lstlisting}

\code{InputParameterList} does not let such freedom: you might define a variable in the current level, or in a section, but you can't go any level deeper. We could extend it to allow one or several levels deeper if required (at the cost of additional static member functions for each additional level), but it didn't seem so pressing.

\subsection{Manual parsing of the file}

Two of the safeties described in \refsection{SecInputParameterSafeties} couldn't be achieved through Ops/Lua alone, at least with my feeble knowledge of Lua and what a Google search provided:

\begin{itemize}
	\item Lua doesn't care about duplicates. When an input parameter is present twice, the second value is retained and there is no memory of the first one. This is performed very early, with the call to \code{luaL\_dofile()} function. As explained early, I deem duplicates too dangerous to be allowed, so this is for me a real problem.
	\item Ops does know only about the input parameters that have already been explicitly fetched by a call to its \code{Get()} function. So there is no way to identify the values that are defined but are completely left aside by the code. I'm almost positive though that it could be achieved with Lua; but I do not know how and have not the time now to learn Lua best.
\end{itemize}

To circumvent these limitations, at the very beginning of the \code{InputParameterList} construction the file is parsed to extract from it all the keys found; with this list we can easily perform the two missing checks. However, the self-written parsing is probably not as robust as Lua's one, and it is entirely possible that some input files that should be accepted by Lua are rejected due to this manual parse. So maybe the function will have to be robustified should one leak be found there.

