/*!
//
// \file
//
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
#define MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_

#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp" // IWYU pragma: export


namespace MoReFEM::PostProcessingNS
{


    //! Alias to the class defined in Core (as it is also needed for restart during Model runs).
    using TimeIterationFile = ::MoReFEM::InterpretOutputFilesNS::TimeIterationFile;


} // namespace MoReFEM::PostProcessingNS


/// @} // addtogroup PostProcessingGroup

#endif // MOREFEM_x_POST_PROCESSING_x_FILE_x_TIME_ITERATION_FILE_HPP_
