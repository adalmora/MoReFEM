/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 Jun 2016 11:13:36 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "PostProcessing/Data/Interface.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::Data
{


    Interface::Interface(InterfaceNS::Nature nature,
                         std::size_t index,
                         std::vector<CoordsNS::processor_wise_position>&& coords_list)
    : nature_(nature), index_(index), coords_list_(coords_list)
    { }


} // namespace MoReFEM::PostProcessingNS::Data


/// @} // addtogroup PostProcessingGroup
