/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:38:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#include <sstream>
#include <string>

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"

#include "FiniteElement/Unknown/EnumUnknown.hpp"

#include "PostProcessing/Data/UnknownInformation.hpp"


namespace MoReFEM::PostProcessingNS::Data
{


    UnknownInformation::UnknownInformation(const std::string& line)
    {
        std::istringstream iconv(line);

        iconv >> name_;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line, __FILE__, __LINE__);

        iconv.ignore(2); // for colon ':'

        std::string buf;
        iconv >> buf;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line, __FILE__, __LINE__);


        if (buf == "scalar")
            nature_ = UnknownNature::scalar;
        else if (buf == "vectorial")
            nature_ = UnknownNature::vectorial;
        else
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line, __FILE__, __LINE__);
    }


} // namespace MoReFEM::PostProcessingNS::Data


/// @} // addtogroup PostProcessingGroup
