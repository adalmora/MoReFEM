/*!
//
// \file
//
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_
#define MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp" // IWYU pragma: export


namespace MoReFEM::PostProcessingNS::Data
{


    //! Alias to the class defined in Core (as it is also needed for restart during Model runs).
    using TimeIteration = ::MoReFEM::InterpretOutputFilesNS::Data::TimeIteration;


} // namespace MoReFEM::PostProcessingNS::Data


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_DATA_x_TIME_ITERATION_HPP_
