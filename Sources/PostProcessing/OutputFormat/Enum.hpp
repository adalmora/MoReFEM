/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup PostProcessingGroup
// \addtogroup PostProcessingGroup
// \{
*/


#ifndef MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENUM_HPP_
#define MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENUM_HPP_


namespace MoReFEM::PostProcessingNS
{


    /*!
     * \brief Enum class that specifies the nature of the mesh using in post-processing.
     *
     * No if the mesh is the one that was present during the run of the model.
     * Yes if it is rather a finer mesh upon which the solution is displayed as if it was Q1.
     *
     * Currently 'no' assumes the model was P1 or Q1, and 'yes' that it was run in sequential.
     */
    enum class RefinedMesh { no, yes };


} // namespace MoReFEM::PostProcessingNS


/// @} // addtogroup PostProcessingGroup


#endif // MOREFEM_x_POST_PROCESSING_x_OUTPUT_FORMAT_x_ENUM_HPP_
