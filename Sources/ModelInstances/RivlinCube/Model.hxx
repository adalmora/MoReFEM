/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/RivlinCube/Model.hpp"

#include <cassert>
#include <string>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::RivlinCubeNS { class VariationalFormulation; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::RivlinCubeNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("RivlinCube");
        return name;
    }

    inline const VariationalFormulation& Model::GetVariationalFormulation() const
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


    inline void Model::SupplInitializeStep()
    { }


} // namespace MoReFEM::RivlinCubeNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_MODEL_HXX_
