/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_INPUT_DATA_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::RivlinCubeNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        highest_dimension = 1,
        face1 = 2,
        face2 = 3,
        face3 = 4,
        face4 = 5,
        face5 = 6,
        face6 = 7,
        face456 = 8,
        full_mesh = 9
    };


    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t { face1 = 1, face2 = 2, face3 = 3 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, surface_pressure = 2 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { displacement = 1 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { monolithic = 1 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };


    //! \copydoc doxygen_hide_initial_condition_enum
    enum class InitialConditionIndex { displacement_initial_condition = 1 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Path,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Format,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::SpaceUnit,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face3),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face4),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face5),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face6),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::face456),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face1),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face2),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face3),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::surface_pressure),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::CheckInvertedElements,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::displacement_initial_condition),

        InputDataNS::Source::StaticPressure,

        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::monolithic),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::displacement),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::Dimension,

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face3),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face4),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face5),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face6),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::face456),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face1),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face2),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::face3),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::surface_pressure),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(InitialConditionIndex::displacement_initial_condition)
    >;
    // clang-format on



    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::model>;


} // namespace MoReFEM::RivlinCubeNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_RIVLIN_CUBE_x_INPUT_DATA_HPP_
