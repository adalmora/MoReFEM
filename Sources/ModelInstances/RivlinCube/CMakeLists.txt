##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4RivlinCube_lib ${LIBRARY_TYPE} )

target_sources(MoReFEM4RivlinCube_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/README.md        
        ${CMAKE_CURRENT_LIST_DIR}/demo_tetrahedra.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_hexahedra.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx        
)

target_link_libraries(MoReFEM4RivlinCube_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4RivlinCube_lib ModelInstances/RivlinCube ModelInstances/RivlinCube)                          

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4RivlinCube ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4RivlinCube
                      MoReFEM4RivlinCube_lib)

apply_lto_if_supported(MoReFEM4RivlinCube)

morefem_organize_IDE(MoReFEM4RivlinCube ModelInstances/RivlinCube ModelInstances/RivlinCube)

morefem_install(MoReFEM4RivlinCube MoReFEM4RivlinCube_lib)


##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4RivlinCubeEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)

target_link_libraries(MoReFEM4RivlinCubeEnsightOutput
                      MoReFEM4RivlinCube_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4RivlinCubeEnsightOutput ModelInstances/RivlinCube ModelInstances/RivlinCube)

morefem_install(MoReFEM4RivlinCubeEnsightOutput)


##########################################
# Executable to update a input Lua file
##########################################

add_executable(MoReFEM4RivlinCubeUpdateLuaFile
              ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)

target_link_libraries(MoReFEM4RivlinCubeUpdateLuaFile
                      MoReFEM4RivlinCube_lib
                      ${MOREFEM_POST_PROCESSING})

morefem_organize_IDE(MoReFEM4RivlinCubeUpdateLuaFile ModelInstances/RivlinCube ModelInstances/RivlinCube)                          

morefem_install(MoReFEM4RivlinCubeUpdateLuaFile)


##########################################
# Tests
##########################################

morefem_test_run_model_in_both_modes(NAME RivlinCube-Hexa
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/RivlinCube/demo_hexahedra.lua
                                     EXE MoReFEM4RivlinCube
                                     TIMEOUT 20)
                                     
morefem_test_run_model_in_both_modes(NAME RivlinCube-Tetra
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/RivlinCube/demo_tetrahedra.lua
                                     EXE MoReFEM4RivlinCube
                                     TIMEOUT 20)


##########################################
# Executable and test which check reference results are properly found.
##########################################

add_executable(MoReFEM4RivlinCubeCheckResults
               ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
              
target_link_libraries(MoReFEM4RivlinCubeCheckResults
                      MoReFEM4RivlinCube_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(MoReFEM4RivlinCubeCheckResults ModelInstances/RivlinCube ModelInstances/RivlinCube)                          
morefem_boost_test_check_results(NAME RivlinCube
                                 EXE MoReFEM4RivlinCubeCheckResults
                                 TIMEOUT 20)


##########################################
## Populate XCode schemes with valid arguments
##########################################

set_property (TARGET MoReFEM4RivlinCube PROPERTY XCODE_SCHEME_ENVIRONMENT "MOREFEM_RESULT_DIR=/tmp/MoReFEM_in_XCode/Results" "MOREFEM_PREPARTITIONED_DATA_DIR=/tmp/MoReFEM_in_XCode/PrepartitionedData")

set_property (TARGET MoReFEM4RivlinCube PROPERTY XCODE_SCHEME_ARGUMENTS "-i ${CMAKE_CURRENT_LIST_DIR}/demo_hexahedra.lua" "--overwrite_directory")
