/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE model_elasticity


#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension, std::string&& ascii_or_bin);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(seq_3d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_bin, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_bin, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_FromPrepartitionedData", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_bin_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_FromPrepartitionedData", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_bin_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "3D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_restart_binary, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_restart_binary, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "3D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_2d_bin, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_bin, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(seq_2d_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_FromPrepartitionedData", "2D", "Ascii");
}

BOOST_FIXTURE_TEST_CASE(mpi4_2d_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "2D", "Ascii");
}


BOOST_FIXTURE_TEST_CASE(seq_2d_bin_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_FromPrepartitionedData", "2D", "Binary");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d_bin_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData", "2D", "Binary");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension, std::string&& ascii_or_bin)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{
                "Sources", "ModelInstances", "Elasticity", "ExpectedResults", ascii_or_bin, dimension },
            __FILE__,
            __LINE__);

        FilesystemNS::Directory obtained_dir(
            output_dir,
            std::vector<std::string>{ seq_or_par, ascii_or_bin, "Elasticity", dimension, "Rank_0" },
            __FILE__,
            __LINE__);

        if (!Utilities::String::EndsWith(seq_or_par, "_FromPrepartitionedData")
            && (!Utilities::String::EndsWith(seq_or_par, "_Restart")))
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");


        if (Utilities::String::StartsWith(seq_or_par,
                                          "Seq")) // in parallel one file per rank with only processor-related data...
            /* BOOST_REQUIRE_NO_THROW */ (
                TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__));

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        /* BOOST_REQUIRE_NO_THROW */ (
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__));

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0ul; i <= 5ul; ++i)
        {
            oconv.str("");
            oconv << "solid_displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            /* BOOST_REQUIRE_NO_THROW */ (TestNS::CompareDataFiles<MeshNS::Format::Ensight>(
                ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__, 1.e-11));
        }
    }


} // namespace
