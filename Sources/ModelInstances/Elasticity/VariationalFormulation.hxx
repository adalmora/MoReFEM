/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Jul 2014 13:43:40 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "ModelInstances/Elasticity/VariationalFormulation.hpp"


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class Unknown; }
namespace MoReFEM::GlobalVariationalOperatorNS { class GradOnGradientBasedElasticityTensor; }
namespace MoReFEM::GlobalVariationalOperatorNS { class Mass; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ElasticityNS
{


    inline void VariationalFormulation ::UpdateDisplacementAndVelocity()
    {
        // Ordering of both calls is capital here!
        UpdateVelocity();
        UpdateDisplacement();
    }


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const noexcept
    {
        return nullptr;
    }


    inline const GlobalVector& VariationalFormulation ::GetVectorCurrentDisplacement() const noexcept
    {
        assert(!(!vector_current_displacement_));
        return *vector_current_displacement_;
    }


    inline GlobalVector& VariationalFormulation ::GetNonCstVectorCurrentDisplacement() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentDisplacement());
    }


    inline const GlobalVector& VariationalFormulation ::GetVectorCurrentVelocity() const noexcept
    {
        assert(!(!vector_current_velocity_));
        return *vector_current_velocity_;
    }


    inline GlobalVector& VariationalFormulation ::GetNonCstVectorCurrentVelocity() noexcept
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVelocity());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMatrixCurrentDisplacement() const noexcept
    {
        assert(!(!matrix_current_displacement_));
        return *matrix_current_displacement_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMatrixCurrentDisplacement() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixCurrentDisplacement());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMatrixCurrentVelocity() const noexcept
    {
        assert(!(!matrix_current_velocity_));
        return *matrix_current_velocity_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMatrixCurrentVelocity() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMatrixCurrentVelocity());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetMass() const noexcept
    {
        assert(!(!mass_));
        return *mass_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstMass() noexcept
    {
        return const_cast<GlobalMatrix&>(GetMass());
    }


    inline const GlobalMatrix& VariationalFormulation ::GetStiffness() const noexcept
    {
        assert(!(!stiffness_));
        return *stiffness_;
    }


    inline GlobalMatrix& VariationalFormulation ::GetNonCstStiffness() noexcept
    {
        return const_cast<GlobalMatrix&>(GetStiffness());
    }


    inline const GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor&
    VariationalFormulation ::GetStiffnessOperator() const noexcept
    {
        assert(!(!stiffness_operator_));
        return *stiffness_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation ::GetMassOperator() const noexcept
    {
        assert(!(!mass_operator_));
        return *mass_operator_;
    }


    inline const NumberingSubset& VariationalFormulation ::GetNumberingSubset() const noexcept
    {
        return numbering_subset_;
    }


    inline const VariationalFormulation::solid_scalar_parameter&
    VariationalFormulation ::GetVolumicMass() const noexcept
    {
        assert(!(!volumic_mass_));
        return *volumic_mass_;
    }


    inline const VariationalFormulation::solid_scalar_parameter&
    VariationalFormulation ::GetYoungModulus() const noexcept
    {
        assert(!(!young_modulus_));
        return *young_modulus_;
    }


    inline const VariationalFormulation::solid_scalar_parameter&
    VariationalFormulation ::GetPoissonRatio() const noexcept
    {
        assert(!(!poisson_ratio_));
        return *poisson_ratio_;
    }


    inline const Unknown& VariationalFormulation ::GetSolidDisplacement() const noexcept
    {
        return solid_displacement_;
    }


    inline const FEltSpace& VariationalFormulation ::GetMainFEltSpace() const noexcept
    {
        return main_felt_space_;
    }


    inline const FEltSpace& VariationalFormulation ::GetNeumannFEltSpace() const noexcept
    {
        return neumann_felt_space_;
    }


} // namespace MoReFEM::ElasticityNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_VARIATIONAL_FORMULATION_HXX_
