/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/Elasticity/Model.hpp"


namespace MoReFEM::ElasticityNS
{


    Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data), variational_formulation_(nullptr)
    { }


    void Model::SupplInitialize()
    {
        decltype(auto) god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();
        decltype(auto) numbering_subset_ptr =
            god_of_dof.GetNumberingSubsetPtr(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));
        const auto& numbering_subset = *numbering_subset_ptr;

        {
            decltype(auto) bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::sole), __FILE__, __LINE__) };

            decltype(auto) main_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
            decltype(auto) neumannn_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::neumann));
            decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);
            decltype(auto) unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::solid_displacement));

            variational_formulation_ = std::make_unique<variational_formulation_type>(morefem_data,
                                                                                      main_felt_space,
                                                                                      neumannn_felt_space,
                                                                                      unknown,
                                                                                      numbering_subset,
                                                                                      GetNonCstTimeManager(),
                                                                                      god_of_dof,
                                                                                      std::move(bc_list));
        }

        // Initialization here is also in charge of running the static case, and in restart mode
        // to load the data from the previous run.
        GetNonCstVariationalFormulation().Init(morefem_data);
    }


    void Model::Forward()
    {
        auto& formulation = this->GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the system.
        formulation.ComputeDynamicSystemRhs();
        const auto& numbering_subset = formulation.GetNumberingSubset();

        static bool is_first_call = true;

        if (is_first_call)
        {
            formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_matrix_and_rhs>(
                numbering_subset, numbering_subset);
            formulation.SolveLinear<IsFactorized::no>(numbering_subset, numbering_subset, __FILE__, __LINE__);
            is_first_call = false;
        } else
        {
            formulation.ApplyEssentialBoundaryCondition<VariationalFormulationNS::On::system_rhs>(numbering_subset,
                                                                                                  numbering_subset);

            formulation.SolveLinear<IsFactorized::yes>(numbering_subset, numbering_subset, __FILE__, __LINE__);
        }
    }


    void Model::SupplFinalizeStep()
    {
        // Update quantities for next iteration.
        auto& formulation = this->GetNonCstVariationalFormulation();
        const auto& numbering_subset = formulation.GetNumberingSubset();

        formulation.WriteSolution(this->GetTimeManager(), numbering_subset);
        formulation.UpdateDisplacementAndVelocity();
    }


    void Model::SupplFinalize() const
    { }


    void Model::SupplInitializeStep() const
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


} // namespace MoReFEM::ElasticityNS
