/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Domain/UniqueId.hpp"

#include "FormulationSolver/Crtp/VolumicAndSurfacicSource.hpp"


namespace MoReFEM::ElasticityNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t {
        mesh = 1 // only one mesh considered in current model!
    };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { highest_dimension = 1, neumann = 2, dirichlet, full_mesh };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { highest_dimension = 1, neumann = 2 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { solid_displacement = 1 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex { solver = 1 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { monolithic = 1 };


    //! \copydoc doxygen_hide_source_enum
    enum class SourceIndex { volumic = 1, surfacic = 2 };


    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t { sole = 1 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::sole),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann),

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Solid::VolumicMass::Value,
        InputDataNS::Solid::YoungModulus,
        InputDataNS::Solid::PoissonRatio,
        InputDataNS::Solid::PlaneStressStrain,

        MOST_USUAL_INPUT_DATA_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::volumic),
        MOST_USUAL_INPUT_DATA_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::surfacic),

        InputDataNS::Parallelism,
        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(NumberingSubsetIndex::monolithic),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(UnknownIndex::solid_displacement),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_MESH(MeshIndex::mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::neumann),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::dirichlet),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DOMAIN(DomainIndex::full_mesh),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(BoundaryConditionIndex::sole),
    
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::highest_dimension),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(FEltSpaceIndex::neumann),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::volumic),
        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(SourceIndex::surfacic),

        MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(SolverIndex::solver),

        InputDataNS::Solid::VolumicMass::Nature
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::model>;


} // namespace MoReFEM::ElasticityNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_INPUT_DATA_HPP_
