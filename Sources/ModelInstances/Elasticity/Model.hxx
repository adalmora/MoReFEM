/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 4 Mar 2014 18:27:50 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/Elasticity/Model.hpp"

#include <cassert>
#include <string>


namespace MoReFEM::ElasticityNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Elasticity");
        return name;
    }


    inline const Model::variational_formulation_type& Model::GetVariationalFormulation() const noexcept
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline Model::variational_formulation_type& Model::GetNonCstVariationalFormulation() noexcept
    {
        return const_cast<variational_formulation_type&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


} // namespace MoReFEM::ElasticityNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_ELASTICITY_x_MODEL_HXX_
