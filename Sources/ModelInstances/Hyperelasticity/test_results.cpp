/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE model_hyperelasticity

#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& solver_name = "");


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}


BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(seq_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart");
}


BOOST_FIXTURE_TEST_CASE(mpi4_3d_restart, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_partitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData");
}


#ifdef MOREFEM_WITH_MUMPS
BOOST_FIXTURE_TEST_CASE(sequential_mumps, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Mumps");
}


#if not defined(__APPLE__)
// Mumps no longer correctly supported on macOS in parallel; see #1705
BOOST_FIXTURE_TEST_CASE(mpi4_mumps, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "Mumps");
}
#endif
#endif // PetscDefined(HAVE_MUMPS)


BOOST_FIXTURE_TEST_CASE(sequential_gmres, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Gmres");
}

// \todo 1783 Investigate to make it work!
// BOOST_FIXTURE_TEST_CASE(parallel_gmres, TestNS::FixtureNS::TestEnvironment)
//{
//    CommonTestCase("Mpi4", "Gmres");
//}

#ifdef MOREFEM_WITH_MUMPS_UMFPACK
BOOST_FIXTURE_TEST_CASE(sequential_umfpack, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Umfpack");
}
#endif // PetscDefined(HAVE_UMFPACK)


BOOST_FIXTURE_TEST_CASE(sequential_petsc_solver, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "Petsc");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& solver_name)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{ "Sources", "ModelInstances", "Hyperelasticity", "ExpectedResults" },
            __FILE__,
            __LINE__);

        std::string model_name = "MidpointHyperelasticity";

        if (!solver_name.empty())
            model_name += "_with_" + solver_name;

        FilesystemNS::Directory obtained_dir(
            output_dir, std::vector<std::string>{ seq_or_par, "Ascii", model_name, "Rank_0" }, __FILE__, __LINE__);

        // No symbolic link to the special Lua file used as input in ExpectedResults (would need dedicated function
        // as produced one would still call it 'input_data.lua', but frankly it's probably not worth the time to write
        // it...
        if (solver_name.empty()
            && (!Utilities::String::EndsWith(seq_or_par, "_FromPrepartitionedData")
                && (!Utilities::String::EndsWith(seq_or_par, "_Restart"))))
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0; i <= 5; ++i)
        {
            oconv.str("");
            oconv << "displacement." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
        }
    }


} // namespace
