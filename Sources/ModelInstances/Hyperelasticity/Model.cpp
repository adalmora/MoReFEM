/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <iostream>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/Hyperelasticity/Model.hpp"


namespace MoReFEM::MidpointHyperelasticityNS
{


    Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        {
            decltype(auto) felt_space_list = god_of_dof.GetFEltSpaceList();

            std::ostringstream oconv;

            oconv << "SUMMARY FOR RANK " << GetMpi().GetRank<int>() << ": " << std::endl;

            for (const auto& felt_space_ptr : felt_space_list)
            {
                oconv << "\tFor FEltSpace " << felt_space_ptr->GetUniqueId()
                      << " Nproc = " << felt_space_ptr->GetProcessorWiseDofList().size()
                      << " and Nghost = " << felt_space_ptr->GetGhostDofList().size() << std::endl;
            }

            oconv << std::endl;

            std::cout << oconv.str();
        }

        decltype(auto) morefem_data = parent::GetMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        {
            const DirichletBoundaryConditionManager& bc_manager =
                DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::clamped), __FILE__, __LINE__) };

            const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

            variational_formulation_ =
                std::make_unique<VariationalFormulation>(morefem_data,
                                                         felt_space_volume.GetNumberingSubset(displacement),
                                                         GetNonCstTimeManager(),
                                                         god_of_dof,
                                                         std::move(bc_list));
        }

        // Exit the program if the 'precompute' mode was chosen.
        PrecomputeExit(morefem_data);

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        const auto& mpi = GetMpi();

        if (!parent::GetTimeManager().IsInRestartMode())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi, __FILE__, __LINE__);

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "----------------------------------------------\n", mpi, __FILE__, __LINE__);


            variational_formulation.SolveNonLinear(
                displacement_numbering_subset, displacement_numbering_subset, __FILE__, __LINE__);

            variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        }

        variational_formulation.PrepareDynamicRuns();
    }


    void Model::SupplInitializeStep()
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.SolveNonLinear(
            displacement_numbering_subset, displacement_numbering_subset, __FILE__, __LINE__);
    }


    void Model::SupplFinalizeStep()
    {
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.UpdateForNextTimeStep();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::MidpointHyperelasticityNS
