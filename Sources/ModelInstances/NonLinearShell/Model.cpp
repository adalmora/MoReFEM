/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 3 Feb 2017 11:26:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <utility>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/VectorialTransientSource.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/NonLinearShell/Model.hpp"


namespace MoReFEM::MidpointNonLinearShellNS
{


    Model::Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) morefem_data = parent::GetMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        {
            const DirichletBoundaryConditionManager& bc_manager =
                DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::clamped), __FILE__, __LINE__) };

            const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

            variational_formulation_ =
                std::make_unique<VariationalFormulation>(morefem_data,
                                                         felt_space_volume.GetNumberingSubset(displacement),
                                                         GetNonCstTimeManager(),
                                                         god_of_dof,
                                                         std::move(bc_list));
        }

        // Exit the program if the 'precompute' mode was chosen.
        PrecomputeExit(morefem_data);

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        const auto& mpi = GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "\n----------------------------------------------\n", mpi, __FILE__, __LINE__);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi, __FILE__, __LINE__);

        Wrappers::Petsc::PrintMessageOnFirstProcessor(
            "----------------------------------------------\n", mpi, __FILE__, __LINE__);

        variational_formulation.SolveStaticContinuation();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);

        const auto output_path = god_of_dof.GetOutputDirectoryForNumberingSubset(displacement_numbering_subset);

        const auto medit_sol_output_file_x = output_path.AddFile("shell_x.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_x, 0);
        const auto medit_sol_output_file_y = output_path.AddFile("shell_y.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_y, 1);
        const auto medit_sol_output_file_z = output_path.AddFile("shell_z.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_z, 2);

        variational_formulation.PrepareDynamicRuns();
    }


    void Model::SupplInitializeStep()
    { }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.SolveNonLinear(
            displacement_numbering_subset, displacement_numbering_subset, __FILE__, __LINE__);
    }


    void Model::SupplFinalizeStep()
    {
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.UpdateForNextTimeStep();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::MidpointNonLinearShellNS
