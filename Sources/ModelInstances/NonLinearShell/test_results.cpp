/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <filesystem>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE model_nonlinear_shell

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}

// To be enabled when ViZiR post processing will be properly handled in #1570.
BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    // CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_partitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    // CommonTestCase("Mpi4_FromPrepartitionedData");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);
        BOOST_CHECK(output_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{ "Sources", "ModelInstances", "NonLinearShell", "ExpectedResults" },
            __FILE__,
            __LINE__);

        FilesystemNS::Directory obtained_dir(
            output_dir,
            std::vector<std::string>{ seq_or_par, "Ascii", "NonLinearShell", "Rank_0" },
            __FILE__,
            __LINE__);

        // No symbolic link to the special Lua file used as input in ExpectedResults (would need dedicated function
        // as produced one would still call it 'input_data.lua', but frankly it's probably not worth the time to write
        // it...
        if (seq_or_par != "Mpi4_FromPrepartitionedData")
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);


        ref_dir.AddSubdirectory("NumberingSubset_1");
        obtained_dir.AddSubdirectory("NumberingSubset_1");

        TestNS::CompareDataFiles<MeshNS::Format::Vizir>(
            ref_dir, obtained_dir, "shell_x.sol", __FILE__, __LINE__, 1.e-7);
        TestNS::CompareDataFiles<MeshNS::Format::Vizir>(
            ref_dir, obtained_dir, "shell_y.sol", __FILE__, __LINE__, 1.e-7);
        TestNS::CompareDataFiles<MeshNS::Format::Vizir>(
            ref_dir, obtained_dir, "shell_z.sol", __FILE__, __LINE__, 1.e-7);
    }


} // namespace
