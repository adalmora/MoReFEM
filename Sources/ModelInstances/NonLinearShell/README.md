A dynamic NonLinear shell model with:
- Q2 Geometry
- Midpoint time scheme.
- CiarletGeymonat hyperelastic law.
