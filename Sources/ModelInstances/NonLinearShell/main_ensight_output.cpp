/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 17 Dec 2014 10:45:44 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include "Model/Main/MainEnsightOutput.hpp"

#include "ModelInstances/NonLinearShell/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::MidpointNonLinearShellNS;


int main(int argc, char** argv)
{
    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        NumberingSubsetIndex::displacement) };

    std::vector<std::string> unknown_list{ "displacement" };

    return ModelNS::MainEnsightOutput<MidpointNonLinearShellNS::Model>(
        argc, argv, AsMeshId(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
