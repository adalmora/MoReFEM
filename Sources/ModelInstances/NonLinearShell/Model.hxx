/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_NON_LINEAR_SHELL_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_NON_LINEAR_SHELL_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/NonLinearShell/Model.hpp"

#include <cassert>
#include <string>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::MidpointNonLinearShellNS { class VariationalFormulation; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::MidpointNonLinearShellNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("MidpointNonLinearShell");
        return name;
    }


    inline const VariationalFormulation& Model::GetVariationalFormulation() const noexcept
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation() noexcept
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false; // ie no additional condition
    }


} // namespace MoReFEM::MidpointNonLinearShellNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_NON_LINEAR_SHELL_x_MODEL_HXX_
