/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 17:19:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HXX_

// IWYU pragma: private, include "ModelInstances/Heat/VariationalFormulation.hpp"


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiGradPhi.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM::GlobalVariationalOperatorNS { class Mass; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HeatNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation::GetVolumicSourceOperator() const noexcept
    {
        assert(!(!volumic_source_operator_));
        return *volumic_source_operator_;
    }


    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation::GetConductivityOperator() const noexcept
    {
        assert(!(!conductivity_operator_));
        return *conductivity_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation ::GetCapacityOperator() const noexcept
    {
        assert(!(!capacity_operator_) && "Only exists in dynamic");
        return *capacity_operator_;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation::GetNeumannOperator() const noexcept
    {
        assert(!(!neumann_operator_));
        return *neumann_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass&
    VariationalFormulation ::GetRobinBilinearPartOperator() const noexcept
    {
        assert(!(!robin_bilinear_part_operator_));
        return *robin_bilinear_part_operator_;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation ::GetRobinLinearPartOperator() const noexcept
    {
        assert(!(!robin_linear_part_operator_));
        return *robin_linear_part_operator_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentVolumicSource() const
    {
        assert(!(!vector_current_volumic_source_));
        return *vector_current_volumic_source_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVolumicSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVolumicSource());
    }

    inline const GlobalVector& VariationalFormulation::GetVectorCurrentNeumann() const
    {
        assert(!(!vector_current_neumann_));
        return *vector_current_neumann_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentNeumann()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentNeumann());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentRobin() const
    {
        assert(!(!vector_current_robin_));
        return *vector_current_robin_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentRobin()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentRobin());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixConductivity() const
    {
        assert(!(!matrix_conductivity_));
        return *matrix_conductivity_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixConductivity()
    {
        return const_cast<GlobalMatrix&>(GetMatrixConductivity());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixCapacityPerTimeStep() const
    {
        assert(!(!matrix_capacity_));
        return *matrix_capacity_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixCapacityPerTimeStep()
    {
        return const_cast<GlobalMatrix&>(GetMatrixCapacityPerTimeStep());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixRobin() const
    {
        assert(!(!matrix_robin_));
        return *matrix_robin_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixRobin()
    {
        return const_cast<GlobalMatrix&>(GetMatrixRobin());
    }


    inline VariationalFormulation::StaticOrDynamic VariationalFormulation::GetStaticOrDynamic() const
    {
        return run_case_;
    }


    inline const NumberingSubset& VariationalFormulation::GetNumberingSubset() const
    {
        return numbering_subset_;
    }


    inline const ScalarParameter<>& VariationalFormulation::GetDiffusionDensity() const
    {
        assert(!(!density_));
        return *density_;
    }


    inline const ScalarParameter<>& VariationalFormulation::GetTransfertCoefficient() const
    {
        assert(!(!transfert_coefficient_));
        return *transfert_coefficient_;
    }


    inline const ScalarParameter<>& VariationalFormulation::GetDiffusionTensor() const
    {
        assert(!(!diffusion_tensor_));
        return *diffusion_tensor_;
    }


} // namespace MoReFEM::HeatNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_VARIATIONAL_FORMULATION_HXX_
