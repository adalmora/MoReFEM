/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 10 Apr 2018 17:53:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#define BOOST_TEST_MODULE model_heat

#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "1D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "1D");
}


BOOST_FIXTURE_TEST_CASE(sequential_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4", "2D");
}


BOOST_FIXTURE_TEST_CASE(sequential_restart_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "1D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_restart_1d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "1D");
}


BOOST_FIXTURE_TEST_CASE(sequential_restart_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq_Restart", "2D");
}


BOOST_FIXTURE_TEST_CASE(mpi4_restart_2d, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_Restart", "2D");
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par, std::string&& dimension)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        FilesystemNS::Directory root_dir(root_dir_path, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);

        BOOST_CHECK(root_dir.DoExist() == true);

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{ "Sources", "ModelInstances", "Heat", "ExpectedResults", dimension },
            __FILE__,
            __LINE__);

        FilesystemNS::Directory obtained_dir(
            output_dir,
            std::vector<std::string>{ seq_or_par, "Ascii", "Heat", dimension, "Rank_0" },
            __FILE__,
            __LINE__);

        BOOST_CHECK(output_dir.DoExist() == true);

        if (!Utilities::String::EndsWith(seq_or_par, "_Restart"))
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        std::ostringstream oconv;

        for (auto i = 0ul; i <= 20; ++i)
        {
            oconv.str("");
            oconv << "temperature." << std::setw(5) << std::setfill('0') << i << ".scl";
            TestNS::CompareDataFiles<MeshNS::Format::Ensight>(ref_dir, obtained_dir, oconv.str(), __FILE__, __LINE__);
        }
    }


} // namespace
