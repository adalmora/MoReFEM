/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 16:20:48 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <utility>

#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "ModelInstances/Heat/Model.hpp"


namespace MoReFEM::HeatNS
{


    Model::Model(const morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const GodOfDof& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();
        const NumberingSubset& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        {
            const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance(__FILE__, __LINE__);

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                                   AsBoundaryConditionId(BoundaryConditionIndex::first), __FILE__, __LINE__),
                               bc_manager.GetDirichletBoundaryConditionPtr(
                                   AsBoundaryConditionId(BoundaryConditionIndex::second), __FILE__, __LINE__) };

            variational_formulation_ = std::make_unique<VariationalFormulation>(
                morefem_data, numbering_subset, GetNonCstTimeManager(), god_of_dof, std::move(bc_list));
        }

        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();

        using TimeManager = InputDataNS::TimeManager;

        decltype(auto) input_data = morefem_data.GetInputData();

        double time_max = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeMax>::Value(input_data);

        if (NumericNS::IsZero(time_max))
            variational_formulation.SetStaticOrDynamic(VariationalFormulation::StaticOrDynamic::static_case);
        else
            variational_formulation.SetStaticOrDynamic(VariationalFormulation::StaticOrDynamic::dynamic_case);

        variational_formulation.Init(morefem_data);
        variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
    }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();

        // Only Rhs is modified at each time iteration; compute it and solve the system.
        variational_formulation.ComputeDynamicSystemRhs();
        variational_formulation.CallSolver();
    }


    void Model::SupplFinalizeStep()
    {
        // Update quantities for next iteration.
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& numbering_subset = variational_formulation.GetNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), numbering_subset);
    }


    void Model::SupplFinalize() const
    { }


    void Model::SupplInitializeStep()
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


} // namespace MoReFEM::HeatNS
