/*!
 * \file
 */

#include "Utilities/InputData/ModelSettings.hpp"

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "ModelInstances/Heat/InputData.hpp"


namespace MoReFEM::HeatNS
{


    void ModelSettings::Init()
    {
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
                "Monolithic numbering subset");
            Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>::DoMoveMesh>(false);
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::temperature)>>({ "Temperature" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::temperature)>::Name>("temperature");

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::temperature)>::Nature>("scalar");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
                { "Highest dimension geometric elements" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>>(
                { "Domain upon which Neumann boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::robin)>>(
                { "Domain upon which Robin boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_1)>>(
                { "Domain upon which first Dirichlet boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_2)>>(
                { "Domain upon which second Dirichlet boundary condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
                { "Covers the while mesh" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::neumann)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_1)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet_2)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::robin)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
                { "Finite element space for highest geometric dimension" });
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>>(
                { "Finite element space for Neumann boundary condition" });
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>>(
                { "Finite element space for Robin boundary condition" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::highest_dimension));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::UnknownList>(
                { "temperature" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::neumann));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::UnknownList>({ "temperature" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::neumann)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::robin));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>::UnknownList>({ "temperature" });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::robin)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>>(
                { "First Dirichlet boundary condition" });
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::second)>>(
                { "Second Dirichlet boundary condition" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::UnknownName>("temperature");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::DomainIndex>(EnumUnderlyingType(DomainIndex::dirichlet_1));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>::IsMutable>(
                false);

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::second)>::UnknownName>("temperature");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::second)>::DomainIndex>(EnumUnderlyingType(DomainIndex::dirichlet_2));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::second)>::IsMutable>(
                false);
        }

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole mesh" });


        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Linear solver" });

        SetDescription<InputDataNS::Diffusion::Tensor<EnumUnderlyingType(TensorIndex::diffusion_tensor)>>(
            { "Diffusion tensor" });


        SetDescription<InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::volumic_source)>>(
            { "Volumic source" });
        SetDescription<
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::neumann_boundary_condition)>>(
            { "Neumann boundary condition" });
        SetDescription<
            InputDataNS::ScalarTransientSource<EnumUnderlyingType(ForceIndexList::robin_boundary_condition)>>(
            { "Robin boundary condition" });

        SetDescription<InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::temperature)>>(
            { "Initial condition for temperature" });
    }


} // namespace MoReFEM::HeatNS
