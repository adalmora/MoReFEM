/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/Heat/Model.hpp"


#include <cassert>
#include <string>

#include "ModelInstances/Heat/VariationalFormulation.hpp"


namespace MoReFEM::HeatNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Heat model");
        return name;
    }


    inline const VariationalFormulation& Model::GetVariationalFormulation() const
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        const VariationalFormulation& formulation = GetVariationalFormulation();

        // In the static case no loop time so HasFinished is set at true to avoid the while loop
        if (formulation.GetStaticOrDynamic() == VariationalFormulation::StaticOrDynamic::static_case)
            return true;
        else
            return false; // ie no additional condition
    }


} // namespace MoReFEM::HeatNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_HEAT_x_MODEL_HXX_
