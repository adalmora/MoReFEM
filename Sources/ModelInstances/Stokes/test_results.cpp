/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <filesystem>
#include <string>
#include <vector>

#define BOOST_TEST_MODULE model_stokes

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ModelInstances/Stokes/Environment.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"
#include "Test/Tools/CompareDataFiles.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par);


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp" // IWYU pragma: keep


BOOST_FIXTURE_TEST_CASE(sequential, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Seq");
}


BOOST_FIXTURE_TEST_CASE(mpi4, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4");
}


BOOST_FIXTURE_TEST_CASE(mpi4_from_prepartitioned_data, TestNS::FixtureNS::TestEnvironment)
{
    CommonTestCase("Mpi4_FromPrepartitionedData");
}

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    void CommonTestCase(std::string&& seq_or_par)
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        std::string root_dir_path, output_dir_path;

        /* BOOST_REQUIRE_NO_THROW */ (root_dir_path =
                                          environment.GetEnvironmentVariable("MOREFEM_ROOT", __FILE__, __LINE__));

        // Really a hack here: in the CMake file value ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}
        // has been given for this environment variable.
        /* BOOST_REQUIRE_NO_THROW */ (
            output_dir_path = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__));

        FilesystemNS::Directory root_dir(std::filesystem::path{ root_dir_path }, FilesystemNS::behaviour::read);

        FilesystemNS::Directory output_dir(std::filesystem::path{ output_dir_path }, FilesystemNS::behaviour::read);

        BOOST_REQUIRE_NO_THROW(root_dir.ActOnFilesystem(__FILE__, __LINE__));
        BOOST_REQUIRE_NO_THROW(output_dir.ActOnFilesystem(__FILE__, __LINE__));

        FilesystemNS::Directory ref_dir(
            root_dir,
            std::vector<std::string>{ "Sources", "ModelInstances", "Stokes", "ExpectedResults" },
            __FILE__,
            __LINE__);

        static bool first_call = true;

        if (first_call)
        {
            StokesNS::SetStokesConfiguration();
            first_call = false;
        }

        FilesystemNS::Directory obtained_dir(
            output_dir,
            std::vector<std::string>{ seq_or_par,
                                      "Ascii",
                                      "Stokes",
                                      environment.GetEnvironmentVariable("MOREFEM_STOKES_N_OP", __FILE__, __LINE__),
                                      "Rank_0" },
            __FILE__,
            __LINE__);

        if (seq_or_par != "Mpi4_FromPrepartitionedData")
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "input_data.lua", __FILE__, __LINE__);

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "model_name.hhdata", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "unknowns.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Mesh_1");
        obtained_dir.AddSubdirectory("Mesh_1");

        if (seq_or_par == "Seq") // in parallel one file per rank with only processor-related data...
            TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "interfaces.hhdata", __FILE__, __LINE__);

        ref_dir.AddSubdirectory("Ensight6");
        obtained_dir.AddSubdirectory("Ensight6");

        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "mesh.geo", __FILE__, __LINE__);
        TestNS::CheckIdenticalFiles(ref_dir, obtained_dir, "problem.case", __FILE__, __LINE__);

        TestNS::CompareDataFiles<MeshNS::Format::Ensight>(
            ref_dir, obtained_dir, "pressure.00000.scl", __FILE__, __LINE__, 1.e-12);
        TestNS::CompareDataFiles<MeshNS::Format::Ensight>(
            ref_dir, obtained_dir, "velocity.00000.scl", __FILE__, __LINE__, 1.e-8);
    }


} // namespace
