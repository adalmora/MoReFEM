/*!
// \file
//
//
*/

#ifndef MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_ENVIRONMENT_HPP_
#define MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_ENVIRONMENT_HPP_

#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM::StokesNS
{

    /*!
     * \brief Set an environment variable to enable both configurations: with Stokes operator or with two separate ones.
     */
    inline void SetStokesConfiguration()
    {

        auto& environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

#ifdef MOREFEM_STOKES_MODEL_WITH_TWO_OPERATORS
        environment.SetEnvironmentVariable(std::make_pair("MOREFEM_STOKES_N_OP", "TwoOperators"), __FILE__, __LINE__);
#else
        environment.SetEnvironmentVariable(std::make_pair("MOREFEM_STOKES_N_OP", "OneOperator"), __FILE__, __LINE__);
#endif
    }

} // namespace MoReFEM::StokesNS

#endif // MOREFEM_x_MODEL_INSTANCES_x_STOKES_x_ENVIRONMENT_HPP_
