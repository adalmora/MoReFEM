/*!
 * \file
 */

#include "Utilities/InputData/ModelSettings.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialCondition/InitialCondition.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "ModelInstances/Laplacian/InputData.hpp"

namespace MoReFEM::LaplacianNS
{


    void ModelSettings::Init()
    {
        // ****** Numbering subset ******
        {
            SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>>(
                "Monolithic numbering subset");
            Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>::DoMoveMesh>(false);
        }

        // ****** Unknown ******
        {
            SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>>({ "pressure" });

            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::Name>("pressure");
            Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::Nature>("scalar");
        }

        // ****** Domain ******
        {
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>(
                { "Encompass of geometric element of the highest available dimension" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>(
                { "Domain upon which Dirichlet condition is applied" });
            SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>(
                { "Domain that covers the whole mesh" });

            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
            Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>::MeshIndexList>(
                { EnumUnderlyingType(MeshIndex::mesh) });
        }

        // ****** Dirichlet boundary condition ******
        {
            SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>>(
                { "Sole boundary condition" });

            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::UnknownName>("pressure");
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
                BoundaryConditionIndex::first)>::DomainIndex>(EnumUnderlyingType(DomainIndex::dirichlet));
            Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::first)>::IsMutable>(
                false);
        }


        // ****** Finite element space ******
        {
            SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>>(
                { "Finite element space for highest geometric dimension" });

            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::GodOfDofIndex>(
                EnumUnderlyingType(MeshIndex::mesh));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::DomainIndex>(
                EnumUnderlyingType(DomainIndex::volume));
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::NumberingSubsetList>(
                { EnumUnderlyingType(NumberingSubsetIndex::monolithic) });
            Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::UnknownList>({ "pressure" });
        }


        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Mesh" });

        SetDescription<InputDataNS::ScalarTransientSource<EnumUnderlyingType(SourceIndexList::volumic_source)>>(
            { "Volumic source" });

        SetDescription<InputDataNS::InitialCondition<EnumUnderlyingType(InitialConditionIndex::initial_condition)>>(
            { "Initial condition" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Linear solver" });
    }


} // namespace MoReFEM::LaplacianNS
