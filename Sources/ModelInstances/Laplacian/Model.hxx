/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HXX_
#define MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HXX_

// IWYU pragma: private, include "ModelInstances/Laplacian/Model.hpp"

#include <cassert>
#include <string>


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::LaplacianNS { class VariationalFormulation; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::LaplacianNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Laplacian");
        return name;
    }


    inline const VariationalFormulation& Model::GetVariationalFormulation() const
    {
        assert(!(!variational_formulation_));
        return *variational_formulation_;
    }


    inline VariationalFormulation& Model::GetNonCstVariationalFormulation()
    {
        return const_cast<VariationalFormulation&>(GetVariationalFormulation());
    }


    inline bool Model::SupplHasFinishedConditions() const
    {
        return false;
    }


    inline void Model::SupplInitializeStep()
    { }


} // namespace MoReFEM::LaplacianNS


#endif // MOREFEM_x_MODEL_INSTANCES_x_LAPLACIAN_x_MODEL_HXX_
