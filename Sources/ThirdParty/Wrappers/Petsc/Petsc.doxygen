/*!
 * \class doxygen_hide_snes_functions_args

* \param[in] snes_function If linear solve, just give nullptr. If non linear solve, give here the function
 * evaluation routine. See [Petsc documentation](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESSetFunction.html)
 * for more details.
 * \param[in] snes_jacobian If non linear solve, give here the function that computes the jacobian. See [Petsc
 * documentation](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESSetJacobian.html)
 * for more details.
 * \param[in] snes_viewer If non linear solve, give here the function that may print some information at each
 * iteration step. See [Petsc documentation](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESMonitorSet.html)
 * for more details.
 * \param[in] snes_convergence_test_function If non linear solve, give here the functional form used for testing of
 *  convergence of nonlinear solver. See [Petsc documentation](http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESConvergenceTestFunction.html)
 * for more details.
 */
