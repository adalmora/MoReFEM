/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 15:45:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_


#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    //! Convenient enum class to tag the type of solver.
    enum class solver_type { direct, iterative };


    /*!
     * \brief Enum to say whether a Newton converged or not.
     *
     * 'pending' means the Newton is still in progress. I don't know how it may happen, as Petsc doc says
     *  SNESGetConvergedReason() can only be called once SNESSolve() is complete, but as it exists in Petsc
     * enum I retranscribe it here.
     * 'unspecified' is there for SNES_CONVERGED_ITS: this is obtained when the maximum number of iterations
     * is reached but no convergence test was performed. I put it if at some point we need to introduce
     * a wrapper over SNESConvergedSkip(); in current state it should never happen.
     */
    enum class convergence_status { yes, no, pending, unspecified };


    //! Strong type,
    // clang-format off
    using absolute_tolerance_type =
    StrongType
    <
        PetscReal,
        struct AbsoluteToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using relative_tolerance_type =
    StrongType
    <
        PetscReal,
        struct RelativeToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using set_restart_type =
    StrongType
    <
        PetscInt,
        struct SetRestartTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using max_iteration_type =
    StrongType
    <
        PetscInt,
        struct MaxIterationTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using preconditioner_type =
    StrongType
    <
        std::string,
        struct PreconditionerTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using step_size_tolerance_type =
    StrongType
    <
        PetscReal,
        struct StepSizeToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using solver_name_type =
    StrongType
    <
        std::string,
        struct SolverNameTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using preconditioner_name_type =
    StrongType
    <
        std::string,
        struct PreconditionerNameTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


} // namespace MoReFEM::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ENUM_HPP_
