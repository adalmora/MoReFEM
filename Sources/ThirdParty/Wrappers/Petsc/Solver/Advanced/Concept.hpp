/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ADVANCED_x_CONCEPT_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ADVANCED_x_CONCEPT_HPP_

#include "Utilities/InputData/Concept.hpp" // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Advanced::Concept::InputDataNS
{


    /*!
     * \brief Defines a concept to identify a type is a section regarding PETSc settings.
     *
     * InputDataNS::Petsc which is defined in Core sub-library is expected to follow this concept.
     *
     */
    template<typename T>
    concept SolverSectionType = requires {
        {
            Advanced::Concept::InputDataNS::SectionType<T>, T::ConceptIsSolverSection == true
        };
    };

} // namespace MoReFEM::Advanced::Concept::InputDataNS

/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_ADVANCED_x_CONCEPT_HPP_
