/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"

#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Advanced/Concept.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    template<Advanced::Concept::InputDataNS::SolverSectionType SectionT>
    Settings::Settings(const SectionT& section)
    : Settings(::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::AbsoluteTolerance>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::RelativeTolerance>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::GmresRestart>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::MaxIteration>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::Preconditioner>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::Solver>(section),
               ::MoReFEM::InputDataNS::ExtractLeafFromSection<typename SectionT::StepSizeTolerance>(section))
    { }

    inline ::MoReFEM::Wrappers::Petsc::absolute_tolerance_type Settings::GetAbsoluteTolerance() const noexcept
    {
        return absolute_tolerance_;
    }


    inline ::MoReFEM::Wrappers::Petsc::relative_tolerance_type Settings::GetRelativeTolerance() const noexcept
    {
        return relative_tolerance_;
    }


    inline ::MoReFEM::Wrappers::Petsc::set_restart_type Settings::GetRestart() const noexcept
    {
        return set_restart_;
    }


    inline ::MoReFEM::Wrappers::Petsc::max_iteration_type Settings::GetMaxIterations() const noexcept
    {
        return max_iteration_;
    }


    inline ::MoReFEM::Wrappers::Petsc::preconditioner_name_type Settings::GetPreconditionerName() const noexcept
    {
        return preconditioner_name_;
    }


    inline ::MoReFEM::Wrappers::Petsc::solver_name_type Settings::GetSolverName() const noexcept
    {
        return solver_name_;
    }


    inline ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type Settings::GetStepSizeTolerance() const noexcept
    {
        return step_size_tolerance_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HXX_
