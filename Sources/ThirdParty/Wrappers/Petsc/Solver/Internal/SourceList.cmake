### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/BasicSolver.cpp
		${CMAKE_CURRENT_LIST_DIR}/Factory.cpp
		${CMAKE_CURRENT_LIST_DIR}/Settings.cpp
		${CMAKE_CURRENT_LIST_DIR}/Solver.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/BasicSolver.hpp
		${CMAKE_CURRENT_LIST_DIR}/Factory.hpp
		${CMAKE_CURRENT_LIST_DIR}/Factory.hxx
		${CMAKE_CURRENT_LIST_DIR}/Settings.hpp
		${CMAKE_CURRENT_LIST_DIR}/Settings.hxx
		${CMAKE_CURRENT_LIST_DIR}/Solver.hpp
		${CMAKE_CURRENT_LIST_DIR}/Solver.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Convergence/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Instantiations/SourceList.cmake)
