/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <cassert>
#include <functional>
#include <map>
#include <memory>
#include <utility>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    Factory::~Factory() = default;


    const std::string& Factory::ClassName()
    {
        static std::string ret("SolverFactory");
        return ret;
    }


    Factory::Factory() = default;


    Solver::unique_ptr Factory::Create(Settings&& solver_settings) const
    {
        const auto& solver_name_object = solver_settings.GetSolverName();
        const auto solver_name = solver_name_object.Get();

        auto it = callbacks_.find(solver_name);

        if (it == callbacks_.cend())
            throw ExceptionNS::Factory::UnregisteredName(solver_name, "solver", __FILE__, __LINE__);

        Solver::unique_ptr ret = it->second(std::move(solver_settings));

        if (ret == nullptr)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::SolverNotSetUp(solver_name_object, __FILE__, __LINE__);

        return ret;
    }


    namespace // anonymous
    {

        auto ReturnNullptr(Internal::Wrappers::Petsc::SolverNS::Settings&&)
        {
            return nullptr;
        }

    } // namespace


    bool Factory::RegisterUnavailableSolver(const std::string& solver_name)
    {
        CallBack::value_type item = std::make_pair(solver_name, ReturnNullptr);

        auto [iterator, is_inserted] = callbacks_.insert(item);

        if (!is_inserted)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(solver_name, "solver", __FILE__, __LINE__));

        return true;
    }


    std::vector<std::string> Factory::GenerateSolverList() const
    {
        std::vector<std::string> ret;
        ret.reserve(callbacks_.size());

        for (const auto& [name, callback_function] : callbacks_)
            ret.push_back(name);

        assert(ret.size() == callbacks_.size());

        return ret;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


/// @} // addtogroup ThirdPartyGroup
