/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 15:45:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    Solver::~Solver() = default;


    Solver::Solver(solver_type type, parallel_support is_parallel_supported, SolverNS::Settings&& solver_settings)
    : type_(type), is_parallel_supported_(is_parallel_supported), settings_(std::move(solver_settings))
    { }


    auto Solver::GetSolverType() const noexcept -> solver_type
    {
        return type_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup
