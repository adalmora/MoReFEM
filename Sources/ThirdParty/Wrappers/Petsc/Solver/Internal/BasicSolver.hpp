/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 17:41:39 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_BASIC_SOLVER_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_BASIC_SOLVER_HPP_

#include <memory>
#include <string>
#include <string_view>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::Wrappers::Petsc { class Snes; } // IWYU pragma: keep
namespace MoReFEM::Wrappers { class Mpi; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    /*!
     * \brief Polymorphic base class over a solver used in Petsc with default settings.
     *
     * This abstract class defines all the virtual methods (save the one related to the destructor) to empty content.
     *
     */
    class BasicSolver : public Solver
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = BasicSolver;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] type An enum in MoReFEM which tells which kind of solver is considered (direct or
        //! iterative).
        //! \copydoc doxygen_hide_solver_settings_param
        //!
        //! \copydoc doxygen_hide_solver_is_parallel_supported
        //!
        //! \param[in] petsc_name Macro that holds the name of the solver in PETSc, e.g. 'MATSOLVERMUMPS'
        explicit BasicSolver(solver_type type,
                             parallel_support is_parallel_supported,
                             SolverNS::Settings&& solver_settings,
                             std::string_view petsc_name);

        //! Destructor.
        virtual ~BasicSolver() override = 0;

        //! \copydoc doxygen_hide_copy_constructor
        BasicSolver(const BasicSolver& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        BasicSolver(BasicSolver&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        BasicSolver& operator=(const BasicSolver& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        BasicSolver& operator=(BasicSolver&& rhs) = delete;

        ///@}

        //! Set the options that are specific to the solver in SolveLinear().
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_invoking_file_and_line
        virtual void SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

        //! Set the options that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_invoking_file_and_line
        virtual void SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

        //! Print informtations after solve that are specific to the solver.
        //! \param[in,out] snes The MoReFEM object which wraps a Petsc solver.
        //! * \copydoc doxygen_hide_invoking_file_and_line
        virtual void SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const override;

        //! \copydoc doxygen_hide_petsc_solver_name
        virtual const std::string& GetPetscName() const noexcept override;

      private:
        //! Petsc name
        std::string petsc_name_;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_BASIC_SOLVER_HPP_
