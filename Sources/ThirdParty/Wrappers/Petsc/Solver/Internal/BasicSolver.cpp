/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <utility>

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/BasicSolver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::Wrappers::Petsc { class Snes; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc
{


    BasicSolver::~BasicSolver() = default;


    BasicSolver::BasicSolver(Solver::solver_type type,
                             parallel_support is_parallel_supported,
                             SolverNS::Settings&& solver_settings,
                             std::string_view petsc_name)
    : Solver(type, is_parallel_supported, std::move(solver_settings)), petsc_name_{ petsc_name }
    { }


    void BasicSolver::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }

    void BasicSolver::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }

    void BasicSolver::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    const std::string& BasicSolver::GetPetscName() const noexcept
    {
        return petsc_name_;
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup
