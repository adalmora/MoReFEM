/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 21:30:28 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <string> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Umfpack.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string UmfpackName()
        {
            return "Umfpack";
        }


#if not defined(MOREFEM_WITH_UMFPACK)

        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).RegisterUnavailableSolver(UmfpackName());

#else // if not defined (MOREFEM_WITH_UMFPACK)

#if not PetscDefined(HAVE_SUITESPARSE) // not a typo!
        static_assert(
            false,
            "If MOREFEM_WITH_UMFPACK is set to true, PETSc should have been installed with this solver configured!"
            " (typically with an option such as --download-suitesparse)");
#endif

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Umfpack>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).Register<Umfpack>(Create);

#endif // if not defined (MOREFEM_WITH_UMFPACK)

    } // namespace


#ifdef MOREFEM_WITH_UMFPACK


    const std::string& Umfpack::Name()
    {
        static std::string ret{ UmfpackName() };
        return ret;
    }


    Umfpack::Umfpack(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::no, std::move(solver_settings))
    { }


    void Umfpack::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    void Umfpack::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    void Umfpack::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    const std::string& Umfpack::GetPetscName() const noexcept
    {
        static std::string ret(MATSOLVERUMFPACK);
        return ret;
    }

#endif // MOREFEM_WITH_UMFPACK


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


/// @} // addtogroup ThirdPartyGroup
