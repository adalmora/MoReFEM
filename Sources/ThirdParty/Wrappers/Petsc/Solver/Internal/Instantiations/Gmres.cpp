/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 2 Nov 2015 21:30:28 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
#include <utility>

// IWYU pragma: no_include <iosfwd>
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Gmres.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Gmres>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).Register<Gmres>(Create);

    } // namespace


    const std::string& Gmres::Name()
    {
        static std::string ret("Gmres");
        return ret;
    }


    Gmres::Gmres(SolverNS::Settings&& solver_settings)
    : parent(solver_type::iterative, parallel_support::yes, std::move(solver_settings))
    { }


    void Gmres::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    void Gmres::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        auto ksp = snes.GetKsp(invoking_file, invoking_line);

        auto error_code = KSPGMRESSetRestart(ksp, GetRestart());

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(
                error_code, "KSPGMRESSetRestart", invoking_file, invoking_line);
    }


    void Gmres::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    const std::string& Gmres::GetPetscName() const noexcept
    {
        static std::string ret(KSPGMRES);
        return ret;
    }


    PetscInt Gmres::GetRestart() const noexcept
    {
        return static_cast<PetscInt>(GetSettings().GetRestart().Get());
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


/// @} // addtogroup ThirdPartyGroup
