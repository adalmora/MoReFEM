//! \file
//
//
//  SuperLU_dist.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 08/02/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#include <string> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/SuperLU_dist.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string SuperLU_distName()
        {
            return "SuperLU_dist";
        }


#if not defined(MOREFEM_WITH_SUPERLU_DIST)

        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).RegisterUnavailableSolver(SuperLU_distName());

#else // if not defined (MOREFEM_WITH_SUPERLU_DIST)

#if not PetscDefined(HAVE_SUPERLU_DIST)
        static_assert(
            false,
            "If MOREFEM_WITH_SUPERLU_DIST is set to true, PETSc should have been installed with this solver configured!"
            " (typically with an option such as --download-superlu_dist)");
#endif

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<SuperLU_dist>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).Register<SuperLU_dist>(Create);

#endif // if not defined (MOREFEM_WITH_SUPERLU_DIST)

    } // namespace

#ifdef MOREFEM_WITH_SUPERLU_DIST

    const std::string& SuperLU_dist::Name()
    {
        static std::string ret{ SuperLU_distName() };
        return ret;
    }


    SuperLU_dist::SuperLU_dist(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::yes, std::move(solver_settings))
    { }


    void SuperLU_dist::SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    void SuperLU_dist::SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line)
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    void SuperLU_dist::SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const
    {
        static_cast<void>(snes);
        static_cast<void>(invoking_file);
        static_cast<void>(invoking_line);
    }


    const std::string& SuperLU_dist::GetPetscName() const noexcept
    {
        static std::string ret(MATSOLVERSUPERLU_DIST);
        return ret;
    }

#endif // MOREFEM_WITH_SUPERLU_DIST

} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
