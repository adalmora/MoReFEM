//! \file
//
//
//  MoReFEM
//
//  Created by Sébastien Gilles
// Copyright © 2023 Inria. All rights reserved.
//

#include <string> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Petsc.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string PetscName()
        {
            return "Petsc";
        }


        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Petsc>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered =
            SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__).Register<Petsc>(Create);


    } // namespace


    Petsc::~Petsc() = default;


    const std::string& Petsc::Name()
    {
        static std::string ret{ PetscName() };
        return ret;
    }


    Petsc::Petsc(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::no, std::move(solver_settings), MATSOLVERPETSC)
    { }


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
