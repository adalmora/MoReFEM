//! \file
//
//
//  SuperLU_dist.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 08/02/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_SUPER_L_U_xDIST_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_SUPER_L_U_xDIST_HPP_

#ifdef MOREFEM_WITH_SUPERLU_DIST

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    /*!
     * \brief Wrappers over SuperLU_dist solver within Petsc.
     */
    class SuperLU_dist final : public Internal::Wrappers::Petsc::Solver
    {

      public:
        //! Alias to parent.
        using parent = Internal::Wrappers::Petsc::Solver;

        //! \copydoc doxygen_hide_alias_self
        using self = SuperLU_dist;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name
        static const std::string& Name();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \copydoc doxygen_hide_solver_settings_param
        explicit SuperLU_dist(SolverNS::Settings&& solver_settings);

        //! Destructor.
        ~SuperLU_dist() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        SuperLU_dist(const SuperLU_dist& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SuperLU_dist(SuperLU_dist&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SuperLU_dist& operator=(const SuperLU_dist& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SuperLU_dist& operator=(SuperLU_dist&& rhs) = delete;

        ///@}

      private:
        /*!
         * \copydoc doxygen_hide_solver_set_solve_linear_option
         *
         */
        void SetSolveLinearOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

        /*!
         * \copydoc doxygen_hide_solver_suppl_init_option
         *
         * Currently nothing is done at this stage for SuperLU_dist solver.
         */
        void SupplInitOptions(Snes& snes, const char* invoking_file, int invoking_line) override;

        /*!
         * \copydoc doxygen_hide_solver_print_infos
         *
         */
        void SupplPrintSolverInfos(Snes& snes, const char* invoking_file, int invoking_line) const override;

        //! \copydoc doxygen_hide_petsc_solver_name
        const std::string& GetPetscName() const noexcept override;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations

#endif // MOREFEM_WITH_SUPERLU_DIST

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_SUPER_L_U_xDIST_HPP_
