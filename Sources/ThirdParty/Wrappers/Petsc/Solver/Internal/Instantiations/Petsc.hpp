//! \file
//
//
//  Petsc.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 08/02/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_PETSC_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_PETSC_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/BasicSolver.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS { class Settings; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    /*!
     * \brief Wrappers over Petsc solver within Petsc.
     */
    class Petsc final : public Internal::Wrappers::Petsc::BasicSolver
    {

      public:
        //! Alias to parent.
        using parent = Internal::Wrappers::Petsc::BasicSolver;

        //! \copydoc doxygen_hide_alias_self
        using self = Petsc;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name
        static const std::string& Name();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \copydoc doxygen_hide_solver_settings_param
        explicit Petsc(SolverNS::Settings&& solver_settings);

        //! Destructor.
        ~Petsc() override;

        //! \copydoc doxygen_hide_copy_constructor
        Petsc(const Petsc& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Petsc(Petsc&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Petsc& operator=(const Petsc& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Petsc& operator=(Petsc&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_INSTANTIATIONS_x_PETSC_HPP_
