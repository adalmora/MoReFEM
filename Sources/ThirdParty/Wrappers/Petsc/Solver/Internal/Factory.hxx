/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_FACTORY_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_FACTORY_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"

#include <utility>

#include "Utilities/Exceptions/Factory.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    template<class SolverT>
    bool Factory::Register(FunctionPrototype callback)
    {
        if (!callbacks_.insert(std::make_pair(SolverT::Name(), callback)).second)
        {
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(SolverT::Name(), "solver", __FILE__, __LINE__));
        }

        return true;
    }


    inline auto Factory::Nvariable() const -> CallBack::size_type
    {
        return callbacks_.size();
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_FACTORY_HXX_
