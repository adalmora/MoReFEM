/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    Settings::~Settings() = default;


    Settings::Settings(::MoReFEM::Wrappers::Petsc::absolute_tolerance_type absolute_tolerance,
                       ::MoReFEM::Wrappers::Petsc::relative_tolerance_type relative_tolerance,
                       ::MoReFEM::Wrappers::Petsc::set_restart_type set_restart,
                       ::MoReFEM::Wrappers::Petsc::max_iteration_type max_iteration,
                       ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name,
                       ::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                       ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type step_size_tolerance)
    : absolute_tolerance_(absolute_tolerance), relative_tolerance_(relative_tolerance), set_restart_(set_restart),
      max_iteration_(max_iteration), preconditioner_name_(preconditioner_name), solver_name_(solver_name),
      step_size_tolerance_(step_size_tolerance)
    { }


    Settings::Settings(::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                       ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name)
    : preconditioner_name_(preconditioner_name), solver_name_(solver_name)
    { }

} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


/// @} // addtogroup ThirdPartyGroup
