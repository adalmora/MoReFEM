/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HPP_

#include "Utilities/InputData/Concept.hpp" // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Advanced/Concept.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS
{


    /*!
     * \brief All the settings related to a PETSc solver, as typically read from the input data file.
     *
     * Please notice this class is not in charge of checking the consistency of values provided: for instance if the
     * solver name doesn't match any know by PETSc it doesn't care (but it is of course handled properly elsewhere - in
     * the SolverNS::Factory in this specific case).
     */
    class Settings final
    {

      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_solver_preconditioner_names_param
         *
         * \param[in] solver_name Name of the solver to use. Must be one managed by
         * \a MoReFEM::Internal::Wrappers::Petsc::SolverNS::Factory, but the check is performed when
         * an actual solver is to be created by the factory, not by present class.
         * \param[in] preconditioner_name Same as \a solver_name for the preconditioner.
         * Must be 'PCLU' for a direct solver - an exception enforces this,
         */

        /*!
         * \brief Constructor.
         *
         * \param[in] absolute_tolerance Absolute tolerance used by PETSc solver.
         * \param[in] relative_tolerance Relative tolerance used by PETSc solver.
         * \param[in] set_restart Number of iterations at which iterative solver (typically GMRes) restarts.
         * \param[in] step_size_tolerance Convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
         * \param[in] max_iteration Maximum number of iterations.
         * \copydoc doxygen_hide_solver_preconditioner_names_param
         *
         * You may find more explanations on PETSc documentation, especially on the following pages:
         * - https://petsc.org/release/docs/manualpages/KSP/KSPSetTolerances
         * - https://petsc.org/release/docs/manualpages/SNES/SNESSetTolerances
         * - https://petsc.org/release/docs/manualpages/KSP/KSPGMRESSetRestart
         * - https://petsc.org/release/overview/linear_solve_table
         */
        explicit Settings(::MoReFEM::Wrappers::Petsc::absolute_tolerance_type absolute_tolerance,
                          ::MoReFEM::Wrappers::Petsc::relative_tolerance_type relative_tolerance,
                          ::MoReFEM::Wrappers::Petsc::set_restart_type set_restart,
                          ::MoReFEM::Wrappers::Petsc::max_iteration_type max_iteration,
                          ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name,
                          ::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                          ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type step_size_tolerance);


        /*!
         * \brief Constructor with only solver and preconditioner names.
         *
         * Other values are kept the PETSc default ones.
         *
         * \copydoc doxygen_hide_solver_preconditioner_names_param
         */
        explicit Settings(::MoReFEM::Wrappers::Petsc::solver_name_type solver_name,
                          ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name =
                              ::MoReFEM::Wrappers::Petsc::preconditioner_name_type{ PCLU });


        /*!
         * \brief Load the settings from the data read in the input data file.
         *
         * \param[in] section A \a Petsc section from the input data file describing the settings of a solver.
         */
        template<Advanced::Concept::InputDataNS::SolverSectionType SectionT>
        explicit Settings(const SectionT& section);

        //! Destructor.
        ~Settings();

        //! \copydoc doxygen_hide_copy_constructor
        Settings(const Settings& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Settings(Settings&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Settings& operator=(const Settings& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Settings& operator=(Settings&& rhs) = default;

        ///@}


      public:
        //! Accessor to absolute tolerance.
        ::MoReFEM::Wrappers::Petsc::absolute_tolerance_type GetAbsoluteTolerance() const noexcept;

        //! Accessor to relative tolerance.
        ::MoReFEM::Wrappers::Petsc::relative_tolerance_type GetRelativeTolerance() const noexcept;

        //! Accessor to restart value for iterative solvers.
        ::MoReFEM::Wrappers::Petsc::set_restart_type GetRestart() const noexcept;

        //! Accessor to maximum number of iterations.
        ::MoReFEM::Wrappers::Petsc::max_iteration_type GetMaxIterations() const noexcept;

        //! Accessor to preconditioner name.
        ::MoReFEM::Wrappers::Petsc::preconditioner_name_type GetPreconditionerName() const noexcept;

        //! Accessor to solver name.
        ::MoReFEM::Wrappers::Petsc::solver_name_type GetSolverName() const noexcept;

        //! Accessor to step size tolerance.
        ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type GetStepSizeTolerance() const noexcept;


      private:
        //! Absolute tolerance used by PETSc solver.
        //! See for instance [this link](https://petsc.org/release/docs/manualpages/KSP/KSPSetTolerances) for more
        //! details.
        ::MoReFEM::Wrappers::Petsc::absolute_tolerance_type absolute_tolerance_{ PETSC_DEFAULT };

        //! Relative tolerance used by PETSc solver.
        //! See for instance [this link](https://petsc.org/release/docs/manualpages/KSP/KSPSetTolerances) for more
        //! details.
        ::MoReFEM::Wrappers::Petsc::relative_tolerance_type relative_tolerance_{ PETSC_DEFAULT };

        //! Number of iterations at which iterative solver (typically GMRes) restarts.
        //! See [this link](https://petsc.org/release/docs/manualpages/KSP/KSPGMRESSetRestart/) for more details.
        ::MoReFEM::Wrappers::Petsc::set_restart_type set_restart_{ PETSC_DEFAULT };

        //! Maximum number of iterations.
        //! See [this link](https://petsc.org/release/docs/manualpages/KSP/KSPSetTolerances) for more details.
        ::MoReFEM::Wrappers::Petsc::max_iteration_type max_iteration_{ PETSC_DEFAULT };

        /*!
         * \brief Preconditioner name.
         *
         * Must be 'PCLU' for a direct solver (an exception enforces this).
         *
         * See [here](https://petsc.org/release/overview/linear_solve_table/) for all available choices.
         */
        ::MoReFEM::Wrappers::Petsc::preconditioner_name_type preconditioner_name_;

        //! Solver name
        //! See [here](https://petsc.org/release/overview/linear_solve_table/) for all available choices.
        ::MoReFEM::Wrappers::Petsc::solver_name_type solver_name_;

        //! Convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || <
        //! stol*|| x || See
        //! [here](https://petsc.org/release/docs/manualpages/SNES/SNESSetTolerances/#snessettolerances)
        ::MoReFEM::Wrappers::Petsc::step_size_tolerance_type step_size_tolerance_{ PETSC_DEFAULT };
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::SolverNS


/// @} // addtogroup ThirdPartyGroup


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hxx" // IWYU pragma: export


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_SOLVER_x_INTERNAL_x_SETTINGS_HPP_
