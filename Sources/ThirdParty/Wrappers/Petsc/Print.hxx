/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 12 Nov 2013 11:55:34 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Print.hpp"

#include <cstdio>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    template<typename... Args>
    void PrintMessageOnFirstProcessor(const std::string& message,
                                      const Mpi& mpi,
                                      const char* invoking_file,
                                      int invoking_line,
                                      Args&&... arguments)
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/format-nonliteral.hpp" // IWYU pragma: keep
#include "Utilities/Warnings/Internal/IgnoreWarning/format-security.hpp"   // IWYU pragma: keep
        int error_code = PetscPrintf(mpi.GetCommunicator(), message.c_str(), std::forward<Args>(arguments)...);
        PRAGMA_DIAGNOSTIC(pop)

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscPrintf", invoking_file, invoking_line);
    }


    template<typename... Args>
    void PrintSynchronizedMessage(const std::string& message,
                                  const Mpi& mpi,
                                  FILE* C_file,
                                  const char* invoking_file,
                                  int invoking_line,
                                  Args&&... arguments)
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/format-nonliteral.hpp" // IWYU pragma: keep
#include "Utilities/Warnings/Internal/IgnoreWarning/format-security.hpp"   // IWYU pragma: keep
        int error_code =
            PetscSynchronizedFPrintf(mpi.GetCommunicator(), C_file, message.c_str(), std::forward<Args>(arguments)...);
        PRAGMA_DIAGNOSTIC(pop)

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscSynchronizedFPrintf", invoking_file, invoking_line);
    }


} // namespace MoReFEM::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_PRINT_HXX_
