/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_EXCEPTIONS_x_VECTOR_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_EXCEPTIONS_x_VECTOR_HPP_

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    //! When loading an ascii file.

    struct InvalidAsciiFile : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] file Ascii dile from which we tried to load vector content.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InvalidAsciiFile(const FilesystemNS::File& file, const char* invoking_file, int invoking_line);


        //! Destructor.
        virtual ~InvalidAsciiFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidAsciiFile(const InvalidAsciiFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvalidAsciiFile(InvalidAsciiFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidAsciiFile& operator=(const InvalidAsciiFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvalidAsciiFile& operator=(InvalidAsciiFile&& rhs) = delete;
    };


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_VECTOR_x_EXCEPTIONS_x_VECTOR_HPP_
