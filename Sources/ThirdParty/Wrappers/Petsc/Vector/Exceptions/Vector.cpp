/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/Exceptions/Vector.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string InvalidAsciiFileMsg(const MoReFEM::FilesystemNS::File& file);

} // namespace


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    InvalidAsciiFile::~InvalidAsciiFile() = default;


    InvalidAsciiFile::InvalidAsciiFile(const FilesystemNS::File& file, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(InvalidAsciiFileMsg(file), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


namespace // anonymous
{


    std::string InvalidAsciiFileMsg(const MoReFEM::FilesystemNS::File& file)
    {
        std::ostringstream oconv;
        oconv << "File " << file
              << " couldn't be interpreted properly as an ascii file. Please check it is an ascii "
                 "file and not a binary file, and that it was:\n"
                 "\t- Either an ad hoc file generated internally which provides one value per row.\n"
                 "\t- Or a Matlab file generated directly by Petsc.\n";

        return oconv.str();
    }


} // namespace


/// @} // addtogroup ThirdPartyGroup
