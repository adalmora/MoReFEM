/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Sep 2013 14:51:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string ErrorCodeMsg(int error_code, std::string_view petsc_function);

    std::string WrongMatlabExtensionMsg(const MoReFEM::FilesystemNS::File& filename);

    std::string SolverNotParallelMsg(const std::string& solver_name);

    std::string SolverNotSetUpMsg(const MoReFEM::Wrappers::Petsc::solver_name_type& solver_name);

} // namespace


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    Exception::Exception(int error_code, std::string&& petsc_function, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(ErrorCodeMsg(error_code, std::move(petsc_function)), invoking_file, invoking_line)
    { }


    WrongMatlabExtension::~WrongMatlabExtension() = default;

    WrongMatlabExtension::WrongMatlabExtension(const MoReFEM::FilesystemNS::File& filename,
                                               const char* invoking_file,
                                               int invoking_line)
    : Exception(WrongMatlabExtensionMsg(filename), invoking_file, invoking_line)
    { }


    SolverNotParallel::~SolverNotParallel() = default;


    SolverNotParallel::SolverNotParallel(const std::string& solver_name, const char* invoking_file, int invoking_line)
    : Exception(SolverNotParallelMsg(solver_name), invoking_file, invoking_line)
    { }


    SolverNotSetUp::~SolverNotSetUp() = default;


    SolverNotSetUp::SolverNotSetUp(const ::MoReFEM::Wrappers::Petsc::solver_name_type& solver_name,
                                   const char* invoking_file,
                                   int invoking_line)
    : Exception(SolverNotSetUpMsg(solver_name), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string ErrorCodeMsg(int error_code, std::string_view petsc_function)
    {
        std::ostringstream oconv;
        oconv << "Petsc (or Slepc) function " << petsc_function << " returned the error code " << error_code << '.';

        return oconv.str();
    }


    std::string WrongMatlabExtensionMsg(const MoReFEM::FilesystemNS::File& filename)
    {
        std::ostringstream oconv;
        oconv << "Invalid name for Matlab output (\"" << filename << "\"): a '.m'extension was expected.";
        return oconv.str();
    }


    std::string SolverNotParallelMsg(const std::string& solver_name)
    {
        std::ostringstream oconv;
        oconv << "Solver " << solver_name << " only tackles sequential solve!";
        return oconv.str();
    }


    std::string SolverNotSetUpMsg(const MoReFEM::Wrappers::Petsc::solver_name_type& solver_name)
    {
        std::ostringstream oconv;
        oconv << "Solver " << solver_name
              << " was found in the solver factory but it was not set up in MoReFEM configuration file.";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup ThirdPartyGroup
