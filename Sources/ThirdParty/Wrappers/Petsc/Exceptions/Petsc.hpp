/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Sep 2013 11:37:23 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_EXCEPTIONS_x_PETSC_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_EXCEPTIONS_x_PETSC_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp" // IWYU pragma: export
#include "Utilities/Exceptions/Exception.hpp"        // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    //! Generic class
    struct Exception : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
          * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);


        /*!
         * \brief Constructor with simple message
         *
         * \param[in] error_code Error code returned by Petsc.
         * \param[in] petsc_function Name of the Petsc function that returned the error code.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit Exception(int error_code, std::string&& petsc_function, const char* invoking_file, int invoking_line);

        //! Destructor.
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! When a matlab output file doesn't end with '.m'
    struct WrongMatlabExtension final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] filename Name in which data was expected in MATLAB format.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit WrongMatlabExtension(const MoReFEM::FilesystemNS::File& filename,
                                      const char* invoking_file,
                                      int invoking_line);

        //! Destructor.
        virtual ~WrongMatlabExtension() override;

        //! \copydoc doxygen_hide_copy_constructor
        WrongMatlabExtension(const WrongMatlabExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        WrongMatlabExtension(WrongMatlabExtension&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        WrongMatlabExtension& operator=(const WrongMatlabExtension& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        WrongMatlabExtension& operator=(WrongMatlabExtension&& rhs) = default;
    };


    //! When a non parallel is attempted in parallel
    struct SolverNotParallel final : public Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] solver_name Name of the solver which doesn't support parallel linear algebra.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit SolverNotParallel(const std::string& solver_name, const char* invoking_file, int invoking_line);

        //! Destructor.
        virtual ~SolverNotParallel() override;

        //! \copydoc doxygen_hide_copy_constructor
        SolverNotParallel(const SolverNotParallel& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        SolverNotParallel(SolverNotParallel&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        SolverNotParallel& operator=(const SolverNotParallel& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        SolverNotParallel& operator=(SolverNotParallel&& rhs) = default;
    };


    //! When a solver was not activated within MoReFEM
    struct SolverNotSetUp final : public Exception
    {
        /*!
         * \brief Constructor
         *
         * \param[in] solver_name Name of the solver that was requested.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit SolverNotSetUp(const ::MoReFEM::Wrappers::Petsc::solver_name_type& solver_name,
                                const char* invoking_file,
                                int invoking_line);

        //! Destructor.
        virtual ~SolverNotSetUp() override;

        //! \copydoc doxygen_hide_copy_constructor
        SolverNotSetUp(const SolverNotSetUp& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SolverNotSetUp(SolverNotSetUp&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SolverNotSetUp& operator=(const SolverNotSetUp& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SolverNotSetUp& operator=(SolverNotSetUp&& rhs) = delete;
    };


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PETSC_x_EXCEPTIONS_x_PETSC_HPP_
