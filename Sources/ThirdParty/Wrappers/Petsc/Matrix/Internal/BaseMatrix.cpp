/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Jun 2017 18:51:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/BaseMatrix.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{

    BaseMatrix::~BaseMatrix() = default;


} // namespace MoReFEM::Internal::Wrappers::Petsc


/// @} // addtogroup ThirdPartyGroup
