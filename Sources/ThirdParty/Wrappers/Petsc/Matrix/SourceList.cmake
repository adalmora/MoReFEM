### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Matrix.cpp
		${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.cpp
		${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Matrix.doxygen
		${CMAKE_CURRENT_LIST_DIR}/Matrix.hpp
		${CMAKE_CURRENT_LIST_DIR}/Matrix.hxx
		${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.hpp
		${CMAKE_CURRENT_LIST_DIR}/MatrixInfo.hxx
		${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hpp
		${CMAKE_CURRENT_LIST_DIR}/MatrixOperations.hxx
		${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hpp
		${CMAKE_CURRENT_LIST_DIR}/MatrixPattern.hxx
		${CMAKE_CURRENT_LIST_DIR}/NonZeroPattern.hpp
		${CMAKE_CURRENT_LIST_DIR}/ShellMatrix.hpp
		${CMAKE_CURRENT_LIST_DIR}/ShellMatrix.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
