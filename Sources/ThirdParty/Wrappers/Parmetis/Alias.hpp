/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 1 Oct 2013 17:33:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_ALIAS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_ALIAS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Scotch/Scotch.hpp" // IWYU pragma: export


namespace MoReFEM
{


    //! Alias to the type used in Parmetis for integers.
    using parmetis_int = idx_t;

    //! Alias to the type used in Parmetis for reals.
    using parmetis_real = real_t;


} // namespace MoReFEM


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_PARMETIS_x_ALIAS_HPP_
