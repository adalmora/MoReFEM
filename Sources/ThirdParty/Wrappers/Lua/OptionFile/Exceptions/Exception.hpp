/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_OPTION_FILE_x_EXCEPTIONS_x_EXCEPTION_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_OPTION_FILE_x_EXCEPTIONS_x_EXCEPTION_HPP_

#include <cstddef>
#include <string_view>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::LuaOptionFileNS
{


    /*!
     * \brief Exception when there are too many values in a vector.
     *
     */
    class TooManyEntriesInVector final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] field_name Name of the field with too many entries.
         * \param[in] expected_dimension Expected dimension of the vector,
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit TooManyEntriesInVector(std::string_view field_name,
                                        std::size_t expected_dimension,
                                        const char* invoking_file,
                                        int invoking_line);

        //! Destructor
        virtual ~TooManyEntriesInVector() override;

        //! \copydoc doxygen_hide_copy_constructor
        TooManyEntriesInVector(const TooManyEntriesInVector& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TooManyEntriesInVector(TooManyEntriesInVector&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TooManyEntriesInVector& operator=(const TooManyEntriesInVector& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TooManyEntriesInVector& operator=(TooManyEntriesInVector&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::LuaOptionFileNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_OPTION_FILE_x_EXCEPTIONS_x_EXCEPTION_HPP_
