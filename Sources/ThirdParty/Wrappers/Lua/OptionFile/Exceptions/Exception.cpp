/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <sstream>
#include <string>
#include <string_view>

#include "ThirdParty/Wrappers/Lua/OptionFile/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string TooManyEntriesInVectorMsg(std::string_view field_name, std::size_t expected_dimension);


} // namespace


namespace MoReFEM::ExceptionNS::LuaOptionFileNS
{


    TooManyEntriesInVector::~TooManyEntriesInVector() = default;


    TooManyEntriesInVector::TooManyEntriesInVector(std::string_view field_name,
                                                   std::size_t expected_dimension,
                                                   const char* invoking_file,
                                                   int invoking_line)
    : MoReFEM::Exception(TooManyEntriesInVectorMsg(field_name, expected_dimension), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::LuaOptionFileNS


namespace // anonymous
{


    std::string TooManyEntriesInVectorMsg(std::string_view field_name, std::size_t expected_dimension)
    {
        std::ostringstream oconv;

        oconv << "Something wrong happened while reading content of field " << field_name << ": " << expected_dimension
              << " value" << (expected_dimension > 1 ? "s were" : " was")
              << " expected but more were actually read. "
                 "This is a fairly low level catch so we can't be more specific; it might happen "
                 "for instance if for your entry in the Lua file a dimension was given and more "
                 "values were actually put as content.";

        return oconv.str();
    }

} // namespace


/// @} // addtogroup UtilitiesGroup
