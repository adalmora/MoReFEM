//! \file
//
//
//  LuaState.hxx
//  MoReFEM
//
//  Created by sebastien on 11/09/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_LUA_STATE_x_LUA_STATE_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_LUA_STATE_x_LUA_STATE_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/LuaState/LuaState.hpp"


#include <string>

#include "ThirdParty/IncludeWithoutWarning/Lua/Lua.hpp"


namespace MoReFEM::Internal::LuaNS
{


    inline const std::string& LuaState::GetString() const noexcept
    {
        return content_;
    }


    inline lua_State* LuaState::GetInternal() const noexcept
    {
        return state_;
    }


} // namespace MoReFEM::Internal::LuaNS


#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_LUA_x_LUA_STATE_x_LUA_STATE_HXX_
