/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Slepc/EigenPair.hpp"


#ifdef MOREFEM_WITH_SLEPC

#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Vector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Slepc
{


    inline const Wrappers::Petsc::Vector& EigenPair::GetEigenVector() const noexcept
    {
        assert(!(!eigen_vector_));
        return *eigen_vector_;
    }


    inline Wrappers::Petsc::Vector& EigenPair::GetNonCstEigenVector() noexcept
    {
        return const_cast<Wrappers::Petsc::Vector&>(GetEigenVector());
    }


    inline double EigenPair::GetEigenValue() const noexcept
    {
        return eigen_value_;
    }


} // namespace MoReFEM::Wrappers::Slepc


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_WITH_SLEPC

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HXX_
