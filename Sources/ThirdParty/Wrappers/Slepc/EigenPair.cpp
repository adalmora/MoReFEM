/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#ifdef MOREFEM_WITH_SLEPC

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Slepc/EigenPair.hpp"


namespace MoReFEM::Wrappers::Slepc
{


    EigenPair::EigenPair(const Wrappers::Petsc::Matrix& matrix, const char* invoking_file, int invoking_line)
    {
        Vec vec;
        int error_code = MatCreateVecs(matrix.InternalForReadOnly(), MOREFEM_PETSC_NULL, &vec);

        if (error_code)
            throw Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatCreateVecs", invoking_file, invoking_line);

        eigen_vector_ = std::make_unique<Wrappers::Petsc::Vector>(vec, true);
    }


    void EigenPair::SetEigenValue(PetscScalar eigen_value)
    {
        eigen_value_ = static_cast<double>(eigen_value);
    }


} // namespace MoReFEM::Wrappers::Slepc


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_WITH_SLEPC
