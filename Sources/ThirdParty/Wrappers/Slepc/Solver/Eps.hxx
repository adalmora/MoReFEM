/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_SOLVER_x_EPS_HXX_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_SOLVER_x_EPS_HXX_

// IWYU pragma: private, include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"

#ifdef MOREFEM_WITH_SLEPC

#include <cassert>
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Petsc { class Matrix; }
namespace MoReFEM::Wrappers::Slepc { class EigenPair; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


/// \addtogroup ThirdPartyGroup
/// \{


namespace MoReFEM::Wrappers::Slepc
{


    inline EPS Eps::Internal() noexcept
    {
        return eps_;
    }


    inline const Wrappers::Petsc::Matrix& Eps::GetMatrix() const noexcept
    {
        return eigensystem_matrix_;
    }


    inline EPS Eps::InternalForReadOnly() const noexcept
    {
        return eps_;
    }


    inline EigenPair& Eps::GetNonCstWorkRealEigenPair() const noexcept
    {
        assert(!(!work_real_eigenpair_));
        return *work_real_eigenpair_;
    }

    inline EigenPair& Eps::GetNonCstWorkImaginaryEigenPair() const noexcept
    {
        assert(!(!work_imaginary_eigenpair_));
        return *work_imaginary_eigenpair_;
    }


} // namespace MoReFEM::Wrappers::Slepc


/// @} // addtogroup ThirdPartyGroup

#endif // MOREFEM_WITH_SLEPC

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_SOLVER_x_EPS_HXX_
