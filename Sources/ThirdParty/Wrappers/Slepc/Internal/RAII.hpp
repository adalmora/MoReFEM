//! \file
//
//
//  RAII.hpp
//  MoReFEM
//
//  Created by sebastien
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_INTERNAL_x_R_A_I_I_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_INTERNAL_x_R_A_I_I_HPP_

#ifdef MOREFEM_WITH_SLEPC

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::SlepcNS
{


    /*!
     * \brief RAII class to initialize / close properly Slepc.
     *
     * Only relevant if MOREFEM_WITH_SLEPC build option is set to true.
     */
    class RAII final : public Utilities::Singleton<RAII>
    {

      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit RAII();

        //! Destructor.
        virtual ~RAII() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<RAII>;

        //! Name of the class.
        static const std::string& ClassName();

        ///@}
    };


} // namespace MoReFEM::Internal::SlepcNS

#endif // MOREFEM_WITH_SLEPC

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_INTERNAL_x_R_A_I_I_HPP_
