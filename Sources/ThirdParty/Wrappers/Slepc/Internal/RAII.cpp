//! \file
//
//
//  RAII.cpp
//  MoReFEM
//
//  Created by sebastien
// Copyright © 2022 Inria. All rights reserved.
//

#ifdef MOREFEM_WITH_SLEPC

#include <cassert>
#include <memory> // IWYU pragma: keep
#include <string>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"


namespace MoReFEM::Internal::SlepcNS
{


    RAII::RAII()
    {
        int error_code = SlepcInitialize(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "");

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "SlepcInitialize", __FILE__, __LINE__);
    }


    RAII::~RAII()
    {
        int error_code = SlepcFinalize();
        assert(!error_code && "Error in SlepcFinalize call!"); // No exception in destructors...
        static_cast<void>(error_code);                         // avoid warning in release compilation.
    }


    const std::string& RAII::ClassName()
    {
        static std::string ret("Internal::SlepcNS::RAII");
        return ret;
    }


} // namespace MoReFEM::Internal::SlepcNS

#endif // MOREFEM_WITH_SLEPC
