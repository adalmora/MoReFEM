/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HPP_
#define MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HPP_

#ifdef MOREFEM_WITH_SLEPC


/// \ingroup ThirdPartyGroup


#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp" // IWYU pragma: export // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Slepc { class Eps; } // IWYU pragma: keep


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Slepc
{


    /*!
     *\brief This class wraps a pair of eigenvector and eigenvalue.
     *
     *
     */
    class EigenPair final
    {
      public:
        //! Unique smart pointer.
        using unique_ptr = std::unique_ptr<EigenPair>;

        //! Friendhip to \a Eps, to access non constant vector.
        friend class Eps;

        /// \name Constructors and destructor.
        ///@{

      private:
        /*!
         * \brief Constructor.
         *
         * \param[in] matrix Eigensystem matrix
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         * The constructor is private as it is intended to be used only internally in EPS class;
         * a developer is expected to use a fully formed object obtained through Eps::GetEigenPair().
         */
        EigenPair(const Wrappers::Petsc::Matrix& matrix, const char* invoking_file, int invoking_line);

      public:
        //! Destructor.
        ~EigenPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        EigenPair(const EigenPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        EigenPair(EigenPair&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        EigenPair& operator=(const EigenPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        EigenPair& operator=(EigenPair&& rhs) = delete;

        ///@}

        /*!
         * \brief Accessor to the eigenvector.
         *
         * \return The eigenvector.
         */
        const Wrappers::Petsc::Vector& GetEigenVector() const noexcept;

        /*!
         * \brief Accessor to the eigenvalue.
         *
         * \return The eigenvalue.
         */
        double GetEigenValue() const noexcept;


      private:
        /*!
         * \brief Non constant accessor to the eigenvector.
         *
         * \return The eigenvector.
         */
        Wrappers::Petsc::Vector& GetNonCstEigenVector() noexcept;

        /*!
         * \brief Mutator to the eigenvalue.
         *
         * \param[in] eigenvalue New value.
         */
        void SetEigenValue(PetscScalar eigenvalue);


      private:
        //! Eigenvector.
        Wrappers::Petsc::Vector::unique_ptr eigen_vector_{ nullptr };

        //! Eigenvalue.
        double eigen_value_;
    };


} // namespace MoReFEM::Wrappers::Slepc


#include "ThirdParty/Wrappers/Slepc/EigenPair.hxx" // IWYU pragma: export

#endif // MOREFEM_WITH_SLEPC

#endif // MOREFEM_x_THIRD_PARTY_x_WRAPPERS_x_SLEPC_x_EIGEN_PAIR_HPP_
