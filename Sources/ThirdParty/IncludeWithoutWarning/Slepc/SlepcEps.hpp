/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/

#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SLEPC_x_SLEPC_EPS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SLEPC_x_SLEPC_EPS_HPP_

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")

#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/redundant-decls.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-macro-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

#include "slepceps.h"

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_SLEPC_x_SLEPC_EPS_HPP_
