/*!
//
// \file
//

// \ingroup ThirdPartyGroup
// \addtogroup ThirdPartyGroup
// \{
*/


#ifndef MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MACROS_HPP_
#define MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MACROS_HPP_

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)

#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-macro-identifier.hpp"

#include "petscmacros.h" // IWYU pragma: export

PRAGMA_DIAGNOSTIC(pop)


/// @} // addtogroup ThirdPartyGroup


#endif // MOREFEM_x_THIRD_PARTY_x_INCLUDE_WITHOUT_WARNING_x_PETSC_x_PETSC_MACROS_HPP_
