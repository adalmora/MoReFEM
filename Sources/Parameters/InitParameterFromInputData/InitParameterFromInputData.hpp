/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_

#include "Core/MoReFEMData/Advanced/Concept.hpp" // IWYU pragma: export

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"
#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
#include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_parameter_init_facility_common
     *
     *
     * \tparam TimeDependencyT Policy in charge of time dependency.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.

     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * \attention Please notice all \a Parameter can't be initialized from the input data file. For instance a
     * parameter defined directly at quadrature points or at dofs must be built in hard in the code; current function
     * enables to choose among certain pre-definite choices (at the time of this writing, 'constant', 'lua function' and
     * 'piecewise constant by domain').
     *
     * \return Parameter considered, or nullptr if irrelevant (i.e. naure was 'ignore').
     */


    /*!
     * \brief Build a \a ScalarParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     *
     * \param[in] nature Value of 'nature' field (read from input data file)
     * \param[in] value Content of 'value' field (read from input data file).
     */
    // clang-format off
    template
    <
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT
    >
    // clang-format on
    typename ScalarParameter<TimeDependencyT>::unique_ptr InitScalarParameterFromInputData(
        StringT&& name,
        const Domain& domain,
        const std::string& nature,
        const Internal::ParameterNS::Traits<ParameterNS::Type::scalar>::variant_type& value);


    /*!
     * \brief Build a \a ScalarParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     * \copydoc doxygen_hide_morefem_data_arg
     */
    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename ScalarParameter<TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


    /*!
     * \brief Build a \a VectorialParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     * \copydoc doxygen_hide_morefem_data_arg
     */
    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename VectorialParameter<TimeDependencyT>::unique_ptr
    InitVectorialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


    /*!
     * \brief Build a \a VectorialParameter from data read in the input data file.
     *
     * \copydoc doxygen_hide_parameter_init_facility_common
     * \copydoc doxygen_hide_morefem_data_arg
     */
    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename MatricialParameter<TimeDependencyT>::unique_ptr
    InitMatricialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HPP_
