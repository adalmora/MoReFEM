/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HPP_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HPP_

#include "Core/MoReFEMData/Advanced/Concept.hpp" // IWYU pragma: export

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtQuadraturePoint/AtQuadraturePoint.hpp"
#include "Parameters/Policy/Constant/Constant.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
#include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Init a three dimensional parameter, e.g. a source.
     *
     * This is handled actually as three separate \a ScalarParameter, which nature may change: one might be defined
     * by a Lua function and another one by a constant.
     *
     * \param[in] name Name of the parameter, as it will appear in outputs.
     * \copydoc doxygen_hide_morefem_data_arg
     * \copydoc doxygen_hide_parameter_domain_arg
     *
     * If any of the nature of the component is 'ignore', all components should be as well. In this case, nullptr is
     * returned.
     *
     * \return Parameter considered (or nullptr if nature is 'ignore').
     */
    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type> class TimeDependencyT = ParameterNS::TimeDependencyNS::None,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    Init3DCompoundParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data);


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HPP_
