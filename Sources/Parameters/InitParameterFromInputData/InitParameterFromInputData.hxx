/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_

// IWYU pragma: private, include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Core/MoReFEMData/Extract.hpp"

#include "Parameters/Exceptions/Exception.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        template<ParameterNS::Type> class TimeDependencyT,
        class StringT
    >
    // clang-format on
    typename ScalarParameter<TimeDependencyT>::unique_ptr InitScalarParameterFromInputData(
        StringT&& name,
        const Domain& domain,
        const std::string& nature,
        const typename Internal::ParameterNS::Traits<ParameterNS::Type::scalar>::variant_type& value)
    {
        if (nature == "ignore")
            return nullptr;

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::scalar>;

        if (nature == "constant")
        {
            using parameter_type = Internal::ParameterNS::
                ParameterInstance<ParameterNS::Type::scalar, ::MoReFEM::ParameterNS::Policy::Constant, TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, double>());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "lua_function")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::SpatialFunctionGlobalCoords,
                     TimeDependencyT
                 >;
            // clang-format on

            using storage_type = ::MoReFEM::Wrappers::Lua::spatial_function;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "piecewise_constant_by_domain")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                     TimeDependencyT
                 >;
            // clang-format on

            using storage_type = traits::piecewise_constant_by_domain_type;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type>
        class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename ScalarParameter<TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Nature>(morefem_data);
        decltype(auto) value =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Value>(morefem_data);
        
        return InitScalarParameterFromInputData<TimeDependencyT>(name, domain, nature, value);
    }


    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type>
        class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename VectorialParameter<TimeDependencyT>::unique_ptr
    InitVectorialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::VectorDimension>(morefem_data);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::vector>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<std::vector<double>>(value));

            decltype(auto) interpreted_value = std::get<std::vector<double>>(value);

            using parameter_type = Internal::ParameterNS::
                ParameterInstance<ParameterNS::Type::vector, ::MoReFEM::ParameterNS::Policy::Constant, TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, LocalVector>());

            if (interpreted_value.size() != dimension)
                throw ExceptionNS::ParameterNS::InconsistentVectorDimension(
                    name, dimension, interpreted_value.size(), __FILE__, __LINE__);

            LocalVector as_local_vector;
            as_local_vector.resize({ dimension });

            std::copy(interpreted_value.cbegin(), interpreted_value.cend(), as_local_vector.begin());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_vector);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            using parameter_type =
                Internal::ParameterNS::ParameterInstance<ParameterNS::Type::vector,
                                                         ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                                                         TimeDependencyT>;

            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_vector] : interpreted_value)
            {
                if (as_vector.size() != dimension)
                    throw ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain(
                        name, domain_id, dimension, as_vector.size(), __FILE__, __LINE__);
                LocalVector as_local_vector;
                as_local_vector.resize({ dimension });

                std::copy(as_vector.cbegin(), as_vector.cend(), as_local_vector.begin());

                content.insert({ domain_id, as_local_vector });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name, __FILE__, __LINE__);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        class ParameterT,
        template<ParameterNS::Type>
        class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename MatricialParameter<TimeDependencyT>::unique_ptr
    InitMatricialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::MatrixDimension>(morefem_data);

        if (dimension.size() != 2ul)
            throw ExceptionNS::ParameterNS::InvalidMatrixDimensionInOptionFile(name, dimension, __FILE__, __LINE__);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::matrix>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<traits::constant_in_lua_file_type>(value));

            decltype(auto) interpreted_value = std::get<traits::constant_in_lua_file_type>(value);

            using parameter_type = Internal::ParameterNS::
                ParameterInstance<ParameterNS::Type::matrix, ::MoReFEM::ParameterNS::Policy::Constant, TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, LocalMatrix>());

            if (interpreted_value.size() != dimension[0] * dimension[1])
                throw ExceptionNS::ParameterNS::InconsistentMatrixDimension(
                    name, dimension, interpreted_value.size(), __FILE__, __LINE__);

            LocalMatrix as_local_matrix;
            as_local_matrix.resize({ dimension[0], dimension[1] });

            std::copy(interpreted_value.cbegin(), interpreted_value.cend(), as_local_matrix.begin());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_matrix);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            using parameter_type =
                Internal::ParameterNS::ParameterInstance<ParameterNS::Type::matrix,
                                                         ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                                                         TimeDependencyT>;

            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_std_vector] : interpreted_value)
            {
                if (as_std_vector.size() != dimension[0] * dimension[1])
                    throw ExceptionNS::ParameterNS::InconsistentMatrixDimensionForDomain(
                        name, domain_id, dimension, as_std_vector.size(), __FILE__, __LINE__);

                LocalMatrix as_local_matrix;
                as_local_matrix.resize({ dimension[0], dimension[1] });

                std::copy(as_std_vector.cbegin(), as_std_vector.cend(), as_local_matrix.begin());

                content.insert({ domain_id, as_local_matrix });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name, __FILE__, __LINE__);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }

} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT_PARAMETER_FROM_INPUT_DATA_HXX_
