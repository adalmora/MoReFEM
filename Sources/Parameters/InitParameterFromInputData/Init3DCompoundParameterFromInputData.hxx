/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 11:30:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HXX_
#define MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HXX_

// IWYU pragma: private, include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "Core/MoReFEMData/Extract.hpp"

#include "Parameters/Exceptions/Exception.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


namespace MoReFEM
{


    template<class ParameterT,
             template<ParameterNS::Type>
             class TimeDependencyT,
             class T,
             ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    auto Init3DCompoundParameterFromInputData(T&& name, const Domain& domain, const MoReFEMDataT& morefem_data) ->
        typename Parameter<ParameterNS::Type::vector, LocalCoords, TimeDependencyT>::unique_ptr
    {
        decltype(auto) nature_vector =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Nature>(morefem_data);

        assert(nature_vector.size() == 3ul);
        const auto& nature_x = nature_vector[0];
        const auto& nature_y = nature_vector[1];
        const auto& nature_z = nature_vector[2];

        if ((nature_x == "ignore" || nature_y == "ignore" || nature_z == "ignore")
            && (nature_x != nature_y || nature_x != nature_z))

            throw ExceptionNS::ParameterNS::PartialIgnoredCompound(
                ParameterT::GetName(), nature_vector, __FILE__, __LINE__);

        if (nature_x == "ignore")
            return nullptr;

        decltype(auto) value_vector =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename ParameterT::Value>(morefem_data);
        assert(value_vector.size() == 3ul);

        auto&& component_x = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
            "Value X", domain, nature_x, value_vector[0]);

        auto&& component_y = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
            "Value Y", domain, nature_y, value_vector[1]);

        auto&& component_z = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
            "Value Z", domain, nature_z, value_vector[2]);

        using type = ParameterNS::ThreeDimensionalCoumpoundParameter<TimeDependencyT>;

        return std::make_unique<type>(
            std::forward<T>(name), std::move(component_x), std::move(component_y), std::move(component_z));
    }


} // namespace MoReFEM


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INIT_PARAMETER_FROM_INPUT_DATA_x_INIT3_D_COMPOUND_PARAMETER_FROM_INPUT_DATA_HXX_
