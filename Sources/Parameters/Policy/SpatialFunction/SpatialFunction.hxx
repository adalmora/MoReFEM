/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 16:38:28 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup ParametersGroup
 // \addtogroup ParametersGroup
 // \{
 */


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_SPATIAL_FUNCTION_x_SPATIAL_FUNCTION_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_SPATIAL_FUNCTION_x_SPATIAL_FUNCTION_HXX_

// IWYU pragma: private, include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    SpatialFunction<TypeT, CoordsTypeT>::SpatialFunction(const std::string&,
                                                         const Domain&,
                                                         parameter_storage_type lua_function)
    : lua_function_(lua_function)
    { }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    auto SpatialFunction<TypeT, CoordsTypeT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                 const GeometricElt& geom_elt) const -> return_type
    {
        if constexpr (CoordsTypeT == CoordsType::local)
            return lua_function_(
                local_coords.GetValueOrZero(0), local_coords.GetValueOrZero(1), local_coords.GetValueOrZero(2));

        else if constexpr (CoordsTypeT == CoordsType::global)
        {
            auto& coords = GetNonCstWorkCoords();
            Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, coords);
            return lua_function_(coords.x(), coords.y(), coords.z());
        }

        assert(false
               && "Should never happen as all possible values of the enum class are addressed in the "
                  "cases above.");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    auto SpatialFunction<TypeT, CoordsTypeT>::GetAnyValueFromPolicy() const -> return_type
    {
        return lua_function_(0., 0., 0.);
    }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    [[noreturn]] auto SpatialFunction<TypeT, CoordsTypeT>::GetConstantValueFromPolicy() const -> return_type
    {
        assert(false && "A Lua function should yield IsConstant() = false!");
        exit(-1);
    }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    bool SpatialFunction<TypeT, CoordsTypeT>::IsConstant() const
    {
        return false;
    }

    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    void SpatialFunction<TypeT, CoordsTypeT>::WriteFromPolicy(std::ostream& out) const
    {
        out << "# Given by the Lua function given in the input data file." << std::endl;
    }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    inline SpatialPoint& SpatialFunction<TypeT, CoordsTypeT>::GetNonCstWorkCoords() const noexcept
    {
        return work_coords_;
    }


    template<ParameterNS::Type TypeT, CoordsType CoordsTypeT>
    inline void SpatialFunction<TypeT, CoordsTypeT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_SPATIAL_FUNCTION_x_SPATIAL_FUNCTION_HXX_
