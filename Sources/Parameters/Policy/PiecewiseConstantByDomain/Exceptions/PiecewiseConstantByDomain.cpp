/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/

#include <sstream>
#include <string>
#include <string_view>

#include "Utilities/Containers/Print.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp"


namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name);


    template<class LinearAlgebraT>
    std::string
    InconsistentLocalLinearAlgebraShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, LinearAlgebraT>& storage,
                                           std::string_view parameter_name);


} // namespace


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    InconsistentDomains::~InconsistentDomains() = default;


    InconsistentDomains::InconsistentDomains(
        ::MoReFEM::GeomEltNS::index_type geom_elt_index,
        std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
        std::string_view parameter_name,
        const char* invoking_file,
        int invoking_line)
    : MoReFEM::Exception(InconsistentDomainsMsg(geom_elt_index, domain_ids, parameter_name),
                         invoking_file,
                         invoking_line)
    { }


    InconsistentLocalVectorShape::~InconsistentLocalVectorShape() = default;


    InconsistentLocalVectorShape::InconsistentLocalVectorShape(
        const std::map<DomainNS::unique_id, LocalVector>& storage,
        std::string_view parameter_name,
        const char* invoking_file,
        int invoking_line)
    : MoReFEM::Exception(InconsistentLocalLinearAlgebraShapeMsg(storage, parameter_name), invoking_file, invoking_line)
    { }


    InconsistentLocalMatrixShape::~InconsistentLocalMatrixShape() = default;


    InconsistentLocalMatrixShape::InconsistentLocalMatrixShape(
        const std::map<DomainNS::unique_id, LocalMatrix>& storage,
        std::string_view parameter_name,
        const char* invoking_file,
        int invoking_line)
    : MoReFEM::Exception(InconsistentLocalLinearAlgebraShapeMsg(storage, parameter_name), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name)
    {
        std::ostringstream oconv;
        oconv << "A geometric element (which index is " << geom_elt_index << ") is in at least two domains ("
              << domain_ids.first << " and " << domain_ids.second << ") with two different values for the '"
              << parameter_name << "' parameter.";

        return oconv.str();
    }


    template<class LinearAlgebraT>
    std::string
    InconsistentLocalLinearAlgebraShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, LinearAlgebraT>& storage,
                                           std::string_view parameter_name)
    {
        using namespace MoReFEM;

        std::ostringstream oconv;
        oconv << "Parameter '" << parameter_name
              << "' was defined as being piecewise constant by domain but the "
                 "shape of the provided linear algebra provided don't match: "
              << std::endl;

        for (const auto& [domain_id, linear_algebra] : storage)
        {
            Utilities::PrintContainer<>::Do(linear_algebra.shape(),
                                            oconv,
                                            PrintNS::Delimiter::separator(", "),
                                            PrintNS::Delimiter::opener("\t- ["),
                                            PrintNS::Delimiter::closer("]\n"));
        }

        return oconv.str();
    }


} // namespace


/// @} // addtogroup ParametersGroup
