/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_EXCEPTIONS_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_EXCEPTIONS_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <string_view>
#include <utility>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/MatrixOrVector.hpp"

#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Index.hpp"


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    /*!
     * \brief Thrown when a \a GeometricElt is in two \a Domain involved in the same \a Parameter and the associated values are inconsistent.
     *
     */
    class InconsistentDomains final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] geom_elt_index The index of the \a GeometricElt, as returned by \a GeometricElt::GetIndex().
         * \param[in] domain_ids Both \a Domain in which the \a GeometricElt was found. The exception is thrown as soon
         as a discrepancy
         * is found, so if more than two \a Domain are involved in the issue only the first two ones for which it occurs
         will be mentioned in the
         * exception message.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InconsistentDomains(
            ::MoReFEM::GeomEltNS::index_type geom_elt_index,
            std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
            std::string_view parameter_name,
            const char* invoking_file,
            int invoking_line);

        //! Destructor
        virtual ~InconsistentDomains() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentDomains(const InconsistentDomains& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentDomains(InconsistentDomains&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentDomains& operator=(const InconsistentDomains& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentDomains& operator=(InconsistentDomains&& rhs) = default;
    };


    /*!
     * \brief Thrown when the\a LocalVector associated with different \a Domains don't share the same shape.
     *
     */
    class InconsistentLocalVectorShape final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] storage  The data provided to fill the internal storage to the \a Parameter.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InconsistentLocalVectorShape(const std::map<::MoReFEM::DomainNS::unique_id, LocalVector>& storage,
                                              std::string_view parameter_name,
                                              const char* invoking_file,
                                              int invoking_line);

        //! Destructor
        virtual ~InconsistentLocalVectorShape() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentLocalVectorShape(const InconsistentLocalVectorShape& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentLocalVectorShape(InconsistentLocalVectorShape&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentLocalVectorShape& operator=(const InconsistentLocalVectorShape& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentLocalVectorShape& operator=(InconsistentLocalVectorShape&& rhs) = default;
    };


    /*!
     * \brief Thrown when the\a LocalMatrix associated with different \a Domains don't share the same shape.
     *
     */
    class InconsistentLocalMatrixShape final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] storage  The data provided to fill the internal storage to the \a Parameter.
         * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InconsistentLocalMatrixShape(const std::map<::MoReFEM::DomainNS::unique_id, LocalMatrix>& storage,
                                              std::string_view parameter_name,
                                              const char* invoking_file,
                                              int invoking_line);

        //! Destructor
        virtual ~InconsistentLocalMatrixShape() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentLocalMatrixShape(const InconsistentLocalMatrixShape& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentLocalMatrixShape(InconsistentLocalMatrixShape&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentLocalMatrixShape& operator=(const InconsistentLocalMatrixShape& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentLocalMatrixShape& operator=(InconsistentLocalMatrixShape&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_EXCEPTIONS_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
