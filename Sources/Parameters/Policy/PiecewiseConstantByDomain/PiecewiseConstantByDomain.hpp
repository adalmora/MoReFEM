/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
 /Users/sebastien/Codes/MoReFEM/CoreLibrary// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <vector>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/UniqueId/UniqueId.hpp"    // IWYU pragma: export

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp" // IWYU pragma: export


namespace MoReFEM::ParameterNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter is piecewise constant by domain.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     */
    template<ParameterNS::Type TypeT>
    class PiecewiseConstantByDomain
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = PiecewiseConstantByDomain<TypeT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = Internal::ParameterNS::Traits<TypeT>;


      public:
        //! Alias to the return type.
        using return_type = typename traits::return_type;

        //! Alias to the type of the value actually stored.
        using parameter_storage_type = typename traits::piecewise_constant_by_domain_type;

        //! Type of the values expressed at a given \a LocalCoords by the \a Parameter.
        using value_type = typename traits::value_type;

        //! Alias for the mapping between \a GeometricElt and the \a Domain to which they belong to.
        using mapping_geom_elt_domain_type =
            std::unordered_map<::MoReFEM::GeomEltNS::index_type, std::vector<DomainNS::unique_id>>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor for the parameter policy PiecewiseConstantByDomain.
         *
         * \copydoc doxygen_hide_parameter_name_and_domain_arg
         * \param[in] value An associative container which key is the index of the domain and the value
         * the value the parameter takes in the matching domain.
         *
         * It is expected that the \a Domain defined here are actually subsets of the \a Domain upon which
         * the \a Parameter is defined.
         */
        explicit PiecewiseConstantByDomain(const std::string& name, const Domain& domain, parameter_storage_type value);

        //! Destructor.
        ~PiecewiseConstantByDomain() = default;

        //! \copydoc doxygen_hide_copy_constructor
        PiecewiseConstantByDomain(const PiecewiseConstantByDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        PiecewiseConstantByDomain(PiecewiseConstantByDomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        PiecewiseConstantByDomain& operator=(const PiecewiseConstantByDomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        PiecewiseConstantByDomain& operator=(PiecewiseConstantByDomain&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Policy.
         */
        void SetConstantValue(value_type);

      protected:
        //! Provided here to make the code compile, but should never be called.
        [[noreturn]] return_type GetConstantValueFromPolicy() const noexcept;

        //! \copydoc doxygen_hide_parameter_get_value
        return_type GetValueFromPolicy(const local_coords_type& local_coords, const GeometricElt& geom_elt) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;


      protected:
        //! Whether the parameter varies spatially or not (so always false here).
        constexpr bool IsConstant() const noexcept;

        //! Write the content of the parameter for which policy is used in a stream.
        //! \copydoc doxygen_hide_stream_inout
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        //! Constant accessor to the attribute value_.
        const parameter_storage_type& GetStoredValuesPerDomain() const noexcept;

        //! Access to the map that links a geometric element to a domain (through their unique ids).
        const mapping_geom_elt_domain_type& GetMapGeomEltDomain() const noexcept;

        //! Non constant access to the map that links a geometric element to a domain (through their unique
        //! ids).
        mapping_geom_elt_domain_type& GetNonCstMapGeomEltDomain() noexcept;


      private:
        //! Value of the parameter for each domain (represented by their unique ids).
        parameter_storage_type stored_values_per_domain_;

        /*!
         * \brief Map of geom_elem associated to its domain (must be unique!).
         *
         * Key is the unique id of the \a GeometricElt
         * Value is the list of the unique ids of the \a Domain to which it belong to. There should be only one
         * in most cases; several is allowed only if the associated value is the same for all \a Domain. The
         * reason I keep all of them is to be future-proof if at some point we want to add the possibility to
         * modify the value in one given \a Domain.
         */
        mapping_geom_elt_domain_type mapping_geom_elt_domain_;
    };


} // namespace MoReFEM::ParameterNS::Policy


/// @} // addtogroup ParametersGroup


#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HPP_
