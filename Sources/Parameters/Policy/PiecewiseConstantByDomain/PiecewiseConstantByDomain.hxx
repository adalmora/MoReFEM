/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 7 Jul 2015 17:54:00 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_

// IWYU pragma: private, include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/UnorderedMap.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT>
    PiecewiseConstantByDomain<TypeT>::PiecewiseConstantByDomain(const std::string& name,
                                                                const Domain& domain,
                                                                parameter_storage_type value)
    : stored_values_per_domain_(value)
    {
        auto& mapping_geom_elt_domain = GetNonCstMapGeomEltDomain();
        mapping_geom_elt_domain.max_load_factor(Utilities::DefaultMaxLoadFactor());

        Internal::ParameterNS::PiecewiseConstantByDomainNS::CheckShapeConsistency<TypeT>(
            value, name, __FILE__, __LINE__);

        decltype(auto) mesh = domain.GetMesh();

        const auto& geom_elem_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

        const auto& domain_manager_instance = DomainManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) stored_values_per_domain = GetStoredValuesPerDomain();

        for (const auto& geom_elt_ptr : geom_elem_list)
        {
            for (const auto& [domain_index, stored_value] : stored_values_per_domain)
            {
                // Domain here is expected to be a subset of the whole \a Domain upon which the \a Parameter is
                // built.
                const auto& current_domain = domain_manager_instance.GetDomain(domain_index, __FILE__, __LINE__);

                assert(!(!geom_elt_ptr));
                const auto& geom_elt = *geom_elt_ptr;

                if (!current_domain.IsGeometricEltInside(geom_elt))
                    continue;

                const auto geom_elt_index = geom_elt.GetIndex();

                auto it = mapping_geom_elt_domain.find(geom_elt_index);
                if (it == mapping_geom_elt_domain.cend())
                    mapping_geom_elt_domain.insert({ geom_elt_index, { current_domain.GetUniqueId() } });
                else
                {
                    // We authorize a \a GeometricElt to be in two distinct Domains for a Parameter definition
                    // only if the associated value is the same for both. If at some point we introduce the
                    // possibility to modify the value of a Parameter within a Domain, this should be revisited
                    // (one could forbid it or add the safety to ensure there is no discrepancy - a safety
                    // is present in debug mode and would trigger an assert if at some point two inconsistent
                    // values were made available for a same \a GeometricÈlt.
                    const auto& already_stored_domain_id_list = it->second;

                    for (const auto& already_stored_domain_id : already_stored_domain_id_list)
                    {
                        auto it_value_for_domain = value.find(already_stored_domain_id);

                        assert(it_value_for_domain != value.cend()
                               && "If not fulfilled something is askew in the internals of the policy.");

                        if (!NumericNS::AreEqual(it_value_for_domain->second, stored_value))
                        {
                            throw ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentDomains(
                                geom_elt.GetIndex(),
                                std::make_pair(current_domain.GetUniqueId(), already_stored_domain_id),
                                name,
                                __FILE__,
                                __LINE__);
                        }
                    }
                }
            }
        }
    }


    template<ParameterNS::Type TypeT>
    inline auto PiecewiseConstantByDomain<TypeT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                     const GeometricElt& geom_elt) const -> return_type
    {
        static_cast<void>(local_coords);

        // First identify the relevant domain for the current \a geom_elt.
        const auto geom_elt_index = geom_elt.GetIndex();

        const auto& mapping_geom_elt_domain = GetMapGeomEltDomain();

        const auto it_domain = mapping_geom_elt_domain.find(geom_elt_index);

        assert(it_domain != mapping_geom_elt_domain.cend());

        const auto& domain_list = it_domain->second;

        const DomainNS::unique_id domain_unique_id = domain_list.back();
        // < by convention if several \a Domain all should yield the same associated value.

        // Then fetch the value related to this domain.
        const auto& stored_values_per_domain = GetStoredValuesPerDomain();

        const auto it = stored_values_per_domain.find(domain_unique_id);
        assert(it != stored_values_per_domain.cend());

        const auto& ret = it->second;

#ifndef NDEBUG
        {
            if (domain_list.size() > 1ul)
            {
                assert(std::all_of(domain_list.cbegin(),
                                   domain_list.cend(),
                                   [&stored_values_per_domain, ret](const auto domain_id)
                                   {
                                       auto it_value = stored_values_per_domain.find(domain_id);
                                       assert(it_value != stored_values_per_domain.cend()
                                              && "If not fulfilled something is askew in the internals of the policy.");

                                       return NumericNS::AreEqual(ret, it_value->second);
                                   }));
            }
        }
#endif // NDEBUG

        return ret;
    }


    template<ParameterNS::Type TypeT>
    inline auto PiecewiseConstantByDomain<TypeT>::GetAnyValueFromPolicy() const -> return_type
    {
        const auto& stored_values_per_domain = GetStoredValuesPerDomain();
        assert(!stored_values_per_domain.empty());
        return stored_values_per_domain.cbegin()->second;
    }


    template<ParameterNS::Type TypeT>
    [[noreturn]] auto PiecewiseConstantByDomain<TypeT>::GetConstantValueFromPolicy() const noexcept -> return_type
    {
        assert(false && "Parameter class should have guided toward GetValue()!");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT>
    inline constexpr bool PiecewiseConstantByDomain<TypeT>::IsConstant() const noexcept
    {
        return false;
    }


    template<ParameterNS::Type TypeT>
    void PiecewiseConstantByDomain<TypeT>::WriteFromPolicy(std::ostream& out) const
    {
        out << "# Domain index; Value by domain:" << std::endl;

        const auto& stored_values_per_domain = GetStoredValuesPerDomain();

        for (const auto& pair : stored_values_per_domain)
            out << pair.first << ';' << pair.second << std::endl;
    }


    template<ParameterNS::Type TypeT>
    inline auto PiecewiseConstantByDomain<TypeT>::GetStoredValuesPerDomain() const noexcept
        -> const parameter_storage_type&
    {
        return stored_values_per_domain_;
    }


    template<ParameterNS::Type TypeT>
    inline auto PiecewiseConstantByDomain<TypeT>::GetMapGeomEltDomain() const noexcept
        -> const mapping_geom_elt_domain_type&
    {
        return mapping_geom_elt_domain_;
    }


    template<ParameterNS::Type TypeT>
    inline auto PiecewiseConstantByDomain<TypeT>::GetNonCstMapGeomEltDomain() noexcept -> mapping_geom_elt_domain_type&
    {
        return const_cast<mapping_geom_elt_domain_type&>(GetMapGeomEltDomain());
    }

    template<ParameterNS::Type TypeT>
    inline void PiecewiseConstantByDomain<TypeT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_PIECEWISE_CONSTANT_BY_DOMAIN_HXX_
