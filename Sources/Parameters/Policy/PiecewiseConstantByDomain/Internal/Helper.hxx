/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HXX_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HXX_

// IWYU pragma: private, include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hpp"


#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS
{


    template<Type TypeT>
    void
    CheckShapeConsistency(const std::map<::MoReFEM::DomainNS::unique_id, typename Traits<TypeT>::value_type>& storage,
                          std::string_view parameter_name,
                          const char* invoking_file,
                          int invoking_line)
    {
        if (storage.empty()) // weird case, but not the place to handle it.
            return;

        if constexpr (TypeT == Type::scalar)
            ;
        else if constexpr (TypeT == Type::vector)
        {
            using ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalVectorShape;

            const auto& first_shape = storage.cbegin()->second.shape();

            if (!std::all_of(storage.cbegin(),
                             storage.cend(),
                             [&first_shape](const auto& item)
                             {
                                 const auto& [domain_id, vector] = item;
                                 return vector.shape() == first_shape;
                             }))
                throw InconsistentLocalVectorShape(storage, parameter_name, invoking_file, invoking_line);
        } else if constexpr (TypeT == Type::matrix)
        {
            using ::MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalMatrixShape;

            const auto& first_shape = storage.cbegin()->second.shape();

            if (!std::all_of(storage.cbegin(),
                             storage.cend(),
                             [&first_shape](const auto& item)
                             {
                                 const auto& [domain_id, matrix] = item;
                                 return matrix.shape() == first_shape;
                             }))
                throw InconsistentLocalMatrixShape(storage, parameter_name, invoking_file, invoking_line);
        } else
        {
            assert(false
                   && "Case not foreseen. If a new type is added in the enum please add it in the "
                      "cases handled above.");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HXX_
