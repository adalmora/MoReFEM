/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 May 2015 14:55:40 +0200
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HPP_
#define MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HPP_

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Parameter/Internal/Traits.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS
{


    /*!
     * \brief Check the values associated to all the \a Domain all share the same shapes.
     *
     * Obviously this make sense only for \a Type::vector and \a Type::matrix cases; in \a Type::scalar nothing is done.
     * If the shapes aren't the same, an exception is thrown (namely
     * \a ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalVectorShape or
     * \a ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalMatrixShape).
     *
     * \param[in] storage The data given in input at \a Parameter construction used to fill the values \a Parameter takes in the different \a Domain s
     * (key is the unique identifier of \a Domain).
     * \param[in] parameter_name Name of the \a Parameter for which the issue arose (as given in its constructor).
     * \copydoc doxygen_hide_invoking_file_and_line
     */
    template<Type TypeT>
    void
    CheckShapeConsistency(const std::map<::MoReFEM::DomainNS::unique_id, typename Traits<TypeT>::value_type>& storage,
                          std::string_view parameter_name,
                          const char* invoking_file,
                          int invoking_line);


} // namespace MoReFEM::Internal::ParameterNS::PiecewiseConstantByDomainNS


/// @} // addtogroup ParametersGroup


#include "Parameters/Policy/PiecewiseConstantByDomain/Internal/Helper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_POLICY_x_PIECEWISE_CONSTANT_BY_DOMAIN_x_INTERNAL_x_HELPER_HPP_
