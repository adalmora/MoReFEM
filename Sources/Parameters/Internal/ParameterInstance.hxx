/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 14:09:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_

// IWYU pragma: private, include "Parameters/Internal/ParameterInstance.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    template<class T, typename... ConstructorArgs>
    ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::ParameterInstance(T&& name,
                                                                                         const Domain& domain,
                                                                                         ConstructorArgs&&... arguments)
    : parent(std::forward<T>(name), domain),
      nature_policy(parent::GetName(), domain, std::forward<ConstructorArgs>(arguments)...)
    {
        static_assert(std::is_convertible<self*, parent*>());
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetValue(
        const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const
    {
        return nature_policy::GetValueFromPolicy(local_coords, geom_elt);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetConstantValue() const
    {
        assert(nature_policy::IsConstant());
        return nature_policy::GetConstantValueFromPolicy();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplGetAnyValue() const
    {
        return nature_policy::GetAnyValueFromPolicy();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline bool ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::IsConstant() const
    {
        return nature_policy::IsConstant();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplWrite(std::ostream& out) const
    {
        return nature_policy::WriteFromPolicy(out);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplTimeUpdate()
    {
        // Do nothing in general case.
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SupplTimeUpdate(double time)
    {
        // Do nothing in general case.
        static_cast<void>(time);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        template<ParameterNS::Type, typename... Args> class NaturePolicyT,
        template<ParameterNS::Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>::SetConstantValue(value_type value)
    {
        nature_policy::SetConstantValue(value);
    }


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HXX_
