/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 22 May 2015 14:09:58 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HPP_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HPP_

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Parameters/Internal/Alias.hpp"
#include "Parameters/Parameter.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    /*!
     * \brief Template class that provides actual instantiation of a parameter.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     *
     * \tparam NaturePolicyT Policy that determines how to handle the parameter. Policies are enclosed in
     * ParameterNS::Policy namespace. Policies might be for instance Constant (same value everywhere),
     * LuaFunction (value is provided by a function defined in the input data file; additional arguments are
     * chosen here with the variadic template argument \a Args.).
     *
     */
    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        template<Type> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    class ParameterInstance final
    : public Parameter<TypeT, typename NaturePolicyT<TypeT, Args...>::local_coords_type, TimeDependencyT>,
      public NaturePolicyT<TypeT, Args...>
    {

      public:
        //! Alias to nature policy (constant, Lua function, etc...).
        using nature_policy = NaturePolicyT<TypeT, Args...>;

        //! \copydoc doxygen_hide_alias_self
        using self = ParameterInstance<TypeT, NaturePolicyT, TimeDependencyT, Args...>;

        static_assert(std::is_convertible<self*, nature_policy*>());

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = typename nature_policy::local_coords_type;

        //! Alias to base class.
        using parent = Parameter<TypeT, local_coords_type, TimeDependencyT>;

        //! Alias to return type.
        using return_type = typename parent::return_type;

        //! Alias.
        using value_type = typename parent::value_type;

        //! Alias to traits of parent class.
        using traits = typename parent::traits;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to constant unique_ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to array of unique_ptr.
        template<std::size_t N>
        using array_unique_ptr = std::array<unique_ptr, N>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] name Name of the \a Parameter built.
         * \copydoc doxygen_hide_parameter_domain_arg
         *
         * \param[in] arguments Potential additional arguments required by specific \a Parameter.
         */
        template<class T, typename... ConstructorArgs>
        explicit ParameterInstance(T&& name, const Domain& domain, ConstructorArgs&&... arguments);

        //! Destructor.
        virtual ~ParameterInstance() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        ParameterInstance(const ParameterInstance& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ParameterInstance(ParameterInstance&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ParameterInstance& operator=(const ParameterInstance& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ParameterInstance& operator=(ParameterInstance&& rhs) = delete;

        ///@}

        /*!
         * \copydoc doxygen_hide_parameter_suppl_get_value
         * \param[in] local_coords Local object at which the \a Parameter is evaluated.
         */
        return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;

        //! \copydoc doxygen_hide_parameter_suppl_get_constant_value
        return_type SupplGetConstantValue() const override;


        /*!
         *
         * \copydoc doxygen_hide_parameter_suppl_get_any_value
         */
        return_type SupplGetAnyValue() const override;

        /*!
         * \brief Write the content of the Parameter in a stream.

         * \copydoc doxygen_hide_stream_inout
         */
        void SupplWrite(std::ostream& stream) const override;


        //! \copydoc doxygen_hide_parameter_suppl_time_update
        void SupplTimeUpdate() override;

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        void SupplTimeUpdate(double time) override;

        /*!
         * \brief Whether the parameter varies spatially or not.
         */
        bool IsConstant() const override;

        /*!
         * \brief Enables to modify the constant value of a parameter.
         *
         * \param[in] value New value to set.
         */
        void SetConstantValue(value_type value) override;
    };


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup


#include "Parameters/Internal/ParameterInstance.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_PARAMETER_INSTANCE_HPP_
