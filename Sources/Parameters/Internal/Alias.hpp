/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 11 Oct 2016 14:00:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_INTERNAL_x_ALIAS_HPP_
#define MOREFEM_x_PARAMETERS_x_INTERNAL_x_ALIAS_HPP_

#include "Core/Parameter/TypeEnum.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    //! Convenient alias to avoid many ::MoReFEM::ParameterNS prefix.
    using Type = ::MoReFEM::ParameterNS::Type;

    //! Convenient alias to avoid many ::MoReFEM::ParameterNS prefix.
    using GradientBasedElasticityTensorConfiguration =
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration;


} // namespace MoReFEM::Internal::ParameterNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_INTERNAL_x_ALIAS_HPP_
