/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 29 Mar 2016 10:53:35 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParametersGroup
// \addtogroup ParametersGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_ALIAS_HPP_
#define MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_ALIAS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <vector>

#include "Core/Parameter/TypeEnum.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Core/Parameter/Internal/Traits.hpp"
#include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"
#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"
#include "Parameters/TimeDependency/Policy/FromFile.hpp"
#include "Parameters/TimeDependency/Policy/Functor.hpp"


namespace MoReFEM::ParameterNS
{

    /*!
     * \brief An instantiation of \a TimeDependencyNS::Base where the function is given by a mere std::function.
     *
     * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
     */
    template<ParameterNS::Type TypeT>
    using TimeDependencyFunctor =
        TimeDependencyNS::Base<TypeT, TimeDependencyNS::PolicyNS::Functor<std::function<double(double)>>>;

    /*!
     * \brief An instantiation of \a TimeDependencyNS::Base where the time dependency policy is FromFile.
     *
     * \tparam TypeT Whether the parameter at a local position yields a scalar, a vector or a matrix.
     */
    template<ParameterNS::Type TypeT>
    using TimeDependencyFromFile = TimeDependencyNS::Base<TypeT, TimeDependencyNS::PolicyNS::FromFile>;

} // namespace MoReFEM::ParameterNS


/// @} // addtogroup ParametersGroup


#endif // MOREFEM_x_PARAMETERS_x_TIME_DEPENDENCY_x_ALIAS_HPP_
