/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_matrix_io
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::PetscNS::MatrixNS::ToyModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


    /*!
     * \brief Determines the path and name of the matrix on file and make sure it doesn't exist yet.
     */
    FilesystemNS::File PrepareFile(const TestNS::PetscNS::MatrixNS::ToyModel& model)
    {
        std::ostringstream oconv;
        oconv << model.GetOutputDirectory() << "/matrix_test_IO.bin";

        const auto file = FilesystemNS::File{ oconv.str() };

        if (model.GetMpi().IsRootProcessor())
        {
            if (file.DoExist())
                file.Remove(__FILE__, __LINE__);
        }

        return file;
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(creation, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);
}


BOOST_FIXTURE_TEST_CASE(load_with_duplicated, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) matrix = model.GetMatrix();

    const auto matrix_file = PrepareFile(model);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == false);

    matrix.View(mpi, matrix_file, __FILE__, __LINE__, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == true);

    GlobalMatrix loaded_matrix(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

    loaded_matrix.DuplicateLayout(matrix, MAT_DO_NOT_COPY_VALUES, __FILE__, __LINE__);

    loaded_matrix.LoadBinary(mpi, matrix_file, __FILE__, __LINE__);

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, loaded_matrix, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(load_with_minimal_init, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) matrix = model.GetMatrix();

    const auto matrix_file = PrepareFile(model);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == false);

    matrix.View(mpi, matrix_file, __FILE__, __LINE__, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);

    if (mpi.IsRootProcessor())
        BOOST_CHECK(matrix_file.DoExist() == true);

    GlobalMatrix loaded_matrix(matrix.GetRowNumberingSubset(), matrix.GetColNumberingSubset());

    loaded_matrix.InitMinimalCase(
        mpi, matrix.GetType(__FILE__, __LINE__), __FILE__, __LINE__); // the line that changed from previous case
    loaded_matrix.LoadBinary(mpi, matrix_file, __FILE__, __LINE__);

    auto Noriginal_program_wise_size = matrix.GetProgramWiseSize(__FILE__, __LINE__);
    auto Nloaded_program_wise_size = matrix.GetProgramWiseSize(__FILE__, __LINE__);

    // That is all we can check: there is no guarantee PETSc will use the same layout than if you specified it
    // yourself - as was done in the previous example. In MoReFEM context, it is unlikely you will ever need
    // the current situation - usually when we load data it is to reuse the same partitioning as was defined
    // in a previous run.
    BOOST_CHECK(Noriginal_program_wise_size == Nloaded_program_wise_size);
}


PRAGMA_DIAGNOSTIC(pop)
