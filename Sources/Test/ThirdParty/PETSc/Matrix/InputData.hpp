/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    //! Sole index used for this very simple model.
    enum class MeshIndex : std::size_t { sole = 1 };


    //! Unknown indexes/
    enum class UnknownIndex : std::size_t { unknown1 = 1, unknown2 = 2 };


    //! Sole index used for this very simple model.
    enum class DomainIndex : std::size_t { sole = 1 };


    //! Sole index used for this very simple model.
    enum class FEltSpaceIndex : std::size_t { sole = 1 };

    //! Sole index used for this very simple model.
    enum class NumberingSubsetIndex : std::size_t { row = 1, column = 2 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown1)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown2)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::row)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::column)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
        std::tuple
        <
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription,
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::IndexedSectionDescription,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown1)>::IndexedSectionDescription,
            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown2)>::IndexedSectionDescription,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>::IndexedSectionDescription,

            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::row)>::IndexedSectionDescription,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::column)>::IndexedSectionDescription
        >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::PetscNS::MatrixNS


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_MATRIX_x_INPUT_DATA_HPP_
