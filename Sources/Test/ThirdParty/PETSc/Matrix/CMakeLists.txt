add_library(TestPetscMatrix_lib "")

target_sources(TestPetscMatrix_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ToyModel.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ToyModel.hxx
        ${CMAKE_CURRENT_LIST_DIR}/ToyModel.hpp
)

target_link_libraries(TestPetscMatrix_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestPetscMatrix_lib Test/ThirdParty/PETSc/Matrix Test/ThirdParty/PETSc/Matrix)

include(${CMAKE_CURRENT_LIST_DIR}/MatEqual/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/IO/CMakeLists.txt)

