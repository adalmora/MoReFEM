/*!
// \file
//
*/

#include "Test/ThirdParty/PETSc/Matrix/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::MatrixNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });


        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown1)>>({ "First unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown2)>>({ "Second unknown" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::row)>>({ "Row" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::column)>>({ "Column" });
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixNS
