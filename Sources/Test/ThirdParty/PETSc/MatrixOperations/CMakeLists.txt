add_executable(TestPetscMatrixOperations)

target_sources(TestPetscMatrixOperations
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
        ${CMAKE_CURRENT_LIST_DIR}/README.md
)
          
target_link_libraries(TestPetscMatrixOperations
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_BASIC_TEST_TOOLS})


morefem_boost_test_both_modes(NAME PetscMatrixOperations
                              EXE TestPetscMatrixOperations
                              TIMEOUT 10
                              LUA ${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/MatrixOperations/demo.lua)

morefem_organize_IDE(TestPetscMatrixOperations Test/ThirdParty/PETSc Test/ThirdParty/PETSc/MatrixOperations)
