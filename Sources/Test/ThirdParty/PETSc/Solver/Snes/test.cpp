
#include <cstdlib>

#define BOOST_TEST_MODULE snes
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/Factory.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"

#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace MoReFEM::TestNS
{


    struct Snes
    {
        Snes() = default;

        static SNES GetPetscSnes(Wrappers::Petsc::Snes& snes)
        {
            return snes.Internal();
        }
    };


} // namespace MoReFEM::TestNS


namespace // anonymous
{

    constexpr auto epsilon = 1.e-12;


} // namespace


BOOST_FIXTURE_TEST_CASE(petsc_default_behaviour, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    // Read first values put by default in a newly created SNES object by PETSc
    PetscInt default_max_it;
    PetscReal default_absolute_tol, default_relative_tol, default_step_tol;

    {
        SNES snes;

        int error_code = SNESCreate(mpi.GetCommunicator(), &snes);
        BOOST_REQUIRE(!error_code);

        error_code = SNESGetTolerances(
            snes, &default_absolute_tol, &default_relative_tol, &default_step_tol, &default_max_it, MOREFEM_PETSC_NULL);
        BOOST_REQUIRE(!error_code);
    }

    // Then create a MoReFEM object with the SolverNS::Settings which uses up mostly default values
    PetscInt morefem_max_it;
    PetscReal morefem_absolute_tol, morefem_relative_tol, morefem_step_tol;

    {
        Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
            Wrappers::Petsc::solver_name_type{ "SuperLU_dist" });

        auto morefem_snes =
            Wrappers::Petsc::Snes(mpi,
                                  std::move(solver_settings_with_petsc_default),
                                  nullptr, // current test isn't about the functions provided for non linear use
                                  nullptr,
                                  nullptr,
                                  nullptr,
                                  __FILE__,
                                  __LINE__);

        auto snes_from_morefem = MoReFEM::TestNS::Snes::GetPetscSnes(morefem_snes);


        int error_code = SNESGetTolerances(snes_from_morefem,
                                           &morefem_absolute_tol,
                                           &morefem_relative_tol,
                                           &morefem_step_tol,
                                           &morefem_max_it,
                                           MOREFEM_PETSC_NULL);
        BOOST_REQUIRE(!error_code);
    }

    // And check the default are really well preserved.
    BOOST_CHECK_CLOSE(default_absolute_tol, morefem_absolute_tol, epsilon);
    BOOST_CHECK_CLOSE(default_relative_tol, morefem_relative_tol, epsilon);
    BOOST_CHECK_CLOSE(default_step_tol, morefem_step_tol, epsilon);
    BOOST_CHECK_EQUAL(default_max_it, morefem_max_it);
}


BOOST_FIXTURE_TEST_CASE(invalid_solver_name, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
        Wrappers::Petsc::solver_name_type{ "Whatever" });

    BOOST_CHECK_THROW(auto morefem_snes = Wrappers::Petsc::Snes(
                          mpi,
                          std::move(solver_settings_with_petsc_default),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr,
                          __FILE__,
                          __LINE__),
                      ExceptionNS::Factory::UnregisteredName);
}


BOOST_FIXTURE_TEST_CASE(solver_not_supporting_parallel, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();

    Internal::Wrappers::Petsc::SolverNS::Settings solver_settings_with_petsc_default(
        Wrappers::Petsc::solver_name_type{ "Whatever" });

    BOOST_CHECK_THROW(auto morefem_snes = Wrappers::Petsc::Snes(
                          mpi,
                          std::move(solver_settings_with_petsc_default),
                          nullptr, // current test isn't about the functions provided for non linear use
                          nullptr,
                          nullptr,
                          nullptr,
                          __FILE__,
                          __LINE__),
                      ExceptionNS::Factory::UnregisteredName);
}


PRAGMA_DIAGNOSTIC(pop)
