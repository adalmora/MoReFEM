
#include <cstdlib>

#define BOOST_TEST_MODULE solver_settings
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Settings.hpp"

#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"

using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    constexpr auto epsilon = 1.e-12;

    using input_data_tuple_type = std::tuple<InputDataNS::Petsc<10>>;

    using input_data_type = InputData<input_data_tuple_type>;

    struct MyModelSettings : public ::MoReFEM::ModelSettings<std::tuple<InputDataNS::Petsc<10>::IndexedSectionDescription>>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override
        {
            SetDescription<InputDataNS::Petsc<10>>({ "Solver" });
        }
    };


} // namespace


BOOST_AUTO_TEST_CASE(basic_check)
{
    namespace petsc = Wrappers::Petsc;

    petsc::absolute_tolerance_type absolute_tolerance{ 1.e-6 };

    petsc::relative_tolerance_type relative_tolerance{ 1.e-7 };
    petsc::set_restart_type set_restart{ 5ul };
    petsc::max_iteration_type max_iteration{ 1234ul };
    petsc::preconditioner_name_type preconditioner_name{ PCLU };
    petsc::solver_name_type solver_name{ "SuperLU_dist" };
    petsc::step_size_tolerance_type step_size_tolerance{ 1.e-9 };


    Internal::Wrappers::Petsc::SolverNS::Settings settings(absolute_tolerance,
                                                           relative_tolerance,
                                                           set_restart,
                                                           max_iteration,
                                                           preconditioner_name,
                                                           solver_name,
                                                           step_size_tolerance);

    BOOST_CHECK_CLOSE(settings.GetAbsoluteTolerance().Get(), absolute_tolerance.Get(), epsilon);
    BOOST_CHECK_CLOSE(settings.GetRelativeTolerance().Get(), relative_tolerance.Get(), epsilon);
    BOOST_CHECK_CLOSE(settings.GetStepSizeTolerance().Get(), step_size_tolerance.Get(), epsilon);

    BOOST_CHECK_EQUAL(settings.GetRestart().Get(), set_restart.Get());
    BOOST_CHECK_EQUAL(settings.GetMaxIterations().Get(), max_iteration.Get());
    BOOST_CHECK_EQUAL(settings.GetPreconditionerName().Get(), preconditioner_name.Get());
    BOOST_CHECK_EQUAL(settings.GetSolverName().Get(), solver_name.Get());
}


BOOST_FIXTURE_TEST_CASE(from_input_data_section, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) morefem_data = TestNS::InitMoReFEMDataFromCLI<input_data_type, MyModelSettings>();

    decltype(auto) section = InputDataNS::ExtractSection<InputDataNS::Petsc<10>>::Value(morefem_data.GetInputData());

    Internal::Wrappers::Petsc::SolverNS::Settings settings(section);

    BOOST_CHECK_CLOSE(settings.GetAbsoluteTolerance().Get(), 1.e-10, epsilon);
    BOOST_CHECK_CLOSE(settings.GetRelativeTolerance().Get(), 1.e-9, epsilon);
    BOOST_CHECK_CLOSE(settings.GetStepSizeTolerance().Get(), 1.e-8, epsilon);

    BOOST_CHECK_EQUAL(settings.GetRestart().Get(), 200);
    BOOST_CHECK_EQUAL(settings.GetMaxIterations().Get(), 1000);
    BOOST_CHECK_EQUAL(settings.GetPreconditionerName().Get(), "lu");
    BOOST_CHECK_EQUAL(settings.GetSolverName().Get(), "SuperLU_dist");
}


PRAGMA_DIAGNOSTIC(pop)
