/*!
// \file
//
*/

#include "Test/ThirdParty/PETSc/VectorIO/InputData.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>>({ "Sole" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>({ "Sole" });
    }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS
