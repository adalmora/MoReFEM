/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE petsc_vector_io

#include "Utilities/AsciiOrBinary/Exceptions/BinarySerialization.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Exceptions/Vector.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/ThirdParty/PETSc/VectorIO/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type =
        TestNS::FixtureNS::Model
        <
            TestNS::PetscNS::VectorIONS::ToyModel,
            TestNS::FixtureNS::call_run_method_at_first_call::no
        >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(processor_wise, fixture_type)

BOOST_AUTO_TEST_CASE(creation)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) vector = model.GetVector();

    FilesystemNS::File ascii_file{ model.GetProcessorWiseAsciiFile() };
    FilesystemNS::File binary_file{ model.GetProcessorWiseBinaryFile() };

    BOOST_CHECK(ascii_file.DoExist() == false);
    BOOST_CHECK(binary_file.DoExist() == false);

    vector.Print<MpiScale::processor_wise>(mpi, binary_file, __FILE__, __LINE__);

    // As Lua file is set to ask binary file.
    BOOST_CHECK(ascii_file.DoExist() == false);
    BOOST_CHECK(binary_file.DoExist() == true);

    vector.Print<MpiScale::processor_wise>(mpi, ascii_file, __FILE__, __LINE__, binary_or_ascii::ascii);

    BOOST_CHECK(ascii_file.DoExist() == true);
    BOOST_CHECK(binary_file.DoExist() == true);
}


BOOST_AUTO_TEST_CASE(load_ascii)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    auto ascii_file = FilesystemNS::File{ model.GetProcessorWiseAsciiFile() };
    BOOST_CHECK(ascii_file.DoExist() == true); // created in previous test!
    decltype(auto) vector = model.GetVector();

    std::vector<PetscInt> ghost_padding;

    if (mpi.Nprocessor<int>() > 1)
        ghost_padding = vector.GetGhostPadding();

    {
        Wrappers::Petsc::Vector from_file;
        from_file.InitParallelFromProcessorWiseAsciiFile(
            mpi,
            static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
            static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
            ghost_padding,
            ascii_file,
            __FILE__,
            __LINE__);

        std::string inequality_description;
        Wrappers::Petsc::AreEqual(
            from_file, vector, NumericNS::DefaultEpsilon<double>(), inequality_description, __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(inequality_description, "");
    }


    {

        Wrappers::Petsc::Vector from_file;
        BOOST_CHECK_THROW(from_file.InitParallelFromProcessorWiseBinaryFile(
                              mpi,
                              static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                              static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                              ghost_padding,
                              ascii_file,
                              __FILE__,
                              __LINE__),
                          MoReFEM::ExceptionNS::BinaryNS::LoadBinaryFileException);
    }
}

BOOST_AUTO_TEST_CASE(load_binary)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    auto binary_file = FilesystemNS::File{ model.GetProcessorWiseBinaryFile() };
    BOOST_CHECK(binary_file.DoExist() == true); // created in previous test!
    decltype(auto) vector = model.GetVector();

    std::vector<PetscInt> ghost_padding;

    if (mpi.Nprocessor<int>() > 1)
        ghost_padding = vector.GetGhostPadding();

    {
        Wrappers::Petsc::Vector from_file;
        from_file.InitParallelFromProcessorWiseBinaryFile(
            mpi,
            static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
            static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
            ghost_padding,
            binary_file,
            __FILE__,
            __LINE__);

        std::string inequality_description;
        Wrappers::Petsc::AreEqual(
            from_file, vector, NumericNS::DefaultEpsilon<double>(), inequality_description, __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(inequality_description, "");
    }

    {
        Wrappers::Petsc::Vector from_file;
        BOOST_CHECK_THROW(from_file.InitParallelFromProcessorWiseAsciiFile(
                              mpi,
                              static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                              static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                              ghost_padding,
                              binary_file,
                              __FILE__,
                              __LINE__),

                          MoReFEM::Wrappers::Petsc::ExceptionNS::InvalidAsciiFile);
    }
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(program_wise, fixture_type)

BOOST_AUTO_TEST_CASE(creation)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) vector = model.GetVector();

    auto ascii_file = FilesystemNS::File{ model.GetProgramWiseAsciiFile() };
    auto binary_file = FilesystemNS::File{ model.GetProgramWiseBinaryFile() };

    BOOST_CHECK(ascii_file.DoExist() == false);
    BOOST_CHECK(binary_file.DoExist() == false);

    vector.Print<MpiScale::program_wise>(mpi, binary_file, __FILE__, __LINE__);

    // As Lua file is set to ask binary file.
    if (mpi.IsRootProcessor())
    {
        BOOST_CHECK(ascii_file.DoExist() == false);
        BOOST_CHECK(binary_file.DoExist() == true);
    }

    vector.Print<MpiScale::program_wise>(mpi, ascii_file, __FILE__, __LINE__, binary_or_ascii::ascii);

    if (mpi.IsRootProcessor())
    {
        BOOST_CHECK(ascii_file.DoExist() == true);
        BOOST_CHECK(binary_file.DoExist() == true);
    }
}


BOOST_AUTO_TEST_CASE(load_binary)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    decltype(auto) vector = model.GetVector();
    Wrappers::Petsc::Vector from_file;

    std::vector<PetscInt> ghost_padding;

    // We explicitly point out here the filename is <b>not</b> used on non root ranks.
    // Something that was not obvious to me - see #1768 for instance
    auto binary_file = FilesystemNS::File{};

    if (mpi.Nprocessor<int>() > 1)
        ghost_padding = vector.GetGhostPadding();

    if (mpi.IsRootProcessor() || mpi.Nprocessor<int>() == 1)
        binary_file = FilesystemNS::File{ model.GetProgramWiseBinaryFile() };

    from_file.InitFromProgramWiseBinaryFile(mpi,
                                            static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
                                            static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
                                            ghost_padding,
                                            binary_file,
                                            __FILE__,
                                            __LINE__);

    std::string inequality_description;
    Wrappers::Petsc::AreEqual(
        from_file, vector, NumericNS::DefaultEpsilon<double>(), inequality_description, __FILE__, __LINE__);

    BOOST_CHECK_EQUAL(inequality_description, "");
}

// No load_ascii: not foreseen apparently in PETSc interface!


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(sequential_only, fixture_type)

BOOST_AUTO_TEST_CASE(load_ascii)
{
    decltype(auto) model = GetModel();
    decltype(auto) mpi = model.GetMpi();

    if (mpi.Nprocessor<int>() == 1)
    {
        auto ascii_file = FilesystemNS::File{ model.GetProgramWiseAsciiFile() };
        auto binary_file = FilesystemNS::File{ model.GetProgramWiseBinaryFile() };

        BOOST_CHECK(ascii_file.DoExist() == true);  // created in previous test!
        BOOST_CHECK(binary_file.DoExist() == true); // created in previous test!
        decltype(auto) vector = model.GetVector();


        std::vector<PetscInt> ghost_padding;

        BOOST_CHECK_EQUAL(vector.GetProcessorWiseSize(__FILE__, __LINE__),
                          vector.GetProgramWiseSize(__FILE__, __LINE__));

        {
            Wrappers::Petsc::Vector from_file;
            from_file.InitSequentialFromAsciiFile(mpi, ascii_file, __FILE__, __LINE__);

            std::string inequality_description;
            Wrappers::Petsc::AreEqual(
                from_file, vector, NumericNS::DefaultEpsilon<double>(), inequality_description, __FILE__, __LINE__);

            BOOST_CHECK_EQUAL(inequality_description, "");
        }

        {
            Wrappers::Petsc::Vector from_file;
            BOOST_CHECK_THROW(from_file.InitSequentialFromAsciiFile(mpi, binary_file, __FILE__, __LINE__),
                              MoReFEM::Wrappers::Petsc::ExceptionNS::InvalidAsciiFile);
        }
    }
}


BOOST_AUTO_TEST_SUITE_END()


PRAGMA_DIAGNOSTIC(pop)
