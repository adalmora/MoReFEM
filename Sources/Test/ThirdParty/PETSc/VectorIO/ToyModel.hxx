/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HXX_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HXX_

// IWYU pragma: private, include "Test/ThirdParty/PETSc/VectorIO/ToyModel.hpp"


namespace MoReFEM::TestNS::PetscNS::VectorIONS
{


    inline const GlobalVector& ToyModel::GetVector() const noexcept
    {
        assert(!(!vector_));
        return *vector_;
    }


    inline const FilesystemNS::Directory& ToyModel::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


    inline auto ToyModel::GetNonCstModelSettings() noexcept -> ModelSettings&
    {
        return model_settings_;
    }


} // namespace MoReFEM::TestNS::PetscNS::VectorIONS


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_P_E_T_SC_x_VECTOR_I_O_x_TOY_MODEL_HXX_
