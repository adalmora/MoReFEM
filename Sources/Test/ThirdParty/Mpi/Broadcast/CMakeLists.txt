add_executable(TestMpiBroadcast)

target_sources(TestMpiBroadcast
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestMpiBroadcast
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_UTILITIES}>)
    
morefem_boost_test_parallel_mode(NAME MpiBroadcast
                                 EXE TestMpiBroadcast
                                 TIMEOUT 20
                                 NPROC 3)

morefem_organize_IDE(TestMpiBroadcast Test/ThirdParty/Mpi Test/ThirdParty/Mpi/Broadcast)
