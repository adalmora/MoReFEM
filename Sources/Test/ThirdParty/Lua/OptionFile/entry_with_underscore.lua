transient = {


    -- Time at the beginning of the code (in seconds).
    -- Expected format: VALUE
    -- Constraint: v >= 0.
    init_time = 0.,


    -- Time step between two iterations, in seconds.
    -- Expected format: VALUE
    -- Constraint: v > 0.
    timeStep = 0.1,


    -- Maximum time, if set to zero run a static case.
    -- Expected format: VALUE
    -- Constraint: v >= 0.
    timeMax = .1

} -- transient
