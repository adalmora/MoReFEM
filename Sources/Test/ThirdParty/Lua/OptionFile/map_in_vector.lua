ValueOutsideBrace = 3.14

TransientSource4 = {

    nature = { "lua_function", "constant", "piecewise_constant_by_domain"},

    value = {
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],
        0.,
    { [1] = 0., [3] = 14.5, [4] = -31.231 }} -- didn't pass before #1468

} -- TransientSource4


TransientSource5 = {

    nature = { "lua_function", "constant", "piecewise_constant_by_domain"},

    value = {
        [[
        function (x, y, z)
        return 100. * math.exp(-((x - 0.5)^2 + (y - 0.5)^2)/(0.1^2));
        end
        ]],
        0.,
    { [1] = 0., [3] = 14.5, [4] = -31.231 } },

    whatever = {

        sublevel = { 4 },

        sublevel2 = 3

    }

} -- TransientSource5



InitialCondition1 = {


    nature = { 'lua_function', 'constant', 'constant' },

    value = { [[
				function(x, y, z)
                if (((x >= 4) and (x <= 5.99)) or ((x >= 7.01) and (x <= 8.99))) then
                    return 1.;
                else
                    return 0.
                end
            end
        ]],
        0., 0. }


} -- InitialCondition1
