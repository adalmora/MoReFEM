/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Mar 2018 17:18:25 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstddef> // IWYU pragma: keep
#include <sstream>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#define BOOST_TEST_MODULE lua_option_file
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    /*!
     * \brief Just an helper function to make the tests more concise.
     *
     * The function returns by value what was read, and constraint is bypassed in the call by default.
     */
    template<class T>
    T ReadOptionFile(::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
                     const std::string& key,
                     const char* invoking_file,
                     int invoking_line,
                     std::string_view constraint = "")
    {
        T ret;
        lua_option_file.Read(key, constraint, ret, invoking_file, invoking_line);
        return ret;
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(parameters_properly_read, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__)));

    auto& lua_option_file = *(ptr.get());

    BOOST_CHECK(NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__), 2.44));

    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "section1.string_value", __FILE__, __LINE__) == "string");
    BOOST_CHECK(NumericNS::AreEqual(
        ReadOptionFile<double>(lua_option_file, "section1.double_value", __FILE__, __LINE__), 5.215));

    BOOST_CHECK(ReadOptionFile<std::size_t>(lua_option_file, "section1.int_value", __FILE__, __LINE__) == 10u);
    BOOST_CHECK_THROW(ReadOptionFile<std::size_t>(lua_option_file, "section1.double_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK(
        ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__).size()
        == 3ul);
    BOOST_CHECK(
        ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[0]
        == "foo");
    BOOST_CHECK(
        ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[1]
        == "bar");
    BOOST_CHECK(
        ReadOptionFile<std::vector<std::string>>(lua_option_file, "section1.vector_value", __FILE__, __LINE__)[2]
        == "baz");
    BOOST_CHECK_THROW(ReadOptionFile<int>(lua_option_file, "section1.vector_value", __FILE__, __LINE__),
                      std::exception);
    BOOST_CHECK_THROW(ReadOptionFile<std::vector<int>>(lua_option_file, "section1.int_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK(ReadOptionFile<std::string>(lua_option_file, "section1.string_value", __FILE__, __LINE__) == "string");

    BOOST_CHECK(
        (ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__).size() == 3ul));
    BOOST_CHECK(
        (ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[3] == 5));
    BOOST_CHECK(
        (ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[4] == 7));
    BOOST_CHECK(
        (ReadOptionFile<std::map<int, int>>(lua_option_file, "section1.map_value", __FILE__, __LINE__)[5] == 8));

    BOOST_CHECK_THROW(ReadOptionFile<std::string>(lua_option_file, "unknown_key", __FILE__, __LINE__), std::exception);
    BOOST_CHECK_THROW(ReadOptionFile<double>(lua_option_file, "section1.string_value", __FILE__, __LINE__),
                      std::exception);

    BOOST_CHECK_THROW(ReadOptionFile<double>(lua_option_file, "section1.invalid_value", __FILE__, __LINE__),
                      std::exception);

    // When I have time to investigate how to do so in Boost
    // CHECK_ABORT(ReadOptionFile<double>(lua_option_file, __FILE__, __LINE__));
}


BOOST_FIXTURE_TEST_CASE(constraints, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__)));
    auto& lua_option_file = *(ptr.get());

    BOOST_CHECK_THROW(
        NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__, "v > 5."), 2.44),
        std::exception);
    /* BOOST_CHECK_NO_THROW */ (
        NumericNS::AreEqual(ReadOptionFile<double>(lua_option_file, "root_value", __FILE__, __LINE__, "v < 3."), 2.44));

    /* BOOST_CHECK_NO_THROW */ (ReadOptionFile<std::vector<std::string>>(
        lua_option_file, "section1.vector_value", __FILE__, __LINE__, "value_in(v, { 'foo', 'bar', 'baz' })"));

    BOOST_CHECK_THROW(
        ReadOptionFile<std::vector<std::string>>(
            lua_option_file, "section1.vector_value", __FILE__, __LINE__, "value_in(v, { 'bar', 'baz' })"),
        std::exception);

    ReadOptionFile<std::vector<int>>(lua_option_file, "section1.vector_int_value", __FILE__, __LINE__, "v < 10");

    BOOST_CHECK_THROW(
        ReadOptionFile<std::vector<int>>(lua_option_file, "section1.vector_int_value", __FILE__, __LINE__, "v > 10"),
        std::exception);
}


BOOST_FIXTURE_TEST_CASE(lua_functions, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__));
    auto& lua_option_file = *(ptr.get());

    decltype(auto) one_arg_fct = ReadOptionFile<Wrappers::Lua::Function<double(double)>>(
        lua_option_file, "section2.one_arg_function", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(one_arg_fct(3.), -3.));

    decltype(auto) several_arg_function = ReadOptionFile<Wrappers::Lua::Function<double(double, double, double)>>(
        lua_option_file, "section2.several_arg_function", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(several_arg_function(3., 4., 5.), 2.));


    decltype(auto) function_with_if = ReadOptionFile<Wrappers::Lua::Function<double(double, double, double)>>(
        lua_option_file, "section2.function_with_if", __FILE__, __LINE__);

    BOOST_CHECK(NumericNS::AreEqual(function_with_if(5., 0., 0.), 1.));
    BOOST_CHECK(NumericNS::AreEqual(function_with_if(15., 0., 0.), 0.));
}


BOOST_FIXTURE_TEST_CASE(redundant, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/redundancy.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__)),
                        std::exception);
}


BOOST_FIXTURE_TEST_CASE(redundant_in_section, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/redundancy_in_section.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr = nullptr;
    BOOST_REQUIRE_THROW(ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__)),
                        std::exception);
}


BOOST_FIXTURE_TEST_CASE(invalid_lua, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/demo_lua_option_file.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__));
    auto& lua_option_file = *(ptr.get());

    BOOST_REQUIRE_THROW(ReadOptionFile<Wrappers::Lua::Function<double(double)>>(
                            lua_option_file, "section3.invalid_function", __FILE__, __LINE__),
                        std::exception);
}


BOOST_FIXTURE_TEST_CASE(map_in_vector, TestNS::FixtureNS::TestEnvironment)
{
    // Introduced after #1468, in which two braces on the same line wreak havoc...
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/map_in_vector.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;

    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(),
                      "[ValueOutsideBrace, "
                      "TransientSource4.nature, TransientSource4.value, "
                      "TransientSource5.nature, TransientSource5.value, "
                      "TransientSource5.whatever.sublevel, "
                      "TransientSource5.whatever.sublevel2, "
                      "InitialCondition1.nature, InitialCondition1.value]");
}


BOOST_FIXTURE_TEST_CASE(no_section, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/no_section.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[a, b, c, d, e, f]");
}


BOOST_FIXTURE_TEST_CASE(entry_with_underscore, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/entry_with_underscore.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    ::MoReFEM::Wrappers::Lua::OptionFile lua(input_file, __FILE__, __LINE__);

    std::ostringstream oconv;
    Utilities::PrintContainer<>::Do(lua.GetEntryKeyList(),
                                    oconv,
                                    ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                    ::MoReFEM::PrintNS::Delimiter::opener("["),
                                    ::MoReFEM::PrintNS::Delimiter::closer("]"));

    BOOST_CHECK_EQUAL(oconv.str(), "[transient.init_time, transient.timeStep, transient.timeMax]");
}


BOOST_FIXTURE_TEST_CASE(advanced_map, TestNS::FixtureNS::TestEnvironment)
{
    std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Lua/OptionFile/advanced_map.lua" };
    FilesystemNS::File input_file{ std::move(path) };


    std::unique_ptr<::MoReFEM::Wrappers::Lua::OptionFile> ptr;
    /* BOOST_REQUIRE_NO_THROW */ (ptr.reset(new ::MoReFEM::Wrappers::Lua::OptionFile(input_file, __FILE__, __LINE__)));

    auto& lua_option_file = *(ptr.get());

    {
        const auto simple_map_value =
            ReadOptionFile<std::map<int, int>>(lua_option_file, "simple_map_value", __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(simple_map_value.size(), 3ul);
        BOOST_CHECK_EQUAL(simple_map_value.at(3), 5);
        BOOST_CHECK_EQUAL(simple_map_value.at(4), 7);
        BOOST_CHECK_EQUAL(simple_map_value.at(5), 8);
    }

    {
        const auto map_of_string =
            ReadOptionFile<std::map<std::string, std::string>>(lua_option_file, "map_of_string", __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(map_of_string.size(), 3ul);
        BOOST_CHECK_EQUAL(map_of_string.at("first"), "foo");
        BOOST_CHECK_EQUAL(map_of_string.at("second"), "bar");
        BOOST_CHECK_EQUAL(map_of_string.at("third"), "baz");
    }

    {
        const auto map_of_vector = ReadOptionFile<std::map<std::string, std::vector<int>>>(
            lua_option_file, "map_of_vector", __FILE__, __LINE__);
        std::vector<int> expected_first{ 2, 3, 5, 7 };
        BOOST_CHECK(map_of_vector.at("first") == expected_first);
        std::vector<int> expected_second{ 11, 13, 17, 23, 29 };
        BOOST_CHECK(map_of_vector.at("second") == expected_second);
    }
}


PRAGMA_DIAGNOSTIC(pop)
