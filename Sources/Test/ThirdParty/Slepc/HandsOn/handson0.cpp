/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr>
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_handson_0
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcSys.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(hello_world)
{
    int ierr;

    decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
    auto argc = master_test_suite.argc;
    auto argv = master_test_suite.argv;

    ierr = SlepcInitialize(&argc, &argv, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL);
    BOOST_CHECK_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, "Hello world\n");
    BOOST_CHECK_EQUAL(ierr, 0);
    ierr = SlepcFinalize();
    BOOST_CHECK_EQUAL(ierr, 0);
}


PRAGMA_DIAGNOSTIC(pop)
