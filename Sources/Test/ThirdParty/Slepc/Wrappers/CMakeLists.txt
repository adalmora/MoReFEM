# Wrappers over the hands on 1 from Slepc website
add_executable(TestSlepcWrappers1)

target_sources(TestSlepcWrappers1
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/wrappers_handson_1.cpp
)
            
target_link_libraries(TestSlepcWrappers1
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestSlepcWrappers1 Test/ThirdParty/Slepc/Wrappers Test/ThirdParty/Slepc/Wrappers)

morefem_boost_test_sequential_mode(NAME SlepcWrappers1
                                   EXE TestSlepcWrappers1
                                   TIMEOUT 5)

# Wrappers over the hands on 3 from Slepc website
add_executable(TestSlepcWrappers3)

target_sources(TestSlepcWrappers3
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/wrappers_handson_3.cpp
)
            
target_link_libraries(TestSlepcWrappers3
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestSlepcWrappers3 Test/ThirdParty/Slepc/Wrappers Test/ThirdParty/Slepc/Wrappers)

morefem_boost_test_sequential_mode(NAME SlepcWrappers3
                                   EXE TestSlepcWrappers3
                                   TIMEOUT 5)
