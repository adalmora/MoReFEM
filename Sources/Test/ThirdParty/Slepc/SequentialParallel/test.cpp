/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr>
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_sequential_parallel

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"

#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;

namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::SlepcNS::ToyModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on

    constexpr auto epsilon = 1.e-9;


} // namespace


BOOST_FIXTURE_TEST_CASE(Standard_Symmetric_Eigenvalue_Problem, fixture_type)
{
    // The model essentially initializes basic stuff and provides a square matrix (sequentially or in parallel)
    decltype(auto) model = GetModel();
    decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(TestNS::SlepcNS::MeshIndex::sole));
    decltype(auto) mpi = GetMpi();

    decltype(auto) problem_matrix = model.GetMatrix();

    Wrappers::Slepc::Eps eps(mpi, problem_matrix, Wrappers::Slepc::problem_type::non_hermitian, __FILE__, __LINE__);

    eps.Solve(__FILE__, __LINE__);

    BOOST_REQUIRE_EQUAL(eps.NconvergedEigenPairs(__FILE__, __LINE__), 1ul);

    const auto [re, im] = eps.GetEigenPair(0ul, __FILE__, __LINE__);

    GlobalVector lhs_result(problem_matrix.GetColNumberingSubset());
    AllocateGlobalVector(god_of_dof, lhs_result);

    GlobalVector rhs_result(problem_matrix.GetColNumberingSubset());
    AllocateGlobalVector(god_of_dof, rhs_result);

    {
        Wrappers::Petsc::MatMult(problem_matrix, re.GetEigenVector(), lhs_result, __FILE__, __LINE__);

        rhs_result.Copy(re.GetEigenVector(), __FILE__, __LINE__);
        rhs_result.Scale(re.GetEigenValue(), __FILE__, __LINE__);

        std::string inequality_description;

        BOOST_CHECK(AreEqual(lhs_result, rhs_result, epsilon, inequality_description, __FILE__, __LINE__));
    }

    {
        Wrappers::Petsc::MatMult(problem_matrix, im.GetEigenVector(), lhs_result, __FILE__, __LINE__);

        rhs_result.Copy(im.GetEigenVector(), __FILE__, __LINE__);
        rhs_result.Scale(im.GetEigenValue(), __FILE__, __LINE__);

        std::string inequality_description;

        BOOST_CHECK(AreEqual(lhs_result, rhs_result, epsilon, inequality_description, __FILE__, __LINE__));
    }
}


PRAGMA_DIAGNOSTIC(pop)
