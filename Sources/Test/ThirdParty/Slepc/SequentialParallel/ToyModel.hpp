/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HPP_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HPP_

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"

#include "Test/ThirdParty/Slepc/SequentialParallel/InputData.hpp"


namespace MoReFEM::TestNS::SlepcNS
{

    /*!
     * \brief Toy model used to perform Slepc test.
     *
     * Its role is mostly to fill a <b>square</b> \a GlobalMatrix in a realistic case (mass operator  over a mesh).
     */
    class ToyModel : public Crtp::CrtpMpi<ToyModel>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = ToyModel;

      public:
        //! Define as a trait - I need that to use it with \a TestNS::FixtureNS::Model .
        using morefem_data_type = ::MoReFEM::TestNS::SlepcNS::morefem_data_type;

        //! \copydoc doxygen_hide_model_specific_model_settings
        using model_settings_type = ModelSettings;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        ToyModel(const morefem_data_type& morefem_data);

        //! Destructor.
        ~ToyModel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ToyModel(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ToyModel(ToyModel&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ToyModel& operator=(const ToyModel& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ToyModel& operator=(ToyModel&& rhs) = delete;

        ///@}

        //! Returns the path to result directory.
        const FilesystemNS::Directory& GetOutputDirectory() const noexcept;

        //! Accessor to the matrix used for tests.
        const GlobalMatrix& GetMatrix() const noexcept;

        //! \copydoc doxygen_hide_model_settings_non_constant_accessor
        ModelSettings& GetNonCstModelSettings() noexcept;

      private:
        //! Global matrix which is used in the tests.
        GlobalMatrix::unique_ptr matrix_ = nullptr;

        //! Path to result directory.
        const FilesystemNS::Directory output_directory_;

        //! \copydoc doxygen_hide_model_settings_attribute
        ModelSettings model_settings_;
    };


} // namespace MoReFEM::TestNS::SlepcNS


#include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HPP_
