/*!
// \file
//
*/

#include "Test/ThirdParty/Slepc/SequentialParallel/InputData.hpp"


namespace MoReFEM::TestNS::SlepcNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::sole)>>({ "Sole" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>({ "Sole" });
    }


} // namespace MoReFEM::TestNS::SlepcNS
