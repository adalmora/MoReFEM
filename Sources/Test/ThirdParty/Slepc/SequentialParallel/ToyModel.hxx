/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HXX_
#define MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HXX_

// IWYU pragma: private, include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"


namespace MoReFEM::TestNS::SlepcNS
{


    inline const GlobalMatrix& ToyModel::GetMatrix() const noexcept
    {
        assert(!(!matrix_));
        return *matrix_;
    }


    inline const FilesystemNS::Directory& ToyModel::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


    inline auto ToyModel::GetNonCstModelSettings() noexcept -> ModelSettings&
    {
        return model_settings_;
    }

} // namespace MoReFEM::TestNS::SlepcNS


#endif // MOREFEM_x_TEST_x_THIRD_PARTY_x_SLEPC_x_SEQUENTIAL_PARALLEL_x_TOY_MODEL_HXX_
