/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 24 Aug 2018 17:15:01 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE solid_poisson_without_young
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"

using namespace MoReFEM;


namespace MoReFEM
{


    struct TestHelper
    {
        TestHelper()
        {
            static bool first = true;

            if (first)
            {
                first = false;
                DomainManager::CreateOrGetInstance(__FILE__, __LINE__)
                    .Create(DomainNS::unique_id{ 1 }, { MeshNS::unique_id{ 1ul } }, { 2 }, {}, {});
            }
        }
    };


} // namespace MoReFEM


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(poisson_no_young, TestNS::FixtureNS::TestEnvironment)
{
    // clang-format off
    using input_data_tuple =
        std::tuple
        <
            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Result
        >;
    // clang-format on

    using input_data_type = InputData<input_data_tuple>;

    FilesystemNS::File lua_file{ std::filesystem::path{ "${MOREFEM_ROOT}/Sources/Test/ParameterInstances/Solid/"
                                                        "demo_poisson_without_young.lua" } };

   MoReFEMData<input_data_type, MoReFEM::TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh" };
    FilesystemNS::File mesh_file{ std::move(path) };

    Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__)
        .Create(MeshNS::unique_id{ 1ul }, mesh_file, 2, MeshNS::Format::Medit, 1.);

    TestHelper test;

    QuadratureRulePerTopology quad_rule_per_topology(3, 3);

    BOOST_CHECK_THROW(
        std::make_unique<Solid>(
            morefem_data,
            DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(DomainNS::unique_id{ 1 }, __FILE__, __LINE__),
            quad_rule_per_topology),
        std::exception);
}

PRAGMA_DIAGNOSTIC(pop)
