/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 28 Jul 2015 11:51:43 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::CoordsInParallelNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh1 = 1, mesh2 = 2 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { mesh1 = 1, mesh2 = 2 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { mesh1 = 1, mesh2 = 2 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { unknown = 1 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { mesh1 = 1, mesh2 = 2 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
        using input_data_tuple = std::tuple
        <
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>,
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>,

            InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,

            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>,
            InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>,

            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh1)>,
            InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh2)>,

            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>,
            InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>,

            InputDataNS::Result

        >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>::IndexedSectionDescription,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>::IndexedSectionDescription,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh1)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh2)>::IndexedSectionDescription,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::CoordsInParallelNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_IN_PARALLEL_x_INPUT_DATA_HPP_
