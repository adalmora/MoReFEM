/*!
// \file
//
*/

#include "Test/Geometry/CoordsInParallel/InputData.hpp"


namespace MoReFEM::TestNS::CoordsInParallelNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh1)>>(
            { "Finite element space for mesh 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mesh2)>>(
            { "Finite element space for mesh 2" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mesh2)>>({ "For mesh 2" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>>({ "Sole" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh2)>>({ "For mesh 2" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh1)>>({ "For mesh 1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::mesh2)>>({ "For mesh 2" });
    }


} // namespace MoReFEM::TestNS::CoordsInParallelNS
