/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE coords_in_parallel
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/CoordsInParallel/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using model_type =
    TestNS::BareModel
    <
        MoReFEM::TestNS::CoordsInParallelNS::morefem_data_type
    >;

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


} // namespace


BOOST_FIXTURE_TEST_CASE(count_coords_in_parallel, fixture_type)
{

#ifndef NDEBUG

    decltype(auto) model = GetModel();

    decltype(auto) mesh1 = model.GetMesh(AsMeshId(TestNS::CoordsInParallelNS::MeshIndex::mesh1));
    decltype(auto) mesh2 = model.GetMesh(AsMeshId(TestNS::CoordsInParallelNS::MeshIndex::mesh2));

    const auto Nmesh1_coords = mesh1.NprocessorWiseCoord() + mesh1.NghostCoord();
    const auto Nmesh2_coords = mesh2.NprocessorWiseCoord() + mesh2.NghostCoord();

    const auto Ncoords_in_meshes = Nmesh1_coords + Nmesh2_coords;

    BOOST_CHECK_EQUAL(Ncoords_in_meshes, Coords::Nobjects());

#endif // NDEBUG
}
