add_executable(TestCoordsInParallel)

target_sources(TestCoordsInParallel
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
)
          
target_link_libraries(TestCoordsInParallel
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestCoordsInParallel Test/Geometry Test/Geometry/CoordsInParallel)

morefem_boost_test_parallel_mode(NAME CoordsInParallel
                                 EXE TestCoordsInParallel
                                 LUA ${MOREFEM_ROOT}/Sources/Test/Geometry/CoordsInParallel/demo.lua
                                 TIMEOUT 20)

