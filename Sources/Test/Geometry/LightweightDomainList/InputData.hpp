/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::LightweightDomainListNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        domain = 1,
        reference_13 = 13,
        reference_14 = 14,
        reference_15 = 15,
        reference_16 = 16,
    };


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused for
    //! current test.
    constexpr auto sole = 0;


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,
        InputDataNS::Domain<13ul>,
        InputDataNS::Domain<14ul>,
        InputDataNS::Domain<15ul>,
        InputDataNS::Domain<16ul>,
        InputDataNS::LightweightDomainList<1>,

        InputDataNS::Unknown<sole>,
        InputDataNS::NumberingSubset<sole>,
        InputDataNS::FEltSpace<sole>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<sole>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::Domain<13ul>::IndexedSectionDescription,
        InputDataNS::Domain<14ul>::IndexedSectionDescription,
        InputDataNS::Domain<15ul>::IndexedSectionDescription,
        InputDataNS::Domain<16ul>::IndexedSectionDescription,
        InputDataNS::LightweightDomainList<1>::IndexedSectionDescription,
        InputDataNS::Unknown<sole>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<sole>::IndexedSectionDescription
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::LightweightDomainListNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_x_INPUT_DATA_HPP_
