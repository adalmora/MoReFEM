/*!
// \file
//
*/

#include "Test/Geometry/LightweightDomainList/InputData.hpp"


namespace MoReFEM::TestNS::LightweightDomainListNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain)" });
        SetDescription<InputDataNS::Domain<13ul>>({ " Reference 13" });
        SetDescription<InputDataNS::Domain<14ul>>({ " Reference 14" });
        SetDescription<InputDataNS::Domain<15ul>>({ " Reference 15" });
        SetDescription<InputDataNS::Domain<16ul>>({ " Reference 16" });
        SetDescription<InputDataNS::LightweightDomainList<1>>({ " 1" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::LightweightDomainListNS
