/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 22:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE coloring
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Geometry/Mesh/ComputeColoring.hpp"

#include "Test/Geometry/Coloring/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{

    using model_type = TestNS::BareModel<TestNS::ColoringNS::morefem_data_type>;

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


    //! For each \a Coords, compute the list of associated \a GeometricElt of the \a dimension considered.
    std::unordered_map<Coords::shared_ptr, GeometricElt::vector_shared_ptr>
    ComputeCoordsRepartition(const Mesh& mesh, std::size_t dimension);

    constexpr auto mesh_index = MeshNS::unique_id{ 1ul };

} // namespace


BOOST_FIXTURE_TEST_CASE(no_adjacent_color, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);

    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);

    const auto& mesh = manager.GetMesh(mesh_index);

    std::vector<std::size_t> Ngeometric_elt_per_color;

    constexpr auto dimension = 2ul;

    auto color_per_geom_elt = ComputeColoring(mesh, dimension, Ngeometric_elt_per_color);

    auto coords_repartition = ComputeCoordsRepartition(mesh, dimension);

    BOOST_REQUIRE(!coords_repartition.empty());

    for (const auto& [coords_ptr, geom_elt_list] : coords_repartition)
    {
        BOOST_REQUIRE(!geom_elt_list.empty());

        std::set<std::size_t> color_found;

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            auto it = color_per_geom_elt.find(geom_elt_ptr);

            BOOST_REQUIRE(it != color_per_geom_elt.cend());
            color_found.insert(it->second);
        }

        BOOST_CHECK_EQUAL(color_found.size(), geom_elt_list.size());
    }
}


/*
 * Sanity check here: do almost the same as in the test above, but replace the last color by the first one.
 * In this case the test should fail: if the last color is not useful there is no reason it exists...
 */
BOOST_FIXTURE_TEST_CASE(sanity_check, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);

    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);

    const auto& mesh = manager.GetMesh(mesh_index);

    std::vector<std::size_t> Ngeometric_elt_per_color;

    constexpr auto dimension = 2ul;

    auto color_per_geom_elt = ComputeColoring(mesh, dimension, Ngeometric_elt_per_color);

    const auto last_color = Ngeometric_elt_per_color.size() - 1ul;

    for (auto& [geom_elt_ptr, color] : color_per_geom_elt)
    {
        if (color == last_color)
            color = 0ul;
    }

    auto coords_repartition = ComputeCoordsRepartition(mesh, dimension);

    BOOST_REQUIRE(!coords_repartition.empty());

    bool is_adjacent_color_found{ false };

    for (const auto& [coords_ptr, geom_elt_list] : coords_repartition)
    {
        BOOST_REQUIRE(!geom_elt_list.empty());

        std::set<std::size_t> color_found;

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            auto it = color_per_geom_elt.find(geom_elt_ptr);

            BOOST_REQUIRE(it != color_per_geom_elt.cend());
            color_found.insert(it->second);
        }

        if (color_found.size() != geom_elt_list.size())
            is_adjacent_color_found = true;
    }

    BOOST_CHECK(is_adjacent_color_found);
}


namespace
{

    std::unordered_map<Coords::shared_ptr, GeometricElt::vector_shared_ptr>
    ComputeCoordsRepartition(const Mesh& mesh, std::size_t dimension)
    {
        std::unordered_map<Coords::shared_ptr, GeometricElt::vector_shared_ptr> ret;
        ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

        decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));

            if (geom_elt_ptr->GetDimension() != dimension)
                continue;

            decltype(auto) coords_list = geom_elt_ptr->GetCoordsList();

            for (const auto& coords_ptr : coords_list)
            {
                assert(!(!coords_ptr));
                ret[coords_ptr].push_back(geom_elt_ptr);
            }
        }

        return ret;
    }

} // namespace
