/*!
// \file
//
*/

#include "Test/Geometry/Coloring/InputData.hpp"


namespace MoReFEM::TestNS::ColoringNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim1)>>(
            { "Finite element space for dimension 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(DimensionIndex::Dim2)>>(
            { "Finite element space for dimension 2" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim1)>>({ "Dimension 1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(DimensionIndex::Dim2)>>({ "Dimension 2" });

        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(Unused::value)>>({ "Sole" });

        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim1)>>({ "Dimension 1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DimensionIndex::Dim2)>>({ "Dimension 2" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ "Sole" });
    }


} // namespace MoReFEM::TestNS::ColoringNS
