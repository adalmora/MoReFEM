/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE Ncoords_in_domain
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Geometry/NcoordInDomain/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{


    using fixture_type = TestNS::FixtureNS::TestEnvironment;


}


BOOST_FIXTURE_TEST_CASE(check, fixture_type)
{
    using namespace TestNS::NcoordInDomainNS;
    decltype(auto) morefem_data =
        TestNS::InitMoReFEMDataFromCLI<TestNS::NcoordInDomainNS::morefem_data_type::input_data_type, TestNS::NcoordInDomainNS::ModelSettings>();

    TestNS::BareModel<TestNS::NcoordInDomainNS::morefem_data_type> model(morefem_data, create_domain_list_for_coords::yes);
    model.Initialize();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));

    decltype(auto) mesh = god_of_dof.GetMesh();

    decltype(auto) mpi = model.GetMpi();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::full_mesh);

        const auto Ncoord_in_domain = NcoordsInDomain<MpiScale::program_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 27ul;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain = NcoordsInDomain<MpiScale::processor_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<std::size_t>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_3);

        const auto Ncoord_in_domain = NcoordsInDomain<MpiScale::program_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 9ul;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain = NcoordsInDomain<MpiScale::processor_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<std::size_t>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_2_5);

        const auto Ncoord_in_domain = NcoordsInDomain<MpiScale::program_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 15ul;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain = NcoordsInDomain<MpiScale::processor_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<std::size_t>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }

    {
        constexpr auto domain_index = AsDomainId(DomainIndex::label_1_4_5_6);

        const auto Ncoord_in_domain = NcoordsInDomain<MpiScale::program_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // These expected results may be counted in the Mesh file cubeQ2.mesh rather easily.
        constexpr auto expected_result = 23ul;

        BOOST_CHECK_EQUAL(Ncoord_in_domain, expected_result);

        const auto Nprocessor_wise_coords_in_domain = NcoordsInDomain<MpiScale::processor_wise>(
            mpi, domain_manager.GetDomain(domain_index, __FILE__, __LINE__), mesh);

        // As a Coords may be counted processor-wise on several ranks, only inequality must be checked.
        // The program-wise computation correctly takes this into account.
        BOOST_CHECK_PREDICATE(
            std::less_equal<std::size_t>(),
            (expected_result)(mpi.AllReduce(Nprocessor_wise_coords_in_domain, Wrappers::MpiNS::Op::Sum)));
    }
}


PRAGMA_DIAGNOSTIC(pop)
