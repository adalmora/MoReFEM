/*!
// \file
//
*/

#include "Test/Geometry/NcoordInDomain/InputData.hpp"


namespace MoReFEM::TestNS::NcoordInDomainNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ " scalar)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ " scalar)" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::label_3)>>({ " label_3)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::label_2_5)>>({ " label_2_5)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::label_1_4_5_6)>>({ " label_1_4_5_6)" });
    }


} // namespace MoReFEM::TestNS::NcoordInDomainNS
