/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Geometry/LoadPrepartitionedMesh/Model.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string ret("Test LoadPrepartitionedMesh");
        return ret;
    }


    inline const Mesh& Model::GetOriginalMesh() const
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
        return mesh_manager.GetMesh(MeshNS::unique_id{ 1ul });
    }


    inline const Mesh& Model::GetMeshFromPrepartitionedData() const
    {
        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
        return mesh_manager.GetMesh(MeshNS::unique_id{ 10ul });
    }


} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_MODEL_HXX_
