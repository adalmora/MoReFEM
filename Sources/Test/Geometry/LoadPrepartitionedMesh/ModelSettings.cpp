/*!
// \file
//
*/

#include "Test/Geometry/LoadPrepartitionedMesh/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::Domain<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
