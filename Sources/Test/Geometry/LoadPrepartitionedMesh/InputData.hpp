/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_INPUT_DATA_HPP_

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused
    //! for current test.
    constexpr auto sole = 1;


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Unknown<sole>,
        InputDataNS::Domain<sole>,
        InputDataNS::NumberingSubset<sole>,
        InputDataNS::FEltSpace<sole>,

        InputDataNS::Result,
        InputDataNS::Parallelism
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<sole>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Unknown<sole>::IndexedSectionDescription,
        InputDataNS::Domain<sole>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<sole>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_LOAD_PREPARTITIONED_MESH_x_INPUT_DATA_HPP_
