/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Utilities/InputData/InputData.hpp"
#include "Utilities/InputData/ModelSettings.hpp"

#include "Core/InputData/Instances/Geometry/CoordsMatchingFile.hpp"
#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::TestNS::CoordsMatchingNS
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::CoordsMatchingFile<1>
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::CoordsMatchingFile<1>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


} // namespace MoReFEM::TestNS::CoordsMatchingNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_COORDS_MATCHING_x_INPUT_DATA_HPP_
