/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Mar 2016 23:31:12 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Geometry/DomainListInCoords/Model.hpp"


namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string ret("Test DomainListInCoords");
        return ret;
    }


} // namespace MoReFEM::TestNS::DomainListInCoordsNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HXX_
