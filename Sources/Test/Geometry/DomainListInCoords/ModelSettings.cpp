/*!
// \file
//
*/

#include "Test/Geometry/DomainListInCoords/InputData.hpp"


namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });

        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>({ " volume)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>>(
            { " exterior_surface)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>>(
            { " interior_surface)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::ring)>>({ " ring)" });
        SetDescription<InputDataNS::LightweightDomainList<sole>>({ " sole" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::DomainListInCoordsNS
