/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        volume = 1,
        exterior_surface = 2,
        interior_surface = 3,
        ring = 4,
        exterior_and_ring = 5, // through lightweight domain list
        interior_and_ring = 6  // through lightweight domain list
    };


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused for
    //! current test.
    constexpr auto sole = 0;


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::ring)>,

        InputDataNS::LightweightDomainList<sole>,

        InputDataNS::Unknown<sole>,
        InputDataNS::NumberingSubset<sole>,
        InputDataNS::FEltSpace<sole>,

        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<sole>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::exterior_surface)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::interior_surface)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::ring)>::IndexedSectionDescription,
        InputDataNS::LightweightDomainList<sole>::IndexedSectionDescription,
        InputDataNS::Unknown<sole>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<sole>::IndexedSectionDescription

    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::DomainListInCoordsNS


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_INPUT_DATA_HPP_
