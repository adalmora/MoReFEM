/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Apr 2017 16:21:23 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_
#define MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/Geometry/DomainListInCoords/InputData.hpp"


namespace MoReFEM::TestNS::DomainListInCoordsNS
{


    /*!
     * \brief Toy model used to perform tests about variable domain.
     *
     */
    //! \copydoc doxygen_hide_model_4_test
    // clang-format off
    class Model : public ::MoReFEM::Model
    <
        Model,
        morefem_data_type,
        DoConsiderProcessorWiseLocal2Global::no,
        ::MoReFEM::TimeManagerNS::Policy::None
    >
    // clang-format on
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        // clang-format off
        using parent =
        MoReFEM::Model
        <
            self,
            morefem_data_type,
            DoConsiderProcessorWiseLocal2Global::no,
            ::MoReFEM::TimeManagerNS::Policy::None
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());


      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        Model(const morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}

        /*!
         * \brief This function encompass tests about \a Domain defined as full-fledged domains in input data.
         */
        void CheckNormalDomain() const;

        /*!
         * \brief This function encompass tests about \a Domain defined with lightweight domain facility in
         * input data.
         */
        void CheckLightweightDomain() const;

      private:
        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        ///@}

      private:
        /*!
         * \class doxygen_hide_domain_list_in_coords_attribute
         *
         * \brief Number of \a Coords in a given domain computed by iterating over all \a Coords.
         *
         */

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_volume_ = 0;

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_ring_ = 0;

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_exterior_ = 0;

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_interior_ = 0;

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_exterior_and_ring_ = 0;

        //! \copydoc doxygen_hide_domain_list_in_coords_attribute
        std::size_t Ncoords_in_interior_and_ring_ = 0;
    };


} // namespace MoReFEM::TestNS::DomainListInCoordsNS


#include "Test/Geometry/DomainListInCoords/Model.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_GEOMETRY_x_DOMAIN_LIST_IN_COORDS_x_MODEL_HPP_
