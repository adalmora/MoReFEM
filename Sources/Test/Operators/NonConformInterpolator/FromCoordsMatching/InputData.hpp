/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/CoordsMatchingFile.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Interpolator/CoordsMatchingInterpolator.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { fluid = 1, solid = 2 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { fluid = 10, solid = 20 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t {
        fluid = 10,

        solid = 20
    };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t {
        unknown = 1,

        chaos = 3 // this unknown is just there to ensure the dof repartition is not the same for unknown and pressure.
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        unknown_on_fluid = 10,

        unknown_on_solid = 20,
        chaos = 22,
    };


    //! Indexes for the vertex matching interpolator.
    enum class CoordsMatchingInterpolator { unknown = 1 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>,

        InputDataNS::CoordsMatchingFile<1>,

        InputDataNS::CoordsMatchingInterpolator
        <
            EnumUnderlyingType(CoordsMatchingInterpolator::unknown)
        >,

        InputDataNS::Parallelism,
        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::IndexedSectionDescription,
        InputDataNS::CoordsMatchingFile<1>::IndexedSectionDescription,
        InputDataNS::CoordsMatchingInterpolator
        <
            EnumUnderlyingType(CoordsMatchingInterpolator::unknown)
        >::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::FromCoordsMatchingNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_INPUT_DATA_HPP_
