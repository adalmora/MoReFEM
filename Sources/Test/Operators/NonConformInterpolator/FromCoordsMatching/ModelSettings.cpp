/*!
// \file
//
*/

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>>(
            { "Finite element space for fluid mesh" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>>(
            { "Finite element space for solid mesh" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>>(
            { " unknown_on_fluid" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>>(
            { " unknown_on_solid" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>>({ " chaos" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>>({ " unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>>({ " chaos" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>>({ " fluid" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>>({ " solid" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid)>>({ " fluid" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid)>>({ " solid" });
        SetDescription<InputDataNS::CoordsMatchingFile<1>>({ "sole" });

        SetDescription<
            InputDataNS::CoordsMatchingInterpolator<EnumUnderlyingType(CoordsMatchingInterpolator::unknown)>>(
            { "sole" });
    }


} // namespace MoReFEM::TestNS::FromCoordsMatchingNS
