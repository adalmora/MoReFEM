/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <algorithm>
#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#define BOOST_TEST_MODULE symmetrize_local_matrix
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Operators/LocalVariationalOperator/Advanced/SymmetrizeMatrix.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

using namespace MoReFEM;

BOOST_AUTO_TEST_CASE(symmetrize_matrix)
{
    constexpr auto size = 5ul;

    LocalMatrix matrix({ size, size });
    matrix.fill(0.);

    for (auto row = 0ul; row < size; ++row)
    {
        for (auto col = row; col < size; ++col)
            matrix(row, col) = static_cast<double>(1 + row + 3 * col);
    }

    Advanced::LocalVariationalOperatorNS::SymmetrizeMatrix(matrix);

    for (auto row = 0ul; row < size; ++row)
        for (auto col = 0ul; col < size; ++col)
            BOOST_CHECK_EQUAL(matrix(row, col), matrix(col, row));
}

PRAGMA_DIAGNOSTIC(pop)
