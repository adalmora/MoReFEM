/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE trait_local_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


#include "OperatorInstances/VariationalOperator/BilinearForm/GradOnGradientBasedElasticityTensor.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(operator_with_same_for_each_ref_geom_elt)
{
    using mass = GlobalVariationalOperatorNS::Mass;

    static_assert(
        std::is_same<typename mass::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Triangle3>,
                     Advanced::LocalVariationalOperatorNS::Mass>());

    static_assert(
        std::is_same<typename mass::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Segment2>,
                     Advanced::LocalVariationalOperatorNS::Mass>());
}


BOOST_AUTO_TEST_CASE(operator_with_different_for_each_ref_geom_elt)
{
    // GradOnGradientBasedElasticityTensor is not defined for a Point1 geometry.
    // It is currently the only global operator for which the local operator is not the same for each RefGeomElt:
    // this differentiation was introduced to implement the shells in MoReFEM but it was never completed (and the
    // current way to introduce them - the NonlinearShell operator - doesn't use this functionality).
    using elast = GlobalVariationalOperatorNS::GradOnGradientBasedElasticityTensor;

    static_assert(
        std::is_same<typename elast::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Point1>,
                     std::false_type>());

    static_assert(
        std::is_same<typename elast::local_operator_type_for_geom_elt_enum<Advanced::GeometricEltEnum::Segment2>,
                     Advanced::LocalVariationalOperatorNS::GradOnGradientBasedElasticityTensor>());
}


PRAGMA_DIAGNOSTIC(pop)
