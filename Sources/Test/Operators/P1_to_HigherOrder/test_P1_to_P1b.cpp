/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE interpolator_P1_to_P1b
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "OperatorInstances/ConformInterpolator/P1_to_P1b.hpp"
#include "OperatorInstances/ConformInterpolator/P1b_to_P1.hpp"

#include "Test/Operators/P1_to_HigherOrder/AnonymousNamespace.hpp"
#include "Test/Operators/P1_to_HigherOrder/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(P1_to_P1b, fixture_type)
{
    using namespace TestNS::P1_to_P_HigherOrder_NS;

    decltype(auto) model = GetModel();

    decltype(auto) god_of_dof = model.GetGodOfDof(MeshNS::unique_id{ 1ul });

    decltype(auto) source_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::fluid_velocity));
    decltype(auto) target_felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::solid_velocity));

    decltype(auto) source_numbering_subset = *source_felt_space.GetNumberingSubsetList()[0];
    decltype(auto) target_numbering_subset = *target_felt_space.GetNumberingSubsetList()[0];

    decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

    decltype(auto) velocity_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::velocity));
    decltype(auto) p_higher_order_velocity_ptr =
        unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::higher_order_velocity));

    auto interpolator_p1_p_higher_order =
        ConformInterpolatorNS::P1_to_P1b(source_felt_space,
                                         source_numbering_subset,
                                         target_felt_space,
                                         target_numbering_subset,
                                         { { velocity_ptr, p_higher_order_velocity_ptr } });
    interpolator_p1_p_higher_order.Init();

    auto interpolator_p_higher_order_p1 =
        ConformInterpolatorNS::P1b_to_P1(target_felt_space,
                                         target_numbering_subset,
                                         source_felt_space,
                                         source_numbering_subset,
                                         { { p_higher_order_velocity_ptr, velocity_ptr } });

    interpolator_p_higher_order_p1.Init();

    //******************************************************************************************
    // Consistency check: Transformation (Phigher -> P1) o (P1 -> Phigher) should be identity!
    //******************************************************************************************

    Wrappers::Petsc::Matrix matrix;

    Wrappers::Petsc::MatMatMult(interpolator_p_higher_order_p1.GetInterpolationMatrix(),
                                interpolator_p1_p_higher_order.GetInterpolationMatrix(),
                                matrix,
                                __FILE__,
                                __LINE__);

    // Check it is the identity matrix, by using a test vector...
    GlobalVector test(source_numbering_subset);
    AllocateGlobalVector(god_of_dof, test);
    GlobalVector result(test);

    test.SetUniformValue(1., __FILE__, __LINE__);

    Wrappers::Petsc::MatMult(matrix, test, result, __FILE__, __LINE__);

    std::string description_if_failed;
    BOOST_CHECK(Wrappers::Petsc::AreEqual(
        test, result, NumericNS::DefaultEpsilon<double>(), description_if_failed, __FILE__, __LINE__));
}


PRAGMA_DIAGNOSTIC(pop)
