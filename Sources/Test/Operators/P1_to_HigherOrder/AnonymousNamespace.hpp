/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Operators/P1_to_HigherOrder/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{

    // clang-format off
    using model_type =
    MoReFEM::TestNS::BareModel
    <
        MoReFEM::TestNS::P1_to_P_HigherOrder_NS::morefem_data_type,
        MoReFEM::TimeManagerNS::Policy::None,
        MoReFEM::DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


} // namespace


PRAGMA_DIAGNOSTIC(pop)
