The results of the tests provided here are not deemed to be physically sound; they are here mostly to check we don't break anything whenever we make a change in the library (some functionalities / laws provided are not used otherwise in the core library models).

At the moment:

- **Generalized** coords aren't handled yet - I am waiting for some answers in #1821 before implementing them in the tests (to be more precise I will of course write the test before the refactoring, but the answers will help me add some asserts in the way).

- Laws that don't use fibers are checked against the `at_node` policy only. The other one gives away the same results, but to my mind the policy shouldn't be modifiable for laws that don't use it. So I'm not wasting time writing the tests without that point cleared either (also in #1821).

- Other laws shall wait likewise these values.


Expected values were rounded by default.
