/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/

#include "Utilities/MatrixOrVector.hpp"


namespace // anonymous
{


    //! An arbitrary Cauchy-Green tensor that appeared in the hyperelastic model. Might not be realistic - that's not
    //! the point of current test anyway.
    MoReFEM::LocalVector SampleCauchyGreenTensor();

    constexpr auto epsilon = 1.e-6;


} // namespace


namespace // anonymous
{


    MoReFEM::LocalVector SampleCauchyGreenTensor()
    {
        return MoReFEM::LocalVector{ 1.001654, 0.999784, 0.999909, -0.000358, 0.000206, -0.000215 };
    }


} // namespace
