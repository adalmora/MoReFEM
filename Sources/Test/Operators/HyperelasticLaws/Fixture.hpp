/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_HYPERELASTIC_LAWS_x_FIXTURE_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_HYPERELASTIC_LAWS_x_FIXTURE_HPP_

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/MoReFEMData/MoReFEMDataForTest.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


namespace MoReFEM::TestNS::HyperelasticLawNS
{


    //! Alias for mode type.
    // clang-format off
    using model_type = TestNS::BareModel
    <
        MoReFEMDataForTest<TestNS::HyperelasticLawNS::ModelSettings>,
        ::MoReFEM::TimeManagerNS::Policy::Static,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on


    //! Helper object to pass string information at compile time.
    struct OutputDirWrapper
    {
        //! The method that does the actual work.
        static constexpr std::string_view Path();
    };

    //! Alias for the fixture parent.
    // clang-format off
    using fixture_parent_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        TestNS::FixtureNS::call_run_method_at_first_call::yes,
        create_domain_list_for_coords::yes
    >;
    // clang-format on


    //! Fixture to use for hyperelastic laws tests.
    class Fixture : public fixture_parent_type
    {
      public:
        
        //! Alias for Cauchy-Green tensor type,
        // clang-format off
        using cauchy_green_tensor_type =
            ParameterAtQuadraturePoint<ParameterNS::Type::vector,
                                                ParameterNS::TimeDependencyNS::None>;
        // clang-format on


      public:
        
        //! Constructor.
        Fixture();

        //! Accessor to Solid.
        const Solid& GetSolid() const noexcept;

        //! Get the only tetrahedron of the mesh.
        const GeometricElt& GetTetrahedron() const noexcept;

        //! Get the quadrature rules to use.
        const QuadratureRulePerTopology& GetQuadratureRulePerTopology() const noexcept;

        //! Get the first \a QuadraturePoint of the \a geom_elt.
        //!  \param[in] geom_elt \a GeometricElt for which a \a QuadraturePoint is sought.
        //!  \return First quadrature point found.
        const QuadraturePoint& GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept;


      private:
        
        //! Storage for solid object.
        Solid::const_unique_ptr solid_{ nullptr };

        //! Storage of all quadrature rules to use.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_{ nullptr };

        //! Only tetrahedron element in the mesg.
        GeometricElt::shared_ptr tetra_{ nullptr };

        //! Operator that updates Cauchy-Green tensor.
        GlobalParameterOperatorNS::UpdateCauchyGreenTensor::const_unique_ptr cauchy_green_tensor_operator_{
            nullptr
        };

        //! Cauchy-Green tensor.
        cauchy_green_tensor_type::unique_ptr cauchy_green_tensor_{ nullptr };

        //! Time manager.
        TimeManager::unique_ptr time_manager_{ nullptr };
    };


} // namespace MoReFEM::TestNS::HyperelasticLawNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_HYPERELASTIC_LAWS_x_FIXTURE_HPP_
