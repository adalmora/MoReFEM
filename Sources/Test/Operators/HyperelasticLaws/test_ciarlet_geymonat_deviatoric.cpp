/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE ciarlet_geymonat_deviatoric

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"
#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::CiarletGeymonatDeviatoric<>;
    law_type law(GetSolid());

    using invariant_holder_type =
        InvariantHolder<typename law_type::traits_parent, law_type::fiber_policy, law_type::coords_policy>;
    invariant_holder_type invariant_holder(3ul, InvariantNS::Content::invariants_and_first_and_second_deriv);

    const auto cauchy_green_tensor = SampleCauchyGreenTensor();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    invariant_holder.UpdateCartesian(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.528049;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.FirstDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 499.775818;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 402984.39051;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -806107.093809;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 1341539.272650;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndSecondInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -166.367959;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
        constexpr auto expected_value = -268295.056029;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)
