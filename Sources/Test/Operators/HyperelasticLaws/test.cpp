/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Mar 2018 18:46:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE hyperelastic_laws

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"
#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4Deviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4J6.hpp"
#include "OperatorInstances/HyperelasticLaws/FiberDensityJ1J4J6.hpp"
#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"
#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hpp"
#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(CiarletGeymonat)
{
    using type = MoReFEM::HyperelasticLawNS::CiarletGeymonat<>;

    static_assert(type::Ninvariants() == 3ul);
    static_assert(type::IsI4Activated() == false);
    static_assert(type::IsI6Activated() == false);
}


BOOST_AUTO_TEST_CASE(CiarletGeymonatDeviatoric)
{
    using type = MoReFEM::HyperelasticLawNS::CiarletGeymonatDeviatoric<>;

    static_assert(type::Ninvariants() == 3ul);
    static_assert(type::IsI4Activated() == false);
    static_assert(type::IsI6Activated() == false);
}

BOOST_AUTO_TEST_CASE(ExponentialJ1J4)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4<>;

    static_assert(type::Ninvariants() == 4ul);
    static_assert(type::IsI4Activated() == true);
    static_assert(type::IsI6Activated() == false);
}


BOOST_AUTO_TEST_CASE(ExponentialJ1J4Deviatoric)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4Deviatoric<>;

    static_assert(type::Ninvariants() == 4ul);
    static_assert(type::IsI4Activated() == true);
    static_assert(type::IsI6Activated() == false);
}

BOOST_AUTO_TEST_CASE(ExponentialJ1J4J6)
{
    using type = MoReFEM::HyperelasticLawNS::ExponentialJ1J4J6<>;

    static_assert(type::Ninvariants() == 5ul);
    static_assert(type::IsI4Activated() == true);
    static_assert(type::IsI6Activated() == true);
}

BOOST_AUTO_TEST_CASE(FiberDensityJ1J4J6)
{
    using type = MoReFEM::HyperelasticLawNS::FiberDensityJ1J4J6<>;

    static_assert(type::Ninvariants() == 5ul);
    static_assert(type::IsI4Activated() == true);
    static_assert(type::IsI6Activated() == true);
}


BOOST_AUTO_TEST_CASE(LogI3Penalization)
{
    using type = MoReFEM::HyperelasticLawNS::LogI3Penalization<>;

    static_assert(type::Ninvariants() == 3ul);
    static_assert(type::IsI4Activated() == false);
    static_assert(type::IsI6Activated() == false);
}

BOOST_AUTO_TEST_CASE(MooneyRivlin)
{
    using type = MoReFEM::HyperelasticLawNS::MooneyRivlin<>;

    static_assert(type::Ninvariants() == 3ul);
    static_assert(type::IsI4Activated() == false);
    static_assert(type::IsI6Activated() == false);
}

BOOST_AUTO_TEST_CASE(StVenantKirchhoff)
{
    using type = MoReFEM::HyperelasticLawNS::StVenantKirchhoff<>;

    static_assert(type::Ninvariants() == 3ul);
    static_assert(type::IsI4Activated() == false);
    static_assert(type::IsI6Activated() == false);
}


PRAGMA_DIAGNOSTIC(pop)
