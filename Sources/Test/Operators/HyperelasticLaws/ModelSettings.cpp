/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/

#include <filesystem>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"


namespace MoReFEM::TestNS::HyperelasticLawNS
{


    void ModelSettings::Init()
    {
        // ****** Mesh ******
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ "Sole mesh" });

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Path>("${MOREFEM_ROOT}/Data/Mesh/one_tetra.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::Dimension>(3ul);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>::SpaceUnit>(1.);

        // ****** Domain ******
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>(
            { "Dimension 3 elements of the mesh" });

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshIndexList>(
            { EnumUnderlyingType(MeshIndex::sole) });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::DimensionList>({ 3ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::GeomEltTypeList>({});

        // ****** Solid ******
        Add<InputDataNS::Solid::VolumicMass::Nature>("constant");
        Add<InputDataNS::Solid::Kappa1::Nature>("constant");
        Add<InputDataNS::Solid::Kappa2::Nature>("constant");
        Add<InputDataNS::Solid::LameLambda::Nature>("constant");
        Add<InputDataNS::Solid::LameMu::Nature>("constant");
        Add<InputDataNS::Solid::HyperelasticBulk::Nature>("constant");
        Add<InputDataNS::Solid::Mu1::Nature>("constant");
        Add<InputDataNS::Solid::Mu2::Nature>("constant");
        Add<InputDataNS::Solid::C0::Nature>("constant");
        Add<InputDataNS::Solid::C1::Nature>("constant");
        Add<InputDataNS::Solid::C2::Nature>("constant");
        Add<InputDataNS::Solid::C3::Nature>("constant");
        Add<InputDataNS::Solid::C4::Nature>("constant");
        Add<InputDataNS::Solid::C5::Nature>("constant");

        Add<InputDataNS::Solid::VolumicMass::Value>(1.2);
        Add<InputDataNS::Solid::Kappa1::Value>(500.);
        Add<InputDataNS::Solid::Kappa2::Value>(403346.);
        Add<InputDataNS::Solid::LameLambda::Value>(1.21154e+06);
        Add<InputDataNS::Solid::LameMu::Value>(807692.);
        Add<InputDataNS::Solid::HyperelasticBulk::Value>(1750000.);

        Add<InputDataNS::Solid::Mu1::Value>(7000.);
        Add<InputDataNS::Solid::Mu2::Value>(3500.);
        Add<InputDataNS::Solid::C0::Value>(8500.);
        Add<InputDataNS::Solid::C1::Value>(0.11);
        Add<InputDataNS::Solid::C2::Value>(5700.);
        Add<InputDataNS::Solid::C3::Value>(0.13);
        Add<InputDataNS::Solid::C4::Value>(6200.);
        Add<InputDataNS::Solid::C5::Value>(0.16);

        
        // ****** Unknown ******
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>("Solid displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Name>("displacement");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::Nature>("vectorial");

        // ****** Numbering subset ******
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>(
            "Numbering subset");
        Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>::DoMoveMesh>(false);

        // ****** Finite element space ******
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>("Finite element space");
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::GodOfDofIndex>(
            EnumUnderlyingType(MeshIndex::sole));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::UnknownList>({ "displacement" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::DomainIndex>(
            EnumUnderlyingType(DomainIndex::volume));
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::NumberingSubsetList>(
            { EnumUnderlyingType(NumberingSubsetIndex::sole) });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::ShapeFunctionList>(
            { "P1" }); // #1824 'P2' here triggers an assert.


        // ****** Fibers ******
        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI4),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I4 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::volume));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::sole));
            Add<fiber_type::UnknownName>("displacement");
        }


        {
            // clang-format off
            using fiber_type = InputDataNS::Fiber
            <
                EnumUnderlyingType(FiberIndex::fiberI6),
                ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                ParameterNS::Type::vector
            >;
            // clang-format on

            SetDescription<fiber_type>("Fiber I6 at node");
            Add<fiber_type::EnsightFile>("${MOREFEM_ROOT}/Data/Fiber/fibers_one_tetra.vct");
            Add<fiber_type::DomainIndex>(EnumUnderlyingType(DomainIndex::volume));
            Add<fiber_type::FEltSpaceIndex>(EnumUnderlyingType(FEltSpaceIndex::sole));
            Add<fiber_type::UnknownName>("displacement");
        }
    }


} // namespace MoReFEM::TestNS::HyperelasticLawNS
