/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 16:00:22 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_GLIMPSE_CAUCHY_GREEN_CONTENT_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_GLIMPSE_CAUCHY_GREEN_CONTENT_HPP_

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    /*!
     * \brief Return the value of the 3rd (arbitrarily) element of the only \a GeometricElt of the highest dimension.
     *
     * \param[in] model The \a Model used for the test.
     *
     * \return The local value of the \a Parameter at the specific (\a GeometricElt, \a QuadraturePoint).
     */
    auto GlimpseCauchyGreenContent(const Model& model) -> const Model::glimpse_cauchy_green_type&;


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_GLIMPSE_CAUCHY_GREEN_CONTENT_HPP_
