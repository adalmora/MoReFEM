/*!
// \file
//
*/

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    void ModelSettings::Init()
    {
        SetDescription<::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(
            NumberingSubsetIndex::generic_numbering_subset)>>({ " generic_numbering_subset" });
        SetDescription<::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_unknown)>>(
            { " generic_unknown" });
        SetDescription<::MoReFEM::InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<::MoReFEM::InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
