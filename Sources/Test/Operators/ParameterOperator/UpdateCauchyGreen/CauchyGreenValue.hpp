#ifndef MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS
{


    // clang-format off

    /*!
     * \brief Input data which gives the initial content at a \a QuadraturePoint of the Cauchy-Green tensor.
     */
    struct Initial
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
            <
                Initial,
                ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    // clang-format off


    /*!
     * \brief Input data which gives the expected result at a \a QuadraturePoint of the Cauchy-Green tensor after the arbitrary upgrade operator implemented
     * in the test is applied.
     */

    struct AfterOperator
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
            <
                AfterOperator,
                ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection,
                std::vector<double>
            >
    // clang-format on
    {

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS::InputDataNS::CauchyGreenValueNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_PARAMETER_OPERATOR_x_UPDATE_CAUCHY_GREEN_x_CAUCHY_GREEN_VALUE_HPP_
