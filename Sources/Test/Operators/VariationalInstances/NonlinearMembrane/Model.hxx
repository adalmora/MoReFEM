/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 6 Jan 2015 11:16:32 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"


namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("NonlinearMembrane");
        return name;
    }


} // namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_MEMBRANE_x_MODEL_HXX_
