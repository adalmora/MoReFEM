/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/NonlinearMembrane/InputData.hpp"


namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacementP1)>>(
            { " displacementP1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacementP2)>>(
            { " displacementP2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacementP1)>>({ " displacementP1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacementP2)>>({ " displacementP2" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::surface)>>({ " surface" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::force)>>({ " force" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::dirichlet)>>({ " dirichlet" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::surface)>>({ " surface" });
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic)>>(
            { "Surfacic force" });
    }


} // namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS
