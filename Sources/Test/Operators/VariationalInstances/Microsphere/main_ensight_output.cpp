//! \file
//
//
//  main_ensight_output.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 23/01/2019.
// Copyright © 2019 Jerôme Diaz. All rights reserved.
//

#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"
#include "Test/Operators/VariationalInstances/Microsphere/Model.hpp"

using namespace MoReFEM;


int main(int argc, char** argv)
{
    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId(
        MoReFEM::TestNS::Microsphere::NumberingSubsetIndex::displacement) };

    std::vector<std::string> unknown_list{ "displacement" };

    return ModelNS::MainEnsightOutput<TestNS::Microsphere::Model>(
        argc, argv, AsMeshId(TestNS::Microsphere::MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}
