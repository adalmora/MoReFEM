//! \file
//
//
//  main.cpp
//  MoReFEM
//
//  Created by Jerôme Diaz on 22/01/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include "Model/Main/Main.hpp"

#include "Test/Operators/VariationalInstances/Microsphere/Model.hpp"


using namespace MoReFEM;


int main(int argc, char** argv)
{
    return ModelNS::Main<TestNS::Microsphere::Model>(argc, argv);
}
