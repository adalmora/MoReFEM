/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Mar 2018 15:13:57 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    void Model::UnknownP2TestP1() const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& displacement_p1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p1));
        const auto& displacement_p2_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p2));

        NonlinearShellType shell_operator(felt_space,
                                          displacement_p2_ptr,
                                          displacement_p1_ptr,
                                          hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                          nullptr);


        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p1));
        decltype(auto) numbering_subset_p2 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p2));

        GlobalMatrix matrix_p1_p2(numbering_subset_p1, numbering_subset_p2);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_p2);

        GlobalVector vector_p1(numbering_subset_p1);
        AllocateGlobalVector(god_of_dof, vector_p1);

        GlobalMatrix matrix_p2_p2(numbering_subset_p2, numbering_subset_p2);
        AllocateGlobalMatrix(god_of_dof, matrix_p2_p2);

        GlobalVector vector_p2(numbering_subset_p2);
        AllocateGlobalVector(god_of_dof, vector_p2);

        GlobalVector previous_iteration_p2(numbering_subset_p2);
        AllocateGlobalVector(god_of_dof, previous_iteration_p2);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1_p2.ZeroEntries(__FILE__, __LINE__);
            vector_p1.ZeroEntries(__FILE__, __LINE__);
            previous_iteration_p2.ZeroEntries(__FILE__, __LINE__);

            previous_iteration_p2.SetValue(3, 0.1, INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p2.SetValue(6, 0.1, INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p2.SetValue(7, 0.1, INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p2.SetValue(10, 0.1, INSERT_VALUES, __FILE__, __LINE__);
        }

        previous_iteration_p2.Assembly(__FILE__, __LINE__);

        GlobalMatrixWithCoefficient matrix(matrix_p1_p2, 1.);
        GlobalVectorWithCoefficient vec(vector_p1, 1.);

        shell_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), previous_iteration_p2);

        /* BOOST_CHECK_NO_THROW */ (
            CheckVector(god_of_dof, vector_p1, GetExpectedVectorP2P1(dimension), __FILE__, __LINE__, 1.e-5));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof, matrix_p1_p2, GetExpectedMatrixP2P1(dimension), __FILE__, __LINE__, 1.e-5));
    }


} // namespace MoReFEM::TestNS::NonlinearShell
