/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Mar 2018 15:13:57 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    namespace // anonymous
    {


        using ::MoReFEM::Internal::assemble_into_matrix;

        using ::MoReFEM::Internal::assemble_into_vector;


    } // namespace


    void Model::SameUnknown(assemble_into_matrix do_assemble_into_matrix,
                            assemble_into_vector do_assemble_into_vector) const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& displacement_p1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p1));

        NonlinearShellType shell_operator(felt_space,
                                          displacement_p1_ptr,
                                          displacement_p1_ptr,
                                          hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                          nullptr);

        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p1));

        GlobalMatrix matrix_p1_p1(numbering_subset_p1, numbering_subset_p1);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_p1);

        GlobalVector vector_p1(numbering_subset_p1);
        AllocateGlobalVector(god_of_dof, vector_p1);

        GlobalVector previous_iteration_p1(vector_p1);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1_p1.ZeroEntries(__FILE__, __LINE__);
            vector_p1.ZeroEntries(__FILE__, __LINE__);
            previous_iteration_p1.ZeroEntries(__FILE__, __LINE__);

            previous_iteration_p1.SetValue(3, 2., INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p1.SetValue(2, 2., INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p1.SetValue(0, -1., INSERT_VALUES, __FILE__, __LINE__);
            previous_iteration_p1.SetValue(1, -2., INSERT_VALUES, __FILE__, __LINE__);

            previous_iteration_p1.Assembly(__FILE__, __LINE__);

            GlobalMatrixWithCoefficient matrix(matrix_p1_p1, 1.);
            GlobalVectorWithCoefficient vec(vector_p1, 1.);

            if (do_assemble_into_matrix == assemble_into_matrix::yes
                && do_assemble_into_vector == assemble_into_vector::yes)
            {
                shell_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), previous_iteration_p1);

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1_p1, GetExpectedMatrixP1P1(dimension), __FILE__, __LINE__, 1.e-5));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1, GetExpectedVectorP1P1(dimension), __FILE__, __LINE__, 1.e-5));
            } else if (do_assemble_into_matrix == assemble_into_matrix::yes
                       && do_assemble_into_vector == assemble_into_vector::no)
            {
                shell_operator.Assemble(std::make_tuple(std::ref(matrix)), previous_iteration_p1);

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1_p1, GetExpectedMatrixP1P1(dimension), __FILE__, __LINE__, 1.e-5));
            } else if (do_assemble_into_matrix == assemble_into_matrix::no
                       && do_assemble_into_vector == assemble_into_vector::yes)
            {
                shell_operator.Assemble(std::make_tuple(std::ref(vec)), previous_iteration_p1);

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1, GetExpectedVectorP1P1(dimension), __FILE__, __LINE__, 1.e-5));
            }
        }
    }


} // namespace MoReFEM::TestNS::NonlinearShell
