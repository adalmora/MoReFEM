/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test NonlinearShell operator");
        return name;
    }


} // namespace MoReFEM::TestNS::NonlinearShell


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_MODEL_HXX_
