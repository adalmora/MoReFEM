/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/Model.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test MITC for operator");
        return name;
    }


    template<class GlobalOperatorT>
    const auto& Model::ComputeForFirstLocalOperator(GlobalOperatorT& global_operator,
                                                    const GlobalVector& previous_iteration) const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto unknown_id = AsUnknownId(UnknownIndex::vectorial);

        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

        decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                    .GetDomain(AsDomainId(DomainIndex::domain), __FILE__, __LINE__);

        auto& local_operator =
            global_operator.template ExtractReadiedLocalOperator<Advanced::GeometricEltEnum::Hexahedron27>(
                domain, previous_iteration);

        const auto& elementary_data = local_operator.GetElementaryData();

        local_operator.InitTyingPointData(elementary_data.GetInformationAtQuadraturePointList(),
                                          elementary_data.GetRefGeomElt(),
                                          elementary_data.GetRefFElt(felt_space.GetExtendedUnknown(*unknown_ptr)),
                                          elementary_data.GetTestRefFElt(felt_space.GetExtendedUnknown(*unknown_ptr)));

        local_operator.ComputeEltArray();

        return elementary_data;
    }


} // namespace MoReFEM::TestNS::MITCNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_MODEL_HXX_
