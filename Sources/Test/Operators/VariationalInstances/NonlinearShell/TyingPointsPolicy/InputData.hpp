/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
 // Copyright (c) Inria. All rights reserved.
 //
 */


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { domain = 1ul };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { sole = 1ul };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { vectorial = 1ul, scalar = 2ul };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { vectorial = 1ul, scalar = 2ul };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1ul
    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>,
        
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
        
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,
        
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,
        
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,
    
        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::CheckInvertedElements,
        
        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::MITCNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_NONLINEAR_SHELL_x_TYING_POINTS_POLICY_x_INPUT_DATA_HPP_
