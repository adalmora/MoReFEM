/*!
 // \file
 //
 //
 // Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/Model.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.Nprocessor<int>() > 1)
        {
            throw Exception("This test intends to run a local operator and there is no point doing so in parallel "
                            "(as this step happens locally on each processor)",
                            __FILE__,
                            __LINE__);
        }
    }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();
        

        decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::domain), __FILE__, __LINE__);

        quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(3, 2);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        constexpr auto relative_tolerance = -1.; // Negative to cancel CheckConsistancy issues.
        solid_ = std::make_unique<Solid>(
                                         morefem_data, domain_volume, felt_space.GetQuadratureRulePerTopology(), relative_tolerance);

        hyperelastic_law_parent::Create(*solid_);

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::MITCNS
