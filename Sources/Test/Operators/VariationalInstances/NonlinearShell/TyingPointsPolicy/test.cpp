/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#define BOOST_TEST_MODULE nonlinear_shell_tying_point_policies
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/ShellTyingPointsPolicy/MITC4.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/ShellTyingPointsPolicy/MITC9.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/ShellTyingPointsPolicy/None.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/InputData.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
       TestNS::MITCNS::Model
    >;
    // clang-format on

    GlobalVector::unique_ptr PreviousIterationVector(const TestNS::MITCNS::Model& model);

    // clang-format off
    template<class TyingPointsPolicyT>
    using NonlinearShellType =
    GlobalVariationalOperatorNS::NonlinearShell
    <
        TestNS::MITCNS::Model::hyperelastic_policy_for_operator,
        TyingPointsPolicyT
    >;
    // clang-format on

    template<class TyingPointsPolicyT>
    std::unique_ptr<NonlinearShellType<TyingPointsPolicyT>> NonlinearShellOperator(const TestNS::MITCNS::Model& model);

    template<class TyingPointsPolicyT>
    LocalVector ExpectedVector();

    /*!
     * \brief First and last lines of the expected matrix.
     *
     *  \return First element of the pair is the first line, second is the last line.
     */
    template<class TyingPointsPolicyT>
    std::pair<std::vector<double>, std::vector<double>> ExpectedSubsetOfMatrix();

    /*!
     * brief Check the model yields the expected results.
     *
     * Those results are the one obtained with the first version of the model; they have not been validated
     * independently.
     */
    template<class TyingPointsPolicyT>
    void CheckResults(TestNS::MITCNS::Model& model, const char* invoking_file, int invoking_line);

    /*!
     * \brief Partial check that the matrix match the expected one.
     *
     * As the matrix is rather big, only the first and last lines are checked.
     *
     * \param[in] epsilon The epsilon used for float comparison.
     *
     *  \return First element of the pair is the first line, second is the last line.
     */
    void CheckSubsetOfMatrix(const LocalMatrix& obtained,
                             std::pair<std::vector<double>, std::vector<double>>&& expected,
                             double epsilon);


} // namespace


BOOST_FIXTURE_TEST_CASE(none, fixture_type)
{
    using tying_points_policy = GlobalVariationalOperatorNS::TyingPointsNS::None;

    CheckResults<tying_points_policy>(GetNonCstModel(), __FILE__, __LINE__);
}


BOOST_FIXTURE_TEST_CASE(MITC4, fixture_type)
{
    using tying_points_policy = GlobalVariationalOperatorNS::TyingPointsNS::MITC4;

    CheckResults<tying_points_policy>(GetNonCstModel(), __FILE__, __LINE__);
}


BOOST_FIXTURE_TEST_CASE(MITC9, fixture_type)
{
    using tying_points_policy = GlobalVariationalOperatorNS::TyingPointsNS::MITC9;

    CheckResults<tying_points_policy>(GetNonCstModel(), __FILE__, __LINE__);
}


namespace // anonymous
{


    GlobalVector::unique_ptr PreviousIterationVector(const TestNS::MITCNS::Model& model)
    {
        decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(TestNS::MITCNS::MeshIndex::mesh));

        auto ret = std::make_unique<GlobalVector>(
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(TestNS::MITCNS::NumberingSubsetIndex::vectorial)));

        std::filesystem::path path{ "${MOREFEM_ROOT}/Sources/Test/Operators/VariationalInstances/NonlinearShell/"
                                    "TyingPointsPolicy/previous_iteration_vector.hhdata" };
        FilesystemNS::File input_file{ std::move(path) };

        ret->InitSequentialFromAsciiFile(god_of_dof.GetMpi(), input_file, __FILE__, __LINE__);

        return ret;
    }


    template<class TyingPointsPolicyT>
    auto NonlinearShellOperator(const TestNS::MITCNS::Model& model)
        -> std::unique_ptr<NonlinearShellType<TyingPointsPolicyT>>
    {
        const auto& god_of_dof = model.GetGodOfDof(AsMeshId(TestNS::MITCNS::MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(TestNS::MITCNS::FEltSpaceIndex::sole));

        const auto unknown_id = AsUnknownId(TestNS::MITCNS::UnknownIndex::vectorial);

        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

        auto nl_op = std::make_unique<NonlinearShellType<TyingPointsPolicyT>>(
            felt_space, unknown_ptr, unknown_ptr, model.GetHyperelasticLawPtr());

        return nl_op;
    }


    template<class TyingPointsPolicyT>
    void CheckResults(TestNS::MITCNS::Model& model, const char* invoking_file, int invoking_line)
    {
        const auto previous_iteration_vector_ptr = PreviousIterationVector(model);

        auto nonlinear_shell_global_operator_ptr = NonlinearShellOperator<TyingPointsPolicyT>(model);

        decltype(auto) elementary_data =
            model.ComputeForFirstLocalOperator(*nonlinear_shell_global_operator_ptr, *previous_iteration_vector_ptr);

        {
            const auto expected = ExpectedVector<TyingPointsPolicyT>();

            TestNS::CheckLocalVector(elementary_data.GetVectorResult(), expected, invoking_file, invoking_line, 1.e-8);
        }

        {
            CheckSubsetOfMatrix(elementary_data.GetMatrixResult(), ExpectedSubsetOfMatrix<TyingPointsPolicyT>(), 1.e-8);
        }
    }


    void CheckSubsetOfMatrix(const LocalMatrix& obtained,
                             std::pair<std::vector<double>, std::vector<double>>&& expected,
                             double epsilon)
    {
        const auto& expected_first_line = expected.first;
        const auto& expected_last_line = expected.second;

        const auto Ncol = expected_first_line.size();
        assert(Ncol == expected_last_line.size());

        {
            auto obtained_first_line = xt::view(obtained, 0, xt::all());
            assert(expected_first_line.size() == obtained_first_line.size());

            for (auto col = 0ul; col < Ncol; ++col)
                BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                      (obtained_first_line[col])(expected_first_line[col])(epsilon));
        }

        {
            auto obtained_last_line = xt::view(obtained, -1, xt::all());
            assert(Ncol == obtained_last_line.size());

            for (auto col = 0ul; col < Ncol; ++col)
                BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                      (obtained_last_line[col])(expected_last_line[col])(epsilon));
        }
    }


    template<>
    LocalVector ExpectedVector<GlobalVariationalOperatorNS::TyingPointsNS::None>()
    {
        return { { -1.1189591811e-05, -3.1076484142e-06, 1.0719705855e-05,  -3.5137162690e-05, -9.5369870412e-06,
                   4.1129477916e-05,  -1.1189591811e-05, -3.1076484141e-06, 1.0719705855e-05,  -3.5137162690e-05,
                   -9.5369870412e-06, 4.1129477916e-05,  -1.0259287526e-04, -2.6665242936e-05, 1.5774694293e-04,
                   -3.5137162690e-05, -9.5369870412e-06, 4.1129477916e-05,  -1.1189591811e-05, -3.1076484141e-06,
                   1.0719705855e-05,  -3.5137162690e-05, -9.5369870411e-06, 4.1129477916e-05,  -1.1189591811e-05,
                   -3.1076484141e-06, 1.0719705855e-05,  9.3870404414e-07,  1.6780236983e-05,  3.5382028323e-06,
                   -3.1440407164e-17, -2.4657923188e-17, -7.6942355346e-19, -9.3870404411e-07, -1.6780236983e-05,
                   -3.5382028323e-06, 2.9463503867e-06,  6.8303988006e-05,  1.6535049992e-05,  6.1258746234e-17,
                   4.6461451223e-17,  7.6478604808e-18,  -2.9463503867e-06, -6.8303988006e-05, -1.6535049992e-05,
                   9.3870404409e-07,  1.6780236983e-05,  3.5382028324e-06,  3.2682236874e-17,  1.0326602176e-17,
                   -7.1718279644e-18, -9.3870404412e-07, -1.6780236983e-05, -3.5382028324e-06, 9.3870404411e-07,
                   1.6780236983e-05,  3.5382028323e-06,  2.9463503866e-06,  6.8303988006e-05,  1.6535049992e-05,
                   9.3870404410e-07,  1.6780236983e-05,  3.5382028323e-06,  -2.8344541447e-17, -6.2572282577e-17,
                   2.3994643451e-17,  -8.1445182494e-17, 1.1690113463e-17,  -8.2382424450e-18, 1.7431938054e-18,
                   -4.2783210649e-17, 1.8456847921e-17,  -9.3870404408e-07, -1.6780236983e-05, -3.5382028324e-06,
                   -2.9463503866e-06, -6.8303988006e-05, -1.6535049992e-05, -9.3870404410e-07, -1.6780236983e-05,
                   -3.5382028324e-06 } };
    }


    template<>
    std::pair<std::vector<double>, std::vector<double>>
    ExpectedSubsetOfMatrix<GlobalVariationalOperatorNS::TyingPointsNS::None>()
    {
        return {
            { 0.5945109165,  -0.1342865306, -0.0454346154, 0.0246768222,  -0.2034302166, 0.0454280476,  -0.0834468537,
              0.0661632213,  -0.0049371636, 0.0246768222,  -0.2034302166, 0.0454280476,  -0.1239509146, -0.1698585946,
              0.056786693,   0.0264212272,  0.0671533416,  -0.019504926,  -0.0834468537, 0.0661632213,  -0.0049371636,
              0.0264212272,  0.0671533416,  -0.019504926,  0.0045664971,  -0.024688699,  0.005308248,   0.0777659443,
              -0.2518387531, 0.0629595679,  0.2518333106,  -0.1382624487, 0.034565554,   -0.062957976,  0.0345660568,
              -0.0086414738, 0.0388888365,  -0.125915815,  0.0314792626,  0.1259229005,  -0.0691274423, 0.0172822054,
              -0.0314802853, 0.0172822415,  -0.00432062,   -0.01944249,   0.062959075,   -0.0157398104, -0.0629600733,
              0.034564552,   -0.0086412348, 0.015739868,   -0.0086412873, 0.0021603348,  0.0777659443,  -0.2518387531,
              0.0629595679,  0.0388888365,  -0.125915815,  0.0314792626,  -0.01944249,   0.062959075,   -0.0157398104,
              0.2518333106,  -0.1382624487, 0.034565554,   0.1259229005,  -0.0691274423, 0.0172822054,  -0.0629600733,
              0.034564552,   -0.0086412348, -0.062957976,  0.0345660568,  -0.0086414738, -0.0314802853, 0.0172822415,
              -0.00432062,   0.015739868,   -0.0086412873, 0.0021603348 },

            { 0.0021603348,  -0.0086414653, -0.0157399428, -0.0043207193, 0.0172827963,  0.0314797336,  -0.0086414738,
              0.0345654916,  0.0629593508,  -0.0086414544, 0.0345657386,  0.0629595476,  0.0172831492,  -0.0691307449,
              -0.1259181611, 0.0345662909,  -0.1382619121, -0.2518369751, 0.0157398104,  -0.0629596874, -0.0194429936,
              -0.0314798233, 0.1259189044,  0.0388855978,  -0.0629595679, 0.2518375246,  0.0777706504,  0.0021606696,
              -0.004321437,  -0.0086428252, -0.0086427088, 0.0172856875,  0.0345713762,  -0.0157413398, 0.0314830865,
              0.0629661736,  -0.0086426925, 0.0172857839,  0.03457134,    0.0345708373,  -0.0691426024, -0.1382853732,
              0.0629653623,  -0.1259320596, -0.2518646702, 0.0157412825,  -0.0314832324, -0.0629661736, -0.0629652536,
              0.1259330012,  0.251864831,   -0.0194460593, 0.0388924914,  0.0777845047,  0.0053087127,  -0.019506135,
              -0.0049392434, -0.019506529,  0.056790557,   0.0454350759,  -0.0049382062, 0.0454308692,  -0.0454300181,
              -0.0246921747, 0.0671619271,  0.0661778845,  0.0671625066,  -0.1698801658, -0.2034685039, 0.0661754093,
              -0.2034603668, -0.1343375548, 0.0045687638,  0.0264166582,  -0.0834583124, 0.0264188863,  -0.1239464645,
              0.0246902681,  -0.0834615223, 0.02470746,    0.5945802181 }

        };
    }


    template<>
    LocalVector ExpectedVector<GlobalVariationalOperatorNS::TyingPointsNS::MITC4>()
    {
        return { { -9.5189233586e-06, -4.4775651163e-06, 9.9046883193e-06,  -2.8520203359e-05, -5.7656011623e-06,
                   3.7863346737e-05,  -9.5189233586e-06, -4.4775651163e-06, 9.9046883193e-06,  -3.7018498991e-05,
                   -6.7183232582e-06, 4.1220651349e-05,  -1.1006091472e-04, -3.3577424099e-05, 1.5788279681e-04,
                   -3.7018498991e-05, -6.7183232580e-06, 4.1220651349e-05,  -9.5189233586e-06, -4.4775651163e-06,
                   9.9046883193e-06,  -2.8520203359e-05, -5.7656011622e-06, 3.7863346737e-05,  -9.5189233586e-06,
                   -4.4775651163e-06, 9.9046883192e-06,  1.0190033203e-06,  1.8118632734e-05,  3.9665301025e-06,
                   -8.5108679400e-18, 6.7169448019e-17,  1.0966959085e-18,  -1.0190033202e-06, -1.8118632735e-05,
                   -3.9665301025e-06, 3.6347281709e-06,  7.2812970506e-05,  1.6724342109e-05,  -1.0281375911e-16,
                   -5.0468340580e-17, 4.6688456053e-18,  -3.6347281708e-06, -7.2812970506e-05, -1.6724342109e-05,
                   1.0190033202e-06,  1.8118632735e-05,  3.9665301026e-06,  3.0074963583e-17,  -2.7035174094e-17,
                   -6.7571206334e-17, -1.0190033202e-06, -1.8118632734e-05, -3.9665301025e-06, 2.0199952623e-05,
                   1.4820988144e-10,  2.7695843610e-05,  -4.7964677454e-10, 6.8267060445e-10,  -1.3126697170e-10,
                   2.0199952623e-05,  1.4820988144e-10,  2.7695843610e-05,  1.2924089590e-16,  1.0987327389e-20,
                   2.0966373609e-16,  -1.1432615111e-20, 2.7364136327e-20,  -1.5299812106e-20, 8.8328489861e-17,
                   2.1345011197e-20,  2.0267804362e-16,  -2.0199952623e-05, -1.4820988144e-10, -2.7695843610e-05,
                   4.7964677453e-10,  -6.8267060444e-10, 1.3126697169e-10,  -2.0199952623e-05, -1.4820988143e-10,
                   -2.7695843610e-05 } };
    }


    template<>
    std::pair<std::vector<double>, std::vector<double>>
    ExpectedSubsetOfMatrix<GlobalVariationalOperatorNS::TyingPointsNS::MITC4>()
    {
        return { { 4.2854818880e-01,  -2.1724967371e-01, -3.9544598382e-03, -5.8280191509e-02, -2.4491074804e-01,
                   6.6168182291e-02,  -4.1968346886e-02, 8.6902379306e-02,  -1.5307230931e-02, 2.1435422079e-01,
                   -1.0862159214e-01, -1.9770994648e-03, -2.9140095754e-02, -1.2245537402e-01, 3.3084091146e-02,
                   -2.0984173443e-02, 4.3452951965e-02,  -7.6536154660e-03, -1.0715633454e-01, 5.4311723131e-02,
                   9.8858700505e-04,  1.4570047877e-02,  6.1227687011e-02,  -1.6542045573e-02, 1.0492086722e-02,
                   -2.1725972465e-02, 3.8268077329e-03,  7.7769011695e-02,  -2.5183700513e-01, 6.2959330341e-02,
                   2.5183778349e-01,  -1.3826009548e-01, 3.4565245648e-02,  -6.2959096365e-02, 3.4565564222e-02,
                   -8.6414078862e-03, 3.8884505847e-02,  -1.2591850253e-01, 3.1479722614e-02,  1.2591989935e-01,
                   -6.9130047738e-02, 1.7282622813e-02,  -3.1479548182e-02, 1.7282782099e-02,  -4.3207039429e-03,
                   -1.9442252924e-02, 6.2959251275e-02,  -1.5739844895e-02, -6.2959661787e-02, 3.4565023869e-02,
                   -8.6413114097e-03, 1.5739774091e-02,  -8.6413910530e-03, 2.1603519715e-03,  -1.1572838025e-01,
                   -1.8348700593e-06, -2.3145284985e-02, -1.0778359445e-06, -3.1797714944e-06, 3.1936776203e-07,
                   -1.2263315197e-07, 1.0020439863e-06,  3.0346859382e-07,  1.5431290713e-01,  -1.2232128420e-06,
                   3.0862180068e-02,  -7.1855729636e-07, -2.1198476629e-06, 2.1291184135e-07,  -9.5751726612e-07,
                   6.6804067037e-07,  5.8275930093e-08,  -3.8577526772e-02, -5.6440541651e-12, -7.7153950097e-03,
                   1.4448576138e-17,  -8.6577408888e-18, 1.4506389704e-18,  1.4596030524e-07,  -1.8910331244e-12,
                   2.4006077633e-08 },
                 { 2.4006077632e-08,  1.1110281381e-01,  -4.6086783568e-08, -1.5431087565e-02, -6.1725492512e-02,
                   7.7156460937e-02,  -7.7153950097e-03, 1.9134289200e-01,  3.8578128746e-02,  -2.4777350176e-07,
                   -4.4441336974e-01, 2.8303456154e-06,  6.1723956857e-02,  2.4690031272e-01,  -3.0862485551e-01,
                   3.0861098052e-02,  -7.6537571176e-01, -1.5430635694e-01, -3.0346859383e-07, 3.3330994410e-01,
                   -1.5338228303e-06, 4.6292351853e-02,  1.8517565205e-01,  -2.3146943490e-01, 2.3145284985e-02,
                   7.5920923184e-01,  -1.1573714720e-01, 2.1367266164e-08,  -1.5431386660e-02, -7.7161183829e-03,
                   1.1111802260e-01,  -6.1728552786e-02, 1.9136969947e-01,  -1.4004383577e-07, 7.7159172766e-02,
                   3.8580692836e-02,  -9.5038774756e-08, 6.1725568376e-02,  3.0865372430e-02,  -4.4447208169e-01,
                   2.4691343706e-01,  -7.6547799007e-01, 3.1013301656e-07,  -3.0863705643e-01, -1.5432410741e-01,
                   -8.9847309231e-08, 4.6298523043e-02,  2.3147273943e-02,  3.3335293581e-01,  1.8518469979e-01,
                   7.5929802174e-01,  1.0347902204e-07,  -2.3148681948e-01, -1.1573941699e-01, 1.0308754491e-01,
                   -1.0007736192e-06, 1.3951859617e-01,  1.7428355373e-07,  1.2400244165e-06,  -3.4929259805e-06,
                   1.3950338317e-01,  4.3674168086e-06,  1.4568372768e-01,  -5.5804428585e-01, 3.3785320075e-06,
                   -9.8277839977e-01, -1.0353586731e-06, -1.3802960762e-06, 6.7080287070e-06,  -9.8274525655e-01,
                   -4.2825478244e-06, -1.6987947783e+00, 2.3274136419e-01,  1.8959080581e-06,  7.3211758697e-01,
                   -1.7273215634e-06, -3.3104685307e-06, 1.3554969041e-05,  7.3216863223e-01,  -2.2591085745e-05,
                   1.9975493867e+00 } };
    }


    template<>
    LocalVector ExpectedVector<GlobalVariationalOperatorNS::TyingPointsNS::MITC9>()
    {
        return { { -1.0917119781e-05, -3.6526022619e-06, 1.0992179761e-05,  -3.4868651800e-05, -1.0074026026e-05,
                   4.1397994166e-05,  -1.0917119781e-05, -3.6526022619e-06, 1.0992179761e-05,  -3.4868686138e-05,
                   -1.0073924810e-05, 4.1397944474e-05,  -1.0475676241e-04, -2.2337415676e-05, 1.5558304749e-04,
                   -3.4868686138e-05, -1.0073924810e-05, 4.1397944474e-05,  -1.0917119781e-05, -3.6526022620e-06,
                   1.0992179761e-05,  -3.4868651800e-05, -1.0074026026e-05, 4.1397994166e-05,  -1.0917119781e-05,
                   -3.6526022619e-06, 1.0992179761e-05,  9.3870733289e-07,  1.6780218848e-05,  3.5382117981e-06,
                   2.4629521114e-17,  -3.9529862978e-17, 1.7672918928e-17,  -9.3870733291e-07, -1.6780218848e-05,
                   -3.5382117981e-06, 2.9463574598e-06,  6.8303989947e-05,  1.6535054977e-05,  1.0838633593e-17,
                   -1.2890359150e-17, 3.8318499984e-17,  -2.9463574598e-06, -6.8303989947e-05, -1.6535054977e-05,
                   9.3870733292e-07,  1.6780218848e-05,  3.5382117981e-06,  -1.0536031073e-18, -7.6817417986e-18,
                   -6.7771106110e-18, -9.3870733291e-07, -1.6780218848e-05, -3.5382117981e-06, 9.3870522103e-07,
                   1.6780202297e-05,  3.5382252295e-06,  2.9463507104e-06,  6.8303916660e-05,  1.6535105703e-05,
                   9.3870522103e-07,  1.6780202297e-05,  3.5382252296e-06,  5.7432513123e-17,  1.7065400428e-16,
                   -3.8737993051e-18, -3.2826232475e-17, 2.2698365404e-18,  -1.4452499663e-17, 1.2933134313e-18,
                   -1.2023547983e-16, -6.0240983209e-17, -9.3870522108e-07, -1.6780202297e-05, -3.5382252295e-06,
                   -2.9463507104e-06, -6.8303916660e-05, -1.6535105703e-05, -9.3870522103e-07, -1.6780202297e-05,
                   -3.5382252295e-06 } };
    }


    template<>
    std::pair<std::vector<double>, std::vector<double>>
    ExpectedSubsetOfMatrix<GlobalVariationalOperatorNS::TyingPointsNS::MITC9>()
    {
        return {
            { 0.5392048883,  -0.0236714621, -0.1007418965, 0.0424559426,  -0.2389850321, 0.0632054681,  -0.0804850615,
              0.0602368539,  -0.0019741523, 0.0424556552,  -0.2389899439, 0.0632073805,  -0.0923486356, -0.2330672302,
              0.0883916459,  0.0165459754,  0.0869060041,  -0.0293815546, -0.0804842727, 0.0602381824,  -0.0019745711,
              0.0165450727,  0.086906091,   -0.0293813835, 0.006541703,   -0.0286392019, 0.0072835348,  0.0777696878,
              -0.2518371908, 0.0629581852,  0.2518307506,  -0.1382622978, 0.0345650494,  -0.0629564856, 0.0345661965,
              -0.0086413435, 0.0388869815,  -0.125918727,  0.0314800613,  0.1259225126,  -0.0691246899, 0.0172828845,
              -0.0314806571, 0.0172803984,  -0.0043205795, -0.0194426983, 0.0629595791,  -0.0157397828, -0.0629595502,
              0.0345639254,  -0.0086413975, 0.0157397352,  -0.0086408745, 0.002160327,   0.0777680012,  -0.251836793,
              0.0629580494,  0.038887787,   -0.1259165419, 0.031479873,   -0.0194426439, 0.0629590127,  -0.015739746,
              0.2518347741,  -0.1382628341, 0.034564984,   0.1259212639,  -0.0691288916, 0.0172831708,  -0.0629598055,
              0.034564972,   -0.0086414342, -0.0629585043, 0.0345661247,  -0.0086412782, -0.031480064,  0.0172823267,
              -0.004320683,  0.0157398652,  -0.0086413163, 0.0021603313 },
            { 0.0021603313,  -0.0086414617, -0.0157398888, -0.0043206679, 0.0172829203,  0.0314796282,  -0.0086412782,
              0.0345651908,  0.0629591515,  -0.0086412023, 0.0345655821,  0.0629590259,  0.0172821301,  -0.069130962,
              -0.1259166606, 0.0345651911,  -0.1382603639, -0.2518359295, 0.015739746,   -0.0629597975, -0.0194426391,
              -0.0314799149, 0.1259192584,  0.038885826,   -0.0629580494, 0.2518371279,  0.0777677061,  0.0021607231,
              -0.0043213885, -0.0086427292, -0.008642943,  0.0172858851,  0.0345710923,  -0.0157414737, 0.0314836507,
              0.0629662054,  -0.008642781,  0.0172851642,  0.0345710006,  0.0345713282,  -0.0691419871, -0.138284795,
              0.0629657077,  -0.1259338701, -0.2518652616, 0.0157415789,  -0.031483513,  -0.0629655036, -0.0629663299,
              0.1259349072,  0.2518632924,  -0.0194465428, 0.0388939714,  0.0777846113,  0.0053087708,  -0.0195062709,
              -0.0049394026, -0.0195066808, 0.0567908813,  0.0454354795,  -0.0049383019, 0.0454311456,  -0.0454303303,
              -0.024692665,  0.0671628562,  0.0661797264,  0.0671635193,  -0.169881815,  -0.2034724341, 0.066176794,
              -0.2034643319, -0.1343343479, 0.0045693722,  0.0264151208,  -0.083459975,  0.026417879,   -0.1239438029,
              0.0246929596,  -0.0834648927, 0.024715086,   0.5945756601 }

        };
    }


} // namespace
