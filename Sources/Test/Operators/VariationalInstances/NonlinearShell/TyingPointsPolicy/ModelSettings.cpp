/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/NonlinearShell/TyingPointsPolicy/InputData.hpp"


namespace MoReFEM::TestNS::MITCNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>>(
            { " vectorial" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>>({ " vectorial" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
    }


} // namespace MoReFEM::TestNS::MITCNS
