/*!
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 2 Aug 2017 12:30:53 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    Model::Model(const morefem_data_type& morefem_data)
    : parent(morefem_data, create_domain_list_for_coords::yes, print_banner::no)
    {
        decltype(auto) mpi = parent::GetMpi();

        if (mpi.Nprocessor<int>() > 1)
        {
            throw Exception("The point of this test is to check operator assemble correctly into a matrix and/or "
                            "a vector; the expected values assume the dof numbering of the sequential case. Please "
                            "run it sequentially.",
                            __FILE__,
                            __LINE__);
        }
    }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetMoReFEMData();
        

        decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) domain_volume = domain_manager.GetDomain(AsDomainId(DomainIndex::volume), __FILE__, __LINE__);

        quadrature_rule_per_topology_for_operators_ = std::make_unique<const QuadratureRulePerTopology>(3, 2);
        quadrature_rule_per_topology_for_operators_->Print(std::cout);

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        solid_ = std::make_unique<Solid>(morefem_data, domain_volume, felt_space.GetQuadratureRulePerTopology());

        hyperelastic_law_parent::Create(*solid_);

        // Required to enable construction of an operator after initialization step.
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::NonlinearShell
