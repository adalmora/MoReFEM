/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/GradGrad/InputData.hpp"


namespace MoReFEM::TestNS::GradGrad
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P1)>>(
            { " scalar_unknown_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_scalar_unknown_P1)>>(
            { " other_scalar_unknown_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar_unknown_P2)>>(
            { " scalar_unknown_P2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial_unknown_P1)>>(
            { " vectorial_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P1)>>(
            { " scalar_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_scalar_unknown_P1)>>(
            { " other_scalar_unknown_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar_unknown_P2)>>(
            { " scalar_unknown_P2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial_unknown_P1)>>(
            { " vectorial_unknown_P1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::GradGrad
