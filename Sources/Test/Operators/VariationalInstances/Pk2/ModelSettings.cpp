/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/Pk2/InputData.hpp"


namespace MoReFEM::TestNS::Pk2
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p1)>>(
            { " displacement_p1 " });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_p2)>>(
            { " displacement_p2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p1)>>({ " displacement_p1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_p2)>>({ " displacement_p2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_displacement_p1)>>(
            { " other_displacement_p1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>>({ " volume" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::Pk2
