/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/Mass/InputData.hpp"


namespace MoReFEM::TestNS::Mass
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_P1)>>(
            { " potential_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::other_potential_P1)>>(
            { " other_potential_P1" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::potential_P2)>>(
            { " potential_P2" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement_P1)>>(
            { " displacement_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P1)>>({ " potential_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::other_potential_P1)>>(
            { " other_potential_P1" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::potential_P2)>>({ " potential_P2" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement_P1)>>({ " displacement_P1" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::Mass
