/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Jan 2016 15:34:09 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Operators/VariationalInstances/Mass/Model.hpp"


namespace MoReFEM::TestNS::Mass
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test Mass operator");
        return name;
    }


} // namespace MoReFEM::TestNS::Mass


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_MASS_x_MODEL_HXX_
