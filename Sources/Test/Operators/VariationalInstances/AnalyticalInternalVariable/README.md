This operator has been tested against a Matlab script independently coded by Patrick Le Tallec and Chloé Giraudet for the local contributions to the first derivative of the energetic potential dW and the second order derivative d2W.

This operator also shows a quadratic convergence for the Newton method. 

The expected results here are the ones which come from the validation case against which the operator was tested.

Update 2022: The step of checking the results against reference one was not properly implemented (this was found during
work on #1707). An expected results directory has been created from the output existing at the time of this ticket,
but there were no new verification the results were still properly in line with the test case mentioned above.
