/*!
// \file
//
*/

#include "Test/Operators/VariationalInstances/AnalyticalInternalVariable/InputData.hpp"


namespace MoReFEM::TestNS::AnalyticalInternalVariable
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::highest_dimension)>>(
            { "Finite element space for highest geometric dimension" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::face1)>>(
            { "Finite element space for face 1" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>>(
            { " displacement" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::electrical_activation)>>(
            { " electrical_activation" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>>({ " displacement" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::electrical_activation)>>(
            { " electrical_activation" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::face1)>>({ " face1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::highest_dimension)>>(
            { " highest_dimension" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::all_faces)>>({ " all_faces" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge1)>>({ " edge1" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge2)>>({ " edge2" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::edge3)>>({ " edge3" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge1)>>(
            { " edge1" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge2)>>(
            { " edge2" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::edge3)>>(
            { " edge3" });

        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber),
                                          ::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>>({ "Vectorial fiber at node" });

        SetDescription<InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Unused" });


        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(ForceIndexList::surfacic_force_face1)>>(
            { "Surfacic force on face 1" });
    }


} // namespace MoReFEM::TestNS::AnalyticalInternalVariable
