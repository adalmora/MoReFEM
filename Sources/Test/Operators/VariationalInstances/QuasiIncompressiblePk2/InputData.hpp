/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

namespace MoReFEM::TestNS::QuasiIncompressiblePk2
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { volume = 1 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { monolithic = 1, displacement = 2, pressure = 3 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { displacement = 1, pressure = 2, monolithic = 3 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { displacement = 1, pressure = 2 };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex

    {
        solver = 1
    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::monolithic)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::displacement)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

        InputDataNS::Solid::VolumicMass,
        InputDataNS::Solid::HyperelasticBulk,
        InputDataNS::Solid::Kappa1,
        InputDataNS::Solid::Kappa2,
        InputDataNS::Solid::CheckInvertedElements,

        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::monolithic)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::volume)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::monolithic)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::displacement)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>::IndexedSectionDescription,
        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::QuasiIncompressiblePk2


#endif // MOREFEM_x_TEST_x_OPERATORS_x_VARIATIONAL_INSTANCES_x_QUASI_INCOMPRESSIBLE_PK2_x_INPUT_DATA_HPP_
