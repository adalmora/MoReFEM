/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_OPERATORS_x_SUBSET_OR_SUPERSET_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_OPERATORS_x_SUBSET_OR_SUPERSET_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::ConformProjectorNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { domain = 1 };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { velocity_pressure = 1, velocity = 2, pressure = 3 };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { velocity = 1, pressure = 2 };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t { velocity_pressure = 1, velocity = 2, pressure = 3 };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_pressure)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity_pressure)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_pressure)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>::IndexedSectionDescription,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity_pressure)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>::IndexedSectionDescription

    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::ConformProjectorNS


#endif // MOREFEM_x_TEST_x_OPERATORS_x_SUBSET_OR_SUPERSET_x_INPUT_DATA_HPP_
