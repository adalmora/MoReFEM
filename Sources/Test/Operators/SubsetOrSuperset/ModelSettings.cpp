/*!
// \file
//
*/

#include "Test/Operators/SubsetOrSuperset/InputData.hpp"


namespace MoReFEM::TestNS::ConformProjectorNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity)>>(
            { "Finite element space with only velocity" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::pressure)>>(
            { "Finite element space with only pressure" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::velocity_pressure)>>(
            { "Finite element space with both velocity and pressure" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity_pressure)>>(
            { " velocity_pressure" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::velocity)>>(
            { " velocity" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::pressure)>>(
            { " pressure" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::velocity)>>({ " velocity" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::pressure)>>({ " pressure" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
    }


} // namespace MoReFEM::TestNS::ConformProjectorNS
