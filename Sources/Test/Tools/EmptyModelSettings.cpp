//! \file
//
//
//  EmptyModelSettings.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 19/04/2023.
//
//

#include "Test/Tools/EmptyModelSettings.hpp"


namespace MoReFEM::TestNS
{


    void EmptyModelSettings::Init()
    {
        CheckTupleCompletelyFilled();
    }


} // namespace MoReFEM::TestNS
