add_library(MoReFEM_extensive_test_tools ${LIBRARY_TYPE} "")
set(MOREFEM_EXTENSIVE_TEST_TOOLS MoReFEM_extensive_test_tools)


add_library(MoReFEM_basic_test_tools ${LIBRARY_TYPE} "")
set(MOREFEM_BASIC_TEST_TOOLS MoReFEM_basic_test_tools)

target_link_libraries(${MOREFEM_BASIC_TEST_TOOLS}
                      ${LIB_BOOST_TEST})

target_link_libraries(${MOREFEM_EXTENSIVE_TEST_TOOLS}
                      ${MOREFEM_BASIC_TEST_TOOLS})
                      
# Shared libraries may need elements from MOREFEM_UTILITIES; static ones would
# yell about duplicate symbols (at least on Linux systems).
if(LIBRARY_TYPE MATCHES SHARED)
    target_link_libraries(${MOREFEM_BASIC_TEST_TOOLS}
                          $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_UTILITIES}>)
                          
    target_link_libraries(${MOREFEM_EXTENSIVE_TEST_TOOLS}
                          $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>)
endif()


# This library encompasses only first level tools related to the handling 
# of environment variable.
# Typically, your test executable will need to also be linked against MoReFEM
# library or sub-library (when --morefem_as_single_library is set to `false`)
# - by default MOREFEM_BASIC_TEST_TOOLS is linked against the lowest sub library
# (MOREFEM_UTILITIES) for shared libraries but if you need higher level functionalities
# you will need to add a dependency with `target_link_libraries`.
# For instance
# ````
# target_link_libraries(TestPetscVectorIO
#                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
#                      ${MOREFEM_BASIC_TEST_TOOLS})
# ````
target_sources(MoReFEM_basic_test_tools
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/TestEnvironment.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/TestEnvironment.hpp
        ${CMAKE_CURRENT_LIST_DIR}/EmptyModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/EmptyModelSettings.hpp
)
          
morefem_organize_IDE(MoReFEM_basic_test_tools Test Test/Tools)
                      
# A more extensive library for test related facilities.
# See the comment above for MoReFEM_basic_test_tools; for this one
# the basic library is MOREFEM_FELT.
target_sources(MoReFEM_extensive_test_tools
	PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
		${CMAKE_CURRENT_LIST_DIR}/TestLinearAlgebra.cpp 
		${CMAKE_CURRENT_LIST_DIR}/TestLinearAlgebra.hpp 
		${CMAKE_CURRENT_LIST_DIR}/TestLinearAlgebra.hxx
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/Enum.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/Model.hxx
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/ModelNoInputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Fixture/ModelNoInputData.hxx
        ${CMAKE_CURRENT_LIST_DIR}/InitMoReFEMDataFromCLI.hpp
        ${CMAKE_CURRENT_LIST_DIR}/InitMoReFEMDataFromCLI.hxx
        ${CMAKE_CURRENT_LIST_DIR}/CheckIdenticalFiles.cpp
        ${CMAKE_CURRENT_LIST_DIR}/CheckIdenticalFiles.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CheckIdenticalFiles.hxx
        ${CMAKE_CURRENT_LIST_DIR}/CompareDataFiles.cpp
        ${CMAKE_CURRENT_LIST_DIR}/CompareDataFiles.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CompareDataFiles.hxx
        ${CMAKE_CURRENT_LIST_DIR}/Internal/CompareDataFilesImpl.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Internal/CompareDataFilesImpl.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Internal/CompareDataFilesImpl.hxx
        ${CMAKE_CURRENT_LIST_DIR}/BareModel.hpp
        ${CMAKE_CURRENT_LIST_DIR}/BareModel.hxx
        ${CMAKE_CURRENT_LIST_DIR}/ClearSingletons.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ClearSingletons.hpp
)

morefem_organize_IDE(MoReFEM_extensive_test_tools Test Test/Tools)
