//! \file
//
//
//  ClearSingletons.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 18/01/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_CLEAR_SINGLETONS_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_CLEAR_SINGLETONS_HPP_


namespace MoReFEM::TestNS
{


    /*!
     * \brief A struct to shelter the ability to clear all defined unique ids.
     *
     * \internal A struct is used as the friendship to a mere function cause many -Wredundant-decls in gcc which doesn't undesrtand correctly
     * the forward declarations. Forward declaration to a struct is better handled by this compiler.
     */
    struct ClearSingletons
    {

        /*!
         * \brief Clear all the defined unique ids.
         *
         * In some tests, we would like to load several Lua files which uses up the same \a input_data_tuple. In
         * fulll-fledged models we don't want to encourage that as it could lead to painful bugs to solve, but for tests
         * it is really handy to be able to clear the content so that many tests can be placed in the same file (without
         * that we need to multiply the number of executables to generate).
         */
        static void Do();
    };

} // namespace MoReFEM::TestNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_CLEAR_SINGLETONS_HPP_
