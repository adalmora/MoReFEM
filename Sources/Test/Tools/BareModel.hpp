//! \file
//
//
//  BareModel.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 11/01/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HPP_

#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Model/Model.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief Helper class when in a test you need a very bare-bone \a Model which implements nothing more than what is in the \a Model CRTP of the
     * library.
     *
     * \attention Don't forget to call \a Initialize() after construction!
     *
     *
     * \tparam TimeDependencyT Time dependency policy; None is chosen in most tests.
     *
     * \copydoc doxygen_hide_do_consider_processor_wise_local_2_global_tparam
     *
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT = ::MoReFEM::TimeManagerNS::Policy::None,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
            = DoConsiderProcessorWiseLocal2Global::no
    >
    class BareModel
    : public ::MoReFEM::Model
             <
                 BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>,
                 MoReFEMDataTypeT,
                 DoConsiderProcessorWiseLocal2GlobalT,
                 TimeDependencyT
             >
    // clang-format on
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>;

        //! Convenient alias.
        using parent = ::MoReFEM::
            Model<self, MoReFEMDataTypeT, DoConsiderProcessorWiseLocal2GlobalT, TimeDependencyT>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         * \param[in] a_create_domain_list_for_coords Whether the model will compute the list of domains a coord is in
         * or not.
         */
        BareModel(const MoReFEMDataTypeT& morefem_data,
                  create_domain_list_for_coords a_create_domain_list_for_coords = create_domain_list_for_coords::no);

        //! Destructor.
        ~BareModel() = default;

        //! \copydoc doxygen_hide_copy_constructor
        BareModel(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        BareModel(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


        //! Manage time iteration.
        void Forward()
            requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>);

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep()
            requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>);


      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const
            requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>);


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep()
            requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>);


        ///@}
    };


} // namespace MoReFEM::TestNS


#include "Test/Tools/BareModel.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HPP_
