/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENUM_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENUM_HPP_

namespace MoReFEM::TestNS::FixtureNS
{


    //! Whether a Run() method should be called at first call or not.
    enum class call_run_method_at_first_call { no, yes };


} // namespace MoReFEM::TestNS::FixtureNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_ENUM_HPP_
