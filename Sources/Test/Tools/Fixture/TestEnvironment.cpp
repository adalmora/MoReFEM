/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    TestEnvironment::TestEnvironment()
    {
        static bool first_call = true;

        if (first_call)
        {
            decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
            decltype(auto) cli_args = master_test_suite.argv;

            BOOST_CHECK(master_test_suite.argc > 2);

            {
                // First set environment variables, which by convention for tests are the second and third argument
                // in CLI.
                decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

                environment.SetEnvironmentVariable(std::make_pair("MOREFEM_ROOT", cli_args[1]), __FILE__, __LINE__);
                environment.SetEnvironmentVariable(
                    std::make_pair("MOREFEM_TEST_OUTPUT_DIR", cli_args[2]), __FILE__, __LINE__);
            }

            {
                Internal::PetscNS::RAII::CreateOrGetInstance(
                    __FILE__, __LINE__, master_test_suite.argc, master_test_suite.argv);

#ifdef MOREFEM_WITH_SLEPC
                Internal::SlepcNS::RAII::CreateOrGetInstance(__FILE__, __LINE__);
#endif // MOREFEM_WITH_SLEPC
            }

            first_call = false;
        }
    }


    const Wrappers::Mpi& TestEnvironment::GetMpi() noexcept
    {
        decltype(auto) raii = Internal::PetscNS::RAII::GetInstance(__FILE__, __LINE__);
        return raii.GetMpi();
    }


} // namespace MoReFEM::TestNS::FixtureNS
