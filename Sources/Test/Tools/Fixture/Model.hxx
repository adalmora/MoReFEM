/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_

// IWYU pragma: private, include "Test/Tools/Fixture/Model.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    Model<ModelT, CallRunMethodT>::Model()
    {
        static bool first_call = true;

        if (first_call)
        {
            const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

            if (root_value != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                         "(currently it is set to "
                      << root_value << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }


            const auto prepartitioned_data_dir = std::getenv("MOREFEM_PREPARTITIONED_DATA_DIR");

            if (prepartitioned_data_dir != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_PREPARTITIONED_DATA_DIR environment variables unset! "
                         "(currently it is set to "
                      << prepartitioned_data_dir << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            first_call = false;
        }
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    const ModelT& Model<ModelT, CallRunMethodT>::GetModel()
    {
        static ModelT model(GetMoReFEMData());

        if constexpr (CallRunMethodT == call_run_method_at_first_call::yes)
        {
            static bool first_call = true;

            if (first_call)
            {
                model.Run();
                first_call = false;
            }
        }

        return model;
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    ModelT& Model<ModelT, CallRunMethodT>::GetNonCstModel()
    {
        return const_cast<ModelT&>(GetModel());
    }


    // clang-format off
    template
    <
        class ModelT,
        call_run_method_at_first_call CallRunMethodT
    >
    // clang-format on
    auto Model<ModelT, CallRunMethodT>::GetMoReFEMData()
    -> const morefem_data_type&
    {
        static auto ret{ InitMoReFEMDataFromCLI<typename morefem_data_type::input_data_type, typename morefem_data_type::model_settings_type>() };
        return ret;
    }


} // namespace MoReFEM::TestNS::FixtureNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_HXX_
