/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_TEST_ENVIRONMENT_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_TEST_ENVIRONMENT_HPP_

#include "Utilities/Environment/Environment.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::FixtureNS
{


    /*!
     * \brief An helper class to build tests with with Boost test.
     *
     * This class:
     * - Defines environment variables MOREFEM_ROOT and MOREFEM_TEST_OUTPUT_DIR which are expected
     * to be given on the command line in second and third position.
     * - Set up mpi data.
     */
    struct TestEnvironment
    {
      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit TestEnvironment();

        //! Destructor.
        ~TestEnvironment() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TestEnvironment(const TestEnvironment& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TestEnvironment(TestEnvironment&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TestEnvironment& operator=(const TestEnvironment& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TestEnvironment& operator=(TestEnvironment&& rhs) = delete;

        ///@}

        //! Accessor to the Mpi object.
        const Wrappers::Mpi& GetMpi() noexcept;
    };


} // namespace MoReFEM::TestNS::FixtureNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_TEST_ENVIRONMENT_HPP_
