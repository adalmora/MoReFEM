/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 16 Mar 2018 12:33:49 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_NO_INPUT_DATA_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_NO_INPUT_DATA_HXX_

// IWYU pragma: private, include "Test/Tools/Fixture/ModelNoInputData.hpp"

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


namespace MoReFEM::TestNS::FixtureNS
{


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    ModelNoInputData<ModelT, OutputDirWrapperT, CallRunMethodT, DoCreateDomainListForCoordsT>::ModelNoInputData()
    {
        static bool first_call = true;

        if (first_call)
        {
            const auto root_value = std::getenv("MOREFEM_RESULT_DIR");

            if (root_value != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_RESULT_DIR environment variables unset! "
                         "(currently it is set to "
                      << root_value << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }


            const auto prepartitioned_data_dir = std::getenv("MOREFEM_PREPARTITIONED_DATA_DIR");

            if (prepartitioned_data_dir != nullptr)
            {
                std::ostringstream oconv;
                oconv << "Please run the tests with MOREFEM_PREPARTITIONED_DATA_DIR environment variables unset! "
                         "(currently it is set to "
                      << prepartitioned_data_dir << ").";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            first_call = false;
        }
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    const ModelT& ModelNoInputData<ModelT, OutputDirWrapperT, CallRunMethodT, DoCreateDomainListForCoordsT>::GetModel()
    {
        static ModelT model(GetMoReFEMData(), DoCreateDomainListForCoordsT);

        if constexpr (CallRunMethodT == call_run_method_at_first_call::yes)
        {
            static bool first_call = true;

            if (first_call)
            {
                model.Run();
                first_call = false;
            }
        }

        return model;
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    ModelT& ModelNoInputData<ModelT, OutputDirWrapperT, CallRunMethodT, DoCreateDomainListForCoordsT>::GetNonCstModel()
    {
        return const_cast<ModelT&>(GetModel());
    }


    // clang-format off
    template
    <
        class ModelT,
        class OutputDirWrapperT,
        call_run_method_at_first_call CallRunMethodT,
        create_domain_list_for_coords DoCreateDomainListForCoordsT
    >
    // clang-format on
    auto
    ModelNoInputData<ModelT, OutputDirWrapperT, CallRunMethodT, DoCreateDomainListForCoordsT>::GetMoReFEMData()
    -> const morefem_data_type&
    {
        static auto ret{ MoReFEMDataForTest<model_settings_type>(OutputDirWrapperT::Path()) };
        return ret;
    }


} // namespace MoReFEM::TestNS::FixtureNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_FIXTURE_x_MODEL_NO_INPUT_DATA_HXX_
