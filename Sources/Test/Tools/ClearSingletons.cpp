//! \file
//
//
//  ClearSingletons.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 18/01/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#include <ostream>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Singleton/Exceptions/Singleton.hpp"


#include "Core/Parameter/FiberEnum.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Test/Tools/ClearSingletons.hpp"


namespace MoReFEM::TestNS
{


    void ClearSingletons::Do()
    {
        {
            decltype(auto) manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            decltype(auto) manager = DomainManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            decltype(auto) manager = UnknownManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            decltype(auto) manager =
                Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            decltype(auto) manager = GodOfDofManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            decltype(auto) manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance(__FILE__, __LINE__);
            manager.Clear();
        }

        {
            try
            {
                decltype(auto) manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                              ParameterNS::Type::scalar>::GetInstance(__FILE__, __LINE__);
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                              ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__);
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                              ParameterNS::Type::scalar>::GetInstance(__FILE__, __LINE__);
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager =
                    FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                              ParameterNS::Type::vector>::GetInstance(__FILE__, __LINE__);
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }

        {
            try
            {
                decltype(auto) manager = Advanced::LightweightDomainListManager::GetInstance(__FILE__, __LINE__);
                manager.Clear();
            }
            catch (const Utilities::ExceptionNS::Singleton::NotYetCreated&)
            {
                // Do nothing: if not existing no need to clear it!
            }
        }
    }


} // namespace MoReFEM::TestNS
