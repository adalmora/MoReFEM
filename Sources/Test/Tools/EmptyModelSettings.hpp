//! \file
//
//
//  EmptyModelSettings.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 19/04/2023.
//
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_EMPTY_MODEL_SETTINGS_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_EMPTY_MODEL_SETTINGS_HPP_

#include <tuple>

#include "Utilities/InputData/ModelSettings.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief Placeholder when there are no \a ModelSettings.
     *
     * Shouldn't be used much - in fact most of the tests should probably from now use exclusively
     * \a ModelSettings and let \a InputData empty.
     */
    struct EmptyModelSettings : public ModelSettings<std::tuple<>>
    {
        //! Override of \a Init() method - which doesn't do anything.
        void Init() override;
    };

} // namespace MoReFEM::TestNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_EMPTY_MODEL_SETTINGS_HPP_
