/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Mar 2018 16:10:26 +0100
// Copyright (c) Inria. All rights reserved.
//
*/

#ifndef MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HXX_

// IWYU pragma: private, include "Test/Tools/InitMoReFEMDataFromCLI.hpp"


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


namespace MoReFEM::TestNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    MoReFEMData<InputDataT, ModelSettingsT, program_type::test> InitMoReFEMDataFromCLI()
    {
        decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
        decltype(auto) cli_args = master_test_suite.argv;

        BOOST_CHECK(master_test_suite.argc > 3);

        FilesystemNS::File lua_file{ std::filesystem::path{ cli_args[3] } };

        return MoReFEMData<InputDataT, ModelSettingsT, program_type::test>{ std::move(lua_file) };
    }


} // namespace MoReFEM::TestNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HXX_
