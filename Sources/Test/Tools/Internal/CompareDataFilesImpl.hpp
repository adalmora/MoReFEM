/*!
 */


#ifndef MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Filesystem/File.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS::Internal
{


    /*!
     * \brief Base implementation of a class that enables comparison of two output files to check their content is
     * consistent (typically a reference file kept in the repository and the one obtained by a test).
     *
     * This class is then inherited by  \a CompareDataFiles which is a template class which specifies exactly how the
     * file may be converted in a vector of doubles (the point of the current class is to make compilable all the parts
     * that are in common - only some very specific instructions are dependent on the format chosen).
     */
    class CompareDataFilesImpl
    {
      public:
        /*!
         * \copydoc doxygen_hide_compare_data_files_constructor
         */
        CompareDataFilesImpl(const FilesystemNS::Directory& ref_dir,
                             const FilesystemNS::Directory& obtained_dir,
                             std::string&& filename,
                             const char* invoking_file,
                             int invoking_line,
                             double epsilon);

        //! Destructor.
        ~CompareDataFilesImpl() = default;

        //! \copydoc doxygen_hide_copy_constructor
        CompareDataFilesImpl(const CompareDataFilesImpl& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        CompareDataFilesImpl(CompareDataFilesImpl&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        CompareDataFilesImpl& operator=(const CompareDataFilesImpl& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CompareDataFilesImpl& operator=(CompareDataFilesImpl&& rhs) = delete;

      protected:
        //! Accessor to the reference file.
        const FilesystemNS::File& GetReferenceFile() const noexcept;

        //! Accessor to the file obtained by the test.
        const FilesystemNS::File& GetObtainedFile() const noexcept;

        //! Tolerance used to declare two floating points numbers equal.
        double GetEpsilon() const noexcept;

      protected:
        /*!
         * \brief Check the content of two results files are equal (or almost equal with \a epsilon_ tolerance).
         *
         * \param[in] ref Vector which holds the values for the reference file.
         * \param[in] obtained Vector which holds the values obtained with the runtime of the test.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        void CheckAreEquals(const std::vector<double>& ref,
                            const std::vector<double>& obtained,
                            const char* invoking_file,
                            int invoking_line) const;

      private:
        //! Reference file stored in the repository (usually under a \a ExpectedResults folder).
        FilesystemNS::File reference_file_;

        //! File obtained by the test in which this class is called.
        FilesystemNS::File obtained_file_;

        //! Tolerance used to declare two floating points numbers equal.
        const double epsilon_;
    };


} // namespace MoReFEM::TestNS::Internal


#include "Test/Tools/Internal/CompareDataFilesImpl.hxx"

#endif // MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HPP_
