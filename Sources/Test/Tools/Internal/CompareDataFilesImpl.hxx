/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HXX_

// IWYU pragma: private, include "Test/Tools/Internal/CompareDataFilesImpl.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS::Internal
{


    inline const FilesystemNS::File& CompareDataFilesImpl::GetReferenceFile() const noexcept
    {
        return reference_file_;
    }


    inline const FilesystemNS::File& CompareDataFilesImpl::GetObtainedFile() const noexcept
    {
        return obtained_file_;
    }


    inline double CompareDataFilesImpl::GetEpsilon() const noexcept
    {
        return epsilon_;
    }


} // namespace MoReFEM::TestNS::Internal

#endif // MOREFEM_x_TEST_x_TOOLS_x_INTERNAL_x_COMPARE_DATA_FILES_IMPL_HXX_
