/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 13 Apr 2018 13:08:50 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cassert>
#include <sstream>
#include <vector>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/Internal/CompareDataFilesImpl.hpp"


namespace MoReFEM::TestNS::Internal
{


    CompareDataFilesImpl::CompareDataFilesImpl(const FilesystemNS::Directory& ref_dir,
                                               const FilesystemNS::Directory& obtained_dir,
                                               std::string&& filename,
                                               const char* invoking_file,
                                               int invoking_line,
                                               double epsilon)
    : epsilon_{ epsilon }
    {
        std::ostringstream oconv;

        if (!ref_dir.DoExist())
        {
            oconv << "Reference folder " << ref_dir << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        if (!obtained_dir.DoExist())
        {
            oconv << "Result folder " << obtained_dir << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        reference_file_ = ::MoReFEM::FilesystemNS::File{ ref_dir.AddFile(filename) };
        obtained_file_ = ::MoReFEM::FilesystemNS::File{ obtained_dir.AddFile(filename) };

        if (!reference_file_.DoExist())
        {
            oconv << "Reference file " << reference_file_ << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        if (!obtained_file_.DoExist())
        {
            oconv << "Result file " << obtained_file_ << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


    void CompareDataFilesImpl::CheckAreEquals(const std::vector<double>& ref,
                                              const std::vector<double>& obtained,
                                              const char* invoking_file,
                                              int invoking_line) const
    {
        assert(ref.size() == obtained.size()
               && "Should have been tested properly by the test facility "
                  "before this function call.");

        const auto end_obtained = obtained.cend();

        for (auto it_obtained = obtained.cbegin(), it_ref = ref.cbegin(); it_obtained != end_obtained;
             ++it_obtained, ++it_ref)
        {
            const auto obtained_value = *it_obtained;
            const auto ref_value = *it_ref;

            // Crude condition, that is ok for most of the cases!
            if (!NumericNS::AreEqual(ref_value, obtained_value, epsilon_))
            {
                std::ostringstream oconv;
                oconv << it_ref - ref.cbegin() << "-th value in " << reference_file_
                      << " is not identical to what "
                         "was stored as a reference in file "
                      << obtained_file_ << "(values are respectively " << ref_value << " and " << obtained_value
                      << ").";
                throw Exception(oconv.str(), invoking_file, invoking_line);
            }
        }
    }


} // namespace MoReFEM::TestNS::Internal
