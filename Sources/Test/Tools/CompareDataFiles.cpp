/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 13 Apr 2018 13:08:50 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <algorithm>
#include <sstream>
#include <string>
#include <vector>
// IWYU pragma: no_include <iosfwd>

#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/CompareDataFiles.hpp"


namespace MoReFEM::TestNS
{


    template<>
    std::vector<double> CompareDataFiles<MeshNS::Format::Ensight>::ExtractDoubleValues(const FilesystemNS::File& file)
    {
        std::ifstream in{ file.Read(__FILE__, __LINE__) };

        std::string line;
        getline(in, line); // skip first line

        std::string str_value;

        std::vector<double> ret;

        while (getline(in, line))
        {
            const auto max = std::min(line.size(), 72ul);

            for (auto index = 0ul; index < max; index += 12ul)
            {
                str_value.assign(line, index, 12ul);
                ret.push_back(std::stod(str_value));
            }
        }

        return ret;
    }


    template<>
    std::vector<double> CompareDataFiles<MeshNS::Format::Vizir>::ExtractDoubleValues(const FilesystemNS::File& file)
    {
        std::ifstream in{ file.Read(__FILE__, __LINE__) };

        std::string line;
        for (int i = 0; i < 9; ++i)
            getline(in, line); // skip first 9 line.

        std::string str_value;

        std::vector<double> ret;

        while (getline(in, line))
        {
            std::istringstream iconv(line);
            double value;
            while (iconv >> value)
                ret.push_back(value);
        }

        return ret;
    }


} // namespace MoReFEM::TestNS
