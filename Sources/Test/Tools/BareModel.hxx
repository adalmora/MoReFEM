//! \file
//
//
//  BareModel.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 11/01/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HXX_

// IWYU pragma: private, include "Test/Tools/BareModel.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::TestNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::BareModel(
        const MoReFEMDataTypeT& morefem_data,
        create_domain_list_for_coords a_create_domain_list_for_coords)
    : parent(morefem_data, a_create_domain_list_for_coords)
    {
        // In tests we may need easily to check the temporary data that are usually flushed when a Model is
        // fully initialized. The following method is however not recommended in a normal Model...
        parent::SetClearGodOfDofTemporaryDataToFalse();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::
        SupplInitialize()
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void
    BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::SupplFinalize()
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    inline const std::string&
    BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::ClassName()
    {
        static std::string name("BareModel");
        return name;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::Forward()
        requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>)
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::
        SupplFinalizeStep()
        requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>)
    { }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    bool BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::
        SupplHasFinishedConditions() const
        requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>)
    {
        return true;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataTypeT,
        class TimeDependencyT,
        DoConsiderProcessorWiseLocal2Global DoConsiderProcessorWiseLocal2GlobalT
    >
    // clang-format on
    void BareModel<MoReFEMDataTypeT, TimeDependencyT, DoConsiderProcessorWiseLocal2GlobalT>::
        SupplInitializeStep()
        requires(!std::is_same_v<TimeDependencyT, ::MoReFEM::TimeManagerNS::Policy::None>)
    { }


} // namespace MoReFEM::TestNS


#endif // MOREFEM_x_TEST_x_TOOLS_x_BARE_MODEL_HXX_
