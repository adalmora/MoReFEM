/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 13 Mar 2018 16:10:26 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HPP_
#define MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HPP_

#include <memory>

#include "Utilities/Filesystem/File.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS
{


    /*!
     * \brief Create a \a MoReFEMData object from the Lua file read on the CLI for a test.
     *
     * \return The created \a MoReFEMData object.
     */
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    MoReFEMData<InputDataT, ModelSettingsT, program_type::test> InitMoReFEMDataFromCLI();


} // namespace MoReFEM::TestNS


#include "Test/Tools/InitMoReFEMDataFromCLI.hxx" // IWYU pragma: export


#endif // MOREFEM_x_TEST_x_TOOLS_x_INIT_MO_RE_F_E_M_DATA_FROM_C_L_I_HPP_
