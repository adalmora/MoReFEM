/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <sstream>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/CheckIdenticalFiles.hpp"


namespace MoReFEM::TestNS
{


    void CheckIdenticalFiles(const FilesystemNS::Directory& ref_dir,
                             const FilesystemNS::Directory& obtained_dir,
                             std::string&& filename,
                             const char* invoking_file,
                             int invoking_line)
    {
        std::ostringstream oconv;

        if (!ref_dir.DoExist())
        {
            oconv << "Reference folder " << ref_dir << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        if (!obtained_dir.DoExist())
        {
            oconv << "Result folder " << obtained_dir << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        auto ref_input_data = ::MoReFEM::FilesystemNS::File{ ref_dir.AddFile(filename) };

        if (!ref_input_data.DoExist())
        {
            oconv << "Reference file " << ref_input_data << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        auto obtained_input_data = ::MoReFEM::FilesystemNS::File{ obtained_dir.AddFile(filename) };

        if (!obtained_input_data.DoExist())
        {
            oconv << "Result file " << obtained_input_data << " does not exist.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        if (!FilesystemNS::AreEquals(ref_input_data, obtained_input_data, invoking_file, invoking_line))
        {
            oconv << "Reference file " << ref_input_data << " and result file " << obtained_input_data
                  << " are not identical.";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


} // namespace MoReFEM::TestNS
