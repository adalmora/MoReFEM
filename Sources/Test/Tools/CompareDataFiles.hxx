/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 Apr 2018 18:54:27 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_TOOLS_x_COMPARE_DATA_FILES_HXX_
#define MOREFEM_x_TEST_x_TOOLS_x_COMPARE_DATA_FILES_HXX_

// IWYU pragma: private, include "Test/Tools/CompareDataFiles.hpp"


#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::MeshNS { enum class Format; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS
{


    template<MeshNS::Format FormatT>
    CompareDataFiles<FormatT>::CompareDataFiles(const FilesystemNS::Directory& a_ref_dir,
                                                const FilesystemNS::Directory& a_obtained_dir,
                                                std::string&& filename,
                                                const char* invoking_file,
                                                int invoking_line,
                                                double epsilon)
    : parent(a_ref_dir, a_obtained_dir, std::move(filename), invoking_file, invoking_line, epsilon)
    {
        decltype(auto) reference_file = parent::GetReferenceFile();
        decltype(auto) obtained_file = parent::GetObtainedFile();

        const auto ref_values = ExtractDoubleValues(reference_file);
        const auto obtained_values = ExtractDoubleValues(obtained_file);

        if (ref_values.size() != obtained_values.size())
        {
            std::ostringstream oconv;
            oconv << "Number of values read in reference (" << ref_values.size() << " for file " << reference_file
                  << ") and output (" << obtained_values.size() << " for file " << obtained_file
                  << ") is not the same!";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }

        // An exception is thrown in this function if they aren't very similar.
        CheckAreEquals(ref_values, obtained_values, invoking_file, invoking_line);
    }

} // namespace MoReFEM::TestNS

#endif // MOREFEM_x_TEST_x_TOOLS_x_COMPARE_DATA_FILES_HXX_
