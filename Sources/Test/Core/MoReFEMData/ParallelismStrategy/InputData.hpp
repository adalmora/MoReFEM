/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_PARALLELISM_STRATEGY_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_PARALLELISM_STRATEGY_x_INPUT_DATA_HPP_

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"


namespace MoReFEM::TestNS::WithParallelismNS
{

    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Parallelism
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_morefem_data_type
     using morefem_data_type = MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test>;


} // namespace MoReFEM::TestNS::WithParallelismNS


#endif // MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_PARALLELISM_STRATEGY_x_INPUT_DATA_HPP_
