/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Jul 2018 11:13:16 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#define BOOST_TEST_MODULE core_parallelism_strategy
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Core/MoReFEMData/ParallelismStrategy/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace // anonymous
{

    using fixture = MoReFEM::TestNS::FixtureNS::TestEnvironment;

    using morefem_data_type = MoReFEM::TestNS::WithParallelismNS::morefem_data_type;

    using input_data_type = morefem_data_type::input_data_type;


} // namespace


BOOST_FIXTURE_TEST_CASE(parallel, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_parallel.lua" } };

   MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::parallel);
}

BOOST_FIXTURE_TEST_CASE(parallel_no_write, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_parallel_no_write.lua" } };

   MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data)
                == Advanced::parallelism_strategy::parallel_no_write);
}

BOOST_FIXTURE_TEST_CASE(run_from_preprocessed, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_run_from_preprocessed.lua" } };

   MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data)
                == Advanced::parallelism_strategy::run_from_preprocessed);
}


BOOST_FIXTURE_TEST_CASE(precompute, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_precompute.lua" } };

   MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::precompute);
}


BOOST_FIXTURE_TEST_CASE(no_parallel_block, fixture)
{
    FilesystemNS::File lua_file{ std::filesystem::path{
        "${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/ParallelismStrategy/demo_no_parallel_field.lua" } };

    using empty_data_tuple = std::tuple<>;

    using empty_input_data_type = InputData<empty_data_tuple>;

    MoReFEMData<empty_input_data_type, TestNS::EmptyModelSettings, program_type::test> morefem_data{ std::move(lua_file) };

    BOOST_CHECK(Advanced::ExtractParallelismStrategy(morefem_data) == Advanced::parallelism_strategy::none);
}


PRAGMA_DIAGNOSTIC(pop)
