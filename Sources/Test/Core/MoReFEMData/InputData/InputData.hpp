/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_

#include "Core/InputData/Instances/Reaction/ReactionCoefficient.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::InputDataSolidNS
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::ReactionNS::ReactionCoefficient
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple = std::tuple<>;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;


} // namespace MoReFEM::TestNS::InputDataSolidNS


#endif // MOREFEM_x_TEST_x_CORE_x_MO_RE_F_E_M_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_
