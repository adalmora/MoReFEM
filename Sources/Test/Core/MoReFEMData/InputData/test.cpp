/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 25 Jul 2018 18:29:56 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#define BOOST_TEST_MODULE core_input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Test/Core/MoReFEMData/InputData/InputData.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::InputDataSolidNS::input_data_type, TestNS::InputDataSolidNS::ModelSettings, program_type::test>;
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(check_value, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    const auto coeff =
        ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::ReactionNS::ReactionCoefficient>::Value(input_data);

    BOOST_CHECK(NumericNS::AreEqual(coeff, 4.));
}


PRAGMA_DIAGNOSTIC(pop)
