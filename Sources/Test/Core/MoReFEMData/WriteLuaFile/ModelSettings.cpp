/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Test/Core/MoReFEMData/WriteLuaFile/InputData.hpp"


namespace MoReFEM::TestNS::WriteLuaFileNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::Mesh<5>>("Mesh indexed 5");
        SetDescription<InputDataNS::ScalarTransientSource<1>>("Scalar transient source indexed 1");
        SetDescription<InputDataNS::ScalarTransientSource<72>>("Scalar transient source indexed 72");
        SetDescription<InputDataNS::VectorialTransientSource<4>>("Vectorial transient source indexed 4");
    }

  

} // namespace MoReFEM::TestNS::WriteLuaFileNS
