/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "Test/Core/MoReFEMData/InitialCondition/InputData.hpp"


namespace MoReFEM::TestNS::InputDataSolidNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::InitialCondition<1>>("Initial condition numbered 1");
        SetDescription<InputDataNS::InitialCondition<4>>("Initial condition numbered 4");
    }


} // namespace MoReFEM::TestNS::InputDataSolidNS
