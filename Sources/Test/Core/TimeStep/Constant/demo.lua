-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

transient = {


	-- Time at the beginning of the code (in seconds).
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	init_time = 0.057,


	-- Time step between two iterations, in seconds.
	-- Expected format: VALUE
	-- Constraint: v > 0.
	timeStep = 0.1,


	-- Maximum time, if set to zero run a static case.
	-- Expected format: VALUE
	-- Constraint: v >= 0.
	timeMax = 2.

} -- transient

Restart = {


	-- Time iteration from which we should restart the model. Put 0 if no restart intended. This value makes 
	-- only sense in 'RunFromPreprocessed' mode. 
	-- Expected format: VALUE
	time_iteration = 0,


	-- Result directory of the previous run. Leave empty if no restart intended.
	-- Expected format: "VALUE"
	data_directory = '',
    
    -- Whether restart data are written or not. Such data are written only on root processor and only in binary
    -- format.
    -- Expected format: 'true' or 'false' (without the quote)
    do_write_restart_data = false

} -- Restart

Result = {


	-- Directory in which all the results will be written. This path may use the environment variable 
	-- MOREFEM_RESULT_DIR, which is either provided in user's environment or automatically set to 
	-- '/Volumes/Data/${USER}/MoReFEM/Results' in MoReFEM initialization step. You may also use 
	-- ${MOREFEM_START_TIME} in the value which will be replaced by a time under format 
	-- YYYY_MM_DD_HH_MM_SS.Please do not read the value directly from this Lua file: whenever you need the path 
	-- to the result directory, use instead MoReFEMData::GetResultDirectory(). 
	-- Expected format: "VALUE"
	output_directory = '${MOREFEM_TEST_OUTPUT_DIR}/Core/TimeStep/Constant',


	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	-- Constraint: v > 0
	display_value = 1,


	-- Defines the solutions output format. Set to false for ascii or true for binary.
	-- Expected format: 'true' or 'false' (without the quote)
	binary_output = false

} -- Result

