/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#define BOOST_TEST_MODULE constant_time_step
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/ConstantTimeStep.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "Test/Core/TimeStep/Constant/InputData.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/Model.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::ConstantTimeStepNS::input_data_type, TestNS::EmptyModelSettings, program_type::test>;

    };


    // clang-format off
    //! \copydoc doxygen_hide_input_data_tuple
    using restart_input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using restart_input_data_type = InputData<restart_input_data_tuple>;

    //! \copydoc doxygen_hide_morefem_data_type
    using restart_morefem_data_type = MoReFEMData<restart_input_data_type, TestNS::EmptyModelSettings, program_type::test>;


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on

    constexpr auto epsilon = 1.e-9;

    constexpr auto initial_time = 0.057;

    constexpr auto time_step = 0.1;


} // namespace

namespace MoReFEM::TestNS::TimeManagerNS
{

    //! An helper class able through friendship to access internal content of \a TimeManagerInstance.
    template<class EvolutionPolicyT>
    class Viewer
    {
      public:
        Viewer(TimeManagerInstance<EvolutionPolicyT>& time_manager);

        std::size_t NtimeModified() const noexcept;

        std::size_t GetTimeStepIndex() noexcept;

      private:
        TimeManagerInstance<EvolutionPolicyT>& time_manager_;
    };

} // namespace MoReFEM::TestNS::TimeManagerNS


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(check_api, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> time_manager(morefem_data);

    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> viewer(time_manager);

    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::static_);

    BOOST_CHECK_EQUAL(time_manager.IsTimeStepConstant(), true);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 0ul);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetInverseTimeStep(), 10., epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 1ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + time_step, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 1ul);

    // As soon as time is modified, it should be set to dynamic and stay there.
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

    for (auto time_index{ 0ul }; time_index < 5ul; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 6ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 6. * time_step, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 6ul);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 5ul);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 7ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 5. * time_step, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetMaximumTime(), 2., epsilon);

    for (auto time_index{ 0ul }; time_index < 14ul; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 19ul);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 21ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19 * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 20ul);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 22ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 20 * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), true);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 23ul);
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 19ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19. * time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.ResetTimeManagerAtInitialTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 24ul);
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 0ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
}

BOOST_FIXTURE_TEST_CASE(time_iteration_file, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> time_manager(morefem_data);

    decltype(auto) file = time_manager.GetTimeIterationFile();

    BOOST_REQUIRE(file.DoExist());

    std::ifstream in{ file.Read(__FILE__, __LINE__) };

    std::string line;
    getline(in, line);

    BOOST_CHECK_EQUAL(line, "# Time iteration; time; numbering subset id; filename");

    // There is no additional content at the moment: it is up to `VariationalFormulation` class
    // to fill the data in this file.
}


BOOST_FIXTURE_TEST_CASE(throw_modify_time_step, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> time_manager(morefem_data);

    BOOST_CHECK_THROW(time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing),
                      ExceptionNS::TimeManagerNS::InvalidMethodForPolicy);

    BOOST_CHECK_THROW(time_manager.SetTimeStep(1.), ExceptionNS::TimeManagerNS::InvalidMethodForPolicy);
}


BOOST_FIXTURE_TEST_CASE(valid_restart, fixture_type)
{
    TestNS::ClearSingletons::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Constant/demo_valid_restart.lua") };

    restart_morefem_data_type morefem_data{ std::move(lua_file) };

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> viewer(time_manager);

    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    decltype(auto) restart_dir_path =
        ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::DataDirectory>::Path(morefem_data.GetInputData());

    auto restart_dir = FilesystemNS::Directory(GetMpi(), restart_dir_path, FilesystemNS::behaviour::read);

    decltype(auto) time_iteration_file = restart_dir.AddFile("time_iteration.hhdata");

    BOOST_REQUIRE(time_iteration_file.DoExist());

    time_manager.SetRestartIfRelevant(morefem_data.GetResultDirectory());
    BOOST_CHECK_EQUAL(viewer.GetTimeStepIndex(), 2ul);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 2ul); // value read in the Lua file
    BOOST_CHECK_CLOSE(time_manager.GetTime(), 0.257, epsilon);
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);
}


BOOST_FIXTURE_TEST_CASE(invalid_restart, fixture_type)
{
    TestNS::ClearSingletons::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Core/TimeStep/Constant/demo_invalid_restart.lua") };

    restart_morefem_data_type morefem_data{ std::move(lua_file) };

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep> viewer(time_manager);

    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    decltype(auto) restart_dir_path =
        ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::DataDirectory>::Path(morefem_data.GetInputData());

    auto restart_dir = FilesystemNS::Directory(GetMpi(), restart_dir_path, FilesystemNS::behaviour::read);

    decltype(auto) time_iteration_file = restart_dir.AddFile("time_iteration.hhdata");

    BOOST_REQUIRE(time_iteration_file.DoExist());

    BOOST_CHECK_THROW(time_manager.SetRestartIfRelevant(morefem_data.GetResultDirectory()),
                      ExceptionNS::TimeManagerNS::ImproperRestartTimeForConstantTimeStep);
}


PRAGMA_DIAGNOSTIC(pop)


namespace MoReFEM::TestNS::TimeManagerNS
{


    template<class EvolutionPolicyT>
    Viewer<EvolutionPolicyT>::Viewer(TimeManagerInstance<EvolutionPolicyT>& time_manager) : time_manager_(time_manager)
    { }


    template<class EvolutionPolicyT>
    std::size_t Viewer<EvolutionPolicyT>::NtimeModified() const noexcept
    {
        return time_manager_.NtimeModified();
    }


    template<class EvolutionPolicyT>
    std::size_t Viewer<EvolutionPolicyT>::GetTimeStepIndex() noexcept
    {
        return time_manager_.TimeStepIndex();
    }

} // namespace MoReFEM::TestNS::TimeManagerNS
