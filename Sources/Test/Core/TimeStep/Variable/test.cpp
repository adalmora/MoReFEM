/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 16 Apr 2018 17:32:47 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#define BOOST_TEST_MODULE variable_time_step
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/VariableTimeStep.hpp"
#include "Core/TimeManager/TimeManagerInstance.hpp"

#include "Test/Core/TimeStep/Variable/InputData.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct MockModel
    {

        using morefem_data_type = MoReFEMData<TestNS::VariableTimeStepNS::input_data_type, TestNS::EmptyModelSettings, program_type::test>;

    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        MockModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on

    constexpr auto epsilon = 1.e-9;

    constexpr auto initial_time = 0.057;

    constexpr auto initial_time_step = 0.1;

    constexpr auto minimum_time_step = 0.007;

} // namespace

namespace MoReFEM::TestNS::TimeManagerNS
{

    //! An helper class able through friendship to access internal content of \a TimeManagerInstance.
    template<class EvolutionPolicyT>
    class Viewer
    {
      public:
        Viewer(const TimeManagerInstance<EvolutionPolicyT>& time_manager);

        std::size_t NtimeModified() const noexcept;

        double GetMaximumTimeStep() const noexcept;

        double GetMinimumTimeStep() const noexcept;

      private:
        const TimeManagerInstance<EvolutionPolicyT>& time_manager_;
    };

} // namespace MoReFEM::TestNS::TimeManagerNS


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(constant_like_api, fixture_type)
{
    // Here we perform almost the same checks as for the constant time step case.

    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);

    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> viewer(time_manager);

    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::static_);

    BOOST_CHECK_EQUAL(time_manager.IsTimeStepConstant(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetInverseTimeStep(), 10., epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 1ul);

    // As soon as time is modified, it should be set to dynamic and stay there.
    BOOST_CHECK(time_manager.GetStaticOrDynamic() == StaticOrDynamic::dynamic_);

    for (auto time_index{ 0ul }; time_index < 5ul; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 6. * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 6ul);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 7ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 5. * initial_time_step, epsilon);

    BOOST_CHECK_CLOSE(time_manager.GetMaximumTime(), 2., epsilon);

    for (auto time_index{ 0ul }; time_index < 14ul; ++time_index)
        time_manager.IncrementTime();

    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 21ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19 * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 22ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 20 * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), true);

    time_manager.DecrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 23ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 19. * initial_time_step, epsilon);
    BOOST_CHECK_EQUAL(time_manager.HasFinished(), false);

    time_manager.ResetTimeManagerAtInitialTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 24ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
}

BOOST_FIXTURE_TEST_CASE(time_iteration_file, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);

    decltype(auto) file = time_manager.GetTimeIterationFile();

    BOOST_REQUIRE(file.DoExist());

    std::ifstream in{ file.Read(__FILE__, __LINE__) };

    std::string line;
    getline(in, line);

    BOOST_CHECK_EQUAL(line, "# Time iteration; time; numbering subset id; filename");

    // There is no additional content at the moment: it is up to `VariationalFormulation` class
    // to fill the data in this file.
}


BOOST_FIXTURE_TEST_CASE(set_time_step, fixture_type)
{
    // Here we perform almost the same checks as for the constant time step case.

    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    time_manager.SetTimeStep(10.21);
    time_manager.IncrementTime();

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 10.21, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 1ul);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_increasing_when_already_max, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMaximumTimeStep(), 0.1, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_decreasing, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMaximumTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMinimumTimeStep(), minimum_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .5 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .25 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .125 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), minimum_time_step, epsilon);

    BOOST_CHECK_THROW(time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing),
                      ExceptionNS::TimeManagerNS::TimeStepAdaptationMinimumReached);
}


BOOST_FIXTURE_TEST_CASE(adapt_time_step_heal_after_decreasing, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();

    decltype(auto) mpi = morefem_data.GetMpi();

    TimeManagerInstance<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> time_manager(morefem_data);
    TestNS::TimeManagerNS::Viewer<::MoReFEM::TimeManagerNS::Policy::VariableTimeStep> viewer(time_manager);

    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time, epsilon);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul);

    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMaximumTimeStep(), 0.1, epsilon);
    BOOST_CHECK_CLOSE(viewer.GetMinimumTimeStep(), minimum_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .5 * initial_time_step, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::decreasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), .25 * initial_time_step, epsilon);

    // So far exactly the same as previous test case... but now trying to recover.
    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 0ul); // adapting time step doesn't change the time iteration
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), 0.043, epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 1ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 0.043, epsilon);

    time_manager.AdaptTimeStep(mpi, policy_to_adapt_time_step::increasing);
    BOOST_CHECK_CLOSE(time_manager.GetTimeStep(), initial_time_step, epsilon);

    time_manager.IncrementTime();
    BOOST_CHECK_EQUAL(viewer.NtimeModified(), 2ul);
    BOOST_CHECK_CLOSE(time_manager.GetTime(), initial_time + 0.043 + initial_time_step, epsilon);
}

PRAGMA_DIAGNOSTIC(pop)


namespace MoReFEM::TestNS::TimeManagerNS
{


    template<class EvolutionPolicyT>
    Viewer<EvolutionPolicyT>::Viewer(const TimeManagerInstance<EvolutionPolicyT>& time_manager)
    : time_manager_(time_manager)
    { }


    template<class EvolutionPolicyT>
    std::size_t Viewer<EvolutionPolicyT>::NtimeModified() const noexcept
    {
        return time_manager_.NtimeModified();
    }


    template<class EvolutionPolicyT>
    double Viewer<EvolutionPolicyT>::GetMaximumTimeStep() const noexcept
    {
        return time_manager_.GetMaximumTimeStep();
    }


    template<class EvolutionPolicyT>
    double Viewer<EvolutionPolicyT>::GetMinimumTimeStep() const noexcept
    {
        return time_manager_.GetMinimumTimeStep();
    }


} // namespace MoReFEM::TestNS::TimeManagerNS
