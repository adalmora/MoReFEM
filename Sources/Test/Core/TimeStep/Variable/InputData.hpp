/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_CORE_x_TIME_STEP_x_VARIABLE_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_CORE_x_TIME_STEP_x_VARIABLE_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"

namespace MoReFEM::TestNS::VariableTimeStepNS
{


    // clang-format off
    //! \copydoc doxygen_hide_input_data_tuple
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::TimeManager::MinimumTimeStep,
        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_morefem_data_type
     using morefem_data_type = MoReFEMData<input_data_type, TestNS::EmptyModelSettings, program_type::test>;


} // namespace MoReFEM::TestNS::VariableTimeStepNS


#endif // MOREFEM_x_TEST_x_CORE_x_TIME_STEP_x_VARIABLE_x_INPUT_DATA_HPP_
