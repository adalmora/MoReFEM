/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 6 Apr 2018 18:06:38 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <chrono>
#include <iostream>
#include <thread>

#define BOOST_TEST_MODULE numeric_utitilies
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Numeric/Numeric.hpp"

using namespace MoReFEM::NumericNS;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(power)
{
    BOOST_CHECK_EQUAL(Square(2), 4);
    BOOST_CHECK_EQUAL(Cube(-3), -27);
    BOOST_CHECK_EQUAL(Cube(5.), 125.);
    BOOST_CHECK_EQUAL(Square(-4.), 16.);

    BOOST_CHECK_EQUAL(PowerFour(2), 16);
    BOOST_CHECK_EQUAL(PowerFour(-4.), 256.);
}


BOOST_AUTO_TEST_CASE(abs_plus)
{
    BOOST_CHECK_EQUAL(AbsPlus(5.23), 5.23);
    BOOST_CHECK_EQUAL(AbsPlus(-5.23), 0.);
    BOOST_CHECK_EQUAL(AbsPlus(134), 134);
    BOOST_CHECK_EQUAL(AbsPlus(0.), 0.);
}


BOOST_AUTO_TEST_CASE(sign)
{
    BOOST_CHECK_EQUAL(Sign(5.23), 1.);
    BOOST_CHECK_EQUAL(Sign(-5.23), -1.);
    BOOST_CHECK_EQUAL(Sign(134), 1);
    BOOST_CHECK_EQUAL(Sign(-134), -1);
    BOOST_CHECK_EQUAL(Sign(0), 0);
}


BOOST_AUTO_TEST_CASE(true_sign)
{
    BOOST_CHECK_EQUAL(TrueSign(5.23), 1.);
    BOOST_CHECK_EQUAL(TrueSign(-5.23), -1.);
    BOOST_CHECK_EQUAL(TrueSign(134), 1);
    BOOST_CHECK_EQUAL(TrueSign(-134), -1);
    BOOST_CHECK_EQUAL(TrueSign(0), 1);
}

BOOST_AUTO_TEST_CASE(heaviside)
{
    BOOST_CHECK_EQUAL(Heaviside(5.23), 1.);
    BOOST_CHECK_EQUAL(Heaviside(-5.23), 0.);
    BOOST_CHECK_EQUAL(Heaviside(134.f), 1.f);
    BOOST_CHECK_EQUAL(Heaviside(-134.f), 0.f);
    BOOST_CHECK_EQUAL(Heaviside(0.), 0.5);
}


BOOST_AUTO_TEST_CASE(is_zero)
{
    BOOST_CHECK(!IsZero(5.23));
    BOOST_CHECK(!IsZero(-5.23));
    BOOST_CHECK(!IsZero(134.f));
    BOOST_CHECK(!IsZero(-134.f));
    BOOST_CHECK(IsZero(0.));

    BOOST_CHECK(IsZero(0));
    BOOST_CHECK(!IsZero(5));
    BOOST_CHECK(!IsZero(-3));

    BOOST_CHECK(IsZero(0.2, 1.));
    BOOST_CHECK(!IsZero(0.2, 0.1));

    BOOST_CHECK(!IsZero(1.e-8, 1.e-12));
    BOOST_CHECK(IsZero(1.e-8, 1.e-6));

    {
        using MoReFEM::LocalVector;

        LocalVector non_zero{ 5., 1., -2., 0. };
        BOOST_CHECK(!IsZero(non_zero));

        LocalVector zero{ 0., 0., 0., 0., 0., 0. };
        BOOST_CHECK(IsZero(zero));

        LocalVector empty;
        BOOST_CHECK(IsZero(empty));
    }

    {
        using MoReFEM::LocalMatrix;

        LocalMatrix non_zero{ { 5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };
        BOOST_CHECK(!IsZero(non_zero));

        LocalMatrix zero{ { 0., 0., 0., 0. }, { 0., 0., 0., 0. }, { 0., 0., 0., 0. } };
        BOOST_CHECK(IsZero(zero));

        LocalMatrix empty;
        BOOST_CHECK(IsZero(empty));
    }
}


BOOST_AUTO_TEST_CASE(are_equal)
{
    BOOST_CHECK(AreEqual(1.e-6, 1.e-6));
    BOOST_CHECK(!AreEqual(1.1e-6, 1.e-6));
    BOOST_CHECK(AreEqual(1.1e-6, 1.e-6, 1.e-5));

    {
        using MoReFEM::LocalVector;

        LocalVector vector1{ 5., 1., -2., 0. };

        LocalVector vector1_bis{ vector1 };

        LocalVector vector2{ -5., 1., -2., 0. };

        LocalVector vector3{ 2. };

        BOOST_CHECK(AreEqual(vector1, vector1));
        BOOST_CHECK(AreEqual(vector1, vector1_bis));
        BOOST_CHECK(!AreEqual(vector1, vector2));

        BOOST_CHECK_THROW(AreEqual(vector1, vector3), ::MoReFEM::ExceptionNS::NumericNS::InconsistentVectors);
    }

    {
        using MoReFEM::LocalMatrix;

        LocalMatrix matrix1{ { 5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };

        LocalMatrix matrix1_bis{ matrix1 };

        LocalMatrix matrix2{ { -5., 1., -2., 0. }, { 1., 2., 3., 4. }, { 1., 2., 3., 0. } };

        LocalMatrix matrix3{ { 2. } };

        BOOST_CHECK(AreEqual(matrix1, matrix1));
        BOOST_CHECK(AreEqual(matrix1, matrix1_bis));
        BOOST_CHECK(!AreEqual(matrix1, matrix2));

        BOOST_CHECK_THROW(AreEqual(matrix1, matrix3), ::MoReFEM::ExceptionNS::NumericNS::InconsistentMatrices);
    }
}


BOOST_AUTO_TEST_CASE(is_number)
{
    // This test was previously fulfilled with a homemade function but I found out STL
    // already provides the functionality. So the former `IsNumber` has been replaced
    // here by `std::isfinite`.
    BOOST_CHECK(std::isfinite(3.14));
    BOOST_CHECK(std::isfinite(-7.));

    BOOST_CHECK(!std::isfinite(std::numeric_limits<double>::infinity()));

#include "Utilities/Warnings/Internal/IgnoreWarning/div-by-zero.hpp" // IWYU pragma: keep
    double a = 1. / 0;
    BOOST_CHECK(!std::isfinite(a));
}


BOOST_AUTO_TEST_CASE(pow_wrapper)
{
    BOOST_CHECK_EQUAL(Pow(-3., 2., __FILE__, __LINE__), 9.);
    BOOST_CHECK_THROW(Pow(-11., -1. / 3., __FILE__, __LINE__), ::MoReFEM::Exception);
    BOOST_CHECK_THROW(Pow(-0., -3., __FILE__, __LINE__), ::MoReFEM::Exception);
}


PRAGMA_DIAGNOSTIC(pop)
