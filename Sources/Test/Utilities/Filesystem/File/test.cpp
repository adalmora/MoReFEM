/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Mar 2018 18:46:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <array>
#include <fstream>
#include <sstream>
#include <vector>

#define BOOST_TEST_MODULE utilities_file

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    const std::string test_subdirectory = "File";

    struct fixture : public TestNS::FixtureNS::TestEnvironment
    {

        explicit fixture();

        const FilesystemNS::Directory& GetPlaygroundDirectory() const noexcept;

      private:
        std::unique_ptr<FilesystemNS::Directory> playground_{ nullptr };
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(basic_operations, fixture)
{
    FilesystemNS::File file = GetPlaygroundDirectory().AddFile("created_file.txt");

    std::filesystem::path file_without_facility(GetPlaygroundDirectory().GetPath());
    file_without_facility /= "created_file.txt";

    BOOST_CHECK_EQUAL(static_cast<std::string>(file), file_without_facility.native());

    std::ostringstream oconv;
    oconv << file;

    BOOST_CHECK_EQUAL(oconv.str(), file_without_facility.native());


    BOOST_CHECK(file.Extension() == ".txt");

    BOOST_CHECK(file.DoExist() == false); // the directory was just recreated so can't exist.

    {
        std::ofstream out = file.NewContent(__FILE__, __LINE__);
        out << "Hello world!" << std::endl;
    }

    BOOST_CHECK(file.DoExist() == true);
    file.Remove(__FILE__, __LINE__);
    BOOST_CHECK(file.DoExist() == false);
}


BOOST_FIXTURE_TEST_CASE(equivalent, fixture)
{
    FilesystemNS::File file1 = GetPlaygroundDirectory().AddFile("file1.txt");

    FilesystemNS::File file1bis = GetPlaygroundDirectory().AddFile("file1.txt"); // same name intentionally

    FilesystemNS::File file2 = GetPlaygroundDirectory().AddFile("file2.txt"); // same name intentionally

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (file1)(file1bis));

    auto not_same_file = [](const auto& lhs, const auto& rhs)
    {
        return !FilesystemNS::IsSameFile(lhs, rhs);
    };

    BOOST_CHECK_PREDICATE(not_same_file, (file1)(file2));

    {
        std::ofstream out = file1.NewContent(__FILE__, __LINE__);
        out << "Hello world!" << std::endl;
    }

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (file1)(file1bis)); // existence or not of the file doesn't matter
}


BOOST_FIXTURE_TEST_CASE(copy, fixture)
{
    FilesystemNS::File source = GetPlaygroundDirectory().AddFile("original.txt");

    {
        std::ofstream out = source.NewContent(__FILE__, __LINE__);
        out << "Hello world!" << std::endl;
    }

    BOOST_CHECK_THROW(
        FilesystemNS::Copy(
            source, source, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no, __FILE__, __LINE__),
        std::exception);

    BOOST_CHECK_NO_THROW(FilesystemNS::Copy(
        source, source, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::yes, __FILE__, __LINE__));


    FilesystemNS::File target = GetPlaygroundDirectory().AddFile("target.txt");

    FilesystemNS::Copy(
        source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no, __FILE__, __LINE__);

    BOOST_CHECK_NO_THROW(FilesystemNS::Copy(
        source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no, __FILE__, __LINE__));

    // Throw as it was created by command above...
    BOOST_CHECK_THROW(
        FilesystemNS::Copy(
            source, target, FilesystemNS::fail_if_already_exist::yes, FilesystemNS::autocopy::no, __FILE__, __LINE__),
        std::exception);

    BOOST_CHECK_NO_THROW(FilesystemNS::Copy(
        source, target, FilesystemNS::fail_if_already_exist::no, FilesystemNS::autocopy::no, __FILE__, __LINE__));

    BOOST_CHECK(FilesystemNS::AreEquals(source, target, __FILE__, __LINE__));

    FilesystemNS::File other = GetPlaygroundDirectory().AddFile("other.txt");

    BOOST_CHECK_THROW(FilesystemNS::AreEquals(other, target, __FILE__, __LINE__),
                      Exception); // file doesn't exist yet

    {
        std::ofstream out = other.NewContent(__FILE__, __LINE__);
        out << "Hello world!" << std::endl;
    }

    BOOST_CHECK(FilesystemNS::AreEquals(other, target, __FILE__, __LINE__) == true);

    {
        std::ofstream out = other.NewContent(__FILE__, __LINE__);
        out << "Goodbye!" << std::endl;
    }

    BOOST_CHECK(FilesystemNS::AreEquals(other, target, __FILE__, __LINE__) == false);
}


BOOST_FIXTURE_TEST_CASE(read_and_append, fixture)
{
    FilesystemNS::File source = GetPlaygroundDirectory().AddFile("original.txt");

    std::string content{ "Hello world!" };

    {
        std::ofstream out = source.NewContent(__FILE__, __LINE__);
        out << content << std::endl;
    }

    {
        std::ifstream in = source.Read(__FILE__, __LINE__);
        std::string read;

        std::getline(in, read);
        BOOST_CHECK_EQUAL(content, read);
    }

    std::string new_content{ "Goodbye!" };

    {
        std::ofstream out = source.Append(__FILE__, __LINE__);
        out << new_content << std::endl;
    }

    {
        std::ifstream in = source.Read(__FILE__, __LINE__);
        std::string read;

        std::getline(in, read);
        BOOST_CHECK(in);
        BOOST_CHECK_EQUAL(content, read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(new_content, read);

        std::getline(in, read);
        BOOST_CHECK(read.empty());

        BOOST_CHECK(in.eof());
    }
}


BOOST_FIXTURE_TEST_CASE(binary, fixture)
{
    FilesystemNS::File ascii = GetPlaygroundDirectory().AddFile("ascii.txt");
    FilesystemNS::File binary = GetPlaygroundDirectory().AddFile("binary.txt");

    std::vector<unsigned int> prime{ 2, 3, 5, 7, 11 };

    {
        std::ofstream out = ascii.NewContent(__FILE__, __LINE__, binary_or_ascii::ascii);
        for (auto value : prime)
            out << value << "\t";

        out.close();
        BOOST_CHECK(ascii.DoExist() == true);
    }

    {
        std::ofstream out = binary.NewContent(__FILE__, __LINE__, binary_or_ascii::binary);
        for (auto value : prime)
            out.write(reinterpret_cast<const char*>(&value), sizeof(char));

        out.close();
        BOOST_CHECK(binary.DoExist() == true);
    }

    auto not_equal = [](const FilesystemNS::File& lhs, const FilesystemNS::File& rhs)
    {
        return !FilesystemNS::AreEquals(lhs, rhs, __FILE__, __LINE__);
    };

    BOOST_CHECK_PREDICATE(not_equal, (ascii)(binary));

    {
        std::vector<unsigned int> read_from_ascii;

        std::ifstream in{ ascii.Read(__FILE__, __LINE__, binary_or_ascii::ascii) };

        unsigned int value;

        while (in >> value)
            read_from_ascii.push_back(value);

        BOOST_CHECK(prime == read_from_ascii);
    }

    {
        std::ifstream in{ binary.Read(__FILE__, __LINE__, binary_or_ascii::binary) };

        std::vector<unsigned int> read_from_binary((std::istreambuf_iterator<char>(in)),
                                                   (std::istreambuf_iterator<char>()));

        BOOST_CHECK(prime == read_from_binary);
    }
}


BOOST_FIXTURE_TEST_CASE(concatenate, fixture)
{
    FilesystemNS::File file1 = GetPlaygroundDirectory().AddFile("file1.txt");
    FilesystemNS::File file2 = GetPlaygroundDirectory().AddFile("file2.txt");
    FilesystemNS::File file3 = GetPlaygroundDirectory().AddFile("file3.txt");

    std::array<std::string, 3ul> content{ { "Content 1", "Content 2", "Content 3" } };

    {
        std::ofstream out = file1.NewContent(__FILE__, __LINE__);
        out << content[0] << std::endl;
    }
    {
        std::ofstream out = file2.NewContent(__FILE__, __LINE__);
        out << content[1] << std::endl;
    }
    {
        std::ofstream out = file3.NewContent(__FILE__, __LINE__);
        out << content[2] << std::endl;
    }

    FilesystemNS::File amalgamated1 = GetPlaygroundDirectory().AddFile("amalgamated1.txt");

    FilesystemNS::Concatenate({ file1 }, amalgamated1, __FILE__, __LINE__);

    BOOST_CHECK(FilesystemNS::AreEquals(file1, amalgamated1, __FILE__, __LINE__));


    BOOST_CHECK_THROW(FilesystemNS::Concatenate({ file1 }, file1, __FILE__, __LINE__), Exception);

    BOOST_CHECK_THROW(FilesystemNS::Concatenate({ file1, file2, file3, file1 }, amalgamated1, __FILE__, __LINE__),
                      Exception); // throws as the target file already exists!

    FilesystemNS::File amalgamated2 = GetPlaygroundDirectory().AddFile("amalgamated2.txt");

    FilesystemNS::Concatenate({ file1, file2, file3, file1 }, amalgamated2, __FILE__, __LINE__);

    {
        std::ifstream in = amalgamated2.Read(__FILE__, __LINE__);
        std::string read;

        std::getline(in, read);
        BOOST_CHECK(in);
        BOOST_CHECK_EQUAL(content[0], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[1], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[2], read);
        std::getline(in, read);
        BOOST_CHECK_EQUAL(content[0], read);
        std::getline(in, read);
        BOOST_CHECK(read.empty());
        BOOST_CHECK(in.eof());
    }
}


BOOST_FIXTURE_TEST_CASE(created_from_ill_formed_string, fixture)
{
    std::filesystem::path filename("/path42/that/shouldn't//exist//foo.txt"); // double slashes intended

    FilesystemNS::File file(std::move(filename));

    BOOST_CHECK(file.DoExist() == false);

    BOOST_CHECK_EQUAL(file.Extension(), ".txt");
}


BOOST_FIXTURE_TEST_CASE(environment_substitution, fixture)
{
    std::ostringstream oconv;
    oconv << "${MOREFEM_TEST_OUTPUT_DIR}/" << test_subdirectory << "/Rank_" << GetMpi().GetRank<int>() << "/foo.txt";

    FilesystemNS::File from_env_substitution{ std::filesystem::path{ oconv.str() } };

    // Here the substitution occurred much earlier - see fixture connstructor
    FilesystemNS::File usual_way{ GetPlaygroundDirectory().AddFile("foo.txt") };

    BOOST_CHECK_PREDICATE(FilesystemNS::IsSameFile, (from_env_substitution)(usual_way));
}


PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    fixture::fixture()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

        decltype(auto) mpi = GetMpi();

        std::filesystem::path path(test_dir);
        path /= test_subdirectory;

        playground_ = std::make_unique<FilesystemNS::Directory>(mpi, path, FilesystemNS::behaviour::overwrite);

        static bool is_first_call{ true };

        if (is_first_call)
        {
            playground_->ActOnFilesystem(__FILE__, __LINE__);
            is_first_call = false;
        }
    }


    const FilesystemNS::Directory& fixture::GetPlaygroundDirectory() const noexcept
    {
        assert(!(!playground_));
        return *playground_;
    }


} // namespace
