-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.


Section1 = {
    
    LeafInSection1 = { 2.1, -3.2, 5. },
    
    SubsectionInSection1 = {
        
        LeafInSubSection1 = "${MOREFEM_ROOT}/Sources/Utilities"
        
    }    
    
}


Section2 = {
    
    FirstLeafInSection2 = { [3] = 7.3, [2] = -12 }
    
}


LeafInNoEnclosingSection = "Hello world!"


NumberingSubset1 =
{
    name = "ns_1",
    do_move_mesh = false
}


NumberingSubset4 =
{
    name = "ns_4",
    do_move_mesh = false
}


NumberingSubset9 =
{
    name = "ns_9",
    do_move_mesh = false
}