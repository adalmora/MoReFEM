/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp" // IWYU pragma: export

namespace MoReFEM::TestNS::ReadInputDataNS
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        Section1,
        LeafInNoEnclosingSection,
        Section2::Leaf1InSection2
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


} // namespace MoReFEM::TestNS::ReadInputDataNS


#endif // MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_x_INPUT_DATA_HPP_
