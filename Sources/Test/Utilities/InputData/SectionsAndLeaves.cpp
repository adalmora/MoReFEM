/*!
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr>
 // Copyright (c) Inria. All rights reserved.
 //
 */

#include "Utilities/String/EmptyString.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp"


namespace MoReFEM::TestNS::ReadInputDataNS
{


    const std::string& LeafInNoEnclosingSection::NameInFile()
    {
        static std::string ret("LeafInNoEnclosingSection");
        return ret;
    }


    const std::string& LeafInNoEnclosingSection::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section1::GetName()
    {
        static std::string ret("Section1");
        return ret;
    }


    const std::string& Section1::LeafInSection1::NameInFile()
    {
        static std::string ret("LeafInSection1");
        return ret;
    }


    const std::string& Section1::LeafInSection1::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section1::SubsectionInSection1::GetName()
    {
        static std::string ret("SubsectionInSection1");
        return ret;
    }


    const std::string& Section1::SubsectionInSection1::LeafInSubSection1::NameInFile()
    {
        static std::string ret("LeafInSubSection1");
        return ret;
    }


    const std::string& Section1::SubsectionInSection1::LeafInSubSection1::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::GetName()
    {
        static std::string ret("Section2");
        return ret;
    }


    const std::string& Section2::Leaf1InSection2::NameInFile()
    {
        static std::string ret("FirstLeafInSection2");
        return ret;
    }


    const std::string& Section2::Leaf1InSection2::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::Leaf2InSection2::NameInFile()
    {
        static std::string ret("SecondLeafInSection2");
        return ret;
    }


    const std::string& Section2::Leaf2InSection2::Description()
    {
        return Utilities::EmptyString();
    }


    const std::string& Section2::SubsectionInSection2::GetName()
    {
        static std::string ret("SubsectionInSection2");
        return ret;
    }


    const std::string& Section2::SubsectionInSection2::LeafInSubsectionInSection2::NameInFile()
    {
        static std::string ret("LeafInSubsectionInSection2");
        return ret;
    }


    const std::string& Section2::SubsectionInSection2::LeafInSubsectionInSection2::Description()
    {
        return Utilities::EmptyString();
    }


} // namespace MoReFEM::TestNS::ReadInputDataNS
