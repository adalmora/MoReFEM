/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_MODEL_SETTINGS_x_MODEL_SETTINGS_HPP_
#define MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_MODEL_SETTINGS_x_MODEL_SETTINGS_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Utilities/InputData/SectionsAndLeaves.hpp" // IWYU pragma: export


namespace MoReFEM::TestNS::ReadInputDataNS
{


    //! \brief Tuple with content for a \a ModelSettings instance.
    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple = std::tuple
    <
        Section1,
        LeafInNoEnclosingSection,
        Section2::Leaf1InSection2
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


} // namespace MoReFEM::TestNS::ReadInputDataNS


#endif // MOREFEM_x_TEST_x_UTILITIES_x_INPUT_DATA_x_MODEL_SETTINGS_x_MODEL_SETTINGS_HPP_
