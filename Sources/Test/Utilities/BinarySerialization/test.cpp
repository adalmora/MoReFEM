/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Mar 2018 18:46:03 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <array>
#include <vector>

#define BOOST_TEST_MODULE binary_serialization

#include "Utilities/AsciiOrBinary/BinarySerialization.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct fixture : public TestNS::FixtureNS::TestEnvironment
    {

        explicit fixture();

        const FilesystemNS::Directory& GetPlaygroundDirectory() const noexcept;

      private:
        std::unique_ptr<FilesystemNS::Directory> playground_{ nullptr };
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(not_existing, fixture)
{
    decltype(auto) output_dir = GetPlaygroundDirectory();
    const auto unexisting_file = output_dir.AddFile("not_existing_file.really_not");

    BOOST_CHECK(unexisting_file.DoExist() == false);

    BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(unexisting_file, __FILE__, __LINE__),
                      ExceptionNS::BinaryNS::NotExistingFile);
}


BOOST_FIXTURE_TEST_CASE(ascii_file, fixture)
{
    decltype(auto) output_dir = GetPlaygroundDirectory();
    const auto ascii_file = output_dir.AddFile("ascii.dat");

    std::ofstream out{ ascii_file.NewContent(__FILE__, __LINE__, binary_or_ascii::ascii) };

    std::vector<int> vec{ 3, -4, 5, 21, 0 };

    for (auto& item : vec)
        out << item;

    out.close();

    BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(ascii_file, __FILE__, __LINE__),
                      ExceptionNS::BinaryNS::LoadBinaryFileException);
}


BOOST_FIXTURE_TEST_CASE(vector_int, fixture)
{
    std::vector<int> vec{ 3, -4, 5, 21, 0 };

    const auto vector_size = vec.size();


    decltype(auto) output_dir = GetPlaygroundDirectory();

    const auto file = output_dir.AddFile("vector_int.bin");
    Advanced::SerializeVectorIntoBinaryFile(file, vec);

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<int>(file, __FILE__, __LINE__);
        BOOST_CHECK(vec == unserialized);
    }

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<int>(file, __FILE__, __LINE__, vector_size);
        BOOST_CHECK(vec == unserialized);
    }

    {
        BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(file, __FILE__, __LINE__, 2ul),
                          ExceptionNS::BinaryNS::NotMatchingSize);
    }

    {
        if constexpr (sizeof(double) != sizeof(int))
            BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<double>(file, __FILE__, __LINE__),
                              ExceptionNS::BinaryNS::LoadBinaryFileException);
    }

    {
        if constexpr (sizeof(long) != sizeof(int))
            BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<long>(file, __FILE__, __LINE__),
                              ExceptionNS::BinaryNS::LoadBinaryFileException);
    }
}


BOOST_FIXTURE_TEST_CASE(vector_double, fixture)
{
    std::vector<double> vec{ 3., -4.2, 5.3, 2.121, 1.e-7 };
    constexpr auto epsilon = 1.e-9;
    const auto vector_size = vec.size();

    decltype(auto) output_dir = GetPlaygroundDirectory();

    const auto file = output_dir.AddFile("vector_double.bin");
    Advanced::SerializeVectorIntoBinaryFile(file, vec);

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<double>(file, __FILE__, __LINE__);

        BOOST_CHECK_EQUAL(unserialized.size(), vector_size);
        BOOST_CHECK_EQUAL(vector_size, 5ul);


        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[0])(unserialized[0])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[1])(unserialized[1])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[2])(unserialized[2])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[3])(unserialized[3])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[4])(unserialized[4])(epsilon));
    }

    {
        const auto unserialized =
            Advanced::UnserializeVectorFromBinaryFile<double>(file, __FILE__, __LINE__, vector_size);
        BOOST_CHECK_EQUAL(unserialized.size(), vector_size);
        BOOST_CHECK_EQUAL(vector_size, 5ul);


        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[0])(unserialized[0])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[1])(unserialized[1])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[2])(unserialized[2])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[3])(unserialized[3])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[4])(unserialized[4])(epsilon));
    }

    {
        BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<double>(file, __FILE__, __LINE__, 2ul),
                          ExceptionNS::BinaryNS::NotMatchingSize);
    }
}

PRAGMA_DIAGNOSTIC(pop)


namespace // anonymous
{


    fixture::fixture()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__);

        decltype(auto) mpi = GetMpi();

        std::filesystem::path path(test_dir);
        path /= "Utilities";
        path /= "BinarySerialization";

        playground_ = std::make_unique<FilesystemNS::Directory>(mpi, path, FilesystemNS::behaviour::overwrite);

        static bool is_first_call{ true };

        if (is_first_call)
        {
            playground_->ActOnFilesystem(__FILE__, __LINE__);
            is_first_call = false;
        }
    }


    const FilesystemNS::Directory& fixture::GetPlaygroundDirectory() const noexcept
    {
        assert(!(!playground_));
        return *playground_;
    }


} // namespace
