/*!
 // \file
 //
 //
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE restart_invalid_data_dir
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,
        InputDataNS::Restart,
        InputDataNS::Result
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    using morefem_data_type =MoReFEMData<input_data_type, MoReFEM::TestNS::EmptyModelSettings, program_type::test>;

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        TimeManagerNS::Policy::ConstantTimeStep,
        DoConsiderProcessorWiseLocal2Global::no
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(incorrect, fixture_type)
{
    decltype(auto) morefem_data = fixture_type::GetMoReFEMData();
    decltype(auto) input_data = morefem_data.GetInputData();

    // Create a file that is required by the model.
    {
        auto path = std::filesystem::path{
            ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::DataDirectory>::Path(input_data)
        };
        FilesystemNS::Directory create(morefem_data.GetMpi(), path, FilesystemNS::behaviour::create_if_necessary);
        create.ActOnFilesystem(__FILE__, __LINE__);
        auto time_file = create.AddFile("time_iteration.hhdata");

        auto stream = time_file.NewContent(__FILE__, __LINE__);
        stream << "# Time iteration; time; numbering subset id; filename" << std::endl;
        stream << "0;0;1;/path_to/solution_time_00000.hhdata" << std::endl;
        stream << "1;0.1;1;/path_to/solution_time_00001.hhdata" << std::endl;
    }

    BOOST_CHECK_THROW(auto model = model_type(morefem_data),
                      ExceptionNS::TimeManagerNS::RestartDirShouldntBeInResultDir);
}


PRAGMA_DIAGNOSTIC(pop)
