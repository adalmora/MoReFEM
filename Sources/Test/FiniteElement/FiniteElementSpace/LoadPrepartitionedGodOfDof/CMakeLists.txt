add_library(TestLoadPrepartitionedGodOfDof_lib ${LIBRARY_TYPE} "")

target_sources(TestLoadPrepartitionedGodOfDof_lib
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp        
        ${CMAKE_CURRENT_LIST_DIR}/Model.hpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.hxx
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
)

target_link_libraries(TestLoadPrepartitionedGodOfDof_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestLoadPrepartitionedGodOfDof_lib Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof) 

add_executable(TestLoadPrepartitionedGodOfDof)


target_sources(TestLoadPrepartitionedGodOfDof
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/demo_tri.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_tri_ensight.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_quad.lua
)

target_link_libraries(TestLoadPrepartitionedGodOfDof
                      TestLoadPrepartitionedGodOfDof_lib
                      )

morefem_organize_IDE(TestLoadPrepartitionedGodOfDof Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof) 


morefem_boost_test_parallel_mode(NAME LoadPrepartitionedGodOfDof_triangles
                                   EXE TestLoadPrepartitionedGodOfDof
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/demo_tri.lua)

morefem_boost_test_parallel_mode(NAME LoadPrepartitionedGodOfDof_triangles_ensight
                                   EXE TestLoadPrepartitionedGodOfDof
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/demo_tri_ensight.lua)

morefem_boost_test_parallel_mode(NAME LoadPrepartitionedGodOfDof_quadrangles
                                   EXE TestLoadPrepartitionedGodOfDof
                                   TIMEOUT 100
                                   LUA ${MOREFEM_ROOT}/Sources/Test/FiniteElement/FiniteElementSpace/LoadPrepartitionedGodOfDof/demo_quad.lua)
