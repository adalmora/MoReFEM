/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_INPUT_DATA_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { original = 1ul, reconstructed = 11ul };


    //! Default value for some input datum that are required by a MoReFEM model but are actually unused
    //! for current test.
    constexpr auto original = 1u;

    //! Index associated to stuff built from pre-partitioned data (which were themselves written from original
    //! ones).
    constexpr auto reconstructed = 11u;

    //! We take the convention of adding this quantity to get reconstructed index from original one.
    //! For instance the pendant of original separated FEltSpaceIndex (1) is 11 (we add 10).
    constexpr auto reconstructed_overhead = 10u;

    //! Enum class for unknowns.
    enum class UnknownIndex : std::size_t { scalar = 1, vectorial };


    //! Enum class for NumberingSubsets
    enum class NumberingSubsetIndex : std::size_t { scalar = 1, vectorial, mixed };


    //! Enum class for NumberingSubsets
    enum class FEltSpaceIndex : std::size_t { separated = 1, mixed, reconstructed_separated = 11, reconstructed_mixed };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::original)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mixed)>,

        InputDataNS::Domain<original>,
        InputDataNS::Domain<reconstructed>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::separated)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mixed)>,

        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::scalar)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::vectorial)>,

        InputDataNS::Result,
        InputDataNS::Parallelism
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::separated)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::mixed)>::IndexedSectionDescription,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::original)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::mixed)>::IndexedSectionDescription,
        InputDataNS::Domain<original>::IndexedSectionDescription,
        InputDataNS::Domain<reconstructed>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::scalar)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(UnknownIndex::vectorial)>::IndexedSectionDescription
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::LoadPrepartitionedGodOfDofNS


#endif // MOREFEM_x_TEST_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_LOAD_PREPARTITIONED_GOD_OF_DOF_x_INPUT_DATA_HPP_
