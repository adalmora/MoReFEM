/*!
// \file
//
*/

#include "Test/FiniteElement/BoundaryCondition/InvalidComp/InputData.hpp"


namespace MoReFEM::TestNS::BoundaryConditionNS::InvalidCompNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::sole)>>({ " sole)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full)>>({ " full)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::sole)>>(
            { " sole)" });
    }


} // namespace MoReFEM::TestNS::BoundaryConditionNS::InvalidCompNS
