/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE bc_invalid_comp
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "FiniteElement/BoundaryConditions/Exceptions/Exception.hpp"

#include "Test/FiniteElement/BoundaryCondition/InvalidComp/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    using model_type =
    TestNS::BareModel
    <
        MoReFEM::TestNS::BoundaryConditionNS::InvalidCompNS::morefem_data_type
    >;

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

} // namespace


BOOST_FIXTURE_TEST_CASE(check, fixture_type)
{
    BOOST_CHECK_THROW(GetModel(), MoReFEM::ExceptionNS::BoundaryConditionNS::Exception);
}


PRAGMA_DIAGNOSTIC(pop)
