/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 18 Nov 2018 22:29:38 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_FINITE_ELEMENT_x_BOUNDARY_CONDITION_x_TWO_MESHES_x_MODEL_SETTINGS_HPP_
#define MOREFEM_x_TEST_x_FINITE_ELEMENT_x_BOUNDARY_CONDITION_x_TWO_MESHES_x_MODEL_SETTINGS_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/DirichletBoundaryCondition/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/MoReFEMData/MoReFEMDataForTest.hpp"


namespace MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { fluid = 1, solid = 2 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        fluid_full = 1,
        fluid_mesh_one_label = 10,
        fluid_mesh_many_labels = 11,

        solid_full = 2,
        solid_mesh_one_label = 20,
        solid_mesh_many_labels = 21
    };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t {
        fluid = 10,

        solid = 20
    };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t {
        unknown = 1,
    };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        unknown_on_fluid = 10,

        unknown_on_solid = 20
    };


    //! \copydoc doxygen_hide_boundary_condition_enum
    enum class BoundaryConditionIndex : std::size_t {
        fluid_mesh = 10,
        fluid_mesh_many_labels = 11,

        solid_mesh = 20,
        solid_mesh_many_labels = 21
    };


 
    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::IndexedSectionDescription,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh_many_labels)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>::IndexedSectionDescription,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh_many_labels)>::IndexedSectionDescription,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>,

        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh_many_labels)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>,
        InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh_many_labels)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>,

        InputDataNS::Parallelism
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };


    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMDataForTest<ModelSettings>;


} // namespace MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS


#endif // MOREFEM_x_TEST_x_FINITE_ELEMENT_x_BOUNDARY_CONDITION_x_TWO_MESHES_x_MODEL_SETTINGS_HPP_
