/*!
// \file
//
*/

#include "Test/FiniteElement/BoundaryCondition/TwoMeshes/ModelSettings.hpp"


namespace MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>>(
            { "Finite element space for fluid mesh" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>>(
            { "Finite element space for solid mesh" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>>(
            { " unknown_on_fluid)" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>>(
            { " unknown_on_solid)" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>>({ " unknown)" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>>({ " fluid)" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>>({ " solid)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>>({ " fluid_full)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>>(
            { " fluid_mesh_one_label)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>>(
            { " fluid_mesh_many_labels)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>>({ " solid_full)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>>(
            { " solid_mesh_one_label)" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>>(
            { " solid_mesh_many_labels)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>>(
            { " fluid_mesh)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>>({ " fluid_mesh_many_labels)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>>(
            { " solid_mesh)" });
        SetDescription<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>>({ " solid_mesh_many_labels)" });

        Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>::DoMoveMesh>(
            false);
        Add<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>::DoMoveMesh>(
            false);


        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>::Name>("unknown");
        Add<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>::Nature>("vectorial");

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::Path>(
            "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::Dimension>(2ul);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>::SpaceUnit>(1.);

        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::Path>(
            "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx50_Ny20_force_label.mesh");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::Format>("Medit");
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::Dimension>(2ul);
        Add<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>::SpaceUnit>(1.);

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>::MeshIndexList>({ 1ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>::DimensionList>({ 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_full)>::GeomEltTypeList>({});

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>::MeshIndexList>({ 1ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>::DimensionList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>::MeshLabelList>({ 1ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_one_label)>::GeomEltTypeList>({});

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>::MeshIndexList>({ 1ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>::DimensionList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>::MeshLabelList>({ 1ul, 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid_mesh_many_labels)>::GeomEltTypeList>({});

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>::MeshIndexList>({ 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>::DimensionList>({ 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>::MeshLabelList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_full)>::GeomEltTypeList>({});

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>::MeshIndexList>({ 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>::DimensionList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>::MeshLabelList>({ 1ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_one_label)>::GeomEltTypeList>({});

        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>::MeshIndexList>({ 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>::DimensionList>({});
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>::MeshLabelList>({ 1ul, 2ul });
        Add<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid_mesh_many_labels)>::GeomEltTypeList>({});


        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>::Component>(
            "Comp12");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh)>::UnknownName>("unknown");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>::Values>(
            { 0., 0. });
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh)>::DomainIndex>(10ul);
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::fluid_mesh)>::IsMutable>(
            false);

        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>::Component>("Comp12");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>::UnknownName>("unknown");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>::Values>({ 0., 0. });
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>::DomainIndex>(11ul);
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::fluid_mesh_many_labels)>::IsMutable>(false);

        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>::Component>(
            "Comp12");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh)>::UnknownName>("unknown");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>::Values>(
            { 0., 0. });
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh)>::DomainIndex>(20ul);
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(BoundaryConditionIndex::solid_mesh)>::IsMutable>(
            false);

        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>::Component>("Comp12");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>::UnknownName>("unknown");
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>::Values>({ 0., 0. });
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>::DomainIndex>(21ul);
        Add<InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(
            BoundaryConditionIndex::solid_mesh_many_labels)>::IsMutable>(false);


        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::GodOfDofIndex>(1ul);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::DomainIndex>(1ul);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::UnknownList>({ "unknown" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::ShapeFunctionList>({ "P2" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>::NumberingSubsetList>({ 10ul });

        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::GodOfDofIndex>(2ul);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::DomainIndex>(2ul);
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::UnknownList>({ "unknown" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::ShapeFunctionList>({ "P1b" });
        Add<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>::NumberingSubsetList>({ 20ul });

        Add<InputDataNS::Parallelism::Policy>("ParallelNoWrite");
        Add<InputDataNS::Parallelism::Directory>({});
    }


} // namespace MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS
