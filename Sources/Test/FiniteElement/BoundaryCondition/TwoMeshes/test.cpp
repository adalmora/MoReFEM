/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/


#include <cstdlib>

#define BOOST_TEST_MODULE bc_two_meshes
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/FiniteElement/BoundaryCondition/TwoMeshes/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    using model_type = TestNS::BareModel<MoReFEM::TestNS::BoundaryConditionNS::TwoMeshesNS::morefem_data_type>;


    struct OutputDirWrapper
    {

        static constexpr std::string_view Path()
        {
            return "FiniteElement/BoundaryCondition/TwoMeshes";
        }
    };


    // clang-format off
    using fixture_type = TestNS::FixtureNS::ModelNoInputData
    <
        model_type,
        OutputDirWrapper,
        TestNS::FixtureNS::call_run_method_at_first_call::yes
    >;
    // clang-format on


} // namespace


BOOST_FIXTURE_TEST_CASE(check, fixture_type)
{
    GetModel();
}


PRAGMA_DIAGNOSTIC(pop)
