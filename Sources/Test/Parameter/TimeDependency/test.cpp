/*!
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:18:22 +0200
// Copyright (c) Inria. All rights reserved.
//
*/

#include <cstdlib>

#define BOOST_TEST_MODULE parameter_time_dependency
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/TimeDependency/TimeDependency.hpp"

#include "Test/Parameter/TimeDependency/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

namespace // anonymous
{

    using constant_time_step_policy = ::MoReFEM::TimeManagerNS::Policy::ConstantTimeStep;

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        TestNS::ParameterNS::TimeDependencyNS::morefem_data_type,
        constant_time_step_policy
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    constexpr auto epsilon = 1.e-8;

} // namespace


BOOST_FIXTURE_TEST_CASE(current_time, fixture_type)
{
    using namespace TestNS::ParameterNS::TimeDependencyNS;
    decltype(auto) model = GetNonCstModel();
    decltype(auto) time_manager = model.GetNonCstTimeManager();
    decltype(auto) morefem_data = model.GetMoReFEMData();
    

    decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                .GetDomain(AsDomainId(DomainIndex::full_mesh), __FILE__, __LINE__);

    auto scalar_param_ptr =
        InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus, ParameterNS::TimeDependencyFunctor>(
            "Young modulus", domain, morefem_data);
    auto& scalar_param = *scalar_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return time + 1;
        };

        auto time_dep_functor = std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::scalar>>(
            time_manager, std::move(time_dep));

        scalar_param.SetTimeDependency(std::move(time_dep_functor));
    }

    using vectorial_param_type = InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::source)>;

    auto vectorial_param_ptr =
        Init3DCompoundParameterFromInputData<vectorial_param_type, ParameterNS::TimeDependencyFunctor>(
            "Source", domain, morefem_data);

    auto& vectorial_param = *vectorial_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return 1.7 * time - 0.2;
        };

        auto time_dep_functor = std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::vector>>(
            time_manager, std::move(time_dep));

        vectorial_param.SetTimeDependency(std::move(time_dep_functor));
    }

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.);
    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(20.)(epsilon));

    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_EQUAL(vectorial_values[0], -2.);
        BOOST_CHECK_EQUAL(vectorial_values[1], -4.);
        BOOST_CHECK_EQUAL(vectorial_values[2], -6.);
    }

    time_manager.IncrementTime();
    scalar_param.TimeUpdate(); // in real model, should be done in Model::InitializeStep()
    vectorial_param.TimeUpdate();

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.1);

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(22.)(epsilon));


    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[0])(-0.3)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[1])(-0.6)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[2])(-0.9)(epsilon));
    }


    time_manager.IncrementTime();
    scalar_param.TimeUpdate(); // in real model, should be done in Model::InitializeStep()
    vectorial_param.TimeUpdate();

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(), 0.2);
    BOOST_CHECK_EQUAL(scalar_param.GetConstantValue(), 24.);

    {
        decltype(auto) vectorial_values = vectorial_param.GetConstantValue();

        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[0])(1.4)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[1])(2.8)(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vectorial_values[2])(4.2)(epsilon));
    }
}


BOOST_FIXTURE_TEST_CASE(fixed_time, fixture_type)
{
    using namespace TestNS::ParameterNS::TimeDependencyNS;
    decltype(auto) model = GetNonCstModel();
    decltype(auto) time_manager = model.GetNonCstTimeManager();
    decltype(auto) morefem_data = model.GetMoReFEMData();
    

    decltype(auto) domain = DomainManager::GetInstance(__FILE__, __LINE__)
                                .GetDomain(AsDomainId(DomainIndex::full_mesh), __FILE__, __LINE__);

    auto scalar_param_ptr =
        InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus, ParameterNS::TimeDependencyFunctor>(
            "Young modulus", domain, morefem_data);
    auto& scalar_param = *scalar_param_ptr;

    {
        auto time_dep = [](double time)
        {
            return time + 1;
        };

        auto time_dep_functor = std::make_unique<ParameterNS::TimeDependencyFunctor<ParameterNS::Type::scalar>>(
            time_manager, std::move(time_dep));

        scalar_param.SetTimeDependency(std::move(time_dep_functor));
    }

    constexpr auto new_time = 10.;
    scalar_param.TimeUpdate(new_time);

    BOOST_REQUIRE_EQUAL(time_manager.GetTime(),
                        0.2); // with fixed time parameter is decorrelated from time manager;
                              // this is to be used with extreme caution! (or better, not at all).

    BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (scalar_param.GetConstantValue())(220.)(epsilon));
}


PRAGMA_DIAGNOSTIC(pop)
