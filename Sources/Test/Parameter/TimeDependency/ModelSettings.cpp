/*!
// \file
//
*/

#include "Test/Parameter/TimeDependency/InputData.hpp"


namespace MoReFEM::TestNS::ParameterNS::TimeDependencyNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::vectorial)>>(
            { " vectorial" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::scalar)>>({ " scalar" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::vectorial)>>({ " vectorial" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::full_mesh)>>({ " full_mesh" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        SetDescription<InputDataNS::Source::PressureFromFile<EnumUnderlyingType(PressureIndex::pressure)>>(
            { "Pressure" });
        SetDescription<InputDataNS::VectorialTransientSource<EnumUnderlyingType(SourceIndex::source)>>({ "Source" });
    }


} // namespace MoReFEM::TestNS::ParameterNS::TimeDependencyNS
