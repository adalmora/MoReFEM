/*!
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 6 June 2020 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_AT_QUAD_PT_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_PARAMETER_x_AT_QUAD_PT_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/InputData/Instances/Reaction/MitchellSchaeffer.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::FibersAtQuadPt
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1ul };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t { domain = 1ul };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { sole = 1ul };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { generic_vectorial_unknown = 1ul, generic_scalar_unknown = 2ul };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        generic_vector_numbering_subset = 1ul,
        generic_scalar_numbering_subset = 2ul
    };


    //! \copydoc doxygen_hide_solver_enum
    enum class SolverIndex { solver = 1ul };

    //! \copydoc doxygen_hide_fiber_enum
    enum class FiberIndex : std::size_t {
        fiber_scalar_at_node = 1ul,
        fiber_vector_at_node = 2ul,
        fiber_scalar_at_quad_pt = 3ul,
        fiber_vector_at_quad_pt = 4ul
    };

    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::TimeManager,

        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_scalar_at_node),
            FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::scalar
        >,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_vector_at_node),
            FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_scalar_at_quad_pt),
            FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
            ParameterNS::Type::scalar
        >,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_vector_at_quad_pt),
            FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
            ParameterNS::Type::vector
        >,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;

    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription,

        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_scalar_at_node),
            FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::scalar
        >::IndexedSectionDescription,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_vector_at_node),
            FiberNS::AtNodeOrAtQuadPt::at_node,
            ParameterNS::Type::vector
        >::IndexedSectionDescription,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_scalar_at_quad_pt),
            FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
            ParameterNS::Type::scalar
        >::IndexedSectionDescription,
        InputDataNS::Fiber
        <
            EnumUnderlyingType(FiberIndex::fiber_vector_at_quad_pt),
            FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
            ParameterNS::Type::vector
        >::IndexedSectionDescription,
    
        ::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>::IndexedSectionDescription
    >;
    // clang-format on

    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;


} // namespace MoReFEM::TestNS::FibersAtQuadPt


#endif // MOREFEM_x_TEST_x_PARAMETER_x_AT_QUAD_PT_x_INPUT_DATA_HPP_
