/*!
// \file
//
*/

#include "Test/Parameter/AtQuadPt/InputData.hpp"


namespace MoReFEM::TestNS::FibersAtQuadPt
{


    void ModelSettings::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>>(
            { " generic_vector_numbering_subset" });
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>>(
            { " generic_scalar_numbering_subset" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>>(
            { " generic_vectorial_unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>>(
            { " generic_scalar_unknown" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain)>>({ " domain" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });

        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber_scalar_at_node),
                                          FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::scalar>>({ " scalar_at_node" });

        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber_vector_at_node),
                                          FiberNS::AtNodeOrAtQuadPt::at_node,
                                          ParameterNS::Type::vector>>({ " vectorial_at_node" });
        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber_scalar_at_quad_pt),
                                          FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                          ParameterNS::Type::scalar>>({ " scalar_at_quad_pt" });
        SetDescription<InputDataNS::Fiber<EnumUnderlyingType(FiberIndex::fiber_vector_at_quad_pt),
                                          FiberNS::AtNodeOrAtQuadPt::at_quad_pt,
                                          ParameterNS::Type::vector>>({ " vectorial_at_quad_pt" });
        SetDescription<::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(SolverIndex::solver)>>({ "Solver" });
    }


} // namespace MoReFEM::TestNS::FibersAtQuadPt
