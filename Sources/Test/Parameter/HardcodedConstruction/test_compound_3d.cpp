/*!
 // \file
 //
 //
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE parameter_hardcoded_construction_compound_3d
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/HardcodedConstruction/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HardcodedConstructionNS;


namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        TimeManagerNS::Policy::None,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;

    // clang-format off
    using model_time_dep_type =
    TestNS::BareModel
    <
        morefem_data_type,
        TimeManagerNS::Policy::ConstantTimeStep,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_dep_type = MoReFEM::TestNS::FixtureNS::Model<model_time_dep_type>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__

BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}


BOOST_FIXTURE_TEST_CASE(compound_3d, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        ParameterNS::ThreeDimensionalCoumpoundParameter
        <
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, double> y_value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), 1.21 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), 7.41 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), -17.2 }
    };

    using variant_type = typename Internal::ParameterNS::Traits<::MoReFEM::ParameterNS::Type::scalar>::variant_type;

    const variant_type x_value = 5.12;

    auto&& component_x = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
        "Value X", full_domain, "constant", x_value);

    const variant_type y_value = y_value_by_domain;

    auto&& component_y = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
        "Value Y", full_domain, "piecewise_constant_by_domain", y_value);

    Wrappers::Lua::Function<double(double, double, double)> lua_function("function (x, y, z)"
                                                                         " return 3. * x + y - 17.4 * math.cos(z)"
                                                                         " end");

    const variant_type z_value = lua_function;

    auto&& component_z = InitScalarParameterFromInputData<ParameterNS::TimeDependencyNS::None>(
        "Value Z", full_domain, "lua_function", z_value);


    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    auto compound_3d =
        parameter_type("compound", std::move(component_x), std::move(component_y), std::move(component_z));

    LocalVector expected_result({ 3ul });
    expected_result.resize({ 3ul });
    expected_result[0] = std::get<double>(x_value);
    SpatialPoint global_coords;

    for (const auto& [domain_id, value] : y_value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id, __FILE__, __LINE__);
        bool at_least_one_geom_elt_handled{ false };

        expected_result[1] = value;

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                const auto& quad_pt = *quad_pt_ptr;

                Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_coords);
                expected_result[2] = lua_function(global_coords[0], global_coords[1], global_coords[2]);

                BOOST_CHECK(
                    NumericNS::AreEqual(compound_3d.GetValue(*quad_pt_ptr, geom_elt), expected_result, epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


PRAGMA_DIAGNOSTIC(pop)
