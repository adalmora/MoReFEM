/*!
 // \file
 //
 //
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE parameter_hardcoded_construction_piecewise_constant_by_domain
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/Internal/ParameterInstance.hpp"
#include "Parameters/Policy/PiecewiseConstantByDomain/PiecewiseConstantByDomain.hpp"
#include "Parameters/TimeDependency/None.hpp"
#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/HardcodedConstruction/InputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HardcodedConstructionNS;


namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<double>();

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        TimeManagerNS::Policy::None,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}


// Check here there is a proper exception when for a same Parameter two Domains yield a different value.
BOOST_FIXTURE_TEST_CASE(inconsistency_between_domains, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(
        model); // very specific case but we really need that call here to fill properly data (DomainManager especially)

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::scalar,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on


    std::map<DomainNS::unique_id, double> inconsistent_value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::full_domain), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::cube1), 0.7 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::cube2), -21.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), 1.21 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), 7.41 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), -17.2 }
    };

    if (GetMpi().Nprocessor<int>() == 1)
    {
        // In parallel, some processors may handle no GeometricElt... and therefore have no
        // reason to throw an exception!
        BOOST_CHECK_THROW(std::make_unique<parameter_type>("inconsistent", full_domain, inconsistent_value_by_domain),
                          ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentDomains);
    }

    std::map<DomainNS::unique_id, double> consistent_value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::full_domain), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::cube1), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::cube2), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), 1.2 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), 1.2 }
    };


    parameter_type::unique_ptr consistent;

    consistent = std::make_unique<parameter_type>("consistent", full_domain, consistent_value_by_domain);
}


// Check an exception is properly thrown if in vectorial case the vectors aren't all the same shape.
BOOST_FIXTURE_TEST_CASE(inconsistent_shape_for_vectors, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(
        model); // very specific case but we really need that call here to fill properly data (DomainManager especially)


    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::vector,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, LocalVector> value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), LocalVector{ 5. } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), LocalVector{ 2., 3. } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), LocalVector{ 1.2, 4. } }
    };

    BOOST_CHECK_THROW(std::make_unique<parameter_type>("vector_with_inconsistent_shapes", full_domain, value_by_domain),
                      ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalVectorShape);
}


// Check an exception is properly thrown if in matricial case the matrices aren't all the same shape.
BOOST_FIXTURE_TEST_CASE(inconsistent_shape_for_matrices, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(
        model); // very specific case but we really need that call here to fill properly data (DomainManager especially)

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::matrix,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, LocalMatrix> value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), LocalMatrix{ { 5. }, { 7. } } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), LocalMatrix{ { 2., 3. }, { 3., -4. } } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), LocalMatrix{ { 1.2, 4. }, { -4., -1.2 } } }
    };

    BOOST_CHECK_THROW(std::make_unique<parameter_type>("matrix_with_inconsistent_shapes", full_domain, value_by_domain),
                      ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS::InconsistentLocalMatrixShape);
}


BOOST_FIXTURE_TEST_CASE(scalar, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::scalar,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on


    std::map<DomainNS::unique_id, double> value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), 1.21 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), 7.41 },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), -17.2 }
    };


    parameter_type param1("scalar_parameter", full_domain, value_by_domain);

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& [domain_id, value] : value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id, __FILE__, __LINE__);
        bool at_least_one_geom_elt_handled{ false };

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                      (param1.GetValue(*quad_pt_ptr, geom_elt))(value)(epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


BOOST_FIXTURE_TEST_CASE(vector, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::vector,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, LocalVector> value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1), LocalVector{ 1., -0.5, 4.21 } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2), LocalVector{ 10., 2., 5. } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3), LocalVector{ -1., -7.5, 2. } }
    };


    parameter_type param1("vector_parameter", full_domain, value_by_domain);

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& [domain_id, value] : value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id, __FILE__, __LINE__);
        bool at_least_one_geom_elt_handled{ false };

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                BOOST_CHECK(NumericNS::AreEqual(param1.GetValue(*quad_pt_ptr, geom_elt), value, epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


BOOST_FIXTURE_TEST_CASE(matrix, fixture_type)
{
    decltype(auto) model = GetModel();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    // clang-format off
    using parameter_type =
        Internal::ParameterNS::ParameterInstance
        <
            ParameterNS::Type::matrix,
            ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
            ::MoReFEM::ParameterNS::TimeDependencyNS::None
        >;
    // clang-format on

    std::map<DomainNS::unique_id, LocalMatrix> value_by_domain{
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad1),
          LocalMatrix{ { 1., 2. }, { 3., 4. }, { 5., 6. } } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad2),
          LocalMatrix{ { 10., 2. }, { -54.1, 1.e-4 }, { 3., -9. } } },
        { AsDomainId(TestNS::HardcodedConstructionNS::DomainIndex::quad3),
          LocalMatrix{ { 2., -1.2 }, { 0.21, -9.87 }, { -5., 3.14 } } }
    };


    parameter_type param1("matrix_parameter", full_domain, value_by_domain);

    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& [domain_id, value] : value_by_domain)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id, __FILE__, __LINE__);
        bool at_least_one_geom_elt_handled{ false };

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                BOOST_CHECK(NumericNS::AreEqual<LocalMatrix>(param1.GetValue(*quad_pt_ptr, geom_elt), value, epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


PRAGMA_DIAGNOSTIC(pop)
