
// An empty file to please CMake.
//
// If I use add_custom_target(), the files listed MUST be PRIVATE (as of CMake 3.20), whereas
// I want some of them public to make them appear as intended in generated IDE.
//
// So to obtain the expected behaviour I need a library, for which I need at least one file
// to be compiled... even if this file is indeed empty...
