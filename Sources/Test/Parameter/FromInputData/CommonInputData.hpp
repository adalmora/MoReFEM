/*!
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMMON_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMMON_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::FromInputDataNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        domain_3d = 1ul,
        full_domain = 2ul,
        cube1 = 10ul,
        cube2 = 11ul,
        quad1 = 20ul,
        quad2 = 21ul,
        quad3 = 22ul
    };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { sole = 1ul };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { generic_vectorial_unknown = 1ul, generic_scalar_unknown = 2ul };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        generic_vector_numbering_subset = 1ul,
        generic_scalar_numbering_subset = 2ul
    };


} // namespace MoReFEM::TestNS::FromInputDataNS


#endif // MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMMON_INPUT_DATA_HPP_
