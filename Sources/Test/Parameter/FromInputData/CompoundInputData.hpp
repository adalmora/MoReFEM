/*!
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMPOUND_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMPOUND_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp"                         // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/Advanced/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/InputData/Instances/Parameter/Source/ScalarTransientSource.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"

#include "Test/Parameter/FromInputData/CommonInputData.hpp"


namespace MoReFEM::TestNS::FromInputDataNS
{


    //! Parameter defined for this test.
    struct CompoundParameter
    : public ::MoReFEM::Internal::InputDataNS::ParamNS::
          ThreeDimensionalCompoundParameter<CompoundParameter, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! Convenient alias.
        using self = CompoundParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("LegitCompoundParameter");
            return ret;
        }
    };


    //! Parameter defined for this test.
    struct PartlyIgnoredParameter : public ::MoReFEM::Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundParameter<
                                        PartlyIgnoredParameter,
                                        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! Convenient alias.
        using self = PartlyIgnoredParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("PartlyIgnoredParameter");
            return ret;
        }
    };


    //! Parameter defined for this test.
    struct CompletelyIgnoredParameter
    : public ::MoReFEM::Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundParameter<
          CompletelyIgnoredParameter,
          ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! Convenient alias.
        using self = CompletelyIgnoredParameter;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("CompletelyIgnoredParameter");
            return ret;
        }
    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>,

        InputDataNS::LightweightDomainList<1>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        CompoundParameter,
        PartlyIgnoredParameter,
        CompletelyIgnoredParameter,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>::IndexedSectionDescription,
        InputDataNS::LightweightDomainList<1>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription
    >;
    // clang-format on

    //! \copydoc doxygen_hide_model_specific_model_settings
    using model_settings_type = ModelSettings<model_settings_tuple>;


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        void Init() override;
    };

//! \copydoc doxygen_hide_morefem_data_type
using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;



} // namespace MoReFEM::TestNS::FromInputDataNS


#endif // MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_x_COMPOUND_INPUT_DATA_HPP_
