-- Comment lines are introduced by "--".
-- In a section (i.e. within braces), all entries must be separated by a comma.

NumberingSubset1 = {



    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset1

NumberingSubset2 = {



    -- Whether a vector defined on this numbering subset might be used to compute a movemesh. If true, a
    -- FEltSpace featuring this numbering subset will compute additional quantities to enable fast computation.
    -- This should be false for most numbering subsets, and when it's true the sole unknown involved should be a
    -- displacement.
    -- Expected format: 'true' or 'false' (without the quote)
    do_move_mesh = false


} -- NumberingSubset2

Unknown1 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "generic_vectorial_unknown",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "vectorial",

} -- Unknown1

Unknown2 = {

    -- Name of the unknown (used for displays in output).
    -- Expected format: "VALUE"
    name = "generic_scalar_unknown",

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'scalar', 'vectorial'})
    nature = "scalar",

} -- Unknown1

Mesh1 = {
    
    -- Path of the mesh file to use.
    -- Expected format: "VALUE"
    mesh = '${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputData/TwoCubesManyRef.mesh',
    
    -- Format of the input mesh.
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'Ensight', 'Medit'})
    format = "Medit",
    
    -- Highest dimension of the input mesh. This dimension might be lower than the one effectively read in the
    -- mesh file; in which case Coords will be reduced provided all the dropped values are 0. If not, an
    -- exception is thrown.
    -- Expected format: VALUE
    -- Constraint: v <= 3 and v > 0
    dimension = 3,
    
    -- Space unit of the mesh.
    -- Expected format: VALUE
    space_unit = 1.
    
} -- Mesh1

Domain1 = {
    
    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = { 1 },
    
    -- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon
    -- dimensions.
    -- Expected format: {VALUE1, VALUE2, ...}
    -- Constraint: value_in(v, {0, 1, 2, 3})
    dimension_list = { 3 },
    
    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { },
    
    -- List of geometric element types considered in the domain. Might be left empty if no restriction upon
    -- these. No constraint is applied at OptionFile level, as some geometric element types could be added after
    -- generation of current input data file. Current list is below; if an incorrect value is put there it
    -- will be detected a bit later when the domain object is built.
    -- The known types when this file was generated are:
    -- . Point1
    -- . Segment2, Segment3
    -- . Triangle3, Triangle6
    -- . Quadrangle4, Quadrangle8, Quadrangle9
    -- . Tetrahedron4, Tetrahedron10
    -- . Hexahedron8, Hexahedron20, Hexahedron27.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    geometric_element_type_list = { }
    
} -- Domain1


LightweightDomainList1 = {

    -- Index of the geometric mesh upon which the domain is defined (as defined in the present file). Might be
    -- left empty if domain not limited to one mesh; at most one value is expected here.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_index = 1,

    -- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh
    -- labels. This parameter does not make sense if no mesh is defined for the domain.
    -- Expected format: {VALUE1, VALUE2, ...}
    mesh_label_list = { 0, 1, 2, 3, 4 },
    
    -- Give an unique id to each of the shorthand domains defined. These must not
    -- clash with each other or with domains defined by a more conventional way.
    -- Expected format: {VALUE1, VALUE2, ...}
    domain_index_list = { 2, 10, 11, 20, 21, 22 },

    -- Number of mesh labels to consider in each domain. Sum of these numbers must be equal to the number of
    -- entries in mesh label list.
    -- Expected format: {VALUE1, VALUE2, ...}
    number_in_domain_list = { 0, 1, 1, 1, 1, 1 }

} -- LightweightDomainList1



FiniteElementSpace1 = {

    -- Index of the god of dof into which the finite element space is defined.
    -- Expected format: VALUE
    god_of_dof_index = 1,

    -- Index of the domain onto which the finite element space is defined. This domain must be unidimensional.
    -- Expected format: VALUE
    domain_index = 1,

    -- List of all unknowns defined in the finite element space. Unknowns here must be defined in this file as
    -- an 'Unknown' block; expected name/identifier is the name given there.
    -- Expected format: {"VALUE1", "VALUE2", ...}
    unknown_list = { "generic_vectorial_unknown", "generic_scalar_unknown" },

    -- List of the shape function to use for each unknown;
    -- Expected format: {"VALUE1", "VALUE2", ...}
    shape_function_list = { "Q1", "Q1" },

    -- List of the numbering subset to use for each unknown;
    -- Expected format: {VALUE1, VALUE2, ...}
    numbering_subset_list = { 1, 2 },

} -- FiniteElementSpace1


ConstantParameter = {
    
    -- How is given the parameter (as a constant, as a Lua function, per quadrature point, etc...). Choose
    -- "ignore" if you do not want this parameter (in this case it will stay at nullptr).
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'lua_function','piecewise_constant_by_domain'})
    nature = "constant",
    
    matrix_dimension = { 2, 4, 3 },


    -- The value for the parameter, which type depends directly on the nature chosen:
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    --  If nature is 'lua_function', expected format is a Lua function with prototype function(x, y, z)
    -- return x + y - z
    -- end
    -- where x, y and z are global coordinates. sin, cos, tan, exp and so forth require a 'math.' prefix.
    -- Expected format: see the variant description...
    value = { }
}


IgnoredParameter = {

    -- How is given the parameter (as a constant, per quadrature point, etc...). Choose "ignore" if you do not
    -- want this parameter (in this case it will stay at nullptr).
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'piecewise_constant_by_domain'})
    nature = "ignore",


    -- Dimension of the vector. Must match exactly the number of elements given in the 'value' field.
    -- Expected format: VALUE
    matrix_dimension = { 1, 1 },


    -- The values of the vectorial parameter; expected format is a table (opening = '{', closing = '} and
    -- separator = ',') and each item depends on the nature specified at the namesake field:
    --
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    -- Expected format: { VALUE1, VALUE2, ...}
    value = { 500. }

} -- IgnoredParameter


PiecewiseConstantByDomainParameter = {


    -- How is given the parameter (as a constant, per quadrature point, etc...). Choose "ignore" if you do not
    -- want this parameter (in this case it will stay at nullptr).
    -- Expected format: "VALUE"
    -- Constraint: value_in(v, {'ignore', 'constant', 'piecewise_constant_by_domain'})
    nature = "piecewise_constant_by_domain",


    -- Dimension of the vector. Must match exactly the number of elements given in the 'value' field.
    -- Expected format: VALUE
    matrix_dimension = { 3  },


    -- The values of the vectorial parameter; expected format is a table (opening = '{', closing = '} and
    -- separator = ',') and each item depends on the nature specified at the namesake field:
    --
    --  If nature is 'constant', expected format is VALUE
    --  If nature is 'piecewise_constant_by_domain', expected format is {[KEY1] = VALUE1, [KEY2] = VALUE2, ...}
    -- Expected format: { 500., 724., 211.1 }
    value = { [20] = { 1., 2., 3., 4., 5., 6. }, [21] = { 10., 2., -54.1, 1.e-4 , 3., -9. }, [22] = { 2., -1.2, 0.21, -9.87, -5., 3.14 } }
    
} -- PiecewiseConstantByDomainParameter



Result = {

	-- Directory in which all the results will be written.  
	-- Expected format: "VALUE"
	output_directory = "${MOREFEM_TEST_OUTPUT_DIR}/Parameter/FromInputData/Matricial",

	-- Enables to skip some printing in the console. Can be used to WriteSolution every n time.
	-- Expected format: VALUE
	display_value = 1,
    
    -- Defines the solutions output format. Set to false for ascii or true for binary.
    -- Expected format: VALUE
    binary_output = false
    
} -- Result

