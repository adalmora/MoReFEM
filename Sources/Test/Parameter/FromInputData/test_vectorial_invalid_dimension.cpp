/*!
 // \file
 //
 //
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE vectorial_parameter_from_input_data_invalid_dimension
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/FromInputData/VectorialInputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataNS;


namespace // anonymous
{

    // clang-format off
    using model_type =
    MoReFEM::TestNS::BareModel
    <
        MoReFEM::TestNS::FromInputDataNS::morefem_data_type,
        MoReFEM::TimeManagerNS::Policy::None,
        MoReFEM::DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(too_big_vector, TestNS::FixtureNS::TestEnvironment)
{
    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputData/too_big_vector.lua") };

    auto morefem_data = TestNS::FromInputDataNS::morefem_data_type{ std::move(lua_file) };
    

    model_type model(morefem_data);
    model.Initialize();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    BOOST_CHECK_THROW(auto constant_param_ptr =
                          InitVectorialParameterFromInputData<ConstantParameter>("constant", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimension);

    BOOST_CHECK_THROW(auto piecewise_constant_by_param_ptr =
                          InitVectorialParameterFromInputData<PiecewiseConstantByDomainParameter>(
                              "piecewise", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain);
}


BOOST_FIXTURE_TEST_CASE(too_small_vector, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ClearSingletons::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputData/too_small_vector.lua") };

    auto morefem_data = TestNS::FromInputDataNS::morefem_data_type{ std::move(lua_file) };
    

    model_type model(morefem_data);
    model.Initialize();

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    BOOST_CHECK_THROW(auto constant_param_ptr =
                          InitVectorialParameterFromInputData<ConstantParameter>("constant", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimension);

    BOOST_CHECK_THROW(auto piecewise_constant_by_param_ptr =
                          InitVectorialParameterFromInputData<PiecewiseConstantByDomainParameter>(
                              "piecewise", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain);
}


PRAGMA_DIAGNOSTIC(pop)
