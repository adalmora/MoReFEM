/*!
 // \file
 //
 //
 // Copyright (c) Inria. All rights reserved.
 //
 */


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"

#define BOOST_TEST_MODULE compound_3d_parameter_from_input_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/FromInputData/CompoundInputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"

using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataNS;


namespace // anonymous
{

    // clang-format off
    using model_type =
    TestNS::BareModel
    <
        morefem_data_type,
        TimeManagerNS::Policy::None,
        DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on

    using fixture_type = MoReFEM::TestNS::FixtureNS::Model<model_type>;


    constexpr auto epsilon = 1.e-12;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(only_sequential_test, fixture_type)
{
    // Please read README for the explanation of why a parallel test was not kept.
    decltype(auto) mpi = GetMpi();
    BOOST_CHECK_EQUAL(mpi.Nprocessor<int>(), 1);
}


BOOST_FIXTURE_TEST_CASE(legit_compound, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();
    

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    const double expected_x = 5.12;

    std::map<DomainNS::unique_id, double> expected_y{ { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad1), 1.21 },
                                                      { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad2), 7.41 },
                                                      { AsDomainId(TestNS::FromInputDataNS::DomainIndex::quad3),
                                                        -17.2 } };

    Wrappers::Lua::Function<double(double, double, double)> expected_z("function (x, y, z)"
                                                                       " return 3. * x + y - 17.4 * math.cos(z)"
                                                                       " end");


    auto compound_ptr =
        Init3DCompoundParameterFromInputData<CompoundParameter>("compound_parameter", full_domain, morefem_data);

    BOOST_REQUIRE(compound_ptr != nullptr);

    const auto& compound_3d = *compound_ptr;

    LocalVector expected_result;
    expected_result.resize({ 3ul });
    expected_result[0] = expected_x;
    SpatialPoint global_coords;


    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(MeshIndex::mesh));
    decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::mesh));
    decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));
    decltype(auto) quadrature_rule_per_topology = felt_space.GetQuadratureRulePerTopology();

    for (const auto& [domain_id, value] : expected_y)
    {
        decltype(auto) domain = domain_manager.GetDomain(domain_id, __FILE__, __LINE__);
        bool at_least_one_geom_elt_handled{ false };

        expected_result[1] = value;

        for (const auto& geom_elt_ptr : geom_elt_list)
        {
            assert(!(!geom_elt_ptr));
            const auto& geom_elt = *geom_elt_ptr;

            if (!domain.IsGeometricEltInside(geom_elt))
                continue;

            if (geom_elt.GetIdentifier() != Advanced::GeometricEltEnum::Quadrangle4)
                continue;

            at_least_one_geom_elt_handled = true;

            decltype(auto) quadrature_rule =
                quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());

            decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();

            for (const auto& quad_pt_ptr : quad_pt_list)
            {
                assert(!(!quad_pt_ptr));
                const auto& quad_pt = *quad_pt_ptr;

                Advanced::GeomEltNS::Local2Global(geom_elt, quad_pt, global_coords);
                expected_result[2] = expected_z(global_coords[0], global_coords[1], global_coords[2]);

                BOOST_CHECK(
                    NumericNS::AreEqual(compound_3d.GetValue(*quad_pt_ptr, geom_elt), expected_result, epsilon));
            }
        }

        BOOST_REQUIRE(at_least_one_geom_elt_handled == true);
    }
}


BOOST_FIXTURE_TEST_CASE(ignore, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) morefem_data = model.GetMoReFEMData();
    

    decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain), __FILE__, __LINE__);

    {
        auto compound_ptr = Init3DCompoundParameterFromInputData<CompletelyIgnoredParameter>(
            "completely_ignored", full_domain, morefem_data);
        BOOST_CHECK(compound_ptr == nullptr);
    }

    {
        BOOST_CHECK_THROW(
            Init3DCompoundParameterFromInputData<PartlyIgnoredParameter>("partially_ignored", full_domain, morefem_data),
            ExceptionNS::ParameterNS::PartialIgnoredCompound);
    }
}


PRAGMA_DIAGNOSTIC(pop)
