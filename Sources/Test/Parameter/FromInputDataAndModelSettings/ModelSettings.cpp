/*!
// \file
//
*/

#include "Test/Parameter/FromInputDataAndModelSettings/InputData.hpp"


namespace MoReFEM::TestNS::FromInputDataAndModelSettingsNS
{


    void ModelSettings::Init()
    {
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>>(
            { " generic_vector_numbering_subset" });
        SetDescription<
            InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>>(
            { " generic_scalar_numbering_subset" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>>(
            { " generic_vectorial_unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>>(
            { " generic_scalar_unknown" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>>({ " domain_3d" });
        SetDescription<InputDataNS::LightweightDomainList<1>>({ " 1" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>>(
            { "Sole finite element space" });
        
        
        Add<ConstantParameterAllInModelSettings::Nature>("constant");
        Add<ConstantParameterAllInModelSettings::Value>(5.42);

        Add<ConstantParameterNatureInModelSettings::Nature>("constant");
        Add<ConstantParameterValueInModelSettings::Value>(5.42);        
    }


} // namespace MoReFEM::TestNS::FromInputDataAndModelSettingsNS
