/*!
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_AND_MODEL_SETTINGS_x_INPUT_DATA_HPP_
#define MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_AND_MODEL_SETTINGS_x_INPUT_DATA_HPP_

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/InputData/Instances/FElt/NumberingSubset.hpp"
#include "Core/InputData/Instances/FElt/Unknown.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"
#include "Core/InputData/Instances/Geometry/LightweightDomainList.hpp"
#include "Core/InputData/Instances/Geometry/Mesh.hpp"
#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/MoReFEMData.hpp"


namespace MoReFEM::TestNS::FromInputDataAndModelSettingsNS
{


    //! \copydoc doxygen_hide_mesh_enum
    enum class MeshIndex : std::size_t { mesh = 1 };


    //! \copydoc doxygen_hide_domain_enum
    enum class DomainIndex : std::size_t {
        domain_3d = 1ul,
        full_domain = 2ul,
        cube1 = 10ul,
        cube2 = 11ul,
        quad1 = 20ul,
        quad2 = 21ul,
        quad3 = 22ul
    };


    //! \copydoc doxygen_hide_felt_space_enum
    enum class FEltSpaceIndex : std::size_t { sole = 1ul };


    //! \copydoc doxygen_hide_unknown_enum
    enum class UnknownIndex : std::size_t { generic_vectorial_unknown = 1ul, generic_scalar_unknown = 2ul };


    //! \copydoc doxygen_hide_numbering_subset_enum
    enum class NumberingSubsetIndex : std::size_t {
        generic_vector_numbering_subset = 1ul,
        generic_scalar_numbering_subset = 2ul
    };


    //! Parameter defined for this test.
    struct ConstantParameterAllInInputData
    // clang-format off
    : public Internal::InputDataNS::ParamNS::ScalarParameter
             <
                ConstantParameterAllInInputData,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = ConstantParameterAllInInputData;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("ConstantParameterAllInInputData");
            return ret;
        }
    };



    //! Parameter defined for this test.
    struct ConstantParameterNatureInModelSettings
    // clang-format off
    : public Internal::InputDataNS::ParamNS::ScalarParameter
             <
                ConstantParameterNatureInModelSettings,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = ConstantParameterNatureInModelSettings;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("ConstantParameterNatureInModelSettings");
            return ret;
        }
    };


    //! Parameter defined for this test.
    struct ConstantParameterValueInModelSettings
    // clang-format off
    : public Internal::InputDataNS::ParamNS::ScalarParameter
             <
                ConstantParameterValueInModelSettings,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = ConstantParameterValueInModelSettings;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("ConstantParameterValueInModelSettings");
            return ret;
        }
    };


     //! Parameter defined for this test.
    struct ConstantParameterAllInModelSettings
    // clang-format off
    : public Internal::InputDataNS::ParamNS::ScalarParameter
             <
                ConstantParameterAllInModelSettings,
                Advanced::InputDataNS::NoEnclosingSection
             >
    // clang-format on
    {
        //! Convenient alias.
        using self = ConstantParameterAllInModelSettings;

        //! Friendship to section parent.
        using parent = Advanced::InputDataNS::Crtp::Section<self>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Return the name of the section in the input datum.
         *
         */
        static const std::string& GetName()
        {
            static std::string ret("ConstantParameterAllInModelSettings");
            return ret;
        }
    };


    //! \copydoc doxygen_hide_input_data_tuple
    // clang-format off
    using input_data_tuple = std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>,

        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>,

        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>,

        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>,

        InputDataNS::LightweightDomainList<1>,

        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>,

        ConstantParameterAllInInputData,
        ConstantParameterNatureInModelSettings::Value,
        ConstantParameterValueInModelSettings::Nature,

        InputDataNS::Result
    >;
    // clang-format on


    //! \copydoc doxygen_hide_model_specific_input_data
    using input_data_type = InputData<input_data_tuple>;


    //! \copydoc doxygen_hide_model_settings_tuple
    // clang-format off
    using model_settings_tuple =
    std::tuple
    <
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_vector_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::generic_scalar_numbering_subset)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_vectorial_unknown)>::IndexedSectionDescription,
        InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::generic_scalar_unknown)>::IndexedSectionDescription,
        InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>::IndexedSectionDescription,
        InputDataNS::Domain<EnumUnderlyingType(DomainIndex::domain_3d)>::IndexedSectionDescription,
        InputDataNS::LightweightDomainList<1>::IndexedSectionDescription,
        InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::sole)>::IndexedSectionDescription,

        ConstantParameterNatureInModelSettings::Nature,
        ConstantParameterValueInModelSettings::Value,
        ConstantParameterAllInModelSettings
    >;
    // clang-format on


    /*!
     * \copydoc doxygen_hide_model_specific_model_settings
     */
    struct ModelSettings : public ::MoReFEM::ModelSettings<model_settings_tuple>
    {

        //! \copydoc doxygen_hide_model_specific_model_settings_init
        void Init() override;
    };

    //! \copydoc doxygen_hide_morefem_data_type
    using morefem_data_type = MoReFEMData<input_data_type, ModelSettings, program_type::test>;


} // namespace MoReFEM::TestNS::FromInputDataAndModelSettingsNS


#endif // MOREFEM_x_TEST_x_PARAMETER_x_FROM_INPUT_DATA_AND_MODEL_SETTINGS_x_INPUT_DATA_HPP_
