/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_STRONG_TYPE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_STRONG_TYPE_HPP_


#include <cstddef> // IWYU pragma: keep

// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::NodeBearerNS
{

    //! Strong type for displacement vectors.
    // clang-format off
    using program_wise_index_type =
        StrongType
        <
            std::size_t,
            struct program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Hashable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::AsMpiDatatype,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::NodeBearerNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_STRONG_TYPE_HPP_
