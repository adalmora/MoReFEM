/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_INDEXES_TYPES_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_INDEXES_TYPES_HPP_

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::DofNS
{


    /*!
     * \brief Strong type to represent a program-wise \a Dof index
     */
    // clang-format off
    using program_wise_index =
    StrongType
    <
        std::size_t,
        struct ProgramWiseIndexTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::AsMpiDatatype,
        StrongTypeNS::Addable,
        StrongTypeNS::Incrementable
    >;
    // clang-format on


    /*!
     * \brief Strong type to represent a program-wise \a Dof index
     */
    // clang-format off
    using processor_wise_or_ghost_index =
    StrongType
    <
        std::size_t,
        struct ProcessorWiseOrGhostIndexTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::AsMpiDatatype,
        StrongTypeNS::Addable,
        StrongTypeNS::Incrementable
    >;
    // clang-format on

} // namespace MoReFEM::DofNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_DOF_INDEXES_TYPES_HPP_
