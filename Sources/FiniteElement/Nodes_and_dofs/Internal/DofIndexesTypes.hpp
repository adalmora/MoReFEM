/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_DOF_INDEXES_TYPES_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_DOF_INDEXES_TYPES_HPP_

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Internal::DofNS
{

    /*!
     * \brief Strong type to represent an internal processor-wise or ghost index for a \a Dof.
     */
    // clang-format off
    using internal_processor_wise_or_ghost_index =
    StrongType
    <
        std::size_t,
        struct InternalProcessorWiseOrGhostIndexTypeTag,
        ::MoReFEM::StrongTypeNS::Comparable,
        ::MoReFEM::StrongTypeNS::Hashable,
        ::MoReFEM::StrongTypeNS::DefaultConstructible,
        ::MoReFEM::StrongTypeNS::Printable,
        ::MoReFEM::StrongTypeNS::AsMpiDatatype,
        ::MoReFEM::StrongTypeNS::Addable
    >;
    // clang-format on


} // namespace MoReFEM::Internal::DofNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_NODES_xAND_xDOFS_x_INTERNAL_x_DOF_INDEXES_TYPES_HPP_
