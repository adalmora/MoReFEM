//! \file
//
//
//  SetIndex.cpp
//
//
//  Created by Sébastien Gilles on 04/09/2022.
//
//

#include "FiniteElement/Nodes_and_dofs/Internal/SetIndex.hpp"
#include "FiniteElement/Nodes_and_dofs/Dof.hpp"
// IWYU pragma: no_include "SetIndex.hpp"

namespace MoReFEM::Internal::DofNS
{


    void SetIndex::ProgramWiseIndex(const NumberingSubset& numbering_subset,
                                    ::MoReFEM::DofNS::program_wise_index index,
                                    Dof& dof)
    {
        dof.SetProgramWiseIndex(numbering_subset, index);
    }


    void SetIndex::ProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset,
                                             ::MoReFEM::DofNS::processor_wise_or_ghost_index index,
                                             Dof& dof)
    {
        dof.SetProcessorWiseOrGhostIndex(numbering_subset, index);
    }


    void SetIndex::InternalProcessorWiseOrGhostIndex(
        ::MoReFEM::Internal::DofNS::internal_processor_wise_or_ghost_index index,
        Dof& dof)
    {
        dof.SetInternalProcessorWiseOrGhostIndex(index);
    }


} // namespace MoReFEM::Internal::DofNS
