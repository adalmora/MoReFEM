/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 18 Sep 2013 11:05:16 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <tuple>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/Nodes_and_dofs/Dof.hpp"  // IWYU pragma: associated
#include "FiniteElement/Nodes_and_dofs/Node.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace // anonymous
    {

#ifndef NDEBUG
        // StorageT is a std::vector<std::pair<::MoReFEM::NumberingSubsetNS::unique_id, INDEXT>
        // where INDEXT is either processor wise or program wise index.
        template<class StorageT>
        void AssertNewNumberingSubset(const StorageT& storage, const NumberingSubset& numbering_subset);
#endif // NDEBUG


        //! Return the const iterator matching \a numbering_subset in \a storage.
        template<class StorageT>
        typename StorageT::const_iterator Iterator(const StorageT& storage, const NumberingSubset& numbering_subset);


        //! Returns the index stored in \a storage that matches the input \a numbering_subset.
        template<class StorageT>
        typename std::tuple_element<1, typename StorageT::value_type>::type
        FindInStorage(const StorageT& storage, const NumberingSubset& numbering_subset);


    } // namespace


    Dof::Dof(const std::shared_ptr<const Node>& node_ptr) : node_(node_ptr)
    { }


    Dof::~Dof() = default;


    void Dof::SetProgramWiseIndex(const NumberingSubset& numbering_subset, DofNS::program_wise_index index)
    {
#ifndef NDEBUG
        AssertNewNumberingSubset(program_wise_index_per_numbering_subset_, numbering_subset);
#endif // NDEBUG

        program_wise_index_per_numbering_subset_.push_back({ numbering_subset.GetUniqueId(), index });
    }


    void Dof::SetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset,
                                           DofNS::processor_wise_or_ghost_index index)
    {
#ifndef NDEBUG
        AssertNewNumberingSubset(processor_wise_or_ghost_index_per_numbering_subset_, numbering_subset);
#endif // NDEBUG

        processor_wise_or_ghost_index_per_numbering_subset_.push_back({ numbering_subset.GetUniqueId(), index });
    }


    void Dof::SetInternalProcessorWiseOrGhostIndex(Internal::DofNS::internal_processor_wise_or_ghost_index index)
    {
        assert(internal_processor_wise_or_ghost_index_
                   == NumericNS::UninitializedIndex<Internal::DofNS::internal_processor_wise_or_ghost_index>()
               && "Should be allocated only once!");
        internal_processor_wise_or_ghost_index_ = index;
    }


    DofNS::processor_wise_or_ghost_index
    Dof::GetProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProcessorWiseOrGhostIndexPerNumberingSubset(), numbering_subset);
    }


    DofNS::program_wise_index Dof::GetProgramWiseIndex(const NumberingSubset& numbering_subset) const
    {
        return FindInStorage(GetProgramWiseIndexPerNumberingSubset(), numbering_subset);
    }


    bool Dof::IsInNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto node_ptr = GetNodeFromWeakPtr();
        assert(!(!node_ptr));
        return node_ptr->IsInNumberingSubset(numbering_subset);
    }


    namespace // anonymous
    {


#ifndef NDEBUG
        template<class StorageTypeT>
        void AssertNewNumberingSubset(const StorageTypeT& storage, const NumberingSubset& numbering_subset)
        {
            const auto id = numbering_subset.GetUniqueId();

            assert(std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                }

                                )
                       == storage.cend()
                   && "A given numbering subset should appear only once!");
        }
#endif // NDEBUG


        template<class StorageT>
        typename StorageT::const_iterator Iterator(const StorageT& storage, const NumberingSubset& numbering_subset)
        {
            assert(!storage.empty());
            const auto id = numbering_subset.GetUniqueId();

            return std::find_if(storage.cbegin(),
                                storage.cend(),
                                [id](const auto& pair)
                                {
                                    return pair.first == id;
                                });
        }


        template<class StorageT>
        typename std::tuple_element<1, typename StorageT::value_type>::type
        FindInStorage(const StorageT& storage, const NumberingSubset& numbering_subset)
        {
            assert(!storage.empty());

            auto it = Iterator(storage, numbering_subset);
            assert(it != storage.cend());
            return it->second;
        }


    } // namespace


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
