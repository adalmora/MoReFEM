/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 7 Nov 2014 09:54:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include "FiniteElement/RefFiniteElement/Internal/Impl/BasicRefFEltHolder.hpp"


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    AbstractBasicRefFEltHolder::~AbstractBasicRefFEltHolder() = default;

} // namespace MoReFEM::Internal::RefFEltNS::Impl


/// @} // addtogroup FiniteElementGroup
