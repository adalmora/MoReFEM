/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Jun 2015 16:12:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <numeric>
#include <type_traits> // IWYU pragma: keep

#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    inline const RefGeomElt& RefLocalFEltSpace::GetRefGeomElt() const noexcept
    {
        assert(!(!ref_geom_elt_));
        return *ref_geom_elt_;
    }


    inline std::size_t RefLocalFEltSpace::Nnode() const noexcept
    {
        assert("Check the cached value reflects the actual content!"
               && Nnode_
                      == std::accumulate(ref_felt_list_.cbegin(),
                                         ref_felt_list_.cend(),
                                         0ul,
                                         [](std::size_t sum, RefFEltInFEltSpace::const_shared_ptr ref_felt)
                                         {
                                             assert(!(!ref_felt));
                                             return sum + ref_felt->Nnode();
                                         }));

        return Nnode_;
    }


    inline std::size_t RefLocalFEltSpace::Ndof() const noexcept
    {
        assert("Check the cached value reflects the actual content!"
               && Ndof_
                      == std::accumulate(ref_felt_list_.cbegin(),
                                         ref_felt_list_.cend(),
                                         0ul,
                                         [](std::size_t sum, RefFEltInFEltSpace::const_shared_ptr ref_felt)
                                         {
                                             assert(!(!ref_felt));
                                             return sum + ref_felt->Ndof();
                                         }));

        return Ndof_;
    }


    inline const RefFEltInFEltSpace& RefLocalFEltSpace ::GetRefFElt(const ExtendedUnknown& extended_unknown) const
    {
        auto it = std::find_if(ref_felt_list_.cbegin(),
                               ref_felt_list_.cend(),
                               [&extended_unknown](const RefFEltInFEltSpace::const_shared_ptr& ref_felt_ptr)
                               {
                                   assert(!(!ref_felt_ptr));
                                   const auto& ref_felt = *ref_felt_ptr;

                                   return ref_felt.GetExtendedUnknown() == extended_unknown;
                               });

        assert(it != ref_felt_list_.cend());
        assert(!(!*it));
        return *(*it);
    }


    inline const RefFEltInFEltSpace::vector_const_shared_ptr& RefLocalFEltSpace::GetRefFEltList() const noexcept
    {
        return ref_felt_list_;
    }


} // namespace MoReFEM::Internal::RefFEltNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INTERNAL_x_REF_LOCAL_F_ELT_SPACE_HXX_
