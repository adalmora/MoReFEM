/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 11 Oct 2020 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/QuadrangleQ0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {


        __attribute__((unused)) const bool registered =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<QuadrangleQ0>();


    } // namespace


    const std::string& QuadrangleQ0::ShapeFunctionLabel()
    {
        static std::string ret("Q0");
        return ret;
    }


    QuadrangleQ0::~QuadrangleQ0() = default;


} // namespace MoReFEM::RefFEltNS


/// @} // addtogroup FiniteElementGroup
