/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/SegmentP0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {


        __attribute__((unused)) const bool registered =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<SegmentP0>();


    } // namespace


    const std::string& SegmentP0::ShapeFunctionLabel()
    {
        static std::string ret("P0");
        return ret;
    }


    SegmentP0::~SegmentP0() = default;


} // namespace MoReFEM::RefFEltNS


/// @} // addtogroup FiniteElementGroup
