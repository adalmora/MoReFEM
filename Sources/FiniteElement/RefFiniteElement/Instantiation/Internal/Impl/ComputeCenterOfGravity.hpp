/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 14:25:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HPP_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hpp"


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    /*!
     * \brief Compute the center of gravity for a given geometric element from its vertices.
     *
     * This is used to give an approximate spatial position for a \a LocalNode.
     *
     * \param[in] vertex_list List of vertex
     * \param[in] vertex_on_interface_index_list List given by methods such as \a TopologyT::GetEdge(index) or
     * \a TopologyT::GetFace(index)
     *
     * \return Approximate position of the center of gravity of the geometric element considered.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT, class InterfaceContentT>
    LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& vertex_list,
                                       const InterfaceContentT& vertex_on_interface_index_list);


} // namespace MoReFEM::Internal::RefFEltNS::Impl


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hxx"


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HPP_
