/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 5 Nov 2014 14:25:32 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HXX_

// IWYU pragma: private, include "FiniteElement/RefFiniteElement/Instantiation/Internal/Impl/ComputeCenterOfGravity.hpp"


namespace MoReFEM::Internal::RefFEltNS::Impl
{


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT, class InterfaceContentT>
    LocalCoords ComputeCenterOfGravity(const std::vector<LocalCoords>& vertex_list,
                                       const InterfaceContentT& vertex_on_interface_index_list)
    {
        if constexpr (std::is_same_v<InterfaceContentT, std::false_type>)
        {
            assert(false
                   && "Should never be called in runtime in this case; just there to prevent compilation failure!");
            exit(EXIT_FAILURE);
        } else
        {
#ifndef NDEBUG
            const std::size_t Nvertex = TopologyT::Nvertex;

            assert(vertex_list.size() == Nvertex);
            assert(std::all_of(vertex_on_interface_index_list.cbegin(),
                               vertex_on_interface_index_list.cend(),
                               [](std::size_t index)
                               {
                                   return index < Nvertex;
                               }));
#endif // NDEBUG

            std::vector<LocalCoords> vertex_on_interface;
            vertex_on_interface.reserve(vertex_on_interface_index_list.size());

            for (std::size_t vertex_in_interface_index : vertex_on_interface_index_list)
                vertex_on_interface.push_back(vertex_list[vertex_in_interface_index]);

            return ComputeCenterOfGravity(vertex_on_interface);
        }
    }


} // namespace MoReFEM::Internal::RefFEltNS::Impl


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_IMPL_x_COMPUTE_CENTER_OF_GRAVITY_HXX_
