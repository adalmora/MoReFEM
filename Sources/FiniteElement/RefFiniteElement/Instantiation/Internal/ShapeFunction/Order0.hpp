/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 16:07:51 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HPP_

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::ShapeFunctionNS
{


    /*!
     * \brief Define the shape function and its derivate for order 0 (P0, Q0).
     */
    struct Order0
    {


        //! Return value of the order.
        enum { Order = 0 };


        /*!
         *
         * \copydoc doxygen_hide_shape_function
         *
         * For this specific case, method always returns 1. regardless of arguments values.
         */
        static double ShapeFunction(LocalNodeNS::index_type local_node_index, const LocalCoords& local_coords);


        /*!
         * \copydoc doxygen_hide_first_derivate_shape_function
         *
         * For this specific case, method always returns 0. regardless of arguments values.
         */
        static double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                 Advanced::ComponentNS::index_type component,
                                                 const LocalCoords& local_coords);
    };


} // namespace MoReFEM::Internal::ShapeFunctionNS


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_INTERNAL_x_SHAPE_FUNCTION_x_ORDER0_HPP_
