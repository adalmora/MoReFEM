//! \file
//
//
//  FwdForCpp.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 24/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_

#include <array>  // IWYU pragma: export
#include <iosfwd> // IWYU pragma: export
#include <vector> // IWYU pragma: export


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"                           // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_FWD_FOR_CPP_HPP_
