/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 30 Oct 2014 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/HexahedronQ2c.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {


        __attribute__((unused)) const bool registered =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__, __LINE__).Register<HexahedronQ2c>();


    } // namespace


    const std::string& HexahedronQ2c::ShapeFunctionLabel()
    {
        static std::string ret("Q2c");
        return ret;
    }


    HexahedronQ2c::~HexahedronQ2c() = default;


} // namespace MoReFEM::RefFEltNS


/// @} // addtogroup FiniteElementGroup
