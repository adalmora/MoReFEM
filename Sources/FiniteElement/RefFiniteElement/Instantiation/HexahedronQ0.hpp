/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 11 Oct 2020 14:35:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q0_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q0_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Topology/Hexahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp" // IWYU pragma: keep (IWYU indecisive here)

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for HexahedronQ0.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * RefGeomEltNS::TopologyNS::Hexahedron class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    class HexahedronQ0 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt<RefGeomEltNS::TopologyNS::Hexahedron,
                                                                               Internal::ShapeFunctionNS::Order0,
                                                                               InterfaceNS::Nature::none,
                                                                               1u>
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit HexahedronQ0() = default;

        //! Destructor.
        ~HexahedronQ0() override;

        //! \copydoc doxygen_hide_copy_constructor
        HexahedronQ0(const HexahedronQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        HexahedronQ0(HexahedronQ0&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        HexahedronQ0& operator=(const HexahedronQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        HexahedronQ0& operator=(HexahedronQ0&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_REF_FINITE_ELEMENT_x_INSTANTIATION_x_HEXAHEDRON_Q0_HPP_
