/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Dec 2013 12:31:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_

// IWYU pragma: private, include "FiniteElement/Unknown/Unknown.hpp"

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "FiniteElement/Unknown/UniqueId.hpp"
#include <type_traits> // IWYU pragma: keep
// IWYU pragma: no_forward_declare MoReFEM::Unknown

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::UnknownNS { enum class Nature; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    inline const std::string& Unknown::GetName() const noexcept
    {
        return name_;
    }


    inline bool operator!=(const Unknown& unknown1, const Unknown& unknown2)
    {
        return !(operator==(unknown1, unknown2));
    }


    inline UnknownNS::Nature Unknown::GetNature() const noexcept
    {
        return nature_;
    }


    template<class EnumT>
    constexpr UnknownNS::unique_id AsUnknownId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return UnknownNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_HXX_
