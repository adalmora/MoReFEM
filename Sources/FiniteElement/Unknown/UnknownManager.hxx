/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 27 Sep 2013 08:44:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/Unknown/UnknownManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    template<class IndexedSectionDescriptionT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT>
    void UnknownManager::Create(const IndexedSectionDescriptionT&, const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) name =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Name>(
                input_data, model_settings);

        decltype(auto) nature =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Nature>(
                input_data, model_settings);

        Create(::MoReFEM::UnknownNS::unique_id{ section_type::GetUniqueId() }, name, nature);
    }


    inline std::size_t UnknownManager::Nunknown() const noexcept
    {
        return unknown_list_.size();
    }


    inline const Unknown& UnknownManager::GetUnknown(UnknownNS::unique_id i) const noexcept
    {
        return *GetUnknownPtr(i);
    }


    inline const Unknown& UnknownManager::GetUnknown(const std::string& unknown_name) const
    {
        return *GetUnknownPtr(unknown_name);
    }


    inline const Unknown::vector_const_shared_ptr& UnknownManager::GetList() const noexcept
    {
        return unknown_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_UNKNOWN_x_UNKNOWN_MANAGER_HXX_
