/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Concept.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GodOfDof; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void GodOfDofManager ::Create(const IndexedSectionDescriptionT&, const ModelSettingsT&, const InputDataT&, const Wrappers::Mpi& mpi)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        const auto unique_id = MeshNS::unique_id{ section_type::GetUniqueId() };

        auto& mesh = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__).GetNonCstMesh(unique_id);

        Create(mpi, mesh);
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(MeshNS::unique_id unique_id) const
    {
        return *(GetGodOfDofPtr(unique_id));
    }


    inline GodOfDof& GodOfDofManager::GetNonCstGodOfDof(MeshNS::unique_id unique_id)
    {
        return const_cast<GodOfDof&>(GetGodOfDof(unique_id));
    }


    inline const GodOfDof& GodOfDofManager::GetGodOfDof(const Mesh& mesh) const
    {
        return GetGodOfDof(mesh.GetUniqueId());
    }


    inline auto GodOfDofManager::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    inline auto GodOfDofManager::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_GOD_OF_DOF_MANAGER_HXX_
