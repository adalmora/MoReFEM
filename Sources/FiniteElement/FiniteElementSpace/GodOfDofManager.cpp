/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 30 Mar 2015 11:30:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Mesh/Mesh.hpp"

#include "Utilities/Containers/UnorderedMap.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"


namespace MoReFEM
{


    GodOfDofManager::~GodOfDofManager() = default;


    void GodOfDofManager::Create(const Wrappers::Mpi& mpi, Mesh& mesh)
    {
        const auto unique_id = mesh.GetUniqueId();

        auto ptr = Internal::WrapShared(new GodOfDof(mpi, mesh));

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        decltype(auto) storage = GetNonCstStorage();

        auto insert_return_value = storage.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two god of dof objects can't share the same unique identifier! (namely "
                                + std::to_string(unique_id.Get()) + ").",
                            __FILE__,
                            __LINE__);
    }


    const std::string& GodOfDofManager::ClassName()
    {
        static std::string ret("GodOfDofManager");
        return ret;
    }


    GodOfDofManager::GodOfDofManager()
    {
        storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    GodOfDof::shared_ptr GodOfDofManager::GetGodOfDofPtr(MeshNS::unique_id unique_id) const
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        assert(it != storage.cend());
        assert(!(!(it->second)));

        return it->second;
    }


    void ClearGodOfDofTemporaryData()
    {
        const auto& storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        for (const auto& pair : storage)
        {
            auto& god_of_dof_ptr = pair.second;
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;
            god_of_dof.ClearTemporaryData();
        }
    }


    void GodOfDofManager::Clear()
    {
        GetNonCstStorage().clear();
        GodOfDof::ClearUniqueIdList();
        FEltSpace::ClearUniqueIdList();
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup
