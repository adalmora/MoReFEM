//! \file
//
//
//  CreateFEltSpaceList.hxx
//  MoReFEM
//
//  Created by sebastien on 29/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/UniqueId.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS
{


    template<::MoReFEM::Concept::InputDataType InputDataT, ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    std::map<::MoReFEM::MeshNS::unique_id, FEltSpace::vector_unique_ptr>
    CreateFEltSpaceList(const InputDataT& input_data, const ModelSettingsT& model_settings)
    {
        std::map<::MoReFEM::MeshNS::unique_id, FEltSpace::vector_unique_ptr> ret;

        auto create = [&ret, &model_settings, &input_data](const auto& indexed_section_description) -> void
        {
            using felt_space_section_type = typename std::decay_t<decltype(indexed_section_description)>::enclosing_section_type;

            static_assert(!std::is_same<felt_space_section_type, Advanced::InputDataNS::NoEnclosingSection>());

            const auto unique_id = ::MoReFEM::FEltSpaceNS::unique_id{ felt_space_section_type::GetUniqueId() };

            decltype(auto) god_of_dof_index =
                ::MoReFEM::MeshNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                    typename felt_space_section_type::GodOfDofIndex>(input_data, model_settings) };

            decltype(auto) domain_index =
                ::MoReFEM::DomainNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                    typename felt_space_section_type::DomainIndex>(input_data, model_settings) };

            auto&& extended_unknown_list =
                MoReFEM::Internal::FEltSpaceNS::ExtractExtendedUnknownList<felt_space_section_type>(input_data,
                                                                                                    model_settings);

            decltype(auto) god_of_dof_ptr =
                GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDofPtr(god_of_dof_index);

            decltype(auto) domain_manager = DomainManager::GetInstance(__FILE__, __LINE__);

            try
            {
                const auto& domain = domain_manager.GetDomain(domain_index, __FILE__, __LINE__);

                auto felt_space_ptr =
                    std::make_unique<FEltSpace>(god_of_dof_ptr, domain, unique_id, std::move(extended_unknown_list));

                ret[god_of_dof_index].emplace_back(std::move(felt_space_ptr));
            }
            catch (const Exception& e)
            {
                std::ostringstream oconv;
                oconv << "Ill-defined finite element space " << unique_id << ": " << e.GetRawMessage();
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }
        };


        // clang-format off
        using model_settings_tuple_iteration =
            Internal::InputDataNS::TupleIteration
            <
                typename ModelSettingsT::underlying_tuple_type,
                0ul
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<
            MoReFEM::Internal::InputDataNS::FEltSpaceNS::Tag>(model_settings.GetTuple(), input_data, create);

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_CREATE_F_ELT_SPACE_LIST_HXX_
