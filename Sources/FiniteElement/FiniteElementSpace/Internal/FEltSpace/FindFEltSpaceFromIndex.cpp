//! \file
//
//
//  FindFEltSpaceFromIndex.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/07/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#include <cassert>
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FindFEltSpaceFromIndex.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    const FEltSpace& FindFEltSpaceFromIndex(const ::MoReFEM::FEltSpaceNS::unique_id felt_space_unique_id,
                                            const char* invoking_file,
                                            int invoking_line)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(invoking_file, invoking_line);
        decltype(auto) god_of_dof_storage = god_of_dof_manager.GetStorage();

        for (const auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            const auto& god_of_dof = *god_of_dof_ptr;
            if (god_of_dof.IsFEltSpace(felt_space_unique_id))
                return god_of_dof.GetFEltSpace(felt_space_unique_id);
        }

        throw Exception("Couldn't find in any GodOfDof a FEltSpace with id = "
                            + std::to_string(felt_space_unique_id.Get()),
                        invoking_file,
                        invoking_line);
    }


} // namespace MoReFEM::Internal::FEltSpaceNS
