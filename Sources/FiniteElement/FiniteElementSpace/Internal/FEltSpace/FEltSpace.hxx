/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Internal/Convert.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    template<std::size_t UniqueIdT, ::MoReFEM::Concept::InputDataType InputDataT>
    Domain& ExtractDomain(const InputDataT& input_data)
    {
        using FEltSpaceNS = ::MoReFEM::InputDataNS::FEltSpace<UniqueIdT>;

        const auto domain_index =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename FEltSpaceNS::DomainIndex>::Value(input_data);

        return DomainManager::GetInstance(__FILE__, __LINE__).GetNonCstDomain(domain_index);
    }


    // clang-format off
    template
    <
        class FEltSectionT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    ExtendedUnknown::vector_const_shared_ptr ExtractExtendedUnknownList(const InputDataT& input_data,
                                                                        const ModelSettingsT& model_settings)
    {
        decltype(auto) unknown_name_list =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename FEltSectionT::UnknownList>(
                input_data, model_settings);

        decltype(auto) shape_function_list =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename FEltSectionT::ShapeFunctionList>(
                input_data, model_settings);

        decltype(auto) numbering_subset_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename FEltSectionT::NumberingSubsetList>(
                input_data, model_settings);

        auto numbering_subset_list =
            Internal::StrongTypeNS::Convert<::MoReFEM::NumberingSubsetNS::unique_id>(numbering_subset_list_as_int);

        constexpr auto unique_id = FEltSectionT::GetUniqueId();

        if (unknown_name_list.size() != numbering_subset_list.size())
            throw Exception("Error in input data file: the number of numbering subsets and the number "
                            "of unknowns in finite element space "
                                + std::to_string(unique_id)
                                + " should be "
                                  "the same.",
                            __FILE__,
                            __LINE__);

        if (unknown_name_list.size() != shape_function_list.size())
            throw Exception("Error in input data file: the number of numbering subsets and the number "
                            "of shape functions in finite element space "
                                + std::to_string(unique_id)
                                + " should be "
                                  "the same.",
                            __FILE__,
                            __LINE__);

        const auto& unknown_manager = UnknownManager::GetInstance(__FILE__, __LINE__);
        auto& numbering_subset_manager =
            Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);

        const auto size = unknown_name_list.size();

        ExtendedUnknown::vector_const_shared_ptr ret;

        for (std::size_t i = 0; i < size; ++i)
        {
            auto&& ptr = std::make_shared<ExtendedUnknown>(
                unknown_manager.GetUnknownPtr(unknown_name_list[i]),
                numbering_subset_manager.GetNumberingSubsetPtr(numbering_subset_list[i]),
                shape_function_list[i]);

            ret.emplace_back(std::move(ptr));
        }

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HXX_
