/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Mutex/Mutex.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Alias.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Find in a LocalFEltSpacePerRefLocalFEltSpace the \a RefLocalFEltSpace matching the \a RefGeomElt
     * which underlying index is I.
     *
     * \tparam I Underlying index beneath a \a GeometricEltEnum
     *
     * \param[in] container The container into which the search is performed.
     *
     * \return The \a RefLocalFEltSpace adequate for the given \a GeometricEltEnum if one is defined, nullptr
     * otherwise. The latter is not an error: it might just be for instance the search was performed for a
     * \a RefGeometricElt not present in the model currently run.
     *
     */
    template<std::size_t I>
    const Internal::RefFEltNS::RefLocalFEltSpace*
    FindRefLocalFEltSpace(const LocalFEltSpacePerRefLocalFEltSpace& container);


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceInternalStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
