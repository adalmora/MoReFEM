/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_ALIAS_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_ALIAS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief Provides for all the \a RefLocalFEltSpace the list of associated \a LocalFEltSpace.
     *
     * \internal <b><tt>[internal]</tt></b> This container behaves almost like a map, except that there
     * is no ordering relation on the keys (that's why a std::map was not used in the first place).
     * \endinternal
     */
    // clang-format off
    using LocalFEltSpacePerRefLocalFEltSpace =
    std::vector
    <
        std::pair<Internal::RefFEltNS::RefLocalFEltSpace::const_shared_ptr, LocalFEltSpace::per_geom_elt_index>
    >;
    // clang-format on


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_ALIAS_HPP_
