/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 14:02:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/FElt/FEltSpace.hpp"
#include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp" // IWYU pragma: export

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Return the domain that is associated to a given finite element space.
     *
     * \internal <b><tt>[internal]</tt></b> A factory function is used here due to the \a UniqueIdT template
     * parameter: C++ grammar doesn't allow an explicit template parameter in a non-template class constructor.
     * \endinternal
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \tparam UniqueIdT Unique id of the finite element space from which the domain is extracted.
     * This unique id is the one used in the input data file; it is for instance 5 for block tagged
     * FiniteElementSpace5.
     * \tparam InputDataT Type of \a input_data.
     *
     * \return The full fledged domain object. It must have been built beforehand by the Domain manager.
     */
    template<std::size_t UniqueIdT, ::MoReFEM::Concept::InputDataType InputDataT>
    Domain& ExtractDomain(const InputDataT& input_data);


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_F_ELT_SPACE_HPP_
