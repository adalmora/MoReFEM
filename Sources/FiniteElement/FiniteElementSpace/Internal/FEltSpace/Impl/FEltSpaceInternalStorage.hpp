/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp" // IWYU pragma: export
#include "Utilities/Mpi/Mpi.hpp"
#include "Utilities/Mutex/Mutex.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Alias.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    /*!
     * \brief Helper class that holds finite element and dof information about either a FEltSpace
     * or a couple FEltSpace/Domain.
     *
     * Should be used only within Internal::FEltSpaceNS::Storage.
     */
    class InternalStorage final : public ::MoReFEM::Crtp::Mutex<InternalStorage>,
                                  public ::MoReFEM::Crtp::CrtpMpi<InternalStorage>
    {

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] felt_list_per_ref_felt_space Finite element list per reference finite element space.
         * It's just stored in the current class; it is actually computed elsewhere.
         */
        explicit InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space);


        //! Destructor.
        ~InternalStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InternalStorage(const InternalStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InternalStorage(InternalStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InternalStorage& operator=(const InternalStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InternalStorage& operator=(InternalStorage&& rhs) = delete;

        ///@}

      public:
        //! Get the list of all finite element sort per reference felt space and local felt space.
        const LocalFEltSpacePerRefLocalFEltSpace& GetLocalFEltSpacePerRefLocalFEltSpace() const noexcept;

        //! Non constant access to the list of all finite element sort per reference felt space and local
        //! felt space.
        LocalFEltSpacePerRefLocalFEltSpace& GetNonCstFEltListPerRefLocalFEltSpace() noexcept;

        //! Whether there are finite elements in the storage.
        bool IsEmpty() const noexcept;


      private:
        //! List of all finite element sort per reference felt space and local felt space.
        LocalFEltSpacePerRefLocalFEltSpace felt_list_per_ref_felt_space_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_IMPL_x_F_ELT_SPACE_INTERNAL_STORAGE_HPP_
