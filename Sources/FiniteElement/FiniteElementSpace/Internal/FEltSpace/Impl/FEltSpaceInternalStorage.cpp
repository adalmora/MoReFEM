/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 Feb 2015 10:45:02 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/Impl/FEltSpaceInternalStorage.hpp"


namespace MoReFEM::Internal::FEltSpaceNS::Impl
{


    InternalStorage ::InternalStorage(const ::MoReFEM::Wrappers::Mpi& mpi,
                                      LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_ref_felt_space)
    : ::MoReFEM::Crtp::CrtpMpi<InternalStorage>(mpi),
      felt_list_per_ref_felt_space_(std::move(felt_list_per_ref_felt_space))
    { }


    bool InternalStorage::IsEmpty() const noexcept
    {
        return GetLocalFEltSpacePerRefLocalFEltSpace().empty();
    }


} // namespace MoReFEM::Internal::FEltSpaceNS::Impl


/// @} // addtogroup FiniteElementGroup
