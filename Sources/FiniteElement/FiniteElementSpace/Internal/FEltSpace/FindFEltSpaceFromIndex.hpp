//! \file
//
//
//  FindFEltSpaceFromIndex.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/07/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_FIND_F_ELT_SPACE_FROM_INDEX_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_FIND_F_ELT_SPACE_FROM_INDEX_HPP_

#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM {  class FEltSpace; };

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Find a \a FEltSpace given only its index.
     *
     * \internal This is really internal stuff: the intended  way to access a \a FEltSpace is to use the dedicated accessor in its \a GodOfDof. However,
     * it may happen in initialization phase that we need to find one without knowing directly its \a GodOfDof (for
     * instance when initializing
     * \a CoordsMarchingInterpolator); current function may be used in this case. It is really shallow: it iterates in all \a GodOfDof until the \a FEltSpace is
     * found; if not an exception is thrown.
     *
     * \param[in] felt_space_unique_id \a Unique id of the sought \a FEltSpace.
     * \copydoc doxygen_hide_invoking_file_and_line
     *
     * \return Constant reference to the \a FEltSpace. If none matches, an exception is thrown.
     */
    const FEltSpace& FindFEltSpaceFromIndex(::MoReFEM::FEltSpaceNS::unique_id felt_space_unique_id,
                                            const char* invoking_file,
                                            int invoking_line);


} // namespace MoReFEM::Internal::FEltSpaceNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_F_ELT_SPACE_x_FIND_F_ELT_SPACE_FROM_INDEX_HPP_
