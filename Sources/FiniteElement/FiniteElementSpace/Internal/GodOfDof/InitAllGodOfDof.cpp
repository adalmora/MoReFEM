/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
 // Copyright (c) Inria. All rights reserved.

 // \{
 */

// IWYU pragma: no_include <__tree>
#include <cassert>
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    namespace // anonymous
    {


        using mesh_output_directory_storage_type =
            std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>;


    } // namespace


    void InitAllGodOfDof::StandardInit()
    {
        InitNodeBearers();

        ComputeGhostNodeBearerListForCoordsMatching();

        Reduce();

        CreateNodesAndDofs();
    }


    void InitAllGodOfDof::InitOutputDirectories(mesh_output_directory_storage_type&& mesh_output_directory_storage)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();
        assert(mesh_output_directory_storage.size() == god_of_dof_storage.size());

        for (auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;

            const auto it = mesh_output_directory_storage.find(god_of_dof_id);
            assert(it != mesh_output_directory_storage.cend());
            const auto& mesh_directory_ptr = it->second;

            god_of_dof.InitOutputDirectories(*mesh_directory_ptr);
        }
    }


    void
    InitAllGodOfDof::FinalizeInitialization(const Internal::Parallelism* parallelism,
                                            DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        for (auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->FinalizeInitialization(do_consider_proc_wise_local_2_global, parallelism);
        }
    }


    void InitAllGodOfDof::InitNodeBearers()
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        for (auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            match_interface_node_bearer_per_god_of_dof_.emplace(
                std::make_pair(god_of_dof_ptr, god_of_dof_ptr->InitNodeBearers()));
        }
    }


    void InitAllGodOfDof ::ComputeGhostNodeBearerListForCoordsMatching()
    {
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        decltype(auto) coords_matching_manager =
            Internal::MeshNS::CoordsMatchingManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) coords_matching_list = coords_matching_manager.GetList();

        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));

            for (const auto& ptr : coords_matching_list)
            {
                assert(!(!ptr));

                if (ptr->GetSourceMeshId() == god_of_dof_ptr->GetUniqueId())
                    god_of_dof_ptr->ComputeGhostNodeBearerListForCoordsMatching(*ptr, match_interface_node_bearer);
            }
        }
    }


    void InitAllGodOfDof::Reduce()
    {
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->Reduce(match_interface_node_bearer);
        }
    }


    void InitAllGodOfDof::CreateNodesAndDofs()
    {
        decltype(auto) match_interface_node_bearer_per_god_of_dof = GetNonCstMatchInterfaceNodeBearerPerGodOfDof();

        for (auto& [god_of_dof_ptr, match_interface_node_bearer] : match_interface_node_bearer_per_god_of_dof)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->CreateNodesAndDofs(match_interface_node_bearer);
        }
    }


} // namespace MoReFEM::Internal::GodOfDofNS
