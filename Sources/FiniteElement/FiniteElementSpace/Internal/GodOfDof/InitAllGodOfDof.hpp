/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <optional>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp" // IWYU pragma: keep
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Mesh/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Partition/MatchInterfaceNodeBearer.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal { class Parallelism; }
namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    /*!
     * \brief Facility used to initialize in parallel several \a GodOfDof.
     *
     * Some steps need to be done for all \a GodOfDof before the reduction to processor-wise data occur  (typically for
     * `FromCoordsMatching` interpolator: we need to know before it which \a NodeBearer should be kept to make this
     * interpolator work).
     *
     * So what is done here is that we initialize here all `GodOfDof` layer by layer.
     *
     * This is done through a friendship granted in `GodOfDof`class.
     */

    class InitAllGodOfDof
    {

      public:
        /*!
         * \brief Create the finite element spaces and init the god of dofs with them.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         *
         * \copydetails doxygen_hide_do_consider_processor_wise_local_2_global
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \param[in] output_directory_per_mesh_index Key is the unique id of the \a Mesh (same
         * as the one for the \a GodOfDof), value the path to the associated output directory (which should have
         * already been created by \a Model class).
         */
        //  clang-format off
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
        //  clang-format on
        InitAllGodOfDof(const MoReFEMDataT& morefem_data,
                        const ModelSettingsT& model_settings,
                        DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global,
                        std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                            output_directory_per_mesh_index);

      private:
        /*!
         * \brief Init all output directories.
         *
         * \param[in] output_directory_per_mesh_index Key is the unique id of the \a Mesh (same
         * as the one for the \a GodOfDof), value the path to the associated output directory (which should have
         * already been created by \a Model class).
         */
        void InitOutputDirectories(
            std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
                output_directory_per_mesh_index);


        /*!
         * \brief Create the finite element spaces and init the god of dofs with them.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
        void InitFEltSpaceList(const MoReFEMDataT& morefem_data, const ModelSettingsT& model_settings);

        /*!
         * \brief Standard initialization of all \a GodOfDof (by opposition to initialization from preprocessed data).
         *
         */
        void StandardInit();


        /*!
         * \brief Init all \a GodOfDof from preprocessed data.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        void InitFromPreprocessedData(const MoReFEMDataT& morefem_data);

        /*!
         * \brief Init all \a NodeBearers in the case of a standard initialization (before repartition between ranks in parallel case)
         *
         * This method sets the helper attribute \a  match_interface_node_bearer_per_god_of_dof_.
         */
        void InitNodeBearers();

        /*!
         * \brief Reduce all \a GodOfDof to processor-wise only data.
         *
         */
        void Reduce();

        /*!
         * \brief Create processor-wise and ghost \a Node and \a Dof for all \a GodOfDof.
         *
         */
        void CreateNodesAndDofs();


        /*!
         * \brief Compute the list of \a NodeBearer that should be kept as ghost for the sake of \a CoordsMatchingInterpolator.
         */
        void ComputeGhostNodeBearerListForCoordsMatching();


        /*!
         * \brief Finalize initialization for all \a GodOfDof.
         *
         * \copydoc doxygen_hide_do_consider_processor_wise_local_2_global
         * \param[in] parallelism Object which holds the parallelism strategy to use. or some of them, additional
         * information will be written on disk.
         *
         * This is the last step to be called in \a GodOfDof initialization process.
         */
        void FinalizeInitialization(const Internal::Parallelism* parallelism,
                                    DoConsiderProcessorWiseLocal2Global do_consider_processor_wise_local_2_global);


        /*!
         * \brief Helper function which computes the \a CoordsMatching object if required.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * If an operator \a FromCoordsMatching is used in the model, additional \a Coords objects have to be
         * kept as ghost in both \a GodOfDof involved; they are computed here to be transmitted for the
         * \a GodOfDof reduction process.
         *
         * \return std::nullopt if no \a CoordsMatching involved, or the \a CoordsMatching to be used otherwise.
         */
        template<::MoReFEM::Concept::InputDataType InputDataT>
        std::optional<::MoReFEM::MeshNS::InterpolationNS::CoordsMatching>
        ComputeCoordsMatchingInformation(const InputDataT& input_data);

      private:
        //! Alias to helper attribute type.
        // clang-format off
        using helper_type = Utilities::PointerComparison::Map
        <
            GodOfDof::shared_ptr,
            Internal::FEltSpaceNS::MatchInterfaceNodeBearer
        >;
        // clang-format on


        //! Accessor to helper \a match_interface_node_bearer_per_god_of_dof_.
        helper_type& GetNonCstMatchInterfaceNodeBearerPerGodOfDof() noexcept;

      private:
        /*!
         * \brief For each \a GodOfDof, the associated helper \a Internal::FEltSpaceNS::MatchInterfaceNodeBearer object.
         *
         */
        helper_type match_interface_node_bearer_per_god_of_dof_;
    };


} // namespace MoReFEM::Internal::GodOfDofNS


/// @} // addtogroup ModelGroup


#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HPP_
