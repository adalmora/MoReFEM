/*!
 */


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

#include <cassert>
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"

#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp" // IWYU pragma: keep
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Mesh/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    //  clang-format off
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    //  clang-format on
    InitAllGodOfDof::InitAllGodOfDof(
        const MoReFEMDataT& morefem_data,
        const ModelSettingsT& model_settings,
        DoConsiderProcessorWiseLocal2Global do_consider_proc_wise_local_2_global,
        std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>&&
            mesh_output_directory_storage)
    {
        InitFEltSpaceList(morefem_data, model_settings);

        InitOutputDirectories(std::move(mesh_output_directory_storage));

        decltype(auto) parallelism_strategy = Advanced::ExtractParallelismStrategy(morefem_data);

        switch (parallelism_strategy)
        {
        case Advanced::parallelism_strategy::parallel:
        case Advanced::parallelism_strategy::precompute:
        case Advanced::parallelism_strategy::none:
        case Advanced::parallelism_strategy::parallel_no_write:
        {
            StandardInit();
            break;
        }
        case Advanced::parallelism_strategy::run_from_preprocessed:
        {
            InitFromPreprocessedData(morefem_data);
            break;
        }
        }

        FinalizeInitialization(morefem_data.GetParallelismPtr(), do_consider_proc_wise_local_2_global);
    }


    //  clang-format off
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, ::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    //  clang-format on
    void InitAllGodOfDof::InitFEltSpaceList(const MoReFEMDataT& morefem_data, const ModelSettingsT& model_settings)
    {
        decltype(auto) input_data = morefem_data.GetInputData();
        auto felt_space_list_per_god_of_dof_index = FEltSpaceNS::CreateFEltSpaceList(input_data, model_settings);

        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        assert(felt_space_list_per_god_of_dof_index.size() == god_of_dof_storage.size()
               && "It likely means you forgot the 'SetDescription' call for a finite element space "
                  "section in ModelSettings::Init(). It should however have been identified earlier...");

        for (auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            auto& god_of_dof = *god_of_dof_ptr;

            auto it = felt_space_list_per_god_of_dof_index.find(god_of_dof_id);
            assert(it != felt_space_list_per_god_of_dof_index.cend());

            god_of_dof.SetFEltSpaceList(std::move(it->second));
        }
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitAllGodOfDof::InitFromPreprocessedData(const MoReFEMDataT& morefem_data)
    {
        decltype(auto) god_of_dof_storage = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetStorage();

        for (auto& [god_of_dof_id, god_of_dof_ptr] : god_of_dof_storage)
        {
            assert(!(!god_of_dof_ptr));
            god_of_dof_ptr->InitFromPreprocessedData(morefem_data);
        }
    }


    inline auto InitAllGodOfDof ::GetNonCstMatchInterfaceNodeBearerPerGodOfDof() noexcept -> helper_type&
    {
        return match_interface_node_bearer_per_god_of_dof_;
    }

} // namespace MoReFEM::Internal::GodOfDofNS


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_GOD_OF_DOF_x_INIT_ALL_GOD_OF_DOF_HXX_
