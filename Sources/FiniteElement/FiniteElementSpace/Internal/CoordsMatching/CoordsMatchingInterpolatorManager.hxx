/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp"

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/NumberingSubset/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void CoordsMatchingInterpolatorManager ::Create(const IndexedSectionDescriptionT&,
                                                    const ModelSettingsT& model_settings,
                                                    const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        const auto source_felt_space_unique_id =
            ::MoReFEM::FEltSpaceNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                typename section_type::SourceFEltSpaceIndex>(input_data, model_settings) };
        const auto target_felt_space_unique_id =
            ::MoReFEM::FEltSpaceNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                typename section_type::TargetFEltSpaceIndex>(input_data, model_settings) };

        const auto source_numbering_subset_index =
            ::MoReFEM::NumberingSubsetNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                typename section_type::SourceNumberingSubsetIndex>(input_data, model_settings) };
        const auto target_numbering_subset_index =
            ::MoReFEM::NumberingSubsetNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<
                typename section_type::TargetNumberingSubsetIndex>(input_data, model_settings) };


        Create(section_type::GetUniqueId(),
               source_felt_space_unique_id,
               source_numbering_subset_index,
               target_felt_space_unique_id,
               target_numbering_subset_index);
    }


    inline const auto& CoordsMatchingInterpolatorManager::GetStorage() const noexcept
    {
        return list_;
    }


    inline auto& CoordsMatchingInterpolatorManager::GetNonCstStorage() noexcept
    {
        return list_;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HXX_
