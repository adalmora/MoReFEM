/*!
 //
 // \file
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */


#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/Exceptions/Exception.hpp"


namespace MoReFEM::Internal::FromCoordsMatchingNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


} // namespace MoReFEM::Internal::FromCoordsMatchingNS


/// @} // addtogroup FiniteElementGroup
