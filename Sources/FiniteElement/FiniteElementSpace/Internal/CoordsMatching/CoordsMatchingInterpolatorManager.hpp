/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 15:36:26 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Interpolator/CoordsMatchingInterpolator.hpp"
#include "Core/NumberingSubset/UniqueId.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"
#include "FiniteElement/FiniteElementSpace/UniqueId.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief This class is used to create and retrieve \a CoordsMatchingInterpolator
     * objects.
     *
     * CoordsMatchingInterpolator objects get private constructor and can only be created
     * through this class.
     */
    class CoordsMatchingInterpolatorManager : public Utilities::Singleton<CoordsMatchingInterpolatorManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::Tag;

      public:
        /*!
         * \brief Create a \a CoordsMatchingInterpolator object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description, const ModelSettingsT& model_settings, const InputDataT& input_data);

        //! Fetch the object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{CoordsMatchingInterpolator}
        const CoordsMatchingInterpolator& GetCoordsMatchingInterpolator(std::size_t unique_id) const;

      private:
        //! Access to the storage.
        const auto& GetStorage() const noexcept;

        //! Access to the storage.
        auto& GetNonCstStorage() noexcept;

        /*!
         * \brief Create from a finite element space and a numbering subset.
         *
         * \param[in] unique_id Unique identifier to give to the \a
         * CoordsMatchingInterpolator object being created.
         * \param[in] source_felt_space Source \a FEltSpace.
         * \param[in] source_numbering_subset Source \a NumberingSubset .
         * \param[in] target_felt_space Target \a FEltSpace.
         * \param[in] target_numbering_subset Target \a NumberingSubset .
         */
        void Create(std::size_t unique_id,
                    ::MoReFEM::FEltSpaceNS::unique_id source_felt_space,
                    ::MoReFEM::NumberingSubsetNS::unique_id source_numbering_subset,
                    ::MoReFEM::FEltSpaceNS::unique_id target_felt_space,
                    ::MoReFEM::NumberingSubsetNS::unique_id target_numbering_subset);


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         */
        CoordsMatchingInterpolatorManager() = default;

        //! Destructor.
        virtual ~CoordsMatchingInterpolatorManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CoordsMatchingInterpolatorManager>;
        ///@}


      private:
        //! Store the \a CoordsMatchingInterpolatorManager per their unique id.
        //! \internal The nature of the storage is for conveniency: we could have stored the objects in a vector
        //! as the key is also what is returned if you use \a GetUniqueId() method on the second member of the
        //! pair.
        std::unordered_map<std::size_t, CoordsMatchingInterpolator::const_unique_ptr> list_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_COORDS_MATCHING_x_COORDS_MATCHING_INTERPOLATOR_MANAGER_HPP_
