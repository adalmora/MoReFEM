/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXCEPTIONS_x_MOVEMESH_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXCEPTIONS_x_MOVEMESH_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }
namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ExceptionsNS::Movemesh
{


    //! Generic exception thrown for movemesh related operations.
    class Exception : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Specific movemesh exception.
    class NumberOfUnknowns : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \param[in] Nmatching_unknown Number of matching unknowns found.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit NumberOfUnknowns(const FEltSpace& felt_space,
                                  const NumberingSubset& numbering_subset,
                                  long Nmatching_unknown,
                                  const char* invoking_file,
                                  int invoking_line);

        //! Destructor
        virtual ~NumberOfUnknowns() override;
    };


    //! Specific movemesh exception.
    class ScalarUnknown : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit ScalarUnknown(const FEltSpace& felt_space,
                               const NumberingSubset& numbering_subset,
                               const char* invoking_file,
                               int invoking_line);

        //! Destructor
        virtual ~ScalarUnknown() override;
    };


    //! Specific movemesh exception.
    class NoMovemeshData : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] felt_space \a FEltSpace involved.
         * \param[in] numbering_subset \a NumberingSubset involved.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit NoMovemeshData(const FEltSpace& felt_space,
                                const NumberingSubset& numbering_subset,
                                const char* invoking_file,
                                int invoking_line);

        //! Destructor
        virtual ~NoMovemeshData() override;
    };


} // namespace MoReFEM::Internal::ExceptionsNS::Movemesh


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_EXCEPTIONS_x_MOVEMESH_HPP_
