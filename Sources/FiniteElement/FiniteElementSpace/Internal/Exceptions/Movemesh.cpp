/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr>
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */


#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/Exceptions/Movemesh.hpp"


namespace MoReFEM::Internal::ExceptionsNS::Movemesh
{


    namespace // anonymous
    {


        std::string NumberOfUnknownsMsg(const FEltSpace& felt_space,
                                        const NumberingSubset& numbering_subset,
                                        long Nmatching_unknown);

        std::string ScalarUnknownMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset);

        std::string NoMovemeshDataMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset);


    } // namespace


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : ::MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    NumberOfUnknowns::~NumberOfUnknowns() = default;

    NumberOfUnknowns::NumberOfUnknowns(const FEltSpace& felt_space,
                                       const NumberingSubset& numbering_subset,
                                       long Nmatching_unknown,
                                       const char* invoking_file,
                                       int invoking_line)
    : Exception(NumberOfUnknownsMsg(felt_space, numbering_subset, Nmatching_unknown), invoking_file, invoking_line)
    { }


    ScalarUnknown::~ScalarUnknown() = default;

    ScalarUnknown::ScalarUnknown(const FEltSpace& felt_space,
                                 const NumberingSubset& numbering_subset,
                                 const char* invoking_file,
                                 int invoking_line)
    : Exception(ScalarUnknownMsg(felt_space, numbering_subset), invoking_file, invoking_line)
    { }


    NoMovemeshData::~NoMovemeshData() = default;

    NoMovemeshData::NoMovemeshData(const FEltSpace& felt_space,
                                   const NumberingSubset& numbering_subset,
                                   const char* invoking_file,
                                   int invoking_line)
    : Exception(NoMovemeshDataMsg(felt_space, numbering_subset), invoking_file, invoking_line)
    { }


    namespace // anonymous
    {


        std::string NumberOfUnknownsMsg(const FEltSpace& felt_space,
                                        const NumberingSubset& numbering_subset,
                                        long Nmatching_unknown)
        {
            std::ostringstream oconv;
            oconv << "Numbering subset " << numbering_subset.GetUniqueId()
                  << " in finite "
                     "element space "
                  << felt_space.GetUniqueId()
                  << " should cover exactly one (vectorial) "
                     "unknown; "
                  << Nmatching_unknown
                  << " unknowns were found in it. Your Lua "
                     "input file is likely ill-constructed.";

            return oconv.str();
        }


        std::string ScalarUnknownMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset)
        {
            std::ostringstream oconv;

            oconv << "The unknown in numbering subset " << numbering_subset.GetUniqueId() << " in finite element space "
                  << felt_space.GetUniqueId()
                  << " should be vectorial (as "
                     "it is expected to be a displacement) but was declared otherwise. Your Lua "
                     "input file is likely ill-constructed.";
            return oconv.str();
        }


        std::string NoMovemeshDataMsg(const FEltSpace& felt_space, const NumberingSubset& numbering_subset)
        {
            std::ostringstream oconv;
            oconv << "You called 'FEltSpace::MoveMesh()' for finite element space " << felt_space.GetUniqueId()
                  << " with a vector which numbering subset is " << numbering_subset.GetUniqueId()
                  << ". No movemesh data were found for "
                     "this one; make sure the option 'do_move_mesh' is set to true in the input "
                     "lua file for this numbering subset (if it already is please submit a bug report)";
            return oconv.str();
        }


    } // namespace


} // namespace MoReFEM::Internal::ExceptionsNS::Movemesh


/// @} // addtogroup FiniteElementGroup
