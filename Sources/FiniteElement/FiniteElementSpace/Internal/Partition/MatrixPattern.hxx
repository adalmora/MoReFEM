/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Apr 2015 12:13:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATRIX_PATTERN_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATRIX_PATTERN_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hpp"

#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    inline const NumberingSubset& MatrixPattern::GetRowNumberingSubset() const
    {
        assert(!(!row_numbering_subset_));
        return *row_numbering_subset_;
    }


    inline const NumberingSubset& MatrixPattern::GetColumnNumberingSubset() const
    {
        assert(!(!column_numbering_subset_));
        return *column_numbering_subset_;
    }


    inline const ::MoReFEM::Wrappers::Petsc::MatrixPattern& MatrixPattern::GetPattern() const
    {
        assert(!(!matrix_pattern_));
        return *matrix_pattern_;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_PARTITION_x_MATRIX_PATTERN_HXX_
