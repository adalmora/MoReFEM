//! \file
//
//
//  ExtractMpi.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 16/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef NDEBUG

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/ExtractMpi.hpp"


namespace MoReFEM::Internal
{


    const ::MoReFEM::Wrappers::Mpi& ExtractMpi(const GeometricElt& geom_elt)
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(geom_elt.GetMeshIdentifier());

        return god_of_dof.GetMpi();
    }


} // namespace MoReFEM::Internal

#endif // NDEBUG
