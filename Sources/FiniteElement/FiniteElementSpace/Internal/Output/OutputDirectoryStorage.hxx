//! \file
//
//
//  OutputDirectoryStorage.hxx
//  MoReFEM
//
//  Created by sebastien on 19/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <unordered_map>

#include "Core/NumberingSubset/UniqueId.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    inline const ::MoReFEM::FilesystemNS::Directory& OutputDirectoryStorage::GetOutputDirectory() const noexcept
    {
        assert(!(!output_directory_));
        return *output_directory_;
    }


    inline const std::unordered_map<::MoReFEM::NumberingSubsetNS::unique_id, ::MoReFEM::FilesystemNS::Directory>&
    OutputDirectoryStorage::GetOutputDirectoryPerNumberingSubset() const noexcept
    {
        return output_directory_per_numbering_subset_;
    }


} // namespace MoReFEM::Internal::GodOfDofNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_OUTPUT_DIRECTORY_STORAGE_HXX_
