//! \file
//
//
//  WritePrepartitionedData.hxx
//  MoReFEM
//
//  Created by sebastien on 04/05/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HXX_

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Output/WritePrepartitionedData.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


} // namespace MoReFEM::Internal::GodOfDofNS


#endif // MOREFEM_x_FINITE_ELEMENT_x_FINITE_ELEMENT_SPACE_x_INTERNAL_x_OUTPUT_x_WRITE_PREPARTITIONED_DATA_HXX_
