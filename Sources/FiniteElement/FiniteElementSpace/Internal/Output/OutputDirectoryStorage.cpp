//! \file
//
//
//  OutputDirectoryStorage.cpp
//  MoReFEM
//
//  Created by sebastien on 19/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <cassert>
#include <filesystem>
#include <memory>
#include <optional>
#include <regex>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/Output/OutputDirectoryStorage.hpp"


namespace MoReFEM::Internal::GodOfDofNS
{


    OutputDirectoryStorage::OutputDirectoryStorage(
        const ::MoReFEM::FilesystemNS::Directory& output_directory,
        const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
        wildcard_for_rank is_wildcard)
    {
        if (is_wildcard == wildcard_for_rank::yes)
        {
            std::regex re("Rank_[0-9]+");

            auto string = output_directory.GetDirectoryEntry().path().native();
            string = std::regex_replace(string, re, "Rank_*");

            output_directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                std::filesystem::path(string), ::MoReFEM::FilesystemNS::behaviour::ignore);
        } else
        {
            if (!output_directory.DoExist())
            {
                std::ostringstream oconv;
                oconv << "Directory " << output_directory << " is assumed to exist before this constructor!";
                throw Exception(oconv.str(), __FILE__, __LINE__);
            }

            output_directory_ = std::make_unique<::MoReFEM::FilesystemNS::Directory>(output_directory);
            output_directory_->SetBehaviour(::MoReFEM::FilesystemNS::behaviour::ignore, __FILE__, __LINE__);
        }

        output_directory_->ActOnFilesystem(__FILE__, __LINE__);

        std::ostringstream oconv;
        output_directory_per_numbering_subset_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        for (const auto& numbering_subset_ptr : numbering_subset_list)
        {
            assert(!(!numbering_subset_ptr));
            const auto& numbering_subset = *numbering_subset_ptr;
            const auto numbering_subset_id = numbering_subset.GetUniqueId();

            oconv.str("");
            oconv << "NumberingSubset_" << numbering_subset_id;

            auto subdirectory = ::MoReFEM::FilesystemNS::Directory(
                GetOutputDirectory(), oconv.str(), ::MoReFEM::FilesystemNS::behaviour::create);

            auto check = output_directory_per_numbering_subset_.insert(
                std::make_pair(numbering_subset_id, std::move(subdirectory)));
            assert(check.second);
            static_cast<void>(check);
        }
    }


    const ::MoReFEM::FilesystemNS::Directory&
    OutputDirectoryStorage ::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        const auto id = numbering_subset.GetUniqueId();

        decltype(auto) output_directory_per_numbering_subset = GetOutputDirectoryPerNumberingSubset();

        const auto it = output_directory_per_numbering_subset.find(id);

        assert(it != output_directory_per_numbering_subset.cend()
               && "All relevant numbering subsets should have been loaded into the map.");

        return it->second;
    }


} // namespace MoReFEM::Internal::GodOfDofNS
