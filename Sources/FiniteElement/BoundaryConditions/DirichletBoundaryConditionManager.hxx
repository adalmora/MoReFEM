/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 5 Apr 2016 13:48:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_

// IWYU pragma: private, include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Domain/UniqueId.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void DirichletBoundaryConditionManager::Create(const IndexedSectionDescriptionT&,
                                                   const ModelSettingsT& model_settings,
                                                   const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) domain_id = DomainNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::DomainIndex>(
                input_data, model_settings)
        };
        decltype(auto) unknown_name =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::UnknownName>(
                input_data, model_settings);
        decltype(auto) component =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Component>(
                input_data, model_settings);
        decltype(auto) value_per_component =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Values>(
                input_data, model_settings);
        decltype(auto) is_mutable =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::IsMutable>(
                input_data, model_settings);

        const auto& domain = DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(domain_id, __FILE__, __LINE__);

        const auto& unknown = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknown(unknown_name);

        Create(BoundaryConditionNS::unique_id{ section_type::GetUniqueId() },
               domain,
               unknown,
               value_per_component,
               component,
               is_mutable);
    }


    inline std::size_t DirichletBoundaryConditionManager::NboundaryCondition() const noexcept
    {
        return boundary_condition_list_.size();
    }


    inline const DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetDirichletBoundaryCondition(BoundaryConditionNS::unique_id id,
                                                                      const char* invoking_file,
                                                                      int invoking_line) const
    {
        return *GetDirichletBoundaryConditionPtr(id, invoking_file, invoking_line);
    }


    inline DirichletBoundaryCondition&
    DirichletBoundaryConditionManager ::GetNonCstDirichletBoundaryCondition(BoundaryConditionNS::unique_id id,
                                                                            const char* invoking_file,
                                                                            int invoking_line)
    {
        return const_cast<DirichletBoundaryCondition&>(GetDirichletBoundaryCondition(id, invoking_file, invoking_line));
    }


    inline const DirichletBoundaryCondition::vector_shared_ptr&
    DirichletBoundaryConditionManager ::GetList() const noexcept
    {
        return boundary_condition_list_;
    }


} // namespace MoReFEM


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_DIRICHLET_BOUNDARY_CONDITION_MANAGER_HXX_
