/*!
 //
 // \file
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */

#include <sstream>

#include "FiniteElement/BoundaryConditions/Exceptions/Exception.hpp"
#include "FiniteElement/BoundaryConditions/UniqueId.hpp"


namespace // anonymous
{

    std::string UnknownBoundaryConditionMsg(const std::string& name);

    std::string UnknownBoundaryConditionMsg(MoReFEM::BoundaryConditionNS::unique_id id);


} // namespace


namespace MoReFEM::ExceptionNS::BoundaryConditionNS
{


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    UnknownBoundaryCondition::~UnknownBoundaryCondition() = default;


    UnknownBoundaryCondition::UnknownBoundaryCondition(const std::string& name,
                                                       const char* invoking_file,
                                                       int invoking_line)
    : Exception(UnknownBoundaryConditionMsg(name), invoking_file, invoking_line)
    { }


    UnknownBoundaryCondition::UnknownBoundaryCondition(::MoReFEM::BoundaryConditionNS::unique_id id,
                                                       const char* invoking_file,
                                                       int invoking_line)
    : Exception(UnknownBoundaryConditionMsg(id), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::BoundaryConditionNS


namespace // anonymous
{


    std::string UnknownBoundaryConditionMsg(const std::string& name)
    {
        std::ostringstream oconv;
        oconv << "Sought boundary condition which name is '" << name
              << "' was not found in the manager. "
                 "Please check the name provided in the Lua input file match the expected one in your model.";
        return oconv.str();
    }


    std::string UnknownBoundaryConditionMsg(MoReFEM::BoundaryConditionNS::unique_id id)
    {
        std::ostringstream oconv;
        oconv
            << "Sought boundary condition which id underlying numerical value is '" << id
            << "' was not found  "
               "in the manager. Please check the way the model is written (there is probably an issue in which the "
               "model "
               "was written but a mere assert would not convey enough informations - please contact the author of the "
               "model you're using if you need assistance).";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup FiniteElementGroup
