/*!
 //
 // \file
 //
 // \ingroup FiniteElementGroup
 // \addtogroup FiniteElementGroup
 // \{
 */


#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_EXCEPTIONS_x_EXCEPTION_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_EXCEPTIONS_x_EXCEPTION_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/UniqueId.hpp"


namespace MoReFEM::ExceptionNS::BoundaryConditionNS
{


    //! Generic class for BoundaryCondition exceptions (useful in some tests).
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! When a sought \a BoundaryCondition is not known
    class UnknownBoundaryCondition : public Exception
    {
      public:
        /*!
         * \brief Constructor when boundary condition was sought through its name.
         *
         * \param[in] name Name of the boundary condition
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit UnknownBoundaryCondition(const std::string& name, const char* invoking_file, int invoking_line);

        /*!
         * \brief Constructor when boundary condition was sought through its identifier..
         *
         * \param[in] id Identfier of the sought boundary condition.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit UnknownBoundaryCondition(::MoReFEM::BoundaryConditionNS::unique_id id,
                                          const char* invoking_file,
                                          int invoking_line);

        //! Destructor
        virtual ~UnknownBoundaryCondition() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnknownBoundaryCondition(const UnknownBoundaryCondition& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnknownBoundaryCondition(UnknownBoundaryCondition&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnknownBoundaryCondition& operator=(const UnknownBoundaryCondition& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnknownBoundaryCondition& operator=(UnknownBoundaryCondition&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::BoundaryConditionNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_EXCEPTIONS_x_EXCEPTION_HPP_
