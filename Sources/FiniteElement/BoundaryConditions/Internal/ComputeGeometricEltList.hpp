//! \file
//
//
//  ComputeGeometricEltList.hpp
//  MoReFEM
//
//  Created by sebastien on 10/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HPP_
#define MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HPP_

#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class DirichletBoundaryCondition; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief Define all \a GeometricElt involved within the given \a boundary_condition.
     *
     * \internal This function is expected to be called on a standard vanilla parallel run BEFORE the reduction
     * occurred.
     *
     * \param[in] mesh \a Mesh for which the involved \a GeometricElt are sought.
     * \param[in] boundary_condition The \a DirichletBoundaryCondition which related \a GeometricElt we seek.
     *
     * \return List of all the relevant \a GeometricElt.
     *
     */
    GeometricElt::vector_shared_ptr ComputeGeometricEltList(const Mesh& mesh,
                                                            const DirichletBoundaryCondition& boundary_condition);


} // namespace MoReFEM::Internal::BoundaryConditionNS


#include "FiniteElement/BoundaryConditions/Internal/ComputeGeometricEltList.hxx" // IWYU pragma: export


#endif // MOREFEM_x_FINITE_ELEMENT_x_BOUNDARY_CONDITIONS_x_INTERNAL_x_COMPUTE_GEOMETRIC_ELT_LIST_HPP_
