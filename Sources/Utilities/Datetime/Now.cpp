//! \file
//
//
//  Now.cpp
//  MoReFEM
//
//  Created by sebastien on 17/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#include <array>
#include <ctime>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "Utilities/Datetime/Now.hpp"


namespace MoReFEM::Utilities
{


    std::string Now(const Wrappers::Mpi& mpi)
    {
        struct tm* timeinfo;

        constexpr auto buffer_size = 20ul;

        std::array<char, buffer_size> buffer;

        time_t rawtime{};

        if (mpi.IsRootProcessor())
            time(&rawtime);

        mpi.Broadcast(rawtime);

        timeinfo = localtime(&rawtime);

        strftime(buffer.data(), buffer_size, "%Y-%m-%d_%H:%M:%S", timeinfo);

        return std::string(buffer.data());
    }


} // namespace MoReFEM::Utilities
