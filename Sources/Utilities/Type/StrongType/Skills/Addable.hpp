//! \file
//
//
//  Addable.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HPP_


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType operator+ is enabled for the \a
     * StrongType.
     */
    template<typename StrongTypeT>
    struct Addable
    {

        /*!
         * \brief Operator+ for the \a StrongTypeT.
         *
         * \param[in] other Second member of the + operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator+(StrongTypeT const& other) const;

        /*!
         * \brief Operator- for the \a StrongTypeT.
         *
         * \param[in] other Second member of the - operator.
         *
         * \return The result with the \a StrongTypeT properly kept.
         */
        StrongTypeT operator-(StrongTypeT const& other) const;
    };


} // namespace MoReFEM::StrongTypeNS


#include "Utilities/Type/StrongType/Skills/Addable.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_ADDABLE_HPP_
