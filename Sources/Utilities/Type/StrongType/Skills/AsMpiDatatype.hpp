//! \file
//
//
//  DefaultConstructible.hpp
//  MoReFEM
//
//  Created by sebastien on 04/09/2020.
// Copyright © 2020 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_AS_MPI_DATATYPE_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_AS_MPI_DATATYPE_HPP_


namespace MoReFEM::StrongTypeNS
{


    /*!
     * \brief If this skill is added as a variadic parameter to a \a StrongType, the facilities related to \a Mpi may be
     * used with the \a StrongType.
     *
     */
    template<typename StrongTypeT>
    struct AsMpiDatatype
    {


        //! Token.
        static constexpr bool use_in_mpi_datatype = true;
    };


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_AS_MPI_DATATYPE_HPP_
