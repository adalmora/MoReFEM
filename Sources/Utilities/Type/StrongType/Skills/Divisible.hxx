//! \file
//
//
//  MoReFEM
//
//  Created by sebastien on 26/05/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Divisible.hpp"


namespace MoReFEM::StrongTypeNS
{

    template<class StrongTypeT>
    StrongTypeT Divisible<StrongTypeT>::operator/(StrongTypeT const& other) const
    {
        return StrongTypeT(static_cast<StrongTypeT const&>(*this).Get() / other.Get());
    }


    template<class StrongTypeT>
    template<class StrongTypeT_>
    std::enable_if_t<std::is_integral_v<typename StrongTypeT_::underlying_type>, StrongTypeT>
    Divisible<StrongTypeT>::operator%(StrongTypeT const& other) const
    {
        return StrongTypeT(static_cast<StrongTypeT const&>(*this).Get() % other.Get());
    }


} // namespace MoReFEM::StrongTypeNS


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_SKILLS_x_DIVISIBLE_HXX_
