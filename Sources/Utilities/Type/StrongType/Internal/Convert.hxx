/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz on 26/02/2020.
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HXX_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HXX_

// IWYU pragma: private, include "Utilities/Type/StrongType/Internal/Convert.hpp"


#include <algorithm> // IWYU pragma: keep
#include <vector>


namespace MoReFEM::Internal::StrongTypeNS
{

    template<class StrongTypeT>
    std::vector<StrongTypeT> Convert(const std::vector<typename StrongTypeT::underlying_type>& pod_vector)
    {
        std::vector<StrongTypeT> ret;
        ret.reserve(pod_vector.size());

        std::transform(pod_vector.cbegin(),
                       pod_vector.cend(),
                       std::back_inserter(ret),
                       [](const auto pod)
                       {
                           return StrongTypeT{ pod };
                       });

        return ret;
    }


} // namespace MoReFEM::Internal::StrongTypeNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HXX_
