/*!
//
// \file
//
//
//
//  Created by Jérôme Diaz
//  Copyright © 2020 Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#ifndef MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HPP_
#define MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HPP_

#include <iostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>


namespace MoReFEM::Internal::StrongTypeNS
{


    /*!
     * \brief Facility to convert of vector with POD (plain old data) into a \a StrongType which underlying type is matching.
     *
     * Typically it is used for data from the input file: \a OptionFile uses POD types, but in the rest of the
     * library we rather use strong types. This function may then be used when interpreting raw data from the input
     * file and convert them into usable types.
     *
     * \param[in] pod_vector Vector with the POD type.
     *
     * \return Vector of \a StrongTypeT with the same content as \a pod_vector plus the strong typing.
     */
    template<class StrongTypeT>
    std::vector<StrongTypeT> Convert(const std::vector<typename StrongTypeT::underlying_type>& pod_vector);


} // namespace MoReFEM::Internal::StrongTypeNS


/// @} // addtogroup UtilitiesGroup


#include "Utilities/Type/StrongType/Internal/Convert.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_TYPE_x_STRONG_TYPE_x_INTERNAL_x_CONVERT_HPP_
