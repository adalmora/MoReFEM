/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HPP_
#define MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HPP_


namespace MoReFEM::Internal::Impl
{


    /*!
     * \brief Common implementation under the hood for MoReFEM::Internal::WrapUnique(), MoReFEM::Internal::WrapShared(), MoReFEM::Internal::WrapUniqueToConst(), MoReFEM::Internal::WrapSharedToConst().
     *
     * \tparam ReturnTypeT Expected return type - a smart pointer to an object that might or might not be constant.
     *
     * \param[in] ptr Raw pointer built with a `new` call that will be immediately owned by the chosen smart pointer type.
     *
     */
    template<class ReturnTypeT>
    ReturnTypeT WrapSmartPointer(typename ReturnTypeT::element_type* ptr);


} // namespace MoReFEM::Internal::Impl


/// @} // addtogroup UtilitiesGroup


#include "Utilities/SmartPointers/Internal/Impl/Wrap.hxx"


#endif // MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HPP_
