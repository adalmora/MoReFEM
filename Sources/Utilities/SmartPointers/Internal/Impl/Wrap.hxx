/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HXX_
#define MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HXX_

// IWYU pragma: private, include "Utilities/SmartPointers/Internal/Impl/Wrap.hpp"


namespace MoReFEM::Internal::Impl
{


    template<class ReturnTypeT>
    ReturnTypeT WrapSmartPointer(typename ReturnTypeT::element_type* ptr)
    {
        using element_type = typename ReturnTypeT::element_type;
        static_assert(!std::is_array<element_type>::value);
        static_assert(std::is_object<element_type>::value);

        return ReturnTypeT(ptr);
    }


} // namespace MoReFEM::Internal::Impl


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_IMPL_x_WRAP_HXX_
