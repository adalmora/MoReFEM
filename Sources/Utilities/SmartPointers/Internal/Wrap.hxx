/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_WRAP_HXX_
#define MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_WRAP_HXX_

// IWYU pragma: private, include "Utilities/SmartPointers/Internal/Wrap.hpp"


namespace MoReFEM::Internal
{


    template<class T>
    std::unique_ptr<T> WrapUnique(T* ptr)
    {
        using return_type = std::unique_ptr<T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::unique_ptr<const T> WrapUniqueToConst(const T* ptr)
    {
        using return_type = std::unique_ptr<const T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::shared_ptr<T> WrapShared(T* ptr)
    {
        using return_type = std::shared_ptr<T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }


    template<class T>
    std::shared_ptr<const T> WrapSharedToConst(const T* ptr)
    {
        using return_type = std::shared_ptr<const T>;
        return Impl::WrapSmartPointer<return_type>(ptr);
    }

} // namespace MoReFEM::Internal


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_SMART_POINTERS_x_INTERNAL_x_WRAP_HXX_
