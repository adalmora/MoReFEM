/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 26 Mar 2015 09:26:19 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_STRING_x_EMPTY_STRING_HPP_
#define MOREFEM_x_UTILITIES_x_STRING_x_EMPTY_STRING_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Utilities
{

    /*!
     * \brief Returns a reference to a static empty string.
     *
     * \return ""
     */
    const std::string& EmptyString();


} // namespace MoReFEM::Utilities


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_STRING_x_EMPTY_STRING_HPP_
