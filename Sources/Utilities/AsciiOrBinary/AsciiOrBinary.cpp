/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"


namespace MoReFEM::Utilities
{

    AsciiOrBinary::~AsciiOrBinary() = default;


    AsciiOrBinary::AsciiOrBinary(bool binary_output)
    : binary_output_(binary_output ? binary_or_ascii::binary : binary_or_ascii::ascii)
    { }


    const std::string& AsciiOrBinary::ClassName()
    {
        static std::string ret("AsciiOrBinary");
        return ret;
    }


    std::string AsciiOrBinary::DatafileExtension() const
    {
        return ::MoReFEM::Advanced::AsciiOrBinaryNS::DatafileExtension(IsBinaryOutput());
    }


} // namespace MoReFEM::Utilities


/// @} // addtogroup UtilitiesGroup
