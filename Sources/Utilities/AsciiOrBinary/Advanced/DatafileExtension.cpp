/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <cassert>
#include <cstdlib>

#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"


namespace MoReFEM::Advanced::AsciiOrBinaryNS
{

    std::string DatafileExtension(binary_or_ascii is_binary_or_ascii)
    {
        switch (is_binary_or_ascii)
        {
        case binary_or_ascii::ascii:
            return ".hhdata";
        case binary_or_ascii::binary:
            return ".binarydata";
        case binary_or_ascii::from_input_data:
        {
            assert(false
                   && "This option is used internally in the library and should "
                      "not have been called in current context.");
            exit(EXIT_FAILURE);
        }
        }

        std::abort(); // to please gcc warning in release mode - should never been called.
    }


} // namespace MoReFEM::Advanced::AsciiOrBinaryNS


/// @} // addtogroup UtilitiesGroup
