/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ADVANCED_x_DATAFILE_EXTENSION_HPP_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ADVANCED_x_DATAFILE_EXTENSION_HPP_

#include <string>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp" // IWYU pragma: export
#include "Utilities/Exceptions/Exception.hpp"        // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp"         // IWYU pragma: export


namespace MoReFEM::Advanced::AsciiOrBinaryNS
{


    /*!
     * \brief Returns the proper extension to use depending on \a is_binary_or_ascii
     *
     * \param[in] is_binary_or_ascii Specifies whether ascii or binry extension is to be provided.
     *
     * \attention Must not be called if binary_or_ascii is neither ascii nor binary.
     *
     * \return ".hhdata" for ascii files, ".binarydata" for binary ones.
     */
    std::string DatafileExtension(binary_or_ascii is_binary_or_ascii);


} // namespace MoReFEM::Advanced::AsciiOrBinaryNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ADVANCED_x_DATAFILE_EXTENSION_HPP_
