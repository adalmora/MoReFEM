
/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <sstream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/AsciiOrBinary/Exceptions/BinarySerialization.hpp"
#include "Utilities/Filesystem/File.hpp"


namespace // anonymous
{


    std::string LoadBinaryFileExceptionMsg(const MoReFEM::FilesystemNS::File& binary_file);

    std::string NotExistingFileMsg(const MoReFEM::FilesystemNS::File& binary_file);


    std::string InconsistentFileSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                        std::string_view type_name,
                                        std::size_t sizeof_type,
                                        std::size_t file_size);


    std::string NotMatchingSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                   std::size_t Nexpected_values,
                                   std::size_t Nread_values);


} // namespace


namespace MoReFEM::ExceptionNS::BinaryNS
{


    LoadBinaryFileException::~LoadBinaryFileException() = default;


    LoadBinaryFileException::LoadBinaryFileException(const FilesystemNS::File& binary_file,
                                                     const char* invoking_file,
                                                     int invoking_line)
    : ::MoReFEM::Exception(LoadBinaryFileExceptionMsg(binary_file), invoking_file, invoking_line)
    { }


    LoadBinaryFileException::LoadBinaryFileException(const std::string& msg,
                                                     const char* invoking_file,
                                                     int invoking_line)
    : ::MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    NotExistingFile::~NotExistingFile() = default;

    NotExistingFile::NotExistingFile(const FilesystemNS::File& binary_file,
                                     const char* invoking_file,
                                     int invoking_line)
    : LoadBinaryFileException(NotExistingFileMsg(binary_file), invoking_file, invoking_line)
    { }


    InconsistentFileSize::~InconsistentFileSize() = default;

    InconsistentFileSize::InconsistentFileSize(const MoReFEM::FilesystemNS::File& binary_file,
                                               std::string_view type_name,
                                               std::size_t sizeof_type,
                                               std::size_t file_size,
                                               const char* invoking_file,
                                               int invoking_line)
    : LoadBinaryFileException(InconsistentFileSizeMsg(binary_file, type_name, sizeof_type, file_size),
                              invoking_file,
                              invoking_line)
    { }


    NotMatchingSize::~NotMatchingSize() = default;

    NotMatchingSize::NotMatchingSize(const MoReFEM::FilesystemNS::File& binary_file,
                                     std::size_t Nexpected_values,
                                     std::size_t Nread_values,
                                     const char* invoking_file,
                                     int invoking_line)
    : LoadBinaryFileException(NotMatchingSizeMsg(binary_file, Nexpected_values, Nread_values),
                              invoking_file,
                              invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::BinaryNS


namespace // anonymous
{


    std::string LoadBinaryFileExceptionMsg(const MoReFEM::FilesystemNS::File& binary_file)
    {
        std::ostringstream oconv;
        oconv << "An error occured while loading binary file " << binary_file << "; the file was probably ill-formed.";
        return oconv.str();
    }


    std::string NotExistingFileMsg(const MoReFEM::FilesystemNS::File& binary_file)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " wasn't found on the filesystem.";
        return oconv.str();
    }


    std::string InconsistentFileSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                        std::string_view type_name,
                                        std::size_t sizeof_type,
                                        std::size_t file_size)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " contains " << file_size
              << " bytes, which is not a multiple of "
                 "the size of "
              << type_name << " (which is on current architecture " << sizeof_type << " bytes).";
        return oconv.str();
    }


    std::string NotMatchingSizeMsg(const MoReFEM::FilesystemNS::File& binary_file,
                                   std::size_t Nexpected_values,
                                   std::size_t Nread_values)
    {
        std::ostringstream oconv;
        oconv << "Binary file " << binary_file << " was expected to hold " << Nexpected_values << " values, but "
              << Nread_values << " were found.";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup UtilitiesGroup
