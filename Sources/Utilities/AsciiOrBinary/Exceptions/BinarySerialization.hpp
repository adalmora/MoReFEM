/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_EXCEPTIONS_x_BINARY_SERIALIZATION_HPP_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_EXCEPTIONS_x_BINARY_SERIALIZATION_HPP_

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <string_view>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::ExceptionNS::BinaryNS
{


    /*!
     * \brief Generic exception when a problem occurred while loading a binary vector with \a UnserializeVectorFromBinaryFile().
     *
     */
    class LoadBinaryFileException : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor when this base exception is directly thrown.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        explicit LoadBinaryFileException(const FilesystemNS::File& binary_file,
                                         const char* invoking_file,
                                         int invoking_line);

        /*!
         * \brief Constructor used by derived exceptions.
         *
         * \param[in] msg Specific message crafted for the derived exception class.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        explicit LoadBinaryFileException(const std::string& msg, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~LoadBinaryFileException() override;

        //! \copydoc doxygen_hide_copy_constructor
        LoadBinaryFileException(const LoadBinaryFileException& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LoadBinaryFileException(LoadBinaryFileException&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LoadBinaryFileException& operator=(const LoadBinaryFileException& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LoadBinaryFileException& operator=(LoadBinaryFileException&& rhs) = default;
    };


    /*!
     * \brief When indicated file was not found.
     *
     */
    class NotExistingFile final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        explicit NotExistingFile(const FilesystemNS::File& binary_file, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~NotExistingFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        NotExistingFile(const NotExistingFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NotExistingFile(NotExistingFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NotExistingFile& operator=(const NotExistingFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NotExistingFile& operator=(NotExistingFile&& rhs) = default;
    };


    /*!
     * \brief When the file doesn't include the expected number of values (when the latter is available)
     *
     */
    class InconsistentFileSize final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \param[in] type_name Name of the type \a T which was expected in \a binary_file, as returned by MoReFEM::GetTypeName<T>().
         * \param[in] sizeof_type Result of `sizeof(T)`
         * \param[in] file_size Size of the file in bytes.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        explicit InconsistentFileSize(const FilesystemNS::File& binary_file,
                                      std::string_view type_name,
                                      std::size_t sizeof_type,
                                      std::size_t file_size,
                                      const char* invoking_file,
                                      int invoking_line);

        //! Destructor
        virtual ~InconsistentFileSize() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentFileSize(const InconsistentFileSize& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentFileSize(InconsistentFileSize&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentFileSize& operator=(const InconsistentFileSize& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentFileSize& operator=(InconsistentFileSize&& rhs) = default;
    };


    /*!
     * \brief When the file doesn't include the expected number of values (when the latter is available)
     *
     */
    class NotMatchingSize final : public LoadBinaryFileException
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_unserialize_binary_file_param
         *
         * \param[in] Nexpected_values Number of values expected in the vector filled from the content of the file.
         * \param[in] Nread_values Number of values actually read from \a binary_file.
         *
         * \copydoc doxygen_hide_invoking_file_and_line
         *
         */
        explicit NotMatchingSize(const FilesystemNS::File& binary_file,
                                 std::size_t Nexpected_values,
                                 std::size_t Nread_values,
                                 const char* invoking_file,
                                 int invoking_line);

        //! Destructor
        virtual ~NotMatchingSize() override;

        //! \copydoc doxygen_hide_copy_constructor
        NotMatchingSize(const NotMatchingSize& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        NotMatchingSize(NotMatchingSize&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        NotMatchingSize& operator=(const NotMatchingSize& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        NotMatchingSize& operator=(NotMatchingSize&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::BinaryNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_EXCEPTIONS_x_BINARY_SERIALIZATION_HPP_
