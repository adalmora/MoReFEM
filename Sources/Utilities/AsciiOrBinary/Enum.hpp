/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ENUM_HPP_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ENUM_HPP_


namespace MoReFEM
{

    //! Enum class to specify whether the choice of output (ascii or binary)
    enum class binary_or_ascii {
        from_input_data, // takes the value specified in the input data file.
        ascii,
        binary
    };

} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ENUM_HPP_
