/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HXX_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HXX_

// IWYU pragma: private, include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class binary_or_ascii; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Utilities
{


    inline binary_or_ascii AsciiOrBinary::IsBinaryOutput() const noexcept
    {
        return binary_output_;
    }


} // namespace MoReFEM::Utilities


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HXX_
