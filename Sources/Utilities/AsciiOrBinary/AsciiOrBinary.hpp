/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 8 Jul 2019 18:10:21 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HPP_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HPP_

#include <memory>
#include <string>

// IWYU pragma: begin_exports
#include "Utilities/AsciiOrBinary/Enum.hpp"
#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Singleton/Singleton.hpp"
// IWYU pragma: end_exports


namespace MoReFEM::Utilities
{


    /*!
     * \brief Provides a way to specify the output format of the solution vectors corresponding to each
     * unknown.
     *
     * This singleton acts as a global variable in order to store the boolean value related to the format
     * of the output. Its only purpose is to propagate this information throughout the whole code.
     *
     */
    class AsciiOrBinary final : public Utilities::Singleton<AsciiOrBinary>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = AsciiOrBinary;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] binary_output True to write resulting vectors in binary, false to write them in ascii.
         */
        explicit AsciiOrBinary(bool binary_output);

        //! Destructor.
        virtual ~AsciiOrBinary() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<AsciiOrBinary>;

        //! Name of the class.
        static const std::string& ClassName();


        ///@}


      public:
        //! Output format for solution vectors.
        binary_or_ascii IsBinaryOutput() const noexcept;

        /*!
         * \brief Returns the proper extension to use depending on the choice.
         *
         * \attention Must not be called if binary_or_ascii is neither ascii nor binary.
         *
         * \return ".hhdata" for ascii files, ".binarydata" for binary ones.
         */
        std::string DatafileExtension() const;

      private:
        //! Used to define whether the output is ascii or binary.
        binary_or_ascii binary_output_ = binary_or_ascii::ascii;
    };


} // namespace MoReFEM::Utilities


/// @} // addtogroup UtilitiesGroup


#include "Utilities/AsciiOrBinary/AsciiOrBinary.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_ASCII_OR_BINARY_HPP_
