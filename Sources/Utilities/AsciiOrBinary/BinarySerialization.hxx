//! \file
//

#ifndef MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_BINARY_SERIALIZATION_HXX_
#define MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_BINARY_SERIALIZATION_HXX_

// IWYU pragma: private, include "Utilities/AsciiOrBinary/BinarySerialization.hpp"

#include <cstring> // required by gcc for std::memcpy.
#include <string>  // IWYU pragma: keep
#include <vector>

// IWYU pragma: no_include <iosfwd>

#include "Utilities/AsciiOrBinary/Exceptions/BinarySerialization.hpp"
#include "Utilities/Type/PrintTypeName.hpp"

namespace MoReFEM::Advanced
{


    template<class T>
    std::vector<T> UnserializeVectorFromBinaryFile(const FilesystemNS::File& binary_file,
                                                   const char* invoking_file,
                                                   int invoking_line,
                                                   std::optional<std::size_t> Nexpected_values)
        requires(std::is_arithmetic<T>::value)
    {
        std::vector<T> ret;

        if (!binary_file.DoExist())
            throw ExceptionNS::BinaryNS::NotExistingFile(binary_file, invoking_file, invoking_line);

        std::ifstream in{ binary_file.Read(invoking_file, invoking_line, binary_or_ascii::binary) };

        std::ifstream::pos_type file_size_helper = in.seekg(0, std::ifstream::end).tellg();
        in.seekg(0, std::ifstream::beg);
        const auto file_size = static_cast<std::size_t>(file_size_helper);

        if (file_size % sizeof(T) != 0)
            throw ExceptionNS::BinaryNS::InconsistentFileSize(
                binary_file, GetTypeName<T>(), sizeof(T), file_size, invoking_file, invoking_line);

        const auto Nread_values = file_size / sizeof(T);

        if (Nexpected_values.has_value())
        {
            if (Nexpected_values.value() != Nread_values)
                throw ExceptionNS::BinaryNS::NotMatchingSize(
                    binary_file, Nexpected_values.value(), Nread_values, invoking_file, invoking_line);
        }

        std::vector<char> buffer(file_size);
        in.read(buffer.data(), file_size_helper);

        if (!in)
            throw ExceptionNS::BinaryNS::LoadBinaryFileException(
                "Unable to read content", invoking_file, invoking_line);

        in.close();

        const auto Nvalues = file_size / sizeof(T);
        ret.resize(Nvalues);
        std::memcpy(ret.data(), buffer.data(), file_size);

        return ret;
    }


    template<class T>
    void SerializeVectorIntoBinaryFile(const FilesystemNS::File& binary_file, const std::vector<T>& vector)
        requires(std::is_arithmetic<T>::value)
    {
        SerializeCArrayIntoBinaryFile(binary_file, vector.data(), vector.size());
    }


    template<class T>
    void SerializeCArrayIntoBinaryFile(const FilesystemNS::File& binary_file, const T* const array, std::size_t Nvalues)
        requires(std::is_arithmetic<T>::value)
    {
        std::ofstream out{ binary_file.NewContent(__FILE__, __LINE__, binary_or_ascii::binary) };

        const auto Nbits = Nvalues * sizeof(T);
        std::vector<char> binary_values(Nbits);
        std::memcpy(binary_values.data(), array, Nbits);
        out.write(binary_values.data(), static_cast<std::streamsize>(Nbits));
        out.close();
    }


} // namespace MoReFEM::Advanced

#endif // MOREFEM_x_UTILITIES_x_ASCII_OR_BINARY_x_BINARY_SERIALIZATION_HXX_
