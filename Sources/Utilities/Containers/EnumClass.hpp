/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 4 Apr 2014 11:08:30 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_


#include <type_traits> // IWYU pragma: keep


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_enum_class_id_for_input_data_macro
     *
     * \param[in] enum_class_id Value within the enum class to designate the specific indexed section considered.
     * For instance typically when writing a model we define something like:
     * \code
     enum class UnknownIndex : std::size_t { displacement = 1ul, velocity = 2ul };
     \endcode
     * to describe all the objects (here unknowns) considered. The leaf in the input data and model settings to call the
     related
     * section is something like:
     \code
     InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>
     \endcode
     * \a enum_class_id to be used in the macro would be here `UnknownIndex::displacement`.
     */


    /*!
     * \brief Extract the integer value behind an enum class.
     *
     * \tparam EnumT Type of the enum considered.
     *
     * \param[in] enumerator Element of the enum for which we want the underlying integer value.
     *
     * \return The underlying integer value behind an enum class member. The exact type of the integer is derived
     * with `std::underlying_type` traits.
     */
    template<typename EnumT>
    constexpr auto EnumUnderlyingType(EnumT enumerator) noexcept
    {
        return static_cast<std::underlying_type_t<EnumT>>(enumerator);
    }


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_ENUM_CLASS_HPP_
