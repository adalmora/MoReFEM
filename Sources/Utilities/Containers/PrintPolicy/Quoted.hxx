//! \file
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HXX_

// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Quoted.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<char QuoteCharacterT>
    template<class ElementTypeT>
    void Quoted<QuoteCharacterT>::Do(std::ostream& stream, ElementTypeT&& element)
    {
        stream << QuoteCharacterT << element << QuoteCharacterT;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HXX_
