//! \file
//
//
//  Associative.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HXX_

// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Associative.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<associative_format FormatT>
    template<class ElementTypeT>
    void Associative<FormatT>::Do(std::ostream& stream, ElementTypeT&& element)
    {
        if constexpr (FormatT == associative_format::Default)
            stream << '(' << element.first << ", " << element.second << ')';
        else if constexpr (FormatT == associative_format::Lua)
        {
            stream << '[';

            using key_type = typename std::decay_t<ElementTypeT>::first_type;

            constexpr bool is_string_or_path =
                (String::IsString<key_type>() || std::is_same_v<key_type, std::filesystem::path>);

            if constexpr (is_string_or_path)
                stream << "'";

            stream << element.first;

            if constexpr (is_string_or_path)
                stream << "'";

            stream << "] = " << element.second;
        } else
        {
            // Unfortunately static_assert(false) is not supported, so we'll have to do with runtime error.
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_ASSOCIATIVE_HXX_
