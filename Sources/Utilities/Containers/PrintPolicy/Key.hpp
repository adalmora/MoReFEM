//! \file
//
//
//  Associative.hpp
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HPP_

#include <memory>
#include <vector>


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy to print only the keys of an associative container.
     *
     */
    struct Key
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


#include "Utilities/Containers/PrintPolicy/Key.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_KEY_HPP_
