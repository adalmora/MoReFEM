//! \file
//
//
//  Created by Sébastien Gilles on 27/12/2022.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HPP_

#include <ostream>


namespace MoReFEM::Utilities::PrintPolicyNS
{


    /*!
     * \brief Policy very similar to the \a Normal one, except that each element is quoted by a single quote.
     *
     * \tparam QuoteCharacterT Character used as a quote. Default is a single quote, but this template
     * opens the possibility to use the double quote as well.
     *
     * This policy assumes the container is not associative.
     */
    template<char QuoteCharacterT = '\''>
    struct Quoted
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Utilities::PrintPolicyNS


#include "Utilities/Containers/PrintPolicy/Quoted.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_QUOTED_HPP_
