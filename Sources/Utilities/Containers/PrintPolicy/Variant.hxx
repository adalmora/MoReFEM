//! \file
//
//
//  Variant.hxx
//  MoReFEM
//
//  Created by sebastien on 24/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HXX_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HXX_

// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Variant.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class T>
    void Variant::Do(std::ostream& stream, T&& value)
    {
        using type = std::decay_t<T>;

        if constexpr (Utilities::IsSpecializationOf<std::variant, type>())
        {
            std::visit(
                [&stream](auto&& arg)
                {
                    stream << arg;
                },
                value);
        } else
            stream << value;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_PRINT_POLICY_x_VARIANT_HXX_
