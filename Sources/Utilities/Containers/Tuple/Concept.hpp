/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_x_CONCEPT_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_x_CONCEPT_HPP_

#include <tuple>


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is an instantiation of a std::tuple.
     *
     * \internal Rather crude check here: just working with tuple size is enough to pass.
     * But it is more than enough for my needs: still better than just putting 'class TupleT'!
     *
     */
    template<typename T>
    concept Tuple = requires { std::tuple_size<T>::value; };


} // namespace MoReFEM::Concept


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_TUPLE_x_CONCEPT_HPP_
