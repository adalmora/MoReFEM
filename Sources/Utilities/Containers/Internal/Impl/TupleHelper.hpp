/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 22:00:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_IMPL_x_TUPLE_HELPER_HPP_
#define MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_IMPL_x_TUPLE_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp" // IWYU pragma: export
#include "Utilities/Numeric/Numeric.hpp"          // IWYU pragma: export


namespace MoReFEM::Internal::Tuple::Impl
{


    /*!
     * \brief Check that elements at \a I -th and \a J -th position in \a TupleT don't share the same
     * type.
     *
     * \tparam TupleT Tuple being checked.
     * \tparam I Index of the first element in the comparison.
     * \tparam J Index of the second element in the comparison. By construct should be higher than I.
     * \tparam TupleSizeT Size of the tuple; it is const but required nonetheless to implement the
     * specialization that ends the recursion.
     */
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t I, std::size_t J, std::size_t TupleSizeT>
    struct CompareRemainingToI
    {


        //! Type of the \a I -th element in the tuple.
        using TypeI = typename std::tuple_element<I, TupleT>::type;

        //! Type of the \a J -th element in the tuple.
        using TypeJ = typename std::tuple_element<J, TupleT>::type;

        //! Static function that does the actual work.
        static void Perform()
        {
            static_assert(I < J, "If not ill-defined call to this static method!");
            static_assert(J != TupleSizeT, "Equality should be handled in a specialization.");
            static_assert(J < TupleSizeT, "If not ill-defined call to this static method!");

            static_assert(!std::is_same<TypeI, TypeJ>::value, "No type should be present twice in this tuple!");

            CompareRemainingToI<TupleT, I, J + 1, TupleSizeT>::Perform();
        }
    };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t I, std::size_t TupleSizeT>
    struct CompareRemainingToI<TupleT, I, TupleSizeT, TupleSizeT>
    {


        static void Perform()
        {
            // End recursion.
        }
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::Tuple::Impl


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_CONTAINERS_x_INTERNAL_x_IMPL_x_TUPLE_HELPER_HPP_
