//
//
//  reserved-identifier.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/12/2021.
//  Copyright © 2021 Inria. All rights reserved.
//

#ifdef __clang__
#if __has_warning("-Wreserved-macro-identifier")
PRAGMA_DIAGNOSTIC(ignored "-Wreserved-macro-identifier")
#endif
#endif // __clang__
