#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if __has_warning("-Wformat-security")
PRAGMA_DIAGNOSTIC(ignored "-Wformat-security")
#endif
#endif // __clang__

#if defined(MOREFEM_GCC)
#if __GNUC__ >= 12
PRAGMA_DIAGNOSTIC(ignored "-Wformat-security")
#endif
#endif
