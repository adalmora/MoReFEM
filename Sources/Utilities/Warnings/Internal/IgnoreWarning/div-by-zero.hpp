//
//  reserved-identifier.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/12/2021.
//  Copyright © 2021 Inria. All rights reserved.
//

#ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wdiv-by-zero") // for some tests only of course...
#endif
