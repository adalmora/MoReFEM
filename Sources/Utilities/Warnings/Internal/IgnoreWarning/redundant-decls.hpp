
#include "Utilities/Warnings/Pragma.hpp"

#ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wredundant-decls")
#endif // __clang__
