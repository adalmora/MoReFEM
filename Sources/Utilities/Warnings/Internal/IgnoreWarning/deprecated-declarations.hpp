//
//
//  Created by sebastien
//  Copyright © 2022 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if __has_warning("-Wdeprecated-declarations")
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated-declarations")
#endif
#endif // __clang__


#if defined(MOREFEM_GCC)
#if __GNUC__ >= 12
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated-declarations")
#endif
#endif
