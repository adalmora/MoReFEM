#include "Utilities/Warnings/Pragma.hpp"


PRAGMA_DIAGNOSTIC(ignored "-Wunused-variable")

#ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wunused-but-set-variable")
#endif
