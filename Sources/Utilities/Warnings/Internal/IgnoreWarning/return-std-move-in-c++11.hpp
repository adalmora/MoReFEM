#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if __has_warning("-Wreturn-std-move-in-c++11")
PRAGMA_DIAGNOSTIC(ignored "-Wreturn-std-move-in-c++11")
#endif
#endif // __clang__
