//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wunused-local-typedef")
#endif // __clang__

#ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wunused-local-typedefs")
#endif // MOREFEM_GCC
