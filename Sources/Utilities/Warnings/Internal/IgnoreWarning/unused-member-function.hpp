#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wunused-member-function")
#endif // __clang__
