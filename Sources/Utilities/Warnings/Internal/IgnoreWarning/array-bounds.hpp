#include "Utilities/Warnings/Pragma.hpp"

#if defined(MOREFEM_GCC)
#if __GNUC__ >= 13
PRAGMA_DIAGNOSTIC(ignored "-Warray-bounds")
#endif
#endif
