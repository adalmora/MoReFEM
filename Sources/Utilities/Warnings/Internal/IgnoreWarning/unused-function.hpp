#include "Utilities/Warnings/Pragma.hpp"

#ifdef MOREFEM_GCC
PRAGMA_DIAGNOSTIC(ignored "-Wunused-function")
#endif // MOREFEM_GCC
