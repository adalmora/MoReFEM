//
//  extra-semi-stmt.hpp
//  Utilities
//
//  Created by sebastien on 20/10/2019.
//  Copyright © 2019 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if !defined(__apple_build_version__) || __apple_build_version__ >= 12050022
PRAGMA_DIAGNOSTIC(ignored "-Wsuggest-override")
#endif
#endif // __clang__
