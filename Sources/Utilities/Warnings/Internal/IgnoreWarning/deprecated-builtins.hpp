//
//
//  Created by sebastien
//  Copyright © 2022 Inria. All rights reserved.
//

#include "Utilities/Warnings/Pragma.hpp"

#ifdef __clang__
#if __has_warning("-Wdeprecated-builtins")
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated-builtins")
#endif
#endif // __clang__
