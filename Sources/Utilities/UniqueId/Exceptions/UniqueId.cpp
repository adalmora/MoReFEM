/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

// IWYU pragma: no_include <__tree>

#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>

#include "Utilities/Numeric/Numeric.hpp"
#include "Utilities/UniqueId/Exceptions/UniqueId.hpp"


namespace MoReFEM::Internal::ExceptionsNS::UniqueId
{


    namespace // anonymous
    {

        std::string ReservedValueMsg();


        std::string AlreadyExistingIdMsg(std::size_t new_unique_id, std::string_view class_considered);

    } // namespace


    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : ::MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    ReservedValue::~ReservedValue() = default;

    ReservedValue::ReservedValue(const char* invoking_file, int invoking_line)
    : Exception(ReservedValueMsg(), invoking_file, invoking_line)
    { }


    AlreadyExistingId::~AlreadyExistingId() = default;

    AlreadyExistingId::AlreadyExistingId(std::size_t new_unique_id,
                                         std::string_view class_considered,
                                         const char* invoking_file,
                                         int invoking_line)
    : Exception(AlreadyExistingIdMsg(new_unique_id, class_considered), invoking_file, invoking_line)
    { }


    namespace // anonymous
    {


        std::string ReservedValueMsg()
        {
            std::ostringstream oconv;
            oconv << "Identifier " << NumericNS::UninitializedIndex<std::size_t>()
                  << " is reserved; please choose any other std::size_t!";
            return oconv.str();
        }


        std::string AlreadyExistingIdMsg(std::size_t new_unique_id, std::string_view class_considered)
        {
            std::ostringstream oconv;

            oconv << "The id " << new_unique_id << " has already been given to another " << class_considered
                  << " object.";

            return oconv.str();
        }


    } // namespace


} // namespace MoReFEM::Internal::ExceptionsNS::UniqueId


/// @} // addtogroup FiniteElementGroup
