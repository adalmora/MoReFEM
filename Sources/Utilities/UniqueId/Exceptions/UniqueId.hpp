/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_EXCEPTIONS_x_UNIQUE_ID_HPP_
#define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_EXCEPTIONS_x_UNIQUE_ID_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <string_view>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::ExceptionsNS::UniqueId
{


    //! Generic exception thrown for unique id related operations.
    class Exception : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Specific unique id exception.
    class ReservedValue : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit ReservedValue(const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~ReservedValue() override;
    };


    //! Specific unique id exception.
    class AlreadyExistingId : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] new_unique_id The unique id which was already existing.
         * \param[in] class_considered The name of the class which unique id is considered (should be the result of a
         GetTypeName<T> specializationn).
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit AlreadyExistingId(std::size_t new_unique_id,
                                   std::string_view class_considered,
                                   const char* invoking_file,
                                   int invoking_line);

        //! Destructor
        virtual ~AlreadyExistingId() override;
    };

} // namespace MoReFEM::Internal::ExceptionsNS::UniqueId


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_EXCEPTIONS_x_UNIQUE_ID_HPP_
