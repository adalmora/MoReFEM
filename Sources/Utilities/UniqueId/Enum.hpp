/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_ENUM_HPP_
#define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_ENUM_HPP_


namespace MoReFEM::UniqueIdNS
{


    /*!
     * \brief Whether the unique id is given in constructor or generated automatically by incrementing a static
     * counter.
     *
     * Details for each mode:
     * - \a automatic: Each time a new instance of \a DerivedT is created, it is assigned a value (which is the
     * number of already created instance). This value is accessible through the method GetUniqueId().
     * - \a manual: The ID is given by the user, and there is a check the id remains effectively unique.
     */
    enum class AssignationMode { automatic, manual };


    /*!
     * \brief Whether it is possible to build an object without assigning to it a unique id.
     *
     * \attention Relevant only for a \a UniqueId which \a AssignationMode is manual.
     */
    enum class DoAllowNoId { no, yes };


} // namespace MoReFEM::UniqueIdNS

/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_ENUM_HPP_
