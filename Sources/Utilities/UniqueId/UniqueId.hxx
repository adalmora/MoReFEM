/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 14 Nov 2014 11:12:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_
#define MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_

// IWYU pragma: private, include "Utilities/UniqueId/UniqueId.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/UniqueId/Exceptions/UniqueId.hpp"


namespace MoReFEM::Crtp
{


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::UniqueId() : unique_id_(AssignUniqueId())
    { }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::UniqueId(UnderlyingTypeT id)
    : unique_id_(NewUniqueId(id))
    { }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::UniqueId(std::nullptr_t)
    : unique_id_(NumericNS::UninitializedIndex<std::size_t>())
    {
        static_assert(AssignationModeT == UniqueIdNS::AssignationMode::manual,
                      "This constructor makes sense only in manual id assignation.");

        static_assert(DoAllowNoIdT == UniqueIdNS::DoAllowNoId::yes,
                      "DerivedT class must accept objects without identifiers!");
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UnderlyingTypeT UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::AssignUniqueId()
    {
        static_assert(AssignationModeT == UniqueIdNS::AssignationMode::automatic,
                      "This method makes sense only in automatic id assignation.");

        decltype(auto) storage = StaticUniqueIdList();

        if (storage.empty()) // should be rare but there are no mechanism to delete a keyword without values.
        {
            const auto new_value = UnderlyingTypeT{ 0ul };
            storage.emplace(new_value);
            return new_value;
        }

        static_assert(std::is_same_v<std::decay_t<decltype(storage)>, std::set<UnderlyingTypeT>>,
                      "The next line relies on the fact the values are properly sort!");

        auto new_value = *(storage.rbegin());
        ++new_value;

        storage.emplace(new_value);
        return new_value;
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UnderlyingTypeT
    UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::NewUniqueId(UnderlyingTypeT new_unique_id)
    {
        static_assert(AssignationModeT == UniqueIdNS::AssignationMode::manual,
                      "This constructor makes sense only in manual id assignation.");

        decltype(auto) unique_id_list = StaticUniqueIdList();

        if (new_unique_id == NumericNS::UninitializedIndex<UnderlyingTypeT>())
            throw Internal::ExceptionsNS::UniqueId::ReservedValue(__FILE__, __LINE__);

        auto [it, is_properly_inserted] = unique_id_list.insert(new_unique_id);

        if (!is_properly_inserted)
            throw Internal::ExceptionsNS::UniqueId::AlreadyExistingId(
                static_cast<std::size_t>(new_unique_id), DerivedT::ClassName(), __FILE__, __LINE__);

        return *it;
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    inline UnderlyingTypeT UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::GetUniqueId() const
    {
#ifndef NDEBUG
        // In the case no unique id is allowed, the default UninitializedIndex is given if no id were provided.
        if (UniqueIdNS::DoAllowNoId::no == DoAllowNoIdT)
            assert(unique_id_ != NumericNS::UninitializedIndex<UnderlyingTypeT>());
#endif // NDEBUG

        return unique_id_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    std::set<UnderlyingTypeT>& UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::StaticUniqueIdList()
    {
        static std::set<UnderlyingTypeT> ret;
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    UnderlyingTypeT UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::GenerateNewEligibleId()
    {
        decltype(auto) unique_id_list = StaticUniqueIdList();

        if (unique_id_list.empty())
            return UnderlyingTypeT{ 0ul };

        auto ret = *unique_id_list.rbegin();
        return ++ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        typename UnderlyingTypeT,
        UniqueIdNS::AssignationMode AssignationModeT,
        UniqueIdNS::DoAllowNoId DoAllowNoIdT
    >
    // clang-format on
    void UniqueId<DerivedT, UnderlyingTypeT, AssignationModeT, DoAllowNoIdT>::ClearUniqueIdList()
    {
        StaticUniqueIdList().clear();
    }


} // namespace MoReFEM::Crtp


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_UNIQUE_ID_x_UNIQUE_ID_HXX_
