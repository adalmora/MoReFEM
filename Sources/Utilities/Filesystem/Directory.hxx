//! \file
//
//
//  Directory.hxx
//  MoReFEM
//
//  Created by sebastien on 01/08/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_

// IWYU pragma: private, include "Utilities/Filesystem/Directory.hpp"

#include <cassert>
#include <filesystem>
#include <iosfwd> // IWYU pragma: keep
#include <optional>
#include <sstream>
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/Internal/CheckForSubdirectoryConstructor.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { enum class behaviour; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FilesystemNS
{


    template<class StringT>
    Directory::Directory(const Directory& parent_directory,
                         std::vector<StringT>&& subdirectories,
                         const char* invoking_file,
                         int invoking_line,
                         std::optional<behaviour> directory_behaviour)
    : mpi_(parent_directory.GetMpiPtr()), with_rank_(parent_directory.IsWithRank())
    {
        SetBehaviour(directory_behaviour.has_value() ? directory_behaviour.value() : parent_directory.GetBehaviour(),
                     invoking_file,
                     invoking_line);

        ::MoReFEM::Internal::FilesystemNS::CheckForSubdirectoryConstructor(
            parent_directory, invoking_file, invoking_line);

        std::ostringstream oconv;
        oconv << parent_directory.GetPath() << "/";
        Utilities::PrintContainer<>::Do(subdirectories,
                                        oconv,
                                        PrintNS::Delimiter::separator("/"),
                                        PrintNS::Delimiter::opener(""),
                                        PrintNS::Delimiter::closer(""));

        auto path = std::filesystem::path{ oconv.str() };
        path = std::filesystem::weakly_canonical(path);
        directory_entry_ = std::filesystem::directory_entry{ path };
    }


    inline const std::string& Directory::GetPath() const noexcept
    {
        return directory_entry_.path().native();
    }


    inline const std::filesystem::directory_entry& Directory::GetDirectoryEntry() const noexcept
    {
        return directory_entry_;
    }


    inline std::filesystem::directory_entry& Directory::GetNonCstDirectoryEntry() noexcept
    {
        return const_cast<std::filesystem::directory_entry&>(GetDirectoryEntry());
    }


    inline const Wrappers::Mpi& Directory::GetMpi() const noexcept
    {
        assert(IsMpi());
        return *mpi_;
    }


    inline bool Directory::DoExist() const noexcept
    {
        directory_entry_.refresh();
        return directory_entry_.is_directory();
    }


    inline bool Directory::IsMpi() const noexcept
    {
        return mpi_ != nullptr;
    }


    inline const Wrappers::Mpi* Directory::GetMpiPtr() const noexcept
    {
        return mpi_; // might be nullptr
    }


    inline behaviour Directory::GetBehaviour() const noexcept
    {
        return directory_behaviour_;
    }


    inline bool Directory::IsWithRank() const noexcept
    {
        return with_rank_;
    }


} // namespace MoReFEM::FilesystemNS


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_DIRECTORY_HXX_
