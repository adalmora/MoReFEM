/*!
 */

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HXX_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HXX_

// IWYU pragma: private, include "Utilities/Filesystem/File.hpp"

#include <filesystem>
#include <string>


namespace MoReFEM::FilesystemNS
{


    inline const std::filesystem::directory_entry& File::GetDirectoryEntry() const noexcept
    {
        // assert(!directory_entry_.path().empty());
        // < This assert has been removed as it is not the right granularity: we get some cases
        // < in which we may want to use an empty file path. If this proves too dangerous,
        // < an exception could be considered, but an assert was anyway unwieldy (it was not
        // < obvious where the problem was - typically when a \a File object was converted
        // < into a std::string it could happen).
        return directory_entry_;
    }


    inline std::filesystem::directory_entry& File::GetNonCstDirectoryEntry() const noexcept
    {
        return const_cast<std::filesystem::directory_entry&>(GetDirectoryEntry());
    }


    inline std::string File::Extension() const
    {
        return GetDirectoryEntry().path().extension().native();
    }


} // namespace MoReFEM::FilesystemNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_FILE_HXX_
