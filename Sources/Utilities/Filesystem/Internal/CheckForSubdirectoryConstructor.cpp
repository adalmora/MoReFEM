//! \file


#include <sstream>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/Internal/CheckForSubdirectoryConstructor.hpp"


namespace MoReFEM::Internal::FilesystemNS
{


    void CheckForSubdirectoryConstructor(const ::MoReFEM::FilesystemNS::Directory& parent_directory,
                                         const char* invoking_file,
                                         int invoking_line)
    {
        if (!parent_directory.DoExist())
        {
            std::ostringstream oconv;
            oconv << "Directory '" << parent_directory.GetPath()
                  << "' couldn't be found whereas we were trying to build a "
                     "subdirectory from it (so the directory has been created at some point and then removed).";
            throw Exception(oconv.str(), invoking_file, invoking_line);
        }
    }


} // namespace MoReFEM::Internal::FilesystemNS
