//! \file

//

#ifndef MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_CHECK_FOR_SUBDIRECTORY_CONSTRUCTOR_HPP_
#define MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_CHECK_FOR_SUBDIRECTORY_CONSTRUCTOR_HPP_

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FilesystemNS
{


    /*!
     * \brief Helper function for subdirectory constructors.
     *
     * \param[in] parent_directory The directory from which a subdirectory is to be constructed.
     * \copydoc doxygen_hide_invoking_file_and_line
     */
    void CheckForSubdirectoryConstructor(const ::MoReFEM::FilesystemNS::Directory& parent_directory,
                                         const char* invoking_file,
                                         int invoking_line);


} // namespace MoReFEM::Internal::FilesystemNS


#endif // MOREFEM_x_UTILITIES_x_FILESYSTEM_x_INTERNAL_x_CHECK_FOR_SUBDIRECTORY_CONSTRUCTOR_HPP_
