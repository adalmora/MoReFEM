//! \file
//
//
//  Extract.hxx
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_

// IWYU pragma: private, include "Utilities/InputData/Extract.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"


namespace MoReFEM::InputDataNS
{


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT, CountAsUsed CountAsUsedT>
    typename Utilities::ConstRefOrValue<typename ExtractLeaf<LeafT>::return_type>::type
    ExtractLeaf<LeafT>::Value(const InputDataT& input_data)
    {
        return input_data.template ExtractLeaf<LeafT, CountAsUsedT>();
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    template<::MoReFEM::Concept::InputDataType InputDataT>
    std::filesystem::path ExtractLeaf<LeafT>::Path(const InputDataT& input_data)
    {
        return input_data.template ExtractLeafAsPath<LeafT>();
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
    template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT>
    const auto& ExtractSection<SectionT>::Value(const InputDataT& input_data)
    {
        return input_data.template ExtractSection<SectionT>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        class DerivedT,
        class EnclosingSectionT,
        CountAsUsed CountAsUsedT
    >
    // clang-format on
    decltype(auto)
    ExtractLeafFromSection(const ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
    {
        using tuple_iteration =
            Internal::InputDataNS::TupleIteration<std::decay_t<decltype(section.GetSectionContent())>, 0ul>;

        const auto& section_content_tuple = section.GetSectionContent();

        using tuple_type = std::decay_t<decltype(section_content_tuple)>;

        static_assert(tuple_iteration::template Find<LeafNameT>());

        static_assert(::MoReFEM::Utilities::Tuple::IndexOf<LeafNameT, tuple_type>::value
                          < std::tuple_size<std::decay_t<decltype(section_content_tuple)>>(),
                      "Leaf is expected to be found directly in the section, whereas it is here enclosed in a "
                      "subsection (if not found at all the previous static assert would have failed).");

        const auto& leaf =
            std::get<::MoReFEM::Utilities::Tuple::IndexOf<LeafNameT, tuple_type>::value>(section_content_tuple);

        if (CountAsUsedT == CountAsUsed::yes)
            leaf.SetAsUsed();

        return leaf.GetTheValue();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafPathT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    decltype(auto) ExtractLeafFromInputDataOrModelSettings(const InputDataT& input_data,
                                                           const ModelSettingsT& model_settings)
    {

        using model_settings_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename ModelSettingsT::underlying_tuple_type, 0ul>;
        using input_data_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename InputDataT::underlying_tuple_type, 0ul>;

        constexpr auto is_in_model_settings = model_settings_tuple_iteration::template Find<LeafPathT>();
        constexpr auto is_in_input_data = input_data_tuple_iteration::template Find<LeafPathT>();

        static_assert((is_in_model_settings && !is_in_input_data) || (!is_in_model_settings && is_in_input_data),
                      "Field must be (exclusively) either in model settings or in input data.");

        if constexpr (is_in_model_settings)
            return ExtractLeaf<LeafPathT>::Value(model_settings);
        else
            return ExtractLeaf<LeafPathT>::Value(input_data);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        class DerivedT,
        class EnclosingSectionT,
        CountAsUsed CountAsUsedT
    >
    // clang-format on
    ::MoReFEM::FilesystemNS::File ExtractLeafFromSectionAsPath(
        const ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section)
    {
        ::MoReFEM::FilesystemNS::File ret{ std::filesystem::path(ExtractLeafFromSection<LeafNameT>(section)) };
        return ret;
    }


} // namespace MoReFEM::InputDataNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HXX_
