/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HXX_

// IWYU pragma: private, include "Utilities/InputData/InputData.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    template<::MoReFEM::Concept::ModelSettingsType ModelSettingsT>
    // clang-format on
    InputData<TupleT, DoUpdateLuaFileT>::InputData(const ModelSettingsT& model_settings,
                                                   const FilesystemNS::File& filename,
                                                   InputDataNS::DoTrackUnusedFields do_track_unused_fields)
    : parent(), input_data_file_(filename)
    {
        static_cast<void>(do_track_unused_fields);

        int is_initialized;

        MPI_Initialized(&is_initialized);

        if (!is_initialized)
            throw ::MoReFEM::InputDataNS::ExceptionNS::MpiNotInitialized(__FILE__, __LINE__);

        static_assert(Utilities::IsSpecializationOf<std::tuple, TupleT>::value,
                      "Template argument is expected to be a std::tuple.");

        ::MoReFEM::Wrappers::Lua::OptionFile lua_option_file(filename, __FILE__, __LINE__);

        if constexpr (DoUpdateLuaFileT == ::MoReFEM::InputDataNS::do_update_lua_file::no)
            CheckUnboundInputData(filename, lua_option_file, do_track_unused_fields);

        // Fill from the file all the objects in tuple_.
        Internal::InputDataNS::FillTuple<DoUpdateLuaFileT>(this, model_settings, lua_option_file, parent::GetNonCstTuple());
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    InputData<TupleT, DoUpdateLuaFileT>::~InputData()
    { }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    inline const FilesystemNS::File& InputData<TupleT, DoUpdateLuaFileT>::GetInputFile() const
    {
        return input_data_file_;
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    std::filesystem::path InputData<TupleT, DoUpdateLuaFileT>::ExtractLeafAsPath() const
    {
        decltype(auto) string = parent::template ExtractLeaf<LeafT, ::MoReFEM::InputDataNS::CountAsUsed::yes>();

        if constexpr (DoUpdateLuaFileT == ::MoReFEM::InputDataNS::do_update_lua_file::yes)
        {
            return std::filesystem::path{ string };
        } else
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);
            return std::filesystem::path{ environment.SubstituteValues(string) };
        }
    }


    // clang-format off
    template
    <

        ::MoReFEM::Concept::Tuple TupleT,
        ::MoReFEM::InputDataNS::do_update_lua_file DoUpdateLuaFileT
    >
    // clang-format on
    void InputData<TupleT, DoUpdateLuaFileT>::CheckUnboundInputData(
        const FilesystemNS::File& filename,
        ::MoReFEM::Wrappers::Lua::OptionFile& lua_option_file,
        InputDataNS::DoTrackUnusedFields do_track_unused_fields) const
    {
        // Check there are no items undefined in the tuple.
        if (do_track_unused_fields == InputDataNS::DoTrackUnusedFields::yes)
        {
            decltype(auto) entry_key_list = lua_option_file.GetEntryKeyList();

            for (decltype(auto) entry_key : entry_key_list)
            {
                const auto pos = entry_key.rfind(".");

                std::string_view section_name, variable;

                if (pos == std::string::npos)
                {
                    section_name = "";
                    variable = entry_key;
                } else
                {
                    section_name = std::string_view(entry_key.data(), pos);
                    variable = std::string_view(&entry_key.at(pos + 1), entry_key.size() - pos - 1);
                }

                if (!parent::tuple_iteration::DoMatchIdentifier(section_name, variable))
                    throw ::MoReFEM::InputDataNS::ExceptionNS::UnboundInputData(
                        filename, section_name, variable, __FILE__, __LINE__);
            }
        }
    }


} // namespace MoReFEM


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INPUT_DATA_HXX_
