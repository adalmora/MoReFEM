/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 15:14:32 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <list>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Exceptions/InputData.hpp"
#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"
#include "Utilities/String/String.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string DuplicateInInputFileMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list);


    std::string UnboundInputDataMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                    std::string_view section,
                                    std::string_view variable);


    std::string DuplicateInTupleMsg(const std::string& key);


    std::string MpiNotInitializedMsg();


    std::string FolderDoesntExistMsg(const std::string& folder);


    std::string NoEntryInModelSettingsMsg(const std::string& entry_identifier);


    std::string ValueCantBeSetTwiceMsg(const std::string& entry_identifier);


    std::string ModelSettingsNotCompletelyFilledMsg(const std::vector<std::string>& missing_entries);


    std::string MissingIndexedSectionDescriptionInModelSettingsTupleMsg(std::string_view section_name);

} // namespace


namespace MoReFEM::InputDataNS::ExceptionNS
{

    Exception::~Exception() = default;


    Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)
    : ::MoReFEM::Exception(msg, invoking_file, invoking_line)
    { }


    DuplicateInInputFile::~DuplicateInInputFile() = default;


    DuplicateInInputFile::DuplicateInInputFile(const ::MoReFEM::FilesystemNS::File& filename,
                                               const std::string& section,
                                               const std::vector<std::string>& variable_list,
                                               const char* invoking_file,
                                               int invoking_line)
    : Exception(DuplicateInInputFileMsg(filename, section, variable_list), invoking_file, invoking_line)
    { }


    UnboundInputData::~UnboundInputData() = default;


    UnboundInputData::UnboundInputData(const ::MoReFEM::FilesystemNS::File& filename,
                                       std::string_view section,
                                       std::string_view variable,
                                       const char* invoking_file,
                                       int invoking_line)
    : Exception(UnboundInputDataMsg(filename, section, variable), invoking_file, invoking_line)
    { }


    DuplicateInTuple::~DuplicateInTuple() = default;


    DuplicateInTuple::DuplicateInTuple(const std::string& key, const char* invoking_file, int invoking_line)
    : Exception(DuplicateInTupleMsg(key), invoking_file, invoking_line)
    { }


    MpiNotInitialized::~MpiNotInitialized() = default;


    MpiNotInitialized::MpiNotInitialized(const char* invoking_file, int invoking_line)
    : Exception(MpiNotInitializedMsg(), invoking_file, invoking_line)
    { }


    FolderDoesntExist::~FolderDoesntExist() = default;


    FolderDoesntExist::FolderDoesntExist(const std::string& folder, const char* invoking_file, int invoking_line)
    : Exception(FolderDoesntExistMsg(folder), invoking_file, invoking_line)
    { }


    NoEntryInModelSettings::~NoEntryInModelSettings() = default;


    NoEntryInModelSettings::NoEntryInModelSettings(const std::string& entry_identifier,
                                                   const char* invoking_file,
                                                   int invoking_line)
    : Exception(NoEntryInModelSettingsMsg(entry_identifier), invoking_file, invoking_line)
    { }


    ValueCantBeSetTwice::~ValueCantBeSetTwice() = default;


    ValueCantBeSetTwice::ValueCantBeSetTwice(const std::string& entry_identifier,
                                             const char* invoking_file,
                                             int invoking_line)
    : Exception(ValueCantBeSetTwiceMsg(entry_identifier), invoking_file, invoking_line)
    { }


    ModelSettingsNotCompletelyFilled::~ModelSettingsNotCompletelyFilled() = default;


    ModelSettingsNotCompletelyFilled::ModelSettingsNotCompletelyFilled(const std::vector<std::string>& missing_entries,
                                                                       const char* invoking_file,
                                                                       int invoking_line)
    : Exception(ModelSettingsNotCompletelyFilledMsg(missing_entries), invoking_file, invoking_line)
    { }


    MissingIndexedSectionDescriptionInModelSettingsTuple::~MissingIndexedSectionDescriptionInModelSettingsTuple() = default;

    MissingIndexedSectionDescriptionInModelSettingsTuple::MissingIndexedSectionDescriptionInModelSettingsTuple(std::string_view section_name,
                                                                       const char* invoking_file,
                                                                       int invoking_line)
    : Exception(MissingIndexedSectionDescriptionInModelSettingsTupleMsg(section_name), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::InputDataNS::ExceptionNS


namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file

    std::string DuplicateInInputFileMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                        const std::string& section,
                                        const std::vector<std::string>& variable_list)
    {
        std::list<std::string> buf(variable_list.cbegin(), variable_list.cend());
        buf.sort();
        buf.unique();

        auto it_duplicate = buf.cbegin();

        // Find the duplicate.
        {
            for (auto end = buf.cend(); it_duplicate != end; ++it_duplicate)
            {
                if (std::count(variable_list.cbegin(), variable_list.cend(), *it_duplicate) >= 2)
                    break;
            }
            assert(it_duplicate != buf.cend());
        }


        std::ostringstream oconv;
        oconv << "Error in input file " << filename
              << ": "
                 "at least one duplicate found ('"
              << *it_duplicate << "')";

        if (!section.empty())
            oconv << " in section '" << section << '\'';
        else
            oconv << "outside of any section";

        oconv << '.';

        return oconv.str();
    }


    std::string UnboundInputDataMsg(const ::MoReFEM::FilesystemNS::File& filename,
                                    std::string_view section,
                                    std::string_view variable)
    {
        std::ostringstream oconv;
        oconv << "In the input file ' " << filename << "' there was a variable '" << variable << '\'';

        if (section.empty())
            oconv << " outside any section";
        else
            oconv << " in the section \'" << section << '\'';

        oconv << "; no tuple element claims this one. It means this input datum is "
                 "completely useless; please remove it from the input file (or check your "
                 "input data tuple is up-to-date).";

        return oconv.str();
    }


    std::string DuplicateInTupleMsg(const std::string& key)
    {
        std::ostringstream oconv;
        oconv << "The key " << key
              << " has been found at least twice in the input data list; "
                 "the tuple that built it is therefore ill-formed.\n";
        oconv << "It might be either the same entry is present twice in the tuple, or two entries share "
                 "the same couple Section() / NameInFile()";

        return oconv.str();
    }


    std::string MpiNotInitializedMsg()
    {
        return "MPI was not initialized properly!";
    }


    std::string FolderDoesntExistMsg(const std::string& folder)
    {
        return "Folder " + folder + " doesn't exist whereas is was expected to.";
    }


    std::string NoEntryInModelSettingsMsg(const std::string& entry_identifier)
    {
        std::ostringstream oconv;
        
        // Check if we're talking about a IndexedSectionDescription - if so adapt the message accordingly.
        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();
        
        if (MoReFEM::Utilities::String::EndsWith(entry_identifier, section_description_suffix))
        {
            const auto pos = entry_identifier.rfind(".");
            assert(pos != std::string::npos);
            auto section_name = entry_identifier.substr(0ul, pos);
            
            oconv << "ModelSettings::SetDescription() wasn't properly called for section "
                  << section_name << ").";
        }
        else
        {
            oconv << "In ModelSettings::Add() method, provided leaf which key is " << entry_identifier
            << " wasn't found in the underlying tuple.";
        }
            
        return oconv.str();
    }


    std::string ValueCantBeSetTwiceMsg(const std::string& entry_identifier)
    {
        std::ostringstream oconv;

        // Check if we're talking about a IndexedSectionDescription - if so adapt the message accordingly.
        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();

        if (MoReFEM::Utilities::String::EndsWith(entry_identifier, section_description_suffix))
        {
            const auto pos = entry_identifier.rfind(".");
            assert(pos != std::string::npos);
            auto section_name = entry_identifier.substr(0ul, pos);

            oconv << "ModelSettings::SetDescription() can't be called twice for the same section (namely "
                  << section_name << ").";
        } else
        {
            oconv << "ModelSettings::Add() can't be called twice over the same entry (namely " << entry_identifier
                  << ").";
        }

        return oconv.str();
    }

    std::string ModelSettingsNotCompletelyFilledMsg(const std::vector<std::string>& missing_entries)
    {
        std::ostringstream oconv;
        oconv << "Some entries in ModelSettings tuple were not properly filled." << std::endl;


        std::vector<std::string> indexed_sections_without_description_list;
        std::vector<std::string> others_missing_entries_list;

        indexed_sections_without_description_list.reserve(missing_entries.size());
        others_missing_entries_list.reserve(missing_entries.size());

        const auto section_description_suffix = MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();

        for (const auto& entry : missing_entries)
        {
            if (MoReFEM::Utilities::String::EndsWith(entry, section_description_suffix))
            {
                const auto pos = entry.rfind(".");
                assert(pos != std::string::npos);

                auto section_name = entry.substr(0ul, pos);

                indexed_sections_without_description_list.push_back(section_name);
            } else
                others_missing_entries_list.push_back(entry);
        }

        if (!others_missing_entries_list.empty())
        {
            oconv
                << "Please add the values (through the `Add()` template method) to the leaves which identifiers are: ";

            MoReFEM::Utilities::PrintContainer<>::Do(others_missing_entries_list,
                                                     oconv,
                                                     MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::opener("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::closer("\n"));
        }

        if (!indexed_sections_without_description_list.empty())
        {
            oconv << "Please add the description (through the `SetDescription()` template method) for the following "
                     "indexed sections: ";

            MoReFEM::Utilities::PrintContainer<>::Do(indexed_sections_without_description_list,
                                                     oconv,
                                                     MoReFEM::PrintNS::Delimiter::separator("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::opener("\n\t- "),
                                                     MoReFEM::PrintNS::Delimiter::closer("\n"));
        }

        return oconv.str();
    }


    std::string MissingIndexedSectionDescriptionInModelSettingsTupleMsg(std::string_view section_name)
    {
        std::ostringstream oconv;

        oconv << "The model is flawed: there should be a 'IndexedSectionDescription' leaf in the 'ModelSettings' tuple for section "
              << std::string(section_name)
              << ". Please ask the author of the model to fix it; the error is not caused "
                 "by data you provided in your Lua file.";

        return oconv.str();
    }


} // namespace


/// @} // addtogroup UtilitiesGroup
