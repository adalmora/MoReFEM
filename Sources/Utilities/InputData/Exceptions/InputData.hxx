/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Aug 2013 17:46:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_

// IWYU pragma: private, include "Utilities/InputData/Exceptions/InputData.hpp"


#include <sstream>
#include <string>


namespace MoReFEM::InputDataNS::ExceptionNS
{

    /*!
     * \brief Prepare the error message for MismatchedVector.
     *
     * \tparam SubTupleT The list of input data objects which vector should have had the same size.
     */
    template<class SubTupleT>
    std::string MismatchedVectorSizeMsg();


    template<class SubTupleT>
    MismatchedVectorSize<SubTupleT>::MismatchedVectorSize(const char* invoking_file, int invoking_line)
    : Exception(MismatchedVectorSizeMsg<SubTupleT>(), invoking_file, invoking_line)
    { }


    template<class SubTupleT>
    MismatchedVectorSize<SubTupleT>::~MismatchedVectorSize() = default;


    template<class SubTupleT>
    std::string MismatchedVectorSizeMsg()
    {
        std::ostringstream oconv;
        oconv << "Some input data were expected to be vectors of the same size, "
                 "but that is not the case:\n";

        return oconv.str();
    }


} // namespace MoReFEM::InputDataNS::ExceptionNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXCEPTIONS_x_INPUT_DATA_HXX_
