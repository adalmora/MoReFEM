/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CONCEPT_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CONCEPT_HPP_

#include <optional>

#include "Utilities/InputData/Advanced/Concept.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify a type is an \a InputData class instance.
     *
     * \internal \a Utilities::IsSpecializationOf can't be used here as there is a non-type
     * template argument in \a InputData template class.
     */
    template<typename T>
    concept InputDataType = requires {
        {
            T::ConceptIsInputData == true
        };
    };

    //! Same as \a InputDataType except that std::nullopt is also an option.
    template<class T>
    concept InputDataTypeOrNullopt = InputDataType<T> || std::is_same_v<std::nullopt_t, T>;


    /*!
     * \brief Defines a concept to identify a type is an \a ModelSettings class instance.
     *
     * \internal \a Utilities::IsSpecializationOf can't be used here as there is a non-type
     * template argument in \a ModelSettings template class.
     */
    template<typename T>
    concept ModelSettingsType = requires {
        {
            T::ConceptIsModelSettings == true
        };
    };


    /*!
     * \brief Self explaining!
     *
     * Used for facilities we would like to work with both \a InputData and \a ModelSettings.
     */
    template<typename T>
    concept InputDataOrModelSettingsType = InputDataType<T> || ModelSettingsType<T>;


} // namespace MoReFEM::Concept


/// @} // addtogroup UtilitiesGroup

#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_CONCEPT_HPP_
