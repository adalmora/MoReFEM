//! \file
//
//
//  Extract.hpp
//  MoReFEM
//
//  Created by sebastien on 23/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <string>

#include "Utilities/Filesystem/File.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Enum.hpp" // IWYU pragma: export
#include "Utilities/Miscellaneous.hpp"  // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief A convenient helper to extract the value stored in a leaf of \a InputData with syntactic sugar.
     *
     * The method of \a InputData used to extract leaf information is a template method, which as argument the
     * type related to the sought quantity (let's say \a InputData::BoundaryCondition::Component).
     *
     * Using the method directly would lead to a syntax like:
     *
     * \code
     * using leaf_type = InputData::BoundaryCondition::Component;
     * auto component_value = input_data.template Extract<leaf_type>();
     * \endcode
     *
     * where \a input_data is the object holding data from the input file (instance of \a InputData template class)
     *
     * There is nothing wrong with such a call, but the template keyword above is a bit arcane for most
     * C++ developers, and so far compilers aren't very good to suggest the keyword `template` might be missing
     * (even if at the time of this writing - decembre 2022 - clang seems to improve on that account).
     *
     * It seems better to provide an alternative which avoids altogether the issue:
     *
     * \code
     * using leaf_type = InputData::BoundaryCondition::Component;
     * auto component_value = ExtractLeaf<leaf_type>::Value(input_data);
     * \endcode
     *
     * In fact, to avoid confusing developers about the proper syntax only the syntactic sugar one remains:
     * the \a ExtractLeaf method of \a InputData template class has been declared private.
     *
     */
    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
    struct ExtractLeaf
    {

        //! Convenient alias over the return type.
        using return_type = typename LeafT::return_type;

        /*!
         * \brief Return the value of a leaf from the input data.
         *
         * \tparam InputDataT The object which holds the information about input data file content.
         * \tparam CountAsUsedT At the end of the program, we may sum up the input data that have not been
         * used. A call to this method tally them as used unless 'no' is specified here.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \return Value of the leaf
         */
        template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT,
                 CountAsUsed CountAsUsedT = CountAsUsed::yes>
        static typename Utilities::ConstRefOrValue<return_type>::type Value(const InputDataT& input_data);

        /*!
         * \brief Return the value of a leaf from the input data as a path.
         *
         * Environment variables that might have been read from the input file have been replaced; for instance
         * "${HOME}/Codes/MoReFEM" will be resolved on a Mac in /Users/ *username* /Codes/MoReFEM.
         *
         * \param[in] input_data Object which holds all relevant input data for the model considered.
         *
         * \return Path related to \a ObjectT.
         */
        template<::MoReFEM::Concept::InputDataType InputDataT>
        static std::filesystem::path Path(const InputDataT& input_data);
    };


    /*!
     * \brief Mostly the same as \a ExtractLeaf for sections.
     *
     * Currently you shouldn't have to use this, but it was not costly to implement and might be used
     * if at some point we need to expand the use of \a InputData.
     *
     * \tparam SectionT Section sought by this syntactic sugar facillity.
     *
     */
    template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
    struct ExtractSection
    {

        /*!
         * \brief Return the content of a section from the input data.
         *
         * \tparam InputDataT The object which holds the information about input data file content.
         * \copydoc doxygen_hide_input_data_arg
         *
         * \return Value of the section. The exact type is rather complex and has thus been obfuscayed
         */
        template<::MoReFEM::Concept::InputDataOrModelSettingsType InputDataT>
        static const auto& Value(const InputDataT& input_data);
    };


    /*!
     * \brief Extract the value of a data in the input file which is directly (i.e. not within a subsection) present in
     * the section.
     *
     * This might be useful for instance if we have tracked a specific section such as \a Domain  and we
     * want to access values of data that we know are directly there, such as \a DimensionList
     *
     * \param[in] section The section currently considered (in the example above, it is a specific Domain ad
     * 'Domain5').
     *
     * \tparam LeafNameT Usual parameter type to be expected within the current section. e.g. \a DimensionList
     * for \a Domain.
     * \tparam DerivedT One of the template parameter to identify the type of section considered.
     * Expected to be automatically derived from section.
     * \tparam EnclosingSectionT Same as \a DerivedT.
     * \tparam CountAsUsedT Whether the extracted is to be marked as used following this extraction. In most
     * of the case the default value of 'yes'is what is expected.
     *
     * \return Value of the input datum (possibly through a reference).
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        class DerivedT,
        class EnclosingSectionT,
        CountAsUsed CountAsUsedT = CountAsUsed::yes
    >
    // clang-format on
    decltype(auto)
    ExtractLeafFromSection(const ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section);


    /*!
     * \brief Same functionality as \a ExtractLeaf, except that environment variables are replaced by their
     * values.
     *
     * \param[in] section Section that enclose the input data being read..
     *
     * \return Path given by the input data being read.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        class DerivedT,
        class EnclosingSectionT,
        CountAsUsed CountAsUsedT = CountAsUsed::yes
    >
    // clang-format on
    ::MoReFEM::FilesystemNS::File ExtractLeafFromSectionAsPath(
        const ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>& section);


    /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     * \tparam InputDataT Type of \a input_data, which holds data the end-user may modify in the Lua file.
     * \tparam ModelSettingsT Type of \a model_settings, which holds data that are specified (and hardcoded) by the
     * author of the model; end-user is not supposed to modify it.
     *
     * \copydoc doxygen_hide_model_settings_input_data_arg
     *
     * \return Value found either in \a input_data or in \a model_settings. Can't be found in both - in this case the
     * model would not even compile.
     *
     * \attention There is a namesake function in Core library that provides syntactic sugar, but requires more stuff not available
     * in current Utilities library. The overload is used more often as it is more convenient (less arguments to provide); make sure
     * however to provide the proper include if you're using one or the other.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    decltype(auto) ExtractLeafFromInputDataOrModelSettings(const InputDataT& input_data,
                                                           const ModelSettingsT& model_settings);

} // namespace MoReFEM::InputDataNS


#include "Utilities/InputData/Extract.hxx" // IWYU pragma: export


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_EXTRACT_HPP_
