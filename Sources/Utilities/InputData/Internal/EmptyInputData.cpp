//! \file


#include "Utilities/InputData/Internal/EmptyInputData.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    EmptyInputData::~EmptyInputData() = default;


} // namespace MoReFEM::Internal::InputDataNS
