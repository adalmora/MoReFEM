/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_WRITE_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_WRITE_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/Write/Write.hpp"


#include <cstddef> // IWYU pragma: keep
#include <fstream>

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#include "Utilities/InputData/Internal/Write/Enum.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    void CreateDefaultInputFile(const ::MoReFEM::FilesystemNS::File& file, const ModelSettingsT& model_settings)
    {
        std::ofstream out{ file.NewContent(__FILE__, __LINE__) };

        out << "-- Comment lines are introduced by \"--\".\n";
        out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";

        PrepareDefaultEntries<InputDataT>(out, model_settings);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
        
    >
    // clang-format on
    void Write(const ModelSettingsT& model_settings,
               const InputDataT& input_data,
               const ::MoReFEM::FilesystemNS::File& file,
               verbosity verbosity_level)
    {
        std::ofstream out{ file.NewContent(__FILE__, __LINE__) };

        if (verbosity_level == verbosity::verbose)
        {
            out << "-- Comment lines are introduced by \"--\".\n";
            out << "-- In a section (i.e. within braces), all entries must be separated by a comma.\n\n";
        }

        PrintContent(model_settings, input_data, out, verbosity_level);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void
    RewriteInputDataFile(const ModelSettingsT& model_settings, const InputDataT& input_data, verbosity verbosity_level)
    {
        decltype(auto) original_file = input_data.GetInputFile();

        auto backup_path = original_file.GetDirectoryEntry().path();
        backup_path.replace_extension(".lua.bak");

        auto backup_file = ::MoReFEM::FilesystemNS::File{ std::move(backup_path) };

        ::MoReFEM::FilesystemNS::Copy(original_file,
                                      backup_file,
                                      ::MoReFEM::FilesystemNS::fail_if_already_exist::no,
                                      ::MoReFEM::FilesystemNS::autocopy::no,
                                      __FILE__,
                                      __LINE__);

        try
        {
            original_file.Remove(__FILE__, __LINE__);

            Internal::InputDataNS::Write(model_settings, input_data, original_file, verbosity_level);
        }
        catch (const Exception& e)
        {
            std::cerr << "Exception caught: " << e.what() << std::endl;
            std::cerr << "File " << original_file
                      << " will be put back in its original state and exception will be"
                         " thrown again."
                      << std::endl;

            ::MoReFEM::FilesystemNS::Copy(backup_file,
                                          original_file,
                                          ::MoReFEM::FilesystemNS::fail_if_already_exist::no,
                                          ::MoReFEM::FilesystemNS::autocopy::no,
                                          __FILE__,
                                          __LINE__);

            throw;
        }

        std::cout << "File " << original_file
                  << " has been updated; a copy of the former version has been made "
                     "(with an additional .bak extension). Please review it before validating it! "
                  << std::endl;
        std::cout
            << "It is possible that some fields have disappeared: if there was in the data file a field which "
               "was not considered at all (i.e. had no match in the InputData tuple) they were in fact unused and were "
               "dropped here."
            << std::endl;
    }


} // namespace MoReFEM::Internal::InputDataNS

/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_WRITE_HXX_
