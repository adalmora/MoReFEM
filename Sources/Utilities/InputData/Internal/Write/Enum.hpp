/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_ENUM_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_ENUM_HPP_


namespace MoReFEM::Internal::InputDataNS
{

    //! Enum class to tell how to rewrite the input lua files.
    //! In verbose mode, description, constraints and so on are written.
    //! In compact mode, comments are skipped and there are less newlines.
    enum class verbosity { verbose, compact };

    /*!
     * \class doxygen_hide_input_data_rewrite_lua_verbosity
     *
     * \param[in] verbosity_level In verbose mode, description, constraints and so on are written.
     * In compact mode, comments are skipped and there are less newlines.
     */


} // namespace MoReFEM::Internal::InputDataNS

/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_WRITE_x_ENUM_HPP_
