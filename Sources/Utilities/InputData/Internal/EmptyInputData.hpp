//! \file
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EMPTY_INPUT_DATA_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EMPTY_INPUT_DATA_HPP_

#include <memory>
#include <tuple>

#include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp" // IWYU pragma: export

namespace MoReFEM::Internal::InputDataNS
{


    /*!
     * \brief Placeholder when there are no \a InputData.
     *
     */
    struct EmptyInputData : public Internal::InputDataNS::AbstractClass<std::tuple<>>
    {
        //! Helper variable to define the \a InputDataType concept.
        static inline constexpr bool ConceptIsInputData = true;

        //! Traits expected in an \a InputData object.
        using underlying_tuple_type = std::tuple<>;

        //! Traits expected in an \a InputData object.
        using const_unique_ptr = std::unique_ptr<const EmptyInputData>;

        //! Destructor
        virtual ~EmptyInputData() override;
    };


} // namespace MoReFEM::Internal::InputDataNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_EMPTY_INPUT_DATA_HPP_
