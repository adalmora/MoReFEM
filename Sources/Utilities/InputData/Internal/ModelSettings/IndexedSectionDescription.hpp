#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HPP_

#include <string>

namespace MoReFEM::Internal::InputDataNS
{

    /*!
     * \brief Suffix applied to \a NameInFile() to identify a \a IndexedSectionDescription.
     *
     * The suffix is chosen so that it is unlikely a model developer would come with a name enacting
     * the same suffix.
     */
    static std::string IndexedSectionDescriptionSuffix();


} // namespace MoReFEM::Internal::InputDataNS

#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hxx"


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HPP_
