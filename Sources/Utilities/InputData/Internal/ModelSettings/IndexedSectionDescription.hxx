#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HXX_

// IWYU pragma: private, include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"


// IWYU pragma: private, include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"



#include <string>

#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"


namespace MoReFEM::Internal::InputDataNS
{


    inline std::string IndexedSectionDescriptionSuffix()
    {
        // The suffix is chosen so that it is unlikely a model developer would come
        // with a name enacting the same suffix.
        return std::string("!_*_!token");
    }


} // namespace MoReFEM::Internal::InputDataNS


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_MODEL_SETTINGS_x_INDEXED_SECTION_DESCRIPTION_HXX_
