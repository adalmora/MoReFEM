/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 14 Aug 2013 15:09:11 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_ABSTRACT_CLASS_x_ABSTRACT_CLASS_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_ABSTRACT_CLASS_x_ABSTRACT_CLASS_HPP_


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"   // IWYU pragma: export
#include "Utilities/InputData/Exceptions/InputData.hpp" // IWYU pragma: export
#include "Utilities/InputData/Extract.hpp"              // IWYU pragma: export
#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#include "Utilities/Mpi/Mpi.hpp" // IWYU pragma: export
#include "Utilities/String/String.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InputDataNS { template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT> struct ExtractLeaf; }
namespace MoReFEM::InputDataNS { template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT> struct ExtractSection; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InputDataNS
{


    /*!
     * \brief An abstract class from which \a InputData and \a ModelSettings classes are built.
     *
     * The goal here is to provide functionalities related to a specific \a TupleT which is made of `Section`
     * and `Leaf` objects.
     *
     * \tparam TupleT Type of the underlying tuple, which  is made of `Section`
     * and `Leaf` objects.
     *
     */
    template<::MoReFEM::Concept::Tuple TupleT>
    class AbstractClass
    // clang-format on
    {

      public:
        //! Friendship.
        template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT>
        friend struct ::MoReFEM::InputDataNS::ExtractLeaf;

        //! Friendship.
        template<::MoReFEM::Advanced::Concept::InputDataNS::SectionType SectionT>
        friend struct ::MoReFEM::InputDataNS::ExtractSection;

      public:
        //! Self.
        using self = AbstractClass<TupleT>;

        //! Alias to smart pointers storing objects.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! The underlying tuple type.
        using underlying_tuple_type = TupleT;

        //! Size of the tuple.
        static constexpr std::size_t Size();

        /*!
         * \brief Returns the number of leaves in the tuple.
         *
         * It is not the same as size: this method explorates the sections to count how many leaves they include.
         *
         * \return Number of leaves found.
         */
        static constexpr std::size_t Nleaves();

        //! Special members.
        //@{

        /*!
         * \brief Constructor.
         */
        explicit AbstractClass();

        //! \copydoc doxygen_hide_copy_constructor
        AbstractClass(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AbstractClass(AbstractClass&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        AbstractClass& operator=(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AbstractClass& operator=(AbstractClass&& rhs) = default;


        //! Destructor.
        virtual ~AbstractClass() = 0;
        //@}


        /*!
         * \brief Print the list of input data that weren't used in the program.
         *
         * This method is dedicated to be called at destruction, but when using OpenMPI it is rather uneasy
         * to make it called in the destructor due to racing conditions (error if MPI_Finalize() has already
         * been called.
         *
         * So this method should be called at the very end of your program if you want the information
         * it gives.
         *
         * \copydoc doxygen_hide_stream_inout
         * If nothing added then all input data were used.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \internal <b><tt>[internal]</tt></b> This method collects the data from all mpi processors; the list
         * is therefore ensured to be exhaustive. \endinternal
         *
         */
        void PrintUnusedLeafs(std::ostream& stream, const ::MoReFEM::Wrappers::Mpi& mpi) const;

        /*!
         * \brief Print the complete identifiers of all the expected leaves expected in an input data tuple.
         *
         * An identifier being here the chaining of sections and subsections followed by the leaf name, with
         * '.' acting as a separator.
         *
         * For instance "Section1.SubsectionInSection1.LeafInSubSection1"
         *
         * \copydoc doxygen_hide_stream_inout
         */
        void PrintKeys(std::ostream& stream) const;


        /*!
         * \brief Extract  the complete identifiers of all the expected leaves expected in an input data tuple.
         *
         * An identifier being here the chaining of sections and subsections followed by the leaf name, with
         * '.' acting as a separator.
         *
         * For instance "Section1.SubsectionInSection1.LeafInSubSection1"
         *
         * \return The list of all extracted items from the tuple.
         */
        std::vector<std::string> ExtractKeys() const;


        /*!
         * \brief Whether \a ItemT is present in the tuple or not.
         *
         * \tparam ItemT Field sought in the tuple.
         *
         * \return True if present.
         */
        template<class ItemT>
        constexpr static bool Find();

        /*!
         * \brief Compute the list of all leaves that are in fact not used.
         *
         * This is a rather crude indicator: a leaf is considered 'used' if \a Extract facility is called upon it.
         * Leaf that are present indirectly because their enclosing section is given in the input data tuple are
         * correctly counted.
         * \attention In parallel, list is computed only on root processor. A leaf is there in this case only
         * if it is counted as unused in <strong>all</strong> of the ranks.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \return All the leafs not counted as used, sort in alphabetical order.
         */
        std::vector<std::string> ComputeUnusedLeafList(const ::MoReFEM::Wrappers::Mpi& mpi) const;


      protected:
        /*!
         * \brief Helper function used to extract value of a leaf.
         *
         * This should not be used directly by a user; \a ExtractLeaf class deals with it with a more
         * user-friendly interface (no pesky template keyword to add!).
         *
         * \tparam AbstractClassT AbstractClass class used to store the wanted input data.
         * \tparam CountAsUsedT Whether the call to the methods counts as an effective use of the input
         * parameter in the program. See CountAsUsed for more details; default value is fine
         * in almost all cases.
         *
         * \return The requested element in the tuple. It is either copied by value or a const reference.
         */
        // clang-format off
        template
        <
            ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafT,
            ::MoReFEM::InputDataNS::CountAsUsed CountAsUsedT = ::MoReFEM::InputDataNS::CountAsUsed::yes
        >
        auto ExtractLeaf() const
        -> typename Utilities::ConstRefOrValue<typename LeafT::return_type>::type;
        // clang-format on


        /*!
         * \brief Helper function used to extract a section.
         *
         * Currently it is not of much use and you shouldn't have to call it; however it might be handy in the future
         * if we need to extend the class.
         *
         * \tparam SectionT The section object we which to extract.
         */
        template<Advanced::Concept::InputDataNS::SectionType SectionT>
        auto& ExtractSection() const;


      private:
        /*!
         * \brief Throw an exception if there is a duplicate in the keys.
         *
         * The key for an AbstractClass A class is given by free function AbstractClassKey<A>().
         *
         * As these keys are what is used by \a OptionFile to read the value, it is important not to use
         * the same for two parameters.
         *
         * An additional bonus is that a same AbstractClass class put twice in the tuple will also trigger this
         * exception.
         */
        void CheckNoDuplicateKeysInTuple() const;


      public:
        //! Accessor to the underlying tuple.
        //! \return Underlying tuple.
        const underlying_tuple_type& GetTuple() const noexcept;

      protected:
        //! Non constant accessor to the underlying tuple.
        //! \return Underlying tuple.
        underlying_tuple_type& GetNonCstTuple();

        //! Helper object to iterate upon the tuple (including the in-depth sections).
        using tuple_iteration = Internal::InputDataNS::TupleIteration<underlying_tuple_type, 0ul>;

      private:
        //! The tuple that actually stores all the relevant quantities for the problem.
        underlying_tuple_type tuple_;
    };


} // namespace MoReFEM::Internal::InputDataNS

/// @} // addtogroup UtilitiesGroup


#include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hxx" // IWYU pragma: export

#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_INTERNAL_x_ABSTRACT_CLASS_x_ABSTRACT_CLASS_HPP_
