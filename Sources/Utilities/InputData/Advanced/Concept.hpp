/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CONCEPT_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CONCEPT_HPP_

#include <optional>

#include "Utilities/Miscellaneous.hpp"


namespace MoReFEM::Advanced::Concept::InputDataNS
{


    /*!
     * \brief Defines a concept to identify a type is a section of an \a InputData object.
     *
     */
    template<typename T>
    concept SectionType = requires {
        {
            T::ConceptIsSection == true
        };
    };


    /*!
     * \brief Defines a concept to identify a type is an indexed section of an \a InputData object.
     *
     */
    template<typename T>
    concept IndexedSectionType = requires {
        requires SectionType<T>;
        T::is_indexed_section == true;
    };


    /*!
     * \brief Defines a concept to identify a type is a leaf of an \a InputData object.
     *
     */
    template<typename T>
    concept LeafType = requires {
        {
            T::ConceptIsLeaf == true
        };
    };


    /*!
     * \brief Defines a concept that encompasses both sections and lead.
     *
     */
    template<class T>
    concept SectionOrLeafType = SectionType<T> || LeafType<T>;


} // namespace MoReFEM::Advanced::Concept::InputDataNS


/// @} // addtogroup UtilitiesGroup

#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CONCEPT_HPP_
