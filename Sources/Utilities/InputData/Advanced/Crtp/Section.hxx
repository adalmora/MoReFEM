/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Aug 2015 15:24:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_SECTION_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_SECTION_HXX_

// IWYU pragma: private, include "Utilities/InputData/Advanced/Crtp/Section.hpp"


namespace MoReFEM::Advanced::InputDataNS::Crtp
{


    template<class DerivedT, class EnclosingSectionT>
    constexpr Nature Section<DerivedT, EnclosingSectionT>::GetNature() noexcept
    {
        return Nature::section;
    }


    template<class DerivedT, class EnclosingSectionT>
    std::string Section<DerivedT, EnclosingSectionT>::GetIdentifier() noexcept
    {
        return DerivedT::GetName();
    }


    template<class DerivedT, class EnclosingSectionT>
    const std::string& Section<DerivedT, EnclosingSectionT>::GetFullName()
    {
        if constexpr (!std::is_same<EnclosingSectionT, NoEnclosingSection>())
        {
            static std::string ret = EnclosingSectionT::GetFullName() + "." + DerivedT::GetName();
            return ret;
        } else
        {
            static std::string ret = DerivedT::GetName();
            return ret;
        }
    }


    template<class DerivedT, class EnclosingSectionT>
    const auto& Section<DerivedT, EnclosingSectionT>::GetSectionContent() const noexcept
    {
        return static_cast<const DerivedT&>(*this).section_content_;
    }


    template<class DerivedT, class EnclosingSectionT>
    auto& Section<DerivedT, EnclosingSectionT>::GetNonCstSectionContent() noexcept
    {
        return static_cast<DerivedT&>(*this).section_content_;
    }


    template<class DerivedT, std::size_t IndexT, class TagT, class EnclosingSectionT>
    constexpr std::size_t IndexedSection<DerivedT, IndexT, TagT, EnclosingSectionT>::GetUniqueId() noexcept
    {
        return IndexT;
    }


    template<class DerivedT, std::size_t IndexT, class TagT, class EnclosingSectionT>
    std::string IndexedSection<DerivedT, IndexT, TagT, EnclosingSectionT>::GetName()
    {
        return DerivedT::BaseName() + std::to_string(IndexT);
    }


} // namespace MoReFEM::Advanced::InputDataNS::Crtp


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_SECTION_HXX_
