/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Aug 2013 10:53:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_LEAF_HXX_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_LEAF_HXX_

// IWYU pragma: private, include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"

#include "Utilities/String/EmptyString.hpp"


namespace MoReFEM::Advanced::InputDataNS::Crtp
{


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    const std::string& Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::GetEnclosingSection()
    {
        if constexpr (std::is_same<EnclosingSectionT, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>())
            return Utilities::EmptyString();
        else
        {
            static std::string ret = EnclosingSectionT::GetFullName() + ".";
            return ret;
        }
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    const std::string& Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::GetIdentifier()
    {
        static std::string ret = GetEnclosingSection() + DerivedT::NameInFile();
        return ret;
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    void Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::SetValue(return_type value)
    {
        value_ = std::move(value);
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    constexpr Nature Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::GetNature() noexcept
    {
        return Nature::leaf;
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    inline typename Utilities::ConstRefOrValue<ReturnTypeT>::type
    Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::GetTheValue() const
    {
        assert(value_.has_value()
               && "If this happens for an item in ModelSettings (rather than in InputData), check "
                  "ModelSettings::CheckTupleCompletelyFilled() was properly called after "
                  "ModelSettings::Init() (this is done automatically if you're using `Model::Main()`, "
                  "but if this occurs while writing a test it isn't).");
        return value_.value();
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    inline bool Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::HasValue() const noexcept
    {
        return value_.has_value();
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    inline void Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::SetAsUsed() const noexcept
    {
        is_used_ = true;
    }


    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    inline bool Leaf<DerivedT, EnclosingSectionT, ReturnTypeT>::IsUsed() const noexcept
    {
        return is_used_;
    }


    template<class DerivedT, class EnclosingSectionT, class TagT>
    std::string IndexedSectionDescription<DerivedT, EnclosingSectionT, TagT>::NameInFile()
    {
        return EnclosingSectionT::GetName() + ::MoReFEM::Internal::InputDataNS::IndexedSectionDescriptionSuffix();
    }


} // namespace MoReFEM::Advanced::InputDataNS::Crtp


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_CRTP_x_LEAF_HXX_
