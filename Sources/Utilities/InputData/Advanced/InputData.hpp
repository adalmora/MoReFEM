//! \file
//
// Created by Sébastien Gilles
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_INPUT_DATA_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_INPUT_DATA_HPP_

#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"    // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_INPUT_DATA_HPP_
