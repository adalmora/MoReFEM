/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_NO_ENCLOSING_SECTION_HPP_
#define MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_NO_ENCLOSING_SECTION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <string> // IWYU pragma: keep


namespace MoReFEM::Advanced::InputDataNS
{


    //! Enum to tell whether current item of input data class is a section or a leaf.
    enum class Nature { section = 0, leaf, undefined };


    /*!
     * \brief Placeholder class to use as one of the template parameter for sections and end parameters at root level.
     *
     * Should be used as Crtp::Section or Crtp::Leaf template argument.
     */
    struct NoEnclosingSection
    { };


} // namespace MoReFEM::Advanced::InputDataNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_INPUT_DATA_x_ADVANCED_x_NO_ENCLOSING_SECTION_HPP_
