/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#ifndef MOREFEM_x_UTILITIES_x_NUMERIC_x_EXCEPTIONS_x_NUMERIC_HPP_
#define MOREFEM_x_UTILITIES_x_NUMERIC_x_EXCEPTIONS_x_NUMERIC_HPP_

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM::ExceptionNS::NumericNS
{


    /*!
     * \brief Thrown when \a NumericNS::AreEqual is used on two vectors that don't share the same shape.
     *
     */
    class InconsistentVectors final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] lhs First vector considered.
         * \param[in] rhs Second vector considered.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InconsistentVectors(const LocalVector& lhs,
                                     const LocalVector& rhs,
                                     const char* invoking_file,
                                     int invoking_line);

        //! Destructor
        virtual ~InconsistentVectors() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentVectors(const InconsistentVectors& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentVectors(InconsistentVectors&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentVectors& operator=(const InconsistentVectors& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentVectors& operator=(InconsistentVectors&& rhs) = default;
    };


    /*!
     * \brief Thrown when \a NumericNS::AreEqual is used on two matrices that don't share the same shape.
     *
     */
    class InconsistentMatrices final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] lhs First matrix considered.
         * \param[in] rhs Second matrix considered.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InconsistentMatrices(const LocalMatrix& lhs,
                                      const LocalMatrix& rhs,
                                      const char* invoking_file,
                                      int invoking_line);

        //! Destructor
        virtual ~InconsistentMatrices() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentMatrices(const InconsistentMatrices& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentMatrices(InconsistentMatrices&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentMatrices& operator=(const InconsistentMatrices& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentMatrices& operator=(InconsistentMatrices&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::NumericNS


/// @} // addtogroup UtilitiesGroup


#endif // MOREFEM_x_UTILITIES_x_NUMERIC_x_EXCEPTIONS_x_NUMERIC_HPP_
