/*!
//
// \file
//
//
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/


#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Exceptions/Numeric.hpp"


namespace // anonymous
{


    // \tparam LocalLinearAlgebraT Either \a LocalVector or \a LocalMatrix.
    template<class LocalLinearAlgebraT>
    std::string InconsistentLinearAlgebraMsg(const LocalLinearAlgebraT& lhs, const LocalLinearAlgebraT& rhs);


} // namespace


namespace MoReFEM::ExceptionNS::NumericNS
{


    InconsistentVectors::~InconsistentVectors() = default;


    InconsistentVectors::InconsistentVectors(const LocalVector& lhs,
                                             const LocalVector& rhs,
                                             const char* invoking_file,
                                             int invoking_line)
    : MoReFEM::Exception(InconsistentLinearAlgebraMsg(lhs, rhs), invoking_file, invoking_line)
    { }


    InconsistentMatrices::~InconsistentMatrices() = default;


    InconsistentMatrices::InconsistentMatrices(const LocalMatrix& lhs,
                                               const LocalMatrix& rhs,
                                               const char* invoking_file,
                                               int invoking_line)
    : MoReFEM::Exception(InconsistentLinearAlgebraMsg(lhs, rhs), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::NumericNS


namespace // anonymous
{


    template<class LocalLinearAlgebraT>
    std::string InconsistentLinearAlgebraMsg(const LocalLinearAlgebraT& lhs, const LocalLinearAlgebraT& rhs)
    {
        using namespace MoReFEM;
        std::ostringstream oconv;

        static_assert(std::is_same<LocalLinearAlgebraT, LocalVector>()
                      || std::is_same<LocalLinearAlgebraT, LocalMatrix>());

        oconv << "NumericNS::AreEqual() facility is attempted upon two ";
        oconv << (std::is_same<LocalVector, LocalLinearAlgebraT>() ? "vectors" : "matrices");
        oconv << " that don't share the same "
                 "shape: first one is ";
        Utilities::PrintContainer<>::Do(lhs.shape(),
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("["),
                                        PrintNS::Delimiter::closer("]"));
        oconv << " whereas the second is ";
        Utilities::PrintContainer<>::Do(rhs.shape(),
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("["),
                                        PrintNS::Delimiter::closer("]"));

        return oconv.str();
    }


} // namespace


/// @} // addtogroup UtilitiesGroup
