/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup UtilitiesGroup
// \addtogroup UtilitiesGroup
// \{
*/

#include <array>
#include <cassert>
#include <cstddef>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


namespace MoReFEM::NumericNS
{


    namespace // anonymous
    {


        /*!
         * \brief Iterate through both underlying 1D arrays of Xtensor and evaluate whether both are equal or not.
         *
         *  \param[in] size Size of the underlying array. It is assumed the validity for both linear algebra was checked
         * prior to the function call. \tparam DataT The type of the underlying data as returned by\a data() method
         * (e.g. LocalMatrix::data()).
         */
        template<class DataT>
        bool AreUnderlyingXtensorDataEqual(std::size_t size, const DataT& lhs, const DataT& rhs, double epsilon);


    } // namespace


    bool AreEqual(const LocalVector& lhs, const LocalVector& rhs, double epsilon)
    {
        if (lhs.shape() != rhs.shape())
            throw ExceptionNS::NumericNS::InconsistentVectors(lhs, rhs, __FILE__, __LINE__);

        const auto size = lhs.shape(0);

        return AreUnderlyingXtensorDataEqual(size, lhs.data(), rhs.data(), epsilon);
    }


    bool IsZero(const LocalVector& vector, double epsilon) noexcept
    {
        return Wrappers::Xtensor::IsZeroVector(vector, epsilon);
    }


    bool AreEqual(const LocalMatrix& lhs, const LocalMatrix& rhs, double epsilon)
    {
        if (lhs.shape() != rhs.shape())
            throw ExceptionNS::NumericNS::InconsistentMatrices(lhs, rhs, __FILE__, __LINE__);

        assert(lhs.size() == rhs.size());

        return AreUnderlyingXtensorDataEqual(lhs.size(), lhs.data(), rhs.data(), epsilon);
    }


    bool IsZero(const LocalMatrix& matrix, double epsilon) noexcept
    {
        return Wrappers::Xtensor::IsZeroMatrix(matrix, epsilon);
    }


    namespace // anonymous
    {


        template<class DataT>
        bool AreUnderlyingXtensorDataEqual(std::size_t size, const DataT& lhs, const DataT& rhs, double epsilon)
        {
            for (auto i = 0ul; i < size; ++i)
            {
                if (!AreEqual(lhs[i], rhs[i], epsilon))
                    return false;
            }

            return true;
        }

    } // namespace

} // namespace MoReFEM::NumericNS


/// @} // addtogroup UtilitiesGroup
