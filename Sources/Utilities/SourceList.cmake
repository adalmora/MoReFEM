### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
		${CMAKE_CURRENT_LIST_DIR}/MatrixOrVector.hpp
		${CMAKE_CURRENT_LIST_DIR}/Miscellaneous.doxygen
		${CMAKE_CURRENT_LIST_DIR}/Miscellaneous.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/Utilities.doxygen
)

include(${CMAKE_CURRENT_LIST_DIR}/AsciiOrBinary/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Containers/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Datetime/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Environment/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Filesystem/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputData/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LinearAlgebra/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Mpi/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Mutex/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Numeric/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Singleton/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/SmartPointers/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/String/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/TimeKeep/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Type/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/UniqueId/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Warnings/SourceList.cmake)
