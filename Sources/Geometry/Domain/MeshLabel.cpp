/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep
#include <sstream>
#include <string>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Domain/MeshLabel.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM
{


    namespace // anonymous
    {


        std::string DescriptionHelper(const std::string& description, std::size_t index)
        {
            if (description.empty())
            {
                std::ostringstream oconv;
                oconv << "MeshLabel " << index;
                return oconv.str();
            }

            return description;
        }


    } // namespace


    MeshLabel::MeshLabel(const MeshNS::unique_id mesh_id, const std::size_t index, const std::string& description)
    : mesh_identifier_(mesh_id), index_(index), description_(DescriptionHelper(description, index))
    { }


    MeshLabel::~MeshLabel() = default;


    const std::string& MeshLabel::ClassName()
    {
        static std::string ret("MeshLabel");
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
