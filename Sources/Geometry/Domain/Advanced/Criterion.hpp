/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 11 Jul 2014 09:57:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_CRITERION_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_CRITERION_HPP_


namespace MoReFEM::Advanced::DomainNS
{


    /*!
     * \brief List of possible criteriathat might be used to describe a \a Domain.
     */
    enum class Criterion { Begin = 0, mesh = Begin, dimension, geometric_elt_type, label, End };


} // namespace MoReFEM::Advanced::DomainNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_CRITERION_HPP_
