/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 16 Apr 2017 22:29:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <unordered_map>
#include <vector>

#include "Utilities/InputData/Concept.hpp"   // IWYU pragma: export
#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Internal/LightweightDomainList.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainList.hpp"
#include "Geometry/Mesh/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { struct ClearSingletons; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    /*!
     * \brief Manager that is aware of all \a LightweightDomainListManager.
     *
     * Please notice that all \a Domain defined therein are known (and actually managed by the \a DomainManager).
     */
    class LightweightDomainListManager : public Utilities::Singleton<LightweightDomainListManager>
    {

      private:
        //! Convenient alias to avoid repeating the type.
        using storage_type = std::unordered_map<std::size_t, LightweightDomainList::const_unique_ptr>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        friend MoReFEM::TestNS::ClearSingletons;

      public:
        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::LightweightDomainListNS::Tag;

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        /*!
         * \brief Create a \a LightweightDomainList object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description, const ModelSettingsT& model_settings, const InputDataT& input_data);


        //! Fetch the domain object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{LightweightDomainList}
        const LightweightDomainList& GetLightweightDomainList(std::size_t unique_id) const;

        //! Fetch the domain region object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{LightweightDomainList}
        LightweightDomainList& GetNonCstLightweightDomainList(std::size_t unique_id);

        //! Constant accessor to the complete lightweight domain list list.
        const storage_type& GetLightweightDomainListStorage() const noexcept;

        //! Non constant accessor to the complete lightweight domain list list.
        storage_type& GetNonCstLightweightDomainListStorage() noexcept;

      private:
        /*!
         * \brief Method in charge of adding a new \a LightweightDomainList to the constructor.
         *
         * \copydetails doxygen_hide_lightweight_domain_list_constructor
         */
        void Create(std::size_t unique_id,
                    ::MoReFEM::MeshNS::unique_id mesh_index,
                    const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list,
                    const std::vector<std::size_t>& mesh_label_list,
                    const std::vector<std::size_t>& number_in_domain_list);

      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        LightweightDomainListManager();

        //! Destructor.
        virtual ~LightweightDomainListManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<LightweightDomainListManager>;
        ///@}

        ///  //! \copydoc doxygen_hide_manager_clear
        void Clear();


      private:
        //! Store the variable \a LightweightDomainList objects by their unique identifier.
        storage_type lightweight_domain_list_storage_;
    };


} // namespace MoReFEM::Advanced


/// @} // addtogroup GeometryGroup


#include "Geometry/Domain/Advanced/LightweightDomainListManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_ADVANCED_x_LIGHTWEIGHT_DOMAIN_LIST_MANAGER_HPP_
