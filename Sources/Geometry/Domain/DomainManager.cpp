/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <iosfwd>
#include <unordered_map>
#include <vector>

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"


namespace MoReFEM
{


    DomainManager::~DomainManager() = default;


    const std::string& DomainManager::ClassName()
    {
        static std::string ret("DomainManager");
        return ret;
    }


    DomainManager::DomainManager() = default;


    void DomainManager::Create(DomainNS::unique_id unique_id,
                               const std::vector<::MoReFEM::MeshNS::unique_id>& mesh_index,
                               const std::vector<std::size_t>& dimension_list,
                               const std::vector<std::size_t>& mesh_label_list,
                               const std::vector<Advanced::GeomEltNS::GenericName>& geometric_type_list)
    {
        auto&& ptr = Internal::WrapUniqueToConst(
            new Domain(unique_id, mesh_index, dimension_list, mesh_label_list, geometric_type_list));

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        decltype(auto) storage = GetNonCstStorage();

        auto [it, was_properly_inserted] = storage.insert(std::move(pair));

        if (!was_properly_inserted)
            throw Exception("Two Domain objects can't share the same unique identifier! (namely "
                                + std::to_string(unique_id.Get()) + ").",
                            __FILE__,
                            __LINE__);
    }


    const Domain&
    DomainManager::GetDomain(DomainNS::unique_id unique_id, const char* invoking_file, int invoking_line) const
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        if (it == storage.cend())
            throw Exception(
                "Domain " + std::to_string(unique_id.Get()) + " is not defined!", invoking_file, invoking_line);

        assert(!(!(it->second)));

        return *(it->second);
    }


    void DomainManager::Clear()
    {
        decltype(auto) storage = GetNonCstStorage();
        storage.clear();
        Domain::ClearUniqueIdList();
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
