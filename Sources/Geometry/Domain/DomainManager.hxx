/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 20 Mar 2015 16:39:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Domain/DomainManager.hpp"

#include <algorithm>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"
#include "Utilities/Type/StrongType/Internal/Convert.hpp"

#include "Geometry/Domain/UniqueId.hpp"
#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void DomainManager::Create(const IndexedSectionDescriptionT&, const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) mesh_index_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::MeshIndexList>(
                input_data, model_settings);
        decltype(auto) dimension_list =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::DimensionList>(
                input_data, model_settings);
        decltype(auto) mesh_label_list =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::MeshLabelList>(
                input_data, model_settings);
        decltype(auto) geometric_type_list =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::GeomEltTypeList>(
                input_data, model_settings);

        std::vector<Advanced::GeomEltNS::GenericName> geom_list(geometric_type_list.size());

        std::transform(geometric_type_list.cbegin(),
                       geometric_type_list.cend(),
                       geom_list.begin(),
                       [](const auto& str)
                       {
                           return Advanced::GeomEltNS::GenericName(str);
                       });

        auto mesh_index_list = Internal::StrongTypeNS::Convert<::MoReFEM::MeshNS::unique_id>(mesh_index_list_as_int);

        Create(DomainNS::unique_id{ section_type::GetUniqueId() },
               mesh_index_list,
               dimension_list,
               mesh_label_list,
               geom_list);
    }


    inline Domain&
    DomainManager::GetNonCstDomain(DomainNS::unique_id unique_id, const char* invoking_file, int invoking_line)
    {
        return const_cast<Domain&>(GetDomain(unique_id, invoking_file, invoking_line));
    }


    inline void DomainManager::CreateLightweightDomain(DomainNS::unique_id unique_id,
                                                       MeshNS::unique_id mesh_index,
                                                       const std::vector<std::size_t>& mesh_label_list)
    {
        Create(unique_id, { mesh_index }, {}, mesh_label_list, {});
    }


    inline auto DomainManager::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    inline auto DomainManager::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


    template<class EnumT>
    constexpr DomainNS::unique_id AsDomainId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return DomainNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_DOMAIN_x_DOMAIN_MANAGER_HXX_
