/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 7 Oct 2014 15:22:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/EnumInterface.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterfaceNS { enum class Nature : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterfaceNS
{


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT, Nature NatureT>
    constexpr std::size_t Ninterface<TopologyT, NatureT>::Value()
    {
        return NatureT == Nature::vertex
                   ? TopologyT::Nvertex
                   : (NatureT == Nature::edge ? TopologyT::Nedge
                                              : (NatureT == Nature::face ? TopologyT::Nface : TopologyT::Nvolume));

        // Note: I personnally like better the more verbosy form below, but gcc can't cope with it (whereas
        // to my knowledge it's perfectly acceptable C++ 14).
        //            switch (NatureT)
        //            {
        //                case Nature::vertex:
        //                    return TopologyT::Nvertex;
        //                case Nature::edge:
        //                    return TopologyT::Nedge;
        //                case Nature::face:
        //                    return TopologyT::Nface;
        //                case Nature::volume:
        //                    return TopologyT::Nvolume;
        //            }
        //
        //            assert(false);
        //            return NumericNS::UninitializedIndex<std::size_t>();
    }


} // namespace MoReFEM::InterfaceNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_ENUM_INTERFACE_HXX_
