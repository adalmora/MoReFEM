/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 2 Oct 2014 14:16:17 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_INTERNAL_x_IS_ON_LOCAL_INTERFACE_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_INTERNAL_x_IS_ON_LOCAL_INTERFACE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/LocalData.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief Whether the \a coords are on the vertex_index -th vertex.
     *
     * \tparam TopologyT Topology considered.
     *
     * \param[in] local_vertex_index Index of the vertex considered; must be in [0, TopologyT::Nvertex[.
     * \param[in] local_coords \a LocalCoords which position is under scrutiny.
     *
     * \return True of the \a LocalCoords is on the vertex pointed by \a local_vertex_index.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);


    /*!
     * \brief Whether the \a coords are on the edge_index -th edge for a segment, quadrangle or hexahedron.
     *
     * \tparam TopologyT Topology of a spectral based element (i.e. segment, quadrangle or hexahedron).
     *
     * \param[in] local_edge_index Index of the edge considered; must be in [0, TopologyT::Nedge[.
     * \param[in] local_coords \a LocalCoords which position is under scrutiny.
     *
     * \return True of the \a LocalCoords is on the edge pointed by \a local_edge_index.
     * Coords of a vertex on the edge would also returns true here.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    bool IsOnEdge_Spectral(std::size_t local_edge_index, const LocalCoords& local_coords);

} // namespace MoReFEM::Internal::InterfaceNS


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_LOCAL_INTERFACE_x_INTERNAL_x_IS_ON_LOCAL_INTERFACE_HPP_
