/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 9 Sep 2015 10:47:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <cassert>
#include <cstdlib>
#include <type_traits> // IWYU pragma: keep

#include "Geometry/Interfaces/LocalInterface/Advanced/LocalInterface.hpp"


namespace MoReFEM::Advanced::InterfaceNS
{


    [[noreturn]] LocalInterface::LocalInterface(std::false_type,
                                                ::MoReFEM::InterfaceNS::Nature nature,
                                                IsInterior interior)
    : nature_(nature), is_interior_(interior)
    {
        assert(false && "Here only for compile requirements; should never be actually called!");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Advanced::InterfaceNS


/// @} // addtogroup GeometryGroup
