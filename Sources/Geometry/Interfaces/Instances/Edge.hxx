/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 25 Mar 2014 11:23:13 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Instances/Edge.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp"

// IWYU pragma: no_forward_declare MoReFEM::Edge


namespace MoReFEM
{


    template<class LocalEdgeContentT>
    Edge::Edge(const LocalEdgeContentT& local_edge_content, const Coords::vector_shared_ptr& elt_coords_list)
    : Internal::InterfaceNS::TInterface<Edge, InterfaceNS::Nature::edge>(local_edge_content, elt_coords_list)
    { }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INSTANCES_x_EDGE_HXX_
