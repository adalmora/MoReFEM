/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Interfaces/Instances/OrientedFace.hpp"


namespace MoReFEM
{


    const std::string& OrientedFace::ClassName()
    {
        static std::string ret("OrientedFace");
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
