/*!
//
// \file
//
//
// Created by Sebastien Gilles
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_x_TRAITS_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_x_TRAITS_HPP_

// IWYU pragma: private, include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::InterfaceNS::Traits
{


    /*!
     * \brief Traits class for oriented interface.
     */
    template<class InterfaceT>
    struct OrientedInterface;


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    template<>
    struct OrientedInterface<Vertex>
    {
        using type = Vertex;
    };


    template<>
    struct OrientedInterface<Volume>
    {
        using type = Volume;
    };


    template<>
    struct OrientedInterface<Edge>
    {
        using type = OrientedEdge;
    };


    template<>
    struct OrientedInterface<OrientedEdge>
    {
        using type = OrientedEdge;
    };


    template<>
    struct OrientedInterface<Face>
    {
        using type = OrientedFace;
    };


    template<>
    struct OrientedInterface<OrientedFace>
    {
        using type = OrientedFace;
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::InterfaceNS::Traits


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORIENTATION_x_TRAITS_HPP_
