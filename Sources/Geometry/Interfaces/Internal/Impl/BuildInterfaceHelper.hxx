/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HXX_

// IWYU pragma: private, include "Geometry/Interfaces/Internal/Impl/BuildInterfaceHelper.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/Interfaces/Advanced/LocalData.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


namespace MoReFEM::Internal::InterfaceNS::Impl
{


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    Vertex::shared_ptr
    CreateNewInterface<Vertex, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                   std::size_t local_vertex_index)
    {
        const std::size_t index = local_vertex_index;
        assert(index < coords_in_geometric_elt.size());

        return std::make_shared<Vertex>(coords_in_geometric_elt[index]);
    }


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    Edge::shared_ptr
    CreateNewInterface<Edge, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                 std::size_t local_edge_index)
    {
        const auto& local_edge = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetEdge(local_edge_index);

        return std::make_shared<Edge>(local_edge, coords_in_geometric_elt);
    }


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    Face::shared_ptr
    CreateNewInterface<Face, TopologyT>::Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                                 std::size_t local_face_index)
    {
        const auto& local_face = ::MoReFEM::Advanced::InterfaceNS::LocalData<TopologyT>::GetFace(local_face_index);

        return std::make_shared<Face>(local_face, coords_in_geometric_elt);
    }


} // namespace MoReFEM::Internal::InterfaceNS::Impl


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HXX_
