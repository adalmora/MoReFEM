/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Instances/OrientedEdge.hpp"
#include "Geometry/Interfaces/Instances/OrientedFace.hpp"
#include "Geometry/Interfaces/Instances/Vertex.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class GeometricElt; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::InterfaceNS::Impl
{


    /*!
     * \brief Helper class to create a new Vertex, Edge or Face interface...
     *
     * ... given the list of coordinates in the geometric element and the index of the
     * chosen interface in the local topologic object.
     *
     * \tparam InterfaceT An Interface object, which derives from the namesake base class. Not
     * OrientedEdge or OrientedFace: orientation is relevant only at GeometricElt scope, whereas
     * here we create Interfaces at mesh scope.
     *
     * For instance,
     * \code
     * CreateNewInterface<Edge, TopologyNS::Triangle>::Perform(coords_in_geom_elt, 2)
     * \endcode
     * will create the third edge of the geometric element which coordinates are given as first argument.
     */
    template<class InterfaceT, ::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    struct CreateNewInterface;

    //! \cond IGNORE_BLOCK_IN_DOXYGEN


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    struct CreateNewInterface<Vertex, TopologyT>
    {


        static Vertex::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                          std::size_t local_vertex_index);
    };


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    struct CreateNewInterface<Edge, TopologyT>
    {


        static Edge::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                        std::size_t local_edge_index);
    };


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    struct CreateNewInterface<Face, TopologyT>
    {


        static Face::shared_ptr Perform(const Coords::vector_shared_ptr& coords_in_geometric_elt,
                                        std::size_t local_face_index);
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::InterfaceNS::Impl


/// @} // addtogroup GeometryGroup


#include "Geometry/Interfaces/Internal/Impl/BuildInterfaceHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_IMPL_x_BUILD_INTERFACE_HELPER_HPP_
