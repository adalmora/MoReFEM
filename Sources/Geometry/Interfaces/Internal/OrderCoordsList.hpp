//! \file
//
//
//  OrderCoordsList.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 14/04/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORDER_COORDS_LIST_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORDER_COORDS_LIST_HPP_

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief Order the list of \a Coords
     *
     * \param[in,out] coords_list List of \a Coords that delimits an interface. In output, the value are sort according to the algorithm below.
     *
     * If more than 2, we look at its two closest neighbours (with a modulo if at the end of the list) because
     * we also want the second element to be lower than the last.
     * The reason is the following: consider the quadrangle 3 1 2 0.
     * Our rotate would yield: 0 3 1 2.
     * However, if later we meet 0 2 1 3, it is also the same quadrangle, with a different orientation...
     * but orientation is dealt with elsewhere; here we do not want it. Hence the following step
     * to ensure a unique sequence to identify the geometric element.
     *
     * \param[in] functor The functor used to compare items of the \a coords_list. The two usual choices are either the default one if
     * the vector is made of \a Coords::shared_ptr, or std::less<StrongTypeT> where \a StrongTypeT is a \a StrongType
     * related to an index of \a Coords.
     *
     * \internal This was previously a method but I need it elsewhere for \a FromCoordsMatching implementation.
     */
    template<class CoordsVectorT, class FunctorT = Utilities::PointerComparison::Less<Coords::shared_ptr>>
    void OrderCoordsList(CoordsVectorT& coords_list,
                         FunctorT&& functor = Utilities::PointerComparison::Less<Coords::shared_ptr>());


} // namespace MoReFEM::Internal::InterfaceNS

#include "Geometry/Interfaces/Internal/OrderCoordsList.hxx"

#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_INTERNAL_x_ORDER_COORDS_LIST_HPP_
