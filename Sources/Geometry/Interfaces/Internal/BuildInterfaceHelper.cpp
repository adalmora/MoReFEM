/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 16 Jan 2014 14:39:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include "Geometry/Interfaces/Internal/BuildInterfaceHelper.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    template<>
    const Vertex::vector_shared_ptr& GetInterfaceOfGeometricElt<Vertex>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetVertexList();
    }


    template<>
    const OrientedEdge::vector_shared_ptr&
    GetInterfaceOfGeometricElt<OrientedEdge>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetOrientedEdgeList();
    }


    template<>
    const OrientedFace::vector_shared_ptr&
    GetInterfaceOfGeometricElt<OrientedFace>(const GeometricElt& geometric_element)
    {
        return geometric_element.GetOrientedFaceList();
    }


} // namespace MoReFEM::Internal::InterfaceNS


/// @} // addtogroup GeometryGroup
