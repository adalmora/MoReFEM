/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Mar 2014 12:43:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <sstream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Interface.hpp"
#include "Geometry/Interfaces/Internal/OrderCoordsList.hpp"


namespace MoReFEM
{


    Interface::Interface(const Coords::shared_ptr& coords) : coords_list_({ coords })
    {
        assert(!(!coords));
    }


    Interface::Interface()
    { }


    Interface::~Interface() = default;


    void Interface::SetCoordsList(const Coords::vector_shared_ptr& value)
    {
        coords_list_ = value;
        Internal::InterfaceNS::OrderCoordsList(coords_list_);
    }


    std::string ShortHand(const Interface& interface)
    {
        std::ostringstream oconv;

        switch (interface.GetNature())
        {
        case InterfaceNS::Nature::vertex:
            oconv << 'V';
            break;
        case InterfaceNS::Nature::edge:
            oconv << 'E';
            break;
        case InterfaceNS::Nature::face:
            oconv << 'F';
            break;
        case InterfaceNS::Nature::volume:
            oconv << 'G';
            break;
        case InterfaceNS::Nature::none:
        case InterfaceNS::Nature::undefined:
            assert(false && "This method should not be called for such objects.");
            exit(EXIT_FAILURE);
        }

        oconv << interface.GetProgramWiseIndex();

        const auto ret = oconv.str();
        return ret;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
