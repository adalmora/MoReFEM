/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERFACES_x_STRONG_TYPE_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERFACES_x_STRONG_TYPE_HPP_

#include <cstddef>                                                   // IWYU pragma: keep
                                                                     // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::InterfaceNS
{

    //! Strong type for displacement vectors.
    // clang-format off
    using program_wise_index_type =
        StrongType
        <
            std::size_t,
            struct program_wise_index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Hashable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::AsMpiDatatype,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::InterfaceNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERFACES_x_STRONG_TYPE_HPP_
