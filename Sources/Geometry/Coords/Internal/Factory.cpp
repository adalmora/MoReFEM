/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Aug 2017 17:16:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"


namespace MoReFEM::Internal::CoordsNS
{


    Coords::shared_ptr Factory::Origin()
    {
        return Internal::WrapShared(new Coords);
    }


    Coords::shared_ptr Factory::FromComponents(double x, double y, double z, const double space_unit)
    {
        return Internal::WrapShared(new Coords(x, y, z, space_unit));
    }


    Coords::shared_ptr Factory::FromStream(std::size_t Ncoor, std::istream& stream, const double space_unit)
    {
        return Internal::WrapShared(new Coords(Ncoor, stream, space_unit));
    }


} // namespace MoReFEM::Internal::CoordsNS


/// @} // addtogroup GeometryGroup
