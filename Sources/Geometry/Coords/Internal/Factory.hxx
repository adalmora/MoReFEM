/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Aug 2017 17:16:29 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_

// IWYU pragma: private, include "Geometry/Coords/Internal/Factory.hpp"

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Coords/Coords.hpp"


namespace MoReFEM::Internal::CoordsNS
{


    template<typename T>
    Coords::shared_ptr Factory::FromArray(T&& value, const double space_unit)
    {
        return Internal::WrapShared(new Coords(value, space_unit));
    }


} // namespace MoReFEM::Internal::CoordsNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_INTERNAL_x_FACTORY_HXX_
