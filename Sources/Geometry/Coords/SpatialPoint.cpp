/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 21 Aug 2017 18:24:57 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#include <array>
#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"


namespace MoReFEM
{


    SpatialPoint::~SpatialPoint() = default;


    SpatialPoint::SpatialPoint()
    {
        coordinate_list_.fill(0.);
    }


    SpatialPoint::SpatialPoint(double x, double y, double z, const double space_unit)
    : coordinate_list_({ { x * space_unit, y * space_unit, z * space_unit } })
    { }


    SpatialPoint::SpatialPoint(std::size_t Ncoor, std::istream& stream, const double space_unit)
    {
        assert(Ncoor <= 3u);

        std::array<double, 3ul> buf_coordinates;
        buf_coordinates.fill(0.);

        auto initialPosition = stream.tellg();

        for (std::size_t i = 0ul; i < Ncoor; ++i)
        {
            stream >> buf_coordinates[i];
            buf_coordinates[i] *= space_unit;
        }

        if (stream)
        {
            // Modify point only if failbit not set.
            coordinate_list_ = buf_coordinates;
        } else
        {
            // In case of failure, put back the cursor at the point it was before trying unsuccessfully to read a point
            auto state = stream.rdstate();
            stream.clear();
            stream.seekg(initialPosition);
            stream.setstate(state);
        }
    }


    void SpatialPoint::Print(std::ostream& out) const
    {
        Utilities::PrintContainer<>::Do(coordinate_list_,
                                        out,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));
    }


    double Distance(const SpatialPoint& point1, const SpatialPoint& point2)
    {
        double sum = 0.;

        for (auto i = 0ul; i < 3u; ++i)
            sum += NumericNS::Square(point1[i] - point2[i]);

        return std::sqrt(sum);
    }


    void SpatialPoint::Reset()
    {
        coordinate_list_.fill(0.);
    }


    std::ostream& operator<<(std::ostream& stream, const SpatialPoint& point)
    {
        point.Print(stream);
        return stream;
    }


} // namespace MoReFEM


/// @} // addtogroup GeometryGroup
