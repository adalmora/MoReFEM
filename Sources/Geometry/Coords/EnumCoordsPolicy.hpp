/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_COORDS_x_ENUM_COORDS_POLICY_HPP_
#define MOREFEM_x_GEOMETRY_x_COORDS_x_ENUM_COORDS_POLICY_HPP_


namespace MoReFEM::CoordsNS
{


    //! Policy to use for the coordinates to choose between cartesian and generalized ones for operators involving
    //! Hyperelasticity/Viscoelasticity/Active policies. The default case is cartesian.
    enum class CoordsPolicy { cartesian, generalized };


} // namespace MoReFEM::CoordsNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_COORDS_x_ENUM_COORDS_POLICY_HPP_
