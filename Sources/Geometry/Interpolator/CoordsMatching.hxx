/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 11:09:00 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_

// IWYU pragma: private, include "Geometry/Interpolator/CoordsMatching.hpp"


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Geometry/CoordsMatchingFile.hpp" // IWYU pragma: export

#include "Geometry/Mesh/UniqueId.hpp"


namespace MoReFEM::MeshNS::InterpolationNS
{


    inline const CoordsMatching::mapping_type& CoordsMatching::GetSourceToTarget() const noexcept
    {
        assert(source_to_target_.size() == target_to_source_.size());
        return source_to_target_;
    }


    inline const CoordsMatching::mapping_type& CoordsMatching::GetTargetToSource() const noexcept
    {
        assert(source_to_target_.size() == target_to_source_.size());
        return target_to_source_;
    }


    inline MeshNS::unique_id CoordsMatching::GetSourceMeshId() const noexcept
    {
        assert(mesh_ids_[0] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_[0];
    }


    inline MeshNS::unique_id CoordsMatching::GetTargetMeshId() const noexcept
    {
        assert(mesh_ids_[1] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_[1];
    }


    inline const std::array<MeshNS::unique_id, 2ul>& CoordsMatching::GetMeshIds() const noexcept
    {
        assert(mesh_ids_[0] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        assert(mesh_ids_[1] != NumericNS::UninitializedIndex<MeshNS::unique_id>());
        return mesh_ids_;
    }


} // namespace MoReFEM::MeshNS::InterpolationNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_COORDS_MATCHING_HXX_
