/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup CoreGroup
 // \addtogroup CoreGroup
 // \{
 */


#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"

#include <cstddef> // IWYU pragma: keep
#include <filesystem>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"


namespace MoReFEM::Internal::MeshNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void
    CoordsMatchingManager ::Create(const IndexedSectionDescriptionT&, const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) path = ::MoReFEM::FilesystemNS::File{ std::filesystem::path{
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Path>(
                input_data, model_settings) } };

        Create(path,
               ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::DoComputeReverse>(
                   input_data, model_settings));
    }


    inline auto CoordsMatchingManager::GetList() const -> const managed_type::vector_const_unique_ptr&
    {
        return list_;
    }


    inline auto CoordsMatchingManager::GetNonCstList() -> managed_type::vector_const_unique_ptr&
    {
        return const_cast<managed_type::vector_const_unique_ptr&>(GetList());
    }

} // namespace MoReFEM::Internal::MeshNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HXX_
