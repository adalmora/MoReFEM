/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup CoreGroup
 // \addtogroup CoreGroup
 // \{
 */

// IWYU pragma: no_include <iosfwd>

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <string>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/SmartPointers/Internal/Wrap.hpp"

#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS
{

    namespace // anonymous
    {


        void CheckNoDuplicate(const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr& new_elt,
                              const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching::vector_const_unique_ptr& list);


    } // namespace


    CoordsMatchingManager::~CoordsMatchingManager() = default;


    const std::string& CoordsMatchingManager::ClassName()
    {
        static std::string ret("CoordsMatchingManager");
        return ret;
    }


    void CoordsMatchingManager::Create(const ::MoReFEM::FilesystemNS::File& interpolation_file,
                                       const bool do_compute_reverse)
    {
        {
            decltype(auto) list = GetNonCstList();
            auto ptr =
                Internal::WrapUniqueToConst(new ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching(interpolation_file));

            CheckNoDuplicate(ptr, list);
            list.emplace_back(std::move(ptr));
        }

        if (do_compute_reverse)
        {
            decltype(auto) list = GetNonCstList();
            const auto& straight = list_.back();
            auto reversed_ptr = straight->GenerateReverseCoordsMatching();
            CheckNoDuplicate(reversed_ptr, list);

            list.emplace_back(std::move(reversed_ptr));
        }
    }


    auto CoordsMatchingManager::GetCoordsMatching(::MoReFEM::MeshNS::unique_id source_mesh_id,
                                                  ::MoReFEM::MeshNS::unique_id target_mesh_id) -> const managed_type&
    {
        decltype(auto) list = GetList();
        const auto end = list.cend();

        auto it = std::find_if(list.cbegin(),
                               end,
                               [source_mesh_id, target_mesh_id](const auto& ptr)
                               {
                                   assert(!(!ptr));
                                   return ptr->GetSourceMeshId() == source_mesh_id
                                          && ptr->GetTargetMeshId() == target_mesh_id;
                               });

        if (it == end)
        {
            std::ostringstream oconv;
            oconv << "A CoordsMatching involving mesh " << source_mesh_id << " as source and " << target_mesh_id
                  << " as target was sought but not found. Please check the data in your Lua file provides a "
                     "CoordsMatchingFile that relates to both these identifier (eventually in the  opposite order if "
                     "reverse was set to  true).";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }

        const auto& ptr = *it;

        assert(!(!ptr));
        return *ptr;
    }


    void CoordsMatchingManager::Clear()
    {
        GetNonCstList().clear();
    }


    namespace // anonymous
    {


        void CheckNoDuplicate(const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr& new_elt,
                              const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching::vector_const_unique_ptr& list)
        {
            assert(!(!new_elt));
            decltype(auto) mesh_ids = new_elt->GetMeshIds();

            for (const auto& elt_ptr : list)
            {
                assert(!(!elt_ptr));
                const auto& elt = *elt_ptr;

                if (elt.GetMeshIds() == mesh_ids)
                    throw Exception("Invalid CoordsMatchingFiles in the Lua file: at least both of the files given "
                                    "in the 'path' field (taking into account possible reverse option) tackle the "
                                    "same source/target meshes identifier.",
                                    __FILE__,
                                    __LINE__);
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::MeshNS


/// @} // addtogroup CoreGroup
