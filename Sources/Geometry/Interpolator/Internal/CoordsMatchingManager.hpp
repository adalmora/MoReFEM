/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup CoreGroup
 // \addtogroup CoreGroup
 // \{
 */


#ifndef MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/InputData/Concept.hpp"   // IWYU pragma: export
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/Internal/CoordsMatchingFile.hpp"

#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Mesh/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::TestNS { struct ClearSingletons; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS
{


    /*!
     * \brief Object that is aware of all existing \a ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching.
     *
     * \internal <b><tt>[internal]</tt></b> Contrary to other managers, this one is really meant to be
     * hidden to users and developers: ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching should be queried against \a
     * GodOfDof objects. \endinternal
     */
    class CoordsMatchingManager : public Utilities::Singleton<CoordsMatchingManager>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = ::MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS::Tag;

        //! Convenient alias around the type that is managed by this singleton.
        using managed_type = ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        friend MoReFEM::TestNS::ClearSingletons;

      public:
        /*!
         * \brief Create a \a CoordsMatchingFile object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description, const ModelSettingsT& model_settings, const InputDataT& input_data);

      public:
        /*!
         * \brief Get access to the list of existing numbering subset.
         *
         * \internal This method is public solely because of its occasional usefulness in debug; you shouldn't
         * have to use it while writing a \a Model.
         * \endinternal
         *
         * \return List of pointers to the \a ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching available throughout the program.
         */
        const managed_type::vector_const_unique_ptr& GetList() const;

        /*!
         * \brief Find the \a Coords Matching with given source and mesh id.
         *
         * An exception is thrown if none found.
         *
         * \param[in] source_mesh_id Unique identifier of the 'source' \a Mesh (or \a GodOfDof obviously)
         * \param[in] target_mesh_id Unique identifier of the 'target' \a Mesh (or \a GodOfDof obviously)
         *
         * \return Constant reference to the proper \a CoordsMatching.
         */
        const managed_type& GetCoordsMatching(::MoReFEM::MeshNS::unique_id source_mesh_id,
                                              ::MoReFEM::MeshNS::unique_id target_mesh_id);


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        CoordsMatchingManager() = default;

        //! Destructor.
        virtual ~CoordsMatchingManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CoordsMatchingManager>;
        ///@}

        //! \copydoc doxygen_hide_manager_clear
        void Clear();


      private:
        /*!
         * \brief Create a new ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching object.
         *
         * \param[in] interpolation_file Path of the interpolation file.
         * \param[in] do_compute_reverse In the interpolation file, there are two meshes involved: one 'source' and one
         * 'target'. If this field is set to true, two CoordsMatching objects are created:
         * one source -> target and one target -> source. If false, only the former is built.
         */
        void Create(const ::MoReFEM::FilesystemNS::File& interpolation_file, bool do_compute_reverse);

        //! Get non constant access to the list of existing numbering subset.
        managed_type::vector_const_unique_ptr& GetNonCstList();


      private:
        //! Store the ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching objects by their unique identifier.
        managed_type::vector_const_unique_ptr list_;
    };


} // namespace MoReFEM::Internal::MeshNS


/// @} // addtogroup CoreGroup


#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_MANAGER_HPP_
