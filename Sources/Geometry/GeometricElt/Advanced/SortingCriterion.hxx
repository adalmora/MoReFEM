/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HXX_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HXX_

// IWYU pragma: private, include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"


#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Domain/MeshLabel.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion
{

    template<class StrictOrderingOperatorT>
    inline MeshLabel::const_shared_ptr SurfaceRef<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
    {
        return ptr->GetMeshLabelPtr();
    }


    template<class StrictOrderingOperatorT>
    inline std::size_t Dimension<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
    {
        return ptr->GetDimension();
    };


    template<class StrictOrderingOperatorT>
    inline Advanced::GeometricEltEnum Type<StrictOrderingOperatorT>::Value(const GeometricElt::shared_ptr& ptr)
    {
        return ptr->GetIdentifier();
    }


} // namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HXX_
