/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr> on the Thu, 17 Jan 2013 10:43:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HPP_


#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Domain/MeshLabel.hpp"

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion
{


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = Utilities::PointerComparison::Less<MeshLabel::const_shared_ptr>>
    struct SurfaceRef
    {
        //! Call the appropriate method of \a GeometricElt that returns the label.
        //! \param[in] ptr \a GeometricElt from which dimension (by \a GeometricElt::GetMeshLabelPtr()) will
        //! be extracted.
        inline static MeshLabel::const_shared_ptr Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = std::less<std::size_t>>
    struct Dimension
    {

        //! Call the appropriate method of \a GeometricElt that returns the dimension.
        //! \param[in] ptr \a GeometricElt from which dimension (by \a GeometricElt::GetDimension()) will
        //! be extracted.
        inline static std::size_t Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


    //! Used as a template parameter of Utilities::Sort (see this free function for much more details!)
    template<class StrictOrderingOperatorT = std::less<Advanced::GeometricEltEnum>>
    struct Type
    {
        //! Call the appropriate method of GeometricElt that returns the type of geometric element.
        //! \param[in] ptr \a GeometricElt from which type (by \a GeometricElt::GetIdentifier()) will
        //! be extracted.
        inline static Advanced::GeometricEltEnum Value(const GeometricElt::shared_ptr& ptr);

        //! Operator used for the sorting criterion.
        using StrictOrderingOperator = StrictOrderingOperatorT;
    };


} // namespace MoReFEM::Advanced::GeometricEltNS::SortingCriterion


/// @} // addtogroup GeometryGroup


#include "Geometry/GeometricElt/Advanced/SortingCriterion.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_ADVANCED_x_SORTING_CRITERION_HPP_
