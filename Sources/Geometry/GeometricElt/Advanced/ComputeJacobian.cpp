/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 1 Jun 2016 10:13:09 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup GeometryGroup
 // \addtogroup GeometryGroup
 // \{
 */

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>
// IWYU pragma: no_include <type_traits>

#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/ComputeJacobian.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"


namespace MoReFEM::Advanced::GeomEltNS
{


    namespace // anonymous
    {

        //! Helper object to compute relevant value for a given \a LocalNode.
        struct Helper
        {

          public:
            /*!
             * \brief Constructor.
             */
            Helper(const GeometricElt& geometric_elt,
                   const LocalCoords& local_coords,
                   std::vector<double>& first_derivate_shape_function_for_local_node);

            //! Compute the relevant quantity from first derivate shape function for each \a LocalNode.
            const std::vector<double>& ComputeForLocalNode(const LocalNodeNS::index_type local_node_index);

          private:
            const GeometricElt& geom_elt_;

            const LocalCoords& local_coords_;

            std::vector<double>& first_derivate_shape_function_for_local_node_;
        };


    } // namespace


    ComputeJacobian::ComputeJacobian(const std::size_t mesh_dimension)
    {
        jacobian_.resize({ mesh_dimension, mesh_dimension });
        work_first_derivate_shape_fct_for_local_node_.resize(mesh_dimension);
    }


    const LocalMatrix& ComputeJacobian::Compute(const GeometricElt& geometric_elt, const LocalCoords& local_coords)
    {
        const LocalNodeNS::index_type Ncoords{ geometric_elt.Ncoords() };

        auto& jacobian = GetNonCstJacobian();

        Helper first_derivate_contribution_helper(
            geometric_elt, local_coords, GetNonCstWorkFirstDerivateShapeFctForLocalNode());

        const auto Nshape_function = Advanced::ComponentNS::index_type{ jacobian.shape(1) };

        assert(geometric_elt.GetDimension() <= Nshape_function.Get());
        jacobian.fill(0.);

        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Ncoords; ++local_node_index)
        {
            decltype(auto) first_derivate_shape_function_for_local_node =
                first_derivate_contribution_helper.ComputeForLocalNode(local_node_index);

            const auto& coords_in_geom_elt = geometric_elt.GetCoord(local_node_index.Get());

            // If LocalCoords is less than geometric element dimension, columns above its dimension are filled
            // with zeros.
            for (ComponentNS::index_type component{ 0ul }; component < ComponentNS::index_type{ Nshape_function };
                 ++component) // \todo 1640 Investigate here!
            {
                for (std::size_t shape_fct_index = 0ul; shape_fct_index < Nshape_function.Get(); ++shape_fct_index)
                {
                    jacobian(component.Get(), shape_fct_index) +=
                        coords_in_geom_elt[component.Get()]
                        * first_derivate_shape_function_for_local_node[shape_fct_index];
                }
            }

            // \todo #446
            if (geometric_elt.GetIdentifier() == Advanced::GeometricEltEnum::Point1)
            {
                jacobian(0, 0) = 1;
            }
        }

        return jacobian;
    }


    namespace // anonymous
    {


        Helper::Helper(const GeometricElt& geometric_elt,
                       const LocalCoords& local_coords,
                       std::vector<double>& first_derivate_shape_function_for_local_node)
        : geom_elt_(geometric_elt), local_coords_(local_coords),
          first_derivate_shape_function_for_local_node_(first_derivate_shape_function_for_local_node)
        { }


        const std::vector<double>& Helper::ComputeForLocalNode(const LocalNodeNS::index_type local_node_index)
        {
            const auto Ncomponent = Advanced::ComponentNS::index_type{ geom_elt_.GetDimension() };

            auto& ret = first_derivate_shape_function_for_local_node_;

            std::fill(ret.begin(), ret.end(), 0.);

            for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                ret[component.Get()] = geom_elt_.FirstDerivateShapeFunction(local_node_index, component, local_coords_);

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Advanced::GeomEltNS


/// @} // addtogroup GeometryGroup
