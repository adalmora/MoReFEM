/*!
//
// \file
//
//
// Created by Sebastien Gilles <srpgilles@gmail.com> on the Tue, 5 Feb 2013 11:59:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_REGISTER_HXX_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_REGISTER_HXX_

// IWYU pragma: private, include "Geometry/GeometricElt/Internal/Register.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Exceptions/Factory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/Mesh/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { enum class GeometricEltEnum : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    template<class GeometricEltT>
    void RegisterEnsight(
        Advanced::GeometricEltFactory::CreateGeometricEltCallBackIstream ensight_create,
        Advanced::GeometricEltFactory::CallBackEnsight& call_back_ensight,
        std::unordered_map<Advanced::GeomEltNS::EnsightName, Advanced::GeometricEltEnum>& ensight_name_matching)

    {
        decltype(auto) ensight_name =
            Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Ensight,
                                                GeometricEltT::traits::Identifier()>::EnsightName();


        if (!ensight_create || !call_back_ensight.insert(std::make_pair(ensight_name, ensight_create)).second)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(ensight_name.Get(), "ensight", __FILE__, __LINE__));

        auto [it, inserted] =
            ensight_name_matching.insert(std::pair(ensight_name, GeometricEltT::traits::Identifier()));

        static_cast<void>(inserted);

        assert(inserted);
    }


    template<class GeometricEltT>
    void RegisterMedit(Advanced::GeometricEltFactory::CreateGeometricEltCallBack default_create,
                       Advanced::GeometricEltFactory::CallBackMedit& call_back_medit,
                       std::map<GmfKwdCod, std::size_t>& Nnode_medit)
    {
        auto medit_id = Internal::MeshNS::FormatNS::Support<::MoReFEM::MeshNS::Format::Medit,
                                                            GeometricEltT::traits::Identifier()>::MeditId();

        if (!call_back_medit.insert(std::make_pair(medit_id, default_create)).second)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(
                GeometricEltT::traits::ClassName().Get(), "medit_id", __FILE__, __LINE__));

        const std::size_t Ncoords = GeometricEltT::traits::Ncoords;

        if (!Nnode_medit.insert(std::make_pair(medit_id, Ncoords)).second)
            ThrowBeforeMain(ExceptionNS::Factory::UnableToRegister(
                GeometricEltT::traits::ClassName().Get(), "medit_id", __FILE__, __LINE__));
    }


} // namespace MoReFEM::Internal


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INTERNAL_x_REGISTER_HXX_
