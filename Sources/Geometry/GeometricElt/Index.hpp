/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien@orque.saclay.inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INDEX_HPP_
#define MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INDEX_HPP_

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export

namespace MoReFEM::GeomEltNS
{


    /*!
     * \brief Strong type to represent the \a DirichletBoundaryCondition unique id
     */
    // clang-format off
    using index_type =
    StrongType
    <
        std::size_t,
        struct IndexTypeTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::Incrementable,
        StrongTypeNS::Addable,
        StrongTypeNS::AsMpiDatatype
    >;
    // clang-format on


} // namespace MoReFEM::GeomEltNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_GEOMETRIC_ELT_x_INDEX_HPP_
