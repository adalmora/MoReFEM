/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FiniteElementGroup
// \addtogroup FiniteElementGroup
// \{
*/

#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_ALIAS_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_ALIAS_HPP_

#include <cstddef> // IWYU pragma: keep

// IWYU pragma: begin_exports
#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp"
// IWYU pragma: end_exports

namespace MoReFEM::LocalNodeNS // #1817 Should probably be in Advanced namespace. If not, move this file outside of
                               // Advanced directory.
{

    //! Strong type for \a LocalNode index.
    // clang-format off
    using index_type =
        StrongType
        <
            std::size_t,
            struct index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible,
            StrongTypeNS::Divisible
        >;

    // clang-format on


} // namespace MoReFEM::LocalNodeNS


/// @} // addtogroup FiniteElementGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_LOCAL_NODE_x_ALIAS_HPP_
