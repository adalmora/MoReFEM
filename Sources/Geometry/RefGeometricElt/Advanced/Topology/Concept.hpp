/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_TOPOLOGY_x_CONCEPT_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_TOPOLOGY_x_CONCEPT_HPP_

#include "Utilities/Miscellaneous.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class LocalCoords; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Concept
{


    /*!
     * \brief Defines a concept to identify topology class
     *
     * Such as MoReFEM::RefGeomEltNS::TopologyNS::Triangle class.
     *
     * \internal I didn't introduce a specific variable to check exactly this is a topology class, but chose
     * to use instead several methods that are expected in such a class.
     */
    template<typename T>
    concept TopologyTraitsClass = requires {
        typename T::EdgeTopology;
        typename T::EdgeContent;
        typename T::FaceTopology;
        typename T::FaceContent;
    };


    /*!
     * \brief Helper type used in defining classes \a OrientedEdge and \a OrientedFace
     *
     * Both this classes are non-template classes... but needs in the constructor data from a template
     * holding topology data (which must respect the concept \a Concept::TopologyTraitsClass.
     *
     * A common trick to do so is to use an indirection: an additional argument is passed that
     * gives the information indirectly. This way, only the constructor is templated, not the whole
     * class (see \a Utilities::Type2Type for a much more detailed explanation).
     *
     * Concept defined here ensure the token passed to \a OrientedEdge and \a OrientedFace constructor
     * is valid (this check is performed at compile time).
     *
     * \tparam Type2TypeT A Type2Type class which template parameter is a class which satisfies
     * the \a Concept::TopologyTraitsClass. For instance Utilities::Type2Type<RefGeomEltNS::TopologyNS::Triangle>.
     */
    template<typename Type2TypeT>
    concept TopologyIndexedSectionDescriptionType = requires {
        {
            Concept::TopologyTraitsClass<typename Type2TypeT::type>
        };
        {
            Utilities::IsSpecializationOf<Utilities::Type2Type, Type2TypeT>()
        };
    };


} // namespace MoReFEM::Concept


/// @} // addtogroup GeometryGroup

#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_ADVANCED_x_TOPOLOGY_x_CONCEPT_HPP_
