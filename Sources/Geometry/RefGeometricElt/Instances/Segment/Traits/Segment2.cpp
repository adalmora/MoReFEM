/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Segment/Traits/Segment2.hpp"


namespace MoReFEM::RefGeomEltNS::Traits
{


    const Advanced::GeomEltNS::GenericName& Segment2::ClassName()
    {
        static Advanced::GeomEltNS::GenericName ret("Segment2");
        return ret;
    }


} // namespace MoReFEM::RefGeomEltNS::Traits


/// @} // addtogroup GeometryGroup
