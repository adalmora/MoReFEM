/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <iosfwd> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Hexahedron/Format/Hexahedron20.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    const Advanced::GeomEltNS::EnsightName&
    Support<::MoReFEM::MeshNS::Format::Ensight, Advanced::GeometricEltEnum::Hexahedron20>::EnsightName()
    {
        static Advanced::GeomEltNS::EnsightName ret("hexa20");
        return ret;
    };


} // namespace MoReFEM::Internal::MeshNS::FormatNS


/// @} // addtogroup GeometryGroup
