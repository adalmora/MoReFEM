/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Traits/Tetrahedron4.hpp"


namespace MoReFEM::RefGeomEltNS::Traits
{


    const Advanced::GeomEltNS::GenericName& Tetrahedron4::ClassName()
    {
        static Advanced::GeomEltNS::GenericName ret("Tetrahedron4");
        return ret;
    }


} // namespace MoReFEM::RefGeomEltNS::Traits


/// @} // addtogroup GeometryGroup
