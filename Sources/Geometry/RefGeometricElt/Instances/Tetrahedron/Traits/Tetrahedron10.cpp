/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 09:23:03 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Traits/Tetrahedron10.hpp"


namespace MoReFEM::RefGeomEltNS::Traits
{


    const Advanced::GeomEltNS::GenericName& Tetrahedron10::ClassName()
    {
        static Advanced::GeomEltNS::GenericName ret("Tetrahedron10");
        return ret;
    }


} // namespace MoReFEM::RefGeomEltNS::Traits


/// @} // addtogroup GeometryGroup
