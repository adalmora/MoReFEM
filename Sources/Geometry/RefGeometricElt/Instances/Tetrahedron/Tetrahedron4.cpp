/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 20 Mar 2014 09:44:10 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron4.hpp"


namespace MoReFEM::RefGeomEltNS
{


    Tetrahedron4::~Tetrahedron4() = default;


} // namespace MoReFEM::RefGeomEltNS


/// @} // addtogroup GeometryGroup
