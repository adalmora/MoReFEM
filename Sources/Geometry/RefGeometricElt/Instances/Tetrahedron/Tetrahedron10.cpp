/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 09:23:03 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron10.hpp"


namespace MoReFEM::RefGeomEltNS
{


    Tetrahedron10::~Tetrahedron10() = default;


} // namespace MoReFEM::RefGeomEltNS


/// @} // addtogroup GeometryGroup
