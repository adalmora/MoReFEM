/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 25 Sep 2014 17:20:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Point/Topology/Point.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"


namespace MoReFEM::RefGeomEltNS::TopologyNS
{


    const std::string& Point::ClassName()
    {
        static std::string ret("Point");
        return ret;
    }


    bool Point::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Point>(vertex_index, coords);
    }


    [[noreturn]] bool Point::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        static_cast<void>(edge_index);
        static_cast<void>(coords);
        assert(false && "Should never be called!");
        throw; // to avoid warning.
    }


    [[noreturn]] bool Point::IsOnFace(std::size_t face_index, const LocalCoords& coords)
    {
        static_cast<void>(face_index);
        static_cast<void>(coords);
        assert(false && "Should never be called!");
        throw; // to avoid warning.
    }


    [[noreturn]] LocalCoords
    Point::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
    {
        static_cast<void>(face_index);
        static_cast<void>(coords);
        static_cast<void>(orientation);

        assert(false && "Should never be called!");
        throw; // to avoid warning.
    }


    const std::vector<Point::EdgeContent>& Point::GetEdgeList()
    {
        static std::vector<Point::EdgeContent> empty;
        return empty;
    }


    const std::vector<Point::FaceContent>& Point::GetFaceList()
    {
        static std::vector<Point::FaceContent> empty;
        return empty;
    }


    const std::vector<LocalCoords>& Point::GetQ1LocalCoordsList()
    {
        static std::vector<LocalCoords> ret{ { LocalCoords(std::vector<double>(1, 0.)) } };

        return ret;
    }


    InterfaceNS::Nature Point::GetInteriorInterface()
    {
        return InterfaceNS::Nature::vertex;
    }


    ::MoReFEM::TopologyNS::Type Point::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::point;
    }


    bool Point::IsInside(const LocalCoords& coords)
    {
        assert(GetQ1LocalCoordsList().size() == 1);
        return coords == GetQ1LocalCoordsList().back();
    }


} // namespace MoReFEM::RefGeomEltNS::TopologyNS


/// @} // addtogroup GeometryGroup
