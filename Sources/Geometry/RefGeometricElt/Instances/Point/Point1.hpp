/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_

#include "Geometry/RefGeometricElt/Instances/Point/Traits/Point1.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp" // IWYU pragma: export


namespace MoReFEM::RefGeomEltNS
{


    /*!
     * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Point1.
     *
     * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
     * store in one dynamic container all the kinds of GeometricElt present in a mesh.
     *
     * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
     * can be included in:
     *
     * \code
     * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
     * \endcode
     *
     */
    class Point1 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Point1>
    {
      public:
        //! Constructor.
        Point1() = default;

        //! Destructor.
        virtual ~Point1() override;

        //! \copydoc doxygen_hide_copy_constructor
        Point1(const Point1& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Point1(Point1&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Point1& operator=(const Point1& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Point1& operator=(Point1&& rhs) = delete;


      private:
        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
    };


} // namespace MoReFEM::RefGeomEltNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_POINT_x_POINT1_HPP_
