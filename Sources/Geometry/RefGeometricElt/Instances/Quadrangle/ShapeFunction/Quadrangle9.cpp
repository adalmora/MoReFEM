/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 24 Mar 2014 09:01:06 +0100
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup GeometryGroup
 // \addtogroup GeometryGroup
 // \{
 */

#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle9.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"


namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS
{


    const std::array<ShapeFunctionType, 9>& Quadrangle9::ShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 9> ret{
            { [](const auto& local)
              {
                  return 0.25 * (1. - local.r()) * (1. - local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. + local.r()) * (1. - local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * (1. + local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * (1. + local.s()) * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r() * local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * (1. - local.s() * local.s()) * local.r();
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * (1. - local.s() * local.s()) * local.r();
              },
              [](const auto& local)
              {
                  return (1. - local.r()) * (1. + local.r()) * (1. - local.s()) * (1. + local.s());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 18>& Quadrangle9::FirstDerivateShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 18> ret{
            { [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.r() * local.s()
                         + 0.25 * (1. - local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * local.r() * local.s()
                         + 0.25 * (1. - local.r()) * (1. - local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.r() * local.s()
                         - 0.25 * (1. + local.r()) * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * local.r() * local.s()
                         - 0.25 * (1. + local.r()) * (1. - local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.r() * local.s()
                         + 0.25 * (1. + local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + local.r()) * local.r() * local.s()
                         + 0.25 * (1. + local.r()) * (1. + local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.r() * local.s()
                         - 0.25 * (1. - local.r()) * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - local.r()) * local.r() * local.s()
                         - 0.25 * (1. - local.r()) * (1. + local.s()) * local.r();
              },

              [](const auto& local)
              {
                  return local.r() * local.s() * (1. - local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (local.s() - (1. - local.s()));
              },

              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * local.s() * (1. + local.r());
              },

              [](const auto& local)
              {
                  return -local.r() * local.s() * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 0.5 * (1. - local.r() * local.r()) * (local.s() + (1. + local.s()));
              },

              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return local.r() * local.s() * (1. - local.r());
              },

              [](const auto& local)
              {
                  return -2. * local.r() * (1. - local.s()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return -2. * local.s() * (1. - local.r()) * (1. + local.r());
              } }
        };

        return ret;
    };


    const std::array<ShapeFunctionType, 36>& Quadrangle9::SecondDerivateShapeFunctionList()
    {

        static std::array<ShapeFunctionType, 36> ret{
            { [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.s() - 0.25 * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * local.r() * (-2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - 2. * local.r()) * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return -0.25 * (1. - local.s()) * local.s() - 0.25 * (1. - local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return -0.25 * local.r() * (-2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + 2. * local.r()) * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.s() + 0.25 * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * local.r() * (2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return 0.25 * (1. + 2. * local.r()) * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return 0.5 * (1. + local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return 0.25 * (1. + local.s()) * local.s() + 0.25 * (1. + local.s()) * local.s();
              },
              [](const auto& local)
              {
                  return 0.25 * local.r() * (2. * local.s() + 1.);
              },
              [](const auto& local)
              {
                  return -0.25 * (1. - 2. * local.r()) * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return -0.5 * (1. - local.r()) * local.r();
              },

              [](const auto& local)
              {
                  return local.s() * (1. - local.s());
              },
              [](const auto& local)
              {
                  return local.r() * (1. - 2. * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (local.s() - (1. - local.s()));
              },
              [](const auto& local)
              {
                  return 1. - local.r() * local.r();
              },

              [](const auto& local)
              {
                  return 0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. + local.r());
              },
              [](const auto& local)
              {
                  return -local.s() * (1. + 2. * local.r());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. + local.r());
              },

              [](const auto& local)
              {
                  return -local.s() * (1. + local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (1. + 2. * local.s());
              },
              [](const auto& local)
              {
                  return -local.r() * (local.s() + (1. + local.s()));
              },
              [](const auto& local)
              {
                  return 1. - local.r() * local.r();
              },

              [](const auto& local)
              {
                  return 0.5 * (1. - local.s() * local.s());
              },
              [](const auto& local)
              {
                  return local.s() * (1. - local.r());
              },
              [](const auto& local)
              {
                  return local.s() * (1. - 2. * local.r());
              },
              [](const auto& local)
              {
                  return local.r() * (1. - local.r());
              },

              [](const auto& local)
              {
                  return -2. * (1. - local.s()) * (1. + local.s());
              },
              [](const auto& local)
              {
                  return 4. * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return 4. * local.r() * local.s();
              },
              [](const auto& local)
              {
                  return -2. * (1. - local.r()) * (1. + local.r());
              } }
        };

        return ret;
    };


} // namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS


/// @} // addtogroup GeometryGroup
