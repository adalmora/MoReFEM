/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TRAITS_x_QUADRANGLE8_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TRAITS_x_QUADRANGLE8_HPP_


#include <cstddef> // IWYU pragma: keep


#include "Geometry/GeometricElt/Advanced/FormatStrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Format/Quadrangle8.hpp"        // IWYU pragma: export
#include "Geometry/RefGeometricElt/Instances/Quadrangle/ShapeFunction/Quadrangle8.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"       // IWYU pragma: export
// < absolutely required to let MoReFEM know which format are actually supported!

#include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"


namespace MoReFEM::RefGeomEltNS::Traits
{


    /*!
     * \brief Traits class that holds the static functions related to shape functions, interface and topology.
     *
     * It can't be instantiated directly: its purpose is either to provide directly a data through
     * static function:
     *
     * \code
     * constexpr auto Nshape_function = Quadrangle8::NshapeFunction();
     * \endcode
     *
     * or to be a template parameter to MoReFEM::RefGeomEltNS::Quadrangle8 class (current class is in an
     * additional layer of namespace):
     *
     * \code
     * MoReFEM::RefGeomEltNS::Traits::Quadrangle8
     * \endcode
     *
     */
    class Quadrangle8 final : public ::MoReFEM::Internal::RefGeomEltNS::
                                  RefGeomEltImpl<Quadrangle8, ShapeFunctionNS::Quadrangle8, TopologyNS::Quadrangle>
    {
      protected:
        //! Convenient alias.
        using self = Quadrangle8;

        /// \name Special members: prevent direct instantiation of RefGeomEltImpl objects.
        ///@{

        //! Constructor.
        Quadrangle8() = delete;

        //! Destructor.
        ~Quadrangle8() = delete;

        //! \copydoc doxygen_hide_copy_constructor
        Quadrangle8(const Quadrangle8& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Quadrangle8(Quadrangle8&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Quadrangle8& operator=(const Quadrangle8& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Name associated to the RefGeomElt.
         *
         * \return Name that is guaranteed to be unique (through the GeometricEltFactory) and can
         * therefore also act as an identifier.
         */
        static const Advanced::GeomEltNS::GenericName& ClassName();

        //! Number of Coords required to describe fully a GeometricElt of this type.
        enum : std::size_t { Ncoords = 8u };

        /*!
         * \brief Enum associated to the RefGeomElt.
         *
         * \return Enum value guaranteed to be unique (through the GeometricEltFactory); its
         * raison d'être is that many operations are much faster on an enumeration than on a string.
         */
        static constexpr MoReFEM::Advanced::GeometricEltEnum Identifier()
        {
            return MoReFEM::Advanced::GeometricEltEnum::Quadrangle8;
        }


      private:
        // THIS IS A TRAIT CLASS, NO MEMBERS ALLOWED HERE!
    };


} // namespace MoReFEM::RefGeomEltNS::Traits


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_QUADRANGLE_x_TRAITS_x_QUADRANGLE8_HPP_
