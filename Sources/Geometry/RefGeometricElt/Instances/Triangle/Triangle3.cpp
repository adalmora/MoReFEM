/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 09:23:03 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include "Geometry/RefGeometricElt/Instances/Triangle/Triangle3.hpp"


namespace MoReFEM::RefGeomEltNS
{


    Triangle3::~Triangle3() = default;


} // namespace MoReFEM::RefGeomEltNS


/// @} // addtogroup GeometryGroup
