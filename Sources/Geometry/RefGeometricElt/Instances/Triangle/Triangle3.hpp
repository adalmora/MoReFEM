/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 18 Mar 2014 15:17:56 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE3_HPP_
#define MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE3_HPP_


#include "Geometry/RefGeometricElt/Instances/Triangle/Traits/Triangle3.hpp"
#include "Geometry/RefGeometricElt/Internal/RefGeomElt/TRefGeomElt.hpp"


namespace MoReFEM::RefGeomEltNS
{


    /*!
     * \brief Acts as a strawman class for MoReFEM::RefGeomEltNS::Traits::Triangle3.
     *
     * The limitation with the traits class is that we can't use it polymorphically; we can't for instance
     * store in one dynamic container all the kinds of GeometricElt present in a mesh.
     *
     * That is the role of the following class: it derives polymorphically from RefGeomElt, and therefore
     * can be included in:
     *
     * \code
     * RefGeomElt::vector_shared_ptr geometric_types_in_mesh_;
     * \endcode
     *
     */

    class Triangle3 final : public ::MoReFEM::Internal::RefGeomEltNS::TRefGeomElt<Traits::Triangle3>
    {
      public:
        //! Constructor.
        Triangle3() = default;

        //! Destructor.
        virtual ~Triangle3() override;

        //! \copydoc doxygen_hide_copy_constructor
        Triangle3(const Triangle3& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Triangle3(Triangle3&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Triangle3& operator=(const Triangle3& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Triangle3& operator=(Triangle3&& rhs) = delete;


      private:
        // THIS CLASS IS NOT INTENDED TO HOLD DATA MEMBERS; please read its description first if you want to...
    };


} // namespace MoReFEM::RefGeomEltNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_REF_GEOMETRIC_ELT_x_INSTANCES_x_TRIANGLE_x_TRIANGLE3_HPP_
