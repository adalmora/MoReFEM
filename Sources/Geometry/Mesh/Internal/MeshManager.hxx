/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 15 Apr 2016 23:01:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Internal/MeshManager.hpp"

// IWYU pragma: no_include <__tree>

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <optional>
#include <sstream>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp" 

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void MeshManager::Create(const IndexedSectionDescriptionT&, const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) mesh_file = ::MoReFEM::FilesystemNS::File{ std::filesystem::path(
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Path>(
                input_data, model_settings)) };
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Dimension>(
                input_data, model_settings);
        const auto format = FormatNS::GetType(
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Format>(
                input_data, model_settings));
        const auto space_unit =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::SpaceUnit>(
                input_data, model_settings);

        Create(::MoReFEM::MeshNS::unique_id{ section_type::GetUniqueId() },
               mesh_file,
               dimension,
               format,
               space_unit,
               Mesh::BuildEdge::yes,
               Mesh::BuildFace::yes,
               Mesh::BuildVolume::yes,
               Mesh::BuildPseudoNormals::no,
               std::nullopt); // \todo #57
    }


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    void MeshManager::LoadFromPrepartitionedData(const IndexedSectionDescriptionT&,
                                                 const ModelSettingsT& model_settings,
                                                 const MoReFEMDataT& morefem_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) input_data = morefem_data.GetInputData();

        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Dimension>(
                input_data, model_settings);

        const auto format = FormatNS::GetType(
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::Format>(
                input_data, model_settings));

        decltype(auto) parallelism_ptr = morefem_data.GetParallelismPtr();

        assert(!(!parallelism_ptr)
               && "This method should not be called in no parallelism defined in input "
                  "data file!");

        decltype(auto) parallelism_dir = parallelism_ptr->GetDirectory();

        std::ostringstream oconv;
        oconv << "Mesh_" << section_type::GetUniqueId();

        ::MoReFEM::FilesystemNS::Directory mesh_dir(
            parallelism_dir, std::filesystem::path{ oconv.str() }, ::MoReFEM::FilesystemNS::behaviour::read);

        mesh_dir.ActOnFilesystem(__FILE__, __LINE__);

        const auto lua_file = mesh_dir.AddFile("mesh_data.lua");

        ::MoReFEM::Wrappers::Lua::OptionFile prepartitioned_data(lua_file, __FILE__, __LINE__);

        const auto mesh_file = mesh_dir.AddFile("mesh.mesh");

        LoadFromPrepartitionedData(morefem_data.GetMpi(),
                                   ::MoReFEM::MeshNS::unique_id{ section_type::GetUniqueId() },
                                   mesh_file,
                                   prepartitioned_data,
                                   dimension,
                                   format);
    }


    inline Mesh& MeshManager::GetNonCstMesh(::MoReFEM::MeshNS::unique_id unique_id)
    {
        return const_cast<Mesh&>(GetMesh(unique_id));
    }


    inline const MeshManager::storage_type& MeshManager ::GetStorage() const noexcept
    {
        return storage_;
    }


    inline MeshManager::storage_type& MeshManager ::GetNonCstStorage() noexcept
    {
        return const_cast<storage_type&>(GetStorage());
    }


} // namespace MoReFEM::Internal::MeshNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_MESH_MANAGER_HXX_
