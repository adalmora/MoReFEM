//! \file
//
//
//  CreateMeshDataDirectory.cpp
//  MoReFEM
//
//  Created by sebastien on 09/06/2020.
// Copyright © 2020 Inria. All rights reserved.
//

// IWYU pragma: no_include <__tree>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"


namespace MoReFEM::Internal::MeshNS
{


    std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr>
    CreateMeshDataDirectory(const ::MoReFEM::FilesystemNS::Directory& output_directory)
    {
        std::map<::MoReFEM::MeshNS::unique_id, ::MoReFEM::FilesystemNS::Directory::const_unique_ptr> ret;

        decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);
        decltype(auto) mesh_id_list = mesh_manager.GetStorage();

        for (const auto& [mesh_id, mesh_ptr] : mesh_id_list)
        {
            auto new_subdir = std::make_unique<::MoReFEM::FilesystemNS::Directory>(
                output_directory, std::string("Mesh_") + std::to_string(mesh_id.Get()));

            new_subdir->ActOnFilesystem(__FILE__, __LINE__);

            ret.insert({ mesh_id, std::move(new_subdir) });
        }

        return ret;
    }


} // namespace MoReFEM::Internal::MeshNS
