/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 13 Jun 2016 17:23:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/


#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <type_traits>
#include <vector>

#include "Utilities/InputData/Extract.hpp"   // IWYU pragma: keep
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Geometry/PseudoNormals.hpp" // IWYU pragma: keep

#include "Geometry/Domain/UniqueId.hpp"           // IWYU pragma: export
#include "Geometry/Mesh/Internal/MeshManager.hpp" // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::TestNS { struct ClearSingletons; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    /*!
     * \brief Manager of all PseudoNormal objects.
     */
    class PseudoNormalsManager : public Utilities::Singleton<PseudoNormalsManager>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = PseudoNormalsManager;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        friend MoReFEM::TestNS::ClearSingletons;

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        //! As in it is not used in a  Manager class, we don't need this for the time being.
        using indexed_section_tag = std::false_type;

        /*!
         * \brief Create a new PseudoNormals object from the data of the input data file.
         *
         *
         * \param[in] section Section from the input data file that gives away characteristics of the
         * pseudo-normal to build.
         */
        template<class PseudoNormalsSectionT>
        void Create(const PseudoNormalsSectionT& section);


        /*!
         * \brief Compute PseudoNormals.
         *
         * \param[in] domain_index_list List of domains to consider. If empty, no restriction on domain.
         * \param[in,out] mesh Mesh for which pseudo-normals are built.
         *
         *
         * \internal <b><tt>[internal]</tt></b> This method is public because it is handy for some test executables;
         * however in full-fledged model instances you should not use this constructor at all: the other one above
         * calls it with the correct data from the input data file.
         * \endinternal
         */
        void Create(const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list, Mesh& mesh);


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        PseudoNormalsManager();

        //! Destructor.
        virtual ~PseudoNormalsManager() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<PseudoNormalsManager>;

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        ///@}
    };


} // namespace MoReFEM::Internal


/// @} // addtogroup GeometryGroup


#include "Geometry/Mesh/Internal/PseudoNormalsManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_PSEUDO_NORMALS_MANAGER_HPP_
