/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Format.hpp"

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_INFORMATION_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_INFORMATION_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/Mesh/Format.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MeshNS::FormatNS { template <::MoReFEM::MeshNS::Format TypeT> struct Information; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    /*!
     * \brief Specialization of Information struct that provides generic information about Ensight format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Ensight>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Ensight files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about Medit format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Medit>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Medit files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about
     * VTK_PolygonalData format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the VTK_PolygonalData files.
        static const std::string& Extension();
    };


    /*!
     * \brief Specialization of Information struct that provides generic information about Medit format.
     */
    template<>
    struct Information<::MoReFEM::MeshNS::Format::Vizir>
    {

        //! Name of the format.
        static const std::string& Name();

        //! Extension of the Medit files.
        static const std::string& Extension();
    };


} // namespace MoReFEM::Internal::MeshNS::FormatNS


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_INFORMATION_HPP_
