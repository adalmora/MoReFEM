/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Format.hpp"

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>


#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::MeshNS::FormatNS::VTK_PolygonalData
{


    /*!
     * \brief Read a VTK_PolygonalData geometric file.
     *
     * It reads VTK_PolygonalData format file; there are absolutely no guarantee it is able to read any
     * valid VTK_PolygonalData file (on the contrary, I'm positive it does not). However it is able to
     * read the VTK_PolygonalData files likely to be given as input in MoReFEM.
     *
     * An VTK_PolygonalData case file is also required to read it with VTK_PolygonalData client.
     *
     * \param[in,out] unsort_element_list List of \a GeometricElt. No specific order is expected here.
     * \param[in,out] coords_list List of \a Coords objects.
     * \param[in,out] mesh_label_list List of \a MeshLabels.
     * \copydoc doxygen_hide_mesh_constructor_4
     * \copydoc doxygen_hide_space_unit_arg
     * \param[out] dimension Dimension of the mesh, as read in the input file.
     * \param[in] mesh_id Unique identifier of the \a Mesh for the construction of which
     * present function is called.
     *
     */
    [[noreturn]] void ReadFile(::MoReFEM::MeshNS::unique_id mesh_id,
                               const ::MoReFEM::FilesystemNS::File& mesh_file,
                               double space_unit,
                               std::size_t& dimension,
                               GeometricElt::vector_shared_ptr& unsort_element_list,
                               Coords::vector_shared_ptr& coords_list,
                               MeshLabel::vector_const_shared_ptr& mesh_label_list);


    /*!
     * \brief Write a mesh in VTK_PolygonalData format.
     *
     * \copydoc doxygen_hide_geometry_format_write_common_arg
     */
    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file);


} // namespace MoReFEM::Internal::MeshNS::FormatNS::VTK_PolygonalData


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_V_T_K_x_POLYGONAL_DATA_HPP_
