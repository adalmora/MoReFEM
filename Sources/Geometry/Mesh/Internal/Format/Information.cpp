/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

#include <string>

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Information.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS
{


    const std::string& Information<::MoReFEM::MeshNS::Format::Ensight>::Name()
    {
        static std::string ret("Ensight");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Ensight>::Extension()
    {
        static std::string ret("geo");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Medit>::Name()
    {
        static std::string ret("Medit");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Medit>::Extension()
    {
        static std::string ret("mesh");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Vizir>::Name()
    {
        static std::string ret("Vizir");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::Vizir>::Extension()
    {
        static std::string ret("mesh");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Name()
    {
        static std::string ret("VTK_PolygonalData");
        return ret;
    }


    const std::string& Information<::MoReFEM::MeshNS::Format::VTK_PolygonalData>::Extension()
    {
        static std::string ret("vtk");
        return ret;
    }


} // namespace MoReFEM::Internal::MeshNS::FormatNS


/// @} // addtogroup GeometryGroup
