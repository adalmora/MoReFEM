/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 18 Apr 2016 15:56:09 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup GeometryGroup
// \addtogroup GeometryGroup
// \{
*/

// IWYU pragma: private, include "Geometry/Mesh/Internal/Format/Format.hpp"

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_
#define MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <map>
#include <optional>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MeshNS::FormatNS::Medit
{

    /*!
     * \brief Read a mesh in Medit format.
     *
     * \copydoc doxygen_hide_mesh_constructor_1_bis
     * \copydoc doxygen_hide_mesh_constructor_3_bis
     * \copydoc doxygen_hide_mesh_constructor_4
     * \param[in] Nprocessor_wise_geom_elt_per_type Number of processor-wise \a GeometricElt sort per
     * \a RefGeomElt. This optional parameter is required only when we rebuild from prepartitioned data;
     it is used
     * to tag whether a given \a GeometricElt is processor-wise or ghost.

     */
    void ReadFile(const ::MoReFEM::MeshNS::unique_id mesh_id,
                  const ::MoReFEM::FilesystemNS::File& mesh_file,
                  double space_unit,
                  std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_geom_elt_per_type,
                  std::size_t& dimension,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_geom_elt_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_geom_elt_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list);

    /*!
     * \brief Write a mesh in Medit format.
     *
     * \copydoc doxygen_hide_geometry_format_write_common_arg
     * \param[in] version Version of Medit to use (usually choose '2'; '1' is for float precision).
     */
    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file, int version = 2);


} // namespace MoReFEM::Internal::MeshNS::FormatNS::Medit


/// @} // addtogroup GeometryGroup


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_INTERNAL_x_FORMAT_x_MEDIT_HPP_
