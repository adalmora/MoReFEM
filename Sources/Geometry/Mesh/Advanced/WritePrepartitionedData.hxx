//! \file
//
//
//  WritePrepartitionedData.hxx
//  MoReFEM
//
//  Created by sebastien on 15/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_
#define MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_

// IWYU pragma: private, include "Geometry/Mesh/Advanced/WritePrepartitionedData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::MeshNS
{


    inline const FilesystemNS::File& WritePrepartitionedData::GetReducedMeshFile() const noexcept
    {
        return reduced_mesh_file_;
    }


    inline const FilesystemNS::File& WritePrepartitionedData::GetPartitionDataFile() const noexcept
    {
        return partition_data_file_;
    }


} // namespace MoReFEM::Advanced::MeshNS


#endif // MOREFEM_x_GEOMETRY_x_MESH_x_ADVANCED_x_WRITE_PREPARTITIONED_DATA_HXX_
