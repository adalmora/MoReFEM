//! \file
//
//
//  ExtractLocalOperatorHelper.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 19/11/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HXX_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HXX_

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hpp"


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    template<class FEltStorageT>
    const LocalFEltSpace& IdentifyLocalFeltSpace(const FEltStorageT& felt_storage, const GeometricElt& geom_elt)
    {
        decltype(auto) ref_geom_elt = geom_elt.GetRefGeomElt();

        const auto geometric_elt_index = geom_elt.GetIndex();

        for (const auto& [ref_local_felt_space_ptr, local_felt_space_list] : felt_storage)
        {
            assert(!(!ref_local_felt_space_ptr));
            const auto& ref_local_felt_space = *ref_local_felt_space_ptr;

            if (ref_geom_elt != ref_local_felt_space.GetRefGeomElt())
                continue;

            assert(!local_felt_space_list.empty());

            auto it = local_felt_space_list.find(geometric_elt_index);
            assert(it != local_felt_space_list.cend());
            const auto& local_felt_space_ptr = it->second;
            assert(!(!local_felt_space_ptr));

            return *local_felt_space_ptr;
        }

        assert(false
               && "Invalid call: the test in which the function has been called is ill-constructed (the mesh "
                  "might not be correct for instance).");
        exit(EXIT_FAILURE);
    }


    template<Advanced::GeometricEltEnum EnumT>
    const GeometricElt& FindFirstGeometricElt(const Mesh& mesh)
    {
        decltype(auto) geom_elt_list = mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

        auto it = std::find_if(geom_elt_list.cbegin(),
                               geom_elt_list.cend(),
                               [](const auto& geom_elt_ptr)
                               {
                                   assert(!(!geom_elt_ptr));

                                   return geom_elt_ptr->GetIdentifier() == EnumT;
                               });

        assert(it != geom_elt_list.cend());
        const auto& geom_elt_ptr = *it;
        assert(!(!geom_elt_ptr));

        return *geom_elt_ptr;
    }


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HXX_
