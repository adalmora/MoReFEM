//! \file
//
//
//  ExtractLocalOperatorHelper.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 19/11/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HPP_

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"


namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{


    /*!
     * \brief Identify in \a FEltSpace storage the \a LocalFEltSpace related to a given \a GeometricElt.
     *
     *  This function is an helper for \a GlobalVariationalOperator::ExtractReadiedLocalOperator().
     *
     * \param[in] felt_storage Internal storage for finite elements in \a FEltSpace object.
     * \param[in] geom_elt Specific \a GeometricElt which matching \a LocalFEltSpace is sought. Typically it is just the very first one found
     * in the mesh (see \a FindFirstGeometricElt()).
     *
     * \return \a LocalFEltSpace matching the \a GeometricElt
     */
    template<class FEltStorageT>
    const LocalFEltSpace& IdentifyLocalFeltSpace(const FEltStorageT& felt_storage, const GeometricElt& geom_elt);


    /*!
     * \brief Identify in a \a Mesh the first \a GeometricElt which type is \a EnumT.
     *
     *  This function is an helper for \a GlobalVariationalOperator::ExtractReadiedLocalOperator().
     *
     * \tparam EnumT Type of the \a GeometricElt sought.
     * \param[in] mesh The \a Mesh in which we seek the \a GeometricElt.
     *
     * \return First \a GeometricElt with type \a EnumT.
     */
    template<Advanced::GeometricEltEnum EnumT>
    const GeometricElt& FindFirstGeometricElt(const Mesh& mesh);


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


#include "Operators/GlobalVariationalOperator/Internal/ExtractLocalOperatorHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_EXTRACT_LOCAL_OPERATOR_HELPER_HPP_
