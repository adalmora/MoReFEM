/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:02:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HXX_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HXX_

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorForRefGeomElt.hpp"


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    constexpr Advanced::GeometricEltEnum
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::RefGeomEltType()
    {
        return RefGeomEltTypeT;
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    void LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::SetLocalOperator(
        LocalVariationalOperatorPtrT&& ptr) noexcept
    {
        assert(local_operator_ == nullptr && "Should be initialized only once!");

        local_operator_ = std::move(ptr);
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    bool LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::IsRelevant() const noexcept
    {
        return (local_operator_ != nullptr);
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    typename LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::local_operator_type&
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::GetNonCstLocalOperator() const noexcept
    {
        assert(!(!local_operator_));
        return *local_operator_;
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    constexpr Advanced::OperatorNS::Nature
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::OperatorNature()
    {
        return local_operator_type::OperatorNature();
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT>
    constexpr Advanced::GeometricEltEnum LocalOperatorForRefGeomElt<RefGeomEltTypeT, std::nullptr_t>::RefGeomEltType()
    {
        return RefGeomEltTypeT;
    }


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HXX_
