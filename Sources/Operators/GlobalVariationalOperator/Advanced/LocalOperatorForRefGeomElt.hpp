/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 13 Mar 2017 22:02:25 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HPP_
#define MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HPP_

#include <cassert>
#include <memory>
#include <vector>

#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"

#include "Operators/Enum.hpp"


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{

    /*!
     * \brief Class which includes the \a LocalVariationalOperator to use for the given \a RefGeomEltT.
     *
     * It is intended to be used when a \a GlobalVariationalOperator uses up a different \a
     * LocalVariationalOperator implementation for different geometries; for most operators implementation is
     * the same whatever the geometry is and in this case you do not have to handle this kind of object
     * directly.
     *
     * \see GlobalVariationalOperatorNS::DependsOnRefGeomElt.
     *
     * \tparam RefGeomEltTypeT Enum which signs which \a RefGeomElt is considered.
     * \tparam LocalVariationalOperatorPtrT Type of the unique_ptr which encloses a \a LocalVariationalOperator.
     * e.g. std::unique_ptr<LocalOperatorNS::Mass>.
     */
    template<::MoReFEM::Advanced::GeometricEltEnum RefGeomEltT, class LocalVariationalOperatorPtrT>
    class LocalOperatorForRefGeomElt
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = LocalOperatorForRefGeomElt;

        //! Returns the \a RefGeomElt considered.
        static constexpr ::MoReFEM::Advanced::GeometricEltEnum RefGeomEltType();

        //! Alias to the type of the \a LocalVariationalOperator.
        using local_operator_type = typename LocalVariationalOperatorPtrT::element_type;

        //! Alias to \a LocalVariationalOperatorPtrT
        using local_operator_pointer_type = LocalVariationalOperatorPtrT;

        /*!
         * \brief Whether the operator is bilinear, linear or non linear (in the latter case, it might be
         * assembled into a matrix and a vector).
         *
         * \return Bilinear, linear or non linear.
         */
        static constexpr ::MoReFEM::Advanced::OperatorNS::Nature OperatorNature();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit LocalOperatorForRefGeomElt() = default;

        //! Destructor.
        ~LocalOperatorForRefGeomElt() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalOperatorForRefGeomElt(const LocalOperatorForRefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LocalOperatorForRefGeomElt(LocalOperatorForRefGeomElt&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LocalOperatorForRefGeomElt& operator=(const LocalOperatorForRefGeomElt& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LocalOperatorForRefGeomElt& operator=(LocalOperatorForRefGeomElt&& rhs) = delete;

        ///@}

        /*!
         * \brief Mutator for the underlying std::unique_ptr.
         *
         * Used only when the list of \a LocalVariationalOperator is actually generated.
         *
         * \param[in] ptr Value set.
         */
        void SetLocalOperator(LocalVariationalOperatorPtrT&& ptr) noexcept;

        /*!
         * \brief Whether for the \a GlobalVariationalOperator the current \a RefGeomElt is relevant.
         *
         * If not (e.g. for instance if in the implementation no \a GeometricElt of the chosen type are
         * present in the relevant finite element), the \a LocalVariationalOperator unique_ptr remains set
         * to a nullptr value.
         *
         * \return True if there is an actual \a LocalVariationalOperator related to the current \a RefGeomElt.
         */
        bool IsRelevant() const noexcept;

        /*!
         * \brief Non constant accessor to the \a LocalVariationalOperator.
         *
         * \attention We assume when this method is called that IsRelevant() returns true!
         *
         * \return Reference to the \a LocalVariationalOperator.
         */
        local_operator_type& GetNonCstLocalOperator() const noexcept;


      private:
        /*!
         * \brief std::unique_ptr to the \a LocalVariationalOperator, if one needs to be defined for the
         * \a GlobalVariationalOperator.
         */
        LocalVariationalOperatorPtrT local_operator_ = nullptr;
    };


    /*!
     * \brief Specialization for the namesake class when the given type for \a LocalVariationalOperator
     * pointer is std::nullptr_t.
     *
     * This is intended if for a given operator a \a RefGeomElt is utterly irrelevant (e.g. Point1 or Segment2
     * for a GradOnGradientBasedElasticTensor).
     */
    template<::MoReFEM::Advanced::GeometricEltEnum RefGeomEltTypeT>
    class LocalOperatorForRefGeomElt<RefGeomEltTypeT, std::nullptr_t>
    {

      public:
        //! Returns the \a RefGeomElt considered.
        static constexpr Advanced::GeometricEltEnum RefGeomEltType();

        //! Alias to \a LocalVariationalOperatorPtrT
        using local_operator_pointer_type = std::nullptr_t;

        //! Alias.
        using local_operator_type = std::false_type;
    };


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorsGroup


#include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorForRefGeomElt.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_GLOBAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_LOCAL_OPERATOR_FOR_REF_GEOM_ELT_HPP_
