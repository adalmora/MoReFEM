/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ALIAS_HPP_
#define MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ALIAS_HPP_

#include <utility>
#include <vector>

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM::ConformInterpolatorNS
{


    //! Convenient alias to pairing.
    using pairing_type = std::vector<std::pair<Unknown::const_shared_ptr, Unknown::const_shared_ptr>>;


} // namespace MoReFEM::ConformInterpolatorNS


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_CONFORM_INTERPOLATOR_x_ALIAS_HPP_
