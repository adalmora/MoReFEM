/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


//  InvariantHolders.h
//
//  Created by Sebastien Gilles on 3/20/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export
#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"


namespace MoReFEM
{


    /*!
     * \brief Class which computes the invariant and its derivatives with respect to Cauchy-Green tensor.
     *
     * This class is entirely optional: you may use directly free functions defined in InvariantComputation file.
     *
     * \tparam TraitsT The \a HyperelasticNS::Traits template class stored in the hyperelastic law you're considering.
     *
     */
    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian
    >
    // clang-format on
    class InvariantHolder final
    // clang-format off
    : public std::conditional_t
            <
                CoordsPolicyT == CoordsNS::CoordsPolicy::generalized,
                Crtp::LocalMatrixStorage<InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>, 3ul>,
                Crtp::LocalMatrixStorage<InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>, 0ul>
            >
    // clang-format on
    {

        static_assert(TraitsT::Ninvariants() >= 3, "The number of invariants must be greater or equal to 3.");

      public:
        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>>;

        //! Alias to the parent that provides LocalMatrixStorage.
        // clang-format off
        using matrix_parent =
            std::conditional_t
            <
                CoordsPolicyT == CoordsNS::CoordsPolicy::generalized,
                Crtp::LocalMatrixStorage<InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>, 3ul>,
                Crtp::LocalMatrixStorage<InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>, 0ul>
            >;
        // clang-format on

        //! Enum class to fetch local matrices when the CoordsPolicyT is generalized.
        enum class LocalMatrixIndex : std::size_t { I2_matrix = 0, intermediate_product, scalar_matrix };

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the \a Mesh considered.
        //! \param[in] content Tells whether first and second derivates need to be stored.
        explicit InvariantHolder(std::size_t mesh_dimension, InvariantNS::Content content);

        //! Destructor.
        ~InvariantHolder() = default;

        //! \copydoc doxygen_hide_copy_constructor
        InvariantHolder(const InvariantHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvariantHolder(InvariantHolder&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvariantHolder& operator=(const InvariantHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvariantHolder& operator=(InvariantHolder&& rhs) = delete;

        ///@}


        /*!
         * \class doxygen_hide_cauchy_green_tensor_as_vector_arg
         *
         * \param[in] cauchy_green_tensor The vector which holds the values of CauchyGreen tensor; expected content is:
         * - (xx, yy, xy) for dimension 2
         * - (xx, yy, zz, xy, yz, xz) for dimension 3
         * It should have been computed by \a UpdateCauchyGreenTensor operator.
         */

        /*!
         * \brief Update the invariants and their derivates with a new Cauchy-Green tensor in cartesian coordinates.
         *
         * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
         *
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         *
         * \param[in] quad_pt \a QuadraturePoint at which the value is computed.
         */
        void UpdateCartesian(const LocalVector& cauchy_green_tensor,
                             const QuadraturePoint& quad_pt,
                             const GeometricElt& geom_elt);

        /*!
         * \brief Update the invariants and their derivates with a new Cauchy-Green tensor in generalized coordinates.
         *
         * \copydoc doxygen_hide_cauchy_green_as_vector_arg
         *
         * \copydoc doxygen_hide_contravariant_metric_as_vector_arg
         *
         * \copydoc doxygen_hide_transposed_contravariant_metric_as_vector_arg
         *
         * \param[in] contravariant_metric_tensor_determinant Determinant of the metric tensor.
         *
         * \param[in] contravariant_basis The contravariant basis used to compute the contravariant fiber vector.
         *
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         *
         * \param[in] quad_pt \a QuadraturePoint at which the value is computed.
         */
        void UpdateGeneralized(const LocalMatrix& cauchy_green_as_vector,
                               const LocalMatrix& contravariant_metric_as_vector,
                               const LocalMatrix& transposed_contravariant_metric_as_vector,
                               const double contravariant_metric_tensor_determinant,
                               const LocalMatrix& contravariant_basis,
                               const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt);


        /*!
         * \brief Get one of the invariants.
         *
         * \param[in] invariants_index The index of the invariant to get (e.g. invariants_index::I1).
         *
         * \return The value of the invariant.
         */
        double GetInvariant(InvariantNS::index invariants_index) const noexcept;

        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \param[in] invariants_first_derivative_index The index of the invariant derivative to get
         * (e.g. invariants_first_derivative_index::dI1dC).
         *
         * \return The value of the invariant derivative.
         */
        const LocalVector& GetFirstDerivativeWrtCauchyGreen(
            InvariantNS::first_derivative_index invariants_first_derivative_index) const noexcept;

        /*!
         * \brief Get one of the first derivative of the invariants against Cauchy-Green tensor.
         *
         * \param[in] invariants_second_derivative_index The index of the invariant second derivative to get
         * (e.g. InvariantNS::second_derivative_index::d2I2dCdC).
         *
         * \return The value of the invariant second derivative.
         */
        const LocalMatrix& GetSecondDerivativeWrtCauchyGreen(
            InvariantNS::second_derivative_index invariants_second_derivative_index) const noexcept;

#ifndef NDEBUG

        /*!
         * \brief In debug mode, indicate invariants should be reset before use.
         */
        void Reset();

#endif // NDEBUG

        /*!
         * \brief Change the pointer to the fibers of the law to update I4. If i4 not activated set to nullptr.
         *
         * \param[in] fibersI4 The fibers used to define I4.
         */
        void SetFibersI4(const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI4);
        /*!
         * \brief Change the pointer to the fibers of the law to update I4. If i4 not activated set to nullptr.
         *
         * \param[in] fibersI6 The fibers used to define I4.
         */
        void SetFibersI6(const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI6);


      private:
        //! Dimension of the mesh.
        std::size_t GetMeshDimension() const noexcept;

        //! Get what is actually stored and computed within the class (see \a InvariantNS::Content for more
        //! details).
        InvariantNS::Content GetContent() const noexcept;

        /*!
         * \brief Get all of the second derivative of the invariants against Cauchy-Green tensor.
         *
         * \return An array containing all the second derivative of the invariants.
         */
        std::array<LocalMatrix, 2>& GetNonCstSecondDerivativeWrtCauchyGreen() noexcept;


        /*!
         * \brief Get all of the second derivative of the invariants against Cauchy-Green tensor.
         *
         * \return An array containing all the first derivative of the invariants.
         */
        std::array<LocalVector, TraitsT::Ninvariants()>& GetNonCstFirstDerivativeWrtCauchyGreen() noexcept;


        /*!
         * \brief Get all the invariants.
         *
         * \return An array containing all the invariants.
         */
        std::array<double, TraitsT::Ninvariants()>& GetNonCstInvariant() noexcept;

        //! Constant accessors to the fibers in case of I4 is activate.
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* GetFibersI4() const noexcept;

        //! Constant accessors to the fibers in case of I4 is activate.
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* GetFibersI6() const noexcept;

      private:
        //! What is actually stored and computed within the class (see \a InvariantNS::Content for more details).
        const InvariantNS::Content content_;

        //! Dimension of the mesh.
        const std::size_t mesh_dimension_;

        //! Array to store all the invariants.
        std::array<double, TraitsT::Ninvariants()> invariants_;

        //! Array to store all the invariants first derivative.
        std::array<LocalVector, TraitsT::Ninvariants()> invariants_first_derivative_;

        /*!
         * \brief Array to store all the invariants second derivative.
         *
         * Only 3 here because d2I1dCdC = d2I4dCdC = d2I6dCdC = 0. See InvariantNS::second_derivative_index.
         */
        std::array<LocalMatrix, 2> invariants_second_derivative_;

        //! Fibers parameter for I4.
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibers_I4_ = nullptr;

        //! Fibers parameter for I6.
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibers_I6_ = nullptr;

#ifndef NDEBUG

        /*!
         * \brief In debug mode, check invariants are correctly set before using them up!
         */
        bool are_invariant_set_ = false;

#endif // NDEBUG
    };


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HPP_
