/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Internal/UpdateHelper.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT
    >::InvariantHolder(std::size_t mesh_dimension,
                       InvariantNS::Content content)
    // clang-format on
    : content_(content), mesh_dimension_(mesh_dimension)
    {
        std::size_t size = 0;

        assert((mesh_dimension >= 1 && mesh_dimension <= 3)
               && "Invariants can only be created in dimension 1, 2 or 3.");

        switch (mesh_dimension)
        {
        case 1:
            size = 1ul;
            break;
        case 2:
            size = 3ul;
            break;
        case 3:
            size = 6ul;
            break;
        }

        switch (content)
        {
        case InvariantNS::Content::invariants_and_first_and_second_deriv:
            static_assert(std::tuple_size<decltype(invariants_second_derivative_)>::value == 2ul);
            for (std::size_t i = 0ul; i < 2u; ++i)
            {
                invariants_second_derivative_[i].resize({ size, size });
            }

            if constexpr (CoordsPolicyT == CoordsNS::CoordsPolicy::generalized)
            {
                this->matrix_parent::InitLocalMatrixStorage({ {
                    { size, size }, // I2_matrix
                    { size, size }, // intermediate_product
                    { 1, 1 }        // scalar_matrix
                } });
            }

            [[fallthrough]];
        case InvariantNS::Content::invariants_and_first_deriv:
            static_assert(std::tuple_size<decltype(invariants_first_derivative_)>::value == TraitsT::Ninvariants());
            for (std::size_t i = 0ul; i < TraitsT::Ninvariants(); ++i)
            {
                invariants_first_derivative_[i].resize({ size });
            }
            [[fallthrough]];
        case InvariantNS::Content::invariants:
            break;
        }
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    void InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::UpdateCartesian(const LocalVector& cauchy_green_tensor,
                                                                                const QuadraturePoint& quad_pt,
                                                                                const GeometricElt& geom_elt)
    {
        const auto mesh_dimension = GetMeshDimension();
        const auto content = GetContent();
        auto& invariants = GetNonCstInvariant();
        auto& invariants_first_derivative = GetNonCstFirstDerivativeWrtCauchyGreen();
        auto& invariants_second_derivative = GetNonCstSecondDerivativeWrtCauchyGreen();

        switch (mesh_dimension)
        {
        case 1u:
            Internal::InvariantNS::UpdateHelper<TraitsT, 1u, FiberPolicyT, CoordsPolicyT>(cauchy_green_tensor,
                                                                                          quad_pt,
                                                                                          geom_elt,
                                                                                          content,
                                                                                          invariants,
                                                                                          invariants_first_derivative,
                                                                                          invariants_second_derivative,
                                                                                          GetFibersI4(),
                                                                                          GetFibersI6());
            break;
        case 2u:
            Internal::InvariantNS::UpdateHelper<TraitsT, 2u, FiberPolicyT, CoordsPolicyT>(cauchy_green_tensor,
                                                                                          quad_pt,
                                                                                          geom_elt,
                                                                                          content,
                                                                                          invariants,
                                                                                          invariants_first_derivative,
                                                                                          invariants_second_derivative,
                                                                                          GetFibersI4(),
                                                                                          GetFibersI6());
            break;
        case 3u:
            Internal::InvariantNS::UpdateHelper<TraitsT, 3u, FiberPolicyT, CoordsPolicyT>(cauchy_green_tensor,
                                                                                          quad_pt,
                                                                                          geom_elt,
                                                                                          content,
                                                                                          invariants,
                                                                                          invariants_first_derivative,
                                                                                          invariants_second_derivative,
                                                                                          GetFibersI4(),
                                                                                          GetFibersI6());
            break;
        default:
        {
            assert(false && "Invariants computation is available only for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }
        }

#ifndef NDEBUG
        are_invariant_set_ = true;
#endif // NDEBUG
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    void InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::UpdateGeneralized(
        const LocalMatrix& cauchy_green_tensor_as_vector,
        const LocalMatrix& contravariant_metric_tensor_as_vector,
        const LocalMatrix& transposed_contravariant_metric_tensor_as_vector,
        const double contravariant_metric_tensor_determinant,
        const LocalMatrix& contravariant_basis,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt)
    {
        const auto content = GetContent();
        auto& invariants = GetNonCstInvariant();
        auto& invariants_first_derivative = GetNonCstFirstDerivativeWrtCauchyGreen();
        auto& invariants_second_derivative = GetNonCstSecondDerivativeWrtCauchyGreen();

        using namespace InvariantNS;

        auto& matrix_helper =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::I2_matrix)>();
        auto& intermediate_product =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::intermediate_product)>();
        auto& scalar_matrix =
            this->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::scalar_matrix)>();

        Internal::InvariantNS::UpdateI2Helper(contravariant_metric_tensor_as_vector, matrix_helper);

        switch (content)
        {
        case Content::invariants_and_first_and_second_deriv:
            SecondDerivativeInvariant2CauchyGreen<CoordsPolicyT>(
                matrix_helper,
                invariants_second_derivative[EnumUnderlyingType(InvariantNS::second_derivative_index::d2I2dCdC)]);

            SecondDerivativeInvariant3CauchyGreen<CoordsPolicyT>(
                cauchy_green_tensor_as_vector,
                contravariant_metric_tensor_determinant,
                invariants_second_derivative[EnumUnderlyingType(InvariantNS::second_derivative_index::d2I3dCdC)]);
            [[fallthrough]];
        case Content::invariants_and_first_deriv:
            FirstDerivativeInvariant1CauchyGreen<CoordsPolicyT>(
                contravariant_metric_tensor_as_vector,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI1dC)]);

            FirstDerivativeInvariant2CauchyGreen<CoordsPolicyT>(
                cauchy_green_tensor_as_vector,
                matrix_helper,
                intermediate_product,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI2dC)]);

            FirstDerivativeInvariant3CauchyGreen<CoordsPolicyT>(
                cauchy_green_tensor_as_vector,
                contravariant_metric_tensor_determinant,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI3dC)]);

            if constexpr (TraitsT::IsI4Activated())
            {
                decltype(auto) fiber_I4_ptr = GetFibersI4();
                assert(!(!fiber_I4_ptr));

                const auto& tau_interpolate_at_quad_pointI4 = fiber_I4_ptr->GetValue(quad_pt, geom_elt);

                FirstDerivativeInvariant4CauchyGreen<CoordsPolicyT>(
                    contravariant_basis,
                    tau_interpolate_at_quad_pointI4,
                    invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI4dC)]);
            }

            if constexpr (TraitsT::IsI6Activated())
            {
                decltype(auto) fiber_I6_ptr = GetFibersI6();
                assert(!(!fiber_I6_ptr));

                const auto& tau_interpolate_at_quad_pointI6 = fiber_I6_ptr->GetValue(quad_pt, geom_elt);

                FirstDerivativeInvariant4CauchyGreen<CoordsPolicyT>(
                    contravariant_basis,
                    tau_interpolate_at_quad_pointI6,
                    invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI6dC)]);
            }
            [[fallthrough]];
        case Content::invariants:
            invariants[EnumUnderlyingType(InvariantNS::index::I1)] = Invariant1<CoordsPolicyT>(
                transposed_contravariant_metric_tensor_as_vector, cauchy_green_tensor_as_vector, scalar_matrix);

            invariants[EnumUnderlyingType(InvariantNS::index::I2)] = Invariant2<CoordsPolicyT>(
                cauchy_green_tensor_as_vector, matrix_helper, intermediate_product, scalar_matrix);

            invariants[EnumUnderlyingType(InvariantNS::index::I3)] =
                Invariant3<CoordsPolicyT>(cauchy_green_tensor_as_vector, contravariant_metric_tensor_determinant);

            if constexpr (TraitsT::IsI4Activated())
            {
                decltype(auto) fiber_I4_ptr = GetFibersI4();
                assert(!(!fiber_I4_ptr));

                const auto& tau_interpolate_at_quad_pointI4 = fiber_I4_ptr->GetValue(quad_pt, geom_elt);

                invariants[EnumUnderlyingType(InvariantNS::index::I4)] = Invariant4<CoordsPolicyT>(
                    cauchy_green_tensor_as_vector, contravariant_basis, tau_interpolate_at_quad_pointI4);
            }

            if constexpr (TraitsT::IsI6Activated())
            {
                decltype(auto) fiber_I6_ptr = GetFibersI6();
                assert(!(!fiber_I6_ptr));

                const auto& tau_interpolate_at_quad_pointI6 = fiber_I6_ptr->GetValue(quad_pt, geom_elt);

                invariants[EnumUnderlyingType(InvariantNS::index::I6)] = Invariant4<CoordsPolicyT>(
                    cauchy_green_tensor_as_vector, contravariant_basis, tau_interpolate_at_quad_pointI6);
            }
            break;
        } // switch

#ifndef NDEBUG
        are_invariant_set_ = true;
#endif // NDEBUG
    }


#ifndef NDEBUG
    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    inline void InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::Reset()
    {
        are_invariant_set_ = false;
    }
#endif // NDEBUG


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    double InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetInvariant(
        InvariantNS::index invariants_index) const noexcept
    {
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(invariants_index) < TraitsT::Ninvariants());
        return invariants_[EnumUnderlyingType(invariants_index)];
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    const LocalVector& InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetFirstDerivativeWrtCauchyGreen(
        InvariantNS::first_derivative_index invariants_first_derivative_index) const noexcept
    {
        assert(GetContent() >= InvariantNS::Content::invariants_and_first_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
                  "constructor argument if you really need first derivate!");
        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(invariants_first_derivative_index) < TraitsT::Ninvariants());

        return invariants_first_derivative_[EnumUnderlyingType(invariants_first_derivative_index)];
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    const LocalMatrix& InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetSecondDerivativeWrtCauchyGreen(
        InvariantNS::second_derivative_index invariants_second_derivative_index) const noexcept
    {
        assert(GetContent() == InvariantNS::Content::invariants_and_first_and_second_deriv
               && "This InvariantHolder object wasn't build to provide first derivates; change the relevant "
                  "constructor argument if you really need first derivate!");


        assert(are_invariant_set_ && "Make sure they are correctly defined before use!");
        assert(EnumUnderlyingType(invariants_second_derivative_index) < 2);

        return invariants_second_derivative_[EnumUnderlyingType(invariants_second_derivative_index)];
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    inline std::size_t InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    inline InvariantNS::Content InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetContent() const noexcept
    {
        return content_;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    std::array<double, TraitsT::Ninvariants()>&
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetNonCstInvariant() noexcept
    {
        return invariants_;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    std::array<LocalVector, TraitsT::Ninvariants()>&
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetNonCstFirstDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_first_derivative_;
    }

    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    std::array<LocalMatrix, 2>&
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetNonCstSecondDerivativeWrtCauchyGreen() noexcept
    {
        return invariants_second_derivative_;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    void InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::SetFibersI4(
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI4)
    {
        fibers_I4_ = fibersI4;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    const FiberList<FiberPolicyT, ParameterNS::Type::vector>*
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetFibersI4() const noexcept
    {
        return fibers_I4_;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    void InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::SetFibersI6(
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI6)
    {
        fibers_I6_ = fibersI6;
    }


    // clang-format off
    template
    <
        class TraitsT,
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    const FiberList<FiberPolicyT, ParameterNS::Type::vector>*
    InvariantHolder<TraitsT, FiberPolicyT, CoordsPolicyT>::GetFibersI6() const noexcept
    {
        return fibers_I6_;
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_HOLDER_HXX_
