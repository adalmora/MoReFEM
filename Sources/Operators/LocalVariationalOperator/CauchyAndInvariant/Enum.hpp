/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_ENUM_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_ENUM_HPP_


namespace MoReFEM::InvariantNS
{


    /*!
     * \brief Content of InvariantHolder.
     *
     * - invariants mean that only the invariants themselves are stored and computed.
     * - invariants_and_first_deriv mean all first derivates are also stored.
     * - invariants_and_first_and_second_deriv adds second derivates to prior content.
     */
    enum class Content {
        invariants = 0,
        invariants_and_first_deriv,
        invariants_and_first_and_second_deriv,
    };


    /*!
     * \brief Enum class to define invariants numbering.
     *
     * \attention This way of handling the invariants for the hyperelastic laws is rather naive and can't handle all
     * possible hyperelastic laws. It works for the moment with the current laws considered, but this should
     * be refactored to be more generic (#1821).
     */
    enum class index : std::size_t { I1 = 0, I2, I3, I4, I6 };

    //! Enum class to define invariants first derivative numbering.
    enum class first_derivative_index : std::size_t { dI1dC = 0, dI2dC, dI3dC, dI4dC, dI6dC };

    //! Enum class to define invariants second derivative numbering. d2I1dCdC = d2I4dCdC = 0.
    enum class second_derivative_index : std::size_t { d2I2dCdC = 0, d2I3dCdC };


} // namespace MoReFEM::InvariantNS


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_ENUM_HPP_
