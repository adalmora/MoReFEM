/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 12 May 2016 09:25:42 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/MatrixOrVector.hpp"

#include "Geometry/Coords/EnumCoordsPolicy.hpp"


namespace MoReFEM
{


    /*!
     * \brief Returns first invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the first invariant.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    double Invariant1(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns second invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the second invariant.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    double Invariant2(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns third invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    double Invariant3(const LocalVector& cauchy_green_tensor) noexcept;

    /*!
     * \brief Returns fourth invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    double Invariant4(const LocalVector& cauchy_green_tensor,
                      const LocalVector& tau_interpolate_at_quad_point) noexcept;

    /*!
     * \brief Returns fourth invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     *
     * \return Value of the third invariant.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    double Invariant6(const LocalVector& cauchy_green_tensor,
                      const LocalVector& tau_interpolate_at_quad_point) noexcept;


    /*!
     * \class doxygen_hide_invariant_first_deriv_arg
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */


    /*!
     * \class doxygen_hide_invariant_second_deriv_arg
     *
     * \copydoc doxygen_hide_cauchy_green_tensor_as_vector_arg
     * \param[out] out Matrix which stores the second derivative of the invariant. Must be already properly allocated.
     */


    /*!
     * \brief Returns first derivative of first invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void FirstDerivativeInvariant1CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void FirstDerivativeInvariant2CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void FirstDerivativeInvariant3CauchyGreen(const LocalVector& cauchy_green_tensor, LocalVector& out);

    /*!
     * \brief Returns first derivative of fourth invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void FirstDerivativeInvariant4CauchyGreen(const LocalVector& cauchy_green_tensor,
                                              const LocalVector& tau_interpolate_at_quad_point,
                                              LocalVector& out);

    /*!
     * \brief Returns first derivative of fourth invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_first_deriv_arg
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void FirstDerivativeInvariant6CauchyGreen(const LocalVector& cauchy_green_tensor,
                                              const LocalVector& tau_interpolate_at_quad_point,
                                              LocalVector& out);


    /*!
     * \brief Returns second derivative of first invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void SecondDerivativeInvariant1CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);

    /*!
     * \brief Returns second derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void SecondDerivativeInvariant2CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);

    /*!
     * \brief Returns second derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_invariant_second_deriv_arg
     */
    template<std::size_t DimensionT, CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian>
    void SecondDerivativeInvariant3CauchyGreen(const LocalVector& cauchy_green_tensor, LocalMatrix& out);

    /*!
     * \brief Returns first invariant.
     *
     * \copydoc doxygen_hide_transposed_contravariant_metric_as_vector_arg
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \param[in,out] out The I1 invariant as a (1,1) matrix (due to matrix-matrix dot product).
     *
     * \return Value of the first invariant.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    double Invariant1(const LocalMatrix& transposed_contravariant_metric_as_vector,
                      const LocalMatrix& cauchy_green_as_vector,
                      LocalMatrix& out);

    /*!
     * \class doxygen_hide_helper_I2_matrix
     *
     * \param[in] helper_I2_matrix Helper matrix for I2 computation which is a (6, 6) matrix.
     * (refer to the note in Documentation/Operators/3D-shells.pdf)
     */

    /*!
     * \brief Returns second invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \copydoc doxygen_hide_helper_I2_matrix
     * \param[in,out] intermediate_product Intermediate matrix matrix contraction product.
     * \param[in,out] out The I2 invariant as a (1,1) matrix (due to matrix-matrix dot product).
     *
     * \return Value of the second invariant.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    double Invariant2(const LocalMatrix& cauchy_green_as_vector,
                      const LocalMatrix& helper_I2_matrix,
                      LocalMatrix& intermediate_product,
                      LocalMatrix& out);

    /*!
     * \brief Returns third invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \param[in] contravariant_metric_tensor_determinant Determinant of the metric tensor.
     *
     * \return Value of the third invariant.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    double Invariant3(const LocalMatrix& cauchy_green_as_vector,
                      const double contravariant_metric_tensor_determinant) noexcept;


    /*!
     * \brief Returns the fourth invariant.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \param[in] contravariant_basis The contravariant basis used to compute the contravariant fiber vector.
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     * \return Value of the fourth invariant.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    double Invariant4(const LocalMatrix& cauchy_green_as_vector,
                      const LocalMatrix& contravariant_basis,
                      const LocalVector& tau_interpolate_at_quad_point) noexcept;

    /*!
     * \brief Returns first derivative of first invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_contravariant_metric_as_vector_arg
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void FirstDerivativeInvariant1CauchyGreen(const LocalMatrix& contravariant_metric_as_vector, LocalVector& out);

    /*!
     * \brief Returns first derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \copydoc doxygen_hide_helper_I2_matrix
     * \param[in,out] intermediate_product Intermediate matrix matrix contraction product.
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void FirstDerivativeInvariant2CauchyGreen(const LocalMatrix& cauchy_green_as_vector,
                                              const LocalMatrix& helper_I2_matrix,
                                              LocalMatrix& intermediate_product,
                                              LocalVector& out);

    /*!
     * \brief Returns first derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \param[in] contravariant_metric_tensor_determinant Determinant of the metric tensor.
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void FirstDerivativeInvariant3CauchyGreen(const LocalMatrix& cauchy_green_as_vector,
                                              const double contravariant_metric_tensor_determinant,
                                              LocalVector& out);

    /*!∏
     * \brief Returns first derivative of fourth invariant with respect to Cauchy-Green tensor.
     *
     * \param[in] contravariant_basis The contravariant basis used to compute the contravariant fiber vector.
     * \param[in] tau_interpolate_at_quad_point The fiber vector interpolated at the quadrature point.
     * \param[out] out Vector which stores the derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void FirstDerivativeInvariant4CauchyGreen(const LocalMatrix& contravariant_basis,
                                              const LocalVector& tau_interpolate_at_quad_point,
                                              LocalVector& out);

    /*!
     * \brief Returns second derivative of second invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_helper_I2_matrix
     * \param[out] out Matrix which stores the second derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void SecondDerivativeInvariant2CauchyGreen(const LocalMatrix& helper_I2_matrix, LocalMatrix& out);

    /*!
     * \brief Returns second derivative of third invariant with respect to Cauchy-Green tensor.
     *
     * \copydoc doxygen_hide_cauchy_green_as_vector_arg
     * \param[in] contravariant_metric_tensor_determinant Determinant of the metric tensor.
     * \param[out] out Matrix which stores the second derivative of the invariant. Must be already properly allocated.
     */
    template<CoordsNS::CoordsPolicy CoordsPolicyT>
    void SecondDerivativeInvariant3CauchyGreen(const LocalMatrix& cauchy_green_as_vector,
                                               const double contravariant_metric_tensor_determinant,
                                               LocalMatrix& out);


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INVARIANT_COMPUTATION_HPP_
