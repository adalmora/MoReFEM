/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <array>
#include <cassert>
#include <cstddef>
#include <memory>
// IWYU pragma: no_include <type_traits>

#include "Utilities/MatrixOrVector.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/Coords/EnumCoordsPolicy.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp" // IWYU pragma: keep


namespace MoReFEM
{


    namespace // anonymous
    {

        double DotProductHelper(std::size_t component,
                                const LocalMatrix& contravariant_basis,
                                const LocalVector& tau_interpolate_at_quad_point)
        {
            double ret = 0.;
            for (auto i = 0ul, size = tau_interpolate_at_quad_point.size(); i < size; ++i)
                ret += tau_interpolate_at_quad_point(i) * contravariant_basis(i, component);

            return ret;
        };


    } // namespace


    template<>
    double Invariant1<CoordsNS::CoordsPolicy::generalized>(const LocalMatrix& transposed_contravariant_metric_tensor,
                                                           const LocalMatrix& cauchy_green_tensor,
                                                           LocalMatrix& out)
    {
        xt::noalias(out) = xt::linalg::dot(transposed_contravariant_metric_tensor, cauchy_green_tensor);

        assert(out.shape(0) == 1);
        assert(out.shape(1) == 1);

        return out(0, 0);
    }

    template<>
    void FirstDerivativeInvariant1CauchyGreen<CoordsNS::CoordsPolicy::generalized>(
        const LocalMatrix& contravariant_metric_tensor,
        LocalVector& out)
    {
        assert(contravariant_metric_tensor.shape(0) == out.size());

        for (auto i = 0ul, size = out.size(); i < size; ++i)
        {
            out(i) = contravariant_metric_tensor(i, 0);
            if (i > 2)
                out(i) *= 0.5;
        }
    }


    template<>
    double Invariant2<CoordsNS::CoordsPolicy::generalized>(const LocalMatrix& cauchy_green_tensor_as_vector,
                                                           const LocalMatrix& helper_I2_matrix,
                                                           LocalMatrix& intermediate_product,
                                                           LocalMatrix& out)
    {
        xt::noalias(intermediate_product) = xt::linalg::dot(helper_I2_matrix, cauchy_green_tensor_as_vector);
        xt::noalias(out) = 0.5 * xt::linalg::dot(xt::transpose(cauchy_green_tensor_as_vector), intermediate_product);

        assert(out.shape(0) == 1);
        assert(out.shape(1) == 1);

        return out(0, 0);
    }


    template<>
    void FirstDerivativeInvariant2CauchyGreen<CoordsNS::CoordsPolicy::generalized>(
        const LocalMatrix& cauchy_green_tensor_as_vector,
        const LocalMatrix& I2_matrix_helper,
        LocalMatrix& intermediate_product,
        LocalVector& out)
    {
        xt::noalias(intermediate_product) = xt::linalg::dot(I2_matrix_helper, cauchy_green_tensor_as_vector);

        assert(intermediate_product.shape(0) == 6);
        assert(intermediate_product.shape(1) == 1);

        const auto size = out.size();
        const std::size_t euclidean_dimension = 3ul;
        for (auto i = 0ul; i < size - euclidean_dimension; ++i)
        {
            out(i) = intermediate_product(i, 0);
        }

        for (auto i = euclidean_dimension; i < size; ++i)
        {
            out(i) = intermediate_product(i, 0);
            out(i) *= 0.5;
        }
    }


    template<>
    void SecondDerivativeInvariant2CauchyGreen<CoordsNS::CoordsPolicy::generalized>(const LocalMatrix& I2_matrix_helper,
                                                                                    LocalMatrix& out)
    {
        out = I2_matrix_helper;
        const auto size_row = out.shape(0);
        const auto size_col = out.shape(1);

        for (auto i = 3ul; i < size_row; ++i)
            for (auto j = 0ul; j < size_col; ++j)
                out(i, j) *= 0.5;

        for (auto i = 0ul; i < size_row; ++i)
            for (auto j = 3ul; j < size_col; ++j)
                out(i, j) *= 0.5;
    }


    template<>
    double
    Invariant3<CoordsNS::CoordsPolicy::generalized>(const LocalMatrix& cauchy_green_tensor_as_vector,
                                                    const double contravariant_metric_tensor_determinant) noexcept
    {
        using NumericNS::Square;
        assert(cauchy_green_tensor_as_vector.shape(0) == 6 && cauchy_green_tensor_as_vector.shape(1) == 1);

        const double Cxx = cauchy_green_tensor_as_vector(0, 0);
        const double Cyy = cauchy_green_tensor_as_vector(1, 0);
        const double Czz = cauchy_green_tensor_as_vector(2, 0);
        const double Cxy = cauchy_green_tensor_as_vector(3, 0);
        const double Cyz = cauchy_green_tensor_as_vector(4, 0);
        const double Cxz = cauchy_green_tensor_as_vector(5, 0);

        const double cauchy_green_determinant =
            Cxx * Cyy * Czz - Cxx * Square(Cyz) - Cyy * Square(Cxz) - Czz * Square(Cxy) + 2. * Cxy * Cyz * Cxz;

        return cauchy_green_determinant * contravariant_metric_tensor_determinant;
    }


    template<>
    void FirstDerivativeInvariant3CauchyGreen<CoordsNS::CoordsPolicy::generalized>(
        const LocalMatrix& cauchy_green_tensor_as_vector,
        const double contravariant_metric_tensor_determinant,
        LocalVector& out)
    {
        using NumericNS::Square;

        assert(out.size() == 6);
        assert(cauchy_green_tensor_as_vector.shape(0) == 6 && cauchy_green_tensor_as_vector.shape(1) == 1);

        const double Cxx = cauchy_green_tensor_as_vector(0, 0);
        const double Cyy = cauchy_green_tensor_as_vector(1, 0);
        const double Czz = cauchy_green_tensor_as_vector(2, 0);
        const double Cxy = cauchy_green_tensor_as_vector(3, 0);
        const double Cyz = cauchy_green_tensor_as_vector(4, 0);
        const double Cxz = cauchy_green_tensor_as_vector(5, 0);

        out(0) = Cyy * Czz - Square(Cyz);
        out(1) = Cxx * Czz - Square(Cxz);
        out(2) = Cxx * Cyy - Square(Cxy);
        out(3) = Cyz * Cxz - Czz * Cxy;
        out(4) = Cxy * Cxz - Cxx * Cyz;
        out(5) = Cxy * Cyz - Cyy * Cxz;

        out *= contravariant_metric_tensor_determinant;
    }


    template<>
    void SecondDerivativeInvariant3CauchyGreen<CoordsNS::CoordsPolicy::generalized>(
        const LocalMatrix& cauchy_green_tensor_as_vector,
        const double contravariant_metric_tensor_determinant,
        LocalMatrix& out)
    {
        // [0    , Czz  , Cyy  , 0        , -Cyz     , 0        ],
        // [Czz  , 0    , Cxx  , 0        , 0        , -Cxz     ],
        // [Cyy  , Cxx  , 0    , -Cxy     , 0        , 0        ],
        // [0    , 0    , -Cxy , -0.5*Czz , 0.5*Cxz  , 0.5*Cyz  ],
        // [-Cyz , 0    , 0    , 0.5*Cxz  , -0.5*Cxx , 0.5*Cxy  ],
        // [0    , -Cxz , 0    , 0.5*Cyz  , 0.5*Cxy  , -0.5*Cyy ]

        assert(out.shape(0) == 6);
        assert(out.shape(1) == 6);
        assert(cauchy_green_tensor_as_vector.shape(0) == 6 && cauchy_green_tensor_as_vector.shape(1) == 1);

        const double Cxx = cauchy_green_tensor_as_vector(0, 0);
        const double Cyy = cauchy_green_tensor_as_vector(1, 0);
        const double Czz = cauchy_green_tensor_as_vector(2, 0);
        const double Cxy = cauchy_green_tensor_as_vector(3, 0);
        const double Cyz = cauchy_green_tensor_as_vector(4, 0);
        const double Cxz = cauchy_green_tensor_as_vector(5, 0);

        out.fill(0.);

        out(3, 3) = -0.5 * Czz;
        out(4, 4) = -0.5 * Cxx;
        out(5, 5) = -0.5 * Cyy;

        out(1, 0) = out(0, 1) = Czz;
        out(2, 0) = out(0, 2) = Cyy;
        out(1, 2) = out(2, 1) = Cxx;

        out(0, 4) = out(4, 0) = -Cyz;
        out(1, 5) = out(5, 1) = -Cxz;
        out(2, 3) = out(3, 2) = -Cxy;

        out(3, 4) = out(4, 3) = 0.5 * Cxz;
        out(3, 5) = out(5, 3) = 0.5 * Cyz;
        out(5, 4) = out(4, 5) = 0.5 * Cxy;

        out *= contravariant_metric_tensor_determinant;
    }


    template<>
    double Invariant4<CoordsNS::CoordsPolicy::generalized>(const LocalMatrix& cauchy_green_tensor_as_vector,
                                                           const LocalMatrix& contravariant_basis,
                                                           const LocalVector& tau_interpolate_at_quad_point) noexcept
    {
        assert(cauchy_green_tensor_as_vector.shape(0) == 6 && cauchy_green_tensor_as_vector.shape(1) == 1);
        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.shape(0)
               && tau_interpolate_at_quad_point.size() == contravariant_basis.shape(1));

        const double Cxx = cauchy_green_tensor_as_vector(0, 0);
        const double Cyy = cauchy_green_tensor_as_vector(1, 0);
        const double Czz = cauchy_green_tensor_as_vector(2, 0);
        const double Cxy = cauchy_green_tensor_as_vector(3, 0);
        const double Cyz = cauchy_green_tensor_as_vector(4, 0);
        const double Cxz = cauchy_green_tensor_as_vector(5, 0);

        std::size_t first_component = 0;
        std::size_t second_component = 1;
        std::size_t third_component = 2;

        const double nx = DotProductHelper(first_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double ny = DotProductHelper(second_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double nz = DotProductHelper(third_component, contravariant_basis, tau_interpolate_at_quad_point);

        const double norm = nx * nx + ny * ny + nz * nz;
        double I4 = 0.;

        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * (nx * Cxx + ny * Cxy + nz * Cxz) + ny * (nx * Cxy + ny * Cyy + nz * Cyz)
                 + nz * (nx * Cxz + ny * Cyz + nz * Czz);
        }

        return I4;
    }


    template<>
    void FirstDerivativeInvariant4CauchyGreen<CoordsNS::CoordsPolicy::generalized>(
        const LocalMatrix& contravariant_basis,
        const LocalVector& tau_interpolate_at_quad_point,
        LocalVector& out)
    {
        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.shape(0)
               && tau_interpolate_at_quad_point.size() == contravariant_basis.shape(1));

        std::size_t first_component = 0;
        std::size_t second_component = 1;
        std::size_t third_component = 2;

        const double nx = DotProductHelper(first_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double ny = DotProductHelper(second_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double nz = DotProductHelper(third_component, contravariant_basis, tau_interpolate_at_quad_point);

        out.fill(0.);

        const double norm = nx * nx + ny * ny + nz * nz;

        if (!(NumericNS::IsZero(norm)))
        {
            out(0) = (nx * nx);
            out(1) = (ny * ny);
            out(2) = (nz * nz);
            out(3) = (nx * ny);
            out(4) = (ny * nz);
            out(5) = (nx * nz);
        }
    }


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup
