/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 11 May 2016 17:24:37 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HPP_

#include <array>
#include <cstddef>

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Parameter/FiberEnum.hpp" // IWYU pragma: keep
#include "Core/Parameter/TypeEnum.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT> class FiberList; }
namespace MoReFEM::CoordsNS { enum class CoordsPolicy; }
namespace MoReFEM::InvariantNS { enum class Content; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InvariantNS
{


    /*!
     * \brief Helper function to update invariants and their derivates.
     *
     * \tparam TraitsT The \a HyperelasticNS::Traits template class stored in the hyperelastic law you're considering.
     *
     * \param[in] cauchy_green_tensor Cauchy-Green tensor.
     * \param[in] quad_pt \a QuadraturePoint considered.
     * \param[in] geom_elt \a GeometricElt considered.
     * \param[in] content Tells whether first and/or second derivates are stored in the \a InvariantHolder for which
     * this internal function is called.
     * \param[out] invariants Computed invariants.
     * \param[out] invariants_first_derivative Computed first derivatives of invariants, if \a content is
     * either \a invariants_and_first_deriv or \a invariants_and_first_and_second_deriv.
     * \param[out] invariants_second_derivative Computed second derivatives of invariants, if \a content is
     * \a invariants_and_first_and_second_deriv.
     * \param[out] fibersI4 If I4 is activated (information is in \a Traits), give the value in output.
     * \param[out] fibersI6 If I6 is activated (information is in \a Traits), give the value in output.
     */
    // clang-format off
    template
    <
        class TraitsT,
        std::size_t DimensionT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ::MoReFEM::CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    [[maybe_unused]] void
    UpdateHelper(const ::MoReFEM::LocalVector& cauchy_green_tensor,
                 const ::MoReFEM::QuadraturePoint& quad_pt,
                 const ::MoReFEM::GeometricElt& geom_elt,
                 ::MoReFEM::InvariantNS::Content content,
                 std::array<double, TraitsT::Ninvariants()>& invariants,
                 std::array<::MoReFEM::LocalVector, TraitsT::Ninvariants()>& invariants_first_derivative,
                 std::array<::MoReFEM::LocalMatrix, 2>& invariants_second_derivative,
                 const ::MoReFEM::FiberList<FiberPolicyT, ::MoReFEM::ParameterNS::Type::vector>* fibersI4,
                 const ::MoReFEM::FiberList<FiberPolicyT, ::MoReFEM::ParameterNS::Type::vector>* fibersI6);

    /*!
     * \brief Helper function to compute I2 matrix.
     *
     * \param[in] contravariant_metric_tensor_as_vector  Contravariant metric tensor.
     * \param[out] out Computed matrix.
     */
    [[maybe_unused]] void UpdateI2Helper(const ::MoReFEM::LocalMatrix& contravariant_metric_tensor_as_vector,
                                         ::MoReFEM::LocalMatrix& out);


} // namespace MoReFEM::Internal::InvariantNS


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Internal/UpdateHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HPP_
