/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/

#include <cassert>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Internal/UpdateHelper.hpp"


namespace MoReFEM::Internal::InvariantNS
{


    [[maybe_unused]] void UpdateI2Helper(const ::MoReFEM::LocalMatrix& contravariant_metric_tensor_as_vector,
                                         ::MoReFEM::LocalMatrix& out)
    {
        using NumericNS::Square;
        assert(contravariant_metric_tensor_as_vector.shape(0) == 6
               && contravariant_metric_tensor_as_vector.shape(1) == 1);

        const double g1 = contravariant_metric_tensor_as_vector(0, 0);
        const double g2 = contravariant_metric_tensor_as_vector(1, 0);
        const double g3 = contravariant_metric_tensor_as_vector(2, 0);
        const double g4 = contravariant_metric_tensor_as_vector(3, 0);
        const double g5 = contravariant_metric_tensor_as_vector(4, 0);
        const double g6 = contravariant_metric_tensor_as_vector(5, 0);

        // Diagonal terms.
        out(0, 0) = 0.;
        out(1, 1) = 0.;
        out(2, 2) = 0.;
        out(3, 3) = 0.5 * Square(g4) - 2. * g1 * g2;
        out(4, 4) = 0.5 * Square(g5) - 2. * g2 * g3;
        out(5, 5) = 0.5 * Square(g6) - 2. * g1 * g3;

        // Upper triangle terms and symmetry.
        out(0, 1) = out(1, 0) = g1 * g2 - 0.25 * Square(g4);
        out(0, 2) = out(2, 0) = g1 * g3 - 0.25 * Square(g6);
        out(0, 3) = out(3, 0) = 0.;
        out(0, 4) = out(4, 0) = g1 * g5 - 0.5 * g4 * g6;
        out(0, 5) = out(5, 0) = 0.;

        out(1, 2) = out(2, 1) = g2 * g3 - 0.25 * Square(g5);
        out(1, 3) = out(3, 1) = 0.;
        out(1, 4) = out(4, 1) = 0.;
        out(1, 5) = out(5, 1) = g2 * g6 - 0.5 * g4 * g5;

        out(2, 3) = out(3, 2) = g3 * g4 - 0.5 * g5 * g6;
        out(2, 4) = out(4, 2) = 0.;
        out(2, 5) = out(5, 2) = 0.;

        out(3, 4) = out(4, 3) = 0.5 * g4 * g5 - g2 * g6;
        out(3, 5) = out(5, 3) = 0.5 * g4 * g6 - g1 * g5;

        out(4, 5) = out(5, 4) = 0.5 * g5 * g6 - g3 * g4;
    }


} // namespace MoReFEM::Internal::InvariantNS


/// @} // addtogroup OperatorsGroup
