/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HXX_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HXX_

// IWYU pragma: private, include "Operators/LocalVariationalOperator/CauchyAndInvariant/Internal/UpdateHelper.hpp"

#include <array>
#include <cassert>
#include <cstddef>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "Core/Parameter/TypeEnum.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Enum.hpp"
#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantComputation.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT> class FiberList; }
namespace MoReFEM::CoordsNS { enum class CoordsPolicy; }
namespace MoReFEM::FiberNS { enum class AtNodeOrAtQuadPt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InvariantNS
{


    // clang-format off
    template
    <
        class TraitsT,
        std::size_t DimensionT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ::MoReFEM::CoordsNS::CoordsPolicy CoordsPolicyT
    >
    // clang-format on
    [[maybe_unused]] void
    UpdateHelper(const ::MoReFEM::LocalVector& cauchy_green_tensor,
                 const ::MoReFEM::QuadraturePoint& quad_pt,
                 const ::MoReFEM::GeometricElt& geom_elt,
                 ::MoReFEM::InvariantNS::Content content,
                 std::array<double, TraitsT::Ninvariants()>& invariants,
                 std::array<::MoReFEM::LocalVector, TraitsT::Ninvariants()>& invariants_first_derivative,
                 std::array<::MoReFEM::LocalMatrix, 2>& invariants_second_derivative,
                 const ::MoReFEM::FiberList<FiberPolicyT, ::MoReFEM::ParameterNS::Type::vector>* fibersI4,
                 const ::MoReFEM::FiberList<FiberPolicyT, ::MoReFEM::ParameterNS::Type::vector>* fibersI6)
    {
        namespace InvariantNS = ::MoReFEM::InvariantNS;

        switch (content)
        {
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv:
            SecondDerivativeInvariant2CauchyGreen<DimensionT, CoordsPolicyT>(
                cauchy_green_tensor,
                invariants_second_derivative[EnumUnderlyingType(InvariantNS::second_derivative_index::d2I2dCdC)]);
            SecondDerivativeInvariant3CauchyGreen<DimensionT, CoordsPolicyT>(
                cauchy_green_tensor,
                invariants_second_derivative[EnumUnderlyingType(InvariantNS::second_derivative_index::d2I3dCdC)]);
            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants_and_first_deriv:
            FirstDerivativeInvariant1CauchyGreen<DimensionT, CoordsPolicyT>(
                cauchy_green_tensor,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI1dC)]);
            FirstDerivativeInvariant2CauchyGreen<DimensionT, CoordsPolicyT>(
                cauchy_green_tensor,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI2dC)]);
            FirstDerivativeInvariant3CauchyGreen<DimensionT, CoordsPolicyT>(
                cauchy_green_tensor,
                invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI3dC)]);

            if constexpr (TraitsT::IsI4Activated())
            {
                assert(!(!fibersI4));
                const auto& tau_interpolate_at_quad_pointI4 = fibersI4->GetValue(quad_pt, geom_elt);

                FirstDerivativeInvariant4CauchyGreen<DimensionT, CoordsPolicyT>(
                    cauchy_green_tensor,
                    tau_interpolate_at_quad_pointI4,
                    invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI4dC)]);
            }

            if constexpr (TraitsT::IsI6Activated())
            {
                assert(!(!fibersI6));

                const auto& tau_interpolate_at_quad_pointI6 = fibersI6->GetValue(quad_pt, geom_elt);

                FirstDerivativeInvariant6CauchyGreen<DimensionT, CoordsPolicyT>(
                    cauchy_green_tensor,
                    tau_interpolate_at_quad_pointI6,
                    invariants_first_derivative[EnumUnderlyingType(InvariantNS::first_derivative_index::dI6dC)]);
            }
            [[fallthrough]];
        case ::MoReFEM::InvariantNS::Content::invariants:
            invariants[EnumUnderlyingType(InvariantNS::index::I1)] =
                Invariant1<DimensionT, CoordsPolicyT>(cauchy_green_tensor);
            invariants[EnumUnderlyingType(InvariantNS::index::I2)] =
                Invariant2<DimensionT, CoordsPolicyT>(cauchy_green_tensor);
            invariants[EnumUnderlyingType(InvariantNS::index::I3)] =
                Invariant3<DimensionT, CoordsPolicyT>(cauchy_green_tensor);

            if constexpr (TraitsT::IsI4Activated())
            {
                assert(!(!fibersI4));
                const auto& tau_interpolate_at_quad_pointI4 = fibersI4->GetValue(quad_pt, geom_elt);

                invariants[EnumUnderlyingType(InvariantNS::index::I4)] =
                    Invariant4<DimensionT, CoordsPolicyT>(cauchy_green_tensor, tau_interpolate_at_quad_pointI4);
            }

            if constexpr (TraitsT::IsI6Activated())
            {
                assert(!(!fibersI6));
                const auto& tau_interpolate_at_quad_pointI6 = fibersI6->GetValue(quad_pt, geom_elt);

                invariants[EnumUnderlyingType(InvariantNS::index::I6)] =
                    Invariant6<DimensionT, CoordsPolicyT>(cauchy_green_tensor, tau_interpolate_at_quad_pointI6);
            }
            break;
        } // switch
    }


} // namespace MoReFEM::Internal::InvariantNS


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CAUCHY_AND_INVARIANT_x_INTERNAL_x_UPDATE_HELPER_HXX_
