//! \file
//
//
//  EnumClass.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 26/10/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ENUM_CLASS_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ENUM_CLASS_HPP_

#include <memory>
#include <vector>


namespace MoReFEM::Internal
{


    //! Convenient enum class.
    enum class assemble_into_matrix { no, yes };

    //! Convenient enum class.
    enum class assemble_into_vector { no, yes };


} // namespace MoReFEM::Internal


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_INTERNAL_x_ENUM_CLASS_HPP_
