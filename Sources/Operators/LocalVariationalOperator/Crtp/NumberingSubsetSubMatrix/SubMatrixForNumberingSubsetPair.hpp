/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 4 May 2015 11:34:55 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HPP_

#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/MatrixOrVector.hpp"

#include "Core/Crtp/NumberingSubsetForMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    /*!
     * \brief Class which binds a \a LocalMatrix object to a pair of numbering subset.
     *
     * It is intended to be used to store efficiently the matrix structure used to assemble
     * according to a given pair of numbering subsets.
     *
     */
    class SubMatrixForNumberingSubsetPair final : public Crtp::NumberingSubsetForMatrix<SubMatrixForNumberingSubsetPair>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SubMatrixForNumberingSubsetPair;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Alias for parent.
        using parent = Crtp::NumberingSubsetForMatrix<SubMatrixForNumberingSubsetPair>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] row_numbering_subset \a NumberingSubset used for numbering the rows.
        //! \param[in] col_numbering_subset \a NumberingSubset used for numbering the columns.
        explicit SubMatrixForNumberingSubsetPair(const NumberingSubset& row_numbering_subset,
                                                 const NumberingSubset& col_numbering_subset);

        //! Destructor.
        ~SubMatrixForNumberingSubsetPair() = default;

        //! \copydoc doxygen_hide_copy_constructor
        SubMatrixForNumberingSubsetPair(const SubMatrixForNumberingSubsetPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SubMatrixForNumberingSubsetPair(SubMatrixForNumberingSubsetPair&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SubMatrixForNumberingSubsetPair& operator=(const SubMatrixForNumberingSubsetPair& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SubMatrixForNumberingSubsetPair& operator=(SubMatrixForNumberingSubsetPair&& rhs) = delete;

        ///@}

        //! Returns the local matrix.
        LocalMatrix& GetSubMatrix();


      private:
        /*!
         * \brief Local matrix to be given to Petsc function for assembling.
         *
         * This matrix is a subset of the computed elementary matrix for the operator.
         */
        LocalMatrix local_matrix_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


/// @} // addtogroup OperatorsGroup


#include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_CRTP_x_NUMBERING_SUBSET_SUB_MATRIX_x_SUB_MATRIX_FOR_NUMBERING_SUBSET_PAIR_HPP_
