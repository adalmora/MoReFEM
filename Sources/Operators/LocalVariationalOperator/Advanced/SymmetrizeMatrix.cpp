//! \file
//
//
//  SymmetrizeMatrix.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 20/07/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#include <cassert>
#include <iostream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/SymmetrizeMatrix.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    void SymmetrizeMatrix(LocalMatrix& matrix)
    {
        assert(matrix.shape().size() == 2ul);
        const auto size = matrix.shape(0);
        assert("Function must only be used on symmetrix matrices!" && size == matrix.shape(1));

#ifndef NDEBUG
        for (auto row = 0ul; row < size; ++row)
            for (auto col = row + 1; col < size; ++col)
            {
                if (!NumericNS::IsZero(matrix(col, row)))
                {
                    std::cerr << "(" << col << ", " << row << ") should be a null value but isn't... ("
                              << matrix(col, row) << ')' << std::endl;
                    assert("Lower half of the matrix should be filled with zeros!" && false);
                }
            }
#endif // NDEBUG

        for (auto row = 0ul; row < size; ++row)
            for (auto col = row; col < size; ++col)
                matrix(col, row) = matrix(row, col);
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS
