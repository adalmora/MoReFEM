//! \file
//
//
//  SymmetrizeMatrix.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 20/07/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_SYMMETRIZE_MATRIX_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_SYMMETRIZE_MATRIX_HPP_

#include "Utilities/MatrixOrVector.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Symmetrize a \a LocalMatrix
     *
     * When Seldon was used to handle local linear algebra, it provided an handy matrix class for symmetric matrices:
     * only the upper half of the matrix was in fact stored in memory, and operations were overloaded knowing the
     * represented matrix was symmetric.
     *
     * To my knowledge, there is no such thing in Xtensor. However, some \a LocalOperator were written with the idea of
     * filling only the upper half.
     *
     * Current function is then to be called at the end of such symmetric operators: it will copy the values in the
     * lower half of the matrix (in debug mode, there is a check the lower half is properly populated with zeroes).
     *
     * \param[in,out] matrix The local matrix to symmetrize. In input, it is expected only the upper half of this matrix is filled with non zero values.
     */
    void SymmetrizeMatrix(LocalMatrix& matrix);


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_SYMMETRIZE_MATRIX_HPP_
