/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorsGroup
// \addtogroup OperatorsGroup
// \{
*/


#ifndef MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATION_AT_QUADRATURE_POINT_x_ENUM_HPP_
#define MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATION_AT_QUADRATURE_POINT_x_ENUM_HPP_


namespace MoReFEM
{


    /*!
     * \brief Whether a gradient matrix should be allocated for \a InformationAtQuadraturePoint.
     *
     * This is required for \a LocalVariationalOperator that uses up gradient-based elements. If you're not sure,
     * try 'no' and run in debug mode: an assert will tell you if 'yes' was required.
     */
    enum class AllocateGradientFEltPhi { no, yes };


} // namespace MoReFEM


/// @} // addtogroup OperatorsGroup


#endif // MOREFEM_x_OPERATORS_x_LOCAL_VARIATIONAL_OPERATOR_x_ADVANCED_x_INFORMATION_AT_QUADRATURE_POINT_x_ENUM_HPP_
