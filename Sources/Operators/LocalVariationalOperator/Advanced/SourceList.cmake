### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_OP}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.cpp
		${CMAKE_CURRENT_LIST_DIR}/GradientDisplacementMatrix.cpp
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.cpp
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.cpp
		${CMAKE_CURRENT_LIST_DIR}/SymmetrizeMatrix.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hpp
		${CMAKE_CURRENT_LIST_DIR}/DerivativeGreenLagrange.hxx
		${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.hpp
		${CMAKE_CURRENT_LIST_DIR}/ExtractGradientBasedBlock.hxx
		${CMAKE_CURRENT_LIST_DIR}/GradientDisplacementMatrix.hpp
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hpp
		${CMAKE_CURRENT_LIST_DIR}/GreenLagrangeTensor.hxx
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.hpp
		${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/SymmetrizeMatrix.hpp
)

include(${CMAKE_CURRENT_LIST_DIR}/ExtractBlockFromLinearAlgebra/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InformationAtQuadraturePoint/SourceList.cmake)
