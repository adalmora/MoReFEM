//! \file
//
//
//  Main.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_HXX_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_HXX_

// IWYU pragma: private, include "Model/Main/Main.hpp"

#include <map>           // IWYU pragma: export
#include <unordered_map> // IWYU pragma: export
#include <vector>        // IWYU pragma: export

#include "Utilities/Containers/Print.hpp"               // IWYU pragma: export
#include "Utilities/Exceptions/GracefulExit.hpp"        // IWYU pragma: export
#include "Utilities/Exceptions/PrintAndAbort.hpp"       // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"   // IWYU pragma: export
#include "Utilities/InputData/InputData.hpp"            // IWYU pragma: export
#include "Utilities/InputData/Internal/Write/Write.hpp" // IWYU pragma: export
#include "Utilities/MatrixOrVector.hpp"                 // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"     // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp" // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int Main(int argc, char** argv)
    {
        try
        {
            typename ModelT::morefem_data_type morefem_data(argc, argv);

            const auto& input_data = morefem_data.GetInputData();
            const auto& mpi = morefem_data.GetMpi();

            try
            {
                ModelT model(morefem_data);
                model.Run();

                input_data.PrintUnusedLeafs(std::cout, mpi);
            }
            catch (const InputDataNS::ExceptionNS::MissingIndexedSectionDescriptionInModelSettingsTuple& e)
            {
                std::ostringstream oconv;
                oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;

                std::cout << oconv.str();
                return EXIT_FAILURE;
            }
            catch (const std::exception& e)
            {
                ExceptionNS::PrintAndAbort(mpi, e.what());
            }
        }
        catch (const ::MoReFEM::ExceptionNS::MoReFEMDataNS::NonExistingLuaFile& e)
        {
            typename ModelT::morefem_data_type::model_settings_type model_settings;
            model_settings.Init();
            model_settings.CheckTupleCompletelyFilled();
            Internal::InputDataNS::CreateDefaultInputFile<typename ModelT::morefem_data_type::input_data_type>(
                e.GetInputDataFile(), model_settings);

            std::cout << "Exception caught from MoReFEMData: " << e.what() << std::endl;
            std::cout << e.GetInputDataFile()
                      << " wasn't existing and has just been created on root processor; "
                         "please edit it and then copy it onto all machines intended to run the code in parallel."
                      << std::endl;

            return EXIT_SUCCESS;
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_HXX_
