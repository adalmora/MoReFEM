//! \file
//
//
//  Main.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_

// IWYU pragma: private, include "Model/Main/MainUpdateLuaFile.hpp"

#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Model/Main/Internal/CommandLineArgumentsForUpdateLuaFile.hpp"


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int MainUpdateLuaFile(int argc, char** argv)
    {
        try
        {
            // We're not using the model input_data_type directly as we want to tolerate missing fields in the Lua file
            // (e.g. if a new one was introduced in the tuple after the first version of the Lua file was generated).
            using tuple = typename ModelT::morefem_data_type::input_data_type::underlying_tuple_type;
            using input_data_type = MoReFEM::InputData<tuple, ::MoReFEM::InputDataNS::do_update_lua_file::yes>;

            // clang-format off
            using morefem_data_type =
                MoReFEMData
                <
                    input_data_type,
                    typename  ModelT::morefem_data_type::model_settings_type,
                    program_type::update_lua_file,
                    InputDataNS::DoTrackUnusedFields::no,
                    Internal::ModelNS::UpdateLuaFileCLI
                >;
            // clang-format on

            morefem_data_type morefem_data(argc, argv);

            Internal::InputDataNS::RewriteInputDataFile(morefem_data.GetModelSettings(), morefem_data.GetInputData(), morefem_data.DoSkipComments());
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught: " << e.what() << std::endl;

            std::cerr << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cerr << oconv.str();
            return EXIT_FAILURE;
        }

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_UPDATE_LUA_FILE_HXX_
