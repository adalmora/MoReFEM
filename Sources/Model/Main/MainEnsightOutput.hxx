//! \file
//
//
//  Main.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 07/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_
#define MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_

// IWYU pragma: private, include "Model/Main/MainEnsightOutput.hpp"

#include <cstddef>   // IWYU pragma: keep // IWYU pragma: export
#include <exception> // IWYU pragma: export

namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int MainEnsightOutput(int argc,
                          char** argv,
                          MeshNS::unique_id mesh_index,
                          const std::vector<NumberingSubsetNS::unique_id>& numbering_subset_id_list,
                          const std::vector<std::string>& unknown_list)

    {
        assert(numbering_subset_id_list.size() == unknown_list.size());

        try
        {
            // We're not using the model input_data_type directly as we want to tolerate missing fields in the Lua file
            // (e.g. if a new one was introduced in the tuple after the first version of the Lua file was generated).
            using input_data_type = typename ModelT::morefem_data_type::input_data_type;

            // clang-format off
            using morefem_data_type =
                MoReFEMData
                <
                    input_data_type,
                    typename  ModelT::morefem_data_type::model_settings_type,
                    program_type::post_processing,
                    InputDataNS::DoTrackUnusedFields::no
                >;
            // clang-format on

            morefem_data_type morefem_data(argc, argv);

            decltype(auto) input_data = morefem_data.GetInputData();
            decltype(auto) mpi = morefem_data.GetMpi();
            decltype(auto) model_settings = morefem_data.GetModelSettings();

            try
            {


                using Result = InputDataNS::Result;
                decltype(auto) result_directory_path =
                    ::MoReFEM::InputDataNS::ExtractLeaf<Result::OutputDirectory>::Path(input_data);

                FilesystemNS::Directory result_directory(mpi, result_directory_path, FilesystemNS::behaviour::read);
                {
                    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
                }

                decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance(__FILE__, __LINE__);

                const Mesh& mesh = mesh_manager.GetMesh(mesh_index);

                {
                    auto& manager =
                        Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
                    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
                }

                {
                    PostProcessingNS::OutputFormat::Ensight6 ensight_output(
                        result_directory, unknown_list, numbering_subset_id_list, mesh);
                }

                std::cout << "End of Post-Processing." << std::endl;
                std::cout << TimeKeep::GetInstance(__FILE__, __LINE__).TimeElapsedSinceBeginning() << std::endl;
            }
            catch (const ExceptionNS::GracefulExit&)
            {
                return EXIT_SUCCESS;
            }
            catch (const std::exception& e)
            {
                ExceptionNS::PrintAndAbort(mpi, e.what());
            }
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;
            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }


        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_MAIN_ENSIGHT_OUTPUT_HXX_
