
//! \file
//

#ifndef MOREFEM_x_MODEL_x_MAIN_x_INTERNAL_x_COMMAND_LINE_ARGUMENTS_FOR_UPDATE_LUA_FILE_HPP_
#define MOREFEM_x_MODEL_x_MAIN_x_INTERNAL_x_COMMAND_LINE_ARGUMENTS_FOR_UPDATE_LUA_FILE_HPP_

#include <memory>

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::InputDataNS { enum class verbosity; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::ModelNS
{


    /*!
     * \brief Supplementary command line options to use in the \a MoReFEMData policy for executables that update
     * Lua files.
     */
    struct UpdateLuaFileCLI
    {

        /*!
         * \brief Required method for the policy to add the new command line options.
         *
         * \param[in,out] command TCLAP object in charge of the command line options.
         */
        void Add(TCLAP::CmdLine& command);

        /*!
         * \brief Returns true if the Lua file should be rewritten in a more compact way,
         * with no comments, description, etc...
         *
         * \return True if comments are not to be written when updating the Lua file.
         */
        InputDataNS::verbosity DoSkipComments() const;

      private:
        //! TCLAP object which hold data about the flag skip-comment.
        std::unique_ptr<TCLAP::SwitchArg> skip_comment_arg_{ nullptr };
    };


} // namespace MoReFEM::Internal::ModelNS


#endif // MOREFEM_x_MODEL_x_MAIN_x_INTERNAL_x_COMMAND_LINE_ARGUMENTS_FOR_UPDATE_LUA_FILE_HPP_
