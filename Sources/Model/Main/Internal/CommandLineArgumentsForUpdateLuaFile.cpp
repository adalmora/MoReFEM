#include <cassert>
#include <memory>

#include "Utilities/InputData/Internal/Write/Enum.hpp"

#include "Model/Main/Internal/CommandLineArgumentsForUpdateLuaFile.hpp"


namespace MoReFEM::Internal::ModelNS
{


    void UpdateLuaFileCLI::Add(TCLAP::CmdLine& command)
    {
        skip_comment_arg_ =
            std::make_unique<TCLAP::SwitchArg>("",
                                               "skip-comments",
                                               "If this flag is set, skip all the comments and just let "
                                               "sections and leafs content.",
                                               command,
                                               false);
    };


    Internal::InputDataNS::verbosity UpdateLuaFileCLI::DoSkipComments() const
    {
        assert(!(!skip_comment_arg_));
        return skip_comment_arg_->getValue() ? InputDataNS::verbosity::compact : InputDataNS::verbosity::verbose;
    }


} // namespace MoReFEM::Internal::ModelNS
