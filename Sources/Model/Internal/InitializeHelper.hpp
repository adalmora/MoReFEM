/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ModelGroup
// \addtogroup ModelGroup
// \{
*/


#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
#define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp" // IWYU pragma: keep
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
#include "Geometry/Interpolator/CoordsMatching.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/CreateFEltSpaceList.hpp"


namespace MoReFEM::Internal::ModelNS
{


    /*!
     * \brief Init most of the singleton managers, using the data from the input data file,
     *
     * \internal This is expected to be used only  for some tests; for \a Model all the managers are actually dealt with in the  `Initialize()` method.
     *
     * \copydoc doxygen_hide_morefem_data_arg
     *
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitMostSingletonManager(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::Internal::ModelNS


/// @} // addtogroup ModelGroup


#include "Model/Internal/InitializeHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HPP_
