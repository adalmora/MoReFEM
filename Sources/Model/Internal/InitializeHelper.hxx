/*!
 //
 // \file
 //
 //
 // Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 6 Apr 2016 18:16:31 +0200
 // Copyright (c) Inria. All rights reserved.
 //
 // \ingroup ModelGroup
 // \addtogroup ModelGroup
 // \{
 */


#ifndef MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
#define MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_

// IWYU pragma: private, include "Model/Internal/InitializeHelper.hpp"

#include <cstddef> // IWYU pragma: keep
#include <optional>

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"
#include "Utilities/InputData/Internal/EmptyInputData.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"


namespace MoReFEM::Internal::ModelNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void InitMostSingletonManager(const MoReFEMDataT& morefem_data)
    {
        decltype(auto) input_data = morefem_data.GetInputData();
        decltype(auto) mpi = morefem_data.GetMpi();
        decltype(auto) model_settings = morefem_data.GetModelSettings();

        {
            auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager =
                Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = UnknownManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = DomainManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Advanced::LightweightDomainListManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = DirichletBoundaryConditionManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
        }

        {
            auto& manager = GodOfDofManager::CreateOrGetInstance(__FILE__, __LINE__);
            Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager, mpi);
        }
    }


} // namespace MoReFEM::Internal::ModelNS


/// @} // addtogroup ModelGroup


#endif // MOREFEM_x_MODEL_x_INTERNAL_x_INITIALIZE_HELPER_HXX_
