/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 10:32:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Fiber/Fiber.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDofManager.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS { struct ClearSingletons; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::FiberNS
{


    /*!
     * \brief This class is used to create and retrieve fiber_list_type objects.
     *
     * fiber_list_type objects get private constructor and can only be created through this class. In addition
     * to their creation, this class keeps their address, so it's possible from instance to retrieve a
     * fiber_list_type object given its unique id (which is the one that appears in the input data file).
     *
     * \tparam TypeT There is actually one manager for scalar parameters and another for vectorial ones.
     */
    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    class FiberListManager : public Utilities::Singleton<FiberListManager<FiberPolicyT, TypeT>>
    {

      public:
        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_indexed_section_tag_alias
        using indexed_section_tag = InputDataNS::FiberTag<FiberPolicyT, TypeT>;

        //! Alias to the type of fiber_list_type stored.
        using fiber_list_type = FiberList<FiberPolicyT, TypeT>;

        //! \copydoc doxygen_hide_clear_unique_ids_friendship
        friend MoReFEM::TestNS::ClearSingletons;

        //! Convenient alias.
        using storage_type = std::unordered_map<FiberListNS::unique_id, typename fiber_list_type::unique_ptr>;

      public:
        /*!
         * \brief Create a \a FiberList object from \a InputData and \a ModelSettings information.
         *
         * \copydoc doxygen_hide_doxygen_hide_indexed_section_description
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        // clang-format off
        template
        <
            class IndexedSectionDescriptionT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
            ::MoReFEM::Concept::InputDataType InputDataT
        >
        // clang-format on
        void Create(const IndexedSectionDescriptionT& indexed_section_description, const ModelSettingsT& model_settings, const InputDataT& input_data);

      private:
        //! Destructor.
        virtual ~FiberListManager() override = default;

      public:
        //! Fetch the \a FiberList object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{fiber_list_type}
        const fiber_list_type& GetFiberList(FiberListNS::unique_id unique_id) const;

        //! Fetch the \a FiberList object associated with \a unique_id unique identifier.
        //! \unique_id_param_in_accessor{fiber_list_type}
        fiber_list_type& GetNonCstFiberList(FiberListNS::unique_id unique_id);

        //! Fetch the \a FiberList object associated with \a unique_id unique identifier and returns it as a pointer.
        //! \unique_id_param_in_accessor{fiber_list_type}
        //! \attention Don't call \a delete on this pointer; the \a FiberListManager deals with ownership.
        const fiber_list_type* GetFiberListPtr(FiberListNS::unique_id unique_id) const;


        //! Access to the storage.
        const storage_type& GetStorage() const noexcept;

      private:
        /*!
         * \brief Method that cconstruct a new \a FiberList and store it into the class.
         *
         * \param[in] unique_id Unique identifier of the \a FiberList.
         * \param[in] fiber_file File from which the fiber data are read.
         * \copydoc doxygen_hide_parameter_domain_arg
         * \param[in] felt_space \a FEltSpace in which the \a Parameter is defined.
         * \param[in] unknown \a Unknown considered.
         *
         */
        void Create(const FiberListNS::unique_id unique_id,
                    const FilesystemNS::File& fiber_file,
                    const Domain& domain,
                    const FEltSpace& felt_space,
                    const Unknown& unknown);

        //! Time manager of the \a Model.
        const TimeManager& GetTimeManager() const noexcept;


      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_time_manager_arg
         */
        explicit FiberListManager(const TimeManager& time_manager);

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<FiberListManager>;
        ///@}

        //! \copydoc doxygen_hide_manager_clear
        void Clear();

        //! Non constant access to the storage.
        storage_type& GetNonCstStorage() noexcept;


      private:
        //! Store the god of dof objects by their unique identifier.
        storage_type storage_;

        //! Time manager of the \a Model.
        const TimeManager& time_manager_;
    };


} // namespace MoReFEM::FiberNS


/// @} // addtogroup ParameterInstancesGroup


#include "ParameterInstances/Fiber/FiberListManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HPP_
