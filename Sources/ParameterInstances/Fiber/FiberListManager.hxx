/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 12 Oct 2016 10:32:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HXX_

// IWYU pragma: private, include "ParameterInstances/Fiber/FiberListManager.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::FiberNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    const std::string& FiberListManager<FiberPolicyT, TypeT>::ClassName()
    {
        static std::string ret(std::string("FiberListManager_") + ::MoReFEM::ParameterNS::Name<TypeT>());
        return ret;
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    FiberListManager<FiberPolicyT, TypeT>::FiberListManager(const TimeManager& time_manager)
    : time_manager_(time_manager)
    {
        storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT
    >
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void FiberListManager<FiberPolicyT, TypeT>::Create(const IndexedSectionDescriptionT&,
                                                       const ModelSettingsT& model_settings,
                                                       const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) ensight_file = FilesystemNS::File{ std::filesystem::path{
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::EnsightFile>(
                input_data, model_settings) } };

        decltype(auto) domain_index = DomainNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::DomainIndex>(
                input_data, model_settings)
        };
        decltype(auto) felt_space_unique_id = FEltSpaceNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::FEltSpaceIndex>(
                input_data, model_settings)
        };
        decltype(auto) unknown_name =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::UnknownName>(
                input_data, model_settings);

        const auto& domain = DomainManager::GetInstance(__FILE__, __LINE__).GetDomain(domain_index, __FILE__, __LINE__);

        decltype(auto) mesh = domain.GetMesh();

        const auto& god_of_dof = GodOfDofManager::GetInstance(__FILE__, __LINE__).GetGodOfDof(mesh.GetUniqueId());

        const auto& felt_space = god_of_dof.GetFEltSpace(felt_space_unique_id);
        const auto& unknown = UnknownManager::GetInstance(__FILE__, __LINE__).GetUnknown(unknown_name);

        Create(FiberListNS::unique_id{ section_type::GetUniqueId() }, ensight_file, domain, felt_space, unknown);
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    void FiberListManager<FiberPolicyT, TypeT>::Create(const FiberListNS::unique_id unique_id,
                                                       const FilesystemNS::File& fiber_file,
                                                       const Domain& domain,
                                                       const FEltSpace& felt_space,
                                                       const Unknown& unknown)
    {
        // make_unique is not accepted here: it makes the code yell about private status of the constructor
        // with both clang and gcc.
        fiber_list_type* buf =
            new fiber_list_type(unique_id, fiber_file, domain, felt_space, GetTimeManager(), unknown);

        auto&& ptr = typename fiber_list_type::unique_ptr(buf);

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto& storage = GetNonCstStorage();

        auto insert_return_value = storage.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two fiber lists objects can't share the same unique identifier! (namely "
                                + std::to_string(unique_id.Get()) + ").",
                            __FILE__,
                            __LINE__);
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    const FiberList<FiberPolicyT, TypeT>&
    FiberListManager<FiberPolicyT, TypeT>::GetFiberList(FiberListNS::unique_id unique_id) const
    {
        decltype(auto) ptr = GetFiberListPtr(unique_id);
        return *ptr;
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    auto FiberListManager<FiberPolicyT, TypeT>::GetFiberListPtr(FiberListNS::unique_id unique_id) const
    -> const fiber_list_type*
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        assert(it != storage.cend());
        assert(!(!(it->second)));

        return it->second.get();
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    FiberList<FiberPolicyT, TypeT>&
    FiberListManager<FiberPolicyT, TypeT>::GetNonCstFiberList(FiberListNS::unique_id unique_id)
    {
        return const_cast<FiberList<FiberPolicyT, TypeT>&>(GetFiberList(unique_id));
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    const TimeManager& FiberListManager<FiberPolicyT, TypeT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    auto FiberListManager<FiberPolicyT, TypeT>::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT>
    auto FiberListManager<FiberPolicyT, TypeT>::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ::MoReFEM::ParameterNS::Type TypeT>
    void FiberListManager<FiberPolicyT, TypeT>::Clear()
    {
        GetNonCstStorage().clear();
        fiber_list_type::ClearUniqueIdList();
    }


} // namespace MoReFEM::FiberNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_FIBER_LIST_MANAGER_HXX_
