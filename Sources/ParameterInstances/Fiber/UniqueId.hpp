/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_UNIQUE_ID_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_UNIQUE_ID_HPP_

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::FiberListNS
{


    /*!
     * \brief Strong type to represent the \a FiberList unique id
     */
    // clang-format off
    using unique_id =
    StrongType
    <
        std::size_t,
        struct UniqueIdTag,
        StrongTypeNS::Comparable,
        StrongTypeNS::Hashable,
        StrongTypeNS::DefaultConstructible,
        StrongTypeNS::Printable,
        StrongTypeNS::AsMpiDatatype,
        StrongTypeNS::Addable
    >;
    // clang-format on


} // namespace MoReFEM::FiberListNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_UNIQUE_ID_HPP_
