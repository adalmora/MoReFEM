/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:06:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_

// IWYU pragma: private, include "ParameterInstances/Fiber/Internal/Traits.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FiberNS { enum class AtNodeOrAtQuadPt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::Internal::FiberNS
{


    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::scalar>::NvertexPerLine()
    {
        return 6u;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::vector>::NvertexPerLine()
    {
        return 2u;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::scalar>::NvaluePerVertex()
    {
        return 1u;
    }

    template<::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT>
    inline constexpr std::size_t Traits<FiberPolicyT, ParameterNS::Type::vector>::NvaluePerVertex()
    {
        return 3u;
    }


} // namespace MoReFEM::Internal::FiberNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_FIBER_x_INTERNAL_x_TRAITS_HXX_
