/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 9 Oct 2015 17:06:05 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/

#include <sstream>
#include <string>

#include "Utilities/Filesystem/File.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "ParameterInstances/Fiber/Internal/Traits.hpp"


namespace MoReFEM::Internal::FiberNS
{


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Scalar per node")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was Ensight one for a scalar "
                     "quantity (the one with scl extension)";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::scalar>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Scalar per quad_pt")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was an Ensight one for a scalar "
                     "quantity defined at the quadrature points.";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Vector per node")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was Ensight one for a vectorial "
                     "quantity (the one with vct extension).";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


    template<>
    void Traits<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_quad_pt, ParameterNS::Type::vector>::CheckFirstLineOfFile(
        const ::MoReFEM::FilesystemNS::File& fiber_file,
        const std::string& line)
    {
        if (line != "Vector per quad_pt")
        {
            std::ostringstream oconv;
            oconv << "Error in fiber file '" << fiber_file
                  << "': expected format was an Ensight one for a vectorial "
                     "quantity defined at the quadrature points.";

            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


} // namespace MoReFEM::Internal::FiberNS


/// @} // addtogroup ParameterInstancesGroup
