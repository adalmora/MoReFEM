/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/Solid/Solid.hpp"


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"
#include "ParameterInstances/Compound/Solid/Internal/IsDefined.hpp" // IWYU pragma: keep
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    Solid::Solid(const MoReFEMDataT& morefem_data,
                 const Domain& domain,
                 const QuadratureRulePerTopology& quadrature_rule_per_topology,
                 const double relative_tolerance)
    : domain_(domain), quadrature_rule_per_topology_(quadrature_rule_per_topology)
    {

        using SolidIP = InputDataNS::Solid;

        volumic_mass_ =
            InitScalarParameterFromInputData<InputDataNS::Solid::VolumicMass>("Volumic mass", domain, morefem_data);
        
        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameLambda>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameMu>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameLambda>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::LameMu>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameLambda>("Lame lambda", domain, morefem_data);
            std::get<1>(lame_coeff_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::LameMu>("Lame mu", domain, morefem_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::YoungModulus>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::PoissonRatio>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::PoissonRatio>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::YoungModulus>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(young_poisson_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::YoungModulus>("Young modulus", domain, morefem_data);


            std::get<1>(young_poisson_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::PoissonRatio>("Poisson ratio", domain, morefem_data);
        }

        if constexpr ((Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa1>())
                      || (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa2>()))
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa1>(),
                          "It makes no sense to define one and not the other");

            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Kappa2>(),
                          "It makes no sense to define one and not the other");

            std::get<0>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa1>("Kappa_1", domain, morefem_data);

            std::get<1>(kappa_list_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Kappa2>("Kappa_2", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::HyperelasticBulk>())
        {
            hyperelastic_bulk_ = InitScalarParameterFromInputData<InputDataNS::Solid::HyperelasticBulk>(
                "Hyperelastic bulk", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Viscosity>())
        {
            viscosity_ =
                InitScalarParameterFromInputData<InputDataNS::Solid::Viscosity>("Viscosity", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>())
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<0>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu1>("Mu1", domain, morefem_data);
            std::get<1>(mu_i_C_i_) =
                InitScalarParameterFromInputData<InputDataNS::Solid::Mu2>("Mu2", domain, morefem_data);
            std::get<2>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C0>("C0", domain, morefem_data);

            std::get<3>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C1>("C1", domain, morefem_data);

            std::get<4>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C2>("C2", domain, morefem_data);

            std::get<5>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C3>("C3", domain, morefem_data);
        }

        if constexpr (Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C4>()
                      || Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C5>())
        {
            static_assert(Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C0>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C1>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C2>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C3>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C4>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::C5>()
                              && Internal::SolidNS::IsDefined<MoReFEMDataT, SolidIP::Mu1>(),
                          "It makes no sense to define one and not the others");

            std::get<6>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C4>("C4", domain, morefem_data);

            std::get<7>(mu_i_C_i_) = InitScalarParameterFromInputData<InputDataNS::Solid::C5>("C5", domain, morefem_data);
        } // if constexpr C4 / C5

        if (relative_tolerance >= 0.)
            CheckConsistency(relative_tolerance);
    }


    inline const Domain& Solid::GetDomain() const noexcept
    {
        return domain_;
    }


    inline const QuadratureRulePerTopology& Solid::GetQuadratureRulePerTopology() const noexcept
    {
        return quadrature_rule_per_topology_;
    }


    inline const Solid::scalar_parameter& Solid::GetVolumicMass() const
    {
        if (!volumic_mass_)
            throw SolidNS::UndefinedData("Volumic mass", __FILE__, __LINE__);

        return *volumic_mass_;
    }


    inline const Solid::scalar_parameter& Solid::GetHyperelasticBulk() const
    {
        if (!IsHyperelasticBulk())
            throw SolidNS::UndefinedData("Hyperelastic bulk", __FILE__, __LINE__);

        assert(!(!hyperelastic_bulk_));
        return *hyperelastic_bulk_;
    }

    inline const Solid::scalar_parameter& Solid::GetKappa1() const
    {
        if (!IsKappa1())
            throw SolidNS::UndefinedData("Kappa1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetKappa2() const
    {
        if (!IsKappa2())
            throw SolidNS::UndefinedData("Kappa2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(kappa_list_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetYoungModulus() const
    {
        if (!IsYoungModulus())
            throw SolidNS::UndefinedData("YoungModulus", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetPoissonRatio() const
    {
        if (!IsPoissonRatio())
            throw SolidNS::UndefinedData("PoissonRatio", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(young_poisson_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameLambda() const
    {
        if (!IsLameLambda())
            throw SolidNS::UndefinedData("Lame lambda", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetLameMu() const
    {
        if (!IsLameMu())
            throw SolidNS::UndefinedData("Lame mu", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(lame_coeff_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetMu1() const
    {
        if (!IsMu1())
            throw SolidNS::UndefinedData("Mu1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<0>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetMu2() const
    {
        if (!IsMu2())
            throw SolidNS::UndefinedData("Mu2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<1>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC0() const
    {
        if (!IsC0())
            throw SolidNS::UndefinedData("C0", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<2>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC1() const
    {
        if (!IsC1())
            throw SolidNS::UndefinedData("C1", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<3>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC2() const
    {
        if (!IsC2())
            throw SolidNS::UndefinedData("C2", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<4>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC3() const
    {
        if (!IsC3())
            throw SolidNS::UndefinedData("C3", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<5>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }

    inline const Solid::scalar_parameter& Solid::GetC4() const
    {
        if (!IsC4())
            throw SolidNS::UndefinedData("C4", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<6>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetC5() const
    {
        if (!IsC5())
            throw SolidNS::UndefinedData("C5", __FILE__, __LINE__);

        decltype(auto) ptr = std::get<7>(mu_i_C_i_);
        assert(!(!ptr));
        return *ptr;
    }


    inline const Solid::scalar_parameter& Solid::GetViscosity() const
    {
        if (!IsViscosity())
            throw SolidNS::UndefinedData("Viscosity", __FILE__, __LINE__);

        assert(!(!viscosity_));
        return *viscosity_;
    }


    inline bool Solid::IsHyperelasticBulk() const noexcept
    {
        return hyperelastic_bulk_ != nullptr;
    }


    inline bool Solid::IsKappa1() const noexcept
    {
        return std::get<0>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsKappa2() const noexcept
    {
        return std::get<1>(kappa_list_) != nullptr;
    }


    inline bool Solid::IsYoungModulus() const noexcept
    {
        return std::get<0>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsPoissonRatio() const noexcept
    {
        return std::get<1>(young_poisson_) != nullptr;
    }


    inline bool Solid::IsLameLambda() const noexcept
    {
        return std::get<0>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsLameMu() const noexcept
    {
        return std::get<1>(lame_coeff_) != nullptr;
    }


    inline bool Solid::IsMu1() const noexcept
    {
        return std::get<0>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsMu2() const noexcept
    {
        return std::get<1>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC0() const noexcept
    {
        return std::get<2>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC1() const noexcept
    {
        return std::get<3>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC2() const noexcept
    {
        return std::get<4>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC3() const noexcept
    {
        return std::get<5>(mu_i_C_i_) != nullptr;
    }

    inline bool Solid::IsC4() const noexcept
    {
        return std::get<6>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsC5() const noexcept
    {
        return std::get<7>(mu_i_C_i_) != nullptr;
    }


    inline bool Solid::IsViscosity() const noexcept
    {
        return viscosity_ != nullptr;
    }


} // namespace MoReFEM


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_SOLID_HXX_
