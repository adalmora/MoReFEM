/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Feb 2016 14:47:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_INTERNAL_x_IS_DEFINED_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_INTERNAL_x_IS_DEFINED_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/Solid/Internal/IsDefined.hpp"


// IWYU pragma: private, include "ParameterInstances/Compound/Solid/Solid.hpp"


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"

#include "ParameterInstances/Compound/Solid/Exceptions/Solid.hpp"
#include "ParameterInstances/Compound/Solid/Internal/IsDefined.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::SolidNS
{


    template<::MoReFEM::Concept::InputDataOrModelSettingsType DataT, class T>
    constexpr bool IsDefinedHelper()
    {
        if constexpr (Utilities::Tuple::IndexOf<T, typename DataT::underlying_tuple_type>::value
                      != NumericNS::UninitializedIndex<std::size_t>())
            return true;
        else
            // If InputDataNS::Solid is defined, ALL parameters are defined and true should be returned!
            return (Utilities::Tuple::IndexOf<::MoReFEM::InputDataNS::Solid, typename DataT::underlying_tuple_type>::value
                != NumericNS::UninitializedIndex<std::size_t>());
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, class T>
    constexpr bool IsDefined()
    {
        using input_data_type = typename MoReFEMDataT::input_data_type;

        constexpr auto is_defined_in_input_data = IsDefinedHelper<input_data_type, T>();

        if constexpr (is_defined_in_input_data)
            return true;
        else
        {
            using model_settings_type = typename MoReFEMDataT::model_settings_type;

            return IsDefinedHelper<model_settings_type, T>();
        }

    }

} // namespace MoReFEM::Internal::SolidNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_SOLID_x_INTERNAL_x_IS_DEFINED_HXX_
