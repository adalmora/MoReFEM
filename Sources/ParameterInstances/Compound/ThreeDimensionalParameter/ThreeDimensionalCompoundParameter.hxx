/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 28 May 2015 14:52:56 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HXX_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HXX_

// IWYU pragma: private, include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"


namespace MoReFEM::ParameterNS
{


    template<template<Type> class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplWrite(std::ostream& out) const
    {
        out << "# This parameter is defined from three scalar parameters:" << std::endl;
        out << std::endl;
        GetScalarParameterX().Write(out);
        out << std::endl;
        GetScalarParameterY().Write(out);
        out << std::endl;
        GetScalarParameterZ().Write(out);
    }


    template<template<Type> class TimeDependencyT>
    template<class T>
    ThreeDimensionalCoumpoundParameter<TimeDependencyT>::ThreeDimensionalCoumpoundParameter(
        T&& name,
        scalar_parameter_no_time_dep_ptr&& x_component,
        scalar_parameter_no_time_dep_ptr&& y_component,
        scalar_parameter_no_time_dep_ptr&& z_component)
    : parent(std::forward<T>(name), x_component->GetDomain()), scalar_parameter_x_(std::move(x_component)),
      scalar_parameter_y_(std::move(y_component)), scalar_parameter_z_(std::move(z_component))
    {
        static_assert(std::is_convertible<self*, parent*>());

        assert(parent::GetDomain() == GetScalarParameterY().GetDomain());
        assert(parent::GetDomain() == GetScalarParameterZ().GetDomain());

        content_.resize({ 3 });

        if (IsConstant())
        {
            content_(0) = GetScalarParameterX().GetConstantValue();
            content_(1) = GetScalarParameterY().GetConstantValue();
            content_(2) = GetScalarParameterZ().GetConstantValue();
        }


        // clang-format off
        // This block is just a static sanity check.
        {
            using scalar_param_type = decltype(GetScalarParameterX());
            
            static_assert(std::is_same
                          <
                              scalar_param_type,
                              decltype(GetScalarParameterY())
                          >());
              
            static_assert(std::is_same
                          <
                              scalar_param_type,
                              decltype(GetScalarParameterZ())
                          >());
              
            using attribute_time_dep = typename std::remove_reference_t<scalar_param_type>::time_dependency;
            
            using expected_time_dep =
                TimeDependencyNS::None
                <
                    Type::scalar
                >;
            
            static_assert(std::is_same
                          <
                              expected_time_dep,
                              attribute_time_dep
                          >(),
                          "This class has been designed so that time dependency is handled "
                          "at a global level, not for each of its scalar parameters. If you "
                          "get this message some refactoring concerning this class was "
                          "performed; please check with library developer - either the "
                          "refactoring entails this static check is no longer warranted, "
                          "or the refactoring didn't fully grasp how the class works in "
                          "the first place.");
        }
        // clang-format on
    }


    template<template<Type> class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeDependencyT>::GetScalarParameterX() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_x_));
        return *scalar_parameter_x_;
    }


    template<template<Type> class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeDependencyT>::GetScalarParameterY() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_y_));
        return *scalar_parameter_y_;
    }


    template<template<Type> class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeDependencyT>::GetScalarParameterZ() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_z_));
        return *scalar_parameter_z_;
    }


    template<template<Type> class TimeDependencyT>
    inline bool ThreeDimensionalCoumpoundParameter<TimeDependencyT>::IsConstant() const
    {
        return GetScalarParameterX().IsConstant() && GetScalarParameterY().IsConstant()
               && GetScalarParameterZ().IsConstant();
    }


    template<template<Type> class TimeDependencyT>
    inline typename ThreeDimensionalCoumpoundParameter<TimeDependencyT>::return_type
    ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplGetConstantValue() const
    {
        return content_;
    }


    template<template<Type> class TimeDependencyT>
    inline typename ThreeDimensionalCoumpoundParameter<TimeDependencyT>::return_type
    ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplGetValue(const local_coords_type& local_coords,
                                                                       const GeometricElt& geom_elt) const
    {
        content_(0) = GetScalarParameterX().GetValue(local_coords, geom_elt);
        content_(1) = GetScalarParameterY().GetValue(local_coords, geom_elt);
        content_(2) = GetScalarParameterZ().GetValue(local_coords, geom_elt);

        return content_;
    }


    template<template<Type> class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplTimeUpdate()
    {
        GetScalarParameterX().TimeUpdate();
        GetScalarParameterY().TimeUpdate();
        GetScalarParameterZ().TimeUpdate();
    }


    template<template<Type> class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplTimeUpdate(double time)
    {
        GetScalarParameterX().TimeUpdate(time);
        GetScalarParameterY().TimeUpdate(time);
        GetScalarParameterZ().TimeUpdate(time);
    }

    template<template<Type> class TimeDependencyT>
    auto ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SupplGetAnyValue() const -> return_type
    {
        return content_;
    }

    template<template<Type> class TimeDependencyT>
    inline void ThreeDimensionalCoumpoundParameter<TimeDependencyT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS


/// @} // addtogroup ParameterInstancesGroup


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_THREE_DIMENSIONAL_PARAMETER_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HXX_
