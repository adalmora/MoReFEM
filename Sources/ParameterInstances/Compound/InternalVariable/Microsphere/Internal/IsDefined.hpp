/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Mon, 27 May 2019 17:35:05 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup ParameterInstancesGroup
// \addtogroup ParameterInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INTERNAL_x_IS_DEFINED_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INTERNAL_x_IS_DEFINED_HPP_

// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/Microsphere/InputMicrosphere.hpp"

#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/Containers/Tuple/Tuple.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Exceptions/InputMicrosphere.hpp"
#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


namespace MoReFEM::Internal::InputMicrosphereNS
{


    /*!
     * \brief Check whether type \a T is defined within \a DataT.
     *
     * This function is used as helper for \a IsDefined() that checks both \a InputData and \a ModelSettings.
     *
     * \return True if \a T was found directly or indirectly (within a \a ::MoReFEM::InputDataNS::Microsphere section)
     */
    template<::MoReFEM::Concept::InputDataOrModelSettingsType DataT, class T>
    constexpr bool IsDefinedHelper();


    /*!
     * \brief Check whether type \a T is defined within \a MoReFEMDataT::input_data_type
     * or \a MoReFEMDataT::model_settings_type
     *
     * \return True if \a T was found directly or indirectly (within a \a ::MoReFEM::InputDataNS::Microsphere section)
     * in either \a MoReFEMDataT::input_data_type or \a MoReFEMDataT::model_settings_type.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT, class T>
    constexpr bool IsDefined();


} // namespace MoReFEM::Internal::InputMicrosphereNS


/// @} // addtogroup ParameterInstancesGroup

#include "ParameterInstances/Compound/InternalVariable/Microsphere/Internal/IsDefined.hxx" // IWYU pragma: export

#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_MICROSPHERE_x_INTERNAL_x_IS_DEFINED_HPP_
