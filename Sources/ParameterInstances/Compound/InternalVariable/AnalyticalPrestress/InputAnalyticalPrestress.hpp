/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HPP_
#define MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HPP_

#include <memory>
#include <vector>

#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/Parameter/AnalyticalPrestress/AnalyticalPrestress.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
#include "Parameters/Parameter.hpp"


namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    /*!
     * \brief Policy to use when \a InputAnalyticalPrestress is involved in \a
     * SecondPiolaKirchhoffStressTensor.
     *
     * \todo #9 (Gautier) Explain difference with AnalyticalPrestress.
     */
    struct InputAnalyticalPrestress final
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = InputAnalyticalPrestress;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         * 
         * \copydoc doxygen_hide_parameter_domain_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit InputAnalyticalPrestress(const MoReFEMDataT& morefem_data, const Domain& domain);

      public:
        //! Constant accessor on contractility.
        const ScalarParameter<>& GetContractility() const noexcept;

        //! Constant accessor on the initial value of the active stress.
        double GetInitialValueInternalVariable() const noexcept;

      private:
        //! Contracitility.
        ScalarParameter<>::unique_ptr contractility_ = nullptr;

        //! Initial value of the active stress.
        const double initial_value_internal_variable_;
    };


} // namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


/// @} // addtogroup OperatorInstancesGroup


#include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hxx" // IWYU pragma: export


#endif // MOREFEM_x_PARAMETER_INSTANCES_x_COMPOUND_x_INTERNAL_VARIABLE_x_ANALYTICAL_PRESTRESS_x_INPUT_ANALYTICAL_PRESTRESS_HPP_
