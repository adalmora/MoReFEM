/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
*/

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/MoReFEMDataForTest.hpp"


#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM
{


    template<class ModelSettingsT>
    MoReFEMDataForTest<ModelSettingsT>::MoReFEMDataForTest(const std::filesystem::path& result_directory_subpath) : parent()
    {
        decltype(auto) environment = Utilities::Environment::GetInstance(__FILE__, __LINE__);
        auto test_dir =
            std::filesystem::path{ environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR", __FILE__, __LINE__) };

        test_dir /= result_directory_subpath;

        parent::SetResultDirectory(test_dir, Internal::MoReFEMDataNS::overwrite_directory::yes);
        
        model_settings_.Init();
        model_settings_.CheckTupleCompletelyFilled();
    }


    template<class ModelSettingsT>
    MoReFEMDataForTest<ModelSettingsT>::~MoReFEMDataForTest() = default;


    template<class ModelSettingsT>
    auto MoReFEMDataForTest<ModelSettingsT>::GetInputData() const noexcept -> const input_data_type&
    {
        return input_data_;
    }


    template<class ModelSettingsT>
    auto MoReFEMDataForTest<ModelSettingsT>::GetModelSettings() const noexcept
    -> const model_settings_type&
    {
        return model_settings_;
    }


    template<class ModelSettingsT>
    const Internal::Parallelism* MoReFEMDataForTest<ModelSettingsT>::GetParallelismPtr() const noexcept
    {
        return nullptr; // seems allright for the time being; maybe we'll have to flesh it out later.
    }



} // namespace MoReFEM

#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HXX_
