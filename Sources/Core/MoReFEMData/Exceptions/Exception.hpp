/*!
 */


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXCEPTIONS_x_EXCEPTION_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXCEPTIONS_x_EXCEPTION_HPP_

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"


namespace MoReFEM::ExceptionNS::MoReFEMDataNS
{


    //! Called when a method makes no sense for a given time step policy.,
    class NonExistingLuaFile : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] input_data_file Input data file not found.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit NonExistingLuaFile(FilesystemNS::File&& input_data_file, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~NonExistingLuaFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        NonExistingLuaFile(const NonExistingLuaFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonExistingLuaFile(NonExistingLuaFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonExistingLuaFile& operator=(const NonExistingLuaFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonExistingLuaFile& operator=(NonExistingLuaFile&& rhs) = delete;

        //! Get the path of the Lua file that was not found.
        const FilesystemNS::File& GetInputDataFile() const noexcept;


      private:
        //! Lua file that was not found.
        FilesystemNS::File input_data_file_;
    };


} // namespace MoReFEM::ExceptionNS::MoReFEMDataNS


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXCEPTIONS_x_EXCEPTION_HPP_
