/*!
 //
 // \file
 //
 // \ingroup CoreGroup
 // \addtogroup CoreGroup
 // \{
 */

#include <sstream>
#include <string>
#include <utility>

#include "Core/MoReFEMData/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string NonExistingLuaFileMsg(const MoReFEM::FilesystemNS::File& input_data_file);


} // namespace


namespace MoReFEM::ExceptionNS::MoReFEMDataNS
{


    NonExistingLuaFile::~NonExistingLuaFile() = default;


    NonExistingLuaFile::NonExistingLuaFile(FilesystemNS::File&& input_data_file,
                                           const char* invoking_file,
                                           int invoking_line)
    : MoReFEM::Exception(NonExistingLuaFileMsg(std::move(input_data_file)), invoking_file, invoking_line),
      input_data_file_(std::move(input_data_file))
    { }


    const FilesystemNS::File& NonExistingLuaFile::GetInputDataFile() const noexcept
    {
        return input_data_file_;
    }

} // namespace MoReFEM::ExceptionNS::MoReFEMDataNS


namespace // anonymous
{


    std::string NonExistingLuaFileMsg(const MoReFEM::FilesystemNS::File& input_data_file)
    {
        std::ostringstream oconv;
        oconv << "Provided Lua file " << input_data_file << " wasn't found by the root processor.";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup CoreGroup
