//! \file

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Extract.hpp"


#include "Core/MoReFEMData/MoReFEMData.hpp" // IWYU pragma: export

namespace MoReFEM::InputDataNS
{

    /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * This is syntactic sugar over a namesake function in `Utilities` which takes two arguments: \a input_data and
     * \a model_settings. Here we take directly both those objects from \a morefem_data one.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     *
     * \copydoc doxygen_hide_morefem_data_arg
     *
     * \return Value found either in InputData or ModelSettings objects stored in \a morefem_data.Can't be found in both
     * - in this case the model would not even compile.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    decltype(auto) ExtractLeafFromInputDataOrModelSettings(const MoReFEMDataT& morefem_data)
    {
        return ExtractLeafFromInputDataOrModelSettings<LeafNameT>(morefem_data.GetInputData(),
                                                                  morefem_data.GetModelSettings());
    }

} // namespace MoReFEM::InputDataNS



#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HXX_
