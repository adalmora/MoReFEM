/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 21 Jan 2015 15:50:06 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/MoReFEMData.hpp"

#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp" 
#include "Core/MoReFEMData/Exceptions/Exception.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    // clang-format on
    constexpr bool MoReFEMData<InputDataT, ModelSettingsT, ProgramTypeT, DoTrackUnusedFieldsT, AdditionalCommandLineArgumentsPolicyT>::
        HasParallelismField()
    {
        return InputDataT::template Find<InputDataNS::Parallelism>();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::MoReFEMData(int argc, char** argv)
    : parent(argc, argv)
    // clang-format on
    {
        auto ptr = std::is_same<AdditionalCommandLineArgumentsPolicyT, std::nullptr_t>() ? nullptr : this;

        try
        {
            Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory{};

            // clang-format off
            auto input_data_file =
                ParseCommandLine
                <
                    ProgramTypeT,
                    AdditionalCommandLineArgumentsPolicyT
                >(argc, argv, do_overwrite_directory, ptr);
            // clang-format on

            Construct(std::move(input_data_file), do_overwrite_directory);
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::MoReFEMData(FilesystemNS::File&& input_data_file)
    : parent()
    // clang-format on
    {
        static_assert(std::is_same_v<AdditionalCommandLineArgumentsPolicyT, std::false_type>,
                      "This constructor is dedicated for simple tests with no additional arguments on command line.");

        Construct(std::move(input_data_file), Internal::MoReFEMDataNS::overwrite_directory::yes);
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    void MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::Construct(FilesystemNS::File&& input_data_file,
                 Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory)
    // clang-format on
    {
        try
        {
            {
                model_settings_.Init();
                model_settings_.CheckTupleCompletelyFilled();
            }
            
            decltype(auto) mpi = parent::GetMpi();

            if (mpi.IsRootProcessor())
            {
                if (!input_data_file.DoExist())
                    throw ::MoReFEM::ExceptionNS::MoReFEMDataNS::NonExistingLuaFile(
                        std::move(input_data_file), __FILE__, __LINE__);
            }

            Internal::MoReFEMDataNS::CheckExistingForAllRank(mpi, input_data_file);

            // We can be here only if the file exists...
            input_data_ = std::make_unique<InputDataT>(GetModelSettings(), input_data_file, DoTrackUnusedFieldsT);

            CheckNoMissingIndexedSectionDescriptions();
            
            {
                if constexpr (ProgramTypeT != program_type::update_lua_file)
                {
                    if constexpr (InputDataT::template Find<InputDataNS::Result::OutputDirectory>())
                    {
                        using Result = InputDataNS::Result;
                        auto path = InputDataNS::ExtractLeaf<Result::OutputDirectory>::Path(*input_data_);
                        parent::SetResultDirectory(std::move(path), do_overwrite_directory);
                    }
                }

                if constexpr (InputDataT::template Find<InputDataNS::Result::BinaryOutput>())
                {
                    const auto& binary_output =
                        InputDataNS::ExtractLeaf<InputDataNS::Result::BinaryOutput>::Value(*input_data_);
                    Utilities::AsciiOrBinary::CreateOrGetInstance(__FILE__, __LINE__, binary_output);
                }

                if constexpr (ProgramTypeT != program_type::update_lua_file)
                {
                    // Parallelism is an optional field: it might not be present in the Lua file (for tests for instance
                    // it is not meaningful).
                    // If we are updating a Lua file, we don't want to create it: doing so requires to interpret
                    // the environment variables, and we don't want that in the updated Lua file (the goal is
                    // just to update comments and field, not to replace environment variables by their specific
                    // values on the machine onto which update_lua_file program is called).
                    if constexpr (HasParallelismField())
                        parallelism_ = std::make_unique<Internal::Parallelism>(
                            mpi, *input_data_, parent::DetermineDirectoryBehaviour(do_overwrite_directory));
                }
            }

            mpi.Barrier();

            if constexpr (InputDataT::template Find<InputDataNS::Solid::CheckInvertedElements>())
            {
                const auto& do_check_inverted_elements =
                    InputDataNS::ExtractLeaf<InputDataNS::Solid::CheckInvertedElements>::Value(*input_data_);

                MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements ::CreateOrGetInstance(
                    __FILE__, __LINE__, do_check_inverted_elements);
            } else
            {
                // Create it nonetheless - but will trigger an exception if the value is called.
                MoReFEM::Internal::MoReFEMDataNS::CheckInvertedElements::CreateOrGetInstance(__FILE__, __LINE__);
            }
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::~MoReFEMData()
    // clang-format on
    {
        input_data_ = nullptr;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    auto
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetInputData() const noexcept
    -> const InputDataT&
    // clang-format on
    {
        assert(!(!input_data_));
        return *input_data_;
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    auto MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetModelSettings() const noexcept
    -> const ModelSettingsT&
    // clang-format on
    {
        return model_settings_;
    }

    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism*
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelismPtr() const noexcept
    // clang-format on
    {
        if (!parallelism_)
            return nullptr;

        return parallelism_.get();
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    const Internal::Parallelism&
    MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::GetParallelism() const noexcept
    // clang-format on
    {
        assert(!(!parallelism_));
        return *parallelism_;
    }


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void PrecomputeExit(const MoReFEMDataT& morefem_data)
    {
        const auto parallelism_ptr = morefem_data.GetParallelismPtr();

        if (!(!parallelism_ptr))
        {
            switch (parallelism_ptr->GetParallelismStrategy())
            {
            case Advanced::parallelism_strategy::precompute:
            {
                if (morefem_data.GetMpi().IsRootProcessor())
                    std::cout << "The parallelism elements have been precomputed; the program will now end."
                              << std::endl;

                throw ExceptionNS::GracefulExit(__FILE__, __LINE__);
            }
            case Advanced::parallelism_strategy::run_from_preprocessed:
            case Advanced::parallelism_strategy::parallel:
            case Advanced::parallelism_strategy::parallel_no_write:
                break;
            case Advanced::parallelism_strategy::none:
            {
                assert(false && "Should not be possible (if so parallelism_ptr should have been nullptr).");
                exit(EXIT_FAILURE);
            }
            }
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        program_type ProgramTypeT,
        InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT,
        class AdditionalCommandLineArgumentsPolicyT
    >
    void MoReFEMData
    <
        InputDataT,
        ModelSettingsT,
        ProgramTypeT,
        DoTrackUnusedFieldsT,
        AdditionalCommandLineArgumentsPolicyT
    >::CheckNoMissingIndexedSectionDescriptions() const
    // clang-format on
    {
        decltype(auto) input_data = GetInputData();
        decltype(auto) model_settings = GetModelSettings();

        Internal::InputDataNS::CheckNoMissingIndexedSectionDescriptions(input_data, model_settings);
        Internal::InputDataNS::CheckNoMissingIndexedSectionDescriptions(model_settings, model_settings);
    }



} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_HXX_
