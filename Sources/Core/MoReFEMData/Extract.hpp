//! \file

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HPP_

#include "Utilities/InputData/Advanced/Concept.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/Advanced/Concept.hpp"  // IWYU pragma: export


namespace MoReFEM::InputDataNS
{

  /*!
     * \brief Extract a model-related specific information from a leaf either from input data (exclusive-)OR from model settings.
     *
     * This is syntactic sugar over a namesake function in `Utilities` which takes two arguments: \a input_data and
     * \a model_settings. Here we take directly both those objects from \a morefem_data one.
     *
     * \tparam LeafT Full type of the leaf which value is sought. Might be for instance
     * InputDataNS::FEltSpace<3>::DomainIndex.
     *
     * \copydoc doxygen_hide_morefem_data_arg
     *
     * \return Value found either in InputData or ModelSettings objects stored in \a morefem_data.Can't be found in both
     * - in this case the model would not even compile.
     */
    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    decltype(auto) ExtractLeafFromInputDataOrModelSettings(const MoReFEMDataT& morefem_data);
                                                           


} // namespace MoReFEM::InputDataNS


#include "Core/MoReFEMData/Extract.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_EXTRACT_HPP_
