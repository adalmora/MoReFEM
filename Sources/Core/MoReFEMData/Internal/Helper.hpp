//! \file
//
//
//  Helper.hpp
//  MoReFEM
//
//  Created by sebastien on 30/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HPP_

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/Filesystem/File.hpp"      // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class program_type; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MoReFEMDataNS
{


    //! In the 'model' program type, default behaviour is to ask the user whether he wants to erase the output
    //! directory if it already exists. This enum class acknowledges another choice might be made
    //! (if --overwrite_directory is given on command line).
    enum class overwrite_directory { yes, no };


    /*!
     * \brief Parse the command line and extract its information.
     *
     * \tparam AdditionalCommandLineArgumentsPolicyT Policy if you need additional arguments on the command line.
     * To see a concrete example of this possibility, have a look at Test/Core/MoReFEMData/test_command_line_options.cpp
     * which demonstrate the possibility. If none, use std::false_type.
     * \tparam ProgramTypeT Type of the program. If 'model', an additional flag --overwrite_directory is enabled on
     * command line.
     *
     * \param[in] argc Number of argument in the command line (including the program name).
     * \param[in] argv List of arguments read.
     * \param[in] additional If AdditionalCommandLineArgumentsPolicyT is not nullptr, give it the this pointer of
     * MoReFEMData
     * \param[out] do_overwrite_directory Whether the flag --overwrite_directory was found on the command line or not.
     * This flag may be given only with ProgramTypeT == program_type::model.
     *
     * \return Path of the input data file.
     */
    template<program_type ProgramTypeT, class AdditionalCommandLineArgumentsPolicyT>
    ::MoReFEM::FilesystemNS::File ParseCommandLine(int argc,
                                                   char** argv,
                                                   overwrite_directory& do_overwrite_directory,
                                                   AdditionalCommandLineArgumentsPolicyT* additional = nullptr);


    /*!
     * \brief Define the few environment variables required to make MoReEM work properly.
     *
     * \copydetails doxygen_hide_mpi_param
     */
    void DefineEnvironmentVariable(const ::MoReFEM::Wrappers::Mpi& mpi);


    /*!
     * \brief Check the input data file was properly found for all ranks.
     *
     * If not, an exception is thrown.
     *
     * \copydetails doxygen_hide_mpi_param
     * \param[in] input_data_file Path to the input data file to be created.
     */
    void CheckExistingForAllRank(const ::MoReFEM::Wrappers::Mpi& mpi,
                                 const ::MoReFEM::FilesystemNS::File& input_data_file);


} // namespace MoReFEM::Internal::MoReFEMDataNS


#include "Core/MoReFEMData/Internal/Helper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HPP_
