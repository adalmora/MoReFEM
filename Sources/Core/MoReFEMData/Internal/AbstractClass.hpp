/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HPP_

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"
#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"     // IWYU pragma: export
#include "Utilities/InputData/ModelSettings.hpp" // IWYU pragma: export
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "Core/InitTimeKeepLog.hpp"

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/InputData/Instances/Result.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Internal/Helper.hpp"      // IWYU pragma: export
#include "Core/MoReFEMData/Internal/Parallelism.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"
#include "Core/MoReFEMData/Internal/CheckInvertedElements.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    /*!
     * \brief Init MoReFEM: initialize mpi and read the input data file.
     *
     * \warning As mpi is not assumed to exist until the constructor has done is job, the exceptions there that might
     * happen only on some of the ranks don't lead to a call to MPI_Abort(), which can lead to a dangling program.
     * Make sure each exception is properly communicated to all ranks so that each rank can gracefully throw
     * an exception and hence allow the program to stop properly.
     *
     * \tparam ProgramTypeT Type of the program run. For instance for post-processing there is no removal of the
     * existing result directory, contrary to what happens in model run. If update_lua_file, the result directory can't
     * be queried at all

     *
     * http://tclap.sourceforge.net gives a nice manual of how to add additional argument on the command lines.
     * By default, there is one mandatory argument (--input_data *lua file*) and one optional that might be
     * repeated to define pseudo environment variables (-e KEY=VALUE).
     */
    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT = ::MoReFEM::InputDataNS::DoTrackUnusedFields::yes
    >
    // clang-format on
    class AbstractClass
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        // clang-format off
        using self = AbstractClass
                     <
                         ProgramTypeT,
                         DoTrackUnusedFieldsT
                     >;
        // clang-format on

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * This is the staple constructor used in models: at the beginning of the main the object is constructed and
         * initializes under the hood stuff like the initialization of MPI (which requires the \a argc and \a argv
         * values).
         *
         * \param[in] argc Number of argument in the command line (including the program name).
         * \param[in] argv List of arguments read.
         */
        explicit AbstractClass(int argc, char** argv);


        /*!
         * \brief Constructor from a Lua file.
         *
         * Useful for tests.
         *
         * \attention This constructor does not initialize \a Internal::PetscNS::RAII singleton and assumes this has already be done!
         */
        explicit AbstractClass();


        //! Destructor.
        virtual ~AbstractClass() = 0;

        //! \copydoc doxygen_hide_copy_constructor
        AbstractClass(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AbstractClass(AbstractClass&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AbstractClass& operator=(const AbstractClass& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AbstractClass& operator=(AbstractClass&& rhs) = delete;

        ///@}

        //! Accessor to underlying mpi object.
        const ::MoReFEM::Wrappers::Mpi& GetMpi() const noexcept;

        /*!
         * \brief Accessor to the result directory, in which all the outputs of MoReFEM should be written.
         *
         * \return Result directory.
         */
        const ::MoReFEM::FilesystemNS::Directory& GetResultDirectory() const noexcept;

      protected:
        /*!
         * \brief Determine the behaviour to adopt for output directory creation.
         *
         * This behaviour depends both on the \a do_overwrite_directory argument and on the type of program you are
         * running (model, test, update a Lua file, etc...)
         *
         * \param[in] do_overwrite_directory Whether an eventual pre-existing output directory should be overwritten.
         *
         * \return Behaviour to adopt for output directory creation.
         */
        ::MoReFEM::FilesystemNS::behaviour
        DetermineDirectoryBehaviour(Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory) const noexcept;

        /*!
         * \brief Set the directory into which outputs will be written.
         *
         * \param[in] path Path on your filesystem to the directory in which the outputs should be written. It should be determined internally
         * in the class derived from this one (\a MoReFEMData or \a MoReFEMDataForTest so far are possible).
         * \param[in] do_overwrite_directory Whether an eventual pre-existing output directory should be overwritten.
         */
        void SetResultDirectory(std::filesystem::path path,
                                Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory);


      private:
        /*!
         * \brief Helper function used in constructors.
         */
        void Construct();

      private:
        //! Directory into which model results will be written,
        ::MoReFEM::FilesystemNS::Directory::const_unique_ptr result_directory_{ nullptr };
    };


} // namespace MoReFEM::Internal::MoReFEMDataNS


/// @} // addtogroup CoreGroup


#include "Core/MoReFEMData/Internal/AbstractClass.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HPP_
