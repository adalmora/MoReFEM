//! \file
//
//
//  Helper.hxx
//  MoReFEM
//
//  Created by sebastien on 30/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Internal/Helper.hpp"

#include <cassert>
#include <filesystem>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Tclap/Tclap.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Core/MoReFEMData/Enum.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::MoReFEMDataNS { enum class overwrite_directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::MoReFEMDataNS
{


    template<program_type ProgramTypeT, class AdditionalCommandLineArgumentsPolicyT>
    ::MoReFEM::FilesystemNS::File ParseCommandLine(int argc,
                                                   char** argv,
                                                   overwrite_directory& do_overwrite_directory,
                                                   AdditionalCommandLineArgumentsPolicyT* additional)
    {
        try
        {
            TCLAP::CmdLine cmd("Command description message");
            cmd.setExceptionHandling(false);

            TCLAP::ValueArg<std::string> input_data_file_arg(
                "i", "input_data", "Input data file (Lua)", true, "", "string", cmd);

            TCLAP::MultiArg<::MoReFEM::Wrappers::Tclap::StringPair> env_arg(
                "e", "env", "environment_variable", false, "string=string", cmd);

            std::unique_ptr<TCLAP::SwitchArg> overwrite_directory_arg = nullptr;

            if constexpr (ProgramTypeT == program_type::model)
            {
                overwrite_directory_arg =
                    std::make_unique<TCLAP::SwitchArg>("",
                                                       "overwrite_directory",
                                                       "If this flag is set, the output directory will be removed "
                                                       "silently "
                                                       "if it already exists.",
                                                       cmd,
                                                       false);
            }

            if constexpr (!std::is_same<AdditionalCommandLineArgumentsPolicyT, std::false_type>())
            {
                assert(!(!additional));
                additional->Add(cmd);
            } else
                static_cast<void>(additional);

            cmd.parse(argc, argv);

            if constexpr (ProgramTypeT == program_type::model)
            {
                assert(!(!overwrite_directory_arg));
                do_overwrite_directory =
                    overwrite_directory_arg->getValue() ? overwrite_directory::yes : overwrite_directory::no;
            }

            decltype(auto) env_var_list = env_arg.getValue();

            auto& environment = Utilities::Environment::CreateOrGetInstance(__FILE__, __LINE__);

            for (const auto& pair : env_var_list)
                environment.SetEnvironmentVariable(pair.GetValue(), __FILE__, __LINE__);

            return ::MoReFEM::FilesystemNS::File{ std::filesystem::path(input_data_file_arg.getValue()) };
        }
        catch (TCLAP::ArgException& e) // catch any exceptions
        {
            std::ostringstream oconv;
            oconv << "Caught command line exception '" << e.error() << "' for argument " << e.argId() << std::endl;
            throw Exception(oconv.str(), __FILE__, __LINE__);
        }
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_HELPER_HXX_
