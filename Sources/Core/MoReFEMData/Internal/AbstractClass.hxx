/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"

#include "Core/MoReFEMData/Exceptions/Exception.hpp"


namespace MoReFEM::Internal::MoReFEMDataNS
{


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::AbstractClass(int argc, char** argv)
    // clang-format on
    {
        Internal::PetscNS::RAII::CreateOrGetInstance(__FILE__, __LINE__, argc, argv);

#ifdef MOREFEM_WITH_SLEPC
        Internal::SlepcNS::RAII::CreateOrGetInstance(__FILE__, __LINE__);
#endif // MOREFEM_WITH_SLEPC
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::AbstractClass()
    // clang-format on
    {
        static_assert(ProgramTypeT == program_type::test, "This constructor is intended only for tests.");

        Construct();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::Construct()
    // clang-format on
    {
#ifndef NDEBUG
        {
            // Code to detect in debug mode whether this singleton has properly been defined.
            // (would have triggered error handling even without these lines but the reason would have been less clear
            // in the error message).
            decltype(auto) raii = Internal::PetscNS::RAII::GetInstance(__FILE__, __LINE__);
            static_cast<void>(raii);
        }
#endif // NDEBUG

        try
        {
            decltype(auto) mpi = GetMpi();

            Internal::MoReFEMDataNS::DefineEnvironmentVariable(mpi);

            mpi.Barrier();
        }
        catch (const ExceptionNS::GracefulExit& e)
        {
            std::cout << e.what() << std::endl;
            throw;
        }
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::~AbstractClass()
    // clang-format on
    {

        // Flush all the standard outputs. Catch all exceptions: exceptions in destructor are naughty!
        const auto& mpi = GetMpi();

        try
        {
            ::MoReFEM::Wrappers::Petsc::SynchronizedFlush(mpi, stdout, __FILE__, __LINE__);
            ::MoReFEM::Wrappers::Petsc::SynchronizedFlush(mpi, stderr, __FILE__, __LINE__);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Untimely exception caught in ~AbstractClass(): " << e.what() << std::endl;
            assert(false && "No exception in destructors!");
        }
        catch (...)
        {
            assert(false && "No exception in destructors!");
        }

        mpi.Barrier(); // to better handle the possible MpiAbort() on one of the branch...
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    inline const ::MoReFEM::Wrappers::Mpi&
    AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::GetMpi() const noexcept
    // clang-format on
    {
        decltype(auto) raii = Internal::PetscNS::RAII::GetInstance(__FILE__, __LINE__);
        return raii.GetMpi();
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    const ::MoReFEM::FilesystemNS::Directory&
    AbstractClass
    <
        ProgramTypeT,
        DoTrackUnusedFieldsT
    >::GetResultDirectory() const noexcept
    // clang-format on
    {
        static_assert(ProgramTypeT != program_type::update_lua_file,
                      "The result directory is not used in this executable and thus its status is unknown, so it's "
                      "bbest to forbid it to be queried at all.");

        assert(
            !(!result_directory_)
            && "If this occur in a test, ensure MoReFEMData::SetResultDirectory() has been "
               "correctly called! If in a model, check InputDataNS::Result::OutputDirectory is correcly defined in the "
               "input data tuple");
        return *result_directory_;
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    void AbstractClass<ProgramTypeT, DoTrackUnusedFieldsT>
    ::SetResultDirectory(std::filesystem::path path, Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory)
    // clang-format on
    {
        static_assert(ProgramTypeT != program_type::update_lua_file);

        result_directory_ = std::make_unique<const ::MoReFEM::FilesystemNS::Directory>(
            GetMpi(), path, DetermineDirectoryBehaviour(do_overwrite_directory));
        result_directory_->ActOnFilesystem(__FILE__, __LINE__);

        InitTimeKeepLog(GetResultDirectory());
    }


    // clang-format off
    template
    <
        program_type ProgramTypeT,
        ::MoReFEM::InputDataNS::DoTrackUnusedFields DoTrackUnusedFieldsT
    >
    ::MoReFEM::FilesystemNS::behaviour  AbstractClass<ProgramTypeT, DoTrackUnusedFieldsT>
    ::DetermineDirectoryBehaviour(Internal::MoReFEMDataNS::overwrite_directory do_overwrite_directory) const noexcept
    // clang-format on
    {
        ::MoReFEM::FilesystemNS::behaviour ret{ ::MoReFEM::FilesystemNS::behaviour::ignore };

        if constexpr (ProgramTypeT == program_type::model)
            ret = do_overwrite_directory == Internal::MoReFEMDataNS::overwrite_directory::yes
                      ? ::MoReFEM::FilesystemNS::behaviour::overwrite
                      : ::MoReFEM::FilesystemNS::behaviour::ask;
        else if constexpr (ProgramTypeT == program_type::test)
            ret = ::MoReFEM::FilesystemNS::behaviour::overwrite;
        else if constexpr (ProgramTypeT == program_type::post_processing)
            ret = ::MoReFEM::FilesystemNS::behaviour::read;

        return ret;
    }


} // namespace MoReFEM::Internal::MoReFEMDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_INTERNAL_x_ABSTRACT_CLASS_HXX_
