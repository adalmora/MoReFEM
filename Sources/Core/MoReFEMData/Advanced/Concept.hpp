/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_CONCEPT_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_CONCEPT_HPP_

#include <optional>

#include "Utilities/Miscellaneous.hpp"

#include "Core/MoReFEMData/Enum.hpp"


namespace MoReFEM::Advanced::Concept
{


    /*!
     * \brief Defines a concept to identify a type is a section of an \a MoReFEMData object.
     *
     */
    template<typename T>
    concept MoReFEMDataType = requires(T object) {
        
        (T::ConceptIsMoReFEMData == is_morefem_data::yes) || (T::ConceptIsMoReFEMData == is_morefem_data::for_test);
        object.GetInputData();
        object.GetModelSettings();
    };


} // namespace MoReFEM::Advanced::Concept


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_CONCEPT_HPP_
