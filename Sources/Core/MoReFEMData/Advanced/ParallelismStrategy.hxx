//! \file
//
//
//  ParallelismStrategy.hpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HXX_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HXX_

// IWYU pragma: private, include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    parallelism_strategy ExtractParallelismStrategy(const MoReFEMDataT& morefem_data)
    {
        if constexpr (!MoReFEMDataT::HasParallelismField())
        {
            assert(morefem_data.GetParallelismPtr() == nullptr);
            return parallelism_strategy::none;
        } else
        {
            const auto parallelism_ptr = morefem_data.GetParallelismPtr();
            assert(!(!parallelism_ptr));
            return parallelism_ptr->GetParallelismStrategy();
        }
    }


} // namespace MoReFEM::Advanced

#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HXX_
