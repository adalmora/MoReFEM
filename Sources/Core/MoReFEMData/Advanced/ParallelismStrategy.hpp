//! \file
//
//
//  ParallelismStrategy.hpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    //! Enum class which specifies the possible behaviour related to parallelism.
    enum class parallelism_strategy { none, precompute, parallel_no_write, parallel, run_from_preprocessed };


    /*!
     * \brief Extract from \a morefem_data the parallelism strategy in use.
     *
     * \copydoc doxygen_hide_morefem_data_arg
     *
     * \return The \a parallelism_strategy is there is a Parallelism block in the file, or parallelism_strategy::none otherwise.
     */
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    parallelism_strategy ExtractParallelismStrategy(const MoReFEMDataT& morefem_data);


} // namespace MoReFEM::Advanced


#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hxx"

#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_ADVANCED_x_PARALLELISM_STRATEGY_HPP_
