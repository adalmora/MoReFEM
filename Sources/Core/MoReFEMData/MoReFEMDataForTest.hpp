/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HPP_
#define MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HPP_

#include <filesystem>
#include <memory>

#include "Utilities/AsciiOrBinary/AsciiOrBinary.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/InputData.hpp" // IWYU pragma: export
#include "Utilities/InputData/Internal/EmptyInputData.hpp"
#include "Utilities/InputData/ModelSettings.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"     // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Print.hpp" // IWYU pragma: export

#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Internal/AbstractClass.hpp" // IWYU pragma: export
#include "Core/MoReFEMData/Internal/Helper.hpp"        // IWYU pragma: export
#include "Core/MoReFEMData/Internal/Parallelism.hpp"   // IWYU pragma: export


namespace MoReFEM
{


    /*!
     * \brief A specific version of \a MoReFEMData which is to be used in most of the tests written after July 2023.
     *
     * Classical \a MoReFEMData holds `InputData` information, but for tests we typically do not need it: there
     * are little reasons to provide data that may be modified by the end user through a Lua file, and it is more
     * sensible to set the data through a `ModelSettings` object instead.
     *
     * The purpose of current class is to provide an alternate for \a MoReFEMData just for those tests; it should not
     * be used outside of this use case for genuine models.
     *
     */
    template<class ModelSettingsT>
    class MoReFEMDataForTest : public Internal::MoReFEMDataNS::AbstractClass<program_type::test>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MoReFEMDataForTest;

        //! Alias to parent.
        using parent = Internal::MoReFEMDataNS::AbstractClass<program_type::test>;

        //! Alias to unique_ptr.
        using const_unique_ptr = std::unique_ptr<self>;

        //! Alias to input data type, which is here a dedicated class that holds no value.
        using input_data_type = Internal::InputDataNS::EmptyInputData;
        
        //! Alias to model settings type.
        using model_settings_type = ModelSettingsT;
        
        //! Helper variable to define the \a MoReFEMDataType concept.
        static inline constexpr is_morefem_data ConceptIsMoReFEMData = is_morefem_data::for_test;
        

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] result_directory_subpath The path inside the generic $MOREFEM_TEST_OUTPUT_DIR
         * output directory to place the outputs specifically related to the test considered.
         */
        explicit MoReFEMDataForTest(const std::filesystem::path& result_directory_subpath);

        //! Destructor.
        virtual ~MoReFEMDataForTest() override;

        //! \copydoc doxygen_hide_copy_constructor
        MoReFEMDataForTest(const MoReFEMDataForTest& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MoReFEMDataForTest(MoReFEMDataForTest&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MoReFEMDataForTest& operator=(const MoReFEMDataForTest& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MoReFEMDataForTest& operator=(MoReFEMDataForTest&& rhs) = delete;

        ///@}

      public:
        //! Accessor to the \a EmptyData object.
        const input_data_type& GetInputData() const noexcept;
        
        //! Accessor to the \a ModelSettings object.
        const model_settings_type& GetModelSettings() const noexcept;

        //! No \a Parallelism object foreseen so far so this method returns `nullptr`.
        const Internal::Parallelism* GetParallelismPtr() const noexcept;

        //! Specify there are no `Parallelism` field.
        static constexpr bool HasParallelismField()
        {
            return false;
        }


      private:
        //! Instantiation of default \a EmptyData object.
        input_data_type input_data_{};
        
        //! Model Settings object.
        model_settings_type model_settings_;
    };


} // namespace MoReFEM


/// @} // addtogroup CoreGroup

#include "Core/MoReFEMData/MoReFEMDataForTest.hxx"


#endif // MOREFEM_x_CORE_x_MO_RE_F_E_M_DATA_x_MO_RE_F_E_M_DATA_FOR_TEST_HPP_
