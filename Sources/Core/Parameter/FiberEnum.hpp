//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_
#define MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_

#include <string>


namespace MoReFEM::FiberNS
{


    //! Type of the input data read in the fiber file to instantiate the fiber parameter.
    enum class AtNodeOrAtQuadPt { at_node, at_quad_pt, irrelevant };


    /*!
     * \brief Returns a string that tells which value is chosen, for logging purpose.
     *
     * \param[in] value The enum value which we want printed as a string.
     *
     * \return The string representing the \a value.
     */
    std::string AsString(AtNodeOrAtQuadPt value);


} // namespace MoReFEM::FiberNS

#endif // MOREFEM_x_CORE_x_PARAMETER_x_FIBER_ENUM_HPP_
