//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles
// Copyright © 2023 Inria. All rights reserved.
//

#include <cassert>
#include <cstdlib>
#include <string>

#include "Core/Parameter/FiberEnum.hpp"


namespace MoReFEM::FiberNS
{


    std::string AsString(AtNodeOrAtQuadPt value)
    {
        switch (value)
        {
        case AtNodeOrAtQuadPt::at_node:
            return "at_node";
        case AtNodeOrAtQuadPt::at_quad_pt:
            return "at_quad_pt";
        case AtNodeOrAtQuadPt::irrelevant:
            return "irrelevant";
        }

        assert(false && "All values should be considered in the switch.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::FiberNS
