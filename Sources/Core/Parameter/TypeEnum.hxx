//! \file
//
//
//  TypeEnum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/01/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_
#define MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_

// IWYU pragma: private, include "Core/Parameter/TypeEnum.hpp"
#include <cassert>


namespace MoReFEM::ParameterNS
{


    template<Type TypeT>
    std::string Name()
    {
        if constexpr (TypeT == Type::scalar)
            return "scalar";
        else if constexpr (TypeT == Type::vector)
            return "vector";
        else if constexpr (TypeT == Type::matrix)
            return "matrix";
    }


} // namespace MoReFEM::ParameterNS


#endif // MOREFEM_x_CORE_x_PARAMETER_x_TYPE_ENUM_HXX_
