/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Jan 2015 14:19:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_
#define MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM::FilesystemNS { class Directory; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Init the time keeper and write its logs in the output directory specified in \a input_data.
     *
     * This function is to be called early in your main if you want to time keep some information lines.
     *
     * \param[in] result_directory Directory into which all outputs should be written.
     */
    void InitTimeKeepLog(const FilesystemNS::Directory& result_directory);


    ///@} // \addtogroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INIT_TIME_KEEP_LOG_HPP_
