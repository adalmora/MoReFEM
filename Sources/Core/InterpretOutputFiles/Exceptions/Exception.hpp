/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Dec 2014 16:05:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_EXCEPTIONS_x_EXCEPTION_HPP_
#define MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_EXCEPTIONS_x_EXCEPTION_HPP_

#include <cstddef>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <string_view>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS
{


    //! Exception when file is ill-formatted.
    class InvalidFormatInFile final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] file File for which the problem occurred.
         * \param[in] description The issue might be explained more explicitly there as a string.
         * \copydoc doxygen_hide_invoking_file_and_line
         */
        explicit InvalidFormatInFile(const FilesystemNS::File& file,
                                     const std::string& description,
                                     const char* invoking_file,
                                     int invoking_line);

        //! Destructor.
        virtual ~InvalidFormatInFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidFormatInFile(const InvalidFormatInFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidFormatInFile(InvalidFormatInFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidFormatInFile& operator=(const InvalidFormatInFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidFormatInFile& operator=(InvalidFormatInFile&& rhs) = default;

        ///@}
    };


    //! Exception when format line is invalid.
    class InvalidFormatInLine final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] line Line in which the problem occurred.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit InvalidFormatInLine(const std::string& line, const char* invoking_file, int invoking_line);

        /*!
         * \brief Constructor with a more detailed explanation.
         *
         * \param[in] line Line in which the problem occurred.
         * \param[in] description String used to describe more deeply what went wrong.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit InvalidFormatInLine(const std::string& line,
                                     const std::string& description,
                                     const char* invoking_file,
                                     int invoking_line);

        //! Destructor.
        virtual ~InvalidFormatInLine() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidFormatInLine(const InvalidFormatInLine& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InvalidFormatInLine(InvalidFormatInLine&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidFormatInLine& operator=(const InvalidFormatInLine& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InvalidFormatInLine& operator=(InvalidFormatInLine&& rhs) = default;

        ///@}
    };


    //! Exception when a data with a specific index was not found.
    class IndexNotFound final : public ::MoReFEM::Exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor
         *
         * \param[in] sought_index Index which data was sought in the file.
         * \param[in] file File in which the sought index was not found.
         * \param[in] file_description Description of the type of file considered, to enrich the error message.
         * For instance 'TimeIteration'.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit IndexNotFound(const std::size_t sought_index,
                               const FilesystemNS::File& file,
                               std::string_view file_description,
                               const char* invoking_file,
                               int invoking_line);


        //! Destructor.
        virtual ~IndexNotFound() override;

        //! \copydoc doxygen_hide_copy_constructor
        IndexNotFound(const IndexNotFound& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        IndexNotFound(IndexNotFound&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        IndexNotFound& operator=(const IndexNotFound& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        IndexNotFound& operator=(IndexNotFound&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_EXCEPTIONS_x_EXCEPTION_HPP_
