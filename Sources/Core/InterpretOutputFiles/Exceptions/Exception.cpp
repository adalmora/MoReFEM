/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 19 Dec 2014 16:05:16 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <sstream>

#include "Utilities/Filesystem/File.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string InvalidFormatInFileMsg(const ::MoReFEM::FilesystemNS::File& file, const std::string& description);


    std::string InvalidFormatInLineMsg(const std::string& line);


    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description);


    std::string IndexNotFoundMsg(const std::size_t sought_index,
                                 const MoReFEM::FilesystemNS::File& file,
                                 std::string_view file_description);


} // namespace


namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS
{


    InvalidFormatInFile::~InvalidFormatInFile() = default;


    InvalidFormatInFile::InvalidFormatInFile(const FilesystemNS::File& file,
                                             const std::string& description,
                                             const char* invoking_file,
                                             int invoking_line)
    : ::MoReFEM::Exception(InvalidFormatInFileMsg(file, description), invoking_file, invoking_line)
    { }


    InvalidFormatInLine::~InvalidFormatInLine() = default;


    InvalidFormatInLine::InvalidFormatInLine(const std::string& line, const char* invoking_file, int invoking_line)
    : ::MoReFEM::Exception(InvalidFormatInLineMsg(line), invoking_file, invoking_line)
    { }


    InvalidFormatInLine::InvalidFormatInLine(const std::string& line,
                                             const std::string& description,
                                             const char* invoking_file,
                                             int invoking_line)
    : ::MoReFEM::Exception(InvalidFormatInLineMsg(line, description), invoking_file, invoking_line)
    { }


    IndexNotFound::~IndexNotFound() = default;

    IndexNotFound::IndexNotFound(const std::size_t sought_index,
                                 const FilesystemNS::File& file,
                                 std::string_view file_description,
                                 const char* invoking_file,
                                 int invoking_line)
    : ::MoReFEM::Exception(IndexNotFoundMsg(sought_index, file, file_description), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::ExceptionNS::InterpretOutputFilesNS


namespace // anonymous
{


    std::string InvalidFormatInFileMsg(const ::MoReFEM::FilesystemNS::File& file, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << "Invalid file (" << file << "): " << description << '.';

        return oconv.str();
    }


    std::string InvalidFormatInLineMsg(const std::string& line)
    {
        std::ostringstream oconv;
        oconv << "Invalid format in " << line;

        return oconv.str();
    }


    std::string InvalidFormatInLineMsg(const std::string& line, const std::string& description)
    {
        std::ostringstream oconv;
        oconv << InvalidFormatInLineMsg(line) << ": " << description;

        return oconv.str();
    }


    std::string IndexNotFoundMsg(const std::size_t sought_index,
                                 const MoReFEM::FilesystemNS::File& file,
                                 std::string_view file_description)
    {
        std::ostringstream oconv;
        oconv << "Index " << sought_index << " was sought in " << file_description << " file " << file
              << " but it was not found." << std::endl;

        return oconv.str();
    }


} // namespace


/// @} // addtogroup CoreGroup
