/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#include <algorithm>
#include <cassert>
#include <fstream>
#include <string>

#include "Utilities/Filesystem/File.hpp" // IWYU pragma: keep
#include "Utilities/String/String.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"


namespace MoReFEM::InterpretOutputFilesNS
{


    TimeIterationFile::TimeIterationFile(const FilesystemNS::File& input_file) : input_file_(input_file)
    {
        std::ifstream stream{ input_file.Read(__FILE__, __LINE__) };

        std::string line;

        while (getline(stream, line))
        {
            if (Utilities::String::StartsWith(line, "#"))
                continue;

            time_iteration_list_.emplace_back(std::make_unique<Data::TimeIteration>(line));
        }
    }


    const Data::TimeIteration&
    TimeIterationFile::GetTimeIteration(std::size_t index, const char* invoking_file, int invoking_line) const
    {
        decltype(auto) time_iteration_list = GetTimeIterationList();

        const auto end = time_iteration_list.cend();

        auto it = std::find_if(time_iteration_list.begin(),
                               end,
                               [index](const auto& time_iteration_ptr)
                               {
                                   assert(!(!time_iteration_ptr));
                                   return time_iteration_ptr->GetIteration() == index;
                               });

        if (it == end)
            throw ExceptionNS::InterpretOutputFilesNS::IndexNotFound(
                index, GetInputFile(), "TimeIteration", invoking_file, invoking_line);


        return *(*it);
    }


} // namespace MoReFEM::InterpretOutputFilesNS


/// @} // addtogroup CoreGroup
