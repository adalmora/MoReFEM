/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:50:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HXX_
#define MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HXX_


// IWYU pragma: private, include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Core/NumberingSubset/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterpretOutputFilesNS::Data
{


    inline std::size_t TimeIteration::GetIteration() const noexcept
    {
        return time_iteration_;
    }


    inline double TimeIteration::GetTime() const noexcept
    {
        return time_;
    }


    inline const FilesystemNS::File& TimeIteration::GetSolutionFilename() const noexcept
    {
        return solution_filename_;
    }


    inline NumberingSubsetNS::unique_id TimeIteration::GetNumberingSubsetId() const noexcept
    {
        return numbering_subset_id_;
    }


} // namespace MoReFEM::InterpretOutputFilesNS::Data


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HXX_
