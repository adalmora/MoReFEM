/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 23 Dec 2014 11:50:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HPP_
#define MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HPP_

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string> // IWYU pragma: keep
#include <vector>

#include "Utilities/Filesystem/File.hpp"

#include "Core/NumberingSubset/UniqueId.hpp"

// IWYU pragma: no_include <iosfwd>


namespace MoReFEM::InterpretOutputFilesNS::Data
{


    /*!
     * \brief Class which holds the information obtained from one line of time_iteration.hhdata output file.
     *
     * There is one such file per mesh.
     *
     * Example:
     * \verbatim
     # Time iteration; time; numbering subset id; filename
     1;0.001;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
     /Mesh_1/NumberingSubset_10/fluid_velocity_time_00001.hhdata
     2;0.002;10;/Volumes/Data/sebastien/MoReFEM/Results/Poromechanics/Rank_*
     /Mesh_1/NumberingSubset_10/fluid_velocity_time_00002.hhdata
     ...
     \endverbatim
     *
     * (no space after Rank_* - but I had to add it to avoid compiler mistaking it for an end of a C comment).
     */
    class TimeIteration final
    {

      public:
        //! Alias for unique ptr.
        using const_unique_ptr = std::unique_ptr<const TimeIteration>;

        //! Alias for a vector of unique_ptr.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] line Line as read in time_iteration.hhdata output file. Format is:
         * *Time iteration*;*time*; *numbering_subset*, *filename*
         *
         * For instance:
         * '2;0.2;0;/Volumes/Data/sebastien/MoReFEM/Results/Hyperelasticity/Rank_\\* /solution_00002_proc0.hhdata'.
         */
        explicit TimeIteration(const std::string& line);

        //! Destructor.
        ~TimeIteration() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeIteration(const TimeIteration& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeIteration(TimeIteration&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TimeIteration& operator=(const TimeIteration& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TimeIteration& operator=(TimeIteration&& rhs) = delete;

        ///@}

      public:
        //! Time iteration.
        std::size_t GetIteration() const noexcept;

        //! Time (in seconds).
        double GetTime() const noexcept;

        //! Get the filename of the solution at the given time iteration, with a wildcard for the rank.
        const FilesystemNS::File& GetSolutionFilename() const noexcept;

        /*!
         * \brief Get the filename of the solution at the given time iteration, and replace the wildcard by the actual rank.
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \return \a File object with the name matching the mpi rank.
         */
        FilesystemNS::File GetSolutionFilename(const Wrappers::Mpi& mpi) const noexcept;

        //! Get the numbering subset id.
        NumberingSubsetNS::unique_id GetNumberingSubsetId() const noexcept;

        //! Print the content of the line (same format as the constructor argument).
        //! \copydoc doxygen_hide_stream_inout
        void Print(std::ostream& stream) const;

      private:
        //! Time iteration.
        std::size_t time_iteration_;

        //! Time (in seconds).
        double time_;

        //! Filename of the solution at this iteration.
        FilesystemNS::File solution_filename_;

        //! Numbering subset id.
        NumberingSubsetNS::unique_id numbering_subset_id_;
    };


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * Write the content of the line.
     */
    std::ostream& operator<<(std::ostream& stream, const TimeIteration& rhs);


} // namespace MoReFEM::InterpretOutputFilesNS::Data


/// @} // addtogroup CoreGroup


#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_HPP_
