/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 26 Dec 2016 23:27:31 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#ifndef MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_FILE_HXX_
#define MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_FILE_HXX_


// IWYU pragma: private, include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include <cstddef> // IWYU pragma: keep

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterpretOutputFilesNS
{

    inline const Data::TimeIteration::vector_const_unique_ptr& TimeIterationFile ::GetTimeIterationList() const
    {
        return time_iteration_list_;
    }


    inline std::size_t TimeIterationFile::Nstep() const
    {
        return time_iteration_list_.size();
    }


    inline const FilesystemNS::File& TimeIterationFile::GetInputFile() const noexcept
    {
        return input_file_;
    }


} // namespace MoReFEM::InterpretOutputFilesNS


#endif // MOREFEM_x_CORE_x_INTERPRET_OUTPUT_FILES_x_TIME_ITERATION_x_TIME_ITERATION_FILE_HXX_
