/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 14 Nov 2013 13:37:55 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_

// IWYU pragma: private, include "Core/TimeManager/TimeManager.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <optional>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp" 
#include "Core/InputData/Instances/TimeManager/Restart.hpp"
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class StaticOrDynamic; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    TimeManager::TimeManager(const MoReFEMDataT& morefem_data)
    {
        using TimeManager = InputDataNS::TimeManager;

        time_iteration_file_ = morefem_data.GetResultDirectory().AddFile("time_iteration.hhdata");

        decltype(auto) mpi = morefem_data.GetMpi();

        if (time_iteration_file_.DoExist())
        {
            std::cerr << "[WARNING] A file named " << time_iteration_file_
                      << " already existed; it has been removed "
                         "and recreated from scratch. Please consider using ${MOREFEM_START_TIME} in your result "
                         "directory path "
                         "to guarantee each run gets its outputs written in an empty directory."
                      << std::endl;

            time_iteration_file_.Remove(__FILE__, __LINE__);
        }

        std::ofstream stream{ time_iteration_file_.NewContent(__FILE__, __LINE__) };

        stream << "# Time iteration; time; numbering subset id; filename" << std::endl;

        using input_data_type = typename MoReFEMDataT::input_data_type;

        // Field `TimeManager::TimeInit` may not be present for static models.
        if constexpr (input_data_type::template Find<TimeManager::TimeInit>())
            time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeInit>::Value(morefem_data.GetInputData());
        else
            std::cout << "[WARNING] No TimeInit field in InputData tuple; initial time set to 0." << std::endl;

        if constexpr (input_data_type::template Find<InputDataNS::Restart>())
        {
            restart_time_index_ = ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::TimeIndex>::Value(
                morefem_data.GetInputData());

            if (restart_time_index_ != 0ul) // input data file explicitly tells to choose this value if no restart
            {
                decltype(auto) restart_dir_path =
                    ::MoReFEM::InputDataNS::ExtractLeaf<InputDataNS::Restart::DataDirectory>::Path(
                        morefem_data.GetInputData());

                restart_data_directory_.emplace(
                    FilesystemNS::Directory(mpi, restart_dir_path, FilesystemNS::behaviour::read));

                restart_time_iteration_file_ = restart_data_directory_.value().AddFile("time_iteration.hhdata");

                if (!restart_time_iteration_file_.DoExist())
                    throw ExceptionNS::TimeManagerNS::MissingFile(restart_time_iteration_file_, __FILE__, __LINE__);
            }
        }
    }


    inline std::size_t TimeManager::NtimeModified() const
    {
        return Ntime_modified_;
    }


    inline double TimeManager::GetTime() const
    {
        return time_;
    }


    inline void TimeManager::SetStaticOrDynamic(StaticOrDynamic value)
    {
        static_or_dynamic_ = value;
    }


    inline StaticOrDynamic TimeManager::GetStaticOrDynamic() const
    {
        return static_or_dynamic_;
    }


    inline double& TimeManager::GetNonCstTime()
    {
        IncrementNtimeModified();
        return time_;
    }


    inline void TimeManager::IncrementNtimeModified()
    {
        SetStaticOrDynamic(StaticOrDynamic::dynamic_);
        ++Ntime_modified_;
    }


    inline double TimeManager::GetInverseTimeStep() const noexcept
    {
        const double time_step = GetTimeStep();
        assert(time_step > 0.);
        return 1. / time_step;
    }


    inline const FilesystemNS::File& TimeManager::GetTimeIterationFile() const noexcept
    {
        assert(time_iteration_file_.DoExist());
        return time_iteration_file_;
    }


    inline std::size_t TimeManager::GetRestartTimeIndex() const noexcept
    {
        return restart_time_index_;
    }


    inline const FilesystemNS::File& TimeManager::GetRestartTimeIterationFile() const noexcept
    {
        return restart_time_iteration_file_;
    }


    inline bool TimeManager::IsInRestartMode() const noexcept
    {
        return restart_time_index_ != 0ul;
    }


    inline const FilesystemNS::Directory& TimeManager::GetRestartDataDirectory() const noexcept
    {
        assert(restart_data_directory_ != std::nullopt
               && "If this accessor is called it should have been initialized!");
        return restart_data_directory_.value();
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HXX_
