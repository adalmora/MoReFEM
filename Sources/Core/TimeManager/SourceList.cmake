### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_CORE}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/TimeManager.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/TimeManager.doxygen
		${CMAKE_CURRENT_LIST_DIR}/TimeManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/TimeManager.hxx
		${CMAKE_CURRENT_LIST_DIR}/TimeManagerInstance.hpp
		${CMAKE_CURRENT_LIST_DIR}/TimeManagerInstance.hxx
)

include(${CMAKE_CURRENT_LIST_DIR}/Exceptions/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
