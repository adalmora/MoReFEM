/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_

// IWYU pragma: private, include "Core/TimeManager/Policy/ConstantTimeStep.hpp"

#include <cassert>
#include <cstddef>
#include <limits>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    template<::MoReFEM::Concept::InputDataType InputDataT>
    ConstantTimeStep::ConstantTimeStep(const InputDataT& input_data)
    {
        using TimeManager = InputDataNS::TimeManager;

        time_step_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeStep>::Value(input_data);
        maximum_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeMax>::Value(input_data);
        initial_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeInit>::Value(input_data);

        assert(time_step_ > 1.e-9
               && "Should have been already checked when reading the input data "
                  "hence the assert rather than an exception.");
    }


    inline void ConstantTimeStep::IncrementTime(double& time)
    {
        auto& time_step_index = TimeStepIndex();
        ++time_step_index;

        time = ComputeTime(time_step_index);
    }


    inline void ConstantTimeStep::DecrementTime(double& time)
    {
        auto& time_step_index = TimeStepIndex();
        --time_step_index;

        time = ComputeTime(time_step_index);
    }


    inline double ConstantTimeStep::GetTimeStep() const noexcept
    {
        assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
        return time_step_;
    }


    inline double ConstantTimeStep::GetMaximumTime() const noexcept
    {
        assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
        return maximum_time_;
    }


    inline bool ConstantTimeStep::HasFinished(const double time) const
    {
        return time + NumericNS::DefaultEpsilon<double>() >= GetMaximumTime();
    }


    inline constexpr bool ConstantTimeStep::IsTimeStepConstant() noexcept
    {
        return true;
    }


    inline std::size_t& ConstantTimeStep::TimeStepIndex() noexcept
    {
        return time_step_index_;
    }


    inline double ConstantTimeStep::GetInitialTime() const noexcept
    {
        return initial_time_;
    }


    inline void ConstantTimeStep::ResetTimeManagerAtInitialTime(double& time)
    {
        time = initial_time_;
        time_step_index_ = 0;
    }


    inline double ConstantTimeStep::ComputeTime(std::size_t current_time_step_index) const noexcept
    {
        return GetInitialTime() + static_cast<double>(current_time_step_index) * GetTimeStep();
    }


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HXX_
