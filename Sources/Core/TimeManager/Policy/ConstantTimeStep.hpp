/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_

#include <cstddef>
#include <limits>
#include <string>

#include "Utilities/InputData/Concept.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class policy_to_adapt_time_step; }
namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::TestNS::TimeManagerNS { template <class EvolutionPolicyT> class Viewer; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    /*!
     * \brief TimeManager policy when time step is constant.
     *
     * So in input file time step and maximum time are enough to describe the whole time evolution.
     */
    class ConstantTimeStep
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ConstantTimeStep;

        //! Convenient alias.
        static inline std::string ClassName = "ConstantTimeStep";

        // \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship used only for tests.
        friend MoReFEM::TestNS::TimeManagerNS::Viewer<ConstantTimeStep>;
        // \endcond IGNORE_BLOCK_IN_DOXYGEN


      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        template<::MoReFEM::Concept::InputDataType InputDataT>
        explicit ConstantTimeStep(const InputDataT& input_data);

        //! Destructor.
        ~ConstantTimeStep() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ConstantTimeStep(const ConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ConstantTimeStep(ConstantTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ConstantTimeStep& operator=(const ConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ConstantTimeStep& operator=(ConstantTimeStep&& rhs) = delete;

        ///@}

        /*!
         * \brief Increment the time.
         *
         * \param[in,out] time Time is incremented with current time step. Tn+1 = Tn + dt.
         * Current time step is stored in the class.
         *
         */
        void IncrementTime(double& time);

        /*!
         * \brief Decrement the time.
         *
         * \param[in,out] time Time is decremented with current time step. Tn+1 = Tn - dt.
         * Current time step is stored in the class.
         *
         */
        void DecrementTime(double& time);

        //! Get the size of a time step.
        double GetTimeStep() const noexcept;

        //! Returns true if current time is equal or beyond maximum time.
        //! \param[in] time Current time.
        bool HasFinished(double time) const;

        //! Returns whether the time step is constant (obviously true for this class!).
        static constexpr bool IsTimeStepConstant() noexcept;

        //! Get the maximum time (in seconds).
        double GetMaximumTime() const noexcept;

        /*!
         * \copydoc doxygen_hide_time_manager_adapt_time_step
         *
         * \param[in] time Current time.
         *
         * For the current policy, this method throws as the time step is not modifiable.
         */
        [[noreturn]] void AdaptTimeStep(const Wrappers::Mpi& mpi,
                                        policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                        const double time);

        //! Set a new time step.
        //! For the current policy, this method throws as the time step is not modifiable.
        [[noreturn]] void SetTimeStep(double);

        /*!
         * \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
         * \param[in,out] time Current time.
         */
        void ResetTimeManagerAtInitialTime(double& time);

        //!\copydoc doxygen_hide_time_manager_set_restart
        //!
        //! This method is called by \a TimeManagerInstance::SupplSetRestart() method.
        void SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data);


      private:
        //! Get the number of iterations done from the start.
        std::size_t& TimeStepIndex() noexcept;

        //! Get the initial time.
        double GetInitialTime() const noexcept;

        /*!
         * \brief Compute current time depending of time step index.
         *
         * For this policy time (in seconds) is initial_time + \a current_time_step_index * time_step.
         *
         * \param[in] current_time_step_index Current time step index.
         *
         * \return Time (in seconds).
         */
        double ComputeTime(std::size_t current_time_step_index) const noexcept;

      private:
        //! Size of a time step.
        double time_step_ = std::numeric_limits<double>::lowest();

        //! Maximum time.
        double maximum_time_ = std::numeric_limits<double>::lowest();

        /*!
         * \brief Number of iterations that separate the initial time to current time step.
         *
         * \attention It is NOT the same as the \a Ntimes_modified that is present in \a TimeManager class:
         * when \a DecrementTime is called for instance \a time_step_index_ decreases... but \a Ntimes_modified
         * increases).
         */
        std::size_t time_step_index_{ 0ul };

        //! Initial time.
        double initial_time_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/Policy/ConstantTimeStep.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_CONSTANT_TIME_STEP_HPP_
