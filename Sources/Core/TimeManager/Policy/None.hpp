//! \file
//
//
//  None.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 15/02/2022.
// Copyright © 2022 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_NONE_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_NONE_HPP_


#include <type_traits> // IWYU pragma: keep


namespace MoReFEM::TimeManagerNS::Policy
{

    //! When this policy is defined, \a TimeManager is expected not to be present in the input data tuple of the model
    //! considered and some operations are disabled in \a Model class.
    using None = std::false_type;


} // namespace MoReFEM::TimeManagerNS::Policy


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_NONE_HPP_
