/*!
//
// \file
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HPP_

#include <string>

#include "Utilities/InputData/Concept.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class policy_to_adapt_time_step; }
namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::TestNS::TimeManagerNS { template <class EvolutionPolicyT> class Viewer; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{

    /*!
     * \class doxygen_hide_static_time_policy_method_that_throws
     *
     * \brief This method is expected by `TimeManagerInstance` interface, but isn't relevant
     * for current policy - so it throws.
     */


    /*!
     * \brief TimeManager policy for static models.
     *
     */
    class Static
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Static;

        //! Convenient alias.
        static inline std::string ClassName = "Static";

        // \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship used only for tests.
        friend MoReFEM::TestNS::TimeManagerNS::Viewer<Static>;
        // \endcond IGNORE_BLOCK_IN_DOXYGEN


      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor that does strictly nothing!
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        template<::MoReFEM::Concept::InputDataType InputDataT>
        explicit Static(const InputDataT& input_data);

        //! Defaut constructor, to use only in tests!
        Static() = default;

        //! Destructor.
        ~Static() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Static(const Static& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Static(Static&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Static& operator=(const Static& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Static& operator=(Static&& rhs) = delete;

        ///@}

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void IncrementTime(double&);

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void DecrementTime(double&);

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] double GetTimeStep() const;

        //! Returns true.
        bool HasFinished(double) const;

        //! Returns true... (exception not possible due to `constexpr`)
        static constexpr bool IsTimeStepConstant();

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] double GetMaximumTime() const;

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void AdaptTimeStep(const Wrappers::Mpi&, policy_to_adapt_time_step, const double);

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void SetTimeStep(double);

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void ResetTimeManagerAtInitialTime(double&);

        //! \copydoc doxygen_hide_static_time_policy_method_that_throws
        [[noreturn]] void SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration&);
    };


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/Policy/Static.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HPP_
