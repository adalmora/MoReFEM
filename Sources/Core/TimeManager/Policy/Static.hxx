/*!
//
// \file
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HXX_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HXX_

// IWYU pragma: private, include "Core/TimeManager/Policy/Static.hpp"


#include "Utilities/InputData/Concept.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    template<::MoReFEM::Concept::InputDataType InputDataT>
    Static::Static(const InputDataT&)
    { }


    inline constexpr bool Static::IsTimeStepConstant()
    {
        return true;
    }


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_STATIC_HXX_
