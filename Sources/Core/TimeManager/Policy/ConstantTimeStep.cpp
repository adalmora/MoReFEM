/*!
//
// \file
//
//
*/

#include <cmath>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"
#include "Core/TimeManager/Policy/ConstantTimeStep.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class policy_to_adapt_time_step; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    void ConstantTimeStep::SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
    {
        const auto sought_time = restart_time_data.GetTime();

        double iteration_index_as_double = (sought_time - GetInitialTime()) / GetTimeStep();

        time_step_index_ = static_cast<std::size_t>(std::round(iteration_index_as_double));

        if (!NumericNS::AreEqual(ComputeTime(time_step_index_), sought_time))
            throw ExceptionNS::TimeManagerNS::ImproperRestartTimeForConstantTimeStep(
                sought_time, GetInitialTime(), GetTimeStep(), __FILE__, __LINE__);
    }


    [[noreturn]] void ConstantTimeStep::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                                      policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                                      const double time)
    {
        static_cast<void>(mpi);
        static_cast<void>(a_policy_to_adapt_time_step);
        static_cast<void>(time);

        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("AdaptTimeStep()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void ConstantTimeStep::SetTimeStep(double time_step)
    {
        static_cast<void>(time_step);
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("SetTimeStep()", ClassName, __FILE__, __LINE__);
    }


} // namespace MoReFEM::TimeManagerNS::Policy
