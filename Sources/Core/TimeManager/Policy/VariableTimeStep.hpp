/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 5 Jun 2015 17:24:41 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HPP_

#include <limits>
#include <string>

#include "Utilities/InputData/Concept.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class policy_to_adapt_time_step; }
namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    /*!
     * \brief TimeManager policy when time step is variable.
     *
     */
    class VariableTimeStep
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = VariableTimeStep;

        //! Convenient alias.
        static inline std::string ClassName = "VariableTimeStep";

      protected:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_input_data_arg
         */
        template<::MoReFEM::Concept::InputDataType InputDataT>
        explicit VariableTimeStep(const InputDataT& input_data);

        //! Destructor.
        ~VariableTimeStep() = default;

        //! \copydoc doxygen_hide_copy_constructor
        VariableTimeStep(const VariableTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariableTimeStep(VariableTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariableTimeStep& operator=(const VariableTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariableTimeStep& operator=(VariableTimeStep&& rhs) = delete;

        ///@}

        /*!
         * \brief Increment the time.
         *
         * \param[in,out] time Time is incremented with current time step. Tn+1 = Tn + dt.
         * Current time step is stored in the class.
         *
         */
        void IncrementTime(double& time) const;

        /*!
         * \brief Decrement the time.
         *
         * \param[in,out] time Time is decremented with current time step. Tn+1 = Tn - dt.
         * Current time step is stored in the class.
         *
         */
        void DecrementTime(double& time) const;

        //! Get the size of a time step, in seconds.
        double GetTimeStep() const;

        //! Returns true if current time is equal or beyond maximum time.
        //! \param[in] time Current time.
        bool HasFinished(double time) const;

        //! Returns whether the time step is constant (obviously false for this class!).
        static constexpr bool IsTimeStepConstant() noexcept;

        //! Get the maximum time (in seconds).
        double GetMaximumTime() const;

        /*!
         * \copydoc doxygen_hide_time_manager_adapt_time_step
         * \param[in] time Current time.
         */
        void AdaptTimeStep(const Wrappers::Mpi& mpi,
                           policy_to_adapt_time_step a_policy_to_adapt_time_step,
                           const double time);

        //! Constant accessor on maximum time step.
        double GetMaximumTimeStep() const noexcept;

        //! Constant accessor on minimum time step.
        double GetMinimumTimeStep() const noexcept;

        //! Set a new time step.
        //! \param[in] time_step New value for time step.
        void SetTimeStep(double time_step);

        /*!
         * \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
         * \param[in,out] time Current time.
         */
        void ResetTimeManagerAtInitialTime(double& time);

        //!\copydoc doxygen_hide_time_manager_set_restart
        //!
        //! This method is called by \a TimeManagerInstance::SupplSetRestart() method.
        void SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data);

      private:
        //! Non constant accessor to the size of a time step.
        double& GetNonCstTimeStep() noexcept;

      private:
        //! Size of a time step.
        double time_step_ = std::numeric_limits<double>::lowest();

        //! Maximum time.
        double maximum_time_ = std::numeric_limits<double>::lowest();

        //! Minimum time step.
        double minimum_time_step_ = std::numeric_limits<double>::lowest();

        //! Maximum time step ie the time step given by the user at the beginning.
        double maximum_time_step_ = std::numeric_limits<double>::lowest();

        //! Initial time.
        double initial_time_ = std::numeric_limits<double>::lowest();
    };


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/Policy/VariableTimeStep.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HPP_
