/*!
 //
 // \file
 //
 //
 */

#include "Core/TimeManager/Policy/Static.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class policy_to_adapt_time_step; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{


    [[noreturn]] void Static::IncrementTime(double&)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("IncrementTime()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void Static::DecrementTime(double&)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("DecrementTime()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] double Static::GetTimeStep() const
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("GetTimeStep()", ClassName, __FILE__, __LINE__);
    }


    bool Static::HasFinished(double) const
    {
        return true;
    }


    [[noreturn]] double Static::GetMaximumTime() const
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("GetMaximumTime()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void Static::AdaptTimeStep(const Wrappers::Mpi&, policy_to_adapt_time_step, const double)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("AdaptTimeStep()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void Static::SetTimeStep(double)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy("SetTimeStep()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void Static::ResetTimeManagerAtInitialTime(double&)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy(
            "ResetTimeManagerAtInitialTime()", ClassName, __FILE__, __LINE__);
    }


    [[noreturn]] void Static::SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration&)
    {
        throw ExceptionNS::TimeManagerNS::InvalidMethodForPolicy(
            "SetRestartForPolicy()", ClassName, __FILE__, __LINE__);
    }


} // namespace MoReFEM::TimeManagerNS::Policy
