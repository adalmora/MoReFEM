/*!
//
// \file
//
//
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/InputData.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/TimeManager/Exceptions/Exception.hpp"
#include "Core/TimeManager/Policy/VariableTimeStep.hpp" // IWYU pragma: export
#include "Core/TimeManager/TimeManager.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TimeManagerNS::Policy
{

    void VariableTimeStep::AdaptTimeStep(const Wrappers::Mpi& mpi,
                                         policy_to_adapt_time_step a_policy_to_adapt_time_step,
                                         const double time)
    {
        auto& time_step = GetNonCstTimeStep();
        auto minimum_time_step = GetMinimumTimeStep();

        switch (a_policy_to_adapt_time_step)
        {
        case policy_to_adapt_time_step::decreasing:
        {
            assert(time_step + NumericNS::DefaultEpsilon<double>() >= minimum_time_step
                   && "I have not written the code here, but it was clearly an assumption...");

            if (NumericNS::AreEqual(time_step, minimum_time_step))
                throw ExceptionNS::TimeManagerNS::TimeStepAdaptationMinimumReached(
                    minimum_time_step, __FILE__, __LINE__);

            if (time_step > minimum_time_step)
            {
                time_step = std::max(0.5 * time_step, minimum_time_step);

                if (mpi.IsRootProcessor())
                    std::cout << std::endl << "Time step subiteration with time step " << time_step << "." << std::endl;
            }

            break;
        }
        case policy_to_adapt_time_step::increasing:
        {
            const auto maximum_time_step = GetMaximumTimeStep();

            const double modulus = time - maximum_time_step * std::floor(time / maximum_time_step);

            bool condition_max_time_step = ((maximum_time_step - modulus) < minimum_time_step);
            condition_max_time_step |=
                NumericNS::AreEqual(maximum_time_step - modulus, minimum_time_step, 0.5 * minimum_time_step);

            const double new_time_step =
                condition_max_time_step ? maximum_time_step : std::min(maximum_time_step - modulus, maximum_time_step);


            if (new_time_step >= time_step)
            {
                time_step = new_time_step;

                if (mpi.IsRootProcessor())
                    std::cout << std::endl
                              << "Time step re-adaptation with time step " << new_time_step << "." << std::endl;
            } else
            {
                // \todo #1753 proper error handling required here!
                std::cerr << new_time_step << " is lower than previous one " << time_step << std::endl;
                assert(NumericNS::AreEqual(time_step, maximum_time_step)
                       && "Only configuration in which the test should happen!");
            }
            break;
        }
        }
    }


    void VariableTimeStep::SetRestartForPolicy(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data)
    {
        static_cast<void>(restart_time_data);
        // \todo Nothing done yet; see it along with #1763 but I would say it can be left empty.
    }


} // namespace MoReFEM::TimeManagerNS::Policy
