/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 26 Jul 2017 14:46:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_

// IWYU pragma: private, include "Core/TimeManager/Policy/VariableTimeStep.hpp"
#include <cassert>
#include <limits>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/TimeManager/TimeManager.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    template<::MoReFEM::Concept::InputDataType InputDataT>
    VariableTimeStep::VariableTimeStep(const InputDataT& input_data)
    {
        using TimeManager = InputDataNS::TimeManager;

        time_step_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeStep>::Value(input_data);
        maximum_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeMax>::Value(input_data);
        minimum_time_step_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::MinimumTimeStep>::Value(input_data);
        maximum_time_step_ = time_step_;
        initial_time_ = ::MoReFEM::InputDataNS::ExtractLeaf<TimeManager::TimeInit>::Value(input_data);

        assert(time_step_ > 1.e-9);
    }


    inline void VariableTimeStep::IncrementTime(double& time) const
    {
        time += GetTimeStep();
    }


    inline void VariableTimeStep::DecrementTime(double& time) const
    {
        time -= GetTimeStep();
    }


    inline double VariableTimeStep::GetTimeStep() const
    {
        assert(!NumericNS::AreEqual(time_step_, std::numeric_limits<double>::lowest()));
        return time_step_;
    }


    inline double VariableTimeStep::GetMaximumTime() const
    {
        assert(!NumericNS::AreEqual(maximum_time_, std::numeric_limits<double>::lowest()));
        return maximum_time_;
    }


    inline bool VariableTimeStep::HasFinished(const double time) const
    {
        return ((time > GetMaximumTime()) || NumericNS::AreEqual(time, GetMaximumTime(), 0.5 * GetMinimumTimeStep()));
    }


    inline constexpr bool VariableTimeStep::IsTimeStepConstant() noexcept
    {
        return false;
    }


    inline double& VariableTimeStep::GetNonCstTimeStep() noexcept
    {
        return time_step_;
    }


    inline double VariableTimeStep::GetMaximumTimeStep() const noexcept
    {
        return maximum_time_step_;
    }


    inline double VariableTimeStep::GetMinimumTimeStep() const noexcept
    {
        return minimum_time_step_;
    }


    inline void VariableTimeStep::SetTimeStep(double time_step)
    {
        time_step_ = time_step;
    }


    inline void VariableTimeStep::ResetTimeManagerAtInitialTime(double& time)
    {
        time = initial_time_;
    }


} // namespace MoReFEM::TimeManagerNS::Policy


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_POLICY_x_VARIABLE_TIME_STEP_HXX_
