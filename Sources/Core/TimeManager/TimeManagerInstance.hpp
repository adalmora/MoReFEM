/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 8 Jun 2015 12:06:22 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HPP_

#include "Core/MoReFEMData/Advanced/Concept.hpp" 
#include "Core/TimeManager/TimeManager.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::TestNS::TimeManagerNS { template<class EvolutionPolicyT> class Viewer; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Class in charge of ruling time iterations.
     *
     * Not reworked very thoroughly so far; just removed the redundancy between integer and double
     * members to determine how much time has elapsed and when it should stop.
     */
    template<class EvolutionPolicyT>
    class TimeManagerInstance final : public TimeManager, public EvolutionPolicyT
    {

      public:
        //! Convenient alias.
        using self = TimeManagerInstance<EvolutionPolicyT>;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<TimeManagerInstance>;

        // \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship used only for tests.
        friend MoReFEM::TestNS::TimeManagerNS::Viewer<EvolutionPolicyT>;
        // \endcond IGNORE_BLOCK_IN_DOXYGEN

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit TimeManagerInstance(const MoReFEMDataT& morefem_data);

        //! Defaut constructor, to use only in tests!
        TimeManagerInstance() = default;

        //! Destructor.
        ~TimeManagerInstance() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        TimeManagerInstance(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeManagerInstance(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

        //! Increment the time.
        void IncrementTime() override;

        //! Decrement the time.
        void DecrementTime() override;

        //! Returns true if all time iterations have been played.
        virtual bool HasFinished() const override;

        //! Returns the current time step.
        virtual double GetTimeStep() const override;

        //! \copydoc doxygen_hide_time_manager_is_time_step_constant
        virtual bool IsTimeStepConstant() const override;

        //! Get the maximum time (in seconds). Iteration loop will stop as soon as it's over this value.
        virtual double GetMaximumTime() const override;

        //! Set a new time step.
        //! \param[in] time_step New value of time step.
        virtual void SetTimeStep(double time_step) override;

        //! \copydoc doxygen_hide_time_manager_adapt_time_step
        virtual void AdaptTimeStep(const Wrappers::Mpi& mpi,
                                   policy_to_adapt_time_step a_policy_to_adapt_time_step) override;

        //! \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
        virtual void ResetTimeManagerAtInitialTime() override;

        //! \copydoc doxygen_hide_time_manager_set_restart
        virtual void SupplSetRestart(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data) override;

      private:
        //! Maximum time (should stop as soon as this time is reached).
        double maximum_time_ = std::numeric_limits<double>::lowest();
    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/TimeManagerInstance.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_INSTANCE_HPP_
