/*!
 //
 // \file
 //
 // \ingroup CoreGroup
 // \addtogroup CoreGroup
 // \{
 */

#include <sstream>
#include <string>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Core/TimeManager/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string InvalidMethodForPolicyMsg(std::string_view method, std::string_view policy);


    std::string TimeStepAdaptationMinimumReachedMsg(double minimum_time_step);


    std::string ImproperRestartTimeForConstantTimeStepMsg(double sought_time, double initial_time, double time_step);


    std::string MissingFileMsg(const MoReFEM::FilesystemNS::File& filename);


    std::string RestartDirShouldntBeInResultDirMsg(const MoReFEM::FilesystemNS::Directory& restart_directory,
                                                   const MoReFEM::FilesystemNS::Directory& result_directory);


} // namespace


namespace MoReFEM::ExceptionNS::TimeManagerNS
{


    InvalidMethodForPolicy::~InvalidMethodForPolicy() = default;


    InvalidMethodForPolicy::InvalidMethodForPolicy(std::string_view method,
                                                   std::string_view policy,
                                                   const char* invoking_file,
                                                   int invoking_line)
    : MoReFEM::Exception(InvalidMethodForPolicyMsg(method, policy), invoking_file, invoking_line)
    { }


    TimeStepAdaptationMinimumReached::~TimeStepAdaptationMinimumReached() = default;


    TimeStepAdaptationMinimumReached::TimeStepAdaptationMinimumReached(double minimum_time_step,
                                                                       const char* invoking_file,
                                                                       int invoking_line)
    : MoReFEM::Exception(TimeStepAdaptationMinimumReachedMsg(minimum_time_step), invoking_file, invoking_line)
    { }


    ImproperRestartTimeForConstantTimeStep::~ImproperRestartTimeForConstantTimeStep() = default;

    ImproperRestartTimeForConstantTimeStep::ImproperRestartTimeForConstantTimeStep(double sought_time,
                                                                                   double initial_time,
                                                                                   double time_step,
                                                                                   const char* invoking_file,
                                                                                   int invoking_line)
    : MoReFEM::Exception(ImproperRestartTimeForConstantTimeStepMsg(sought_time, initial_time, time_step),
                         invoking_file,
                         invoking_line)
    { }


    MissingFile::~MissingFile() = default;

    MissingFile::MissingFile(const FilesystemNS::File& filename, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(MissingFileMsg(filename), invoking_file, invoking_line)
    { }


    RestartDirShouldntBeInResultDir::~RestartDirShouldntBeInResultDir() = default;

    RestartDirShouldntBeInResultDir::RestartDirShouldntBeInResultDir(const FilesystemNS::Directory& restart_directory,
                                                                     const FilesystemNS::Directory& result_directory,
                                                                     const char* invoking_file,
                                                                     int invoking_line)
    : MoReFEM::Exception(RestartDirShouldntBeInResultDirMsg(restart_directory, result_directory),
                         invoking_file,
                         invoking_line)
    { }

} // namespace MoReFEM::ExceptionNS::TimeManagerNS


namespace // anonymous
{


    std::string InvalidMethodForPolicyMsg(std::string_view method, std::string_view policy)
    {
        std::ostringstream oconv;
        oconv << "TimeManager::" << method << " method was called for policy " << policy
              << " which doesn't support it.";
        return oconv.str();
    }


    std::string TimeStepAdaptationMinimumReachedMsg(double minimum_time_step)
    {
        std::ostringstream oconv;
        oconv << "Your simulation tries to decrease the time step to less than the minimum "
                 "time step allowed ("
              << minimum_time_step << " s).";
        return oconv.str();
    }


    std::string ImproperRestartTimeForConstantTimeStepMsg(double sought_time, double initial_time, double time_step)
    {
        std::ostringstream oconv;
        oconv << "Restart time read from restart data is " << sought_time
              << ", but it does not "
                 "seem to be a proper value (allowed values for constant time step policy must respect "
              << initial_time << " + n * " << time_step
              << ", where n is an integer). It is likely you are trying to use the wrong restart data.";
        return oconv.str();
    }


    std::string MissingFileMsg(const MoReFEM::FilesystemNS::File& filename)
    {
        std::ostringstream oconv;
        oconv << "Expected file " << filename << " couldn't be found.";
        return oconv.str();
    }


    std::string RestartDirShouldntBeInResultDirMsg(const MoReFEM::FilesystemNS::Directory& restart_directory,
                                                   const MoReFEM::FilesystemNS::Directory& result_directory)
    {
        std::ostringstream oconv;
        oconv << "Restart directory (" << restart_directory << ") can't be enclosed in result directory ("
              << result_directory << "). Please fix your input data file.";
        return oconv.str();
    }


} // namespace


/// @} // addtogroup CoreGroup
