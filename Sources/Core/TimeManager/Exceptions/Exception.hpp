/*!
 */


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_EXCEPTIONS_x_EXCEPTION_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_EXCEPTIONS_x_EXCEPTION_HPP_

#include <string_view>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::TimeManagerNS
{


    //! Called when a method makes no sense for a given time step policy.,
    class InvalidMethodForPolicy : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] policy Policy for which the exception was thrown.
         * \param[in] method Name of the method not supported by the policy.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit InvalidMethodForPolicy(std::string_view method,
                                        std::string_view policy,
                                        const char* invoking_file,
                                        int invoking_line);

        //! Destructor
        virtual ~InvalidMethodForPolicy() override;

        //! \copydoc doxygen_hide_copy_constructor
        InvalidMethodForPolicy(const InvalidMethodForPolicy& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InvalidMethodForPolicy(InvalidMethodForPolicy&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InvalidMethodForPolicy& operator=(const InvalidMethodForPolicy& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InvalidMethodForPolicy& operator=(InvalidMethodForPolicy&& rhs) = delete;
    };


    //! Called when time step adaptation reaches too small a value.
    class TimeStepAdaptationMinimumReached : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] minimum_time_step Minimum time step, in seconds
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit TimeStepAdaptationMinimumReached(double minimum_time_step,
                                                  const char* invoking_file,
                                                  int invoking_line);

        //! Destructor
        virtual ~TimeStepAdaptationMinimumReached() override;

        //! \copydoc doxygen_hide_copy_constructor
        TimeStepAdaptationMinimumReached(const TimeStepAdaptationMinimumReached& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeStepAdaptationMinimumReached(TimeStepAdaptationMinimumReached&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TimeStepAdaptationMinimumReached& operator=(const TimeStepAdaptationMinimumReached& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TimeStepAdaptationMinimumReached& operator=(TimeStepAdaptationMinimumReached&& rhs) = delete;
    };


    //! Called when restart data can't be interpreted correctly.
    class ImproperRestartTimeForConstantTimeStep : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] sought_time Time which was attempted, in seconds. Should be \a initial_time + n * \a time_step...
         * but if the exception is called it is not the case.
         * \param[in] initial_time Initial time for the model with the constant step policy, in seconds
         * \param[in] time_step Time step of  the constant step policy, in seconds
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit ImproperRestartTimeForConstantTimeStep(double sought_time,
                                                        double initial_time,
                                                        double time_step,
                                                        const char* invoking_file,
                                                        int invoking_line);

        //! Destructor
        virtual ~ImproperRestartTimeForConstantTimeStep() override;

        //! \copydoc doxygen_hide_copy_constructor
        ImproperRestartTimeForConstantTimeStep(const ImproperRestartTimeForConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ImproperRestartTimeForConstantTimeStep(ImproperRestartTimeForConstantTimeStep&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ImproperRestartTimeForConstantTimeStep& operator=(const ImproperRestartTimeForConstantTimeStep& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ImproperRestartTimeForConstantTimeStep& operator=(ImproperRestartTimeForConstantTimeStep&& rhs) = delete;
    };


    //! Called when restart data file couldn't be found.
    class MissingFile : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] filename Name of the file that couldn't be found.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit MissingFile(const FilesystemNS::File& filename, const char* invoking_file, int invoking_line);

        //! Destructor
        virtual ~MissingFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        MissingFile(const MissingFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MissingFile(MissingFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MissingFile& operator=(const MissingFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MissingFile& operator=(MissingFile&& rhs) = delete;
    };


    //! Called when restart directory is poorly located.
    class RestartDirShouldntBeInResultDir : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor
         *
         * \param[in] restart_directory The directory which encloses the read only data required to run restart mode.
         * \param[in] result_directory The directory in write mode into which outputs will be written. Rank last subdir has been stripped beforehand.
         * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.
         */
        explicit RestartDirShouldntBeInResultDir(const FilesystemNS::Directory& restart_directory,
                                                 const FilesystemNS::Directory& result_directory,
                                                 const char* invoking_file,
                                                 int invoking_line);

        //! Destructor
        virtual ~RestartDirShouldntBeInResultDir() override;

        //! \copydoc doxygen_hide_copy_constructor
        RestartDirShouldntBeInResultDir(const RestartDirShouldntBeInResultDir& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        RestartDirShouldntBeInResultDir(RestartDirShouldntBeInResultDir&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        RestartDirShouldntBeInResultDir& operator=(const RestartDirShouldntBeInResultDir& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RestartDirShouldntBeInResultDir& operator=(RestartDirShouldntBeInResultDir&& rhs) = delete;
    };


} // namespace MoReFEM::ExceptionNS::TimeManagerNS


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_EXCEPTIONS_x_EXCEPTION_HPP_
