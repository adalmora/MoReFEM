/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <cassert>
#include <filesystem>

#include "Utilities/Filesystem/Directory.hpp"

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include "Core/TimeManager/Exceptions/Exception.hpp"
#include "Core/TimeManager/TimeManager.hpp"


namespace MoReFEM
{


    TimeManager::~TimeManager() = default;


    void TimeManager::SetRestartIfRelevant(const FilesystemNS::Directory& result_directory)
    {
        if (IsInRestartMode())
        {
            // We need to check restart data aren't in the output directory
            {
                auto restart_directory = GetRestartDataDirectory();
                assert(result_directory.IsWithRank());

                FilesystemNS::Directory result_directory_without_rank(
                    result_directory.GetDirectoryEntry().path().parent_path(), FilesystemNS::behaviour::read);

                assert(result_directory_without_rank.DoExist());

                if (FilesystemNS::IsFirstSubfolderOfSecond(restart_directory, result_directory_without_rank))
                    throw ExceptionNS::TimeManagerNS::RestartDirShouldntBeInResultDir(
                        restart_directory, result_directory_without_rank, __FILE__, __LINE__);
            }


            InterpretOutputFilesNS::TimeIterationFile time_iteration_file(GetRestartTimeIterationFile());

            decltype(auto) time_iteration_data =
                time_iteration_file.GetTimeIteration(restart_time_index_, __FILE__, __LINE__);
            assert(time_iteration_data.GetIteration() == restart_time_index_);

            time_ = time_iteration_data.GetTime();
            Ntime_modified_ = restart_time_index_;

            SetStaticOrDynamic(StaticOrDynamic::dynamic_);
            SupplSetRestart(time_iteration_data);
        }
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
