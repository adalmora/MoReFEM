/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 26 Apr 2013 12:11:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HPP_
#define MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <optional>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/InputData.hpp" // IWYU pragma: keep

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export // IWYU pragma: keep

#include "Core/MoReFEMData/Advanced/Concept.hpp" 
#include "Core/InputData/Instances/TimeManager/TimeManager.hpp" // IWYU pragma: keep

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::InterpretOutputFilesNS::Data { class TimeIteration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /// \addtogroup CoreGroup
    ///@{


    /*!
     * \brief Handy enumeration for models that work in two steps: first a static phase during initialization and
     * then a dynamic phase.
     */
    enum class StaticOrDynamic { static_, dynamic_ }; // underscore is there due to static status as C++ keyword.

    /*!
     * \brief Handy enumeration to increase or decrease time step.
     */
    enum class policy_to_adapt_time_step { decreasing, increasing };


    /*!
     * \brief Class in charge of managing the elapsing of time within the simulation.
     *
     * \attention This is an abstract class; in your model you are probably seeking a TimeManagerInstance.
     *
     * \internal <b><tt>[internal]</tt></b> Must not be confused with TimeKeep() which is a chronometer to evaluate
     * efficiency of the code.
     * \endinternal
     */
    class TimeManager
    {
      public:
        //! Convenient alias.
        using self = TimeManager;

        //! Alias to unique_ptr.
        using unique_ptr = std::unique_ptr<TimeManager>;

      public:
        /// \name
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg
         */
        template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
        explicit TimeManager(const MoReFEMDataT& morefem_data);

        //! Defaut constructor, to use only in tests!
        TimeManager() = default;

        //! Destructor.
        virtual ~TimeManager();

        //! \copydoc doxygen_hide_copy_constructor
        TimeManager(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TimeManager(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

        //! How the time is incremented. To be determined by a policy in the private derived class.
        virtual void IncrementTime() = 0;

        //! How the time is decremented. To be determined by a policy in the private derived class.
        virtual void DecrementTime() = 0;

        /*!
         * \brief If there is restart data in the input datafile, set it up properly.
         *
         * \param[in] result_directory Directory into which model results should be written. It is given here just to
         * perform a check: we need to ensure that the directory with restart data isn't enclosed by this one.
         */
        void SetRestartIfRelevant(const FilesystemNS::Directory& result_directory);

        //! Get the current time (in seconds).
        double GetTime() const;

        /*!
         * \brief Number of times the time has been modified.
         *
         * It doesn't necessarily mean an increasement of time: in data assimilation it might happen we go backward.
         * It is just an index that is used to tag the output files; for instance a file which tag 45 is the 46th
         * (possible) generated file of the same nature (possible as we might imagine some of the steps might not be
         * written on disk).
         *
         * \return Index that tags the current time iteration.
         */
        std::size_t NtimeModified() const;

        /*!
         * \brief Set whether the system is static or dynamic.
         *
         * \internal <b><tt>[internal]</tt></b> Only a Model should be able to call this method.
         * \endinternal
         *
         * \param[in] value Value to assign.
         */
        void SetStaticOrDynamic(StaticOrDynamic value);

        //! Returns whether the system is currently static or dynamic.
        StaticOrDynamic GetStaticOrDynamic() const;

        //! Returns the current time step.
        virtual double GetTimeStep() const = 0;

        //! Returns the inverse of the time step.
        double GetInverseTimeStep() const noexcept;

        /*!
         * \class doxygen_hide_time_manager_is_time_step_constant
         *
         * \brief Returns whether the time step is constant.
         *
         * Some models rely explicitly on this hypothesis; this test is there to enable an assert checking it is
         * fulfilled.
         *
         * \return True if the time step doesn't vary during the runtime of the model.
         */

        //! \copydoc doxygen_hide_time_manager_is_time_step_constant
        virtual bool IsTimeStepConstant() const = 0;

        //! Returns true if all time iterations have been played.
        virtual bool HasFinished() const = 0;

        //! Get the maximum time (in seconds).
        virtual double GetMaximumTime() const = 0;

        //! Set a new time step.
        //! \param[in] time_step New value of time step.
        virtual void SetTimeStep(double time_step) = 0;

        /*!
         * \class doxygen_hide_time_manager_adapt_time_step
         *
         * \brief Adapt the time step for the simulation.
         *
         * This method should be called if needed in your Model's FinalizeStep() method (typically when a Newton
         * didn't converge and you want to retry with a smaller step).
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] a_policy_to_adapt_time_step \a policy_to_adapt_time_step parameter, either increasing
         * or decreasing.
         */

        //! \copydoc doxygen_hide_time_manager_adapt_time_step
        virtual void AdaptTimeStep(const Wrappers::Mpi& mpi, policy_to_adapt_time_step a_policy_to_adapt_time_step) = 0;

        /*!
         * \class doxygen_hide_time_manager_reset_time_manager_at_initial_time
         *
         * \brief Reset the TimeManager at the beginning of the simulation.
         *
         * This method should not be called in most cases. This method was needed for data assimilation purposes,
         * the 4DVAR method which is a variational method that needs multiple runs both ways (0 to T and T to 0)
         * to evaluate quantities. Hence a generic MoReFEM user should not call this method in its model.
         *
         */

        //! \copydoc doxygen_hide_time_manager_reset_time_manager_at_initial_time
        virtual void ResetTimeManagerAtInitialTime() = 0;

        //! Path to the time iteration file in which the files written are recorded with their time index.
        const FilesystemNS::File& GetTimeIterationFile() const noexcept;

        /*!
         * \brief Returns the time index used to tag the data that should be loaded at the beginning of the restart run.
         *
         * This index is the one used in time_iteration.hhdata line of the original run from which we want to restart.
         *
         * \return Index at which the model should begin.
         */
        std::size_t GetRestartTimeIndex() const noexcept;

        /*!
         * \brief Returns the file which contains the time iteration from previous run data.
         *
         * \return File which time iteration data.
         */
        const FilesystemNS::File& GetRestartTimeIterationFile() const noexcept;

        /*!
         * \brief Tells whether the model is run in restart mode or not.
         *
         * \return True if model is in restart mode.
         */
        bool IsInRestartMode() const noexcept;

        /*!
         * \brief Return the result directory from a previous run of the model, which we may want to restart from.
         *
         * If not in restart mode, just set it to nullopt.
         *
         * \return Directory which already exists and contains the data from a previous run.
         */
        const FilesystemNS::Directory& GetRestartDataDirectory() const noexcept;


      protected:
        //! Current time (in seconds).
        //! Calling this method also increment \a Ntime_modified_, ensuring there won't be any overwriting in output
        //! data.
        double& GetNonCstTime();

        /*!
         * \brief Increment the number of times the time has been modified.
         *
         * Due to the nature of the data attribute tracked, it is the only allowed operations on it.
         */
        void IncrementNtimeModified();

      private:
        /*!
         * \class doxygen_hide_time_manager_set_restart
         *
         * \brief Specific restart operations related to the time manager policy chosen.
         *
         * \param[in] restart_time_data Restart data.
         */

        //! \copydoc doxygen_hide_time_manager_set_restart
        //!
        //! This method is automatically called at the end of \a SetRestartIfRelevant().
        virtual void SupplSetRestart(const InterpretOutputFilesNS::Data::TimeIteration& restart_time_data) = 0;


      private:
        //! Path to the time iteration file in which the files written are recorded with their time index.
        FilesystemNS::File time_iteration_file_;

        //! Current time (in seconds).
        //! \internal \attention Except in the class constructor, please <strong>never</strong> call directly this attribute
        //! (this is true in all of MoReFEM, but even more so here). If you need the value, please call `GetTime()`; if
        //! you're meddling with `TimeManagerInstance` or current class and need to modify it please call
        //! `GetNonCstTime()`.
        double time_ = 0.;

        //! Whether the system is currently running static or dynamic case.
        StaticOrDynamic static_or_dynamic_ = StaticOrDynamic::static_;

        /*!
         * \brief Number of times the time has been modified.
         *
         * It doesn't necessarily mean an increasement of time: in data assimilation it might happen we go backward.
         * It is just an index that is used to tag the output files; for instance a file which tag 45 is the 46th
         * (possible) generated file of the same nature (possible as we might imagine some of the steps might not be
         * written on disk).
         */
        std::size_t Ntime_modified_{ 0ul };

        //! \brief Time index used to tag the data that should be loaded at the beginning of the restart run.
        //!
        //! This index is the one used in time_iteration.hhdata line of the original run from which we want to restart.
        std::size_t restart_time_index_{ 0ul };

        /*!
         * \brief Result directory from a previous run of the model, which we may want to restart from.
         *
         * If not in restart mode, just set it to nullopt.
         */
        std::optional<FilesystemNS::Directory> restart_data_directory_ = std::nullopt;

        /*!
         * \brief File which contains the time iteration from previous run data.
         */
        FilesystemNS::File restart_time_iteration_file_;
    };


    ///@} // \addtogroup CoreGroup


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#include "Core/TimeManager/TimeManager.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_TIME_MANAGER_x_TIME_MANAGER_HPP_
