/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_
#define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_

// IWYU pragma: private, include "Core/NumberingSubset/Internal/NumberingSubsetManager.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/NumberingSubset/NumberingSubset.hpp"


namespace MoReFEM::Internal::NumberingSubsetNS
{


    template<class IndexedSectionDescriptionT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT>
    void
    NumberingSubsetManager::Create(const IndexedSectionDescriptionT&, const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) do_move_mesh =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<typename section_type::DoMoveMesh>(
                input_data, model_settings);

        Create(::MoReFEM::NumberingSubsetNS::unique_id{ section_type::GetUniqueId() }, do_move_mesh);
    }


    inline const NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetList() const
    {
        return list_;
    }


    inline NumberingSubset::vector_const_shared_ptr& NumberingSubsetManager::GetNonCstList()
    {
        return const_cast<NumberingSubset::vector_const_shared_ptr&>(GetList());
    }


    inline const NumberingSubset&
    NumberingSubsetManager::GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }


    inline NumberingSubset::const_shared_ptr
    NumberingSubsetManager ::GetNumberingSubsetPtr(::MoReFEM::NumberingSubsetNS::unique_id unique_id) const
    {
        auto it = GetIterator(unique_id);
        assert(it != GetList().cend());
        assert(!(!(*it)));
        return *it;
    }


} // namespace MoReFEM::Internal::NumberingSubsetNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_INTERNAL_x_NUMBERING_SUBSET_MANAGER_HXX_
