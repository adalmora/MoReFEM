/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 31 Mar 2015 17:11:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_
#define MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_

// IWYU pragma: private, include "Core/NumberingSubset/NumberingSubset.hpp"

#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/UniqueId/UniqueId.hpp"

#include "Core/NumberingSubset/UniqueId.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    inline bool operator==(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() == rhs.GetUniqueId();
    }


    inline bool operator<(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return lhs.GetUniqueId() < rhs.GetUniqueId();
    }


    inline bool operator!=(const NumberingSubset& lhs, const NumberingSubset& rhs)
    {
        return !(operator==(lhs, rhs));
    }


    inline bool NumberingSubset::DoMoveMesh() const noexcept
    {
        return do_move_mesh_;
    }


    template<class EnumT>
    constexpr NumberingSubsetNS::unique_id AsNumberingSubsetId(EnumT enum_value)
    {
        static_assert(std::is_enum<EnumT>());
        return NumberingSubsetNS::unique_id{ EnumUnderlyingType(enum_value) };
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_NUMBERING_SUBSET_x_NUMBERING_SUBSET_HXX_
