/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Oct 2016 14:15:02 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HPP_

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    /*!
     * \brief Create all the instances read from the input data of the type held by \a ManagerT.
     *
     * \code
     * SetFromInputDataAndModelSettings<DomainManager>(model_settings, input_data, domain_manager);
     * \endcode
     *
     * iterates through the entries of \a input_data and create all the domains encountered doing so.
     *
     * \copydoc doxygen_hide_input_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \copydoc doxygen_hide_cplusplus_variadic_args
     * These variadic arguments are here transmitted to the Create() static method arguments, not to \a ManagerT
     * constructor.
     *
     *
     * \param[in,out] manager The (singleton) manager which content will be set through this function. The singleton
     * must be created beforehand: this was not the case before but could lead to issues for singletons that takes
     * constructor arguments.
     */
    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        typename... Args
    >
    // clang-format on
    void SetFromInputDataAndModelSettings(const ModelSettingsT& model_settings,
                                          const InputDataT& input_data,
                                          ManagerT& manager,
                                          Args&&... args);


    /*!
     * \brief Create all the instances read from the input data of the type held by \a ManagerT in the
     * case we are loading prepartitioned data.
     *
     * For instance in case of \a DomainManager, it looks like:
     *
     * \code
     * SetFromPreprocessedData<DomainManager>(model_settings, input_data, domain_manager);
     * \endcode
     *
     * iterates through the entries of \a input_data and create all the domains encountered doing so.
     *
     * \copydoc doxygen_hide_morefem_data_arg
     *
     * \copydoc doxygen_hide_model_settings_arg
     *
     * \copydoc doxygen_hide_cplusplus_variadic_args
     * These variadic arguments are here transmitted to the Create() static method arguments, not to \a ManagerT
     * constructor.
     *
     * \param[in,out] manager The (singleton) manager which content will be set through this function. The singleton
     * must be created beforehand: this was not the case before but could lead to issues for singletons that takes
     * constructor arguments.
     */
    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        typename... Args
    >
    // clang-format on
    void SetFromPreprocessedData(const ModelSettingsT& model_settings,
                                 const MoReFEMDataT& morefem_data,
                                 ManagerT& manager,
                                 Args&&... args);


} // namespace MoReFEM::Advanced


/// @} // addtogroup CoreGroup


#include "Core/InputData/Advanced/SetFromInputData.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HPP_
