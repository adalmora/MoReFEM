/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 22 Dec 2015 11:34:42 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_

// IWYU pragma: private, include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"


namespace MoReFEM::Advanced
{


    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT,
        typename... Args
    >
    // clang-format on
    void SetFromInputDataAndModelSettings(const ModelSettingsT& model_settings,
                                          const InputDataT& input_data,
                                          ManagerT& manager,
                                          Args&&... args)
    {
        auto create = [&model_settings, &input_data, &manager, &args...](const auto& indexed_section_description) -> void
        {
            manager.Create(indexed_section_description, model_settings, input_data, std::forward<Args>(args)...);
        };


        // clang-format off
        using model_settings_tuple_iteration =
            ::MoReFEM::Internal::InputDataNS::TupleIteration
            <
                typename ModelSettingsT::underlying_tuple_type,
                0ul
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<typename ManagerT::indexed_section_tag>(
            model_settings.GetTuple(), input_data, create);
    }


    // clang-format off
    template
    <
        class ManagerT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT,
        typename... Args
    >
    // clang-format on
    void SetFromPreprocessedData(const ModelSettingsT& model_settings,
                                 const MoReFEMDataT& morefem_data,
                                 ManagerT& manager,
                                 Args&&... args)
    {
        auto load = [&model_settings, &morefem_data, &manager, &args...](const auto& indexed_section_description) -> void
        {
            manager.LoadFromPrepartitionedData(indexed_section_description, model_settings, morefem_data, std::forward<Args>(args)...);
        };

        // clang-format off
        using model_settings_tuple_iteration =
            ::MoReFEM::Internal::InputDataNS::TupleIteration
            <
                typename ModelSettingsT::underlying_tuple_type,
                0ul
            >;
        // clang-format on

        model_settings_tuple_iteration::template ActIfLeafInheritsFrom<typename ManagerT::indexed_section_tag>(
            model_settings.GetTuple(), morefem_data.GetInputData(), load);
    }


} // namespace MoReFEM::Advanced


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_ADVANCED_x_SET_FROM_INPUT_DATA_HXX_
