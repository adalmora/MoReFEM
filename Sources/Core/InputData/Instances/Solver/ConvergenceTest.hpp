/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct ConvergenceTest
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<ConvergenceTest,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ConvergenceTest' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = ConvergenceTest;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct ResidualNormMax : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ResidualNormMax, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();


        }; // struct ResidualNormMax

        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                ResidualNormMax
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct ConvergenceTest


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_CONVERGENCE_TEST_HPP_
