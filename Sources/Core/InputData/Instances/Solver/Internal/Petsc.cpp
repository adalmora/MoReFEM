/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 15:17:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <sstream>
#include <string>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Quoted.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"

#include "Core/InputData/Instances/Solver/Internal/Petsc.hpp"


namespace MoReFEM::Internal::InputDataNS::PetscNS
{

    namespace // anonymous
    {


        std::string GenerateConstraint()
        {
            decltype(auto) solver_factory =
                Internal::Wrappers::Petsc::SolverNS::Factory::CreateOrGetInstance(__FILE__, __LINE__);

            std::ostringstream oconv;

            Utilities::PrintContainer<Utilities::PrintPolicyNS::Quoted<>>::Do(
                solver_factory.GenerateSolverList(),
                oconv,
                ::MoReFEM::PrintNS::Delimiter::separator(", "),
                ::MoReFEM::PrintNS::Delimiter::opener("value_in(v, {"),
                ::MoReFEM::PrintNS::Delimiter::closer(" })"));

            std::string ret = oconv.str();
            return ret;
        }


    } // namespace


    const std::string& Solver::NameInFile()
    {
        static std::string ret("solver");
        return ret;
    }


    const std::string& Solver::Description()
    {
        static std::string ret("Solver to use. The list includes all the solvers which have been "
                               "explicitly activated in MoReFEM. However, it does not mean all are "
                               "actually available in your local environment: some require that "
                               "some external dependencies were added along with PETSc. The "
                               "ThirdPartyCompilationFactory facility set them up by default.");
        // chebychev cg gmres preonly bicg python };\n\t-- To use Mumps choose preonly.");
        return ret;
    }

    const std::string& Solver::Constraint()
    {
        static std::string ret{ GenerateConstraint() };
        return ret;
    }


    const std::string& Solver::DefaultValue()
    {
        static std::string ret("'SuperLU_dist'");
        return ret;
    }


    const std::string& GmresRestart::NameInFile()
    {
        static std::string ret("gmresRestart");
        return ret;
    }


    const std::string& GmresRestart::Description()
    {
        static std::string ret("gmresStart");
        return ret;
    }

    const std::string& GmresRestart::Constraint()
    {
        static std::string ret("v >= 0");
        return ret;
    }


    const std::string& GmresRestart::DefaultValue()
    {
        static std::string ret("200");
        return ret;
    }


    const std::string& Preconditioner::NameInFile()
    {
        static std::string ret("preconditioner");
        return ret;
    }


    const std::string& Preconditioner::Description()
    {
        static std::string ret("Preconditioner to use. Must be lu for any direct solver.");
        return ret;
    }

    const std::string& Preconditioner::Constraint()
    {
        static std::string ret("value_in(v, {'lu', 'none'})");
        // 'jacobi', 'sor', 'lu', 'bjacobi', 'ilu', 'asm', 'cholesky'})");
        return ret;
    }


    const std::string& Preconditioner::DefaultValue()
    {
        static std::string ret("'lu'");
        return ret;
    }


    const std::string& RelativeTolerance::NameInFile()
    {
        static std::string ret("relativeTolerance");
        return ret;
    }


    const std::string& RelativeTolerance::Description()
    {
        static std::string ret("Relative tolerance");
        return ret;
    }

    const std::string& RelativeTolerance::Constraint()
    {
        static std::string ret("v > 0.");
        return ret;
    }


    const std::string& RelativeTolerance::DefaultValue()
    {
        static std::string ret("1e-9");
        return ret;
    }


    const std::string& AbsoluteTolerance::NameInFile()
    {
        static std::string ret("absoluteTolerance");
        return ret;
    }


    const std::string& AbsoluteTolerance::Description()
    {
        static std::string ret("Absolute tolerance");
        return ret;
    }

    const std::string& AbsoluteTolerance::Constraint()
    {
        static std::string ret("v > 0.");
        return ret;
    }


    const std::string& AbsoluteTolerance::DefaultValue()
    {
        static std::string ret("1e-50");
        return ret;
    }


    const std::string& StepSizeTolerance::NameInFile()
    {
        static std::string ret("stepSizeTolerance");
        return ret;
    }


    const std::string& StepSizeTolerance::Description()
    {
        static std::string ret("Step size tolerance");
        return ret;
    }

    const std::string& StepSizeTolerance::Constraint()
    {
        static std::string ret("v > 0.");
        return ret;
    }


    const std::string& StepSizeTolerance::DefaultValue()
    {
        static std::string ret("1e-8");
        return ret;
    }


    const std::string& MaxIteration::NameInFile()
    {
        static std::string ret("maxIteration");
        return ret;
    }


    const std::string& MaxIteration::Description()
    {
        static std::string ret("Maximum iteration");
        return ret;
    }

    const std::string& MaxIteration::Constraint()
    {
        static std::string ret("v > 0");
        return ret;
    }


    const std::string& MaxIteration::DefaultValue()
    {
        static std::string ret("1000");
        return ret;
    }

} // namespace MoReFEM::Internal::InputDataNS::PetscNS


/// @} // addtogroup CoreGroup
