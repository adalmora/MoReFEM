/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_INTERNAL_x_PETSC_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_INTERNAL_x_PETSC_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::PetscNS
{


    //! Holds information related to the input datum Petsc::Solver.
    struct Solver
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::gmresRestart.
    struct GmresRestart
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::preconditioner.
    struct Preconditioner
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::relativeTolerance.
    struct RelativeTolerance
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::absoluteTolerance.
    struct AbsoluteTolerance
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::stepSizeTolerance.
    struct StepSizeTolerance
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


    //! Holds information related to the input datum Petsc::maxIteration.
    struct MaxIteration
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some
         * text from \a OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::PetscNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_INTERNAL_x_PETSC_HPP_
