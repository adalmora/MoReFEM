/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 30 Oct 2015 15:17:11 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"

#include "Core/InputData/Instances/Solver/Internal/Petsc.hpp"


namespace MoReFEM::InputDataNS
{

    struct PetscTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct Petsc
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::
          IndexedSection<Petsc<IndexT>, IndexT, PetscTag, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "Petsc";
        }

        //! Helper variable to define the \a MoReFEM::Advanced::Concept::SolverSection concept.
        static inline constexpr bool ConceptIsSolverSection = true;


        //! Convenient alias.
        using self = Petsc<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::
            IndexedSection<Petsc<IndexT>, IndexT, PetscTag, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());
        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Holds information related to the input datum Petsc::Solver.
        struct Solver
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Solver, self, Wrappers::Petsc::solver_name_type>,
          public ::MoReFEM::Internal::InputDataNS::PetscNS::Solver
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Holds information related to the input datum Petsc::gmresRestart.
        struct GmresRestart
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<GmresRestart, self, Wrappers::Petsc::set_restart_type>,
          public ::MoReFEM::Internal::InputDataNS::PetscNS::GmresRestart
        { };


        //! Holds information related to the input datum Petsc::preconditioner.
        struct Preconditioner : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                    Leaf<Preconditioner, self, Wrappers::Petsc::preconditioner_name_type>,
                                public ::MoReFEM::Internal::InputDataNS::PetscNS::Preconditioner
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Holds information related to the input datum Petsc::relativeTolerance.
        struct RelativeTolerance : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                       Leaf<RelativeTolerance, self, Wrappers::Petsc::relative_tolerance_type>,
                                   public ::MoReFEM::Internal::InputDataNS::PetscNS::RelativeTolerance
        { };


        //! Holds information related to the input datum Petsc::absoluteTolerance.
        struct AbsoluteTolerance : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                       Leaf<AbsoluteTolerance, self, Wrappers::Petsc::absolute_tolerance_type>,
                                   public ::MoReFEM::Internal::InputDataNS::PetscNS::AbsoluteTolerance
        { };


        //! Holds information related to the input datum Petsc::stepSizeTolerance.
        struct StepSizeTolerance : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                       Leaf<StepSizeTolerance, self, Wrappers::Petsc::step_size_tolerance_type>,
                                   public ::MoReFEM::Internal::InputDataNS::PetscNS::StepSizeTolerance
        { };


        //! Holds information related to the input datum Petsc::maxIteration.
        struct MaxIteration
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MaxIteration, self, Wrappers::Petsc::max_iteration_type>,
          public ::MoReFEM::Internal::InputDataNS::PetscNS::MaxIteration
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                AbsoluteTolerance,
                GmresRestart,
                MaxIteration,
                Preconditioner,
                RelativeTolerance,
                StepSizeTolerance,
                Solver
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Petsc


} // namespace MoReFEM::InputDataNS


/*!
 * \brief Macro used in most models when defining a \a Petsc section in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all fields save the \a IndexedSectionDescription  one are modifiable by the end-user.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_PETSC(enum_class_id)                                                      \
    ::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a Petsc section in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all mesh-related settings are defined in the input data tuple (save the \a IndexedSectionDescription  expected in \a
 * ModelSettings).
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_PETSC(enum_class_id)                                                          \
    ::MoReFEM::InputDataNS::Petsc<EnumUnderlyingType(enum_class_id)>


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_SOLVER_x_PETSC_HPP_
