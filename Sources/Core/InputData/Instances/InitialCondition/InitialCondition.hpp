/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>


#include "Core/InputData/Instances/Parameter/Advanced/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    struct InitialConditionTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    // clang-format off
    template<std::size_t IndexT>
    struct InitialCondition
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
    <
        InitialCondition<IndexT>,
        IndexT,
        InitialConditionTag,
        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
    >
    // clang-format on
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "InitialCondition";
        }

        //! Convenient alias.
        using self = InitialCondition<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        // clang-format off
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
        <
            InitialCondition<IndexT>,
            IndexT,
            InitialConditionTag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
        >;
        // clang-format on

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());
        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choose how is described the initial condition (through a scalar, a function, etc...)
         */
        struct Nature : public Internal::InputDataNS::ParamNS::
                            ThreeDimensionalCompoundNature<Nature, self, ::MoReFEM::ParameterNS::Type::vector>
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        /*!
         * \brief Value associated to the initial condition.
         */
        struct Value : Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundValue<Value, Nature>
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct InitialCondition


} // namespace MoReFEM::InputDataNS


/*!
 * \brief Macro used in most models when defining a \a InitialCondition section in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all fields save the \a IndexedSectionDescription  one are modifiable by the end-user.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_INITIAL_CONDITION(enum_class_id)                                          \
    ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a InitialCondition section in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, all mesh-related settings are defined in the input data tuple (save the \a IndexedSectionDescription  expected in \a
 * ModelSettings).
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_INITIAL_CONDITION(enum_class_id)                                              \
    ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::Nature,                               \
        ::MoReFEM::InputDataNS::InitialCondition<EnumUnderlyingType(enum_class_id)>::Value


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_x_INITIAL_CONDITION_HPP_
