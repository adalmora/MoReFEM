/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_INTERPOLATOR_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_INTERPOLATOR_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS
{


    //! \copydoc doxygen_hide_indexed_section_tag_alias
    struct Tag
    { };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct SourceFEltSpaceIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct SourceNumberingSubsetIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct TargetFEltSpaceIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct TargetNumberingSubsetIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_INTERNAL_x_COORDS_MATCHING_INTERPOLATOR_HPP_
