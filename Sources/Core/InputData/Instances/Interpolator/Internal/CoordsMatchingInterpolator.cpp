/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 18 Dec 2015 16:19:57 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Interpolator/Internal//CoordsMatchingInterpolator.hpp"


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS
{


    const std::string& SourceFEltSpaceIndexImpl::NameInFile()
    {
        static std::string ret("source_finite_element_space");
        return ret;
    }


    const std::string& SourceFEltSpaceIndexImpl::Description()
    {
        static std::string ret(
            "Source finite element space for which the dofs index will be associated to each Coords.");
        return ret;
    }


    const std::string& SourceNumberingSubsetIndexImpl::NameInFile()
    {
        static std::string ret("source_numbering_subset");
        return ret;
    }


    const std::string& SourceNumberingSubsetIndexImpl::Description()
    {
        static std::string ret("Source numbering subset for which the dofs index will be associated to each Coords.");
        return ret;
    }

    const std::string& TargetFEltSpaceIndexImpl::NameInFile()
    {
        static std::string ret("target_finite_element_space");
        return ret;
    }


    const std::string& TargetFEltSpaceIndexImpl::Description()
    {
        static std::string ret(
            "Target finite element space for which the dofs index will be associated to each Coords.");
        return ret;
    }


    const std::string& TargetNumberingSubsetIndexImpl::NameInFile()
    {
        static std::string ret("target_numbering_subset");
        return ret;
    }


    const std::string& TargetNumberingSubsetIndexImpl::Description()
    {
        static std::string ret("Target numbering subset for which the dofs index will be associated to each Coords.");
        return ret;
    }

} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS


/// @} // addtogroup CoreGroup
