/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_

#include <memory>
#include <vector>


#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Interpolator/Internal//CoordsMatchingInterpolator.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{

    /*!
     * \brief Holds information related to the input datum CoordsMatchingInterpolator.
     *
     */
    template<std::size_t IndexT>
    struct CoordsMatchingInterpolator : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                            CoordsMatchingInterpolator<IndexT>,
                                            IndexT,
                                            Internal::InputDataNS::CoordsMatchingInterpolatorNS::Tag,
                                            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "CoordsMatchingInterpolator";
        }

        //! Convenient alias.
        using self = CoordsMatchingInterpolator<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Source finite element space.
         */
        struct SourceFEltSpaceIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<SourceFEltSpaceIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::SourceFEltSpaceIndexImpl
        { };


        /*!
         * \brief \a NumberingSubset for the source.
         */
        struct SourceNumberingSubsetIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<SourceNumberingSubsetIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::SourceNumberingSubsetIndexImpl
        { };


        /*!
         * \brief Target finite element space.
         */
        struct TargetFEltSpaceIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TargetFEltSpaceIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::TargetFEltSpaceIndexImpl
        { };


        /*!
         * \brief \a NumberingSubset for the target.
         */
        struct TargetNumberingSubsetIndex
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TargetNumberingSubsetIndex, self, std::size_t>,
          public ::MoReFEM::Internal::InputDataNS::CoordsMatchingInterpolatorNS::TargetNumberingSubsetIndexImpl
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                SourceFEltSpaceIndex,
                SourceNumberingSubsetIndex,
                TargetFEltSpaceIndex,
                TargetNumberingSubsetIndex
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Mesh


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INTERPOLATOR_x_COORDS_MATCHING_INTERPOLATOR_HPP_
