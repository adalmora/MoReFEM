/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Mon, 18 Apr 2016 11:44:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Parameter/ViscoelasticBoundaryCondition/ViscoelasticBoundaryCondition.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& ViscoelasticBoundaryCondition::GetName()
    {
        static std::string ret("ViscoelasticBoundaryCondition");
        return ret;
    }


    const std::string& ViscoelasticBoundaryCondition::Damping::GetName()
    {
        static std::string ret("Damping");
        return ret;
    }


    const std::string& ViscoelasticBoundaryCondition::Stiffness::GetName()
    {
        static std::string ret("Stiffness");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup
