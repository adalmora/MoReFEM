/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Aug 2013 11:26:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp"             // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct ViscoelasticBoundaryCondition
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<ViscoelasticBoundaryCondition,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ViscoelasticBoundaryCondition' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = ViscoelasticBoundaryCondition;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choose how is described the  (through a scalar, a function, etc...)
         */
        struct Damping : public Internal::InputDataNS::ParamNS::ScalarParameter<Damping, ViscoelasticBoundaryCondition>
        {


            //! Convenient alias.
            using self = Damping;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, ViscoelasticBoundaryCondition>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g.
            //! ViscoelasticBoundaryCondition.poisson_ratio).
            static const std::string& GetName();

        }; // struct Damping


        /*!
         * \brief Choose how is described the  (through a scalar, a function, etc...)
         */
        struct Stiffness
        : public Internal::InputDataNS::ParamNS::ScalarParameter<Stiffness, ViscoelasticBoundaryCondition>
        {


            //! Convenient alias.
            using self = Stiffness;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, ViscoelasticBoundaryCondition>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g.
            //! ViscoelasticBoundaryCondition.poisson_ratio).
            static const std::string& GetName();

        }; // struct Stiffness


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Damping,
                Stiffness
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct ViscoelasticBoundaryCondition


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/ViscoelasticBoundaryCondition/ViscoelasticBoundaryCondition.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_VISCOELASTIC_BOUNDARY_CONDITION_x_VISCOELASTIC_BOUNDARY_CONDITION_HPP_
