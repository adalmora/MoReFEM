/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 12:18:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_

#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


namespace MoReFEM::InputDataNS::MaterialProperty
{


    //! \copydoc doxygen_hide_core_input_data_section
    template<class EnclosingTypeT>
    struct VolumicMass
    : public Internal::InputDataNS::ParamNS::ScalarParameter<VolumicMass<EnclosingTypeT>, EnclosingTypeT>
    {


        //! Convenient alias.
        using self = VolumicMass<EnclosingTypeT>;

        //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
        using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, EnclosingTypeT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship to an underlying internal class, which is required for the internal mechanics of
        //! interpreting the content of input data file.
        friend typename parent::section_type;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
        static const std::string& GetName();


    }; // struct VolumicMass


} // namespace MoReFEM::InputDataNS::MaterialProperty


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/MaterialProperty/VolumicMass.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MATERIAL_PROPERTY_x_VOLUMIC_MASS_HPP_
