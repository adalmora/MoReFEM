/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Parameter/Solid/Solid.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Solid::Kappa1::GetName()
    {
        static std::string ret("Kappa1");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup
