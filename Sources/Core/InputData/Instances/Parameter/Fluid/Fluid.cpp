/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 15:31:20 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include "Core/InputData/Instances/Parameter/Fluid/Fluid.hpp"
#include <string>


namespace MoReFEM::InputDataNS
{


    const std::string& Fluid::GetName()
    {
        static std::string ret("Fluid");
        return ret;
    };


    const std::string& Fluid::Viscosity::GetName()
    {
        static std::string ret("Viscosity");
        return ret;
    }


    const std::string& Fluid::Density::GetName()
    {
        static std::string ret("Density");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup
