/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/MaterialProperty/VolumicMass.hpp"
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Fluid
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Fluid,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = Fluid;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Convenient alias.
        using VolumicMass = MaterialProperty::VolumicMass<self>;

        //! \copydoc doxygen_hide_core_input_data_section_with_index
        struct Viscosity : public Internal::InputDataNS::ParamNS::ScalarParameter<Viscosity, Fluid>
        {


            //! Convenient alias.
            using self = Viscosity;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Fluid>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct Viscosity


        //! \copydoc doxygen_hide_core_input_data_section
        struct Density : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Density, Fluid>
        {


            //! Convenient alias.
            using self = Density;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, Fluid>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Return the name of the section in the input datum.
             *
             */
            static const std::string& GetName();


        }; // struct Density


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                VolumicMass,
                Density,
                Viscosity
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Fluid


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FLUID_x_FLUID_HPP_
