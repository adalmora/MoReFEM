/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Fri, 29 Apr 2016 16:46:08 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"

#include <cstddef> // IWYU pragma: keep
#include <string>


namespace MoReFEM::InputDataNS::Source
{


    template<std::size_t IndexT>
    const std::string& PressureFromFile<IndexT>::FilePath::NameInFile()
    {
        static std::string ret("FilePath");
        return ret;
    }


    template<std::size_t IndexT>
    const std::string& PressureFromFile<IndexT>::FilePath::Description()
    {
        static std::string ret("Path of the file to use. "
                               "Format: "
                               "time pressure "
                               "value1 value1 "
                               "value2 value2 "
                               "... ");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::Source


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HXX_
