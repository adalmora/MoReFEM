/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 26 Aug 2015 12:18:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS::Source
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct StaticPressure
    : public Internal::InputDataNS::ParamNS::ScalarParameter<StaticPressure,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! Convenient alias.
        using self = StaticPressure;

        //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
        using parent =
            Internal::InputDataNS::ParamNS::ScalarParameter<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship to an underlying internal class, which is required for the internal mechanics of
        //! interpreting the content of input data file.
        friend typename parent::section_type;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Return the moniker of the section in the input data file (e.g. NoEnclosingSection.poisson_ratio).
        static const std::string& GetName();

    }; // struct StaticPressure


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct PressureFromFile
    : public Internal::InputDataNS::ParamNS::
          IndexedScalarParameter<PressureFromFile<IndexT>, IndexT, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "PressureFromFile";
        }

        //! Convenient alias.
        using self = PressureFromFile<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Class that holds the definition of all non template dependents static functions.
         */
        struct FilePath : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<FilePath, self, std::string>
        {
            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();
        };


        //! Alias to the tuple of structs.
        // clang-format off
                using section_content_type = std::tuple
                <
                    FilePath
                >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct PressureFromFile


} // namespace MoReFEM::InputDataNS::Source


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Source/Pressure.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_PRESSURE_HPP_
