/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Source/Internal/RectangularSourceTimeParameter.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{

    struct RectangularSourceTimeParameterTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    // clang-format off
    template<std::size_t IndexT>
    class RectangularSourceTimeParameter
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
    <
        RectangularSourceTimeParameter<IndexT>,
        IndexT,
        RectangularSourceTimeParameterTag,
        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
    >
    // clang-format on
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "RectangularSourceTimeParameter";
        }

        //! Convenient alias.
        using self = RectangularSourceTimeParameter<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Initial time of the horizontal source in ReactionDiffusion.
        struct InitialTimeOfActivationList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<InitialTimeOfActivationList, self, double>,
        public ::MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS::InitialTimeOfActivationList
        { };


        //! Initial time of the horizontal source in ReactionDiffusion.
        struct FinalTimeOfActivationList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<FinalTimeOfActivationList, self, double>,
        public ::MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS::FinalTimeOfActivationList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                InitialTimeOfActivationList,
                FinalTimeOfActivationList
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct RectangularSourceTimeParameter


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_RECTANGULAR_SOURCE_TIME_PARAMETER_HPP_
