/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 26 May 2015 17:13:18 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/ThreeDimensionalCompoundParameter.hpp" // IWYU pragma: export
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct VectorialTransientSource : public Internal::InputDataNS::ParamNS::IndexedThreeDimensionalCompoundParameter<
                                          VectorialTransientSource<IndexT>,
                                          IndexT,
                                          ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "VectorialTransientSource";
        }

        //! Convenient alias.
        using self = VectorialTransientSource<IndexT>;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    }; // struct TransientSource


/*!
 * \brief Macro used in most models when defining a \a VectorialTransientSource in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, only the \a IndexedSectionDescription  used internally to store the description is put in \a ModelSettinga,
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(enum_class_id)                                 \
    ::MoReFEM::InputDataNS::VectorialTransientSource<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription


/*!
 * \brief Macro used in most models when defining a \a VectorialTransientSource in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, the whole section (which is only two leaves...) is up to the end user:
 * - What is the nature of the input data (constant, piecewise constant by domain, Lua function, etc...)
 * - What is the value.
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_VECTORIAL_TRANSIENT_SOURCE(enum_class_id)                                     \
    ::MoReFEM::InputDataNS::VectorialTransientSource<EnumUnderlyingType(enum_class_id)>


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_SOURCE_x_VECTORIAL_TRANSIENT_SOURCE_HPP_
