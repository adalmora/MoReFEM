/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 3 Dec 2015 14:11:41 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>


#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"


namespace MoReFEM::InputDataNS::Source
{


    const std::string& StaticPressure::GetName()
    {
        static std::string ret("StaticPressure");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::Source


/// @} // addtogroup CoreGroup
