/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 20 Oct 2015 11:54:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Parameter/Source/Internal/RectangularSourceTimeParameter.hpp"


namespace MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS
{


    const std::string& InitialTimeOfActivationList::NameInFile()
    {
        static std::string ret("initial_time_of_activation");
        return ret;
    }


    const std::string& InitialTimeOfActivationList::Description()
    {
        static std::string ret("Value of the initial time to activate the source.");
        return ret;
    }

    const std::string& InitialTimeOfActivationList::DefaultValue()
    {
        static std::string ret("0.");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::NameInFile()
    {
        static std::string ret("final_time_of_activation");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::Description()
    {
        static std::string ret("Value of the final time to activate the source.");
        return ret;
    }


    const std::string& FinalTimeOfActivationList::DefaultValue()
    {
        static std::string ret("0.");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS


/// @} // addtogroup CoreGroup
