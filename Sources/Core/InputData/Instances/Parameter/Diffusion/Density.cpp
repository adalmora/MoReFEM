/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 20 May 2015 14:19:50 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Parameter/Diffusion/Diffusion.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Diffusion::Density::GetName()
    {
        static std::string ret("Density");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup
