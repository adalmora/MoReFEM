/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 17 Aug 2015 11:45:45 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <type_traits> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp"             // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Diffusion
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Diffusion,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = Diffusion;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());


        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! \copydoc doxygen_hide_core_input_data_section_with_index
        template<std::size_t IndexT>
        struct Tensor : public Internal::InputDataNS::ParamNS::IndexedScalarParameter<Tensor<IndexT>, IndexT, Diffusion>
        {
            //! \copydoc doxygen_hide_indexed_section_basename
            static std::string BaseName()
            {
                return "Tensor";
            }

            //! Convenient alias.
            using self = Tensor<IndexT>;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::IndexedScalarParameter<self, IndexT, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        }; // struct Tensor


        //! \copydoc doxygen_hide_core_input_data_section
        struct Density : public Internal::InputDataNS::ParamNS::ScalarParameter<Density, Diffusion>
        {


            //! Convenient alias.
            using self = Density;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct Density


        //! \copydoc doxygen_hide_core_input_data_section
        struct TransfertCoefficient
        : public Internal::InputDataNS::ParamNS::ScalarParameter<TransfertCoefficient, Diffusion>
        {

            //! Convenient alias.
            using self = TransfertCoefficient;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Diffusion>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct TransfertCoefficient


    }; // struct Diffusion


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_DIFFUSION_x_DIFFUSION_HPP_
