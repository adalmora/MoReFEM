/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HPP_

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"
#include "Core/InputData/Instances/Parameter/Internal/ThreeDimensionalCompoundParameterFields.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    /*!
     * \brief A parent class used to define a very specific type of\a Parameter in the input data file.
     *
     * \a Internal::InputDataNS::ParamNS::VectorialParameter enables a user to define a \a Parameter which each value is actually a vector; all
     * components of the vector are in this case defined the same way.
     *
     * Current class provides a different trade-off:
     * - The size of the vector is fixed at 3 (class aims to represent three dimensional data)
     * - Each component may be defined differently: one may be for instance a constant while the other is defined by a
     * Lua function.
     *
     * Test \a Compound3DParameterFromInputData illustrates how to define such a \a Parameter.
     *
     * \tparam DerivedT Name of the derived class in the CRTP.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     */
    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    struct ThreeDimensionalCompoundParameter
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>
    {

        //! Convenient alias used to define a required friendship.
        using section_type = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature : public Internal::InputDataNS::ParamNS::
                            ThreeDimensionalCompoundNature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::vector>
        { };


        //! \copydoc doxygen_hide_param_value_struct
        struct Value : Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundValue<Value, Nature>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;


      private:
        //! Content of the section.
        section_content_type section_content_;
    };


    /*!
     * \brief Same as \a ScalarParameter for a \a Parameter that may be present multiple times with an index.
     */
    // clang-format off
    template
    <
        class DerivedT,
        std::size_t IndexT,
        class EnclosingSectionT
    >
    // clang-format on
    struct IndexedThreeDimensionalCompoundParameter
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::
          IndexedSection<DerivedT, IndexT, std::false_type, EnclosingSectionT>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "ThreeDimensionalCompoundParameter";
        }

        //! Convenient alias used to define a required friendship.
        using section_type =
            ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<DerivedT, IndexT, EnclosingSectionT>;


        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature : public Internal::InputDataNS::ParamNS::
                            ThreeDimensionalCompoundNature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::vector>
        { };


        //! \copydoc doxygen_hide_param_value_struct
        struct Value : Internal::InputDataNS::ParamNS::ThreeDimensionalCompoundValue<Value, Nature>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;


      private:
        //! Content of the section.
        section_content_type section_content_;
    };


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_HPP_
