/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_PARAMETER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_PARAMETER_HPP_

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp"
#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"
#include "Core/Parameter/TypeEnum.hpp"
#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{

    /*!
     * \class doxygen_hide_section_content_attribute
     *
     * \brief Tuple which contents the actual values of the section read from the input data file.
     *
     */


    /*!
     * \brief A parent class used to define scalar \a Parameter in the input data file.
     *
     * Two fields are expected to define such a Parameter:
     * - One called 'nature', which specifies how the \a Parameter is defined (as a constant, as piecewise constant by
    domain, etc...)
     * - One called 'value' which specifies the associated value,
     *
     * Current class provides those mandatory fields; a specific \a ScalarParameter is expected to inherit from it.
     *
     * Let's take the example of \a PoissonRatio, which is defined within a much larger section \a Solid in the input
    data file.
     * Its declaration looks like:
     *
     \code
     struct PoissonRatio : public Internal::InputDataNS::ParamNS::ScalarParameter<PoissonRatio, Solid>
     {
         //! Convenient alias.
         using self = PoissonRatio;

         //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
         using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Solid>;

         static_assert(std::is_convertible<self*, parent*>());

         //! \cond IGNORE_BLOCK_IN_DOXYGEN
         //! Friendship to an underlying internal class, which is required for the internal mechanics of
         //! interpreting the content of input data file.
         friend typename parent::section_type;
         //! \endcond IGNORE_BLOCK_IN_DOXYGEN

         //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
         static const std::string& GetName();


     }; // struct PoissonRatio
    \endcode
     *
     * \tparam DerivedT Name of the derived class in the CRTP.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     *
     * Actual initialization is expected to be performed with the \a InitScalarParameterFromInputData function.
     */
    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    struct ScalarParameter : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>
    {

        //! Convenient alias used to define a required friendship.
        using section_type = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature
        : public Internal::InputDataNS::ParamNS::Nature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::scalar>
        { };


        struct Value : public Internal::InputDataNS::ParamNS::Value<Value, Nature, std::false_type>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

      private:
        //! \copydoc doxygen_hide_section_content_attribute
        section_content_type section_content_;
    };


    /*!
     * \brief Same as \a ScalarParameter for a \a Parameter that may be present multiple times with an index.
     */
    // clang-format off
    template
    <
        class DerivedT,
        std::size_t IndexT,
        class EnclosingSectionT
    >
    // clang-format on
    struct IndexedScalarParameter : public ::MoReFEM::Advanced::InputDataNS::Crtp::
                                        IndexedSection<DerivedT, IndexT, std::false_type, EnclosingSectionT>
    {
        //! Convenient alias used to define a required friendship.
        using section_type = ::MoReFEM::Advanced::InputDataNS::Crtp::
            IndexedSection<DerivedT, IndexT, std::false_type, EnclosingSectionT>;


        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature
        : public Internal::InputDataNS::ParamNS::Nature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::scalar>
        { };


        struct Value : public Internal::InputDataNS::ParamNS::Value<Value, Nature, std::false_type>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

      private:
        //! \copydoc doxygen_hide_section_content_attribute
        section_content_type section_content_;
    };


    /*!
     * \brief A parent class used to define vectorial \a Parameter in the input data file.
     *
     * Three fields are expected to define such a Parameter:
     * - One called 'nature', which specifies how the \a Parameter is defined (as a constant, as piecewise constant by
     * domain, etc...)
     * - One called 'vector_dimension', which specifies the size of the vector.
     * - One called 'value' which specifies the content of the vector.
     *
     * Current class provides those mandatory fields; a specific \a VectorialParameter is expected to inherit from it.
     *
     * All the components share the same nature; if you would like a 3D vector which each component might not be of the
     * same nature, see \a ThreeDimensionalCompoundParameter class instead.
     *
     * Way to declare a specific \a VectorialParameter is very close to the one described for \a ScalarParameter; please
     * have a look there and just replace \a ScalarParameter by \a VectorialParameter. You may also have a look at the
     * \a VectorialParameterFromInputData test.
     *
     * \tparam DerivedT Name of the derived class in the CRTP.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     *
     * Actual initialization is expected to be performed with the \a InitVectorialParameterFromInputData function.
     */
    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    struct VectorialParameter : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>
    {

        //! Convenient alias used to define a required friendship.
        using section_type = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature
        : public Internal::InputDataNS::ParamNS::Nature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::vector>
        { };


        //! Field to specify the expected size of a vector.
        struct VectorDimension : public Internal::InputDataNS::ParamNS::VectorDimension<VectorDimension, DerivedT>
        { };

        //! Field to specify the values for  vector.
        struct Value : public Internal::InputDataNS::ParamNS::Value<Value, Nature, VectorDimension>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            VectorDimension,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;


      private:
        //! \copydoc doxygen_hide_section_content_attribute
        section_content_type section_content_;
    };


    /*!
     * \brief A parent class used to define matricial \a Parameter in the input data file.
     *
     * Three fields are expected to define such a Parameter:
     * - One called 'nature', which specifies how the \a Parameter is defined (as a constant, as piecewise constant by
     * domain, etc...)
     * - One called 'matrix_dimension', which specifies the numbers of rows and columns in the matrix..
     * - One called 'value' which specifies the content of the matrix (given as a vector row by row - description in Lua
     * file should guide well enough the user).
     *
     * Current class provides those mandatory fields; a specific \a MatricialParameter is expected to inherit from it.
     *
     * Way to declare a specific \a MatricialParameter is very close to the one described for \a ScalarParameter; please
     * have a look there and just replace \a ScalarParameter by \a VectorialParameter. You may also have a look at the
     * \a MatricialParameterFromInputData test.
     *
     * \tparam DerivedT Name of the derived class in the CRTP.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     *
     * Actual initialization is expected to be performed with the \a InitMatricialParameterFromInputData function.
     */
    template<class DerivedT, class EnclosingSectionT>
    // clang-format on
    struct MatricialParameter : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>
    {

        //! Convenient alias used to define a required friendship.
        using section_type = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

        /*!
         * \brief Choose how is described the parameter (through a scalar, a function, etc...)
         */
        struct Nature
        : public Internal::InputDataNS::ParamNS::Nature<Nature, DerivedT, ::MoReFEM::ParameterNS::Type::matrix>
        { };


        //! Field to specify the expected size of a matrix.
        struct MatrixDimension : public Internal::InputDataNS::ParamNS::MatrixDimension<MatrixDimension, DerivedT>
        { };

        //! Field to specify the values for  matrix.
        struct Value : public Internal::InputDataNS::ParamNS::Value<Value, Nature, MatrixDimension>
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Nature,
            MatrixDimension,
            Value
        >;
        // clang-format on

        //! Friendship to grant access to \a section_content_ data attribute.
        friend ::MoReFEM::Advanced::InputDataNS::Crtp::Section<DerivedT, EnclosingSectionT>;

      private:
        //! \copydoc doxygen_hide_section_content_attribute
        section_content_type section_content_;
    };


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ADVANCED_x_PARAMETER_HPP_
