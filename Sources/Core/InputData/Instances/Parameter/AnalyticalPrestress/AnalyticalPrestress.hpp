/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 31 Mar 2016 16:44:25 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ANALYTICAL_PRESTRESS_x_ANALYTICAL_PRESTRESS_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ANALYTICAL_PRESTRESS_x_ANALYTICAL_PRESTRESS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/Crtp/Leaf.hpp"    // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp" // IWYU pragma: export

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export

namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct AnalyticalPrestress
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<AnalyticalPrestress,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ActiveStress' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = AnalyticalPrestress;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_core_input_data_section
        struct Contractility
        : public Internal::InputDataNS::ParamNS::ScalarParameter<Contractility, AnalyticalPrestress>
        {


            //! Convenient alias.
            using self = Contractility;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, AnalyticalPrestress>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. solid.poisson_ratio).
            static const std::string& GetName();


        }; // struct Contractility


        //! \copydoc doxygen_hide_core_input_data_section
        struct InitialCondition
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<InitialCondition, AnalyticalPrestress>
        {

            /*!
             * \brief Return the name of the section in the input datum ('InitialCondition' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = InitialCondition;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, AnalyticalPrestress>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct ActiveStress : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ActiveStress, self, double>
            {


                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();

                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            //! Alias to the tuple of structs.
            // clang-format off
                using section_content_type = std::tuple
                <
                    ActiveStress
                >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;

        }; // struct InitialCondition


        //! Alias to the tuple of structs.
        using section_content_type = std::tuple<Contractility, InitialCondition>;

      private:
        //! Content of the section.
        section_content_type section_content_;

    }; // struct AnalyticalPrestress


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_ANALYTICAL_PRESTRESS_x_ANALYTICAL_PRESTRESS_HPP_
