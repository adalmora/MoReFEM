/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/Parameter/Internal/Traits.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    template<::MoReFEM::ParameterNS::Type TypeT>
    std::string SelectorDescription(bool do_print_header)
    {
        std::ostringstream oconv;

        decltype(auto) empty_str = Utilities::EmptyString();

        if (do_print_header)
        {
            if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
                oconv << "The value for the parameter, which type depends directly on the nature chosen in the "
                         "namesake field:\n";
            else if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::vector)
                oconv << "The values of the vectorial parameter; expected format is a table (opening = '{', "
                         "closing = '} and separator = ',') and each item depends on the nature specified at the "
                         "namesake field:\n";
            else if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::matrix)
                oconv << "The values of the matricial parameter; expected format is a table (opening = '{', "
                         "closing = '} and separator = ',') and each item depends on the nature specified at the "
                         "namesake field (values are given as a vector and redispatched with help of "
                         "'matrix_dimension' field; the content is to be given row by row.):\n";
        }

        oconv << "\n If nature is 'constant', expected format is "
              << Internal::InputDataNS::Traits::Format<double>::Print(empty_str);
        oconv << "\n If nature is 'piecewise_constant_by_domain', expected format is "
              << Internal::InputDataNS::Traits::Format<std::map<std::size_t, double>>::Print(empty_str);

        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
            oconv << "\n If nature is 'lua_function', expected format is a Lua function inside a '[[' ']]' block:\n"
                     "[[\n"
                     "function(x, y, z) \n"
                     "return x + y - z\n"
                     "end\n"
                     "]]\n"
                     "where x, y and z are global coordinates. "
                     "sin, cos, tan, exp and so forth require a 'math.' prefix.\n";

        std::string ret(oconv.str());
        return ret;
    }


    template<::MoReFEM::ParameterNS::Type TypeT>
    typename Internal::ParameterNS::Traits<TypeT>::variant_type SelectorHelper(const std::string& nature)
    {
        using traits = Internal::ParameterNS::Traits<TypeT>;

        if constexpr (!std::is_same_v<typename traits::lua_function_type, std::false_type>)
        {
            if (nature == "lua_function")
                return typename traits::lua_function_type();
        }

        if (nature == "constant")
            return typename traits::constant_in_lua_file_type();
        else if (nature == "piecewise_constant_by_domain")
            return typename traits::piecewise_constant_in_lua_file_type();
        else if (nature == "ignore")
            return nullptr;
        else
        {
            std::cerr << "Choice '" << nature << "' is invalid!" << std::endl;
            assert(false
                   && "Possible choices should all be dealt with here; the possibilities should have been "
                      "checked by the Constraint in the option file.");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HXX_
