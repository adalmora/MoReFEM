/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_FIELDS_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_FIELDS_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/ThreeDimensionalCompoundParameterFields.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::NameInFile()
    {
        static std::string ret("nature");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::Description()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static std::string ret("How is given each component of the parameter (as a constant, as a Lua function"
                                   ", per quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                   "parameter (in this case it will stay at nullptr).");
            return ret;
        } else
        {
            static std::string ret("How is given the parameter (as a constant, per "
                                   "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                   "parameter (in this case it will stay at nullptr) - in this case all "
                                   "components must be 'ignore'.");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::Constraint()
    {
        static std::string ret("value_in(v, {"
                               "'ignore', "
                               "'constant', "
                               "'lua_function',"
                               "'piecewise_constant_by_domain'})");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& ThreeDimensionalCompoundNature<DerivedT, EnclosingSectionT, TypeT>::DefaultValue()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
            return Utilities::EmptyString();
        else
        {
            static std::string ret("{}");
            return ret;
        }
    }


    // clang-format off
    template<class DerivedT, class NatureT>
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    // clang-format on
    auto ThreeDimensionalCompoundValue<DerivedT, NatureT>::Selector(const ModelSettingsT& model_settings,
                                                                    const InputDataT* input_data)
        -> storage_type_for_compound
    {
        auto selector = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<NatureT>(*input_data,
                                                                                                 model_settings);

        static_assert(std::is_same<decltype(selector), std::vector<std::string>>());
        assert(selector.size() == 3ul);

        return { SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[0]),
                 SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[1]),
                 SelectorHelper<::MoReFEM::ParameterNS::Type::scalar>(selector[2]) };
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::NameInFile()
    {
        static std::string ret("value");
        return ret;
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::Description()
    {
        static std::string ret = "For each 'VALUE*n* (see 'Expected format' below), the format may be: "
                                 + SelectorDescription<::MoReFEM::ParameterNS::Type::scalar>(false);

        return ret;
    }


    template<class DerivedT, class NatureT>
    const std::string& ThreeDimensionalCompoundValue<DerivedT, NatureT>::DefaultValue()
    {
        static std::string ret("{}");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_THREE_DIMENSIONAL_COMPOUND_PARAMETER_FIELDS_HXX_
