/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HPP_

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    /*!
     * \brief Returns the description of the Selector class.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     * \param[in] do_print_header If true, print the introductory header which explains format for scalar, vector or matrix.
     * Currently choose false for a Compound 3D parameter and true for any other cases.
     *
     * \return The description as a new std::string.
     */
    template<::MoReFEM::ParameterNS::Type TypeT>
    std::string SelectorDescription(bool do_print_header = true);


    /*!
     * \brief Helper function to return the proper value for the variant depending on the \a nature provided
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     * \param[in] nature The nature of the parameter in the input datafile (see the description inside this file to see what are the possibilities).
     *
     * \return The relevant value.
     */
    template<::MoReFEM::ParameterNS::Type TypeT>
    typename Internal::ParameterNS::Traits<TypeT>::variant_type SelectorHelper(const std::string& nature);


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Internal/Selector.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_SELECTOR_HPP_
