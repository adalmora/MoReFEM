/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HPP_

// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <map>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/InputData/Instances/Parameter/Internal/Selector.hpp"
#include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
#include "Core/Parameter/Internal/Traits.hpp"
#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    /*!
     * \brief Choose how is described the \a Parameter (through a scalar, a function, etc...)
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    struct Nature
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
             <
                 DerivedT,
                 EnclosingSectionT,
                 std::string
             >
    // clang-format on
    {

        //! Useful alias for defining associated field 'Value'.
        using enclosing_section_type = EnclosingSectionT;

        //! Useful alias for defining associated field 'Value'.
        static constexpr auto parameter_type_enum = TypeT;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Constraint to fulfill.
         *
         * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some text from \a
         * OptionFile example file:
         *
         * An age should be greater than 0 and less than, say, 150. It is possible
         * to check it with a logical expression (written in Lua). The expression
         * should be written with 'v' being the variable to be checked.
         * \a constraint = "v >= 0 and v < 150"
         *
         * It is possible to check whether a variable is in a set of acceptable
         * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
         * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
         *
         * If a vector is retrieved, the constraint must be satisfied on every
         * element of the vector.
         */
        static const std::string& Constraint();
    };


    /*!
     * \brief Field which specify the number of items expected in the vector,
     */
    template<class DerivedT, class EnclosingSectionT>
    struct VectorDimension
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DerivedT, EnclosingSectionT, std::size_t>
    // clang-format on
    {

        //! Useful alias for defining associated field 'Value'.
        using enclosing_section_type = EnclosingSectionT;

        //! Type of the \a Parameter.
        static constexpr auto parameter_type_enum = ::MoReFEM::ParameterNS::Type::vector;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Field which specify the number of rows and columns (in that order) expected for the matrix,
     */
    template<class DerivedT, class EnclosingSectionT>
    struct MatrixDimension
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DerivedT, EnclosingSectionT, std::vector<std::size_t>>
    // clang-format on
    {

        //! Useful alias for defining associated field 'Value'.
        using enclosing_section_type = EnclosingSectionT;

        //! Type of the \a Parameter.
        static constexpr auto parameter_type_enum = ::MoReFEM::ParameterNS::Type::matrix;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief The value of the parameter.
     *
     * A std::variant is used here: the value might be a scalar, an associative container or a LuaFunction
     * depending on what the user chose as 'NatureT'.
     *
     * A std::variant might be read with a visitor (see std::visit) or std::get<> calls; anyway these calls
     * aren't supposed to be exposed for user interface.
     *
     * \tparam NatureT Another field of the input data file, which should provide the type of expected data.
     * The associated string read should be "constant", "piecewise_constant_by_domain" or "lua_function".
     *
     * Typically it should be a sibling field in the same section, e.g.:
     *
     \verbatim
     Kappa1 =
     {
        nature = "constant",
        value = 5.
     }
     \endverbatim
     * where nature is the field interpreted by NatureT and value the field interpreted by current struct.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    // clang-format off
    template<class DerivedT, class NatureT, class NeltsT>
    struct Value
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf
    <
        DerivedT,
        typename NatureT::enclosing_section_type,
        typename Internal::ParameterNS::Traits<NatureT::parameter_type_enum>::variant_type
    >
    // clang-format on
    {

        //! Enum class which specifies the type of \a Parameter considered (scalar, vector or matrix). It is the same as
        //! the \a TypeT template argument of sister \a Nature field.
        static constexpr auto parameter_type_enum = NatureT::parameter_type_enum;

        //! Alias to the underlying storage type.
        using storage_type = typename Internal::ParameterNS::Traits<parameter_type_enum>::variant_type;


        /*!
         * \brief Selector which role is to set the std::variant with the proper type pointed by \a NatureT.
         *
         * The std::variant is filled with default value: the point is not the actual value put there, but
         * the fact that the type is known (e.g. assigning a double if \a NatureT points to 'constant').
         *
         * \tparam InputDataT Type of \a input_data.
         *
         * \copydoc doxygen_hide_input_data_arg
         *
         * \copydoc doxygen_hide_model_settings_arg
         *
         * \return The std::variant properly filled with a non important value of the correct type (e.g. double
         * if NatureT gives away "constant", std::map<std::size_t, double> if NatureT is
         * "piecewise_constant_by_domain".
         */
        // clang-format off
        template
        <
            ::MoReFEM::Concept::InputDataType InputDataT,
            ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
        >
        static storage_type Selector(const ModelSettingsT& model_settings,
                                     const InputDataT* input_data);
        // clang-format on

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value has
         * been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HPP_
