/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 17 Jul 2018 17:54:43 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HXX_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HXX_

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/String/EmptyString.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Internal::InputDataNS::ParamNS
{


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::NameInFile()
    {
        static std::string ret("nature");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::Description()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static std::string ret("How is given each component of the parameter (as a constant, as a Lua function"
                                   ", per quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                   "parameter (in this case it will stay at nullptr).");
            return ret;
        } else
        {
            static std::string ret("How is given the parameter (as a constant, per "
                                   "quadrature point, etc...). Choose \"ignore\" if you do not want this "
                                   "parameter (in this case it will stay at nullptr).");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT,
        ::MoReFEM::ParameterNS::Type TypeT
    >
    // clang-format on
    const std::string& Nature<DerivedT, EnclosingSectionT, TypeT>::Constraint()
    {
        if constexpr (TypeT == ::MoReFEM::ParameterNS::Type::scalar)
        {
            static std::string ret("value_in(v, {"
                                   "'ignore', "
                                   "'constant', "
                                   "'lua_function',"
                                   "'piecewise_constant_by_domain'})");
            return ret;
        } else
        {
            static std::string ret("value_in(v, {"
                                   "'ignore', "
                                   "'constant', "
                                   "'piecewise_constant_by_domain'})");
            return ret;
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& VectorDimension<DerivedT, EnclosingSectionT>::NameInFile()
    {
        static std::string ret("vector_dimension");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& VectorDimension<DerivedT, EnclosingSectionT>::Description()
    {
        static std::string ret("Number of elements expected in each local vector (returned by Parameter::GetValue() at "
                               "a local coords and for a given GeometricElt). The values "
                               "given in the option file must match exactly this constraint - there is no padding "
                               "whatsoever if your data are inconsistent and an exception is thrown in this case).");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& MatrixDimension<DerivedT, EnclosingSectionT>::NameInFile()
    {
        static std::string ret("matrix_dimension");
        return ret;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class EnclosingSectionT
    >
    // clang-format on
    const std::string& MatrixDimension<DerivedT, EnclosingSectionT>::Description()
    {
        static std::string ret("Number of rows and columns (in that order) expected in each local matrix (returned by "
                               "Parameter::GetValue() at a local coords and for a given GeometricElt). The values "
                               "given in the option file must match exactly this constraint - there is no padding "
                               "whatsoever if your data are inconsistent and an exception is thrown in this case).");
        return ret;
    }



    template<class DerivedT, class NatureT, class NeltsT>
    template
    <
        ::MoReFEM::Concept::InputDataType InputDataT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT
    >
    auto Value<DerivedT, NatureT, NeltsT>::Selector(const ModelSettingsT& model_settings,
                                                    const InputDataT* input_data)
    -> storage_type
    {
        const auto nature = ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<NatureT>(*input_data, model_settings);
        static_assert(Utilities::IsSpecializationOf<std::variant, storage_type>());
        return SelectorHelper<parameter_type_enum>(nature);
    }


    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::NameInFile()
    {
        static std::string ret("value");
        return ret;
    }


    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::Description()
    {
        static std::string ret = ::MoReFEM::Internal::InputDataNS::ParamNS::SelectorDescription<parameter_type_enum>();
        return ret;
    }

    template<class DerivedT, class NatureT, class NeltsT>
    const std::string& Value<DerivedT, NatureT, NeltsT>::DefaultValue()
    {
        if constexpr (parameter_type_enum == ::MoReFEM::ParameterNS::Type::scalar)
            return Utilities::EmptyString();
        else
        {
            static std::string ret("{}");
            return ret;
        }
    }


} // namespace MoReFEM::Internal::InputDataNS::ParamNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_INTERNAL_x_PARAMETER_FIELDS_HXX_
