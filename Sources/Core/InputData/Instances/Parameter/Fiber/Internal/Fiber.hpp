/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_INTERNAL_x_FIBER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_INTERNAL_x_FIBER_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep


namespace MoReFEM::InputDataNS::FiberNS
{


    /*!
     * \brief Ensight file from which data are read.
     *
     */
    struct EnsightFile
    {

        //! Convenient alias.
        using storage_type = std::string;


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Index of the mesh.
     *
     */
    struct DomainIndex
    {


        //! Convenient alias.
        using storage_type = std::size_t;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Index of the finite element space.
     *
     */
    struct FEltSpaceIndex
    {


        //! Convenient alias.
        using storage_type = std::size_t;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Name of the unknown considered.
     *
     */
    struct UnknownName
    {


        //! Convenient alias.
        using storage_type = std::string;

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


} // namespace MoReFEM::InputDataNS::FiberNS

/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_FIBER_x_INTERNAL_x_FIBER_HPP_
