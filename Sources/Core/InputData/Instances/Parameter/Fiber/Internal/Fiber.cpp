/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 12 Oct 2015 12:00:59 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Parameter/Fiber/Internal/Fiber.hpp"


namespace MoReFEM::InputDataNS::FiberNS
{


    const std::string& EnsightFile::NameInFile()
    {
        static std::string ret("ensight_file");
        return ret;
    }


    const std::string& EnsightFile::Description()
    {
        static std::string ret("Path to Ensight file.");
        return ret;
    }


    const std::string& DomainIndex::NameInFile()
    {
        static std::string ret("domain_index");
        return ret;
    }


    const std::string& DomainIndex::Description()
    {
        static std::string ret("Index of the domain upon which parameter is defined.");
        return ret;
    }


    const std::string& FEltSpaceIndex::NameInFile()
    {
        static std::string ret("felt_space_index");
        return ret;
    }


    const std::string& FEltSpaceIndex::Description()
    {
        static std::string ret("Index of the finite element space upon which parameter is defined.");
        return ret;
    }

    const std::string& UnknownName::NameInFile()
    {
        static std::string ret("unknown");
        return ret;
    }


    const std::string& UnknownName::Description()
    {
        static std::string ret("Name of the unknown used to describe the dofs. Might be a fictitious one (see "
                               "documentation for more details).");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::FiberNS


/// @} // addtogroup CoreGroup
