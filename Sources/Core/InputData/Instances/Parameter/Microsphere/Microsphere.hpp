/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 7 May 2019 17:05:14 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp"             // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Microsphere
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Microsphere,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ActiveStress' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Microsphere;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_core_input_data_section
        struct InPlaneFiberDispersionI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<InPlaneFiberDispersionI4, Microsphere>
        {


            //! Convenient alias.
            using self = InPlaneFiberDispersionI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct InPlaneFiberDispersionI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct OutOfPlaneFiberDispersionI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<OutOfPlaneFiberDispersionI4, Microsphere>
        {


            //! Convenient alias.
            using self = OutOfPlaneFiberDispersionI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct OutOfPlaneFiberDispersionI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct FiberStiffnessDensityI4
        : public Internal::InputDataNS::ParamNS::ScalarParameter<FiberStiffnessDensityI4, Microsphere>
        {


            //! Convenient alias.
            using self = FiberStiffnessDensityI4;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct FiberStiffnessDensityI4


        //! \copydoc doxygen_hide_core_input_data_section
        struct InPlaneFiberDispersionI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<InPlaneFiberDispersionI6, Microsphere>
        {


            //! Convenient alias.
            using self = InPlaneFiberDispersionI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();


        }; // struct InPlaneFiberDispersionI6


        //! \copydoc doxygen_hide_core_input_data_section
        struct OutOfPlaneFiberDispersionI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<OutOfPlaneFiberDispersionI6, Microsphere>
        {


            //! Convenient alias.
            using self = OutOfPlaneFiberDispersionI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct OutOfPlaneFiberDispersionI6


        //! \copydoc doxygen_hide_core_input_data_section
        struct FiberStiffnessDensityI6
        : public Internal::InputDataNS::ParamNS::ScalarParameter<FiberStiffnessDensityI6, Microsphere>
        {


            //! Convenient alias.
            using self = FiberStiffnessDensityI6;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, Microsphere>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g. Microsphere.poisson_ratio).
            static const std::string& GetName();

        }; // struct FiberStiffnessDensityI6


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
               InPlaneFiberDispersionI4,
               OutOfPlaneFiberDispersionI4,
               FiberStiffnessDensityI4,
               InPlaneFiberDispersionI6,
               OutOfPlaneFiberDispersionI6,
               FiberStiffnessDensityI6
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;

    }; // struct Microsphere


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARAMETER_x_MICROSPHERE_x_MICROSPHERE_HPP_
