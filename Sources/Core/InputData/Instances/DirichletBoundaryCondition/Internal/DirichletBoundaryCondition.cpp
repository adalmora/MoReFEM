/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 24 Sep 2015 11:23:48 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/DirichletBoundaryCondition/Internal/DirichletBoundaryCondition.hpp"


namespace MoReFEM::Internal::InputDataNS::BoundaryConditionNS
{


    const std::string& UnknownName::NameInFile()
    {
        static std::string ret("unknown");
        return ret;
    }


    const std::string& UnknownName::Description()
    {
        static std::string ret("Name of the unknown addressed by the boundary condition.");
        return ret;
    }


    const std::string& Component::NameInFile()
    {
        static std::string ret("component");
        return ret;
    }


    const std::string& Component::Description()
    {
        static std::string ret("Comp1, Comp2 or Comp3");
        return ret;
    }

    const std::string& Component::Constraint()
    {
        static std::string ret("value_in(v, {'Comp1', 'Comp2', 'Comp3', 'Comp12', 'Comp23', 'Comp13', 'Comp123'})");
        return ret;
    }


    const std::string& Values::NameInFile()
    {
        static std::string ret("value");
        return ret;
    }


    const std::string& Values::Description()
    {
        static std::string ret("Values at each of the relevant component.");
        return ret;
    }

    const std::string& DomainIndex::NameInFile()
    {
        static std::string ret("domain_index");
        return ret;
    }


    const std::string& DomainIndex::Description()
    {
        static std::string ret("Index of the domain onto which essential boundary condition is defined.");
        return ret;
    }

    const std::string& IsMutable::NameInFile()
    {
        static std::string ret("is_mutable");
        return ret;
    }


    const std::string& IsMutable::Description()
    {
        static std::string ret("Whether the values of the boundary condition may vary over time.");
        return ret;
    }


    const std::string& IsMutable::DefaultValue()
    {
        static std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::BoundaryConditionNS


/// @} // addtogroup CoreGroup
