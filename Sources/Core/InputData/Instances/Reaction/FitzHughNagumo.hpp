/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS::ReactionNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct FitzHughNagumo
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<FitzHughNagumo,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = FitzHughNagumo;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Coefficient a in FitzHughNagumo.
        struct ACoefficient : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ACoefficient, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient b in FitzHughNagumo.
        struct BCoefficient : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<BCoefficient, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient c in FitzHughNagumo.
        struct CCoefficient : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<CCoefficient, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();

            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
                using section_content_type = std::tuple
                <
                    ACoefficient,
                    BCoefficient,
                    CCoefficient
                >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct FitzHughNagumo


} // namespace MoReFEM::InputDataNS::ReactionNS


/// @} // addtogroup CoreGroup

#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_FITZ_HUGH_NAGUMO_HPP_
