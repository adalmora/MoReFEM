/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export


namespace MoReFEM::InputDataNS::ReactionNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct MitchellSchaeffer
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<MitchellSchaeffer,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = MitchellSchaeffer;


        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Coefficient Tau_in in MitchellSchaeffer.
        struct TauIn : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauIn, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient Tau_out in MitchellSchaeffer.
        struct TauOut : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauOut, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient Tau_open in MitchellSchaeffer.
        struct TauOpen : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TauOpen, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! \copydoc doxygen_hide_core_input_data_section
        struct TauClose : public Internal::InputDataNS::ParamNS::ScalarParameter<TauClose, MitchellSchaeffer>
        {


            //! Convenient alias.
            using self = TauClose;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, MitchellSchaeffer>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the name of the section in the input datum.
            static const std::string& GetName();


        }; // struct TauClose


        //! Coefficient U_gate in MitchellSchaeffer.
        struct PotentialGate : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialGate, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient U_min in MitchellSchaeffer.
        struct PotentialMin : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialMin, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Coefficient U_max in MitchellSchaeffer.
        struct PotentialMax : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<PotentialMax, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Reaction Coefficient in FitzHughNagumo.
        struct ReactionCoefficient
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ReactionCoefficient, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no
             * value has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            TauIn,
            TauOut,
            TauOpen,
            TauClose,
            PotentialGate,
            PotentialMin,
            PotentialMax
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct MitchellSchaeffer


} // namespace MoReFEM::InputDataNS::ReactionNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_MITCHELL_SCHAEFFER_HPP_
