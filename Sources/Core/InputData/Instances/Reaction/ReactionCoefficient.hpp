/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_REACTION_COEFFICIENT_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_REACTION_COEFFICIENT_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS::ReactionNS
{


    //! Reaction Coefficient.
    struct ReactionCoefficient
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::
          Leaf<ReactionCoefficient, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection, double>
    {


        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();

        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no value
         * has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::InputDataNS::ReactionNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_REACTION_x_REACTION_COEFFICIENT_HPP_
