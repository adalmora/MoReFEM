/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 15:14:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_COORDS_MATCHING_FILE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_COORDS_MATCHING_FILE_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/CoordsMatchingFile.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    class CoordsMatchingFile : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                   CoordsMatchingFile<IndexT>,
                                   IndexT,
                                   Internal::InputDataNS::CoordsMatchingFileNS::Tag,
                                   ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "CoordsMatchingFile";
        }


        //! Convenient alias.
        using self = CoordsMatchingFile<IndexT>;

        //! Friendship to section parent.
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Indicates the mesh upon which the domain is defined (if any).
        struct Path : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Path, self, std::string>,
                      public ::MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS::Path
        { };


        //! Indicates both meshes involved in the interpolation
        struct DoComputeReverse : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DoComputeReverse, self, bool>,
                                  public ::MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS::DoComputeReverse
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Path,
                DoComputeReverse
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Domain


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_COORDS_MATCHING_FILE_HPP_
