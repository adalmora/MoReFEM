/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 15:14:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/Domain.hpp"
#include "Core/InputData/Instances/Geometry/Internal/LightweightDomainList.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    class LightweightDomainList : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                      LightweightDomainList<IndexT>,
                                      IndexT,
                                      Internal::InputDataNS::LightweightDomainListNS::Tag,
                                      ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "LightweightDomainList";
        }

        //! Convenient alias.
        using self = LightweightDomainList<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            LightweightDomainList<IndexT>,
            IndexT,
            Internal::InputDataNS::LightweightDomainListNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! Indicates the mesh upon which the domain is defined (if any).
        struct MeshIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshIndex, self, std::size_t>,
                           public Internal::InputDataNS::LightweightDomainListNS::MeshIndex
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates the list of mesh labels considered in the variable domains. Separators then will split
        //! them into different domains.
        struct MeshLabelList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshLabelList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::DomainNS::MeshLabelList
        { };


        struct DomainIndexList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndexList, self, std::vector<std::size_t>>,
          public Internal::InputDataNS::LightweightDomainListNS::DomainIndexList
        { };


        struct NumberInDomainList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<NumberInDomainList, self, std::vector<std::size_t>>,
          public Internal::InputDataNS::LightweightDomainListNS::NumberInDomainList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                MeshIndex,
                MeshLabelList,
                DomainIndexList,
                NumberInDomainList
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // class LightweightDomainList


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_LIGHTWEIGHT_DOMAIN_LIST_HPP_
