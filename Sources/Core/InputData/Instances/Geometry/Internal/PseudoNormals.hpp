/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 14:48:37 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_INTERNAL_x_PSEUDO_NORMALS_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_INTERNAL_x_PSEUDO_NORMALS_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::PseudoNormalsNS
{

    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct MeshIndexImpl
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();
    };


    /*!
     * \brief Class that holds the definition of all non template dependents static functions.
     */
    struct DomainIndexListImpl
    {

        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::PseudoNormalsNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_INTERNAL_x_PSEUDO_NORMALS_HPP_
