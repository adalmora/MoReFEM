/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 19 Mar 2015 15:14:35 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/Geometry/Internal/CoordsMatchingFile.hpp"


namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS
{


    const std::string& Path::NameInFile()
    {
        static std::string ret("path");
        return ret;
    }


    const std::string& Path::Description()
    {
        static std::string ret("File that gives for each Coords on the first mesh on the interface the index of "
                               "the equivalent Coords in the second mesh.");

        return ret;
    }


    const std::string& DoComputeReverse::NameInFile()
    {
        static std::string ret("do_compute_reverse");
        return ret;
    }


    const std::string& DoComputeReverse::Description()
    {
        static std::string ret("In the interpolation file, there are two meshes involved: one 'source' and one "
                               "'target'. If this field is set to true, two CoordsMatching objects are created: "
                               "one source -> target and one target -> source. If false, only the former is built. ");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::CoordsMatchingFileNS


/// @} // addtogroup CoreGroup
