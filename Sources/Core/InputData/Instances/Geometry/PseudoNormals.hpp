/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Sun, 11 Aug 2013 15:06:06 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Geometry/Internal/PseudoNormals.hpp"


namespace MoReFEM::InputDataNS
{


    struct PseudoNormalsTag
    { };


    /*!
     * \brief Holds information related to the input datum PseudoNormals.
     *
     * \tparam IndexT Several PseudoNormals can be present in the input data file, provided they each
     * use a different value for this template parameter. For instance:
     *
     * \code
     * PseudoNormals1 = {
     * mesh_index = 1
     * ...
     * }
     *
     * PseudoNormals2 = {
     * mesh_index = 2
     * ...
     * }
     * \endcode
     */
    template<std::size_t IndexT>
    struct PseudoNormals : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                               PseudoNormals<IndexT>,
                               IndexT,
                               PseudoNormalsTag,
                               ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "PseudoNormals";
        }

        //! Convenient alias.
        using self = PseudoNormals<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Indicates the mesh upon which the pseudo-normals are defined (if any).
         */
        struct MeshIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<MeshIndex, self, std::size_t>,
                           public ::MoReFEM::Internal::InputDataNS::PseudoNormalsNS::MeshIndexImpl
        { };


        /*!
         * \brief Indicates the domain upon which the pseudo-normals are defined (if any).
         */
        struct DomainIndexList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndexList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::PseudoNormalsNS::DomainIndexListImpl
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                MeshIndex,
                DomainIndexList
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct PseudoNormals


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_GEOMETRY_x_PSEUDO_NORMALS_HPP_
