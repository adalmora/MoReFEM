/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 16:10:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_NUMBERING_SUBSET_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_NUMBERING_SUBSET_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Core/InputData/Instances/FElt/Internal/NumberingSubset.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct NumberingSubset : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                 NumberingSubset<IndexT>,
                                 IndexT,
                                 Internal::InputDataNS::NumberingSubsetNS::Tag,
                                 ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "NumberingSubset";
        }

        //! Convenient alias.
        using self = NumberingSubset<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        /*!
         * \brief Whether a vector defined on this numbering subset might be used to compute a
         * movemesh.
         */
        struct DoMoveMesh : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DoMoveMesh, self, bool>,
                            public ::MoReFEM::Internal::InputDataNS::NumberingSubsetNS::DoMoveMesh
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            DoMoveMesh
        >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct NumberingSubset


} // namespace MoReFEM::InputDataNS


/*!
 * \brief Macro used in most models when defining a \a NumberingSubset in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, we do not want the user to meddle at all with numbering subset and the whole section is in \a
 * ModelSettings.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_NUMBERING_SUBSET(enum_class_id)                                           \
    ::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,                                 \
        ::MoReFEM::InputDataNS::NumberingSubset<EnumUnderlyingType(enum_class_id)>

/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_NUMBERING_SUBSET_HPP_
