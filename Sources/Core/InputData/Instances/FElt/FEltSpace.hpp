/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 11:47:43 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/FElt/Internal/FEltSpace.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct FEltSpace : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                           FEltSpace<IndexT>,
                           IndexT,
                           Internal::InputDataNS::FEltSpaceNS::Tag,
                           ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "FiniteElementSpace";
        }

        //! Convenient alias.
        using self = FEltSpace<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            FEltSpace<IndexT>,
            IndexT,
            Internal::InputDataNS::FEltSpaceNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Indicates the god of dof that possess the finite element space.
         *
         * God of dof index is the same as mesh one.
         */
        struct GodOfDofIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<GodOfDofIndex, self, std::size_t>,
                               public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::GodOfDofIndex
        { };


        //! Indicates the domain upon which the finite element space is defined. This domain must be
        //! uni-dimensional.
        struct DomainIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndex, self, std::size_t>,
                             public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::DomainIndex
        { };


        //! Indicates which unknowns are defined on the finite element space.
        struct UnknownList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<UnknownList, self, std::vector<std::string>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::UnknownList
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates for each unknowns the shape function to use.
        struct ShapeFunctionList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<ShapeFunctionList, self, std::vector<std::string>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::ShapeFunctionList
        { };


        //! Indicates the numbering subset to use for each unknown.
        struct NumberingSubsetList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<NumberingSubsetList, self, std::vector<std::size_t>>,
          public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::NumberingSubsetList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            GodOfDofIndex,
            DomainIndex,
            UnknownList,
            ShapeFunctionList,
            NumberingSubsetList
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct FEltSpace


/*!
 * \brief Macro used in most models when defining a \a FEltSpace in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, finite element spaces are tightly defined by the author of the model and the only field upon which we
 * want to let the end user deal with is the shape function to use.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(enum_class_id)                                                 \
    ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,                                       \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::GodOfDofIndex,                           \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::UnknownList,                             \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::DomainIndex,                             \
        ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::NumberingSubsetList

/*!
 * \brief Macro used in most models when defining a \a FEltSpace in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, the only freedom we want to let about the finite element spaces is the shape function to use.
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(enum_class_id)                                                     \
    ::MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::ShapeFunctionList


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_F_ELT_SPACE_HPP_
