/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 17:15:58 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/FElt/Internal/Unknown.hpp"


namespace MoReFEM::Internal::InputDataNS::UnknownNS
{


    const std::string& Name::NameInFile()
    {
        static std::string ret("name");
        return ret;
    }


    const std::string& Name::Description()
    {
        static std::string ret("Name of the unknown (used for displays in output).");
        return ret;
    }


    const std::string& Nature::NameInFile()
    {
        static std::string ret("nature");
        return ret;
    }


    const std::string& Nature::Description()
    {
        static std::string ret("Index of the god of dof into which the finite element space is defined.");
        return ret;
    }


    const std::string& Nature::Constraint()
    {
        static std::string ret("value_in(v, {'scalar', 'vectorial'})");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::UnknownNS


/// @} // addtogroup CoreGroup
