/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 16:10:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_INTERNAL_x_NUMBERING_SUBSET_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_INTERNAL_x_NUMBERING_SUBSET_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>


namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS
{

    //! \copydoc doxygen_hide_indexed_section_tag_alias
    struct Tag
    { };


    //! Helper class that defines non template static functions of namesake class in NumberingSubset
    //! namespace.
    struct DoMoveMesh
    {
        //! Name of the input datum in Lua input file.
        static const std::string& NameInFile();

        //! Description of the input datum.
        static const std::string& Description();


        /*!
         * \return Default value.
         *
         * This is intended to be used only when the class is used to create a default file; never when no
         * value has been given in the input data file (doing so is too much error prone...)
         *
         * This is given as a string; if no default value return an empty string. The value must be \a
         * OptionFile-formatted.
         */
        static const std::string& DefaultValue();
    };


} // namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_INTERNAL_x_NUMBERING_SUBSET_HPP_
