/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 10 Apr 2015 16:10:10 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <string>

#include "Core/InputData/Instances/FElt/Internal/NumberingSubset.hpp"


namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS
{

    const std::string& DoMoveMesh::NameInFile()
    {
        static std::string ret("do_move_mesh");
        return ret;
    }


    const std::string& DoMoveMesh::Description()
    {
        static std::string ret("Whether a vector defined on this numbering subset might be used to compute "
                               "a movemesh. If true, a FEltSpace featuring this numbering subset will "
                               "compute additional quantities to enable fast computation. This should be "
                               "false for most numbering subsets, and when it's true the sole "
                               "unknown involved should be a displacement.");
        return ret;
    }


    const std::string& DoMoveMesh::DefaultValue()
    {
        static std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::NumberingSubsetNS


/// @} // addtogroup CoreGroup
