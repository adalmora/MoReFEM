/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 24 Mar 2015 17:15:58 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_UNKNOWN_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_UNKNOWN_HPP_

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/FElt/Internal/Unknown.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct Unknown : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                         Unknown<IndexT>,
                         IndexT,
                         Internal::InputDataNS::UnknownNS::Tag,
                         ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "Unknown";
        }

        //! Convenient alias.
        using self = Unknown<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

        //! \copydoc doxygen_hide_alias_self intended to be used in classes enclosed in current class.
        using enclosing_section_type = self;


        /*!
         * \brief Name of the unknown.
         */
        struct Name : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Name, self, std::string>,
                      public ::MoReFEM::Internal::InputDataNS::UnknownNS::Name
        { };


        /*!
         * \brief Nature of the unknown.
         */
        struct Nature : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Nature, self, std::string>,
                        public ::MoReFEM::Internal::InputDataNS::UnknownNS::Nature
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Name,
                Nature
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Unknown


} // namespace MoReFEM::InputDataNS


/*!
 * \brief Macro used in most models when defining a \a Unknown in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 * In most models, we do not want the user to meddle at all with unknownsand the whole section is in \a ModelSettings.
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_UNKNOWN(enum_class_id)                                                    \
    ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,                                         \
        ::MoReFEM::InputDataNS::Unknown<EnumUnderlyingType(enum_class_id)>


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_F_ELT_x_UNKNOWN_HPP_
