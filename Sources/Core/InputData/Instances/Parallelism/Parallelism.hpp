//! \file
//
//
//  Parallelism.hpp
//  MoReFEM
//
//  Created by sebastien on 29/07/2019.
// Copyright © 2019 Inria. All rights reserved.
//

#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARALLELISM_x_PARALLELISM_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARALLELISM_x_PARALLELISM_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/Parallelism/Internal/Parallelism.hpp"


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief Input data which says how the program should do in its run.
     *
     * \copydoc doxygen_hide_parallelism_input_data_cases
     */
    struct Parallelism
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Parallelism,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        /*!
         * \brief Return the name of the section in the input datum.
         *
         * e.g. 'Mesh1' for IndexT = 1.
         *
         * \return Name of the section in the input datum.
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Parallelism;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choice of the parallel behaviour.
         *
         * \copydoc doxygen_hide_parallelism_input_data_cases
         */
        struct Policy : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Policy, self, std::string>,
                        public Internal::InputDataNS::ParallelismNS::Policy
        { };


        /*!
         * \brief Path that might be useful (depending on the \a Policy choice).
         */
        struct Directory : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Directory, self, std::string>,
                           public Internal::InputDataNS::ParallelismNS::Directory
        { };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Policy,
            Directory
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;
    };


} // namespace MoReFEM::InputDataNS


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_PARALLELISM_x_PARALLELISM_HPP_
