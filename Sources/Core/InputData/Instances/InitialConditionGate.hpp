/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Tue, 27 Aug 2013 11:26:39 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_GATE_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_GATE_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct InitialConditionGate
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<InitialConditionGate,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        //! Return the name of the section in the input datum.
        static const std::string& GetName();

        //! Convenient alias.
        using self = InitialConditionGate;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Initial Condition Gate in ReactionDiffusion.
        struct Value : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Value, InitialConditionGate, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };

        //! To create output of the Gate or not.
        struct WriteGate : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<WriteGate, InitialConditionGate, bool>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Value,
            WriteGate
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct InitialConditionGate


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_INITIAL_CONDITION_GATE_HPP_
