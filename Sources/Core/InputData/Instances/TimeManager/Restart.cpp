//! \file
//
//
//  Restart.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/02/2023.
//
//

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/TimeManager/Restart.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Restart::GetName()
    {
        static std::string ret("Restart");
        return ret;
    }


    const std::string& Restart::TimeIndex::NameInFile()
    {
        static std::string ret("time_iteration");
        return ret;
    }


    const std::string& Restart::TimeIndex::Description()
    {
        static std::string ret("Time iteration from which we should restart the model (should be present in the first "
                               "column of the 'time_iteration.hhdata' file of the previous run. Put 0 if no restart "
                               "intended. This value makes only sense in 'RunFromPreprocessed' mode.");
        return ret;
    }


    const std::string& Restart::TimeIndex::DefaultValue()
    {
        static std::string ret("0");
        return ret;
    }


    const std::string& Restart::DataDirectory::NameInFile()
    {
        static std::string ret("data_directory");
        return ret;
    }


    const std::string& Restart::DataDirectory::Description()
    {
        static std::string ret("Result directory of the previous run (as it was written in Result::OutputDirectory "
                               "field - so without the 'Rank_*'subfolder that is automatically added). Leave empty "
                               "if no restart intended.");
        return ret;
    }


    const std::string& Restart::DataDirectory::DefaultValue()
    {
        static std::string ret("''"); // not empty string on purpose: I want the default value to be this!
        return ret;
    }


    const std::string& Restart::DoWriteRestartData::NameInFile()
    {
        static std::string ret("do_write_restart_data");
        return ret;
    }


    const std::string& Restart::DoWriteRestartData::Description()
    {
        static std::string ret("Whether restart data are written or not. Such data are written only on root processor "
                               "and only in binary format.");
        return ret;
    }

    const std::string& Restart::DoWriteRestartData::DefaultValue()
    {
        static std::string ret("true");
        return ret;
    }

} // namespace MoReFEM::InputDataNS
