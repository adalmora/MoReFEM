//! \file
//
//
//  Restart.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/02/2023.
//
//

#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_RESTART_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_RESTART_HPP_

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    //! For more informations about the whereabouts of restart mode, please refer to this [wiki
    //! note](../Wiki/Restart.md).
    struct Restart
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Restart,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {


        /*!
         * \brief Return the name of the section in the input datum ('transient' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Restart;


        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Time iteration from which restart mode must kick off.
         */
        struct TimeIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<TimeIndex, self, std::size_t>
        {
            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has
             * been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        /*!
         * \brief Location of the restart data.
         */
        struct DataDirectory : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DataDirectory, self, std::string>
        {
            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has
             * been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        /*!
         * \brief Whether restart data are written or not.
         */
        struct DoWriteRestartData : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DoWriteRestartData, self, bool>
        {
            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value has
             * been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
    using section_content_type = std::tuple
    <
        TimeIndex,
        DataDirectory,
        DoWriteRestartData
    >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Restart


} // namespace MoReFEM::InputDataNS


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_TIME_MANAGER_x_RESTART_HPP_
