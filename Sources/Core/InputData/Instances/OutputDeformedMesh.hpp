/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Fri, 2 Aug 2013 11:08:01 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HPP_
#define MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HPP_

#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    struct OutputDeformedMeshTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section
    template<std::size_t IndexT>
    struct OutputDeformedMesh : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                    OutputDeformedMesh<IndexT>,
                                    IndexT,
                                    OutputDeformedMeshTag,
                                    ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {
        //! Convenient alias.
        using self = OutputDeformedMesh<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<OutputDeformedMesh,
                                                            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Output directory in which results will be written.
        struct OutputName : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputName, self, std::string>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Output directory in which results will be written.
        struct OutputFormat : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputFormat, self, std::string>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();

            /*!
             * \return Constraint to fulfill.
             *
             * Might be left empty; if not the format to respect is the \a OptionFile one. Hereafter some text
             * from \a OptionFile example file:
             *
             * An age should be greater than 0 and less than, say, 150. It is possible
             * to check it with a logical expression (written in Lua). The expression
             * should be written with 'v' being the variable to be checked.
             * \a constraint = "v >= 0 and v < 150"
             *
             * It is possible to check whether a variable is in a set of acceptable
             * value. This is performed with 'value_in' (a Lua function defined by \a OptionFile).
             * \a constraint = "value_in(v, {'Messiah', 'Water Music'})"
             *
             * If a vector is retrieved, the constraint must be satisfied on every
             * element of the vector.
             */
            static const std::string& Constraint();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Output directory in which results will be written.
        struct OutputSpaceUnit : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputSpaceUnit, self, double>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };

        //! Output directory in which results will be written.
        struct OutputOffset : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputOffset, self, std::size_t>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };

        //! Output frequence to create meshes.
        struct OutputFrequence : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<OutputFrequence, self, std::size_t>
        {


            //! Name of the input datum in Lua input file.
            static const std::string& NameInFile();

            //! Description of the input datum.
            static const std::string& Description();


            /*!
             * \return Default value.
             *
             * This is intended to be used only when the class is used to create a default file; never when no value
             * has been given in the input data file (doing so is too much error prone...)
             *
             * This is given as a string; if no default value return an empty string. The value must be \a
             * OptionFile-formatted.
             */
            static const std::string& DefaultValue();
        };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                OutputName,
                OutputFormat,
                OutputSpaceUnit,
                OutputOffset,
                OutputFrequence
            >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct Result


} // namespace MoReFEM::InputDataNS


/// @} // addtogroup CoreGroup


#include "Core/InputData/Instances/OutputDeformedMesh.hxx" // IWYU pragma: export


#endif // MOREFEM_x_CORE_x_INPUT_DATA_x_INSTANCES_x_OUTPUT_DEFORMED_MESH_HPP_
