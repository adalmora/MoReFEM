/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Wed, 7 Jan 2015 14:19:22 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/

#include <cassert>
#include <iosfwd>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "Core/InitTimeKeepLog.hpp"


namespace MoReFEM
{


    void InitTimeKeepLog(const FilesystemNS::Directory& result_directory)
    {
        assert(result_directory.DoExist());

        auto file = result_directory.AddFile("time_log.hhdata");

        std::ofstream out{ file.NewContent(__FILE__, __LINE__) };

        TimeKeep::CreateOrGetInstance(__FILE__, __LINE__, std::move(out));
    }


} // namespace MoReFEM


/// @} // addtogroup CoreGroup
