/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Mon, 14 Dec 2015 12:18:51 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HXX_

// IWYU pragma: private, include "OperatorInstances/NonConformInterpolator/FromCoordsMatching/FromCoordsMatching.hpp"

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "Core/MoReFEMData/Advanced/ParallelismStrategy.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolatorManager.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM::NonConformInterpolatorNS { enum class store_matrix_pattern; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::NonConformInterpolatorNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    FromCoordsMatching::FromCoordsMatching(const MoReFEMDataT& morefem_data,
                                           const std::size_t coords_matching_interpolator_index,
                                           store_matrix_pattern do_store_matrix_pattern)
#ifndef NDEBUG
    : do_store_matrix_pattern_(do_store_matrix_pattern)
#endif // NDEBUG
    {
        decltype(auto) coords_matching_interpolator_manager =
            Internal::FEltSpaceNS::CoordsMatchingInterpolatorManager::GetInstance(__FILE__, __LINE__);

        decltype(auto) coords_matching_interpolator =
            coords_matching_interpolator_manager.GetCoordsMatchingInterpolator(coords_matching_interpolator_index);

        decltype(auto) parallelism_ptr = morefem_data.GetParallelismPtr();

        assert(!(!parallelism_ptr));
        const auto& parallelism = *parallelism_ptr;

        SetFileForPrepartitionedData(parallelism);

        switch (parallelism.GetParallelismStrategy())
        {
        case Advanced::parallelism_strategy::precompute:
        case Advanced::parallelism_strategy::parallel_no_write:
        case Advanced::parallelism_strategy::parallel:
        {
            StandardConstruct(coords_matching_interpolator, do_store_matrix_pattern);
            break;
        }
        case Advanced::parallelism_strategy::run_from_preprocessed:
        {
            ConstructFromPrepartitionedData(morefem_data.GetMpi(), coords_matching_interpolator_index);
            break;
        }
        case Advanced::parallelism_strategy::none:
            assert(false && "Invalid choice for this method!");
            exit(EXIT_FAILURE);
        }
    }


    inline const GlobalMatrix& FromCoordsMatching::GetInterpolationMatrix() const noexcept
    {
        assert(!(!interpolation_matrix_));
        return *interpolation_matrix_;
    }


    inline GlobalMatrix& FromCoordsMatching::GetNonCstInterpolationMatrix() noexcept
    {
        return const_cast<GlobalMatrix&>(GetInterpolationMatrix());
    }


    inline const Wrappers::Petsc::MatrixPattern& FromCoordsMatching::GetMatrixPattern() const noexcept
    {
        assert(do_store_matrix_pattern_ == store_matrix_pattern::yes && "If 'no', present method shouldn't be called.");
        assert(!(!matrix_pattern_));
        return *matrix_pattern_;
    }


    inline const std::vector<PetscInt>& FromCoordsMatching::GetNonZeroPositionPerRow() const noexcept
    {
        return non_zero_position_per_row_;
    }


} // namespace MoReFEM::NonConformInterpolatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_NON_CONFORM_INTERPOLATOR_x_FROM_COORDS_MATCHING_x_FROM_COORDS_MATCHING_HXX_
