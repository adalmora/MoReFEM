/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Fri, 21 Aug 2020 11:34:26 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_DEVIATORIC_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_DEVIATORIC_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4Deviatoric.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("ExponentialJ1J4Deviatoric");
        return ret;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::ExponentialJ1J4Deviatoric(
        const Solid& solid,
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibers)
    : mu_1_(solid.GetMu1()), mu_2_(solid.GetMu2()), c_0_(solid.GetC0()), c_1_(solid.GetC1()), c_2_(solid.GetC2()),
      c_3_(solid.GetC3()), fibers_(fibers)
    { }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quad_pt,
                                                                     const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return GetMu1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetMu2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
               + GetC0().GetValue(quad_pt, geom_elt) * expI1 + GetC2().GetValue(quad_pt, geom_elt) * expI4;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Square(I3_pow_minus_one_third) * (I1 - 3. * I3_pow_one_third) * expI1
               + mu1 * I3_pow_minus_one_third;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * NumericNS::Pow(I3, -2. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_third = 2. / 3.;

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return -two_third * C0 * C1 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__) * I1
                   * (I1 - 3. * I3_pow_one_third) * expI1
               + two_third * C2 * C3 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__) * I4
                     * (I3_pow_one_third - I4) * expI4
               + mu1 * minus_one_third * I1 * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__)
               + 2. * mu2 * minus_one_third * I2 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return -2. * C2 * C3 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I4) * expI4;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Pow(I3, -4. / 3., __FILE__, __LINE__)
               * (2. * C1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + NumericNS::Square(I3_pow_one_third))
               * expI1;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_ninth = 2. / 9.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return two_ninth * NumericNS::Pow(I3, -10. / 3., __FILE__, __LINE__)
                   * (C0 * C1 * expI1 * I1
                          * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                             + 5. * I1 * NumericNS::Square(I3_pow_one_third) - 12. * I3)
                      + C2 * C3 * expI4 * I4
                            * (-4. * I3 + (5. + 2. * C3) * I4 * NumericNS::Square(I3_pow_one_third)
                               - 4. * C3 * I3_pow_one_third * NumericNS::Square(I4) + 2. * C3 * NumericNS::Cube(I4)))
               + 4. / 9. * mu1 * I1 * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__)
               + 10. / 9. * mu2 * I2 * NumericNS::Pow(I3, -8. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return 2. * C2 * C3 * NumericNS::Pow(I3, 4 * minus_one_third, __FILE__, __LINE__) * expI4
               * ((1. + 2. * C3) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__)
                  - 4. * C3 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I4 + 2. * C3 * NumericNS::Square(I4));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        constexpr const double minus_two_third = -2. / 3.;

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return minus_two_third * C0 * C1 * expI1 * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__)
                   * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                      + 2. * I1 * NumericNS::Square(I3_pow_one_third) - 3. * I3)
               - 1. / 3. * mu1 * NumericNS::Pow(I3, 2. * minus_two_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * 2. * minus_one_third * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double minus_two_third = 2. * minus_one_third;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        return minus_two_third * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__) * C2 * C3 * expI4
               * (-I3 + 2. * (1. + C3) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__) * I4
                  - 4. * C3 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I4 * I4
                  + 2. * C3 * NumericNS::Cube(I4));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetMu1() const noexcept
    {
        return mu_1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetMu2() const noexcept
    {
        return mu_2_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetC0() const noexcept
    {
        return c_0_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetC1() const noexcept
    {
        return c_1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetC2() const noexcept
    {
        return c_2_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetC3() const noexcept
    {
        return c_3_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const FiberList<FiberPolicyT, ParameterNS::Type::vector>*
    ExponentialJ1J4Deviatoric<FiberPolicyT, CoordsPolicyT>::GetFibersI4() const noexcept
    {
        return fibers_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_EXPONENTIAL_J1_J4_DEVIATORIC_HXX_
