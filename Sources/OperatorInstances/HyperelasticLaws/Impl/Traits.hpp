//! \file


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HPP_

#include <cstddef>

namespace MoReFEM::HyperelasticLawNS
{

    /*!
     * \brief Enum class to tell whether I4 or I6 are activated.
     *
     * \attention This way of handling the invariants for the hyperelastic laws is rather naive and can't handle all
     * possible hyperelastic laws. It works for the moment with the current laws considered, but this should
     * be refactored to be more generic (#1821).
     *
     */
    enum class highest_activated_invariant { I3, I4, I6 };


    /*!
     * \brief Traits class which provides basic informations about the hyperelastic law considered.
     */
    template<highest_activated_invariant HighestActivatedInvariantT = highest_activated_invariant::I3>
    // clang-format on
    class Traits
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Traits<HighestActivatedInvariantT>;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Traits() = default;

        //! Destructor.
        ~Traits() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Traits(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Traits(self&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        self& operator=(const self& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        self& operator=(self&& rhs) = delete;

        ///@}

      public:
        //! Whether I4 is activated.
        static constexpr bool IsI4Activated();

        //! Whether I6 is activated.
        static constexpr bool IsI6Activated();

        //! Returns the number of invariants.
        static constexpr std::size_t Ninvariants();
    };


} // namespace MoReFEM::HyperelasticLawNS


#include "OperatorInstances/HyperelasticLaws/Impl/Traits.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HPP_
