//! \file


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/Impl/Traits.hpp"


// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/Internal/Traits.hpp"


namespace MoReFEM::HyperelasticLawNS
{


    template<highest_activated_invariant HighestActivatedInvariantT>
    constexpr bool Traits<HighestActivatedInvariantT>::IsI4Activated()
    {
        if constexpr (HighestActivatedInvariantT == highest_activated_invariant::I3)
            return false;
        else
            return true;
    }


    template<highest_activated_invariant HighestActivatedInvariantT>
    constexpr bool Traits<HighestActivatedInvariantT>::IsI6Activated()
    {
        if constexpr (IsI4Activated() == false)
            return false;
        else
        {
            if constexpr (HighestActivatedInvariantT == highest_activated_invariant::I4)
                return false;
            else
                return true;
        }
    }


    template<highest_activated_invariant HighestActivatedInvariantT>
    constexpr std::size_t Traits<HighestActivatedInvariantT>::Ninvariants()
    {
        if constexpr (HighestActivatedInvariantT == highest_activated_invariant::I3)
            return 3ul;
        else if constexpr (HighestActivatedInvariantT == highest_activated_invariant::I4)
            return 4ul;
        else
            return 5ul;
    }


} // namespace MoReFEM::HyperelasticLawNS


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_IMPL_x_TRAITS_HXX_
