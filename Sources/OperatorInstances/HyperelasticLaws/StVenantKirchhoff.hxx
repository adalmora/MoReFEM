/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hpp"


// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("StVenant-Kirchhoff");
        return ret;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::StVenantKirchhoff(const Solid& solid)
    : lame_lambda_(solid.GetLameLambda()), lame_mu_(solid.GetLameMu()), bulk_(solid.GetHyperelasticBulk())
    { }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt) const
    {

        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);
        const double bulk = GetBulk().GetValue(quad_pt, geom_elt);

        return (0.125 * lame_lambda_value + 0.25 * lame_mu_value) * NumericNS::Square(I1)
               - (0.75 * lame_lambda_value + 0.5 * lame_mu_value) * I1 - 0.5 * lame_mu_value * I2
               + 1.125 * lame_lambda_value + 0.75 * lame_mu_value + 0.5 * bulk * NumericNS::Square(I3 - 1.);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);

        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return (0.25 * lame_lambda_value + 0.5 * lame_mu_value) * I1 - (0.75 * lame_lambda_value + 0.5 * lame_mu_value);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        static_cast<void>(invariant_holder);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return -0.5 * lame_mu_value;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double bulk = GetBulk().GetValue(quad_pt, geom_elt);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        return bulk * (I3 - 1.);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        static_cast<void>(invariant_holder);
        const double lame_lambda_value = GetLameLambda().GetValue(quad_pt, geom_elt);
        const double lame_mu_value = GetLameMu().GetValue(quad_pt, geom_elt);

        return (0.25 * lame_lambda_value + 0.5 * lame_mu_value);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);

        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const noexcept
    {
        static_cast<void>(invariant_holder);

        return GetBulk().GetValue(quad_pt, geom_elt);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);

        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);

        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);

        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::GetLameLambda() const noexcept
    {
        return lame_lambda_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::GetLameMu() const noexcept
    {
        return lame_mu_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>::GetBulk() const noexcept
    {
        return bulk_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HXX_
