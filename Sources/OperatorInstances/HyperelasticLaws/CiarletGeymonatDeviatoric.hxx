/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"


// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("Ciarlet-Geymonat");
        return ret;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::CiarletGeymonatDeviatoric(const Solid& solid)
    : kappa1_(solid.GetKappa1()), kappa2_(solid.GetKappa2())
    { }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                                     const QuadraturePoint& quad_pt,
                                                                     const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third
                   * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third
                     * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double one_ninth = 1. / 9.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth
                   * NumericNS::Pow(I3, 7. * minus_one_third, __FILE__, __LINE__)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth
                     * NumericNS::Pow(I3, 8. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third
               * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third
               * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        return GetKappa1().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        return GetKappa2().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -2. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double
    CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::GetKappa1() const noexcept
    {
        return kappa1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    CiarletGeymonatDeviatoric<FiberPolicyT, CoordsPolicyT>::GetKappa2() const noexcept
    {
        return kappa2_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_DEVIATORIC_HXX_
