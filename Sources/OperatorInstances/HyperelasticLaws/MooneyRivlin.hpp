/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HPP_

#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string> // IWYU pragma: keep

#include "Core/Parameter/FiberEnum.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

#include "OperatorInstances/HyperelasticLaws/Impl/Traits.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class Solid; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    /*!
     * \brief MooneyRivlin laws, to use a a policy of class HyperElasticityLaw.
     *
     * W = k1(J1-3) + k2(J2-3) + k(J-1)^2
     */
    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT = FiberNS::AtNodeOrAtQuadPt::at_node,
        CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian
    >
    // clang-format on
    class MooneyRivlin : public Traits<highest_activated_invariant::I3>
    {
      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_self
        using self = MooneyRivlin<FiberPolicyT, CoordsPolicyT>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to traits parent class,
        using traits_parent = Traits<highest_activated_invariant::I3>;

        static_assert(std::is_convertible<self*, traits_parent*>());

        //! Alias on the type of the invariant holder for the current law.
        using invariant_holder_type = InvariantHolder<traits_parent, FiberPolicyT, CoordsPolicyT>;

        //! Alias to fiber policy.
        static constexpr auto fiber_policy = FiberPolicyT;

        //! Alias to coords policy.
        static constexpr auto coords_policy = CoordsPolicyT;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] solid Object which provides the required material parameters for the solid.
         */
        explicit MooneyRivlin(const Solid& solid);

        //! Destructor.
        ~MooneyRivlin() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MooneyRivlin(const MooneyRivlin& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MooneyRivlin(MooneyRivlin&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MooneyRivlin& operator=(const MooneyRivlin& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MooneyRivlin& operator=(MooneyRivlin&& rhs) = delete;

        ///@}

      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const invariant_holder_type& invariant_holder,
                 const QuadraturePoint& quadrature_point,
                 const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of first invariant (dWdI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                              const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of second invariant (dWdI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                              const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first invariant (d2WdI1dI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                                                const QuadraturePoint& quadrature_point,
                                                                const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second invariant (d2WdI2dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                 const QuadraturePoint& quadrature_point,
                                                                 const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quadrature_point,
                                                                         const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                       const QuadraturePoint& quadrature_point,
                                                       const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                        const QuadraturePoint& quadrature_point,
                                                        const GeometricElt& geom_elt) const;

      private:
        //! Kappa1.
        const scalar_parameter& GetKappa1() const noexcept;

        //! Kappa2.
        const scalar_parameter& GetKappa2() const noexcept;

        //! Hyperelastic bulk.
        const scalar_parameter& GetBulk() const noexcept;

      private:
        //! Kappa1.
        const scalar_parameter& kappa1_;

        //! Kappa2.
        const scalar_parameter& kappa2_;

        //! Bulk.
        const scalar_parameter& bulk_;
    };


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/HyperelasticLaws/MooneyRivlin.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_MOONEY_RIVLIN_HPP_
