/*!
//
// \file
//
//
// Created by Philippe Moireau <philippe.moireau@inria.fr> on the Fri, 23 Feb 2018 10:31:15 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_

#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

#include "OperatorInstances/HyperelasticLaws/Impl/Traits.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class Solid; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    /*!
     * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
     */
    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT = FiberNS::AtNodeOrAtQuadPt::at_node,
        CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian
    >
    // clang-format on
    class LogI3Penalization : public Traits<highest_activated_invariant::I3>
    {
      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_self
        using self = LogI3Penalization<FiberPolicyT, CoordsPolicyT>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to traits parent class,
        using traits_parent = Traits<highest_activated_invariant::I3>;

        static_assert(std::is_convertible<self*, traits_parent*>());

        //! Alias on the type of the invariant holder for the current law.
        using invariant_holder_type = InvariantHolder<traits_parent, FiberPolicyT, CoordsPolicyT>;

        //! Alias to fiber policy.
        static constexpr auto fiber_policy = FiberPolicyT;

        //! Alias to coords policy.
        static constexpr auto coords_policy = CoordsPolicyT;

      public:
        //! Number of invariants of the law.
        static constexpr std::size_t Ninvariants()
        {
            return 3;
        }

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] solid Object which provides the required material parameters for the solid.
         */
        explicit LogI3Penalization(const Solid& solid);

        //! Destructor.
        ~LogI3Penalization() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LogI3Penalization(const LogI3Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LogI3Penalization(LogI3Penalization&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LogI3Penalization& operator=(const LogI3Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LogI3Penalization& operator=(LogI3Penalization&& rhs) = delete;

        ///@}

      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const invariant_holder_type& invariant_holder,
                 const QuadraturePoint& quadrature_point,
                 const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                              const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;


      public:
        //! Hyperelastic bulk.
        const scalar_parameter& GetBulk() const noexcept;

      private:
        //! Bulk.
        const scalar_parameter& bulk_;
    };


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HPP_
