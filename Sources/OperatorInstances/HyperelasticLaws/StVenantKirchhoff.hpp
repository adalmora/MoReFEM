/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string> // IWYU pragma: keep

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/InvariantHolder.hpp"

#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/HyperelasticLaws/Impl/Traits.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class Solid; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{

    /*!
     * \brief StVenant-Kirchhoff laws, to use a a policy of class HyperElasticityLaw.
     *
     * Note: this class defines the St venant Kirchhoff laws using invariants, thus complying with the generic
     * interface of Model class. However, these laws could be defined without those invariants, and
     * this definition would be probably better in term of calculation time. So if Saint Venant Kirchhoff laws are
     * to be used extensively, the possibility to implement them without invariants should remain open.
     */
    // clang-format off
    template
    <
        FiberNS::AtNodeOrAtQuadPt FiberPolicyT = FiberNS::AtNodeOrAtQuadPt::at_node,
        CoordsNS::CoordsPolicy CoordsPolicyT = CoordsNS::CoordsPolicy::cartesian
    >
    // clang-format on
    class StVenantKirchhoff : public Traits<highest_activated_invariant::I3>
    {

      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_alias_self
        using self = StVenantKirchhoff<FiberPolicyT, CoordsPolicyT>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter = ScalarParameter<ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to traits parent class,
        using traits_parent = Traits<highest_activated_invariant::I3>;

        static_assert(std::is_convertible<self*, traits_parent*>());

        //! Alias on the type of the invariant holder for the current law.
        using invariant_holder_type = InvariantHolder<traits_parent, FiberPolicyT, CoordsPolicyT>;

        //! Alias to fiber policy.
        static constexpr auto fiber_policy = FiberPolicyT;

        //! Alias to coords policy.
        static constexpr auto coords_policy = CoordsPolicyT;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] solid Object which provides the required material parameters for the solid.
         */
        explicit StVenantKirchhoff(const Solid& solid);

        //! Destructor.
        ~StVenantKirchhoff() = default;

        //! \copydoc doxygen_hide_copy_constructor
        StVenantKirchhoff(const StVenantKirchhoff& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        StVenantKirchhoff(StVenantKirchhoff&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        StVenantKirchhoff& operator=(const StVenantKirchhoff& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        StVenantKirchhoff& operator=(StVenantKirchhoff&& rhs) = delete;

        ///@}

      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const invariant_holder_type& invariant_holder,
                 const QuadraturePoint& quadrature_point,
                 const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of first invariant (dWdI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                              const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of second invariant (dWdI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                              const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of first invariant (d2WdI1dI1)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWFirstInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of second invariant (d2WdI2dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                 const QuadraturePoint& quadrature_point,
                                                                 const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWThirdInvariant(const invariant_holder_type& invariant_holder,
                                               const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const noexcept;

        //! Second derivative of W with respect of first and second invariant (d2WdI1dI2)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWFirstAndSecondInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quadrature_point,
                                                                         const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of first and third invariant (d2WdI1dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWFirstAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                        const QuadraturePoint& quadrature_point,
                                                                        const GeometricElt& geom_elt) noexcept;

        //! Second derivative of W with respect of second and third invariant (d2WdI2dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        static constexpr double SecondDerivativeWSecondAndThirdInvariant(const invariant_holder_type& invariant_holder,
                                                                         const QuadraturePoint& quadrature_point,
                                                                         const GeometricElt& geom_elt) noexcept;

      protected:
        //! Lame coefficient lambda.
        const scalar_parameter& GetLameLambda() const noexcept;

        //! Lame coefficient mu.
        const scalar_parameter& GetLameMu() const noexcept;

        //! Hyperelastic bulk.
        const scalar_parameter& GetBulk() const noexcept;


      private:
        //! Lame coefficient lambda.
        const scalar_parameter& lame_lambda_;

        //! Lame coefficient mu.
        const scalar_parameter& lame_mu_;

        //! Bulk.
        const scalar_parameter& bulk_;
    };


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/HyperelasticLaws/StVenantKirchhoff.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_ST_VENANT_KIRCHHOFF_HPP_
