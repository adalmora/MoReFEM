/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonat.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("Ciarlet-Geymonat");
        return ret;
    }

    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::CiarletGeymonat(const Solid& solid)
    : kappa1_(solid.GetKappa1()), kappa2_(solid.GetKappa2()), bulk_(solid.GetHyperelasticBulk())
    { }

    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                           const QuadraturePoint& quad_pt,
                                                           const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double sqrt_I3 = std::sqrt(I3);

        return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
               + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
    }

    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third
                   * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third
                     * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__)
               + GetBulk().GetValue(quad_pt, geom_elt)
                     * (0.5 * NumericNS::Pow(I3, -0.5, __FILE__, __LINE__) - 0.5 / I3);
    }

    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double one_ninth = 1. / 9.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth
                   * NumericNS::Pow(I3, 7. * minus_one_third, __FILE__, __LINE__)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth
                     * NumericNS::Pow(I3, 8. * minus_one_third, __FILE__, __LINE__)
               + GetBulk().GetValue(quad_pt, geom_elt)
                     * (-0.25 * NumericNS::Pow(I3, -1.5, __FILE__, __LINE__)
                        + 0.5 * NumericNS::Pow(I3, -2., __FILE__, __LINE__));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third
               * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third
               * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        return GetKappa1().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        return GetKappa2().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -2. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::GetKappa1() const noexcept
    {
        return kappa1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::GetKappa2() const noexcept
    {
        return kappa2_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    CiarletGeymonat<FiberPolicyT, CoordsPolicyT>::GetBulk() const noexcept
    {
        return bulk_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_CIARLET_GEYMONAT_HXX_
