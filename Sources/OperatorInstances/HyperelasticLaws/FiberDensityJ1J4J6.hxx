/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 30 Jan 2019 11:51:45 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_FIBER_DENSITY_J1_J4_J6_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_FIBER_DENSITY_J1_J4_J6_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/FiberDensityJ1J4J6.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("FiberDensityJ1J4J6");
        return ret;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FiberDensityJ1J4J6(
        const Solid& solid,
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI4,
        const FiberList<FiberPolicyT, ParameterNS::Type::scalar>* fiber_density_I4,
        const FiberList<FiberPolicyT, ParameterNS::Type::vector>* fibersI6,
        const FiberList<FiberPolicyT, ParameterNS::Type::scalar>* fiber_density_I6)
    : mu_1_(solid.GetMu1()), mu_2_(solid.GetMu2()), c_0_(solid.GetC0()), c_1_(solid.GetC1()), c_2_(solid.GetC2()),
      c_3_(solid.GetC3()), c_4_(solid.GetC4()), c_5_(solid.GetC5()), bulk_(solid.GetHyperelasticBulk()),
      fibers_I4_(fibersI4), fiber_density_I4_(fiber_density_I4), fibers_I6_(fibersI6),
      fiber_density_I6_(fiber_density_I6)
    { }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                              const QuadraturePoint& quad_pt,
                                                              const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double sqrt_I3 = std::sqrt(I3);
        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 =
            std::exp(GetC5().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return GetMu1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetMu2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.)
               + GetC0().GetValue(quad_pt, geom_elt) * expI1
               + fiber_density_I4 * GetC2().GetValue(quad_pt, geom_elt) * expI4
               + fiber_density_I6 * GetC4().GetValue(quad_pt, geom_elt) * expI6
               + GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Square(I3_pow_minus_one_third) * (I1 - 3. * I3_pow_one_third) * expI1
               + mu1 * I3_pow_minus_one_third;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * NumericNS::Pow(I3, -2. / 3., __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_third = 2. / 3.;

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);
        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);
        const double kappa = GetBulk().GetValue(quad_pt, geom_elt);

        const double expI1 =
            std::exp(GetC1().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 =
            std::exp(GetC3().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 =
            std::exp(GetC5().GetValue(quad_pt, geom_elt) * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return -two_third * C0 * C1 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__) * I1
                   * (I1 - 3. * I3_pow_one_third) * expI1
               + fiber_density_I4 * two_third * C2 * C3 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__)
                     * I4 * (I3_pow_one_third - I4) * expI4
               + fiber_density_I6 * two_third * C4 * C5 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__)
                     * I6 * (I3_pow_one_third - I6) * expI6
               + mu1 * minus_one_third * I1 * NumericNS::Pow(I3, 4. * minus_one_third, __FILE__, __LINE__)
               + 2. * mu2 * minus_one_third * I2 * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__)
               + kappa * (0.5 * NumericNS::Pow(I3, -0.5, __FILE__, __LINE__) - 0.5 / I3);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return -2. * fiber_density_I4 * C2 * C3 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I4)
               * expI4;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWSixthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return -2. * fiber_density_I6 * C4 * C5 * NumericNS::Square(I3_pow_minus_one_third) * (I3_pow_one_third - I6)
               * expI6;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3., __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return 2. * C0 * C1 * NumericNS::Pow(I3, -4. / 3., __FILE__, __LINE__)
               * (2. * C1 * NumericNS::Square(I1 - 3. * I3_pow_one_third) + NumericNS::Square(I3_pow_one_third))
               * expI1;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I2 = invariant_holder.GetInvariant(InvariantNS::index::I2);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double two_ninth = 2. / 9.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);
        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);
        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);
        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));
        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));
        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);
        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return two_ninth * NumericNS::Pow(I3, -10. / 3., __FILE__, __LINE__)
                   * (C0 * C1 * expI1 * I1
                          * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                             + 5. * I1 * NumericNS::Square(I3_pow_one_third) - 12. * I3)
                      + fiber_density_I4 * C2 * C3 * expI4 * I4
                            * (-4. * I3 + (5. + 2. * C3) * I4 * NumericNS::Square(I3_pow_one_third)
                               - 4. * C3 * I3_pow_one_third * NumericNS::Square(I4) + 2. * C3 * NumericNS::Cube(I4))
                      + fiber_density_I6 * C4 * C5 * expI6 * I6
                            * (-4. * I3 + (5. + 2. * C5) * I6 * NumericNS::Square(I3_pow_one_third)
                               - 4. * C5 * I3_pow_one_third * NumericNS::Square(I6) + 2. * C5 * NumericNS::Cube(I6)))
               + 4. / 9. * mu1 * I1 * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__)
               + 10. / 9. * mu2 * I2 * NumericNS::Pow(I3, -8. / 3., __FILE__, __LINE__)
               + GetBulk().GetValue(quad_pt, geom_elt)
                     * (0.5 * NumericNS::Pow(I3, -2., __FILE__, __LINE__)
                        - 0.25 * NumericNS::Pow(I3, -1.5, __FILE__, __LINE__));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return 2. * fiber_density_I4 * C2 * C3 * NumericNS::Pow(I3, 4 * minus_one_third, __FILE__, __LINE__) * expI4
               * ((1. + 2. * C3) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__)
                  - 4. * C3 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I4 + 2. * C3 * NumericNS::Square(I4));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSixthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);

        return 2. * fiber_density_I6 * C4 * C5 * NumericNS::Pow(I3, 4 * minus_one_third, __FILE__, __LINE__) * expI6
               * ((1. + 2. * C5) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__)
                  - 4. * C5 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I6 + 2. * C5 * NumericNS::Square(I6));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder.GetInvariant(InvariantNS::index::I1);
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        constexpr const double minus_one_third = -1. / 3.;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double I3_pow_one_third = NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__);

        constexpr const double minus_two_third = -2. / 3.;

        const double mu1 = GetMu1().GetValue(quad_pt, geom_elt);
        const double C0 = GetC0().GetValue(quad_pt, geom_elt);
        const double C1 = GetC1().GetValue(quad_pt, geom_elt);

        const double expI1 = std::exp(C1 * (NumericNS::Square(I1 * I3_pow_minus_one_third - 3.)));

        return minus_two_third * C0 * C1 * expI1 * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__)
                   * (2. * C1 * I1 * NumericNS::Square(I1 - 3. * I3_pow_one_third)
                      + 2. * I1 * NumericNS::Square(I3_pow_one_third) - 3. * I3)
               - 1. / 3. * mu1 * NumericNS::Pow(I3, 2. * minus_two_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        constexpr const double minus_one_third = -1. / 3.;
        const double mu2 = GetMu2().GetValue(quad_pt, geom_elt);

        return mu2 * 2. * minus_one_third * NumericNS::Pow(I3, 5. * minus_one_third, __FILE__, __LINE__);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I4 = invariant_holder.GetInvariant(InvariantNS::index::I4);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double minus_two_third = 2. * minus_one_third;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C2 = GetC2().GetValue(quad_pt, geom_elt);
        const double C3 = GetC3().GetValue(quad_pt, geom_elt);

        const double expI4 = std::exp(C3 * (NumericNS::Square(I4 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I4 = GetFiberDensityI4()->GetValue(quad_pt, geom_elt);

        return minus_two_third * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__) * fiber_density_I4 * C2 * C3 * expI4
               * (-I3 + 2. * (1. + C3) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__) * I4
                  - 4. * C3 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I4 * I4
                  + 2. * C3 * NumericNS::Cube(I4));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdAndSixthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double I6 = invariant_holder.GetInvariant(InvariantNS::index::I6);

        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double minus_two_third = 2. * minus_one_third;

        const double I3_pow_minus_one_third = NumericNS::Pow(I3, minus_one_third, __FILE__, __LINE__);

        const double C4 = GetC4().GetValue(quad_pt, geom_elt);
        const double C5 = GetC5().GetValue(quad_pt, geom_elt);

        const double expI6 = std::exp(C5 * (NumericNS::Square(I6 * I3_pow_minus_one_third - 1.)));

        const auto& fiber_density_I6 = GetFiberDensityI6()->GetValue(quad_pt, geom_elt);


        return minus_two_third * NumericNS::Pow(I3, -7. / 3., __FILE__, __LINE__) * fiber_density_I6 * C4 * C5 * expI6
               * (-I3 + 2. * (1. + C5) * NumericNS::Pow(I3, 2. / 3., __FILE__, __LINE__) * I6
                  - 4. * C5 * NumericNS::Pow(I3, 1. / 3., __FILE__, __LINE__) * I6 * I6
                  + 2. * C5 * NumericNS::Cube(I6));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSecondInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndFourthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWFirstAndSixthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline constexpr double FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWSecondAndSixthInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);
        static_cast<void>(invariant_holder);
        return 0.;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetMu1() const noexcept
    {
        return mu_1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetMu2() const noexcept
    {
        return mu_2_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC0() const noexcept
    {
        return c_0_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC1() const noexcept
    {
        return c_1_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC2() const noexcept
    {
        return c_2_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC3() const noexcept
    {
        return c_3_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC4() const noexcept
    {
        return c_4_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetC5() const noexcept
    {
        return c_5_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetBulk() const noexcept
    {
        return bulk_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const FiberList<FiberPolicyT, ParameterNS::Type::vector>*
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetFibersI4() const noexcept
    {
        return fibers_I4_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const FiberList<FiberPolicyT, ParameterNS::Type::scalar>*
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetFiberDensityI4() const noexcept
    {
        return fiber_density_I4_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const FiberList<FiberPolicyT, ParameterNS::Type::vector>*
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetFibersI6() const noexcept
    {
        return fibers_I6_;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const FiberList<FiberPolicyT, ParameterNS::Type::scalar>*
    FiberDensityJ1J4J6<FiberPolicyT, CoordsPolicyT>::GetFiberDensityI6() const noexcept
    {
        return fiber_density_I6_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_FIBER_DENSITY_J1_J4_J6_HXX_
