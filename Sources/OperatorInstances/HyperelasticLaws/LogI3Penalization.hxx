/*!
//
// \file
//
//
// Created by Philippe Moireau <philippe.moireau@inria.fr> on the Fri, 23 Feb 2018 10:31:15 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"


// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/LogI3Penalization<FiberPolicyT, CoordsPolicyT>.hpp"


namespace MoReFEM::HyperelasticLawNS
{


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    const std::string& LogI3Penalization<FiberPolicyT, CoordsPolicyT>::ClassName()
    {
        static std::string ret("Ciarlet-Geymonat");
        return ret;
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    LogI3Penalization<FiberPolicyT, CoordsPolicyT>::LogI3Penalization(const Solid& solid)
    : bulk_(solid.GetHyperelasticBulk())
    { }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double LogI3Penalization<FiberPolicyT, CoordsPolicyT>::W(const invariant_holder_type& invariant_holder,
                                                             const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);
        const double sqrt_I3 = std::sqrt(I3);

        return GetBulk().GetValue(quad_pt, geom_elt) * (sqrt_I3 - 1. - std::log(sqrt_I3));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double LogI3Penalization<FiberPolicyT, CoordsPolicyT>::FirstDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        return GetBulk().GetValue(quad_pt, geom_elt) * (0.5 * NumericNS::Pow(I3, -0.5, __FILE__, __LINE__) - 0.5 / I3);
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    double LogI3Penalization<FiberPolicyT, CoordsPolicyT>::SecondDerivativeWThirdInvariant(
        const invariant_holder_type& invariant_holder,
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder.GetInvariant(InvariantNS::index::I3);

        return GetBulk().GetValue(quad_pt, geom_elt)
               * (-0.25 * NumericNS::Pow(I3, -1.5, __FILE__, __LINE__)
                  + 0.5 * NumericNS::Pow(I3, -2., __FILE__, __LINE__));
    }


    template<FiberNS::AtNodeOrAtQuadPt FiberPolicyT, CoordsNS::CoordsPolicy CoordsPolicyT>
    inline const typename LogI3Penalization<FiberPolicyT, CoordsPolicyT>::scalar_parameter&
    LogI3Penalization<FiberPolicyT, CoordsPolicyT>::GetBulk() const noexcept
    {
        return bulk_;
    }


} // namespace MoReFEM::HyperelasticLawNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_HYPERELASTIC_LAWS_x_LOG_I3_PENALIZATION_HXX_
