/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Tue, 30 Jun 2015 17:06:15 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Instantiations/MitchellSchaeffer.hpp"


#include <cassert>
#include <memory>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Core/InputData/Instances/InitialConditionGate.hpp"
#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp" // IWYU pragma: keep
#include "Parameters/ParameterAtQuadraturePoint.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }
namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class TimeManager; }
namespace MoReFEM::Advanced::ReactionLawNS { enum class ReactionLawName; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ReactionLawNS
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    ReactionLaw<ReactionLawName::MitchellSchaeffer>::ReactionLaw(
        const MoReFEMDataT& morefem_data,
        const Domain& domain,
        const TimeManager& time_manager,
        const QuadratureRulePerTopology& default_quadrature_rule_set)
    : tau_in_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::TauIn>(morefem_data)),
      tau_out_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::TauOut>(morefem_data)),
      tau_open_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::TauOpen>(morefem_data)),
      u_gate_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::PotentialGate>(morefem_data)),
      u_min_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::PotentialMin>(morefem_data)),
      u_max_(::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<input_data_ms::PotentialMax>(morefem_data)),
      time_manager_(time_manager)
    {
        using InitialConditionGate = ::MoReFEM::InputDataNS::InitialConditionGate;

        const double initial_condition_gate =
            ::MoReFEM::InputDataNS::ExtractLeafFromInputDataOrModelSettings<InitialConditionGate::Value>(morefem_data);

        gate_ = std::make_unique<ScalarParameterAtQuadPt>(
            "Gate", domain, default_quadrature_rule_set, initial_condition_gate, this->GetTimeManager());

        tau_close_ = InitScalarParameterFromInputData<input_data_ms::TauClose>("Tau Close", domain, morefem_data);
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetLocalPotential() const noexcept
    {
        return local_potential_;
    }


    inline double& ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetNonCstLocalPotential() noexcept
    {
        return local_potential_;
    }


    inline const TimeManager& ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauIn() const noexcept
    {
        return tau_in_;
    }

    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauOut() const noexcept
    {
        return tau_out_;
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauOpen() const noexcept
    {
        return tau_open_;
    }


    inline const ReactionLaw<ReactionLawName::MitchellSchaeffer>::scalar_parameter&
    ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetTauClose() const
    {
        assert(!(!tau_close_));
        return *tau_close_;
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUGate() const noexcept
    {
        return u_gate_;
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUMin() const noexcept
    {
        return u_min_;
    }


    inline double ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetUMax() const noexcept
    {
        return u_max_;
    }

    inline ReactionLaw<ReactionLawName::MitchellSchaeffer>::ScalarParameterAtQuadPt&
    ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetNonCstGate() noexcept
    {
        return const_cast<ParameterAtQuadraturePoint<ParameterNS::Type::scalar>&>(this->GetGate());
    }


    inline const ReactionLaw<ReactionLawName::MitchellSchaeffer>::ScalarParameterAtQuadPt&
    ReactionLaw<ReactionLawName::MitchellSchaeffer>::GetGate() const noexcept
    {
        assert(!(!gate_));
        return *gate_;
    }


} // namespace MoReFEM::Advanced::ReactionLawNS

/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_LINEAR_FORM_x_LOCAL_x_NON_LINEAR_SOURCE_x_REACTION_LAW_x_INSTANTIATIONS_x_MITCHELL_SCHAEFFER_HXX_
