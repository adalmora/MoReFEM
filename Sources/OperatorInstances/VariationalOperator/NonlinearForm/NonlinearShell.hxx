/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_SHELL_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_SHELL_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearShell.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::NonlinearShell(
        const FEltSpace& felt_space,
        Unknown::const_shared_ptr unknown_list,
        Unknown::const_shared_ptr test_unknown_list,
        const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             std::move(quadrature_rule_per_topology),
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             hyperelastic_law)
    {
        assert(unknown_list->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list->GetNature() == UnknownNS::Nature::vectorial);
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    const std::string& NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::ClassName()
    {
        static std::string name("NonlinearShell");
        return name;
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    template<class LinearAlgebraTupleT>
    inline void
    NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                                        const GlobalVector& input_vector,
                                                                        const Domain& domain) const
    {
        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<class HyperelasticityPolicyT, class TyingPointPolicyT>
    template<class LocalOperatorTypeT>
    inline void NonlinearShell<HyperelasticityPolicyT, TyingPointPolicyT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<const GlobalVector&>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(),
                              std::get<0>(additional_arguments),
                              local_operator.GetNonCstFormerLocalDisplacement());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_NONLINEAR_SHELL_HXX_
