/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_NONE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_NONE_HPP_

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/None/None.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \class doxygen_none_shell_policy
     *
     * \brief Tying point policy without tying points. With this policy the NonlinearShell Operator is actually
     *  equivalent to the SecondPiolaKirchhoff Operator.
     *
     */


    //! \copydoc doxygen_none_shell_policy
    class None
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::None;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = None;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit None() = default;

        //! Destructor.
        ~None() = default;

        //! \copydoc doxygen_hide_copy_constructor
        None(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        None(None&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        None& operator=(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        None& operator=(None&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_NONE_HPP_
