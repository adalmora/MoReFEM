/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C9_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C9_HPP_


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC9/MITC9.hpp"

namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \class doxygen_mitc9_shell_policy
     *
     * \brief Tying point policy for MITC9 elements.
     *
     */


    //! \copydoc doxygen_mitc9_shell_policy
    class MITC9
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::MITC9;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MITC9;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit MITC9() = default;

        //! Destructor.
        ~MITC9() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MITC9(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MITC9(MITC9&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MITC9& operator=(const MITC9& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MITC9& operator=(MITC9&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C9_HPP_
