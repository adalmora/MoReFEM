/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Thu, 14 Jan 2016 12:00:52 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C4_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C4_HPP_


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC4/MITC4.hpp"

namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \class doxygen_mitc4_shell_policy
     *
     * \brief Tying point policy for MITC4 elements.
     *
     */


    //! \copydoc doxygen_mitc4_shell_policy
    class MITC4
    {
      public:
        //! \copydoc doxygen_hide_gvo_local_policy_alias
        using local_policy = MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::MITC4;

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MITC4;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit MITC4() = default;

        //! Destructor.
        ~MITC4() = default;

        //! \copydoc doxygen_hide_copy_constructor
        MITC4(const MITC4& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MITC4(MITC4&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MITC4& operator=(const MITC4& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MITC4& operator=(MITC4&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_SHELL_TYING_POINTS_POLICY_x_M_I_T_C4_HPP_
