/*!
//
// \file
//
//
// Created by Gautier Bureau <gautier.bureau@inria.fr> on the Wed, 13 Jan 2016 11:18:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"

#include <cstddef> // IWYU pragma: keep

#include "Operators/LocalVariationalOperator/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{


    template<class HyperelasticLawT>
    Hyperelasticity<HyperelasticLawT>::Hyperelasticity(const std::size_t mesh_dimension,
                                                       const HyperelasticLawT* const hyperelastic_law)
    : hyperelastic_law_(hyperelastic_law)
    {
        switch (mesh_dimension)
        {
        case 1u:
            GetNonCstWorkMatrixOuterProduct().resize({ 1, 1 });
            break;
        case 2u:
            GetNonCstWorkMatrixOuterProduct().resize({ 3, 3 });
            break;
        case 3u:
            GetNonCstWorkMatrixOuterProduct().resize({ 6, 6 });
            break;
        default:
            assert(false);
            break;
        }

        invariant_holder_ = std::make_unique<invariant_holder_type>(
            mesh_dimension, InvariantNS::Content::invariants_and_first_and_second_deriv);

        if constexpr (HyperelasticLawT::IsI4Activated())
            invariant_holder_->SetFibersI4(hyperelastic_law_->GetFibersI4());

        if constexpr (HyperelasticLawT::IsI6Activated())
            invariant_holder_->SetFibersI6(hyperelastic_law_->GetFibersI6());
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(const QuadraturePoint& quad_pt,
                                                              const GeometricElt& geom_elt,
                                                              const Advanced::RefFEltInLocalOperator& ref_felt,
                                                              const LocalVector& cauchy_green_tensor_value,
                                                              LocalVector& dW,
                                                              LocalMatrix& d2W)
    {
        static_cast<void>(ref_felt);

        auto& invariant_holder = this->GetNonCstInvariantHolder();
        invariant_holder.UpdateCartesian(cauchy_green_tensor_value, quad_pt, geom_elt);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeGeneralizedWDerivates(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalMatrix& cauchy_green_as_vector,
        const LocalMatrix& contravariant_metric_as_vector,
        const LocalMatrix& transposed_contravariant_metric_as_vector,
        const double contravariant_metric_tensor_determinant,
        const LocalMatrix& contravariant_basis,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        static_cast<void>(ref_felt);

        auto& invariant_holder = this->GetNonCstInvariantHolder();

        invariant_holder.UpdateGeneralized(cauchy_green_as_vector,
                                           contravariant_metric_as_vector,
                                           transposed_contravariant_metric_as_vector,
                                           contravariant_metric_tensor_determinant,
                                           contravariant_basis,
                                           quad_pt,
                                           geom_elt);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeChainRule(const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt,
                                                             LocalVector& dW,
                                                             LocalMatrix& d2W)
    {
        auto& invariant_holder = this->GetNonCstInvariantHolder();
        const auto& law = GetHyperelasticLaw();

        const auto& dI1dC =
            invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI1dC);
        const auto& dI2dC =
            invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI2dC);
        const auto& dI3dC =
            invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI3dC);

        // Note: one of these quantities might be infinite... but in this case this infinite (or a resulting
        // nan) will be assembled into a Petsc matrix or vector, which will in turn handle properly
        // the case. See #1317 to know more about it.
        const double dWdI1 = law.FirstDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
        const double dWdI2 = law.FirstDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
        const double dWdI3 = law.FirstDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);

        {
            const auto& d2I2dCdC =
                invariant_holder.GetSecondDerivativeWrtCauchyGreen(InvariantNS::second_derivative_index::d2I2dCdC);
            const auto& d2I3dCdC =
                invariant_holder.GetSecondDerivativeWrtCauchyGreen(InvariantNS::second_derivative_index::d2I3dCdC);

            {
                assert(Wrappers::Xtensor::IsZeroMatrix(d2W));
                // d2I1dCdC = d2I4dCdC = 0
                xt::noalias(d2W) += dWdI2 * d2I2dCdC;
                xt::noalias(d2W) += dWdI3 * d2I3dCdC;
            }

            {
                const double d2WdI1dI1 = law.SecondDerivativeWFirstInvariant(invariant_holder, quad_pt, geom_elt);
                const double d2WdI2dI2 = law.SecondDerivativeWSecondInvariant(invariant_holder, quad_pt, geom_elt);
                const double d2WdI3dI3 = law.SecondDerivativeWThirdInvariant(invariant_holder, quad_pt, geom_elt);

                const double d2WdI1dI2 =
                    law.SecondDerivativeWFirstAndSecondInvariant(invariant_holder, quad_pt, geom_elt);
                const double d2WdI1dI3 =
                    law.SecondDerivativeWFirstAndThirdInvariant(invariant_holder, quad_pt, geom_elt);
                const double d2WdI2dI3 =
                    law.SecondDerivativeWSecondAndThirdInvariant(invariant_holder, quad_pt, geom_elt);

                using namespace Wrappers::Xtensor;

                auto& outer_prod = GetNonCstWorkMatrixOuterProduct();

                xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI1dC);
                xt::noalias(d2W) += d2WdI1dI1 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI2dC);
                xt::noalias(d2W) += d2WdI1dI2 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI3dC);
                xt::noalias(d2W) += d2WdI1dI3 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI1dC);
                xt::noalias(d2W) += d2WdI1dI2 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI2dC);
                xt::noalias(d2W) += d2WdI2dI2 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI3dC);
                xt::noalias(d2W) += d2WdI2dI3 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI1dC);
                xt::noalias(d2W) += d2WdI1dI3 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI2dC);
                xt::noalias(d2W) += d2WdI2dI3 * outer_prod;

                xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI3dC);
                xt::noalias(d2W) += d2WdI3dI3 * outer_prod;

                if constexpr (HyperelasticLawT::IsI4Activated())
                {
                    const auto& dI4dC =
                        invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI4dC);

                    const double d2WdI1dI4 =
                        law.SecondDerivativeWFirstAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
                    const double d2WdI2dI4 =
                        law.SecondDerivativeWSecondAndFourthInvariant(invariant_holder, quad_pt, geom_elt);
                    const double d2WdI3dI4 =
                        law.SecondDerivativeWThirdAndFourthInvariant(invariant_holder, quad_pt, geom_elt);

                    const double d2WdI4dI4 = law.SecondDerivativeWFourthInvariant(invariant_holder, quad_pt, geom_elt);

                    xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI1dC);
                    xt::noalias(d2W) += d2WdI1dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI2dC);
                    xt::noalias(d2W) += d2WdI2dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI3dC);
                    xt::noalias(d2W) += d2WdI3dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI4dC);
                    xt::noalias(d2W) += d2WdI1dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI4dC);
                    xt::noalias(d2W) += d2WdI2dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI4dC);
                    xt::noalias(d2W) += d2WdI3dI4 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI4dC);
                    xt::noalias(d2W) += d2WdI4dI4 * outer_prod;
                }

                if constexpr (HyperelasticLawT::IsI6Activated())
                {
                    const auto& dI6dC =
                        invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI6dC);

                    const double d2WdI1dI6 =
                        law.SecondDerivativeWFirstAndSixthInvariant(invariant_holder, quad_pt, geom_elt);
                    const double d2WdI2dI6 =
                        law.SecondDerivativeWSecondAndSixthInvariant(invariant_holder, quad_pt, geom_elt);
                    const double d2WdI3dI6 =
                        law.SecondDerivativeWThirdAndSixthInvariant(invariant_holder, quad_pt, geom_elt);

                    const double d2WdI6dI6 = law.SecondDerivativeWSixthInvariant(invariant_holder, quad_pt, geom_elt);

                    xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI1dC);
                    xt::noalias(d2W) += d2WdI1dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI2dC);
                    xt::noalias(d2W) += d2WdI2dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI3dC);
                    xt::noalias(d2W) += d2WdI3dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI6dC);
                    xt::noalias(d2W) += d2WdI1dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI6dC);
                    xt::noalias(d2W) += d2WdI2dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI6dC);
                    xt::noalias(d2W) += d2WdI3dI6 * outer_prod;

                    xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI6dC);
                    xt::noalias(d2W) += d2WdI6dI6 * outer_prod;
                }
            }

            d2W *= 4.;
        }


        {
            assert(Wrappers::Xtensor::IsZeroVector(dW));
            xt::noalias(dW) += dWdI1 * dI1dC;
            xt::noalias(dW) += dWdI2 * dI2dC;
            xt::noalias(dW) += dWdI3 * dI3dC;

            if constexpr (HyperelasticLawT::IsI4Activated())
            {
                const double dWdI4 = law.FirstDerivativeWFourthInvariant(invariant_holder, quad_pt, geom_elt);
                const auto& dI4dC =
                    invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI4dC);

                xt::noalias(dW) += dWdI4 * dI4dC;
            }

            if constexpr (HyperelasticLawT::IsI6Activated())
            {
                const double dWdI6 = law.FirstDerivativeWSixthInvariant(invariant_holder, quad_pt, geom_elt);
                const auto& dI6dC =
                    invariant_holder.GetFirstDerivativeWrtCauchyGreen(InvariantNS::first_derivative_index::dI6dC);

                xt::noalias(dW) += dWdI6 * dI6dC;
            }

            dW *= 2.;
        }

#ifndef NDEBUG
        this->GetNonCstInvariantHolder().Reset();
#endif // NDEBUG
    }


    template<class HyperelasticLawT>
    inline const typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&
    Hyperelasticity<HyperelasticLawT>::GetInvariantHolder() const noexcept
    {
        assert(!(!invariant_holder_));
        return *invariant_holder_;
    }


    template<class HyperelasticLawT>
    inline typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&
    Hyperelasticity<HyperelasticLawT>::GetNonCstInvariantHolder() noexcept
    {
        return const_cast<typename Hyperelasticity<HyperelasticLawT>::invariant_holder_type&>(GetInvariantHolder());
    }


    template<class HyperelasticLawT>
    inline LocalMatrix& Hyperelasticity<HyperelasticLawT>::GetNonCstWorkMatrixOuterProduct() noexcept
    {
        return work_matrix_outer_product_;
    }


    template<class HyperelasticLawT>
    inline const HyperelasticLawT& Hyperelasticity<HyperelasticLawT>::GetHyperelasticLaw() const noexcept
    {
        assert(!(!hyperelastic_law_));
        return *hyperelastic_law_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_x_HYPERELASTICITY_POLICY_x_HYPERELASTICITY_HXX_
