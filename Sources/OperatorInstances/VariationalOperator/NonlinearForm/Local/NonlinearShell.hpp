/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_SHELL_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_SHELL_HPP_

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Parameters/Parameter.hpp"

#include "ParameterInstances/Fiber/FiberList.hpp"
#include "ParameterInstances/Fiber/FiberListManager.hpp"

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"
#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"
#include "Operators/LocalVariationalOperator/NonlinearLocalVariationalOperator.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/Helper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC9/MITC9.hpp"

namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a NonlinearShell. Look for the specific documentation in Documentation/Operators directory for details.
     */
    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    class NonlinearShell final
    : public HyperelasticityPolicyT,
      public TyingPointsPolicyT,
      public NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>,
      public Crtp::LocalMatrixStorage<NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>, 16ul>,
      public Crtp::LocalVectorStorage<NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>, 2ul>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to variational operator parent.
        using parent = NonlinearLocalVariationalOperator<LocalMatrix, LocalVector>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 16ul>;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 2ul>;

        //! Convenient alias.
        using tying_pt_interpolation_component =
            Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] hyperelastic_law The hyperelastic law if there is an hyperelastic part in the operator,
         * or nullptr otherwise. Example of hyperelasticlaw is MoReFEM::HyperelasticLawNS::CiarletGeymonat.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
         * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit NonlinearShell(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                elementary_data_type&& elementary_data,
                                const typename HyperelasticityPolicyT::law_type* hyperelastic_law);

        //! Destructor.
        virtual ~NonlinearShell() override;

        //! \copydoc doxygen_hide_copy_constructor
        NonlinearShell(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonlinearShell(NonlinearShell&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonlinearShell& operator=(const NonlinearShell& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonlinearShell& operator=(NonlinearShell&& rhs) = delete;

        ///@}

        //! Computes the elementary vector and matrix contributions.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation();

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }

        /*!
         * \brief Constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Reference to the former local displacement.
         */
        const std::vector<double>& GetFormerLocalDisplacement() const noexcept;

        /*!
         * \brief Non constant accessor to the former local displacement required by ComputeEltArray().
         *
         * \return Non constant reference to the former local displacement.
         */
        std::vector<double>& GetNonCstFormerLocalDisplacement() noexcept;

      private:
        /*!
         * \brief Helper function to compute the value of a component the Cauchy Green tensor at the tying points.
         *
         * \param[in] covariant_basis Covariant basis expressed at the tying points.
         * \param[in] displacement_gradient Displacement gradient expressed at the tying points.
         * \param[in] component_pair Both tensorial components considered
         * \return The value of the \a i \a j Cauchy Green component (which i and j for each of item of \a component_pair).
         */
        double ComputeCauchyGreenAtTyingPointHelper(
            const LocalMatrix& covariant_basis,
            const LocalMatrix& displacement_gradient,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& component_pair);

        /*!
         * \brief Computes the metric tensors directly at the quadrature points (used for the Cauchy Green tensor).
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         *
         */
        void ComputeMetricTensorsAtQuadPt(const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data);


        /*!
         * \brief Computes the contravariant basis at the tying points (used for the Green Lagrange tensor).
         *
         *  \param[in] dphi_at_tying_point Gradient of the geometric shape functions at the tying points.
         *
         * \return Work matrix which conntains the covariant basis.
         *
         */
        [[nodiscard]] const LocalMatrix& ComputeCovariantBasisAtTyingPoint(const LocalMatrix& dphi_at_tying_point);

        /*!
         * \brief Computes displacement gradient at a tying point.
         *
         * \param[in] dphi_at_tying_point Gradient of the finite element shape functions at the tying points.
         *
         * \return Work matrix which conntains the computed gradient.
         */
        [[nodiscard]] const LocalMatrix&
        ComputeDisplacementGradientAtTyingPoint(const LocalMatrix& dphi_at_tying_point);


        /*!
         * \brief Updates all local matrices/vectors for the current quadrature point from the list of values
         * obtained at the tying points.
         *
         * \param[in] quad_pt Current quadrature point.
         *
         */
        void PrepareInternalDataFromTyingPtToQuadPt(const QuadraturePoint& quad_pt);

        /*!
         * \brief Computes the Second Piola-Kirchhoff stress tensor and its derivative in generalized coordinates.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] geom_elt \a GeometricElt for which the computation takes place.
         * \param[in] ref_felt Reference finite element considered.
         *
         */
        void ComputeWDerivates(const QuadraturePoint& quad_pt,
                               const GeometricElt& geom_elt,
                               const Advanced::RefFEltInLocalOperator& ref_felt);


        //! Sets the interpolated product of finite element shape function gradients at each quadrature point.
        void InitGradGradArray();

        /*!
         * \brief Updates the diagonal components of the strain tensors (Cauchy Green and Green Lagrange derivative)
         *  at each quadrature point from the tying point data.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] component Component of the strain tensors for which the update is required.
         *
         * \return The value of the updated Cauchy Green component.
         */
        double UpdateDiagonalStrainsFromTyingPt(const QuadraturePoint& quad_pt,
                                                tying_pt_interpolation_component component);

        /*!
         * \brief Updates the extradiagonal components of the strain tensors (Cauchy Green and Green Lagrange derivative)
         *  at each quadrature point from the tying point data.
         *
         * \param[in] quad_pt \a QuadraturePoint considered.
         * \param[in] component Component of the strain tensors for which the update is required.
         *
         * \return The value of the updated Cauchy Green component.
         *
         */
        double UpdateExtraDiagonalStrainsFromTyingPt(const QuadraturePoint& quad_pt,
                                                     tying_pt_interpolation_component component);


        /*!
         * \brief Gets the local matrix array containing the product of gradients, interpolated from tying points at
         * the current quadrature point. Array index stands for a component index.
         *
         * \param[in] quad_pt_index Quadrature point index sought.
         * \return The array of matrices containing the product of gradients.
         *
         */
        const std::array<LocalMatrix, 9>& GetGradGradProductAtQuadPt(std::size_t quad_pt_index) const noexcept;

      private:
        //! Local displacement computed at the former time iteration.
        std::vector<double> former_local_displacement_;

        //! Contains the product of gradients of the Felt Shape functions at each quadrature point.
        std::vector<std::array<LocalMatrix, 9>> grad_grad_list_;

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            d2W,
            displacement_gradient_at_tying_pt,
            covariant_basis,
            contravariant_basis,
            transposed_covariant_basis,
            covariant_metric_tensor,
            contravariant_metric_tensor,
            cauchy_green_as_vector,         // in a (6,1) matrix as we need to transpose and do matrix operations.
            contravariant_metric_as_vector, // in a (6,1) matrix as we need to transpose and do matrix operations.
            transposed_contravariant_metric_as_vector,
            De_dot_test_grad,
            transposed_De_dot_test_grad,
            de_dot_d2W,
            linear_part,
            covariant_basis_at_tying_pt
        };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t {
            dW,
            extended_dW // used for the geometric stiffness contribution
        };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearShell.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_NONLINEAR_SHELL_HPP_
