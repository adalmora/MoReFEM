//! \file
//
//
//  TyingPoint.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 08/12/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"


#include "Utilities/MatrixOrVector.hpp"

#include "FiniteElement/RefFiniteElement/Advanced/RefFEltInLocalOperator.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    template<class LocalCoordsT>
    TyingPoint::TyingPoint(double shape_function_value,
                           LocalCoordsT&& local_coords,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator& ref_felt,
                           const Advanced::RefFEltInLocalOperator& test_ref_felt)
    : TyingPoint(shape_function_value,
                 ComputeGeometricGradientAtLocalCoords(ref_geom_elt, local_coords),
                 ComputeFEltGradientAtLocalCoords(ref_felt.GetBasicRefFElt(), local_coords),
                 ComputeFEltGradientAtLocalCoords(test_ref_felt.GetBasicRefFElt(), local_coords))
    { }


    inline double TyingPoint::GetShapeFunctionValue() const noexcept
    {
        return shape_function_value_;
    }


    inline const LocalMatrix& TyingPoint::GetGeometricGradient() const noexcept
    {
        return dphi_geo_;
    }


    inline const LocalMatrix& TyingPoint::GetFEltGradient() const noexcept
    {
        return dphi_felt_;
    }


    inline const LocalMatrix& TyingPoint::GetTestFEltGradient() const noexcept
    {
        return dphi_test_felt_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_HXX_
