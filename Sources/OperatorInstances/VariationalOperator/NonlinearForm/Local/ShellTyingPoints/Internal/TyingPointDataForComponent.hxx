/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"

#include "Utilities/MatrixOrVector.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    inline const TyingPoint::vector_const_shared_ptr& TyingPointDataForComponent::GetTyingPointList() const noexcept
    {
        return tying_point_list_;
    }


    inline const LocalMatrix& TyingPointDataForComponent::GetGradGradProduct() const noexcept
    {
        return grad_grad_product_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HXX_
