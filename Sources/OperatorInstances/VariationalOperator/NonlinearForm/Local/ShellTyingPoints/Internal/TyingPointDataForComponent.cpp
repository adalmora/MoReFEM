/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    TyingPointDataForComponent::TyingPointDataForComponent(const TyingPoint::vector_const_shared_ptr& tying_point_list,
                                                           LocalMatrix grad_grad_product)
    : tying_point_list_(tying_point_list), grad_grad_product_(grad_grad_product)
    { }


    TyingPointDataForComponent::TyingPointDataForComponent(const TyingPoint::const_shared_ptr& tying_point_ptr,
                                                           LocalMatrix grad_grad_product)
    : TyingPointDataForComponent(TyingPoint::vector_const_shared_ptr{ tying_point_ptr }, grad_grad_product)
    { }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup
