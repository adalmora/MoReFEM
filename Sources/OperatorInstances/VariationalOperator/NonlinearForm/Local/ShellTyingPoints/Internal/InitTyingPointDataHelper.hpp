//! \file
//
//
//  InitTyingPointDataHelper.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/12/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_INIT_TYING_POINT_DATA_HELPER_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_INIT_TYING_POINT_DATA_HELPER_HPP_


#include <array>
#include <cstddef>

#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
#include "Utilities/MatrixOrVector.hpp"

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief An helper class to be used by tying point policies to help fill the internal data.
     *
     *  This class encapsulates many computations that are in fact common to all the three policies available so far (\a
     * None, \a MITC4 and \a MITC9).
     */
    class InitTyingPointDataHelper : public Crtp::LocalMatrixStorage<InitTyingPointDataHelper, 2ul, LocalMatrix>
    {

        //! \copydoc doxygen_hide_alias_self
        using self = InitTyingPointDataHelper;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 2ul>;

        // clang-format off
        //! Convenient alias.
        using data_array_type =
        std::array
        <
            TyingPointDataForComponent::unique_ptr,
            EnumUnderlyingType(Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component::Ncomponents)
        >;
        // clang-format on

      public:
        /*!
         * \brief Constructor
         *
         * \copydoc doxygen_hide_ref_elements_geom_felt
         */
        InitTyingPointDataHelper(const RefGeomElt& ref_geom_elt,
                                 const Advanced::RefFEltInLocalOperator& ref_felt,
                                 const Advanced::RefFEltInLocalOperator& test_ref_felt);

        /*!
         * \brief Set information related to the current \a QuadraturePoint used in the helper class.
         *
         * \param[in] infos_at_quad_pt The \a Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint
         related to the
         * \a QuadraturePoint under scrutiny.
         *
         * Should be called as soon as the \a QuadraturePoint changes; typically the code should looks something like:
         *
         * \code
         Internal::LocalVariationalOperatorNS::TyingPointsNS::InitTyingPointDataHelper helper(ref_geom_elt, ref_felt,
         test_ref_felt);

         for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
         {
             helper.SetInfosAtQuadPoint(infos_at_quad_pt);
                        ...
         }
         \endcode
         */
        void
        SetInfosAtQuadPoint(const Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint& infos_at_quad_pt);


        /*!
         * \brief Fill the data related to the tying point component \a component that requires interpolation.
         *
         * \param[in] component The component for which data is computed; if a non symmetric one (e.g. e_rz) then its symmetric one is computed as
         * well (e.g. e_zr).
         *
         *  \tparam PlaneDataT A struct (typically defined in anonymous namespace - have a look at MITC9 policy for
         * instance) defining data related to the plane in a static way. It musst define:
         *  - static constexpr auto related_tying_point_component = XX; // e.g. e_rz for the RZ plane.
         *  - static const std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert); //
         * do_invert is a trick to avoid defining more planes which have mostly symmetric behaviours (e.g. in MITC9 no
         * SZ plane is defined: it could be but its content is in fact the same as plane RZ except for the ordering).
         *  - static const std::array<std::array<double, 2ul>, N>& GetTyingPointsCoords(); // where N is plane-dependent
         */
        template<class PlaneDataT>
        void InterpolatedCase(tying_pt_interpolation_component component);

        /*!
         * \brief Fill the data related to the tying point component \a component that requires no interpolation.
         *
         * \param[in] component The component for which data is computed.
         */
        void NonInterpolatedComponent(tying_pt_interpolation_component component);

        //! \brief Non-constant accessor to extract the internal data array when computation of its content is done, so that said content may be moved in the
        //! tying point policy storage
        //! \return Non constant reference to the computed data array.
        data_array_type& ExtractDataArray() noexcept;

      private:
        /*!
         * \brief Helper method to \a InterpolatedCase, which covers a computation that may be done twice within one \a InterpolatedCase call
         *
         * \param[in] tying_point_list List of all \a TyingPoint involved in the computation.
         * \param[in] component The component for which data is computed.
         */
        void ComputeInterpolatedComponent(const TyingPoint::vector_const_shared_ptr& tying_point_list,
                                          tying_pt_interpolation_component component);


        /*!
         * \brief Build the list opf \a TyingPoint relevant for the current \a PlaneData.
         *
         * \tparam PlaneDataT The same as given to \a InterpolatedCase (whjch current method is an helper one)
         *
         * \param[in] do_invert If True, swap components read from the \a PlaneDataT. \a do_invert is a trick to avoid defining
         *  more planes which have mostly symmetric behaviours (e.g. in MITC9 no SZ plane is defined: it could be but
         * its content is in fact the same as plane RZ except for the ordering).
         *
         * \return List of \a TyingPoint.
         */
        template<class PlaneDataT>
        TyingPoint::vector_const_shared_ptr BuildTyingPointList(bool do_invert) const;


      private:
        //! The purpose of this class is to  fill properly this data.
        data_array_type data_array_;


        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t { grad_grad_contribution, grad_grad_global };

        ///@}


      protected:
        //! Reference geometric element used.
        const RefGeomElt& GetRefGeomElt() const noexcept;

        //! Reference finite element used for the unknown.
        const Advanced::RefFEltInLocalOperator& GetRefFElt() const noexcept;

        //!  Reference finite element used for the test function.
        const Advanced::RefFEltInLocalOperator& GetTestRefFElt() const noexcept;

        //! \a QuadraturePoint for which the computation is currently done.
        const QuadraturePoint& GetCurrentQuadraturePoint() const noexcept;


      private:
        //! Reference geometric element used.
        const RefGeomElt& ref_geom_elt_;

        //! Reference finite element used for the unknown.
        const Advanced::RefFEltInLocalOperator& ref_felt_;

        //!  Reference finite element used for the test function.
        const Advanced::RefFEltInLocalOperator& test_ref_felt_;

        //! \a QuadraturePoint for which the computation is currently done.
        const QuadraturePoint* current_quad_pt_{ nullptr };
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/InitTyingPointDataHelper.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_INIT_TYING_POINT_DATA_HELPER_HPP_
