//! \file
//
//
//  InitTyingPointDataHelper.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/12/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#include <algorithm>
#include <cassert>
#include <memory>
#include <utility>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/InitTyingPointDataHelper.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    namespace // anonymous
    {


        /*!
         * \brief Helper function to compute the product of the gradients of the finite element shape functions
         *  at the tying points.
         *
         * \param[in] dphi_at_tying_point Gradient of the finite element shape functions for the unknown at a tying point.
         * \param[in] dphi_test_at_tying_point Gradient of the finite element shape functions for the test function at
         *  a the tying point.
         * \param[in] i First tensorial component considered.
         * \param[in] j Second tensorial component considered.
         * \param[in,out] grad_grad_out Local matrix containing the product of gradients.
         *
         */
        void ComputeGradGrad(
            const LocalMatrix& dphi_at_tying_point,
            const LocalMatrix& dphi_test_at_tying_point,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& tensor_component_pair,
            LocalMatrix& grad_grad_out);


    } // namespace


    InitTyingPointDataHelper::InitTyingPointDataHelper(const RefGeomElt& ref_geom_elt,
                                                       const Advanced::RefFEltInLocalOperator& ref_felt,
                                                       const Advanced::RefFEltInLocalOperator& test_ref_felt)
    : ref_geom_elt_(ref_geom_elt), ref_felt_(ref_felt), test_ref_felt_(test_ref_felt)
    {
        const auto& basic_test_ref_felt = test_ref_felt.GetBasicRefFElt();
        const auto Nnode_test = basic_test_ref_felt.NlocalNode();

        const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
        const auto Nnode = basic_ref_felt.NlocalNode();

        matrix_parent::InitLocalMatrixStorage({ {
            { Nnode_test * 3ul, Nnode * 3ul }, // helper_grad_grad
            { Nnode_test * 3ul, Nnode * 3ul }  // work_grad_grad
        } });
    }


    void InitTyingPointDataHelper::NonInterpolatedComponent(tying_pt_interpolation_component component)
    {
        auto quad_pt_as_sole_tying_pt = std::make_shared<TyingPoint>(
            1., GetCurrentQuadraturePoint(), GetRefGeomElt(), GetRefFElt(), GetTestRefFElt());

        assert(!(!quad_pt_as_sole_tying_pt));
        const auto& tying_point = *quad_pt_as_sole_tying_pt;

        const auto component_pair = ComputePair(component);

        auto& grad_grad_global = matrix_parent::GetLocalMatrix<EnumUnderlyingType(
            InitTyingPointDataHelper::LocalMatrixIndex::grad_grad_global)>();

        ComputeGradGrad(
            tying_point.GetFEltGradient(), tying_point.GetTestFEltGradient(), component_pair, grad_grad_global);

        data_array_[EnumUnderlyingType(component)] =
            std::make_unique<TyingPointDataForComponent>(quad_pt_as_sole_tying_pt, grad_grad_global);
    }


    void InitTyingPointDataHelper ::SetInfosAtQuadPoint(
        const Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint& infos_at_quad_pt)
    {
        decltype(auto) quad_pt_unknown_list_data = infos_at_quad_pt.GetUnknownData();
        const auto& quad_pt = quad_pt_unknown_list_data.GetQuadraturePoint();

        current_quad_pt_ = &quad_pt;

#ifndef NDEBUG
        std::fill(data_array_.begin(), data_array_.end(), nullptr);
#endif // NDEBUG
    }


    void
    InitTyingPointDataHelper::ComputeInterpolatedComponent(const TyingPoint::vector_const_shared_ptr& tying_point_list,
                                                           tying_pt_interpolation_component component)
    {
        auto& grad_grad_contribution = matrix_parent::GetLocalMatrix<EnumUnderlyingType(
            InitTyingPointDataHelper::LocalMatrixIndex::grad_grad_contribution)>();
        auto& grad_grad_global = matrix_parent::GetLocalMatrix<EnumUnderlyingType(
            InitTyingPointDataHelper::LocalMatrixIndex::grad_grad_global)>();

        grad_grad_contribution.fill(0.);
        grad_grad_global.fill(0.);

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            ComputeGradGrad(tying_point.GetFEltGradient(),
                            tying_point.GetTestFEltGradient(),
                            ComputePair(component),
                            grad_grad_contribution);

            grad_grad_global += tying_point.GetShapeFunctionValue() * grad_grad_contribution;
        }


        assert(EnumUnderlyingType(component) < data_array_.size());
        data_array_[EnumUnderlyingType(component)] =
            std::make_unique<TyingPointDataForComponent>(tying_point_list, grad_grad_global);
    }


    namespace // anonymous
    {


        void ComputeGradGrad(
            const LocalMatrix& dphi_at_tying_point,
            const LocalMatrix& dphi_test_at_tying_point,
            const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& tensor_component_pair,
            LocalMatrix& grad_grad_out)
        {
            grad_grad_out.fill(0.);

            const auto Nnode = dphi_at_tying_point.shape(0);
            const auto Nnode_test = dphi_test_at_tying_point.shape(0);
            constexpr auto Ndimension = 3u;
            assert(dphi_at_tying_point.shape(1) == Ndimension);
            assert(grad_grad_out.shape(0) == 3 * Nnode_test && grad_grad_out.shape(1) == 3 * Nnode);

            const auto i = EnumUnderlyingType(tensor_component_pair.first);
            const auto j = EnumUnderlyingType(tensor_component_pair.second);

            for (auto k = 0u; k < Ndimension; ++k)
            {
                for (auto row_node = 0u; row_node < Nnode_test; ++row_node)
                {
                    for (auto col_node = 0u; col_node < Nnode; ++col_node)
                    {
                        grad_grad_out(row_node + k * Nnode_test, col_node + k * Nnode) =
                            dphi_test_at_tying_point(row_node, i) * dphi_at_tying_point(col_node, j);
                    }
                }
            }
        }
    } // namespace

} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
