//! \file
//
//
//  Enum.hpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 06/12/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_ENUM_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_ENUM_HPP_


#include <cstddef>
#include <utility>


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Enum class to fetch data for each component. Note that for interpolation rules that do not use every component,
     * we define their internal data arrays directly at the quadrature points.
     *
     */
    enum class tying_pt_interpolation_component : std::size_t {
        e_rr,
        e_ss,
        e_zz,
        e_rs,
        e_sz,
        e_rz,
        e_sr,
        e_zs,
        e_zr,
        Ncomponents,
        none // placeholder for internal functionalities
    };


    /*!
     * \brief Returns the 'symmetric' component of \a component, e.g. \a tying_pt_interpolation_component::e_sz for \a component =
     * \a tying_pt_interpolation_component::e_zs.
     *
     * \param[in] component Component which symmetric is sought.
     *
     * If the component is itself symmetric, by convention this function returns \a
     * tying_pt_interpolation_component::none.
     *
     * \return The symmetric component if \a component is not symmetric already, or \a tying_pt_interpolation_component::none otherwise.
     */
    tying_pt_interpolation_component Symmetric(tying_pt_interpolation_component component);

    /*!
     * \brief Returns the 'pair' of symmetric component forf \a component
     *
     * \param[in] component Component under study.
     *
     * The rule to determine the pair is given e_xy (where \a x and \a y in { 'r', 's', 'z' }:
     *
     *  e_xy -> (e_xx, e_yy).
     *
     * This rule is applied as such when \a x = \a y, so for instance:
     *
     * e_rr -> (e_rr, e_rr)
     *
     * \return The pair of symmetric component with the rule described above.
     */
    std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>
    ComputePair(tying_pt_interpolation_component component);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_ENUM_HPP_
