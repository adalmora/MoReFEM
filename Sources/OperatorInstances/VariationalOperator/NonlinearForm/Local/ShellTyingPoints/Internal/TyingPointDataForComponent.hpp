/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>

#include "Utilities/MatrixOrVector.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Class in charge of storing the relevant data used for the interpolation at tying points.
     *
     * There is one TyingPointDataForComponent object for each component and each quadrature point.
     */
    class TyingPointDataForComponent final // \todo #1664 Rename it with a clearer name!
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = TyingPointDataForComponent;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] tying_point_list List of \a TyingPoint list.
         * \param[in] grad_grad_product Product of the gradients of the test functions and the unknown
         *  shape functions directly interpolated from the tying points to the current quadrature point (hence we only
         *  store one matrix at each quadrature point) - or not interpolated if not required by the policy (\a None and
         * some of \a MITC4 components).
         *
         */
        explicit TyingPointDataForComponent(const TyingPoint::vector_const_shared_ptr& tying_point_list,
                                            LocalMatrix grad_grad_product);

        /*!
         * \brief Constructor for non interpolated points.
         *
         *  This one is typically called when there are no interpolation of tying points involved.
         *
         * \param[in] tying_point_list List of \a TyingPoint list.
         * \param[in] grad_grad_product Product of the gradients of the test functions and the unknown
         *  shape functions directly interpolated from the tying points to the current quadrature point (hence we only
         *  store one matrix at each quadrature point) - or not interpolated if not required by the policy (\a None and
         * some of \a MITC4 components).
         *
         */
        explicit TyingPointDataForComponent(const TyingPoint::const_shared_ptr& tying_point_list,
                                            LocalMatrix grad_grad_product);


        //! Destructor.
        ~TyingPointDataForComponent() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TyingPointDataForComponent(const TyingPointDataForComponent& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TyingPointDataForComponent(TyingPointDataForComponent&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        TyingPointDataForComponent& operator=(const TyingPointDataForComponent& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TyingPointDataForComponent& operator=(TyingPointDataForComponent&& rhs) = delete;

        ///@}


      public:
        //! Returns the product of the gradients of shape functions of the unknown and the test functions.
        const LocalMatrix& GetGradGradProduct() const noexcept;

        //! Accessor the the list of \a TyingPoint.
        const TyingPoint::vector_const_shared_ptr& GetTyingPointList() const noexcept;

      private:
        //! List of \a TyingPoint used for the computation.
        TyingPoint::vector_const_shared_ptr tying_point_list_;

        //! Matrix containing the product of the gradients of the test functions and the unknown
        //! shape functions directly interpolated from the tying points to the current quadrature point.
        //! It might be interpolated or not - it is if there is more than 1 item in \a tying_point_list_.
        const LocalMatrix grad_grad_product_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_INTERNAL_x_TYING_POINT_DATA_FOR_COMPONENT_HPP_
