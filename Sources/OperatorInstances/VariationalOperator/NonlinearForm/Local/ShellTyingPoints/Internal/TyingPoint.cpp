//! \file
//
//
//  TyingPoint.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 08/12/2021.
// Copyright © 2021 Inria. All rights reserved.
//

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    TyingPoint::TyingPoint(double shape_function_value,
                           LocalMatrix&& dphi_geo,
                           LocalMatrix&& dphi_felt,
                           LocalMatrix&& dphi_test_felt)
    : shape_function_value_(shape_function_value), dphi_geo_(dphi_geo), dphi_felt_(dphi_felt),
      dphi_test_felt_(dphi_test_felt)
    { }


    LocalMatrix ComputeGeometricGradientAtLocalCoords(const RefGeomElt& ref_geom_elt, const LocalCoords& local_coords)
    {
        const auto Ngeometric_coords = ref_geom_elt.Ncoords();
        const auto Ndimension = ref_geom_elt.GetDimension();
        LocalMatrix work_dphi_geo;
        work_dphi_geo.resize({ Ngeometric_coords, Ndimension });
        work_dphi_geo.fill(0.);

        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index.Get() < Ngeometric_coords;
             ++local_node_index)
        {
            for (Advanced::ComponentNS::index_type component{ 0ul }; component.Get() < Ndimension; ++component)
            {
                work_dphi_geo(local_node_index.Get(), component.Get()) =
                    ref_geom_elt.FirstDerivateShapeFunction(local_node_index, component, local_coords);
            }
        }

        return work_dphi_geo;
    }


    LocalMatrix ComputeFEltGradientAtLocalCoords(const Internal::RefFEltNS::BasicRefFElt& ref_felt,
                                                 const LocalCoords& local_coords)
    {
        const auto Nnode = ref_felt.NlocalNode();
        const auto Ndimension = ref_felt.GetTopologyDimension();

        LocalMatrix work_dphi_felt;
        work_dphi_felt.resize({ Nnode, Ndimension });
        work_dphi_felt.fill(0.);

        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index.Get() < Nnode; ++local_node_index)
        {
            for (Advanced::ComponentNS::index_type component{ 0ul }; component.Get() < Ndimension; ++component)
            {
                work_dphi_felt(local_node_index.Get(), component.Get()) =
                    ref_felt.FirstDerivateShapeFunction(local_node_index, component, local_coords);
            }
        }

        return work_dphi_felt;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
