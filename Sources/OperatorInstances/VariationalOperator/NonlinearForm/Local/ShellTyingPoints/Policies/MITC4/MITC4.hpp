/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_M_I_T_C4_x_M_I_T_C4_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_M_I_T_C4_x_M_I_T_C4_HPP_


#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }
namespace MoReFEM::Advanced::LocalVariationalOperatorNS { class InformationAtQuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    //! \copydoc doxygen_mitc4_shell_policy
    class MITC4 : public Base
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = MITC4;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        //! Constructor which does not initialize the object completely. A call to InitTyingPointData() is required.
        explicit MITC4() = default;

        //! Destructor.
        ~MITC4() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        MITC4(const MITC4& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        MITC4(MITC4&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        MITC4& operator=(const MITC4& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        MITC4& operator=(MITC4&& rhs) = delete;

        ///@}

      public:
        //! \copydoc doxygen_hide_init_point_data_prototype
        void InitTyingPointData(const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
                                    infos_at_quad_pt_list,
                                const RefGeomElt& ref_geom_elt,
                                const Advanced::RefFEltInLocalOperator& ref_felt,
                                const Advanced::RefFEltInLocalOperator& test_ref_felt) override;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_M_I_T_C4_x_M_I_T_C4_HPP_
