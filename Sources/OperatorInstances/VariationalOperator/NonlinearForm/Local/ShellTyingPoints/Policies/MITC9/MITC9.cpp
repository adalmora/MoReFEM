/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Mesh/Mesh.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/InitTyingPointDataHelper.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/MITC9/MITC9.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    namespace // anonymous
    {


        const double one_over_sqrt_three = 1. / std::sqrt(3.);
        const double sqrt_3_over_5 = std::sqrt(3. / 5.);


        struct PlaneRR
        {

            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_rr;

            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RR plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static const std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);

            static const std::array<std::array<double, 2ul>, 6ul>& GetTyingPointsCoords();
        };


        struct PlaneRZ
        {

            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_rz;

            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RR plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static const std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);

            static const std::array<std::array<double, 2ul>, 6ul>& GetTyingPointsCoords();
        };


        struct PlaneRS
        {

            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_rs;

            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RR plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static const std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);

            static const std::array<std::array<double, 2ul>, 4ul>& GetTyingPointsCoords();
        };


        struct PlaneZZ
        {

            static constexpr auto related_tying_point_component = tying_pt_interpolation_component::e_zz;

            /*!
             * \brief Array of the shape functions values for the tying point interpolation in the RR plane.
             *
             * \param[in] r Local coordinates along r.
             * \param[in] s Local coordinates along s.
             * \return Array containing the shape function values.
             */
            static const std::vector<double> ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert);

            static const std::array<std::array<double, 2ul>, 9ul>& GetTyingPointsCoords();
        };


        using tying_pt_interpolation_component =
            ::MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;


    } // namespace


    const std::string& MITC9::ClassName()
    {
        static std::string name("MITC9");
        return name;
    }


    void MITC9::InitTyingPointData(
        const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>& infos_at_quad_pt_list,
        const RefGeomElt& ref_geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const Advanced::RefFEltInLocalOperator& test_ref_felt)
    {

#ifndef NDEBUG
        {
            const auto& basic_ref_felt = ref_felt.GetBasicRefFElt();
            const auto ref_felt_space_dimension = basic_ref_felt.GetTopologyDimension();
            constexpr auto euclidean_dimension = 3ul;
            assert(ref_felt_space_dimension == euclidean_dimension
                   && "This interpolation rule requires volumic elements.");

            const auto Ngeometric_coords = ref_geom_elt.Ncoords();

            assert(basic_ref_felt.NlocalNode() == Ngeometric_coords
                   && "This interpolation rule requires isoparametric elements ");
        }
#endif // NDEBUG

        // Symmetric interpolation for r and s components.
        const auto Nquad_pt = infos_at_quad_pt_list.size();
        auto& mitc_data = GetNonCstMITCData();
        mitc_data.reserve(Nquad_pt);

        Internal::LocalVariationalOperatorNS::TyingPointsNS::InitTyingPointDataHelper helper(
            ref_geom_elt, ref_felt, test_ref_felt);


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            helper.SetInfosAtQuadPoint(infos_at_quad_pt);

            helper.InterpolatedCase<PlaneRR>(tying_pt_interpolation_component::e_rr);
            helper.InterpolatedCase<PlaneRR>(tying_pt_interpolation_component::e_ss);
            helper.InterpolatedCase<PlaneZZ>(tying_pt_interpolation_component::e_zz);
            helper.InterpolatedCase<PlaneRS>(tying_pt_interpolation_component::e_rs);
            helper.InterpolatedCase<PlaneRZ>(tying_pt_interpolation_component::e_sz);
            helper.InterpolatedCase<PlaneRZ>(tying_pt_interpolation_component::e_rz);

            auto& data_array = helper.ExtractDataArray();

            assert(std::none_of(
                data_array.cbegin(), data_array.cend(), Utilities::IsNullptr<TyingPointDataForComponent::unique_ptr>));

            mitc_data.emplace_back(std::move(data_array));
        }
    }


    namespace // anonymous
    {


        const std::vector<double> PlaneRR::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto r = do_invert ? quad_pt.s() : quad_pt.r();
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            const double sqrt_3 = std::sqrt(3.);
            const double sqrt_3_over_3 = sqrt_3 / 3.;
            const double sqrt_15_over_5 = std::sqrt(15.) / 5.;
            constexpr auto one_twelfth = 1. / 12.;
            constexpr auto one_sixth = 1. / 6.;

            std::vector<double> ret{ -(5. * sqrt_3 * s * (r - sqrt_3_over_3) * (s - sqrt_15_over_5)) * one_twelfth,
                                     (5. * sqrt_3 * s * (r + sqrt_3_over_3) * (s - sqrt_15_over_5)) * one_twelfth,
                                     -((sqrt_3 * r + 1.) * (5. * NumericNS::Square(s) - 3.)) * one_sixth,
                                     (5. * sqrt_3 * s * (r + sqrt_3_over_3) * (s + sqrt_15_over_5)) * one_twelfth,
                                     -(5. * sqrt_3 * s * (r - sqrt_3_over_3) * (s + sqrt_15_over_5)) * one_twelfth,
                                     ((sqrt_3 * r - 1.) * (5. * NumericNS::Square(s) - 3.)) * one_sixth };
            return ret;
        }


        const std::array<std::array<double, 2ul>, 6ul>& PlaneRR::GetTyingPointsCoords()
        {
            static std::array<std::array<double, 2ul>, 6ul> ret{ { { { -one_over_sqrt_three, -sqrt_3_over_5 } },
                                                                   { { one_over_sqrt_three, -sqrt_3_over_5 } },
                                                                   { { one_over_sqrt_three, 0. } },
                                                                   { { one_over_sqrt_three, sqrt_3_over_5 } },
                                                                   { { -one_over_sqrt_three, sqrt_3_over_5 } },
                                                                   { { -one_over_sqrt_three, 0. } } } };

            return ret;
        }


        const std::vector<double> PlaneRS::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto r = do_invert ? quad_pt.s() : quad_pt.r();
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            const double sqrt_3_over_3 = std::sqrt(3.) / 3.;

            std::vector<double> ret{ (3. * (r - sqrt_3_over_3) * (s - sqrt_3_over_3)) * .25,
                                     -(3. * (r + sqrt_3_over_3) * (s - sqrt_3_over_3)) * .25,
                                     (3. * (r + sqrt_3_over_3) * (s + sqrt_3_over_3)) * .25,
                                     -(3. * (r - sqrt_3_over_3) * (s + sqrt_3_over_3)) * .25 };
            return ret;
        }


        const std::array<std::array<double, 2ul>, 4ul>& PlaneRS::GetTyingPointsCoords()
        {
            static std::array<std::array<double, 2ul>, 4ul> ret{
                { { { -one_over_sqrt_three, -one_over_sqrt_three } },
                  { { one_over_sqrt_three, -one_over_sqrt_three } },
                  { { one_over_sqrt_three, one_over_sqrt_three } },
                  { { -one_over_sqrt_three, one_over_sqrt_three } } }
            };

            return ret;
        }


        const std::vector<double> PlaneRZ::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto r = do_invert ? quad_pt.s() : quad_pt.r();
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            const double sqrt_3 = std::sqrt(3.);
            const double sqrt_3_over_3 = std::sqrt(3.) / 3.;

            std::vector<double> ret{ -(sqrt_3 * s * (r - sqrt_3_over_3) * (s - 1.)) * .25,
                                     (sqrt_3 * s * (r + sqrt_3_over_3) * (s - 1.)) * .25,
                                     -(sqrt_3 * (r + sqrt_3_over_3) * (s - 1.) * (s + 1.)) * .5,
                                     (sqrt_3 * s * (r + sqrt_3_over_3) * (s + 1.)) * .25,
                                     -(sqrt_3 * s * (r - sqrt_3_over_3) * (s + 1.)) * .25,
                                     (sqrt_3 * (r - sqrt_3_over_3) * (s - 1.) * (s + 1.)) * .5 };
            return ret;
        }


        const std::array<std::array<double, 2ul>, 6ul>& PlaneRZ::GetTyingPointsCoords()
        {
            static std::array<std::array<double, 2ul>, 6ul> ret{ { { -one_over_sqrt_three, -1. },
                                                                   { one_over_sqrt_three, -1. },
                                                                   { one_over_sqrt_three, 0. },
                                                                   { one_over_sqrt_three, 1. },
                                                                   { -one_over_sqrt_three, 1. },
                                                                   { -one_over_sqrt_three, 0. } } };

            return ret;
        }


        const std::vector<double> PlaneZZ::ComputeShapeFunction(const QuadraturePoint& quad_pt, bool do_invert)
        {
            const auto r = do_invert ? quad_pt.s() : quad_pt.r();
            const auto s = do_invert ? quad_pt.r() : quad_pt.s();

            // clang-format off
            std::vector<double> ret
            {
                (r * s * (r - 1.) * (s - 1.)) * .25,
                -(s * (r - 1.) * (r + 1.) * (s - 1.)) * .5,
                (r * s * (r + 1.) * (s - 1.)) * .25,
                -(r * (r + 1.) * (s - 1.) * (s + 1.)) * .5,
                (r * s * (r + 1.) * (s + 1.)) * .25,
                -(s * (r - 1.) * (r + 1.) * (s + 1.)) * .5,
                (r * s * (r - 1.) * (s + 1.)) * .25,
                -(r * (r - 1.) * (s - 1.) * (s + 1.)) * .5,
                (r - 1.) * (r + 1.) * (s - 1.) * (s + 1.)
            };
            // clang-format on

            return ret;
        }


        const std::array<std::array<double, 2ul>, 9ul>& PlaneZZ::GetTyingPointsCoords()
        {
            static std::array<std::array<double, 2ul>, 9ul> ret{ {
                { { -1., -1. } },
                { { 0., -1. } },
                { { 1., -1. } },
                { { 1., 0. } },
                { { 1., 1. } },
                { { 0., 1. } },
                { { -1., 1. } },
                { { -1., 0. } },
                { { 0., 0. } },
            } };

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup
