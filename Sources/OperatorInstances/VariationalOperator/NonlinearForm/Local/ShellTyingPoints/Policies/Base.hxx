/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hpp"


#include <array>
#include <cassert>
#include <cstddef>
#include <memory>

#include "Utilities/Containers/EnumClass.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS { class TyingPointDataForComponent; }
namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS { enum class tying_pt_interpolation_component : std::size_t; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    inline const TyingPointDataForComponent& Base::GetTyingPointDataForComponent(
        const QuadraturePoint& quad_pt,
        const Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component tying_pt_component)
        const
    {
        const auto quad_pt_index = quad_pt.GetIndex();
        assert(quad_pt_index < mitc_data_.size());

        decltype(auto) mitc_data_at_quad_pt = mitc_data_[quad_pt_index];
        const auto tying_pt_component_index = EnumUnderlyingType(tying_pt_component);
        assert(tying_pt_component_index < mitc_data_at_quad_pt.size());

        assert(!(!mitc_data_at_quad_pt[tying_pt_component_index]));
        return *mitc_data_at_quad_pt[tying_pt_component_index];
    }


    inline auto Base::GetNonCstMITCData() noexcept -> mitc_data_type&
    {
        return mitc_data_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HXX_
