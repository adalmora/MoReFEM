/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Tue, 22 Dec 2020 17:43:53 +0200
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HPP_


#include <array>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }
namespace MoReFEM::Advanced::LocalVariationalOperatorNS { class InformationAtQuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Base class for tying point policy, which encapsulates the storage of the data and the related accessor.
     *
     * The method to fill these data is left for the inherited class.
     */
    class Base
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Base;

        //! Convenient alias.
        using tying_pt_interpolation_component =
            Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;

        // clang-format off
        //! Storage for the MITC data, computed during the initialization phase.
        using mitc_data_type =
        std::vector
        <
            std::array
            <
                TyingPointDataForComponent::unique_ptr,
                EnumUnderlyingType(tying_pt_interpolation_component::Ncomponents)
            >
        >;
        // clang-format on

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        //! Constructor which does not initialize the object completely. A call to InitTyingPointData() is required.
        explicit Base() = default;

        //! Destructor.
        virtual ~Base();

        //! \copydoc doxygen_hide_copy_constructor
        Base(const Base& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Base(Base&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Base& operator=(const Base& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Base& operator=(Base&& rhs) = delete;

        ///@}


      public:
        /*!
         * \class doxygen_hide_init_point_data_prototype
         *
         * \brief Method which actually computes all of the tying point related data and stores them in
         *  TyingPointDataForComponent objects.
         *
         * \param[in] infos_at_quad_pt_list List of InformationAtQuadraturePoint objects, for each quad point.
         * \param[in] ref_geom_elt Reference geometric element used.
         * \param[in] ref_felt Reference finite element used for the unknown.
         * \param[in] test_ref_felt Reference finite element used for the test function.
         *
         */

        //! \copydoc doxygen_hide_init_point_data_prototype
        virtual void
        InitTyingPointData(const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
                               infos_at_quad_pt_list,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator& ref_felt,
                           const Advanced::RefFEltInLocalOperator& test_ref_felt) = 0;

        /*!
         * \brief Returns the tying point data for a given quadrature point and a given component.
         *
         * \param[in] quad_pt Quadrature point  for which data is sought.
         * \param[in] tying_pt_component Component index for which the data is sought.
         *
         * \return The \a TyingPointDataForComponent object.
         */
        const TyingPointDataForComponent&
        GetTyingPointDataForComponent(const QuadraturePoint& quad_pt,
                                      const tying_pt_interpolation_component tying_pt_component) const;

      protected:
        //! Non constant accessor to the stored data, for the sake of the derived class only.
        mitc_data_type& GetNonCstMITCData() noexcept;

      private:
        //! Attribute containing the tying point data for each tying component. Vector index stands for a quadrature
        //! point index and array index stands for a component index.
        mitc_data_type mitc_data_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hxx"


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_LOCAL_x_SHELL_TYING_POINTS_x_POLICIES_x_BASE_HPP_
