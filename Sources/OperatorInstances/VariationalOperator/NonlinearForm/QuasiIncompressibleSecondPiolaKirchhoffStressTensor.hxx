/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Fri, 6 Mar 2020 11:47:17 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                        ViscoelasticityPolicyT,
                                                        InternalVariablePolicyT,
                                                        HydrostaticLawPolicyT,
                                                        MixedSolidIncompressibilityPolicyT>::
        QuasiIncompressibleSecondPiolaKirchhoffStressTensor(
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const Solid& solid,
            const TimeManager& time_manager,
            const typename HyperelasticityPolicyT::law_type* hyperelastic_law,
            const HydrostaticLawPolicyT* hydrostatic_law,
            const QuadratureRulePerTopology* const a_quadrature_rule_per_topology,
            input_internal_variable_policy_type* input_internal_variable_policy)
    : stiffness_operator_parent(felt_space,
                                unknown_list[0],
                                test_unknown_list[0],
                                solid,
                                time_manager,
                                hyperelastic_law,
                                a_quadrature_rule_per_topology,
                                input_internal_variable_policy),
      penalization_operator_parent(felt_space,
                                   unknown_list,
                                   test_unknown_list,
                                   stiffness_operator_parent::GetCauchyGreenTensor(),
                                   time_manager,
                                   hydrostatic_law,
                                   a_quadrature_rule_per_topology)
    {
        assert(!(!unknown_list[0]));
        assert(!(!unknown_list[1]));
        assert(!(!test_unknown_list[0]));
        assert(!(!test_unknown_list[1]));
        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefDisplacementGlobalVector(state_previous_iteration.Get()), domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple, state_previous_iteration, domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefDisplacementGlobalVector state_previous_iteration,
                                                      ConstRefVelocityGlobalVector velocity_previous_iteration,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, state_previous_iteration, velocity_previous_iteration, domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const bool do_update_sigma_c,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            do_update_sigma_c,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const bool do_update_sigma_c,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            do_update_sigma_c,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()), domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefMonolithicDeviatoricGlobalVector state_previous_iteration,
                                                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(
            linear_algebra_tuple, ConstRefDisplacementGlobalVector(state_previous_iteration.Get()), domain);

        penalization_operator_parent::Assemble(
            linear_algebra_tuple, state_previous_iteration, volumetric_vector, domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                      ConstRefDisplacementGlobalVector state_previous_iteration,
                                                      ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                                                      const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple, state_previous_iteration, domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()),
                                               volumetric_vector,
                                               domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    template<class LinearAlgebraTupleT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<HyperelasticityPolicyT,
                                                             ViscoelasticityPolicyT,
                                                             InternalVariablePolicyT,
                                                             HydrostaticLawPolicyT,
                                                             MixedSolidIncompressibilityPolicyT>::
        Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                 ConstRefDisplacementGlobalVector state_previous_iteration,
                 ConstRefVelocityGlobalVector velocity_previous_iteration,
                 ConstRefPreviousElectricalActivationGlobalVector electrical_activation_previous_time,
                 ConstRefCurrentElectricalActivationGlobalVector electrical_activation_at_time,
                 ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
                 const Domain& domain) const
    {
        stiffness_operator_parent::Assemble(linear_algebra_tuple,
                                            state_previous_iteration,
                                            velocity_previous_iteration,
                                            electrical_activation_previous_time,
                                            electrical_activation_at_time,
                                            domain);

        penalization_operator_parent::Assemble(linear_algebra_tuple,
                                               ConstRefMonolithicDeviatoricGlobalVector(state_previous_iteration.Get()),
                                               volumetric_vector,
                                               domain);
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::RestartInternalVariablesAtPreviousTimeStep()
    {
        stiffness_operator_parent::RestartInternalVariablesAtPreviousTimeStep();
    }


    template<class HyperelasticityPolicyT,
             class ViscoelasticityPolicyT,
             class InternalVariablePolicyT,
             class HydrostaticLawPolicyT,
             template<class>
             class MixedSolidIncompressibilityPolicyT>
    void QuasiIncompressibleSecondPiolaKirchhoffStressTensor<
        HyperelasticityPolicyT,
        ViscoelasticityPolicyT,
        InternalVariablePolicyT,
        HydrostaticLawPolicyT,
        MixedSolidIncompressibilityPolicyT>::UpdateInternalVariablesBetweenTimeStep()
    {
        stiffness_operator_parent::UpdateInternalVariablesBetweenTimeStep();
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_SECOND_PIOLA_KIRCHHOFF_STRESS_TENSOR_HXX_
