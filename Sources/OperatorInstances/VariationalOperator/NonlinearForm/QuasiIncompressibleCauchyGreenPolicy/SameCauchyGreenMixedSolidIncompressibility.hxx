/*!
//
// \file
//
//
// Created by Philippe Moireau <philippe.moireau@inria.fr> on the Fri, 23 Feb 2018 10:31:15 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_SAME_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HXX_
#define MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_SAME_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HXX_

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT>
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>::SameCauchyGreenMixedSolidIncompressibility(
        const FEltSpace& felt_space,
        const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
        const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
        const cauchy_green_tensor_type& cauchy_green_tensor,
        const TimeManager& time_manager,
        const HydrostaticLawPolicyT* hydrostatic_law,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             cauchy_green_tensor,
             hydrostatic_law)
    {
        static_cast<void>(time_manager);
        felt_space.ComputeLocal2Global(felt_space.GetExtendedUnknownPtr(*unknown_list[1]),
                                       DoComputeProcessorWiseLocal2Global::yes);

        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
    }


    template<class HydrostaticLawPolicyT>
    const std::string& SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>::ClassName()
    {
        static std::string name("SameCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT>
    template<class LinearAlgebraTupleT>
    inline void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefMonolithicDeviatoricGlobalVector input_vector,
        const Domain& domain) const
    {
        return parent::AssembleImpl(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<class HydrostaticLawPolicyT>
    template<class LocalOperatorTypeT>
    inline void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<ConstRefMonolithicDeviatoricGlobalVector>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressure());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_VARIATIONAL_OPERATOR_x_NONLINEAR_FORM_x_QUASI_INCOMPRESSIBLE_CAUCHY_GREEN_POLICY_x_SAME_CAUCHY_GREEN_MIXED_SOLID_INCOMPRESSIBILITY_HXX_
