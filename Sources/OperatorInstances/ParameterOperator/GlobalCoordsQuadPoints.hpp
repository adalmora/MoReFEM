/*!
//
// \file
//
//
// Created by Jérôme Diaz <jerome.diaz@inria.fr> on the Wed, 13 May 2020 16:53:38 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup OperatorInstancesGroup
// \addtogroup OperatorInstancesGroup
// \{
*/


#ifndef MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HPP_
#define MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HPP_

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: export

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/GlobalCoordsQuadPoints.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadratureRulePerTopology; }
namespace MoReFEM { class Unknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{


    /*!
     * \brief Implementation of global \a GlobalCoordsQuadPoints operator.
     *
     * Given a \a QuadratureRule, this operator computes the global coordinates of the \a QuadraturePoints used by
     * this rule on the relevant finite element space and stores them into a \a ParameterAtQuadraturePoint
     * attribute.
     */
    class GlobalCoordsQuadPoints final
    : public GlobalParameterOperator<GlobalCoordsQuadPoints,
                                     LocalParameterOperatorNS::GlobalCoordsQuadPoints,
                                     ParameterNS::Type::vector>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalCoordsQuadPoints;

        //! Alias to unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Convenient alias to pinpoint the GlobalParameterOperator parent.
        using parent =
            GlobalParameterOperator<self, LocalParameterOperatorNS::GlobalCoordsQuadPoints, ParameterNS::Type::vector>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] felt_space Finite element space upon which the operator is defined.
         * \param[in] unknown Unknown considered for this operator (might be vector or vectorial).
         * \param[in] quadrature_rule_per_topology Quadrature rule from which the global coordinates of the
         * quadrature points will be computed. \param[in,out] global_coords_quad_pt \a Parameter to update.
         */
        explicit GlobalCoordsQuadPoints(
            const FEltSpace& felt_space,
            const Unknown& unknown,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology,
            // clang-format off
                                            ParameterAtQuadraturePoint
                                            <
                                                ParameterNS::Type::vector,
                                                ParameterNS::TimeDependencyNS::None
                                            >& global_coords_quad_pt);
        // clang-format on

        //! Destructor.
        ~GlobalCoordsQuadPoints() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalCoordsQuadPoints(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalCoordsQuadPoints(GlobalCoordsQuadPoints&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalCoordsQuadPoints& operator=(const GlobalCoordsQuadPoints& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalCoordsQuadPoints& operator=(GlobalCoordsQuadPoints&& rhs) = delete;

        ///@}


        /*!
         * \brief To get the local operator to convert local quadrature point coordinates into global ones.
         */
        void Update() const;


      public:
        /*!
         * \brief Only defined to respect the generic interface expected by the parent, it is not used as there is
         * no extraction of data from a global to a local level needed for this class.
         *
         * \param[in] local_operator Local operator in charge of the elementary computation. A  mutable work
         * variable is actually set in this call.
         * \param[in] local_felt_space List of finite elements being considered; all those related to the
         * same GeometricElt.
         *
         */
        void SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                         LocalParameterOperator& local_operator,
                                         const std::tuple<>&&) const;
    };


} // namespace MoReFEM::GlobalParameterOperatorNS


/// @} // addtogroup OperatorInstancesGroup


#include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hxx" // IWYU pragma: export


#endif // MOREFEM_x_OPERATOR_INSTANCES_x_PARAMETER_OPERATOR_x_GLOBAL_COORDS_QUAD_POINTS_HPP_
