//! \file
//
//
//  Restart.cpp
//  MoReFEM
//
//  Created by Sébastien Gilles on 03/02/2023.
//
//

#include <cstddef>
#include <filesystem>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include "Utilities/AsciiOrBinary/Advanced/DatafileExtension.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIterationFile.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "FormulationSolver/Exceptions/Restart.hpp"
#include "FormulationSolver/Restart.hpp"


namespace MoReFEM::Advanced::RestartNS
{


    namespace // anonymous
    {


        // Compute the filename that which hold the \a vector data for the
        // proper time iteration (without its encompassing directory)
        std::string ComputeFilename(const TimeManager& time_manager, std::string_view tag);

        /*!
         * \brief Returns the directory in which restart data may be found.
         *
         * \param[in] time_manager The \a TimeManager provides the base directory in which all restart data may be found,
         * regardless of \a GodOfDof and \a NumberingSubset.
         * \param[in] god_of_dof The \a GodOfDof considered - in practice used to get a step further and find the
         * subdirectory dealing with it.
         * \param[in] numbering_subset The \a NumberingSubset considered - in practice used to get a step further and find the
         * subdirectory dealing with it.
         *
         * \return The full directory in which the restart files may be found.
         */
        FilesystemNS::Directory GetDataDirectory(const TimeManager& time_manager,
                                                 const GodOfDof& god_of_dof,
                                                 const NumberingSubset& numbering_subset);


    } // namespace


    void LoadGlobalVectorFromData(const TimeManager& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalVector& vector)
    {
        FilesystemNS::File input_file;

        decltype(auto) mpi = god_of_dof.GetMpi();
        decltype(auto) numbering_subset = vector.GetNumberingSubset();

        if (mpi.IsRootProcessor())
        {
            auto filename = ComputeFilename(time_manager, tag);
            auto directory = GetDataDirectory(time_manager, god_of_dof, numbering_subset);

            input_file = directory.AddFile(filename);

            if (mpi.IsRootProcessor() && !input_file.DoExist())
                throw ExceptionNS::FormulationSolverNS::RestartNS::InexistantFile(input_file, __FILE__, __LINE__);
        }

        // Temporary file used here as a safety: we already know the expected structure of
        // the vector, and the Init...() function below doesn't know it. Using an intermediate
        // vector enables safety check.
        GlobalVector buf(numbering_subset);

        buf.InitFromProgramWiseBinaryFile(
            mpi,
            static_cast<std::size_t>(vector.GetProcessorWiseSize(__FILE__, __LINE__)),
            static_cast<std::size_t>(vector.GetProgramWiseSize(__FILE__, __LINE__)),
            (mpi.template Nprocessor<int>() == 1 ? std::vector<PetscInt>{} : vector.GetGhostPadding()),
            (mpi.IsRootProcessor() ? input_file : FilesystemNS::File{}),
            __FILE__,
            __LINE__);

        vector.Copy(buf, __FILE__, __LINE__);
    }


    void WriteDataForRestart(const TimeManager& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalVector& vector)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        auto filename = ComputeFilename(time_manager, tag);
        auto directory = god_of_dof.GetOutputDirectoryForNumberingSubset(vector.GetNumberingSubset());
        auto output_file = directory.AddFile(filename);

        vector.template Print<MpiScale::program_wise>(mpi, output_file, __FILE__, __LINE__, binary_or_ascii::binary);
    }


    void WriteDataForRestart(const TimeManager& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalMatrix& matrix)
    {
        decltype(auto) mpi = god_of_dof.GetMpi();

        auto filename = ComputeFilename(time_manager, tag);
        auto directory = god_of_dof.GetOutputDirectoryForNumberingSubset(matrix.GetRowNumberingSubset());
        auto output_file = directory.AddFile(filename);

        matrix.ViewBinary(mpi, output_file, __FILE__, __LINE__);
    }


    void LoadGlobalMatrixFromData(const TimeManager& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalMatrix& matrix)
    {
        FilesystemNS::File input_file;

        decltype(auto) mpi = god_of_dof.GetMpi();
        decltype(auto) numbering_subset = matrix.GetRowNumberingSubset();

        if (mpi.IsRootProcessor())
        {
            auto filename = ComputeFilename(time_manager, tag);
            auto directory = GetDataDirectory(time_manager, god_of_dof, numbering_subset);

            input_file = directory.AddFile(filename);

            if (mpi.IsRootProcessor() && !input_file.DoExist())
                throw ExceptionNS::FormulationSolverNS::RestartNS::InexistantFile(input_file, __FILE__, __LINE__);
        }

        matrix.LoadBinary(mpi, input_file, __FILE__, __LINE__);
    }


    void CopySolutionFilesFromOriginalRun(const TimeManager& time_manager,
                                          const GodOfDof& god_of_dof,
                                          const NumberingSubset& numbering_subset)
    {
        auto original_run_directory = GetDataDirectory(time_manager, god_of_dof, numbering_subset);
        auto output_directory = god_of_dof.GetOutputDirectoryForNumberingSubset(numbering_subset);

        InterpretOutputFilesNS::TimeIterationFile time_iteration_data_from_original_run(
            time_manager.GetRestartTimeIterationFile());

        const auto restart_time_index = time_manager.GetRestartTimeIndex();

        decltype(auto) mpi = god_of_dof.GetMpi();

        for (auto time_iteration_file_index = 0ul; time_iteration_file_index < restart_time_index;
             ++time_iteration_file_index)
        {
            decltype(auto) time_iteration_data_for_index =
                time_iteration_data_from_original_run.GetTimeIteration(time_iteration_file_index, __FILE__, __LINE__);

            decltype(auto) source = time_iteration_data_for_index.GetSolutionFilename(mpi);
            std::filesystem::path filename = source.GetDirectoryEntry().path().filename();
            const auto target = output_directory.AddFile(static_cast<std::string>(filename));

            FilesystemNS::Copy(source,
                               target,
                               FilesystemNS::fail_if_already_exist::yes,
                               FilesystemNS::autocopy::no,
                               __FILE__,
                               __LINE__);

            // Complete the time_iteration file as well
            {
                decltype(auto) file = time_manager.GetTimeIterationFile();
                std::ofstream inout{ file.Append(__FILE__, __LINE__) };
                inout << time_iteration_data_for_index << std::endl;
            }
        }
    }


    namespace // anonymous
    {


        std::string ComputeFilename(const TimeManager& time_manager, std::string_view tag)
        {
            std::ostringstream oconv;
            oconv << "restart_" << tag << '_' << std::setfill('0') << std::setw(5) << time_manager.NtimeModified()
                  << ::MoReFEM::Advanced::AsciiOrBinaryNS::DatafileExtension(binary_or_ascii::binary);
            std::string filename = oconv.str();
            return filename;
        }


        auto GetDataDirectory(const TimeManager& time_manager,
                              const GodOfDof& god_of_dof,
                              const NumberingSubset& numbering_subset) -> FilesystemNS::Directory
        {
            auto ret = time_manager.GetRestartDataDirectory();
            ret.AddSubdirectory("Mesh_" + std::to_string(god_of_dof.GetUniqueId().Get()));
            ret.AddSubdirectory("NumberingSubset_" + std::to_string(numbering_subset.GetUniqueId().Get()));

            return ret;
        }


    } // namespace


} // namespace MoReFEM::Advanced::RestartNS
