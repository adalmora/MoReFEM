//! \file
//
//
//  Restart.hxx
//  MoReFEM
//
//  Created by Sébastien Gilles on 03/02/2023.
//
//

#ifndef MOREFEM_x_FORMULATION_SOLVER_x_RESTART_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_RESTART_HPP_

#include <string_view>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class TimeManager; }
namespace MoReFEM { class GodOfDof; }
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GlobalMatrix; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::RestartNS
{


    /*!
     * \class doxygen_hide_restart_functions_most_common_param
     *
     * \param[in] time_manager  \a TimeManager is used to give the main base directory in which restart data are stored and
     * to provide which time iteration is sought.
     * \param[in] god_of_dof The \a GodOfDof considered - it will be used to identify the folder in which is the
     * sought data (used in a call to \a GetDataDirectory()).
     *
     * Restart mode is explained in more details on [this wiki note](../Wiki/Restart.md).
     */


    /*!
     * \class doxygen_hide_restart_serialization_arguments
     *
     * \copydoc doxygen_hide_restart_functions_most_common_param
     * \param[in] tag The name of the quantity (e.g. unknown) that is covered by the sought data; this name had been used
     * to save the restart data in the first place and it appears in the resulting filename, which is in the format:
     * restart_**tag**_**time id**.binarydata where 'time_id' is given by \a time_manager.
     */

    /*!
     * \brief Write content of \a vector into a dedicated directory that might be used in a later restart run.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     * \param[in] vector The vector which data is written in disk.
     */
    void WriteDataForRestart(const TimeManager& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalVector& vector);


    /*!
     * \brief Write content of \a matrix into a dedicated directory that might be used in a later restart run.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     * \param[in] matrix The matrix which data is written in disk.
     *
     * \note This function is not used currently but was tested and work (the model didn't need it).
     */
    void WriteDataForRestart(const TimeManager& time_manager,
                             const GodOfDof& god_of_dof,
                             std::string_view tag,
                             const GlobalMatrix& matrix);


    /*!
     * \brief Load \a vector from the correct restart data.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     * \param[in,out] vector The vector into which the sought data is loaded. It is expected this vector is already properly
     * initialized, with its layout already computed.
     */
    void LoadGlobalVectorFromData(const TimeManager& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalVector& vector);

    /*!
     * \brief Load \a matrix from the correct restart data.
     *
     * \copydoc doxygen_hide_restart_serialization_arguments
     * \param[in,out] matrix The matrix into which the sought data is loaded. It is expected this matrix is already properly
     * initialized, with its layout already computed.
     */
    void LoadGlobalMatrixFromData(const TimeManager& time_manager,
                                  const GodOfDof& god_of_dof,
                                  std::string_view tag,
                                  GlobalMatrix& matrix);


    /*!
     * \brief Copy the solution files that are prior to the restart time index.
     *
     * \copydoc doxygen_hide_restart_functions_most_common_param
     * \param[in] numbering_subset The \a NumberingSubset of the solutions to be copied.
     */
    void CopySolutionFilesFromOriginalRun(const TimeManager& time_manager,
                                          const GodOfDof& god_of_dof,
                                          const NumberingSubset& numbering_subset);


} // namespace MoReFEM::Advanced::RestartNS


#endif // MOREFEM_x_FORMULATION_SOLVER_x_RESTART_HPP_
