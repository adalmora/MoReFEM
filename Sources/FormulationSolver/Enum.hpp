/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_ENUM_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_ENUM_HPP_


namespace MoReFEM
{


    /*!
     * \brief Whether the matrix has already been factorized or not.
     *
     * If yes, there is a huge speed-up in the solver, but you must be sure the matrix is still the same.
     * So in the first call, the value must be 'no'.
     */
    enum class IsFactorized { yes, no };

    //! Whether volumic or surfacic source is considered.
    enum class SourceType { volumic, surfacic };


} // namespace MoReFEM


namespace MoReFEM::VariationalFormulationNS
{


    /*!
     * \brief Specify whether boundary conditions are applied on system rhs, system matrix or both.
     */
    enum class On { system_matrix, system_rhs, system_matrix_and_rhs };


} // namespace MoReFEM::VariationalFormulationNS


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_ENUM_HPP_
