/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_EXCEPTIONS_x_RESTART_HPP_
#define MOREFEM_x_FORMULATION_SOLVER_x_EXCEPTIONS_x_RESTART_HPP_

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS
{


    //! When the loading of data fails.
    struct InexistantFile : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] file File that was not found
          * \param[in] invoking_file File that invoked the function or class; usually __FILE__.
         * \param[in] invoking_line File that invoked the function or class; usually __LINE__.

         */
        explicit InexistantFile(const FilesystemNS::File& file, const char* invoking_file, int invoking_line);


        //! Destructor.
        virtual ~InexistantFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InexistantFile(const InexistantFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InexistantFile(InexistantFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InexistantFile& operator=(const InexistantFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InexistantFile& operator=(InexistantFile&& rhs) = delete;
    };


} // namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS


/// @} // addtogroup FormulationSolverGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_EXCEPTIONS_x_RESTART_HPP_
