/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr>
// Copyright (c) Inria. All rights reserved.
//
// \ingroup FormulationSolverGroup
// \addtogroup FormulationSolverGroup
// \{
*/


#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Filesystem/File.hpp"

#include "FormulationSolver/Exceptions/Restart.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string InexistantFileMsg(const MoReFEM::FilesystemNS::File& file);

} // namespace


namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS
{


    InexistantFile::~InexistantFile() = default;


    InexistantFile::InexistantFile(const FilesystemNS::File& file, const char* invoking_file, int invoking_line)
    : MoReFEM::Exception(InexistantFileMsg(file), invoking_file, invoking_line)
    { }


} // namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS


namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string InexistantFileMsg(const MoReFEM::FilesystemNS::File& file)
    {
        std::ostringstream oconv;
        oconv << "File " << file << " expected to hold restart data wasn't found on the filesystem.";

        return oconv.str();
    }


} // namespace


/// @} // addtogroup FormulationSolverGroup
