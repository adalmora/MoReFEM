/*!
//
// \file
//
//
// Created by Sebastien Gilles <sebastien.gilles@inria.fr> on the Thu, 29 Oct 2015 16:00:12 +0100
// Copyright (c) Inria. All rights reserved.
//
// \ingroup CoreGroup
// \addtogroup CoreGroup
// \{
*/


#ifndef MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_
#define MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_

// IWYU pragma: private, include "FormulationSolver/Internal/Snes/Solver.hpp"

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::SolverNS
{


    template<std::size_t SolverIndexT, ::MoReFEM::Concept::InputDataType InputDataT>
    ::MoReFEM::Wrappers::Petsc::Snes::unique_ptr
    InitSolver(const ::MoReFEM::Wrappers::Mpi& mpi,
               const InputDataT& input_data,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESFunction snes_function,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESJacobian snes_jacobian,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESViewer snes_viewer,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function)
    {
        decltype(auto) section =
            ::MoReFEM::InputDataNS::ExtractSection<::MoReFEM::InputDataNS::Petsc<SolverIndexT>>::Value(input_data);

        Internal::Wrappers::Petsc::SolverNS::Settings solver_settings(section);

        return std::make_unique<::MoReFEM::Wrappers::Petsc::Snes>(mpi,
                                                                  std::move(solver_settings),
                                                                  snes_function,
                                                                  snes_jacobian,
                                                                  snes_viewer,
                                                                  snes_convergence_test_function,
                                                                  __FILE__,
                                                                  __LINE__);
    }


} // namespace MoReFEM::Internal::SolverNS


/// @} // addtogroup CoreGroup


#endif // MOREFEM_x_FORMULATION_SOLVER_x_INTERNAL_x_SNES_x_SOLVER_HXX_
