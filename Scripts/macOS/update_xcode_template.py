import os
import subprocess

import sys



def update_xcode_template():
    """Update the available XCode templates related to MoReFEM in XCode."""

    print("=== Update XCode templates in user's Library. ===")
    
    # MoReFEM root folder is twice removed from the one this script is found.
    morefem_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..")

    target = os.path.join(
        os.path.expanduser("~"), "Library", "Developer", "Xcode", "Templates",
        "File Templates", "MoReFEM")

    if not os.path.exists(target):
        os.makedirs(target)

    # Same as target except for the handling of white space in
    # File Templates. I didn't manage to handle it differently.
    target = target.replace(r' ', r'\ ')

    cmd = "rsync -v -r --delete {0} {1}".format(
        os.path.join(morefem_folder, "cmake", "XCodeTemplates", "."), target)

    print(target)

    subprocess.Popen(cmd, shell=True).communicate()


if __name__ == "__main__":
    update_xcode_template()
