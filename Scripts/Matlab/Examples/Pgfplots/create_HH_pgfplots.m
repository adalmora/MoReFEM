function [maximum, minimum] = create_HH_pgfplots(dir_hh, name_in, extension, x_init, x_final, dx, directory_data, name_data_output)
% create a varargin might be better to handle x better 

addpath('/Volumes/Data/gautier/Matlab/MoReFEM');
addpath('/Volumes/Data/gautier/Matlab/IOfunctions/');

x = str2num(x_init):str2num(dx):str2num(x_final);

res_hh = load_files(dir_hh, name_in, extension, 0);

maximum = max(max(res_hh));
minimum = min(min(res_hh));

for (i=1:(size(res_hh,2)))
    save_fid = ...
    fopen([directory_data '/' name_data_output '_' int2str(i) '.data'],'w');
    fprintf(save_fid, 'grid solution \n');
    fprintf(save_fid, '%12.6e %12.6e \n', [x;res_hh(:,i)']);
    fclose(save_fid);
end

fprintf('Max : %1.12e Min : %1.12e\n', ceil(maximum), floor(minimum));

obj = [directory_data,'/max_min.dat'];
fidend = fopen(obj,'w');
fprintf(fidend,'%1.12e %1.12e\n', ceil(maximum),floor(minimum));
fclose('all');

disp('End of create_HH_pgfplots.');

end