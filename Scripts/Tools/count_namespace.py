######################## 
## WARNING
## This is at best beta script, which was useful while dealing with #1816
######################## 

import os
import re


def count_namespace(myfile):
    """
    Read the content of \a myfile (which is expected to be a path on the filesystem) and count how many
    relevant 'namespace' there are.

    What we aim to check with this function is that all the code in the current file belongs to a same 
    namespace, using C++ 17 inlined namespaces (e.g. namespace MoReFEM::Advanced::GeometricEltNS).

    We do not count on purpose:
    - 'namespace' in comments and descriptions
    - anonymous namespaces
    - namespace std (may be used for instance when providing overload for `std::hash`)

    @return Number of relevant namespaces found in \a myfile; target is 1.
    """

    count_activated = True
    count = 0

    ignore_list = ("namespace GVO", "namespace GPO", "namespace.", "namespace scope", "namespace FormatExceptionNS", "namespace EnsightExceptionNS", "namespace MeditExceptionNS")

    ignore_regex = re.compile(r"""([a-zA-z*]+ namespace|namespace std|namespace // anonymous|namespace,|namespace \w* =|} // namespace)""")

    skip_current_line = False

    with open(myfile) as FILE:

        for line in FILE:

            if "// Forward declarations." in line:
                count_activated = False
            elif "// End of forward declarations." in line:
                count_activated = True
            elif count_activated:
                if line.lstrip().startswith("//! "):
                    continue

                if ignore_regex.search(line):
                    continue

                for ignore_item in ignore_list:
                    if ignore_item in line:
                        #print(f"Line {line} ignored")
                        skip_current_line = True
                        break
                
                if skip_current_line:
                    skip_current_line = False
                    continue

                
                if "namespace" in line:
                    count += 1

        return count

def skim_files(directory):
    """
    Skim through all the files found in the tree beginning at \a directory and for all C++ files (with 
    cpp, hpp or hxx extension) count the number of namespaces and check it is properly 1.

    If not, store in the list the file path which will be printed by the function.
    """

    exclusion_list = ["Sources/ThirdParty/Wrappers/Petsc/Matrix/MatrixOperations.hpp",
                      "Sources/ThirdParty/Wrappers/Petsc/Vector/Vector.hpp",
                      "Sources/ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp", # 'namespace' in comment
                      "Sources/ThirdParty/Wrappers/Tclap/StringPair.hpp", # traits needed in TCLAP original namespace
                      "Sources/Test/Core/TimeStep/Variable/test.cpp", # due to friendship shaenanigans
                      "Sources/Test/Core/TimeStep/Constant/test.cpp",  # due to friendship shaenanigans
                      "Sources/Test/Utilities/UniqueId/test.cpp", # made up namespaces part of the test!
                      "Sources/Test/Utilities/StrongType/test.cpp", # made up namespaces part of the test!
                      "Sources/OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hpp", # several 'namespace XXX = '
                      "Sources/FormulationSolver/Enum.hpp", # Convenient here to define 2 namespaces
                      "Sources/PostProcessing/OutputDeformedMesh/OutputDeformedMesh.cpp", # should be removed (#1560)
                      "Sources/PostProcessing/OutputDeformedMesh/OutputDeformedMesh.hpp", # should be removed (#1560)
                      "Sources/Geometry/Mesh/Mesh.cpp", # namespace alias  
                      "Sources/Geometry/Domain/DomainManager.hpp", # namespace in comment
                      ]

    too_many_namespaces_list = []

    for root, dirs, files in os.walk(directory):

        for myfile in files:

            extension = os.path.splitext(myfile)[1]
            if extension not in [".hpp", ".hxx", ".cpp"]:
                continue

            path = os.path.join(root, myfile)

            if path in exclusion_list:
                continue

            count = count_namespace(path)

            if count > 1:
                too_many_namespaces_list.append(path)

    print(f"There are {len(too_many_namespaces_list)} files with too many namespaces: ")

    for myfile in too_many_namespaces_list:
        count = count_namespace(myfile)
        print(f"\t- {myfile} ({count})")
    
    print(f"End of the {len(too_many_namespaces_list)} files with too many namespaces: ")


if __name__ == "__main__":

    #count_namespace("Sources/FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp")



    skim_files("Sources")

    




            



        

