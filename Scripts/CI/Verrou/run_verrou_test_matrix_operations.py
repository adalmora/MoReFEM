import os

from run_verrou_tools import MoReFEMRootDir, RunVerrou

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    lua_file = os.path.join(morefem_root_dir, "Sources", "Test", "ThirdParty", "PETSc", "MatrixOperations", "demo.lua")

    RunVerrou("Sources/MoReFEMTestPetscMatrixOperations",  lua_file, os.path.join(morefem_root_dir, "verrou_test_matrix_operations.txt"), is_model = False)
