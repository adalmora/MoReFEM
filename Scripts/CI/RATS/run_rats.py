import os
import subprocess



if __name__ == "__main__":

    morefem_source_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..", "Sources")

    cmd = \
        [
            "rats",
            "--quiet",
            "--xml",
            "-w 3",
            morefem_source_dir
            ]
        

    xml_output = open('morefem-rats.xml', 'w')

    subprocess.Popen(cmd, shell=False, stdout=xml_output, stderr=xml_output).communicate()

    print("Output written in morefem-rats.xml")