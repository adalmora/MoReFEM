import os
import tempfile
from interpret_log import *

def test_file_ok():

    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_filename = os.path.join(tmp_dir, 'tmp_file')

        with open(tmp_filename, "w") as tmp_file:
            tmp_file.write("[1/238] Building CXX object Sources/CMakeFiles/MoReFEM4Hyperelasticity_lib.dir/ModelInstances/  Hyperelasticity/Model.cpp.o\n")
            tmp_file.write("[2/238] Building CXX object Sources/CMakeFiles/MoReFEM4Stokes_2_operatorsEnsightOutput.dir/ModelInstances/Stokes/main_ensight_output_monolithic.cpp.o\n")

        assert FindWarningInIWYULog(tmp_filename).Nwarning == 0

def test_file_empty():

    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_filename = os.path.join(tmp_dir, 'tmp_file')

        with open(tmp_filename, "w") as tmp_file:
            pass

        assert FindWarningInIWYULog(tmp_filename).Nwarning == 0

def test_file_with_warning():

    with tempfile.TemporaryDirectory() as tmp_dir:
        tmp_filename = os.path.join(tmp_dir, 'tmp_file')

        with open(tmp_filename, "w") as tmp_file:
            tmp_file.write("../Sources/FiniteElement/FiniteElement/LocalFEltSpace.cpp should add these lines:\n")

        result = FindWarningInIWYULog(tmp_filename)

        assert result.Nwarning == 1
        assert result.ProblematicFileList[0] == "../Sources/FiniteElement/FiniteElement/LocalFEltSpace.cpp"
