import os
import unittest
from tempfile import TemporaryDirectory

from interpret_valgrind_log import interpret_valgrind_log


class TestInterpretValgrindLog(unittest.TestCase):
    
    
    def test_indirectly_lost(self):            
        log = interpret_valgrind_log("test/memcheck_bogus.txt")        
        leak = log.indirectly_lost        
        self.assertEqual(leak.Nbytes, 8)
        self.assertEqual(leak.Nblocks, 1)
        
    def test_definitely_lost(self):
        log = interpret_valgrind_log("test/memcheck_bogus.txt")
        leak = log.definitely_lost
        self.assertEqual(leak.Nbytes, 578)
        self.assertEqual(leak.Nblocks, 17)

    def test_possibly_lost(self):
        log = interpret_valgrind_log("test/memcheck_bogus.txt")
        leak = log.possibly_lost
        self.assertEqual(leak.Nbytes, 15)
        self.assertEqual(leak.Nblocks, 3)

    def test_still_reachable(self):
        log = interpret_valgrind_log("test/memcheck_bogus.txt")
        leak = log.still_reachable
        self.assertEqual(leak.Nbytes, 245)
        self.assertEqual(leak.Nblocks, 24)

    def test_suppressed(self):
        log = interpret_valgrind_log("test/memcheck_bogus.txt")
        leak = log.suppressed
        self.assertEqual(leak.Nbytes, 429)
        self.assertEqual(leak.Nblocks, 13)
        
    def test_only_indirectly_lost_and_suppressed(self):
        log = interpret_valgrind_log("test/memcheck_bogus.txt")
        self.assertFalse(log.only_indirectly_lost_and_suppressed())
        log = interpret_valgrind_log("test/memcheck_realcase.txt")
        self.assertTrue(log.only_indirectly_lost_and_suppressed())

