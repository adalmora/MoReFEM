import os

from run_valgrind_tools import MoReFEMRootDir, RunValgrind

if __name__ == "__main__":

    morefem_root_dir = MoReFEMRootDir()
    morefem_model_instances_dir = os.path.join(morefem_root_dir, "Sources", "ModelInstances")

    lua_file = os.path.join(morefem_model_instances_dir, "Elasticity", "demo_2d_binary.lua")
#
    RunValgrind("Sources/MoReFEM4Elasticity",  lua_file, os.path.join(morefem_root_dir, "memcheck_elasticity.txt"))
