import os
from integration_manager import extract_issue_numbers

def test_extract_issue_numbers():

    assert(extract_issue_numbers("#1784 ") == [ 1784 ])

    assert(extract_issue_numbers("#1784 - #234 :") == [ 1784, 234 ])

    assert(extract_issue_numbers("#1784 `foo` - #234 ad") == [ 1784, 234 ])

    assert(extract_issue_numbers("qwerty") == None)

    assert(extract_issue_numbers("#1784:") == [ 1784 ])

    assert(extract_issue_numbers("#1784 #abcd #4a #a4 ") == [ 1784 ])