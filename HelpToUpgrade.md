# From v23.11.2 to v23.37

- [#1801](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1801) `MainEnsightOutput` now takes the model as template argument, not just the `InputData`.
- [#1750](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1750) A concept for `MoReFEMData` has been introduced; some `class InputDataT` must be replaced by `::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT`.
- [#1796](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1796) `InputData` has been split so that some values may be defined only by the author of the model. At minimum you should introduce a `ModelSettings` object which should defined the `IndexedSectionDescription` leaves of the indexed section object - but I advise you to take the time to fully use what this new object enables. There is an extended explanation of it in `${MOREFEM_ROOT}/Documentation/Wiki/Utilities/InputData.md`; you may also see the `${MOREFEM_ROOT}/Documentation/Wiki/Model/ModelTutorial.md` which has been updated to use this.
- [#1809](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1809) The way library dependencies is handled has been modified; you may get link errors for some of the executables. The best is to mimic in your CMakeLists.txt the dependencies used in the example models in `ModelInstances` in MoReFEM library. In particular, make sure your model dependency uses up properly something as `$<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MAIN_LIBS}>` rather than just `${MOREFEM_MAIN_LIBS}`.
- [#1807](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1807) TransientSource in Lua files has been renamed: VectorialTransientSource or ScalarTransientSource.

# From v22.47 to v23.11.2

- [#1771](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1771) File::SubstituteEnvironmentVariables() has been removed, you may just delete the line (operation is automatically done in constructor now).
- [#1762](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1762) This is a complete overhaul to the class; check the issue for details! (for instance `ResetTimeAtPreviousTimeStep()` has been decommissioned and should be replaced by `DecreaseTime()`).
- [#1776](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1776) Rename  `VariationalFormulation` method `InitializeVectorSystemSolution` either `SetInitialSystemSolution()` or `ApplyInitialConditionToVector()` depending whether it is applied on the system solution or on any vector.
- [#1780](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1780) Replace `#include "Utilities/InputData/Internal/ExtractLeaf/ExtractLeaf.hpp"` by `"Utilities/InputData/Extract.hpp"` and `Internal::InputDataNS::ExtractLeaf` by `InputDataNS::ExtractLeafFromSection`.
- [#1784](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1784) Replace `InputDataNS::Extract<>` by `InputDataNS::ExtractLeaf<>`.
- [#1787](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1787) Add __FILE__ and __LINE__ in all calls for `DirichletBoundaryConditionManager::GetDirichletBoundaryCondition()` and likewise functions.
- [#1755](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1755) Replace `Utilities::InputDataNS` by `InputDataNS`, `public Crtp::Section` by `public ::MoReFEM::Advanced::InputDataNS::Crtp::Section` and `public Crtp::InputData` by `public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf`.


# From v22.37 to v22.47

- [#1749](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1749) Add if relevant for your model path to Slepc library (and its include).

- [#1751](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1751) Modify the path to PETSc (and possibly Slepc) library in your precache files: a subdirectory `morefem_config` should be added (provided you're using the ThirdPartyCompilationFactory facility to install third party dependencies.) SO for instance `${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/lib` becomes `${MOREFEM_THIRD_PARTY_LIBRARIES_DIR}/Petsc/lib/morefem_config`


# From v21.31 to v22.37

- [(#1566)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1566) XCode: modify the MoReFEM path in the workspace settings if you're using it.

- Change includes:

`Core/InputData/Instances/Parameter/Tools/Advanced/UsualDescription.hpp` -> `Core/InputData/Instances/Parameter/Internal/ParameterFields.hpp` [(#1724)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1724)

`Test/Tools/CompareEnsightFiles.hpp` -> `Test/Tools/CompareDataFiles.hpp` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696)
`FixtureNS::Environment` -> `FixtureNS::TestEnvironment`  [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 
`Test/Tools/Fixture/Environment.hpp` -> `Test/Tools/Fixture/TestEnvironment.hpp` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 


Remove `#include "Utilities/InputData/LuaFunction.hpp"` [(#1623)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1623)

- [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) In `InputData.hpp` for your model, explicitly make the enum class contains a `std::size_t`, e.g.:

````
enum class MeshIndex : std::size_t { mesh = 1 };
````

instead of:

````
enum class MeshIndex { mesh = 1 };
````


- Make the following find/replace:

`GetDomain(EnumUnderlyingType(DomainIndex::` -> `GetDomain(AsDomainId(DomainIndex::`  [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetUnknown(EnumUnderlyingType(UnknownIndex::` -> `GetUnknown(AsUnknownId(UnknownIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetUnknownPtr(EnumUnderlyingType(UnknownIndex::` -> `GetUnknownPtr(AsUnknownId(UnknownIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetFEltSpace(EnumUnderlyingType(FEltSpaceIndex::` -> `GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetGodOfDof(EnumUnderlyingType(MeshIndex::` -> `GetGodOfDof(AsMeshId(MeshIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNumberingSubset(EnumUnderlyingType(NumberingSubsetIndex::` -> `GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNumberingSubsetPtr(EnumUnderlyingType(NumberingSubsetIndex::` -> `GetNumberingSubsetPtr(AsNumberingSubsetId(NumberingSubsetIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetFiberList(EnumUnderlyingType(FiberIndex::`-> `GetFiberList(AsFiberListId(FiberIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
`GetNonCstFiberList(EnumUnderlyingType(FiberIndex::`-> `GetNonCstFiberList(AsFiberListId(FiberIndex::` [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) 
If you were using directly integers instead of enums (e.g. `GetNumberingSubset(1)`), this is no longer admissible and you must know use an enum.



`TestNS::CompareEnsightFile` -> `TestNS::CompareDataFiles<MeshNS::Format::Ensight>` [(#1696)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1696) 


- [(#1724)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1724) Initialization of `Parameter` from input data has changed: calls such as `  InitParameterFromInputData<TypeT>::template Perform<parameter_type>(...)` must now specifically tell whether it is a scalar, vectorial or matricial `Parameter`, e.g.:
`InitScalarParameterFromInputData<parameter_type>(...)`

You must explicitly do the `if constexpr` branching if the same line was both used for scalar and vectorial

(new call site a lighter but slightly less generic than previous one - but the previous one didn't work as intended in all cases...)                  

For the vectorial ones, the function to use is `Init3DCompoundParameterFromInputData` (`InitVectorialParameterFromInputData` also exists but do not use exactly the same interface; the new one is rather useful but is something that wasn't existing previously - you can safely bet the function fr your code that worked in MoReFEM v21.31 is the former). This requires the header `#include "Parameters/InitParameterFromInputData/Init3DCompoundParameterFromInputData.hpp"`


- [(#1731)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1731) In `main_ensight_output` files, use strong types for unique ids, e.g.

````
 std::vector<std::size_t> numbering_subset_id_list{ EnumUnderlyingType(NumberingSubsetIndex::monolithic) };
 ````

 becomes

 ````
std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{ AsNumberingSubsetId (NumberingSubsetIndex::monolithic) };
 ````

 and third argument of ` ModelNS::MainEnsightOutput` must be a `AsMeshId` (so something as `AsMeshId(MeshIndex::mesh)` works).


 - [(#1692)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1692)  Check upon the existence of directories on the filesystem has changed.

Now it should look like:

````
FilesystemNS::Directory output_dir(output_dir_path, FilesystemNS::behaviour::read);
output_dir.DoExist();
````

- [(#1692)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1692) Several methods of `FileSystemNS::Directory` (such as `AddSubdirectory`  or even constructors) no longer take `invoking_file` and `invoking_line` as argument.

- [(#1705)](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1705) Use `SuperLU_dist` in place of `Mumps` in Lua files.